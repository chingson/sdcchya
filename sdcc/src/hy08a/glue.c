/*-------------------------------------------------------------------------

  glue.c - glues everything we have done together into one file.
                Written By -  Sandeep Dutta . sandeep.dutta@usa.net (1998)

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 2, or (at your option) any
   later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

   In other words, you are welcome to use, share and improve this program.
   You are forbidden to forbid anyone else to use, share and improve
   what you give them.   Help stamp out software-hoarding!


   2016/2017: modified by chingsonchen@hycontek.com.tw for HYCON 8-bit CPU
-------------------------------------------------------------------------*/


#ifdef _WIN32
#include <direct.h>
#else
#include <unistd.h>
#endif
#include "glue.h"
#include "dbuf_string.h"

#include "device.h"
#include "gen.h"
#include "main.h"
#include "city.h"

/*
 * Imports
 */
extern set *publics;
extern set *externs;
extern symbol *mainf;
extern struct dbuf_s *codeOutBuf;

extern void initialComments (FILE *afile);
extern operand *operandFromAst (ast *tree, int lvl);
extern value *initPointer (initList *ilist, sym_link *toType);


set *HY08A_localFunctions = NULL;
int HY08A_hasInterrupt = 0;             // Indicates whether to emit interrupt handler or not

int HY08A_stringInSet(const char *str, set **world, int autoAdd);
#define hash(x) CityHash64(x, strlen(x))


#ifdef WORDS_BIGENDIAN
#define _ENDIAN(x)  (3-x)
#else
#define _ENDIAN(x)  (x)
#endif

#define BYTE_IN_LONG(x,b) ((x>>(8*_ENDIAN(b)))&0xff)
#define IS_GLOBAL(sym)  ((sym)->level == 0)
#define IS_DEFINED_HERE(sym)    (!IS_EXTERN(sym->etype))

/* dbufs for initialized data (idata and code sections),
 * extern, and global declarations */


extern void cdbWriteSymbol(symbol *);
extern void cdbWriteFunction(symbol *, iCode *);
static struct dbuf_s *ivalBuf, *ivalBuf2, *extBuf, *gloBuf, *gloDefBuf;

static set *emitted = NULL;

static void showAllMemmaps (FILE *of); // XXX: emits initialized symbols

// to fix bug of gcc-torture-20010924-1, we add some "heap vars


static void
emitPseudoStack(struct dbuf_s *oBuf, struct dbuf_s *oBufExt)
{
	int  low = 0, high = 0, size = 0;
	int i;



    /* also emit STK symbols
     * XXX: This is ugly and fails as soon as devices start to get
     *      differently sized sharebanks, since STK12 will be
     *      required by larger devices but only up to STK03 might
     *      be defined using smaller devices. */
    //int shared = HY08A_getSharedStack(&low, &high, &size,!(0));
	HY08A_getSharedStack(&low, &high, &size, !(0));

// 2020 change _SAVE at low address
    if (!HY08A_options.isLibrarySource)
    {
        dbuf_printf (oBuf, "\n");
        dbuf_printf (oBuf, "\t.globl WSAVE\n");
        for (i = size -4; i >= 0; i--) {
            dbuf_printf (oBuf, "\t.globl STK%02d\n", i);
			dbuf_printf(oBuf, "\t.globl STK%02d_SAVE\n", i);

        } // for i
		for (i = 0; i < 3; i++)
		{
			dbuf_printf(oBuf, "\t.globl _FSR%1dL_SAVE\n", i);
			dbuf_printf(oBuf, "\t.globl _FSR%1dH_SAVE\n", i);
		}
		dbuf_printf(oBuf, "\t.globl\t_TBLPTRH_SAVE\n");
		dbuf_printf(oBuf, "\t.globl\t_TBLPTRL_SAVE\n");
		// 2022 may add pclath save
		dbuf_printf(oBuf, "\t.globl\t_PCLATH_SAVE\n");
        dbuf_printf (oBuf, "\n");

        // 16f84 has no SHAREBANK (in linkerscript) but memory aliased in two
        // banks, sigh...
        //if (1 || !shared) {
        // for single banked devices: use normal, "banked" RAM
        dbuf_printf (oBuf, ".area UDATA (DATA,REL,CON)\n");
        //} else {
        // for devices with at least two banks, require a sharebank section
        //  dbuf_printf (oBuf, ".area sharebank udata_shr\n");
        //}
        //dbuf_printf(oBuf, ";PSAVE:\t.ds 1\n");
        //dbuf_printf(oBuf, ";SSAVE:\t.ds 1\n");
        
        /* fill rest of sharebank with stack STKxx .. STK00 */
        // _save at low address
		dbuf_printf(oBuf, "_TBLPTRH_SAVE:\t.ds\t 1\n");
		dbuf_printf(oBuf, "_TBLPTRL_SAVE:\t.ds\t 1\n"); // for tempsave!!, no save in interr
		dbuf_printf(oBuf, "_PCLATH_SAVE:\t.ds\t 1\n");
		for (i = size - 4; i >= 0; i--) {
			dbuf_printf(oBuf, "STK%02d_SAVE:\t.ds\t 1\n", i); // interrupt use, remove in linker
		}
		for (i = 0; i < 3; i++)
		{
			dbuf_printf(oBuf, "_FSR%1dL_SAVE:\t.ds\t 1\n", i);
			dbuf_printf(oBuf, "_FSR%1dH_SAVE:\t.ds\t 1\n", i);
		}

		
		dbuf_printf(oBuf, "WSAVE:\t.ds 1\n"); // WSAVE *must* be in sharebank (IRQ handlers)

        for (i = size-4 ; i >= 0; i--) {
			if (i < 3 && HY08A_getPART()->isEnhancedCore == 4)
			{
				// save to TBLDL/TBLDH/TBLPTRL
				dbuf_printf(oBuf, "STK%02d:\t.equ\t 0x%02X\n", i, 0x20 - i); 

			}
			else
			{
				dbuf_printf(oBuf, "STK%02d:\t.ds\t 1\n", i);
			}
			//dbuf_printf(oBuf, "STK%02d_SAVE:\t.ds\t 1\n", i); // interrupt use, remove in linker
        } // for i
    } else {
        /* declare STKxx as extern for all files
         * except the one containing main() */
        dbuf_printf (oBufExt, "\n");
        //dbuf_printf(oBufExt, "\t;.globl PSAVE\n");
        //dbuf_printf(oBufExt, "\t;.globl SSAVE\n");
        //dbuf_printf (oBufExt, "\t.globl WSAVE\n"); 
        for (i = size - 4; i >= 0; i--) {
            char buffer[128];
            SNPRINTF(&buffer[0], 127, "STK%02d", i);
            dbuf_printf (oBufExt, "\t.globl %s\n", &buffer[0]);
			dbuf_printf(oBufExt, "\t.globl %s_SAVE\n", &buffer[0]);
            HY08A_stringInSet(&buffer[0], &emitted, 1);
        } // for i
    }
    dbuf_printf (oBuf, "\n");
}

static int
emitIfNew(struct dbuf_s *oBuf, set **emitted, const char *fmt,
          const char *name)
{
    int wasPresent = HY08A_stringInSet(name, emitted, 1);

    if (!wasPresent) {
        dbuf_printf (oBuf, fmt, name);
    } // if
    return (!wasPresent);
}

static void
HY08A_constructAbsMap (struct dbuf_s *oBuf, struct dbuf_s *gloBuf)
{
    memmap *maps[] = { data, sfr, NULL };
    int i;
    hTab *ht = NULL;
    symbol *sym;
    set *aliases;
    int addr, min=-1, max=-1;
    unsigned int size;

    for (i=0; maps[i] != NULL; i++)
    {
        for (sym = (symbol *)setFirstItem (maps[i]->syms);
                sym; sym = setNextItem (maps[i]->syms))
        {
            if (IS_DEFINED_HERE(sym) && SPEC_ABSA(sym->etype))
            {
                addr = SPEC_ADDR(sym->etype);

                /* handle CONFIG words here */ // HYCON need not config here
                //if (IS_CONFIG_ADDRESS( addr ))
                //{
                //    //fprintf( stderr, "%s: assignment to CONFIG@0x%x found\n", __FUNCTION__, addr );
                //    //fprintf( stderr, "ival: %p (0x%x)\n", sym->ival, (int)list2int( sym->ival ) );
                //    if (sym->ival) {
                //        HY08A_assignConfigWordValue( addr, (int)list2int( sym->ival ) );
                //    } else {
                //        fprintf( stderr, "ERROR: Symbol %s, which is covering a __CONFIG word must be initialized!\n", sym->name );
                //    }
                //    continue;
                //}

                if ((addr<0x1000000) && (max == -1 || (addr > max ) )) max = addr; // no more 24 bit address
                if ((addr>=0)&&(min == -1 || addr < min)) min = addr;
                //fprintf (stderr, "%s: sym %s @ 0x%x\n", __FUNCTION__, sym->name, addr);
                aliases = hTabItemWithKey (ht, addr);
                if (aliases) {
                    /* May not use addSetHead, as we cannot update the
                     * list's head in the hastable `ht'. */
                    addSet (&aliases, sym);
#if 0
                    fprintf( stderr, "%s: now %d aliases for %s @ 0x%x\n",
                             __FUNCTION__, elementsInSet(aliases), sym->name, addr);
#endif
                } else {
                    addSet (&aliases, sym);
					if(!strstr(sym->name,"SWTGTID"))
						hTabAddItem (&ht, addr, aliases);
                } // if
            } // if
        } // for sym
    } // for i

    /* now emit definitions for all absolute symbols */
    dbuf_printf (oBuf, "%s", iComments2);
    dbuf_printf (oBuf, "; absolute symbol definitions\n");
    dbuf_printf (oBuf, "%s", iComments2);
    for (addr=min; addr <= max; addr++)
    {
        size = 1;
        aliases = hTabItemWithKey (ht, addr);
        if (aliases && elementsInSet(aliases)) {
            /* Make sure there is no initialized value at this location! */
            for (sym = setFirstItem(aliases); sym; sym = setNextItem(aliases)) {
                if (sym->ival) break;
            } // for
            if (sym) continue;

            //dbuf_printf (oBuf, "UD_abs_%s_%x\tudata_ovr\t0x%04x\n",
             //            moduleName, addr, addr);
            for (sym = setFirstItem (aliases); sym;
                    sym = setNextItem (aliases))
            {
                if (getSize(sym->type) > size) {
                    size = getSize(sym->type);
                }

                /* initialized values are handled somewhere else */
                if (sym->ival) continue;

                /* emit STATUS as well as _STATUS, required for SFRs only */
                //dbuf_printf (oBuf, "%s\tres\t0\n", sym->name);
                //dbuf_printf (oBuf, "%s\n", sym->rname);

                if (IS_GLOBAL(sym) && !IS_STATIC(sym->etype) && !IS_ABSOLUTE(sym->etype)) {
                    //emitIfNew(gloBuf, &emitted, "\t.globl\t%s\n", sym->name);
                    emitIfNew(gloBuf, &emitted, "\t.globl\t%s\n", sym->rname);
                } // if
            } // for
            //dbuf_printf (oBuf, "\tres\t%d\n", size); // for our system, equ is enough!!
        } // if
    } // for i
}

/*-----------------------------------------------------------------*/
/* createInterruptVect - creates the interrupt vector              */
/*-----------------------------------------------------------------*/
static void
HY08AcreateInterruptVect (struct dbuf_s * vBuf)
{
	char buf[1024];
    mainf = newSymbol ("main", 0);
    mainf->block = 0;

    /* only if the main function exists */
    if (!(mainf = findSymWithLevel (SymbolTab, mainf)))
    {
        struct options *op = &options;
        if (!(op->cc_only || op->c1mode || op->no_assemble))
            //      werror (E_NO_MAIN);
            fprintf(stderr,"WARNING: function 'main' undefined\n");
        return;
    }

    /* if the main is only a prototype ie. no body then do nothing */
    if (!IFFUNC_HASBODY(mainf->type))
    {
        /* if ! compile only then main function should be present */
        if (!(options.cc_only || options.c1mode || options.no_assemble))
            //      werror (E_NO_MAIN);
            fprintf(stderr,"WARNING: function 'main' undefined\n");

        return;
    }
    dbuf_printf(vBuf, "%s", "\t.globl _main\n");
    dbuf_printf (vBuf, "%s", iComments2);
    dbuf_printf (vBuf, "; reset vector \n");
    dbuf_printf (vBuf, "%s", iComments2);
    // Lkr file should place section STARTUP at address 0x0, but does not ...
    dbuf_printf (vBuf, "\t.area STARTUP\t(%s,REL,CON) \n", CODE_NAME);
    //dbuf_printf(vBuf, "\t.ORG 0x0 \n", CODE_NAME);

    dbuf_printf (vBuf, "\tnop\n"); /* first location for used by incircuit debugger */
    //dbuf_printf (vBuf, "\tpagesel __sdcc_gsinit_startup\n");
	
	snprintf(buf, 1023, "__sdcc_gsinit_startup%d", HY08A_getPART()->dataMemSize);
	if (HY08A_getPART()->isEnhancedCore ==4 || HY08A_getPART()->isEnhancedCore == 3) // p41 also fsr2?
		strcat(buf, "fsr2");
	else if(HY08A_getPART()->isEnhancedCore ==5)
		strcat(buf, "_08e");
	// p55,p56 becomre fsr2start80
	if((!strcmp(HY08A_getPART()->name,"HY17P55"))|| (!strcmp(HY08A_getPART()->name, "HY17P56")))
		strcat(buf, "ini80");
    dbuf_printf (vBuf, "\tjmp\t%s\n", buf); // this is 2 words
    dbuf_printf(vBuf, "\tnop\n"); /* first location for used by incircuit debugger */
    //dbuf_printf(vBuf, "\tnop\n"); /* first location for used by incircuit debugger */

	popGetExternal(buf, 0);// this is a label
}


/*-----------------------------------------------------------------*/
/* initialComments - puts in some initial comments                 */
/*-----------------------------------------------------------------*/
static void
HY08AinitialComments (FILE * afile)
{
    initialComments (afile);
    fprintf (afile, "; Port for HYCON CPU\n");
    fprintf (afile, "%s", iComments2);

}

int
HY08A_stringInSet(const char *str, set **world, int autoAdd)
{
    char *s;

    if (!str) return 1;
    assert(world);

    for (s = setFirstItem(*world); s; s = setNextItem(*world))
    {
        /* found in set */
        if (0 == strcmp(s, str)) return 1;
    }

    /* not found */
    if (autoAdd) 
    {
        //if(world == &emitted && strstr(str,"_bar2_x" ))
            //fprintf(stderr,"_bar2_x\n");
        addSet(world, Safe_strdup(str));
    }
    return 0;
}

static void
HY08AprintLocals (struct dbuf_s *oBuf)
{
    set *allregs[6] = { dynAllocRegs/*, dynStackRegs, dynProcessorRegs*/,
                        dynDirectRegs, dynDirectBitRegs,  dynDirectXRegs,NULL, NULL/*, dynInternalRegs */
                      };
    reg_info *reg;
    int i, is_first = 1, is_xfirst=1;
    static unsigned sectionNr = 0;
    int hasLocStack = 0;

    dbuf_printf(oBuf, "\t.area IDATA (DATA,REL,CON); pre-def\n");
    dbuf_printf(oBuf, "\t.area IDATAROM (CODE,REL,CON); pre-def\n");
    dbuf_printf(oBuf, "\t.area UDATA (DATA,REL,CON); pre-def\n");

    /* emit all registers from all possible sets */
    for (i = 0; i < 6; i++) {
        if (allregs[i] == NULL) continue;

        for (reg = setFirstItem(allregs[i]); reg; reg = setNextItem(allregs[i])) {
            if (reg->isEmitted) continue;
			if (HY08A_stringInSet(reg->name, &emitted, 0))
			{
				reg->isEmitted = 1;
				continue;
			}
            if (reg->isFuncLocal && !reg->isLocalStatic)
            {
                hasLocStack = 1;
                continue;
            }

            if (reg->wasUsed && !reg->isExtern && !(reg->isBitField && reg->reg_alias)) {
                if (!HY08A_stringInSet(reg->name, &emitted, 1)) {
                    if (reg->isFixed) {
                        // Should not happen, really...
                        assert ( !"Compiler-assigned variables should not be pinned... This is a bug." );
                        dbuf_printf(oBuf, "\t.area UDATA (DATA,REL,CON) ;UDL_%s_%u\tudata\t0x%04X\n%s\tres\t%d\n",
                                    moduleName, sectionNr++, reg->address, reg->name, reg->size);
                    } else {
                        if (getenv("SDCC_HY08A_SPLIT_LOCALS")) {
                            // assign each local register into its own section
                            dbuf_printf(oBuf, "\t.area UDATA (DATA,REL,CON) ;UDL_%s_%u\tudata\n%s\tres\t%d\n",
                                        moduleName, sectionNr++, reg->name, reg->size);
                        } else {
                            // group all local registers into a single section
                            // This should greatly improve BANKSEL generation...
                            if (i == 3 && is_xfirst)
                            {
                                dbuf_printf(oBuf, "\t.area UXDATA (XDATA,REL,CON) ;UDL_%s_%u\tuxdata\n", moduleName, sectionNr++);
                                is_xfirst = 0;
                            } else if (i!=3 && is_first) {
                                dbuf_printf(oBuf, "\t.area UDATA (DATA,REL,CON) ;UDL_%s_%u\tudata\n", moduleName, sectionNr++);
                                is_first = 0;
                            }
							//if (!(reg->pc_type == PO_DIR && reg->size == 1))

							// special case for bit fields!!
							if (reg->packBitIni)
							{
								dbuf_printf(oBuf, "\t.area IDATA (DATA,REL,CON) ;bf_%s_%u\tidata\n", moduleName, sectionNr++);
								dbuf_printf(oBuf, "%s:\t.ds\t%d\n", reg->name, reg->size);
								dbuf_printf(oBuf, "\t.area IDATAROM (CODE,REL,CON) ;bf_%s_%u\tidatarom\n", moduleName, sectionNr++);
								dbuf_printf(oBuf, "%s_shadow:\t.db\t0x%02X\n", reg->name, reg->packBitIni);
								//shadow_size = 0;
								is_first = 1; is_xfirst = 1;
							}else
								dbuf_printf(oBuf, "%s:\t.ds\t%d\n", reg->name, reg->size);
							//else
								//fprintf(stderr, "special reg %s\n", reg->name);
                        }
                    }
                }
            }
            reg->isEmitted = 1;
        } // for
    } // for

	if (criticalTempRegID != 0x3000) // 2020 need dump critical ID
	{
		int i;
		dbuf_printf(oBuf, "\t.area UDATA (DATA,REL,CON) ;UDL_%s_%u\tudata\n", moduleName, sectionNr++);
		for (i = 0x3000; i < criticalTempRegID; i++)
			dbuf_printf(oBuf, "r0x%x:\t.ds 1\n", i);
	}
    if (!hasLocStack)
        return;
    dbuf_printf(oBuf, "\t.area LOCALSTK (STK); local stack var\n");
    // if that var cannot found linker will kill the instruction
    for (i = 0; i < 6; i++) {
        if (allregs[i] == NULL) continue;

        for (reg = setFirstItem(allregs[i]); reg; reg = setNextItem(allregs[i])) {
            if (reg->isEmitted) continue;

            if (reg->isFuncLocal && !reg->isBitField)// functionlocal
            {
                if(!reg->isExtern )
                    dbuf_printf(oBuf, "%s:\t.ds\t%d\n", reg->name, reg->size);
                if (reg->isPublic)
                    dbuf_printf(oBuf, "\t.globl %s\n", reg->name);

                reg->isEmitted = 1;
            }
        } // for
    } // for
}

/*-----------------------------------------------------------------*/
/* emitOverlay - will emit code for the overlay stuff              */
/*-----------------------------------------------------------------*/
static void
HY08AemitOverlay (struct dbuf_s * aBuf)
{
    set *ovrset;

    /*  if (!elementsInSet (ovrSetSets))*/

    /* the hack below, fixes translates for devices which
    * only have udata_shr memory */
    dbuf_printf (aBuf, "%s\t%s\n",
                 (elementsInSet(ovrSetSets)?"":";"),
                 port->mem.overlay_name);

    /* for each of the sets in the overlay segment do */
    for (ovrset = setFirstItem (ovrSetSets); ovrset;
            ovrset = setNextItem (ovrSetSets))
    {

        symbol *sym;

        if (elementsInSet (ovrset))
        {
            /* this dummy area is used to fool the assembler
            otherwise the assembler will append each of these
            declarations into one chunk and will not overlay
                    sad but true */

            /* I don't think this applies to us. We are using gpasm.  CRF */

            dbuf_printf (aBuf, ";\t.area _DUMMY\n");
            /* output the area informtion */
            dbuf_printf (aBuf, ";\t.area\t%s\n", port->mem.overlay_name);   /* MOF */
        }

        for (sym = setFirstItem (ovrset); sym;
                sym = setNextItem (ovrset))
        {

            /* if extern then do nothing */
            if (IS_EXTERN (sym->etype))
                continue;

            /* if allocation required check is needed
            then check if the symbol really requires
            allocation only for local variables */
            if (!IS_AGGREGATE (sym->type) &&
                    !(sym->_isparm && !IS_REGPARM (sym->etype))
                    && !sym->allocreq && sym->level)
                continue;

            /* if global variable & not static or extern
            and addPublics allowed then add it to the public set */
            if ((sym->_isparm && !IS_REGPARM (sym->etype))
                    && !IS_STATIC (sym->etype))
                addSetHead (&publics, sym);

            /* if extern then do nothing or is a function
            then do nothing */
            if (IS_FUNC (sym->type))
                continue;

            /* print extra debug info if required */
            //if (options.debug || sym->level == 0)
            if(sym->level==0)
            {
                if (!sym->level)
                {   /* global */
                    if (IS_STATIC (sym->etype))
                        dbuf_printf (aBuf, "F%s_", moduleName); /* scope is file */
                    else
                        dbuf_printf (aBuf, "G_");       /* scope is global */
                }
                else
                    /* symbol is local */
                    dbuf_printf (aBuf, "L%s_",
                                 (sym->localof ? sym->localof->name : "-null-"));
                dbuf_printf (aBuf, "%s_%ld_%d", sym->name, sym->level, sym->block);
            }

            /* if is has an absolute address then generate
            an equate for this no need to allocate space */
            if (SPEC_ABSA (sym->etype))
            {

                //if (options.debug || sym->level == 0)
                if(sym->level==0)
                    dbuf_printf (aBuf, " == 0x%04x\n", SPEC_ADDR (sym->etype));

                dbuf_printf (aBuf, "%s\t=\t0x%04x\n",
                             sym->rname,
                             SPEC_ADDR (sym->etype));
            }
            else
            {
                // if (options.debug || sym->level == 0)
                if (sym->level == 0)
                    dbuf_printf (aBuf, "==.\n");

                /* allocate space */
                dbuf_printf (aBuf, "%s:\n", sym->rname);
                dbuf_printf (aBuf, "\t.ds\t0x%04x\n", (unsigned int) getSize (sym->type) & 0xffff);
            }

        }
    }
}


static void
HY08A_emitInterruptHandler (FILE * asmFile)
{
    if (HY08A_hasInterrupt)
    {

        fprintf (asmFile, "%s", iComments2);
        fprintf (asmFile, "; interrupt and initialization code\n");
        fprintf (asmFile, "%s", iComments2);
        // Note - for mplink may have to enlarge section vectors in .lnk file
        // Note: Do NOT name this code_interrupt to avoid nameclashes with
        //       source files's code segment (interrupt.c -> code_interrupt)
        fprintf (asmFile, "\t.area c_interrupt\t(%s,REL,CON)\n", CODE_NAME);
//				fprintf(asmFile, "\t.ORG 0x4\n");

        /* interrupt service routine */
        fprintf (asmFile, "__sdcc_interrupt:\n");
        copypCode(asmFile, 'I');
    }
}

/*
static bool reginFunc(reg_info *r, char *fname)
{
    pBlock *pb;
    pCode *pc;
    int found = 0;
    for (pb = the_pFile->pbHead; pb; pb = pb->next)
    {
        for (pc = pb->pcHead; pc; pc = pc->next)
        {
            if (isPCF(pc))
            {
                if (!strcmp(fname, PCF(pc)->fname + 1))
                    found = 1;
                break;
            }
        }
        if (found)
            break;
    }
    if (!found)
        return FALSE;
    if (isinSet(pb->tregisters, (const void*)r) ||
            isinSet(pb->txregisters, (const void*)r))
        return TRUE;
    return FALSE;
}
*/
// some local variable optimized out, we check it
/*
static bool symbolValid(symbol *sym)
{
    char *funname;
    symbol *TempSym;
    int a;
    return true;
    funname = sym->localof->name;

    if (!(sym->reqv)) // convert to special value for reference
    {
        return true; // just becomes local
    }

    TempSym = OP_SYMBOL(sym->reqv);

    if (!TempSym->isspilt || TempSym->remat || TempSym->rematX)
    {

        for (a = 0; a < 4; a++)
        {
            if (TempSym->regs[a])
            {
                if (!reginFunc(TempSym->regs[a], funname))
                    return FALSE;
            }
        }
    }
    return true;




}
*/

extern FILE *cdbFilePtr;
extern int cdbWriteType(structdef *sdef, int block, int inStruct, const char *tag);
void
appendCdbStructBlock(int block, FILE *fp)
{
    int i;
    bucket **table = StructTab;
    bucket *chain;
    symbol *sym;

    /* go thru the entire  table  */
    cdbFilePtr = fp;
    // structs only
    for (i = 0; i < 256; i++)
    {
        for (chain = table[i]; chain; chain = chain->next)
        {
            if (chain->block >= block)
            {
                fprintf(fp, "\t;--cdb--");

                cdbWriteType((structdef *)chain->sym, chain->block, 0, NULL);
            }
        }
    }
    //// also write other
    //if (data)
    //{
    //	for (sym = setFirstItem(data->syms); sym; sym = setNextItem(data->syms))
    //	{
    //		if (symbolValid(sym));
    //		{
    //			fprintf(fp, "\t;--cdb--");
    //			cdbWriteSymbol(sym);
    //		}
    //	}
    //}

    //
    //if (idata)
    //{
    //	for (sym = setFirstItem(idata->syms); sym; sym = setNextItem(idata->syms))
    //		debugFile->writeSymbol(sym);
    //}
    ///*
    //if (bit)
    //{
    //	for (sym = setFirstItem(bit->syms); sym; sym = setNextItem(bit->syms))
    //		debugFile->writeSymbol(sym);
    //}

    //if (pdata)
    //{
    //	for (sym = setFirstItem(pdata->syms); sym; sym = setNextItem(pdata->syms))
    //		debugFile->writeSymbol(sym);
    //}
    //*/
    //if (xdata)
    //{
    //	for (sym = setFirstItem(xdata->syms); sym; sym = setNextItem(xdata->syms))
    //	{
    //		fprintf(fp, "\t;--cdb--");
    //		cdbWriteSymbol(sym);
    //	}
    //}


    // note that function symbol need to be emitted here!!
    if (code)
    {
        for (sym = setFirstItem(code->syms); sym; sym = setNextItem(code->syms))
        {
            fprintf(fp, "\t;--cdb--");
            cdbWriteSymbol(sym);
            if (!IS_EXTERN(sym->etype))
            {
                fprintf(fp, "\t;--cdb--");
                if (IS_FUNC(sym->type))
                    cdbWriteFunction(sym, NULL);
                else
                    cdbWriteSymbol(sym);
            }
        }
    }

}

// dumpOpt dump the optimization list



static void dumpTypeDef(FILE *asmFile) // only level 0
{
	int i;
	bucket *bp;
	for (i = 0; i < 256; i++)
	{
		bp = TypedefTab[i];
		while (bp)
		{
			if (bp->level == 0 && bp->block == 0) // only 00, global
			{
				fputs("\t;--cdb--D:", asmFile); // D means typedef!!
				cdbWriteSymbol(bp->sym);
			}
			bp = bp->next;
		}
	}
}


static void dumpVarOpt(FILE *asmfile)
{
	pBlock *pb;
	char buf[1024];
	//varopt *vp,*vp2;
	varopt *vp;
	uint64 *hashes, hashid;
	int optCount = 0;
	int i = 0;
	int j;
	if (!the_pFile)
		return;
	fprintf(asmfile, "%s\n", "\t;Following is optimization info, \n\t;xxcdbxxW:dst:src+offset:srclit:just-remove");


	for (pb = the_pFile->pbHead; pb; pb = pb->next)
	{
		// combine the list
		for (vp = setFirstItem(pb->opt_list); vp; vp = setNextItem(pb->opt_list))
		{
			SNPRINTF(buf, 1023, "\t;--cdb--W:%s:%s+%d:%d:%d\n", vp->source_reg->name, (vp->dest_reg) ? (vp->dest_reg->name) :
				"NULL",
				vp->dstOffset,
				vp->lit,
				vp->sta_remove);
			vp->varOptString = strdup(buf);
			vp->hashid = CityHash64(buf, strlen(buf));
			optCount++;
		}
	}
	hashes = Safe_calloc(optCount, sizeof(uint64));
	for (pb = the_pFile->pbHead; pb; pb = pb->next)
	{
		for (vp = setFirstItem(pb->opt_list); vp; vp = setNextItem(pb->opt_list))
		{
			int found = 0;
			hashid = vp->hashid;
			for (j = 0; j < i; j++)
			{
				if (hashes[j] == hashid)
				{
					found = 1;
					break;
				}
			}
			if (found)
				continue;
			hashes[i++] = vp->hashid;
			fprintf(asmfile, "%s", vp->varOptString);
		}
	}
	
}

// dump peep infomation
extern void dumpPeepInfoHYA(FILE *asmf);


/*-----------------------------------------------------------------*/
/* glue - the final glue that hold the whole thing together        */
/*-----------------------------------------------------------------*/
extern set *asmOptionsSet;
extern set *linkOptionsSet;

// process temp file to real asm file,
// no close before close file
// for the live range of local variables
static int asmPostProc(FILE *tempFile, FILE *asmFile)
{
	char lineBuf[4096];
	char *ptr;
	int outLineNo;
	int *seqMap = (int*)malloc(1024 * 1024 * 4); // no more than 1Meg line
	char **localNameArray = (char**)calloc(10240, sizeof(char*));
	int  *localMinLineNo = (int*)calloc(10240, sizeof(int));
	int  *localMaxLineNo = (int*)calloc(10240, sizeof(int));
	uint64_t *funcNameHashMap = (uint64_t*)calloc(1024 * 1024, sizeof(uint64_t));
	int localVarIndex = 0;
	int i, j;
	int prevseqno = -1;

	if (seqMap == NULL || funcNameHashMap == NULL || localNameArray==NULL || localMinLineNo==NULL ||
		localMaxLineNo==NULL)
	{
		fprintf(stderr, "Internal error, memory alloc error at %s:%d\n", __FILE__, __LINE__);
		exit(-__LINE__);
	}
	memset(seqMap, -1, 1024 * 1024 * 4);
	memset(funcNameHashMap, 0, 1024 * 1024 * 8);
	for(outLineNo=1; outLineNo<1024*1024; outLineNo++)
	{
		ptr = fgets(lineBuf, sizeof(lineBuf) - 1, tempFile);
		if (ptr == NULL)
			break;

		// local variable is depends on functions
		// we parse func with name 

		if (!strncmp(lineBuf, "\t.FUNC",5)) // special token compare from begin
		{
			char fnamebuf[4096];
		
			strncpy(fnamebuf, lineBuf + 8, 4095);

			j = strlen(fnamebuf);
			for (i = 0; i < j; i++)
				if (fnamebuf[i] == ':' || fnamebuf[i] == '\\' || fnamebuf[i] == '\n')
				{
					fnamebuf[i] = '\0';
					break;
				}
			funcNameHashMap[outLineNo] = hash(fnamebuf);
			fputs(lineBuf, asmFile);
			prevseqno = -1;
			continue;
		}
		if (!strncmp(lineBuf, "\t.ENDFUNC", 5)) // special token compare from begin
		{
			prevseqno = -1;
		}


		// check if SEQ or RNG in line
		if ((ptr = strstr(lineBuf, ";$SEQ:")) != NULL)
		{
			int seqno=-1;
			sscanf(ptr, ";$SEQ:%d", &seqno);
			if (seqno > 0)
			{
				seqMap[outLineNo] = seqno;
			}
			*ptr = '\0';
			//fputs(lineBuf, asmFile);
			fprintf(asmFile, "%s\n", lineBuf);
			prevseqno = seqno;
			continue;
		}
		seqMap[outLineNo] = prevseqno;
		if ((ptr = strstr(lineBuf, "$RNG"))!=NULL && strstr(lineBuf,"c1db")!=NULL)
		{
			// no write, but proc is complex
			// we find previous .
			uint64_t fnameHash;
			int minseq=10000000, maxseq=-1; // translate seq to line 
			int minLine = 10000000, maxLine = -1;
			char *p1 = ptr;
			char *p2;
			


			--outLineNo; // this line no out;
			if (2 != sscanf(ptr, "$RNG%d.%d", &minseq, &maxseq))
				continue;

			while (p1 != lineBuf && *p1 != '.')
				p1--;
			if (*p1 != '.') // strange
			{
				continue;
			}
			p2 = p1; *p1 = '\0';
			p1++;// p1 is the assembly variable name
			*ptr = '\0'; ++ptr; // now ptr points to "RNG"
			// now we need use p2 to find the function name , without leading_
			while (p2 != lineBuf && *p2 != '.')
				p2--;
			if (*p2 != '.')
			{
				continue;
			}
			p2++; // now p2 points to function name
			fnameHash = hash(p2);
			for (i = 0; i < outLineNo; i++)
				if (funcNameHashMap[i] == fnameHash)
					break;
			for (++i; i < outLineNo; i++)
			{
				if (funcNameHashMap[i] != 0ULL)
					break;
				if (seqMap[i] >= minseq && seqMap[i] <= maxseq)
				{
					if (i < minLine)
						minLine = i;
					if (i > maxLine)
						maxLine = i;
				}

			}
			if (minLine > 0 && maxLine > 0)
			{
				localMinLineNo[localVarIndex] = minLine;
				localMaxLineNo[localVarIndex] = maxLine;
				localNameArray[localVarIndex] = strdup(p1);
				localVarIndex++;
				if (localVarIndex >= 10240)
					break;
			}
			continue;
		}
		fprintf(asmFile, "%s", lineBuf); // no newline

	}
	// now we append local var information and ".END"
	for (i = 0; i < localVarIndex; i++)
	{
		fprintf(asmFile, "\t;--c2db--RNG$%s$%d$%d\n", localNameArray[i], localMinLineNo[i], localMaxLineNo[i]);
	}
	fprintf(asmFile, "\tEND\n");
	free(seqMap);
	free(localMaxLineNo);
	free(localMinLineNo);
	free(localNameArray);
	free(funcNameHashMap);
	return 0;

}

// glue, the main operation list of back-end
void
hyaglue (void )
{
    FILE *asmFile;
	FILE *asmFileProc;
    struct dbuf_s ovrBuf;
    struct dbuf_s vBuf;
	
	
#ifndef _WIN32
	char dirbuf[1024];
#endif

    dbuf_init(&ovrBuf, 4096);
    dbuf_init(&vBuf, 4096);
	if (options.model == MODEL_LARGE)
	{
		addSet(&asmOptionsSet, "-r2");
		if (HY08A_getPART()->isEnhancedCore == 4)
			addSet(&linkOptionsSet, "-rfd");
		else if(		HY08A_getPART()->isEnhancedCore!=3)
			addSet(&linkOptionsSet, "-rfa");
		
	}
	if (HY08A_options.force_h08a)
		addSet(&linkOptionsSet, "-f08a"); // linker need force, too!!

    if (!pCodeInitRegisters())
        return;




    /* check for main() */
    mainf = newSymbol ("main", 0);
    mainf->block = 0;
    mainf = findSymWithLevel (SymbolTab, mainf);

    if (!mainf || !IFFUNC_HASBODY(mainf->type))
    {
        /* main missing -- import stack from main module */
        //fprintf (stderr, "main() missing -- assuming we are NOT the main module\n");
        HY08A_options.isLibrarySource = 1;
    }

    /* At this point we've got all the code in the form of pCode structures */
    /* Now it needs to be rearranged into the order it should be placed in the */
    /* code space */

    movepBlock2Head('P');              // Last
    movepBlock2Head(code->dbName);
    movepBlock2Head('X');
    movepBlock2Head(statsg->dbName);   // First


    /* print the global struct definitions */
    //if (options.debug)
    cdbStructBlock (0);

    /* do the overlay segments */
    HY08AemitOverlay(&ovrBuf);

    /* PENDING: this isnt the best place but it will do */
    if (port->general.glue_up_main) {
        /* create the interrupt vector table */
        HY08AcreateInterruptVect (&vBuf);
    }

    AnalyzepCode('*');

    //if ((0))
    //{
    //    ReuseReg(); // ReuseReg where call tree permits ==> reuse when link?
    //}

	// only not p41 and have main we inline pcode
	if (mainf && (HY08A_getPART()->isEnhancedCore < 3 )) // no for 08e either?
	{
		InlinepCode();

		AnalyzepCode('*');
	}

    if (options.debug) pcode_test();





    /* now put it all together into the assembler file */
    /* create the assembler file name */

    if ((options.no_assemble || options.c1mode) && fullDstFileName)
    {
        sprintf (buffer, "%s", fullDstFileName);
    }
    else
    {
        sprintf (buffer, "%s", dstFileName);
        strcat (buffer, ".asm");
    }

    if (!(asmFileProc = fopen (buffer, "w"))) {
        werror (E_FILE_OPEN_ERR, buffer);
        exit (1);
    }

	if (!(asmFile = tmpfile()))
	{
		werror(E_FILE_OPEN_ERR, buffer);
		exit(1);
	}
	// use temp file first

    /* prepare statistics */
	resetpCodeStatistics();

    /* initial comments */
    HY08AinitialComments (asmFile);

    /* print module name */
#ifdef _WIN32
	fprintf(asmFile, ";\t;CCFROM:\"%s\"\n", _getcwd(NULL, 0));
#else
	fprintf(asmFile, ";\t;CCFROM:\"%s\"\n", getcwd(dirbuf,1023));
#endif
    fprintf(asmFile, ";%s\t.file\t\"%s\"\n",
            options.debug ? "" : ";", fullSrcFileName);
    //fprintf(asmFile, "\t; --cdb--M:%s\n", moduleName);

    /* Let the port generate any global directives, etc. */
    if (port->genAssemblerStart)
    {
        port->genAssemblerStart(asmFile);
    }

    /* Put all variables into a cblock */

    reCheckLocalVar(the_pFile);

    //AnalyzeBanking();

    /* emit initialized data */
    //showAllMemmaps(asmFile);

    /* print the locally defined variables in this module */
    appendCdbStructBlock(0, asmFile);
    writeUsedRegs(asmFile);

    /* create the overlay segments */
    fprintf (asmFile, "%s", iComments2);
    fprintf (asmFile, "; overlayable items in internal ram \n");
    fprintf (asmFile, "%s", iComments2);
    dbuf_write_and_destroy (&ovrBuf, asmFile);

    /* copy the interrupt vector table */
    if (mainf && IFFUNC_HASBODY(mainf->type))
        dbuf_write_and_destroy (&vBuf, asmFile);
    else
        dbuf_destroy(&vBuf);

    /* create interupt ventor handler */
    HY08A_emitInterruptHandler (asmFile);

    /* copy over code */
//        fprintf (asmFile, "%s", iComments2);
//        fprintf (asmFile, "; code\n");
//        fprintf (asmFile, "%s", iComments2);
	if(options.code_seg && strcmp(options.code_seg,"code"))
		fprintf(asmFile, ".area %s   ; %s-code \n",options.code_seg,  moduleName);
	else
		fprintf(asmFile, ".area CCODE (%s,REL,CON) ; %s-code \n",port->mem.code_name,moduleName);

    /* unknown */
    copypCode(asmFile, 'X');

    /* _main function */
    copypCode(asmFile, 'M');

    /* other functions */
    copypCode(asmFile, code->dbName);

    /* unknown */
    copypCode(asmFile, 'P');

    // no need dump static, see LST file
    //dumppCodeStatistics (asmFile);

	//if (!(0))
	//{
	//	// we need change parameter symbol to stack
	//	// so the debugger can see the parameter when it enters the function
	//	// though later optimizaton is another story
	//	symbol *fsym;
	//	for (fsym = setFirstItem(code->syms); fsym; fsym = setNextItem(code->syms))
	//	{
	//		if (IS_PTR(fsym->type) || !IFFUNC_HASBODY(fsym->type))
	//			continue;
	//		value *paraVal;
	//		symbol *paramSym,*paramSym2;
	//		char buf[1024];
	//		int i, size, j, idx = 0;
	//		int skipFirstParaByte = 1;
	//		for (paraVal = fsym->type->funcAttrs.args; paraVal; paraVal = paraVal->next)
	//		{
	//			paramSym = paraVal->sym;
	//			paramSym2 = OP_SYMBOL(paramSym->reqv);
	//			if (!paramSym2)
	//				continue;
	//			size = getSize(paramSym->type);
	//			for (j = 0; j < size; j++)
	//			{
	//				if (skipFirstParaByte)
	//				{
	//					skipFirstParaByte = 0; // first is by W!!
	//					continue;
	//				}

	//				SNPRINTF(buf, 1023, "%s_STK%02d", fsym->rname, idx++);
	//				paramSym2->regs[size-1-j] = regFindWithName(buf);
	//			}

	//		}
	//	}
	//}


    showAllMemmaps(asmFile);
	dumpTypeDef(asmFile);
	dumpVarOpt(asmFile);

	if (HY08A_options.dumpPeepInfo)
		dumpPeepInfoHYA(asmFile);

	fflush(asmFile);
	fseek(asmFile, 0, SEEK_SET);


	asmPostProc(asmFile, asmFileProc);
    

    fclose (asmFile);
	fclose(asmFileProc);
    HY08A_debugLogClose();
}

/*
 * Deal with initializers.
 */
#undef DEBUGprintf
#if 0
// debugging output
#define DEBUGprintf printf
#else
// be quiet
#define DEBUGprintf 1 ? (void)0 : (void)printf
#endif

/*
static char *
parseIvalAst (ast *node, int *inCodeSpace) {
#define LEN 4096
    char *buffer = NULL;
    char *left, *right;

    if (IS_AST_VALUE(node)) {
        value *val = AST_VALUE(node);
        symbol *sym = IS_AST_SYM_VALUE(node) ? AST_SYMBOL(node) : NULL;
        if (inCodeSpace && val->type
                && (IS_FUNC(val->type) || IS_CODE(getSpec(val->type))))
        {
            *inCodeSpace = 1;
        }
        if (inCodeSpace && sym
                && (IS_FUNC(sym->type)
                    || IS_CODE(getSpec(sym->type))))
        {
            *inCodeSpace = 1;
        }

        DEBUGprintf ("%s: AST_VALUE\n", __FUNCTION__);
        if (IS_AST_LIT_VALUE(node)) {
            buffer = Safe_alloc(LEN);
            SNPRINTF(buffer, LEN, "0x%lx", AST_ULONG_VALUE (node));
        } else if (IS_AST_SYM_VALUE(node)) {
            assert ( AST_SYMBOL(node) );
            
            //printf ("sym %s: ", AST_SYMBOL(node)->rname);
            //printTypeChain(AST_SYMBOL(node)->type, stdout);
            //printTypeChain(AST_SYMBOL(node)->etype, stdout);
            //printf ("\n---sym %s: done\n", AST_SYMBOL(node)->rname);
            
            buffer = Safe_strdup(AST_SYMBOL(node)->rname);
        } else {
            assert ( !"Invalid values type for initializers in AST." );
        }
    } else if (IS_AST_OP(node)) {
        DEBUGprintf ("%s: AST_OP\n", __FUNCTION__);
        switch (node->opval.op) {
        case CAST:
            assert (node->right);
            buffer = parseIvalAst(node->right, inCodeSpace);
            DEBUGprintf ("%s: %s\n", __FUNCTION__, buffer);
            break;
        case '&':
            assert ( node->left && !node->right );
            buffer = parseIvalAst(node->left, inCodeSpace);
            DEBUGprintf ("%s: %s\n", __FUNCTION__, buffer);
            break;
        case '+':
            assert (node->left && node->right );
            left = parseIvalAst(node->left, inCodeSpace);
            right = parseIvalAst(node->right, inCodeSpace);
            buffer = Safe_alloc(LEN);
            SNPRINTF(buffer, LEN, "(%s + %s)", left, right);
            DEBUGprintf ("%s: %s\n", __FUNCTION__, buffer);
            Safe_free(left);
            Safe_free(right);
            break;
        case '[':
            assert ( node->left && node->right );
            assert ( IS_AST_VALUE(node->left) && AST_VALUE(node->left)->sym );
            right = parseIvalAst(node->right, inCodeSpace);
            buffer = Safe_alloc(LEN);
            SNPRINTF(buffer, LEN, "(%s + %u * %s)",
                     AST_VALUE(node->left)->sym->rname, getSize(AST_VALUE(node->left)->type), right);
            Safe_free(right);
            DEBUGprintf ("%s: %s\n", __FUNCTION__, &buffer[0]);
            break;
        default:
            assert ( !"Unhandled operation in initializer." );
            break;
        }
    } else {
        assert ( !"Invalid construct in initializer." );
    }

    return (buffer);
}
*/

/*
 * Emit the section preamble, absolute location (if any) and
 * symbol name(s) for intialized data.
 */
static int
emitIvalLabel(struct dbuf_s *oBuf, symbol *sym, int inrom) //
{
    char *segname;
    static int in_code = 0;
    static int sectionNr = 0;
	int needCopy = 0;

    if (sym) {
        // code or data space?
        if (IS_CODE(getSpec(sym->type))) {
            segname = "code";
            in_code = 1;
        }
        else {
            segname = "data";
            in_code = 0;
        }
        if (in_code)
        {
			//if (sym->needCopy == 1)
			//{
			//	dbuf_printf(oBuf, "\n\t.area\tIDATAROM\t(CODE) ;%s-%d-%s", moduleName, sectionNr++, segname); // need
			//}
			//else if (sym->needCopy == 2)
			//{
			//	dbuf_printf(oBuf, "\n\t.area\tXDATAROM\t(CODE) ;%s-%d-%s", moduleName, sectionNr++, segname); // need
			//}
			//else
			if(options.const_seg)
				dbuf_printf(oBuf, "\n\t.area\t%s\t ;%s-%d-%s, const\n", options.const_seg, moduleName, sectionNr++, segname);
			else
				dbuf_printf(oBuf, "\n\t.area\tSEGC\t(CODE) ;%s-%d-%s, const\n", moduleName, sectionNr++, segname); // need
        }
        else
        {
            if (IN_XSPACE(SPEC_OCLS(sym->etype)))
            {
				if (inrom) // special abs
				{
					if (SPEC_ABSA(sym->etype))
					{
						int a = SPEC_ADDR(sym->etype);
						dbuf_printf(oBuf, "\n\t.area\tXDATAROM\t(CODE) at 0x%X;%s-%d-%s", a-0x200, moduleName, sectionNr++, segname); // xdata from 0x200
					}else

						dbuf_printf(oBuf, "\n\t.area\tXDATAROM\t(CODE) ;%s-%d-%s", moduleName, sectionNr++, segname); // need
				}
                else
                    dbuf_printf(ivalBuf2, "\n\t.area\tIDATAX\t(XDATA) ;%s-%d-%s", moduleName, sectionNr++, segname); // shadow

            }
            else
            {
				if (inrom)
				{
					if (SPEC_ABSA(sym->etype))
					{
						int a = SPEC_ADDR(sym->etype);
						dbuf_printf(oBuf, "\n\t.area\tIDATAROM\t(CODE) at 0x%X ;%s-%d-%s",a, moduleName, sectionNr++, segname); // xdata from 0x200
					}
					else
						dbuf_printf(oBuf, "\n\t.area\tIDATAROM\t(CODE) ;%s-%d-%s", moduleName, sectionNr++, segname); // need
				}
                else
                    dbuf_printf(ivalBuf2, "\n\t.area\tIDATA\t(DATA) ;%s-%d-%s", moduleName, sectionNr++, segname); // shadow
            }
        }
        if (SPEC_ABSA(getSpec(sym->type)) && !inrom ) {
            // specify address for absolute symbols
			int i = SPEC_ADDR(sym->etype);
            dbuf_printf(oBuf, "\n%s\t.equ\t0x%04X", sym->rname, i);// first ch
			
        } // if
		else {
			if (!in_code && inrom) // this case, we use shadow
			{
				dbuf_printf(oBuf, "\n%s_shadow:\n", sym->rname);
				if (IN_XSPACE(SPEC_OCLS(sym->etype)))
					needCopy = 2; // if this is 1, the EQU should be in IDATAROM / XDATAROM
				else
					needCopy = 1;
				//shadow_size = 0;
			}
			else
				dbuf_printf(oBuf, "\n%s:\n", sym->rname);
		}
        addSet(&emitted, sym->rname);
    }
    return (needCopy);
}

//static int
//emitIvalLabel(struct dbuf_s *oBuf, symbol *sym)
//{
//    char *segname;
//    static int in_code = 0;
//    static int sectionNr = 0;
//
//    if (sym) {
//        // code or data space?
//        if (IS_CODE(getSpec(sym->type))) {
//            segname = "code";
//            in_code = 1;
//        } else {
//            segname = "data";
//            in_code  = 0;
//        }
//		dbuf_printf(oBuf, "\n.area IDATA (%s,REL,CON) ; ID_%s_%d", segname, moduleName, sectionNr++);
//        if (SPEC_ABSA(getSpec(sym->type))) {
//            // specify address for absolute symbols
//            dbuf_printf(oBuf, "\t0x%04X", SPEC_ADDR(getSpec(sym->type)));
//        } // if
//        dbuf_printf(oBuf, "\n%s:\n", sym->rname);
//
//        addSet(&emitted, sym->rname);
//    }
//    return (in_code);
//}

/*
static int
emitIvalLabelROM(struct dbuf_s *oBuf, symbol *sym)
{
    //char *segname;
    static int in_code = 0;
    static int sectionNr = 0;

    if (sym) {
        // code or data space?
        dbuf_printf(oBuf, "\n.area IDATAROM (CODE,REL,CON) ; ID_%s_%d", moduleName, sectionNr++);
        if (SPEC_ABSA(getSpec(sym->type))) {
            // specify address for absolute symbols
            dbuf_printf(oBuf, "\t0x%04X", SPEC_ADDR(getSpec(sym->type)));
        } // if
        dbuf_printf(oBuf, "\n%s_rom:\n", sym->rname);

        addSet(&emitted, sym->rname);
    }
    return 0;
}
*/

/*
 * Actually emit the initial values in .asm format.
 */
//static void
//emitIvals(struct dbuf_s *oBuf, symbol *sym, initList *list, long lit, int size)
//{
//    int i;
//    ast *node;
//    operand *op;
//    value *val = NULL;
//    int inCodeSpace = 0;
//    char *str = NULL;
//    int in_code;
//
//    assert (size <= sizeof(long));
//    assert (!list || (list->type == INIT_NODE));
//    node = list ? list->init.node : NULL;
//
//    in_code = emitIvalLabel(oBuf, sym);
//	if (!in_code)
//	{
//		//dbuf_printf(oBuf, "\tdb\t");
//		dbuf_printf(oBuf, "\t.ds\t%d\n", (unsigned int)getSize(sym->type) & 0xffff);
//		emitIvalLabelROM(oBuf, sym);
//		dbuf_printf(oBuf, "\t.db\t");
//	}
//    if (!node) {
//        for (i = 0; i < size; i++) {
//            if (in_code) {
//                dbuf_printf (oBuf, "\tretlw 0x%02x\n", (int)(lit & 0xff));
//                // dbuf_printf (oBuf, "\tretlw 0x00\n"); // conflict from merge of sf-patch-2991122 ?
//            } else {
//                dbuf_printf (oBuf, "%s0x%02x", (i == 0) ? "" : ", ", (int)(lit & 0xff));
//            }
//            lit >>= 8;
//        } // for
//        if (!in_code)
//            dbuf_printf (oBuf, "\n");
//        return;
//    } // if
//
//    op = NULL;
//    if (constExprTree(node) && (val = constExprValue(node, 0))) {
//        op = operandFromValue(val);
//        DEBUGprintf ("%s: constExpr ", __FUNCTION__);
//    } else if (IS_AST_VALUE(node)) {
//        op = operandFromAst(node, 0);
//    } else if (IS_AST_OP(node)) {
//        str = parseIvalAst(node, &inCodeSpace);
//        DEBUGprintf("%s: AST_OP: %s\n", __FUNCTION__, str);
//        op = NULL;
//    } else {
//        assert ( !"Unhandled construct in intializer." );
//    }
//
//    if (op) {
//        aopOp(op, NULL, 1);
//        assert(AOP(op));
//        //printOperand(op, of);
//    }
//
//    for (i = 0; i < size; i++) {
//        char *text;
//
//        /*
//         * FIXME: This is hacky and needs some more thought.
//         */
//        if (op && IS_SYMOP(op) && IS_FUNC(OP_SYM_TYPE(op))) {
//            /* This branch is introduced to fix #1427663. */
//            PCOI(AOP(op)->aopu.pcop)->offset+=i;
//            text = get_op(AOP(op)->aopu.pcop, NULL, 0);
//            PCOI(AOP(op)->aopu.pcop)->offset-=i;
//        } else {
//            text = op ? aopGet(AOP(op), i, 0, 0)
//                : get_op(newpCodeOpImmd(str, i, 0, inCodeSpace, 0), NULL, 0);
//        } // if
//        if (in_code) {
//            dbuf_printf (oBuf, "\tretlw %s\n", text);
//        } else {
//            dbuf_printf (oBuf, "%s%s", (i == 0) ? "" : ", ", text);
//        }
//    } // for
//    if (!in_code)
//        dbuf_printf (oBuf, "\n");
//}
//

/*
static void
emitIvals(struct dbuf_s *oBuf, symbol *sym, initList *list, long lit, int size, int mode)
{
    int i;
    ast *node;
    operand *op;
    value *val = NULL;
    int inCodeSpace = 0;
    char *str = NULL;
    int in_code;

    assert(size <= sizeof(long));
    assert(!list || (list->type == INIT_NODE));
    node = list ? list->init.node : NULL;

    in_code = emitIvalLabel(oBuf, sym, 1);
    if (!in_code)
        dbuf_printf(oBuf, "\t.db\t");

    if (!node) {
        for (i = 0; i < size; i++) {
            if (in_code) {
                dbuf_printf(oBuf, "\t.db 0x%02x\n", (int)(lit & 0xff));
                //           // dbuf_printf (oBuf, "\tretlw 0x00\n"); // conflict from merge of sf-patch-2991122 ?
            }
            else {
                dbuf_printf(oBuf, "%s0x%02x", (i == 0) ? "" : ", ", (int)(lit & 0xff));
            }
            lit >>= 8;
        } // for
        if (!in_code)
        {
            dbuf_printf(ivalBuf2, "\n");
            emitIvalLabel(ivalBuf2, sym, 0); // again, real address
            dbuf_printf(ivalBuf2, "\t.ds\t%d\n", size);

        }
        return;
    } // if

    // if ! code, we make a shadow copy

    op = NULL;
    if (constExprTree(node) && (val = constExprValue(node, 0))) {
        op = operandFromValue(val);
        DEBUGprintf("%s: constExpr ", __FUNCTION__);
    }
    else if (IS_AST_VALUE(node)) {
        op = operandFromAst(node, 0);
    }
    else if (IS_AST_OP(node)) {
        str = parseIvalAst(node, &inCodeSpace);
        DEBUGprintf("%s: AST_OP: %s\n", __FUNCTION__, str);
        op = NULL;
    }
    else {
        assert(!"Unhandled construct in intializer.");
    }

    if (op) {
        aopOp(op, NULL, 1);
        assert(AOP(op));
        //printOperand(op, of);
    }

    for (i = 0; i < size; i++) {
        char *text;

        if (op && IS_SYMOP(op) && IS_FUNC(OP_SYM_TYPE(op))) {
            PCOI(AOP(op)->aopu.pcop)->offset += i;
            text = get_op(AOP(op)->aopu.pcop, NULL, 0);
            PCOI(AOP(op)->aopu.pcop)->offset -= i;
        }
        else {
            text = op ? aopGet(AOP(op), i, 0, 0)
                   : get_op(newpCodeOpImmd(str, i, 0, inCodeSpace,0, 0,1,0), NULL, 0);
        } // if

        if (in_code) {
            dbuf_printf(oBuf, "\t.db %s\n", text);
        }
        else {
            dbuf_printf(oBuf, "%s%s", (i == 0) ? "" : ", ", text);
        }
    } // for
    //if (!in_code)
    dbuf_printf(oBuf, "\n");
    if (!in_code && !mode)
    {
        emitIvalLabel(ivalBuf2, sym, 0); // again, real address
        dbuf_printf(ivalBuf2, "\t.ds\t%d\n", size);
    }
}
*/

/*
 * For UNIONs, we first have to find the correct alternative to map the
 * initializer to. This function maps the structure of the initializer to
 * the UNION members recursively.
 * Returns the type of the first `fitting' member.
 */
/*
static sym_link *
matchIvalToUnion (initList *list, sym_link *type, int size)
{
    symbol *sym;

    assert (type);

    if (IS_PTR(type) || IS_CHAR(type) || IS_INT(type) || IS_LONG(type)
            || IS_FLOAT(type))
    {
        if (!list || (list->type == INIT_NODE)) {
            DEBUGprintf ("OK, simple type\n");
            return (type);
        } else {
            DEBUGprintf ("ERROR, simple type\n");
            return (NULL);
        }
    } else if (IS_BITFIELD(type)) {
        if (!list || (list->type == INIT_NODE)) {
            DEBUGprintf ("OK, bitfield\n");
            return (type);
        } else {
            DEBUGprintf ("ERROR, bitfield\n");
            return (NULL);
        }
    } else if (IS_STRUCT(type) && SPEC_STRUCT(getSpec(type))->type == STRUCT) {
        if (!list || (list->type == INIT_DEEP)) {
            if (list) list = list->init.deep;
            sym = SPEC_STRUCT(type)->fields;
            while (sym) {
                DEBUGprintf ("Checking STRUCT member %s\n", sym->name);
                if (!matchIvalToUnion(list, sym->type, 0)) {
                    DEBUGprintf ("ERROR, STRUCT member %s\n", sym->name);
                    return (NULL);
                }
                if (list) list = list->next;
                sym = sym->next;
            } // while

            // excess initializers?
            if (list) {
                DEBUGprintf ("ERROR, excess initializers\n");
                return (NULL);
            }

            DEBUGprintf ("OK, struct\n");
            return (type);
        }
        return (NULL);
    } else if (IS_STRUCT(type) && SPEC_STRUCT(getSpec(type))->type == UNION) {
        if (!list || (list->type == INIT_DEEP)) {
            if (list) list = list->init.deep;
            sym = SPEC_STRUCT(type)->fields;
            while (sym) {
                while (list && list->type == INIT_HOLE) {
                    list = list->next;
                    sym = sym->next;
                }
                DEBUGprintf ("Checking UNION member %s.\n", sym->name);
                if (((IS_STRUCT(sym->type) || getSize(sym->type) == size))
                        && matchIvalToUnion(list, sym->type, size))
                {
                    DEBUGprintf ("Matched UNION member %s.\n", sym->name);
                    return (sym->type);
                }
                sym = sym->next;
            } // while
        } // if
        // no match found
        DEBUGprintf ("ERROR, no match found.\n");
        return (NULL);
    } else {
        assert ( !"Unhandled type in UNION." );
    }

    assert ( !"No match found in UNION for the given initializer structure." );
    return (NULL);
}
*/

/*
 * Parse the type and its initializer and emit it (recursively).
 */
extern PORT hy08a_port;
extern int
printIval(symbol * sym, sym_link * type, initList * ilist, struct dbuf_s *oBuf, bool check);

extern char *str_need_ini; // chingson add 2017
static int
emitInitVal(struct dbuf_s *oBuf, symbol *topsym, sym_link *my_type, initList *list)
{
    //emitIvalLabel(oBuf,topsym,1);
    unsigned char *str = NULL;
	//int needCopy;
	int initSize = 0;
	static set * printname = NULL;

    //dump to node

	// special case
	if (!strncmp(topsym->name, "__str_", 6))
	{
		int id = -1;
		sscanf(topsym->name, "__str_%d", &id);
		if (!topsym->used && (id < 0 || (str_need_ini == NULL || str_need_ini[id] == 0)))
			return 0;
	}


    if (IS_ARRAY(my_type) && topsym && topsym->isstrlit  ) {
        str = (unsigned char *)SPEC_CVAL(topsym->etype).v_char;
		char * namep;
		int found = 0;
		for (namep = setFirstItem(printname); namep; namep = setNextItem(printname))
		{
			if (!strcmp(topsym->name, namep))
			{
				found = 1;
				break;
			}
			
		}
		if (!found)
		{
			addSet(&printname, topsym->name);

			emitIvalLabel(oBuf, topsym, 1);
			int num = DCL_ELEM(topsym->type); // ... dame torture test 
			int outn = 0;
			do {
				dbuf_printf(oBuf, "\t.db 0x%02x ; '%c'\n", str[0], (str[0] >= 0x20 && str[0] < 128) ? str[0] : '.');
				str++;
				initSize++;
			} while (++outn < num);
		}
        return initSize;
    }


   
    emitIvalLabel(oBuf, topsym, 1);
    //hy08a_port.little_endian = true;
	// back 1 char? 
	
	//if (list->type==INIT_NODE && list->init.node && (list->init.node->type == EX_VALUE) && list->init.node->opval.val && list->init.node->opval.val->name 
	//	&& list->init.node->opval.val->name[0]) // this is special
	//{
	//	oBuf->len--;
	//	dbuf_printf(oBuf, "\t.EQU\t%s\n", list->init.node->opval.val->name);
	//	if (list->init.node->opval.val->sym)
	//		list->init.node->opval.val->sym->needCopy = needCopy;
	//}else
		// because some struct size is var, we need to get the size of initval
		initSize+=printIval(topsym, my_type, list, oBuf, TRUE);
		if (!IS_CODE(getSpec(topsym->type)))
		{
			emitIvalLabel(ivalBuf2, topsym, 0);
			//		if (SPEC_ABSA(getSpec(topsym->type)))
			//		{
			//			int a = SPEC_ADDR(getSpec(topsym->type));
			//			dbuf_printf(ivalBuf2, "\t.equ\t0xX\n",a);
			//		}
			//		else
			if (!(SPEC_ABSA(getSpec(topsym->type))))
				dbuf_printf(ivalBuf2, "\t.ds\t%d\n", initSize);// this is the size!!
			//dbuf_printf(ivalBuf2, "\t.ds\t%d\n", getSize());
		}
		return initSize;
}

//static void
//emitInitVal(struct dbuf_s *oBuf, symbol *topsym, sym_link *my_type, initList *list)
//{
//    symbol *sym;
//    int size;
//    long lit;
//    unsigned char *str;
//
//    /* Handle designated initializers */
//    if (list)
//      list = reorderIlist (my_type, list);
//
//    /* If this is a hole, substitute an appropriate initializer. */
//    if (list && list->type == INIT_HOLE)
//      {
//        if (IS_AGGREGATE (my_type))
//          {
//            list = newiList(INIT_DEEP, NULL); /* init w/ {} */
//          }
//        else
//          {
//            ast *ast = newAst_VALUE (constVal("0"));
//            ast = decorateType (ast, RESULT_TYPE_NONE);
//            list = newiList(INIT_NODE, ast);
//          }
//      }
//
//    size = getSize(my_type);
//
//    if (IS_PTR(my_type)) {
//        DEBUGprintf ("(pointer, %d byte) 0x%x\n", size, list ? (unsigned int)list2int(list) : 0);
//        emitIvals(oBuf, topsym, list, 0, size);
//        return;
//    }
//
//    if (IS_ARRAY(my_type) && topsym && topsym->isstrlit) {
//        str = (unsigned char *)SPEC_CVAL(topsym->etype).v_char;
//        emitIvalLabel(oBuf, topsym);
//        do {
//            dbuf_printf (oBuf, "\tretlw 0x%02x ; '%c'\n", str[0], (str[0] >= 0x20 && str[0] < 128) ? str[0] : '.');
//        } while (*(str++));
//        return;
//    }
//
//    if (IS_ARRAY(my_type) && list && list->type == INIT_NODE) {
//        fprintf (stderr, "Unhandled initialized symbol: %s\n", topsym->name);
//        assert ( !"Initialized char-arrays are not yet supported, assign at runtime instead." );
//        return;
//    }
//
//    if (IS_ARRAY(my_type)) {
//        size_t i;
//
//        DEBUGprintf ("(array, %d items, %ud byte) below\n", (unsigned int) DCL_ELEM(my_type), size);
//        assert (!list || list->type == INIT_DEEP);
//        if (list) list = list->init.deep;
//        for (i = 0; i < DCL_ELEM(my_type); i++) {
//            emitInitVal(oBuf, topsym, my_type->next, list);
//            topsym = NULL;
//            if (list) list = list->next;
//        } // for i
//        return;
//    }
//
//    if (IS_FLOAT(my_type)) {
//        // float, 32 bit
//        DEBUGprintf ("(float, %d byte) %lf\n", size, list ? list2int(list) : 0.0);
//        emitIvals(oBuf, topsym, list, 0, size);
//        return;
//    }
//
//    if (IS_CHAR(my_type) || IS_INT(my_type) || IS_LONG(my_type)) {
//        // integral type, 8, 16, or 32 bit
//        DEBUGprintf ("(integral, %d byte) 0x%lx/%ld\n", size, list ? (long)list2int(list) : 0, list ? (long)list2int(list) : 0);
//        emitIvals(oBuf, topsym, list, 0, size);
//        return;
//
//    } else if (IS_STRUCT(my_type) && SPEC_STRUCT(my_type)->type == STRUCT) {
//        // struct
//        DEBUGprintf ("(struct, %d byte) handled below\n", size);
//        assert (!list || (list->type == INIT_DEEP));
//
//        // iterate over struct members and initList
//        if (list) list = list->init.deep;
//        sym = SPEC_STRUCT(my_type)->fields;
//        while (sym) {
//            long bitfield = 0;
//            int len = 0;
//            if (IS_BITFIELD(sym->type)) {
//                while (sym && IS_BITFIELD(sym->type)) {
//                    int bitoff = SPEC_BSTR(getSpec(sym->type)) + 8 * sym->offset;
//                    assert (!list || ((list->type == INIT_NODE)
//                                && IS_AST_LIT_VALUE(list->init.node)));
//                    lit = (long) (list ? list2int(list) : 0);
//                    DEBUGprintf ( "(bitfield member) %02lx (%d bit, starting at %d, bitfield %02lx)\n",
//                            lit, SPEC_BLEN(getSpec(sym->type)),
//                            bitoff, bitfield);
//                    bitfield |= (lit & ((1ul << SPEC_BLEN(getSpec(sym->type))) - 1)) << bitoff;
//                    len += SPEC_BLEN(getSpec(sym->type));
//
//                    sym = sym->next;
//                    if (list) list = list->next;
//                } // while
//                assert (len < sizeof (long) * 8); // did we overflow our initializer?!?
//                len = (len + 7) & ~0x07; // round up to full bytes
//                emitIvals(oBuf, topsym, NULL, bitfield, len / 8);
//                topsym = NULL;
//            } // if
//
//            if (sym) {
//                emitInitVal(oBuf, topsym, sym->type, list);
//                topsym = NULL;
//                sym = sym->next;
//                if (list) list = list->next;
//            } // if
//        } // while
//        if (list) {
//            assert ( !"Excess initializers." );
//        } // if
//        return;
//
//    } else if (IS_STRUCT(my_type) && SPEC_STRUCT(my_type)->type == UNION) {
//        // union
//        DEBUGprintf ("(union, %d byte) handled below\n", size);
//        assert (list && list->type == INIT_DEEP);
//
//        // iterate over union members and initList, try to map number and type of fields and initializers
//        my_type = matchIvalToUnion(list, my_type, size);
//        if (my_type) {
//            emitInitVal(oBuf, topsym, my_type, list->init.deep);
//            topsym = NULL;
//            size -= getSize(my_type);
//            if (size > 0) {
//                // pad with (leading) zeros
//                emitIvals(oBuf, NULL, NULL, 0, size);
//            }
//            return;
//        } // if
//
//        assert ( !"No UNION member matches the initializer structure.");
//    } else if (IS_BITFIELD(my_type)) {
//        assert ( !"bitfields should only occur in structs..." );
//
//    } else {
//        printf ("SPEC_NOUN: %d\n", SPEC_NOUN(my_type));
//        assert( !"Unhandled initialized type.");
//    }
//}

/*
 * Emit a set of symbols.
 * type - 0: have symbol tell whether it is local, extern or global
 *        1: assume all symbols in set to be global
 *        2: assume all symbols in set to be extern
 */

extern int
cdbWriteBasicSymbol(symbol *sym, int isStructSym, int isFunc, int lev, int version)
;
static void dumpIvalDeep(symbol *sym, struct sym_link *type, struct initList *list);
static void
emitSymbolSet(set *s, int type, FILE *fp)
{
    symbol *sym;
    initList *list;
    //unsigned sectionNr = 0;
	// 2024 MAY, stop emit same global record

    for (sym = setFirstItem(s); sym; sym = setNextItem(s)) {
#if 0
        fprintf(stdout, ";    name %s, rname %s, level %d, block %d, key %d, local %d, ival %p, static %d, cdef %d, used %d\n",
                sym->name, sym->rname, sym->level, sym->block, sym->key, sym->islocal, sym->ival, IS_STATIC(sym->etype), sym->cdef, sym->used);
#endif
        //if (!strcmp(sym->name, "Temp_Offset_Fun"))
        //{
        //	fprintf(stderr, "tempOffsetFun found.\n");
        //}
        if (sym->etype != NULL)
        {
            fprintf(fp, "\t;--cdb--");
            cdbWriteSymbol(sym);
			if (sym->islocal && sym->reqv)
			{
				fprintf(fp, "\t;--c1db--");
				cdbWriteBasicSymbol(sym,0,0,sym->level,1); // version 1.0 record
			}
            //fprintf(fp, "\n");
        }
        if (sym->etype && SPEC_ABSA(sym->etype)
                && IS_CONFIG_ADDRESS(SPEC_ADDR(sym->etype))
                && sym->ival && type <3)
        {
            // handle config words
            HY08A_assignConfigWordValue(SPEC_ADDR(sym->etype),
                                        (int)list2int(sym->ival));
            HY08A_stringInSet(sym->rname, &emitted, 1);
            continue;
        }


        if (sym->isstrlit  && type<3 && !isinSet(emitted, sym)) { //?
            // special case: string literals
            emitInitVal(ivalBuf, sym, sym->type, sym->ival);
            continue;
        }
        if(!sym || sym->rname[0]=='\0')
            continue; // empty no process
        if ((type != 0 && type<3) || sym->cdef
                || (!IS_STATIC(sym->etype)
                    && IS_GLOBAL(sym))  || IS_STRUCT(sym->etype) 
			|| (IS_STATIC(sym->etype) && !sym->ival) // special var no init but is address referenced, bug3188899.c
			) // special case static 
        {
            // bail out for ___fsadd and friends
            //if (sym->cdef && !sym->used) continue;

            //if (debugFile)
            //debugFile->writeSymbol(sym);// try to add in debugfile

            if (IS_FUNC(sym->type) && !sym->used) continue;
            /* export or import non-static globals */
            if ( !HY08A_stringInSet(sym->rname, &emitted, 0)) {

                //if(strstr(sym->rname,"_bar2_x"))
                //    fprintf(stderr,"bar2 x\n");
                if (!IS_ABSOLUTE(sym->etype) && (type == 2 || IS_EXTERN(sym->etype) || sym->cdef))
                {
                    /* do not add to emitted set, it might occur again! */
                    //if (!sym->used) continue;
                    // declare symbol
                    emitIfNew(extBuf, &emitted, "\t.globl\t%s\n", sym->rname); // for  extern same globl
                }
                else {
                    // declare symbol
					if(!IS_STATIC(sym->etype) && !sym->islocal && !IS_ABSOLUTE(sym->etype) && !sym->_isparm) // abs not global!!
						emitIfNew(gloBuf, &emitted, "\t.globl\t%s\n", sym->rname);
                    if (!sym->ival && !IS_FUNC(sym->type)) {
                        // also define symbol
                        if (IS_ABSOLUTE(sym->etype)) {
                            // absolute location?
                           // use EQU to define it
							if (IN_XSPACE(SPEC_OCLS(sym->etype)))
								dbuf_printf(gloDefBuf, "\t.area XSEG(XDATA)\n");
							else
								dbuf_printf(gloDefBuf, "\t.area DSEG(DATA)\n");
							int a = SPEC_ADDR(getSpec(sym->type));
							dbuf_printf(gloDefBuf, "%s\t.equ\t0x%X\n\n", sym->rname, a );
							if (mainf && strstr(sym->rname, "SWTGTID"))
							{
								// we check if match with part assigned
								char *pn = HY08A_getPART()->name;
								int compilerID = 0;
								if (isdigit(pn[4])) // hy4145...
								{
									compilerID = atoi(pn + 2); // HY...
								}
								else if (strlen(pn) == 9) // HY11P414M
								{
									compilerID = (pn[2] - '0') * 10000000 + (pn[3] - '0') * 1000000 +
										(pn[5] - '0') * 10000 + (pn[6] - '0')*1000+(pn[7]-'0')*100 +
										(isdigit(pn[8])?(pn[8]-'0'):(toupper(pn[8])-'A'+10))
										;
									if (toupper(pn[4]) == 'S')
									{
										compilerID += 100000;
									}
								}
								else if (strlen(pn) == 10) // HY11P4104M
								{
									compilerID = (pn[2] - '0') * 100000000 + (pn[3] - '0') * 10000000 +
										(pn[5] - '0') * 100000 + (pn[6] - '0') * 10000 + (pn[7] - '0') * 1000 +
										(pn[8] - '0') * 100 +
										(isdigit(pn[9]) ? (pn[9] - '0') : (toupper(pn[9]) - 'A' + 10))
										;
									if (toupper(pn[4]) == 'S')
									{
										compilerID += 1000000;
									}
								}
								else
								{
									compilerID = (pn[2] - '0') * 10000 + (pn[3] - '0') * 1000 +
										(pn[5] - '0') * 10 + (pn[6] - '0');
									if (toupper(pn[4]) == 'S')
									{
										compilerID += 100;
									}
								}
								if (compilerID != a && compilerID != a+100 && compilerID+100!=a)
								{
									// try HASH
									if((CityHash64(pn,strlen(pn))&0xffffffff)!=((unsigned int)a))
                                    {
                                        // 2022 change to error if id mismatch with main defined!!
										fprintf(stderr, "Error, include SFR header file is different from %s.\n", pn);
                                        exit ((-__LINE__)|5);

                                    }
								}
							}
                            // deferred to _constructAbsMap
                        }
                        else if (IN_XSPACE(SPEC_OCLS(sym->etype)) ) // if not abs, we use ds
                        {
                            dbuf_printf(gloDefBuf, "\t.area XSEG(XDATA)\n");
                            dbuf_printf(gloDefBuf, "%s:\t.ds\t%d\n\n", sym->rname, getSize(sym->type));
                        }
                        else
                        {
                            //dbuf_printf (gloDefBuf, "\t.area UD_%s_%u\t(data)\n", moduleName, sectionNr++);
							// ok , we try support bit var after this version
							if (!IS_BIT(sym->type)  && !(sym->_isparm && strstr(sym->rname,"_STK"))) // bug-2756.c
							{
								dbuf_printf(gloDefBuf, "\t.area DSEG(DATA)\n");
								dbuf_printf(gloDefBuf, "%s:\t.ds\t%d\n\n", sym->rname, getSize(sym->type));
							}
                        }
                    } // if
                } // if
                HY08A_stringInSet(sym->rname, &emitted, 1);
            } // if
        } // if
        list = sym->ival;
        //if (list) showInitList(list, 0);
        if ((list && type < 3) || (list && type ==3 && SPEC_STAT(sym->type)
                                   && sym->islocal)) {
			// bit init value feature added at 2017 MARCH!!
			if (sym->ival && IS_BIT((sym->type)))
			{
				//fprintf(stderr, "Warning!!, bool type symbol %s initial value is experimental!!\n", sym->name);
				continue;
			}
            dumpIvalDeep(sym, sym->type, sym->ival);
            dbuf_printf(ivalBuf, "\n");
            dbuf_printf(ivalBuf2, "\n");
        }
    } // for sym
}
static void dumpIvalDeep(symbol *sym, struct sym_link *type, struct initList *list)
{
    //struct initList *k;
    if (!list)
        return;
    //if (list->type == INIT_DEEP)
    //{
    //	for (k = list->init.deep; k; k = k->next)
    //	{
    //		dumpIvalDeep(sym, type->next, k);
    //	}
    //}
    resolveIvalSym(list, type);

    emitInitVal(ivalBuf, sym, type, list);

}
//static void
//emitSymbolSet(set *s, int type)
//{
//    symbol *sym;
//    initList *list;
//    unsigned sectionNr = 0;
//
//    for (sym = setFirstItem(s); sym; sym = setNextItem(s)) {
//#if 0
//        fprintf (stdout, ";    name %s, rname %s, level %d, block %d, key %d, local %d, ival %p, static %d, cdef %d, used %d\n",
//                sym->name, sym->rname, sym->level, sym->block, sym->key, sym->islocal, sym->ival, IS_STATIC(sym->etype), sym->cdef, sym->used);
//#endif
//
//        if (sym->etype && SPEC_ABSA(sym->etype)
//                && IS_CONFIG_ADDRESS(SPEC_ADDR(sym->etype))
//                && sym->ival)
//        {
//            // handle config words
//            HY08A_assignConfigWordValue(SPEC_ADDR(sym->etype),
//                    (int)list2int(sym->ival));
//            HY08A_stringInSet(sym->rname, &emitted, 1);
//            continue;
//        }
//
//        if (sym->isstrlit) {
//            // special case: string literals
//            emitInitVal(ivalBuf, sym, sym->type, NULL);
//            continue;
//        }
//
//        if (type != 0 || sym->cdef
//                || (!IS_STATIC(sym->etype)
//                    && IS_GLOBAL(sym)))
//        {
//            // bail out for ___fsadd and friends
//            if (sym->cdef && !sym->used) continue;
//
//            /* export or import non-static globals */
//            if (!HY08A_stringInSet(sym->rname, &emitted, 0)) {
//
//                if (type == 2 || IS_EXTERN(sym->etype) || sym->cdef)
//                {
//                    /* do not add to emitted set, it might occur again! */
//                    //if (!sym->used) continue;
//                    // declare symbol
//                    emitIfNew (extBuf, &emitted, "\t.globl\t%s\n", sym->rname);
//                } else {
//                    // declare symbol
//                    emitIfNew (gloBuf, &emitted, "\t.globl\t%s\n", sym->rname);
//                    if (!sym->ival && !IS_FUNC(sym->type)) {
//                        // also define symbol
//                        if (IS_ABSOLUTE(sym->etype)) {
//                            // absolute location?
//                            //dbuf_printf (gloDefBuf, "UD_%s_%u\tudata\t0x%04X\n", moduleName, sectionNr++, SPEC_ADDR(sym->etype));
//                            // deferred to HY08A_constructAbsMap
//                        } else {
//							dbuf_printf(gloDefBuf, "\t.area UDATA (DATA,REL,CON) ; UD_%s_%u\n", moduleName, sectionNr++);
//                            dbuf_printf (gloDefBuf, "%s:\t.ds\t%d\n\n", sym->rname, getSize(sym->type));
//                        }
//                    } // if
//                } // if
//                HY08A_stringInSet(sym->rname, &emitted, 1);
//            } // if
//        } // if
//        list = sym->ival;
//        //if (list) showInitList(list, 0);
//        if (list) {
//            resolveIvalSym( list, sym->type );
//            emitInitVal(ivalBuf, sym, sym->type, sym->ival);
//            dbuf_printf (ivalBuf, "\n");
//        }
//    } // for sym
//}


// change to SFR style, use EQU !!
static int addExternIfReq(char*regname, int addr)
{
    symbol *i;
	pBlock *pb;
	bool needAdd = false;
	pCode *pc;
	uint64 hashid;
    for (i = setFirstItem(publics); i; i = setNextItem(publics))
    {
		
        if (hash(i->name)==hash(regname))
            return 0;
    }
    for (i = setFirstItem(externs); i; i = setNextItem(externs))
    {
		if (hash(i->name) == hash(regname))
            return 0;
    }
	hashid = hash(regname);
    // we add to externs .. only if necessary?
	// we check if code have reference it
	if (!the_pFile)
		return 0;
	for (pb = the_pFile->pbHead; pb; pb = pb->next) {
		for (pc = pb->pcHead; pc; pc = pc->next)
		{
			if (!isPCI(pc))
				continue;
			if (!PCI(pc)->num_ops)
				continue; // it is still possible op is null!!

			if (PCI(pc)->isBitInst)
			{
				if (PCI(pc)->pcop &&  PCORB(PCI(pc)->pcop)->pcor.r && PCORB(PCI(pc)->pcop)->pcor.r->name
					&& hash(PCORB(PCI(pc)->pcop)->pcor.r->name) == hashid)
				{
					needAdd = true;
					break;
				}
			}
            if (PCI(pc)->pcop)
            {
			    if (PCI(pc)->pcop->name)
			    {
				    if (!PCI(pc)->pcop->name_hash)
					    PCI(pc)->pcop->name_hash = hash(PCI(pc)->pcop->name);
				    if (PCI(pc)->pcop->name_hash == hashid)
				    {
					    needAdd = true;
					    break;
				    }
                }else
                {
                    if(PCI(pc)->pcop->type == PO_PRODL)
                        PCI(pc)->pcop=PCOP(&pc_prodl); // replace directly
                    if(PCI(pc)->pcop->type == PO_PRODH)
                        PCI(pc)->pcop=PCOP(&pc_prodh); // replace directly
                    if(PCI(pc)->pcop->name)
                    {
				        if (!PCI(pc)->pcop->name_hash)
					        PCI(pc)->pcop->name_hash = hash(PCI(pc)->pcop->name);
				        if (PCI(pc)->pcop->name_hash == hashid)
				        {
					        needAdd = true;
					        break;
				        }
                    }
                    
                }
			}
			if (PCI(pc)->pcop2 && PCI(pc)->pcop2->name)
			{
				if (!PCI(pc)->pcop2->name_hash)
					PCI(pc)->pcop2->name_hash = hash(PCI(pc)->pcop2->name);
				if (PCI(pc)->pcop2->name_hash == hashid)
				{
					needAdd = true;
					break;
				}
			}
		}
	}
	if (!needAdd)
		return 0;
	// see if defined already..
    i = newSymbol(regname, 0);

	i->etype=i->type = newLink(SPECIFIER);
	if (addr >= 0)
	{
		i->type->select.s.b_unsigned = 1;
		i->type->select.s.b_absadr = 1;
		i->type->select.s._addr = addr;
	}
	else
	{
		i->type->select.s.b_extern = 1;
		
	}
    strcpy(i->rname, i->name);
    addSet(&externs, i);
    return 1;
}
//
//static int addImplicitFuncIfReq(void)
//{
//	pBlock *pb;
//	pCode *pc;
//	int implicitn = 0;
//	if (!the_pFile)
//		return 0;
//	for (pb = the_pFile->pbHead; pb; pb = pb->next)
//	{
//		for (pc = setFirstItem(pb->function_calls); pc; pc = setNextItem(pb->function_calls))
//		{
//			if (pc->type == PC_OPCODE && (PCI(pc)->op == POC_CALL || PCI(pc)->op == POC_JMP) && PCI(pc)->pcop->type == PO_STR)
//			{
//				implicitn += addExternIfReq(PCI(pc)->pcop->name, -1);
//				
//			}
//		}
//		
//	}
//	return implicitn;
//
//
//}
//
static int addProcRegIfReq(void)
{

    addExternIfReq("_STATUS", 0x2B );
	addExternIfReq("_FSR2L", 0x14 );
	addExternIfReq("_FSR1L", 0x11);
	addExternIfReq("_FSR2H", 0x13);
	addExternIfReq("_PRINC2", 0x0d);
	addExternIfReq("_INDF2", 0x0a);
	addExternIfReq("_PLUSW2", 0x0e);

	addExternIfReq("_PODEC2", 0x0c);
	addExternIfReq("_PODEC0", 0x02);
    addExternIfReq("_FSR0L", 0x10);

    addExternIfReq("_FSR0H", 0x0f);
    addExternIfReq("_INDF0", 0x00);
    addExternIfReq("_PRINC0", 0x03);
    addExternIfReq("_POINC0", 0x01);
	
    addExternIfReq("_PLUSW0", 0x04);
    addExternIfReq("_PRODL", 0x22);
    addExternIfReq("_PRODH", 0x21);
    addExternIfReq("_TBLPTRL", 0x1E);
    addExternIfReq("_TBLPTRH", 0x1D);
    addExternIfReq("_TBLDL", 0x20);
    addExternIfReq("_TBLDH", 0x1F);
    //addExternIfReq("_INTE1", 0x24);
	addExternIfReq("_PCLATU", 0x19);
	addExternIfReq("_PCLATH", 0x1A); //PCLATU should not be necessary.
	addExternIfReq("_PCLATL", 0x1B); //PCLATU should not be necessary.
    addExternIfReq("_WREG", 0x29);
	addExternIfReq("_PRINC2", 0x0d);
	addExternIfReq("_INTE0", 0x23);

    return 0;

}
/*
 * Iterate over all memmaps and emit their contents (attributes, symbols).
 */
extern set  *operKeyReset;
static void
showAllMemmaps(FILE *of)
{
    struct dbuf_s locBuf;
    memmap *maps[] = {
        xstack, istack, code, data, pdata, xdata, xidata, xinit,
        idata, bit, statsg, c_abs, x_abs, i_abs, d_abs,
        sfr, sfrbit, reg, generic, overlay, eeprom, home
    };
    memmap * map;
    int i;

    DEBUGprintf ("---begin memmaps---\n");
    if (!extBuf) extBuf = dbuf_new(1024);
    if (!gloBuf) gloBuf = dbuf_new(1024);
    if (!gloDefBuf) gloDefBuf = dbuf_new(1024);
    if (!ivalBuf) ivalBuf = dbuf_new(1024);
    if (!ivalBuf2) ivalBuf2 = dbuf_new(1024);
    dbuf_init(&locBuf, 1024);

    dbuf_printf (extBuf, "%s; external declarations\n%s", iComments2, iComments2);
    dbuf_printf (gloBuf, "%s; global -1 declarations\n%s", iComments2, iComments2);
    dbuf_printf (gloDefBuf, "%s; global -2 definitions\n%s", iComments2, iComments2);
    dbuf_printf (ivalBuf, "%s; initialized data\n%s", iComments2, iComments2);
    dbuf_printf(ivalBuf2, "%s; initialized data - mirror\n%s", iComments2, iComments2);
    dbuf_printf (&locBuf, "%s; compiler-defined variables\n%s", iComments2, iComments2);

    for (i = 0; i < sizeof(maps) / sizeof (memmap *); i++) {
        map = maps[i];
        //DEBUGprintf ("memmap %i: %p\n", i, map);
        //if (i == 10)
        //fprintf(stderr, "statsg\n");
        if (map) {
#if 0
            fprintf (stdout, ";  pageno %c, sname %s, dbName %c, ptrType %d, slbl %d, sloc %u, fmap %u, paged %u, direct %u, bitsp %u, codesp %u, regsp %u, syms %p\n",
                     map->pageno, map->sname, map->dbName, map->ptrType, map->slbl,
                     map->sloc, map->fmap, map->paged, map->direct, map->bitsp,
                     map->codesp, map->regsp, map->syms);
#endif
            emitSymbolSet(map->syms, 0, of);
        } // if (map)
    } // for i
    DEBUGprintf ("---end of memmaps---\n");

    // see if set has define _WREG _STATUS _FSR0L _FSR0H ...

    //emitSymbolSet(operKeyReset, 3, of); // for local static only
    addProcRegIfReq();
	//addImplicitFuncIfReq(); // 2019, implicit call processed in assembler

    emitSymbolSet(publics, 1, of);

    emitSymbolSet(externs, 2, of);

	// for struct copy 
	

    emitPseudoStack(gloBuf, extBuf);
    HY08A_constructAbsMap(gloDefBuf, gloBuf);
    HY08AprintLocals (&locBuf);
	emitIfNew(gloBuf, &emitted, "\t.globl\t%s\n", "_memcpy");
    //HY08A_emitConfigWord(of); // must be done after all the rest

    dbuf_write_and_destroy(extBuf, of);
    dbuf_write_and_destroy(gloBuf, of);
    dbuf_write_and_destroy(gloDefBuf, of);
    dbuf_write_and_destroy(&locBuf, of);
    dbuf_write_and_destroy(ivalBuf, of);
    dbuf_write_and_destroy(ivalBuf2, of);
    extBuf = gloBuf = gloDefBuf = ivalBuf = NULL;
}
