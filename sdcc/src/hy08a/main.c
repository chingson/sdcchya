/** @file main.c
    HY08A specific general functions.

   2016/2017: modified by chingsonchen@hycontek.com.tw for HYCON 8-bit CPU
*/
#include "common.h"
#include "dbuf_string.h"
#include "SDCCmacro.h"

#include "device.h"
#include "gen.h"
#include "glue.h"
#include "main.h"
#include "pcode.h"
#include "ralloc.h"

/*
 * Imports
 */
extern set *dataDirsSet;
extern set *includeDirsSet;
extern set *libDirsSet;
extern set *libPathsSet;
extern set *linkOptionsSet;


HY08A_options_t HY08A_options;
int debug_verbose = 0;
int max_rom_addr = 0;


#define OPTION_STACK_SIZE         "--stack-size"

static char _defaultRules[] =
{
#include "peeph.rul"
};

static OPTION _HY08A_poptions[] =
{
    //{ 0, "--debug-xtra",   &debug_verbose, "show more debug info in assembly output" },
    //{ 0, "--no-pcode-opt", &HY08A_options.disable_df, "disable (slightly faulty) optimization on pCode" },
    //{ 0, OPTION_STACK_SIZE, &options.stack_size, "sets the size if the argument passing stack (default: 16, minimum: 4)", CLAT_INTEGER },
    //{ 0, "--no-extended-instructions", &HY08A_options.no_ext_instr, "forbid use of the extended instruction set (e.g., ADDFSR)" },
    //{ 0, "--no-warn-non-free", &HY08A_options.no_warn_non_free, "suppress warning on absent --use-non-free option" },
    { 0, "--mon", &HY08A_options.ob_count, "max optimization step # (for debugging optimzation)",CLAT_INTEGER },
    { 0, "--retmore", &HY08A_options.extraStkSize, "Extra (more than 8) bytes for function to return struct/union.",CLAT_INTEGER },
    //{ 0, "--nlklo", &(0), "--nlklo local var always occupy memory.",CLAT_BOOLEAN },
    { 0, "--fsrno", &HY08A_options.dis_fsr_opt, "--fsrno will disable FSR(0) optimization.",CLAT_BOOLEAN },
	{ 0, "--isl", &HY08A_options.inline_shift_long, "--isl will inline shift long operations instead of call func.",CLAT_BOOLEAN },
	{ 0, "--f08a", &HY08A_options.force_h08a, "--f08a: force H08A mode even target supports H08D.",CLAT_BOOLEAN },
	{ 0, "--dumppeep", &HY08A_options.dumpPeepInfo, "--dumppeep: dump peeprule match info in asm.",CLAT_BOOLEAN },
    { 0, "--keepIndu", &HY08A_options.keepLoopInduction, "--keepIndu: enable global loop-induction(default off).", CLAT_BOOLEAN},
	{ 0, "--keepLocal", &HY08A_options.keepLocal, "--keepLocal: No re-use local var space. (default off)", CLAT_BOOLEAN},
    //{ 0, "--keepGCSE", &HY08A_options.keepGCSE, "--keepGCSE: enable global GCSE(default off).", CLAT_BOOLEAN},
    { 0, NULL, NULL, NULL }
};

/* list of key words used by HY08A */
static char *_HY08A_keywords[] =
{
    "at",
    "bit",
    "code",
    "critical",
    "data",
    "far",
    //"idata",
    "interrupt",
    "naked",
    "near",
    //"pdata",
    "reentrant",
    "sfr",
    //"sbit",
    "using",
    "xdata",
    NULL
};

static int regParmFlg = 0;  /* determine if we can register a parameter */
static struct sym_link *regParmFuncType;

/** $1 is always the basename.
    $2 is always the output file.
    $3 varies
    $l is the list of extra options that should be there somewhere...
    MUST be terminated with a NULL.
*/
/*
static const char *_linkCmd[] =
{
  "gplink", "$l", "-w", "-r", "-o \"$2\"", "\"$1\"", "$3", NULL
};
*/

static const char *_asmCmd[] =
{
    "sdashya", "$l", "$3", "-o $2", "", "", "$1", NULL
};

static void
_HY08A_init (void)
{
    asm_addTree (&asm_asxxxx_mapping);
    memset (&HY08A_options, 0, sizeof (HY08A_options));
    //options.debug = 1; // this is default
}

static void
_HY08A_reset_regparm (struct sym_link *funcType)
{
    regParmFlg = 0;
    regParmFuncType = funcType;
}

static int
_HY08A_regparm (sym_link * l, bool reentrant)
{
    /* for this processor it is simple
      can pass only the first parameter in a register */
    //if (regParmFlg)
    //  return 0;
    if (IFFUNC_HASVARARGS (regParmFuncType))
    return 0;

    regParmFlg++;// = 1;
    return 1;
}

static bool
_HY08A_parseOptions (int *pargc, char **argv, int *i)
{
    /* TODO: allow port-specific command line options to specify
    * segment names here.
    */
    return FALSE;
}

static void // change back to void
_HY08A_finaliseOptions (void)
{
    struct dbuf_s dbuf;



	if (options.nopeep)
	{ // disable related optimizations
        // 2023 keep gcse because bug happen if it is disabled
		//optimize.global_cse = 0;
		optimize.loopInduction = 0;
		optimize.loopInvariant = 0;
		optimize.lospre = 0;
		optimize.noLoopReverse = 1; // "no"
		//optimize.label1 = 0;
		//optimize.label2 = 0;
		//optimize.label3 = 0;
		//optimize.label4 = 0;
		//optimize.ptrArithmetic = 1;
		
	}
    if(HY08A_options.keepLoopInduction)
    {
        optimize.loopInduction = 1;
    }else 
    {
        optimize.loopInduction = 0; // this option confuses us, not much use for small projects
    }
    // if(HY08A_options.keepGCSE)
    // {
    //     optimize.global_cse = 1;
    // }else 
    // {
    //     optimize.global_cse = 0; // this option confuses us, not much use for small projects
    // }
    if (!pCodeInitRegisters())
    {
        return ;
    }

    port->mem.default_local_map = data;
    port->mem.default_globl_map = data;

	

    dbuf_init (&dbuf, 512);
    dbuf_printf (&dbuf, "-D__SDCC_PROCESSOR=\"%s\"", port->processor);
    addSet (&preArgvSet, Safe_strdup (dbuf_detach_c_str (&dbuf)));

    {
        char *upperProc, *p1, *p2;
        int len;

        dbuf_set_length (&dbuf, 0);
        len = strlen (port->processor);
        upperProc = Safe_malloc (len);
        for (p1 = port->processor, p2 = upperProc; *p1; ++p1, ++p2)
        {
            *p2 = toupper (*p1);
        }
        dbuf_append (&dbuf, "-D__SDCC_HYA", sizeof ("-D__SDCC_HYA") - 1);
        dbuf_append (&dbuf, upperProc, len);
        addSet (&preArgvSet, dbuf_detach_c_str (&dbuf));
    }
    /*

    if (!HY08A_options.no_warn_non_free && !options.use_non_free)
    {
      fprintf(stderr,
              "WARNING: Command line option --use-non-free not present.\n"
              "         When compiling for HY08A, please provide --use-non-free\n"
              "         to get access to device headers and libraries.\n"
              "         If you do not use these, you may provide --no-warn-non-free\n"
              "         to suppress this warning (not recommended).\n");
    } // if
    */
    return ;
}

static void
_HY08A_setDefaultOptions (void)
{
    HY08A_options.ob_count = 65536;
}

static const char *
_HY08A_getRegName (const struct reg_info *reg)
{
    if (reg)
        return reg->name;
    return "err";
}

static void
_HY08A_genAssemblerPreamble (FILE * of)
{
    char * name = processor_base_name();

    if(!name) {

        name = "HY11P13";
        fprintf(stderr,"WARNING: No HYCON CPU has been selected, defaulting to %s\n",name);
    }

    fprintf(of, "\t.module %s\n", moduleName);
    fprintf(of, "\t;.list\tp=%s\n", name);
    //fprintf(of, "\t;.radix dec\n");
    //fprintf(of, "\t.include \"hy11s14.inc\"\n");
}

/* Generate interrupt vector table. */
static int
_HY08A_genIVT (struct dbuf_s * oBuf, symbol ** interrupts, int maxInterrupts)
{
    /* Let the default code handle it. */
    return FALSE;
}

static bool
_hasNativeMulFor (iCode *ic, sym_link *left, sym_link *right)
{
    if ( ic->op != '*')
    {
        return FALSE;
    }

    /* multiply chars in-place */
    if (getSize(left) == 1 && getSize(right) == 1)
        return TRUE;

    /* use library functions for more complex maths */
    return FALSE;
}

/* Indicate which extended bit operations this port supports */
static bool
hasExtBitOp (int op, sym_link *left, int right)
{
    unsigned int lbits = bitsForType (left);
  return ((op == ROT && (right == 1 || right==(lbits-1))) ||
    op == GETABIT);
}

/* Indicate the expense of an access to an output storage class */
static int
oclsExpense (struct memmap *oclass)
{
    /* The IN_FARSPACE test is compatible with historical behaviour, */
    /* but I don't think it is applicable to the chip. If so, please feel */
    /* free to remove this test -- EEP */
    if (IN_FARSPACE(oclass))
        return 1;

    return 0;
}

static void
_HY08A_do_link (void)
{
    /*
     * link command format:
     * {linker} {incdirs} {lflags} -o {outfile} {spec_ofiles} {ofiles} {libs}
     *
     */
#define LFRM  "{linker} {incdirs} {sysincdirs} {lflags} -u -m -i -y {outfile} {user_ofile} {spec_ofiles} {ofiles} {libs}"
    hTab *linkValues = NULL;
    char *lcmd;
    set *tSet = NULL;
    int ret;
    char * procName;

    shash_add (&linkValues, "linker", "sdld");

    /* LIBRARY SEARCH DIRS */
    mergeSets (&tSet, libPathsSet);
    mergeSets (&tSet, libDirsSet);
    shash_add (&linkValues, "incdirs", joinStrSet (processStrSet (tSet, "-k ", NULL, shell_escape)));

    joinStrSet (processStrSet (libDirsSet, "-I", NULL, shell_escape));
    shash_add (&linkValues, "sysincdirs", joinStrSet (processStrSet (libDirsSet, "-k ", NULL, shell_escape)));

    shash_add (&linkValues, "lflags", joinStrSet (linkOptionsSet));

    {
        char *s = shell_escape (fullDstFileName ? fullDstFileName : dstFileName);

        shash_add (&linkValues, "outfile", s);
        Safe_free (s);
    }

    if (fullSrcFileName)
    {
        struct dbuf_s dbuf;
        char *s;

        dbuf_init (&dbuf, 128);

        dbuf_append_str (&dbuf, fullDstFileName ? fullDstFileName : dstFileName);
        //dbuf_append (&dbuf, ".o", 2); // no need .o
        s = shell_escape (dbuf_c_str (&dbuf));
        dbuf_destroy (&dbuf);
        shash_add (&linkValues, "user_ofile", s);
        Safe_free (s);
    }

    shash_add (&linkValues, "ofiles", joinStrSet (processStrSet (relFilesSet, NULL, NULL, shell_escape)));

    /* LIBRARIES */

	


    procName = processor_base_name ();
    if (!procName)
        procName = "HY11P13";
    {
        char libname[256]="";
//        int i;
		switch (HY08A_getPART()->isEnhancedCore)
		{
		case 2:
			strcpy(libname, "hy41xxsdcc.lib"); break;
		case 3:
			strcpy(libname, "hy15p41sdcc.lib"); break;
		case 4: // 3 and 4 both use hy14p41sdcc.lib
			strcpy(libname, "hy17psdcc.lib"); break;
		case 5:
			strcpy(libname, "hy08esdcc.lib"); break;
		default:
			strcpy(libname, "hy11psdcc.lib"); break;

		}
        addSet(&libFilesSet, Safe_strdup(libname));
    }

    /*
      {
        struct dbuf_s dbuf;

        dbuf_init (&dbuf, 128);
        dbuf_append (&dbuf, "hycon", sizeof ("hycon") - 1);
        dbuf_append_str (&dbuf, procName);
        dbuf_append (&dbuf, ".lib", sizeof (".lib") - 1);
        addSet (&libFilesSet, dbuf_detach_c_str (&dbuf));
      }
    */
    shash_add (&linkValues, "libs", joinStrSet (processStrSet (libFilesSet, "-l " , NULL, shell_escape)));

    lcmd = msprintf(linkValues, LFRM);
    ret = sdcc_system (lcmd);
    Safe_free (lcmd);

    if (ret)
        exit (1);
}

/* Globals */
PORT hy08a_port =
{
    TARGET_ID_HY08A,
    "HY08A",
    "HYCON MCU HY08A",        /* Target name */
    "Undefined",               /* Processor */
    {
        hyaglue,
        TRUE,           /* Emit glue around main */
        MODEL_SMALL, // suport models
		MODEL_SMALL,
        NULL,           /* model == target */
    },
    {
        _asmCmd,
        NULL,
        "-l -s -y",           /* options with --debug */
        "-l -s -y",           /* options without --debug */
        0,
        ".asm",
        NULL            /* no do_assemble function */
    },
    {
        NULL,
        NULL,
        _HY08A_do_link, /* own do link function */
        ".rel",
        0
    },
    {   /* Peephole optimizer */
        _defaultRules
    },
    {
        /* Sizes: char, short, int, long, long long, ptr, fptr, gptr, bit, float, max */
        1, 2, 2, 4, 8, 2,// near ptr
         2,// far ptr
          3,// ptr_size .... depends
           2,// funcptrsiz
            2,//banked fpr size
             1, // bitsize
             4, // float size
	     64 // bitint max

        /* TSD - I changed the size of gptr from 3 to 1. However, it should be
           2 so that we can accomodate the CPU's with 4 register banks (like the
           16f877)
         */
    },
    /* tags for generic pointers */
    { 0x00, 0x00, 0x00, 0x80 },   /* far, near, xstack, code */
    {
        "XSEG    (XDATA)",
        "STACK   (DATA)",
        "code",
        "DSEG    (DATA)",
        "ISEG    (DATA)",
        NULL, /* pdata */
        "XSEG    (XDATA)",
        "BSEG    (BIT)",
        "RSEG    (DATA)",
        "GSINIT  (CODE)",
        "udata_ovr",
        "GSFINAL (CODE)",
        "HOME        (CODE)",
        NULL, // xidata
        NULL, // xinit
        "CONST   (CODE)",       // const_name - const data (code or not)
        "CABS    (ABS,CODE)",   // cabs_name - const absolute data (code or not)
        "XABS    (ABS,XDATA)",  // xabs_name - absolute xdata
        "IABS    (ABS,DATA)",   // iabs_name - absolute data
        NULL,                   // name of segment for initialized variables
        NULL,                   // name of segment for copies of initialized variables in code space
        NULL,
        NULL,
        1,                      // code is read only
        1                       // No fancy alignments supported.
    },
    { NULL, NULL }, // extra areas
    0,              // 2021 aug, abi revision
    {
        +1, 1, 4, 1, 1, 0, 0  // add offset
    }, // this is stack .. direct, bank overhead, isr overhead .. don't change 
    /* HY08A has an 8 bit mul */
    {
        -1, false,false
    }, // shift, has mulint2long, we have no mulint2long
    {
#ifdef DEBUG_
        HY08A_emitDebuggerSymbol
#else
		NULL
#endif
    }, // debugger, we have no dwarf
    {
        127,      /* maxCount ..127 entries*/
        2,          /* sizeofElement, we use RJ */
        /* The rest of these costs are bogus. They approximate */
        /* the behavior of src/SDCCicode.c 1.207 and earlier.  */
        {4,4,4},    /* sizeofMatchJump[] */
        {4,4,4},    /* sizeofRangeCompare[] */
        4,          /* sizeofSubtract */
        4,          /* sizeofDispatch */
    },// jumptablecost
    "_",
    _HY08A_init,
    _HY08A_parseOptions,
    _HY08A_poptions,
    NULL,
    _HY08A_finaliseOptions,
    _HY08A_setDefaultOptions,
    HY08A_assignRegisters,
    _HY08A_getRegName,
	0, // getRegByName ... give a zero!!
    NULL,
    _HY08A_keywords,
    _HY08A_genAssemblerPreamble,
    NULL,         /* no genAssemblerEnd */
    _HY08A_genIVT,
    NULL, // _HY08A_genXINIT
    NULL,         /* genInitStartup */
    _HY08A_reset_regparm,
    _HY08A_regparm,
    NULL,         /* process a pragma */
    NULL,
    _hasNativeMulFor,
    hasExtBitOp,  /* hasExtBitOp */
    oclsExpense,  /* oclsExpense */
    FALSE,
    TRUE,       /* little endian */
    //FALSE,        /* little endian - code enumlates big endian */
    0,            /* leave lt */
    0,            /* leave gt */
    1,            /* transform <= to ! > */
    1,            /* transform >= to ! < */
    1,            /* transform != to !(a == b) */
    0,            /* leave == */
    FALSE,        /* No array initializer support. */
    0,            /* no CSE cost estimation yet */
    NULL,         /* no builtin functions */
    GPOINTER,     /* treat unqualified pointers as "generic" pointers */
    1,            /* reset labelKey to 1 */
    1,            /* globals & local static allowed */
    0,            /* Number of registers handled in the tree-decomposition-based register allocator in SDCCralloc.hpp */
    PORT_MAGIC
};

