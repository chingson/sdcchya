/*-------------------------------------------------------------------------

  SDCCralloc.h - header file register allocation

                Written By -  Sandeep Dutta . sandeep.dutta@usa.net (1998)
    PIC port   - T. Scott Dattalo scott@dattalo.com (2000)
	HYCON .. chingson

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 2, or (at your option) any
   later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

   In other words, you are welcome to use, share and improve this program.
   You are forbidden to forbid anyone else to use, share and improve
   what you give them.   Help stamp out software-hoarding!
-------------------------------------------------------------------------*/

#ifndef SDCCRALLOC_H
#define SDCCRALLOC_H 1
#include<stdint.h>
#include "common.h"

#include "pcoderegs.h"


enum {
    REG_PTR=1,
    REG_GPR,
    REG_CND,
    REG_SFR,
    REG_STK,
    REG_TMP
};

/* definition for the registers */
typedef struct reg_info
{
    short type;  /* can have value
                * REG_GPR, REG_PTR or REG_CND
                * This like the "meta-type" */
    short pc_type; /* pcode type */
    short rIdx;    /* index into register table */
    char *name;    /* name */

    unsigned isFree:1;          /* is currently unassigned  */
    unsigned wasUsed:1;         /* becomes true if register has been used */
    unsigned isFixed:1;         /* True if address can't change */
    unsigned isMapped:1;        /* The Register's address has been mapped to physical RAM */
    unsigned isBitField:1;      /* True if reg is type bit OR is holder for several bits */
    unsigned isEmitted:1;       /* True if the reg has been written to a .asm file */
    unsigned isPublic:1;        /* True if the reg is not static and can be modified in another module (ie a another c or asm file) */
    unsigned isExtern:1;        /* True if the reg is in another module */
    unsigned isVolatile : 1;
    unsigned isInX: 1;          // for LDPR optimize
    unsigned isFuncLocal : 1;   // add for HYA, funcLocal
	unsigned isLocalStatic : 1; // sometime is local statc
	unsigned addrReferenced : 1; // if used by pointer, it cannot be used for bridge opt
	unsigned isBitInit1 : 1;	// if bit var, this is 1 if it is initialised to TRUE
	unsigned char packBitIni;  // 8 bit may pack to a byte, if any bit is 1 will be noted here
    //char *funcName;
    unsigned address;           /* reg's address if isFixed | isMapped is true */
    unsigned size;              /* 0 for byte, 1 for int, 4 for long */
    unsigned alias;             /* Alias mask if register appears in multiple banks */
    struct reg_info *reg_alias; /* If more than one register share the same address
                               * then they'll point to each other. (primarily for bits)*/
    pCodeRegLives reglives;     /* live range mapping */
}
reg_info;

struct svaropt
{
	reg_info *source_reg;
	reg_info *dest_reg;
	unsigned int sta_remove : 1;
	int lit; // can be optimized to other reg or literal (-1) is fake literal
	int dstOffset;
	char *varOptString;
	uint64_t hashid;
} 
;

extern reg_info regsHY08A[];
extern int Gstack_base_addr;

/*
  As registers are created, they're added to a set (based on the
  register type). Here are the sets of registers that are supported
  in the  port:
*/
extern set *dynAllocRegs;
extern reg_info *dynAllocRegs_map[32768];
extern set *dynStackRegs;
extern reg_info *dynStackRegs_map[512];
extern set *dynProcessorRegs;
// 2017 add a map to faster search
extern reg_info *dynProcessorRegs_map[32768]; // only 512
extern set *dynDirectRegs;
extern reg_info *dynDirectRegs_map[32768];
extern set *dynDirectXRegs;
extern set *dynDirectBitRegs;
extern set *dynInternalRegs;


void initStack(int base_address, int size, int shared);
reg_info *HY08A_regWithIdx (int);
reg_info *dirregWithName (char *name );
void HY08A_assignRegisters (ebbIndex *ebbi);
reg_info *HY08A_findFreeReg(short type);
reg_info *HY08A_allocWithIdx (int idx);
reg_info *typeRegWithIdx (int idx, int type, int fixed);
reg_info *regFindWithName (const char *name);

void HY08A_debugLogClose(void);
void writeUsedRegs(FILE *of);

reg_info *allocDirReg (operand *op );
reg_info *allocInternalRegister(int rIdx, char * name, HYA_OPTYPE po_type, int alias);
reg_info *allocProcessorRegister(int rIdx, char * name, short po_type, int alias);
reg_info *allocRegByName (char *name, int size, int xregion );
reg_info *allocNewDirReg (sym_link *symlnk,const char *name, struct initList *ival);

/* Define register address that are constant across family */
#define IDX_INDF    0
#define IDX_INDF0   0
#define IDX_TMR0    1
#define IDX_PCL     2
#define IDX_STATUS  3
#define IDX_FSR     4
#define IDX_FSR0L   4
#define IDX_FSR0H   5
#define IDX_PLUSW0	6
#define IDX_PCLATU  0x09
#define IDX_PCLATH  0x0a
#define IDX_INTCON  0x0b
#define IDX_PRODL    0x10c
#define IDX_PRODH    0x10d
#define IDX_TBLPTR  0x0d
#define IDX_TBLD    0x0e
#define IDX_POINC  0x0f
#define IDX_SSPBUF 0x10
#define IDX_WREG 0x11
#define IDX_PODEC 0x12
#define IDX_PRINC 0x13

#define IDX_FSR0PTR 0x7000
#define IDX_FSR1PTR 0x7001
#define IDX_FSR2PTR 0x7002
#define IDX_ADCR 0x7003
#define IDX_ADCO1 0x7004
#define IDX_ADCO2 0x7005

#define IDX_ADCRH 0x7006
#define IDX_ADCRM 0x7007
#define IDX_ADCRL 0x7008
#define IDX_ADCO1H 0x7009
#define IDX_ADCO1M 0x700a
#define IDX_ADCO1L 0x700b
#define IDX_ADCO2H 0x700c
#define IDX_ADCO2M 0x700d
#define IDX_ADCO2L 0x700e

//#define IDX_KZ      0x7fff   /* Known zero - actually just a general purpose reg. */
// 2020 June remove WSAVE
#define IDX_WSAVE   0x7ffe
//#define IDX_SSAVE   0x7ffd
//#define IDX_PSAVE   0x7ffc

#define HY08A_nRegs 128
#ifndef PTRSIZE
#define PTRSIZE 2
#endif

#endif
