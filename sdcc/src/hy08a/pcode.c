/*-------------------------------------------------------------------------

    pcode.c - post code generation
    Written By -  Scott Dattalo scott@dattalo.com

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2, or (at your option) any
    later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.`

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
    `
    2016/2017: modified by chingsonchen@hycontek.com.tw for HYCON 8-bit CPU
-------------------------------------------------------------------------*/

#include "device.h"
#include "gen.h"
#include "pcode.h"
#include "pcodeflow.h"
#include "ralloc.h"
#include "city.h"
#ifndef hash
#define hash(x) CityHash64(x, strlen(x))
#endif

/****************************************************************/
/****************************************************************/

// Eventually this will go into device dependent files:
pCodeOpReg pc_status = {{PO_STATUS, "_STATUS", NULL, 0, 0, NULL, {0, 0, 0, 0, 0, 0, 0, 0, 0}}, -1, NULL, 0, NULL};
// pCodeOpReg pc_fsr       = {{PO_FSR,     "_FSR"}, -1, NULL,0,NULL};
pCodeOpReg pc_fsr0l = {{PO_FSR0L, "_FSR0L", NULL, 0, 0, NULL, {0, 0, 0, 0, 0, 0, 0, 0, 0}}, -1, NULL, 0, NULL};
pCodeOpReg pc_fsr0h = {{PO_FSR0H, "_FSR0H", NULL, 0, 0, NULL, {0, 0, 0, 0, 0, 0, 0, 0, 0}}, -1, NULL, 0, NULL};
pCodeOpReg pc_indf0 = {{PO_INDF, "_INDF0", NULL, 0, 0, NULL, {0, 0, 0, 0, 0, 0, 0, 0, 0}}, -1, NULL, 0, NULL};
pCodeOpReg pc_poinc0 = {{PO_POINC0, "_POINC0", NULL, 0, 0, NULL, {0, 0, 0, 0, 0, 0, 0, 0, 0}}, -1, NULL, 0, NULL}; // POINC0 is special one, for later optimization
pCodeOpReg pc_princ0 = {{PO_PRINC0, "_PRINC0", NULL, 0, 0, NULL, {0, 0, 0, 0, 0, 0, 0, 0, 0}}, -1, NULL, 0, NULL}; //
pCodeOpReg pc_podec0 = {{PO_PODEC0, "_PODEC0", NULL, 0, 0, NULL, {0, 0, 0, 0, 0, 0, 0, 0, 0}}, -1, NULL, 0, NULL}; //
pCodeOpReg pc_princ2 = {{PO_PRINC2, "_PRINC2", NULL, 0, 0, NULL, {0, 0, 0, 0, 0, 0, 0, 0, 0}}, -1, NULL, 0, NULL}; //
pCodeOpReg pc_podec2 = {{PO_PODEC2, "_PODEC2", NULL, 0, 0, NULL, {0, 0, 0, 0, 0, 0, 0, 0, 0}}, -1, NULL, 0, NULL}; //
pCodeOpReg pc_plusw0 = {{PO_PLUSW, "_PLUSW0", NULL, 0, 0, NULL, {0, 0, 0, 0, 0, 0, 0, 0, 0}}, -1, NULL, 0, NULL};
pCodeOpReg pc_wreg = {{PO_W, "_WREG", NULL, 0, 0, NULL, {0, 0, 0, 0, 0, 0, 0, 0, 0}}, -1, NULL, 0, NULL};

pCodeOpReg pc_fsr1l = {{PO_FSR, "_FSR1L", NULL, 0, 0, NULL, {0, 0, 0, 0, 0, 0, 0, 0, 0}}, -1, NULL, 0, NULL};
pCodeOpReg pc_fsr1h = {{PO_FSR, "_FSR1H", NULL, 0, 0, NULL, {0, 0, 0, 0, 0, 0, 0, 0, 0}}, -1, NULL, 0, NULL};
pCodeOpReg pc_indf1 = {{PO_INDF, "_INDF1", NULL, 0, 0, NULL, {0, 0, 0, 0, 0, 0, 0, 0, 0}}, -1, NULL, 0, NULL};
pCodeOpReg pc_poinc1 = {{PO_POINC, "_POINC1", NULL, 0, 0, NULL, {0, 0, 0, 0, 0, 0, 0, 0, 0}}, -1, NULL, 0, NULL};
pCodeOpReg pc_plusw1 = {{PO_PLUSW, "_PLUSW1", NULL, 0, 0, NULL, {0, 0, 0, 0, 0, 0, 0, 0, 0}}, -1, NULL, 0, NULL};

pCodeOpReg pc_fsr2l = {{PO_FSR, "_FSR2L", NULL, 0, 0, NULL, {0, 0, 0, 0, 0, 0, 0, 0, 0}}, -1, NULL, 0, NULL};
pCodeOpReg pc_fsr2h = {{PO_FSR, "_FSR2H", NULL, 0, 0, NULL, {0, 0, 0, 0, 0, 0, 0, 0, 0}}, -1, NULL, 0, NULL};
pCodeOpReg pc_indf2 = {{PO_INDF, "_INDF2", NULL, 0, 0, NULL, {0, 0, 0, 0, 0, 0, 0, 0, 0}}, -1, NULL, 0, NULL};
pCodeOpReg pc_poinc2 = {{PO_POINC, "_POINC2", NULL, 0, 0, NULL, {0, 0, 0, 0, 0, 0, 0, 0, 0}}, -1, NULL, 0, NULL};
pCodeOpReg pc_plusw2 = {{PO_PLUSW, "_PLUSW2", NULL, 0, 0, NULL, {0, 0, 0, 0, 0, 0, 0, 0, 0}}, -1, NULL, 0, NULL};

pCodeOpReg pc_inte0 = {{PO_INTE0, "_INTE0", NULL, 0, 0, NULL, {0, 0, 0, 0, 0, 0, 0, 0, 0}}, -1, NULL, 0, NULL};
pCodeOpReg pc_pclatl = {{PO_PCL, "_PCLATL", NULL, 0, 0, NULL, {0, 0, 0, 0, 0, 0, 0, 0, 0}}, -1, NULL, 0, NULL};
pCodeOpReg pc_pclath = {{PO_PCLATH, "_PCLATH", NULL, 0, 0, NULL, {0, 0, 0, 0, 0, 0, 0, 0, 0}}, -1, NULL, 0, NULL};
pCodeOpReg pc_pclatu = {{PO_PCLATU, "_PCLATU", NULL, 0, 0, NULL, {0, 0, 0, 0, 0, 0, 0, 0, 0}}, -1, NULL, 0, NULL};
pCodeOpReg pc_prodl = {{PO_PRODL, "_PRODL", NULL, 0, 0, NULL, {0, 0, 0, 0, 0, 0, 0, 0, 0}}, -1, NULL, 0, NULL};
pCodeOpReg pc_prodh = {{PO_PRODH, "_PRODH", NULL, 0, 0, NULL, {0, 0, 0, 0, 0, 0, 0, 0, 0}}, -1, NULL, 0, NULL};
pCodeOpReg pc_tblptrl = {{PO_TBLPTRL, "_TBLPTRL", NULL, 0, 0, NULL, {0, 0, 0, 0, 0, 0, 0, 0, 0}}, -1, NULL, 0, NULL};
pCodeOpReg pc_tblptrh = {{PO_TBLPTRH, "_TBLPTRH", NULL, 0, 0, NULL, {0, 0, 0, 0, 0, 0, 0, 0, 0}}, -1, NULL, 0, NULL};
pCodeOpReg pc_tbldl = {{PO_TBLDL, "_TBLDL", NULL, 0, 0, NULL, {0, 0, 0, 0, 0, 0, 0, 0, 0}}, -1, NULL, 0, NULL};
pCodeOpReg pc_tbldh = {{PO_TBLDH, "_TBLDH", NULL, 0, 0, NULL, {0, 0, 0, 0, 0, 0, 0, 0, 0}}, -1, NULL, 0, NULL};

// FSRPTR is for hw operation of ++ operations, etc
pCodeOpReg pc_fsr0ptr = {{PO_FSRPTR0, "_FSR0P", NULL, 0, 0, NULL, {0, 0, 0, 0, 0, 0, 0, 0, 0}}, -1, NULL, 0, NULL};
pCodeOpReg pc_fsr1ptr = {{PO_FSRPTR1, "_FSR1P", NULL, 0, 0, NULL, {0, 0, 0, 0, 0, 0, 0, 0, 0}}, -1, NULL, 0, NULL};
pCodeOpReg pc_fsr2ptr = {{PO_FSRPTR2, "_FSR2P", NULL, 0, 0, NULL, {0, 0, 0, 0, 0, 0, 0, 0, 0}}, -1, NULL, 0, NULL};

// pseudo ADC data registers
pCodeOpReg pc_adcr = {{PO_ADCR, "_ADCR", NULL, 0, 0, NULL, {0, 0, 0, 0, 0, 0, 0, 0, 0}}, -1, NULL, 0, NULL};
pCodeOpReg pc_adc1o = {{PO_ADCO1, "_ADC1O", NULL, 0, 0, NULL, {0, 0, 0, 0, 0, 0, 0, 0, 0}}, -1, NULL, 0, NULL};
pCodeOpReg pc_adc2o = {{PO_ADCO2, "_ADC2O", NULL, 0, 0, NULL, {0, 0, 0, 0, 0, 0, 0, 0, 0}}, -1, NULL, 0, NULL};
pCodeOpReg pc_adcrh = {{PO_ADCRB, "_ADCRH", NULL, 0, 0, NULL, {0, 0, 0, 0, 0, 0, 0, 0, 0}}, -1, NULL, 0, NULL};
pCodeOpReg pc_adcrm = {{PO_ADCRB, "_ADCRM", NULL, 0, 0, NULL, {0, 0, 0, 0, 0, 0, 0, 0, 0}}, -1, NULL, 0, NULL};
pCodeOpReg pc_adcrl = {{PO_ADCRB, "_ADCRL", NULL, 0, 0, NULL, {0, 0, 0, 0, 0, 0, 0, 0, 0}}, -1, NULL, 0, NULL};
pCodeOpReg pc_adco1h = {{PO_ADCO1B, "_ADCO1H", NULL, 0, 0, NULL, {0, 0, 0, 0, 0, 0, 0, 0, 0}}, -1, NULL, 0, NULL};
pCodeOpReg pc_adco1m = {{PO_ADCO1B, "_ADCO1M", NULL, 0, 0, NULL, {0, 0, 0, 0, 0, 0, 0, 0, 0}}, -1, NULL, 0, NULL};
pCodeOpReg pc_adco1l = {{PO_ADCO1B, "_ADCO1L", NULL, 0, 0, NULL, {0, 0, 0, 0, 0, 0, 0, 0, 0}}, -1, NULL, 0, NULL};
pCodeOpReg pc_adco2h = {{PO_ADCO2B, "_ADCO2H", NULL, 0, 0, NULL, {0, 0, 0, 0, 0, 0, 0, 0, 0}}, -1, NULL, 0, NULL};
pCodeOpReg pc_adco2m = {{PO_ADCO2B, "_ADCO2M", NULL, 0, 0, NULL, {0, 0, 0, 0, 0, 0, 0, 0, 0}}, -1, NULL, 0, NULL};
pCodeOpReg pc_adco2l = {{PO_ADCO2B, "_ADCO2L", NULL, 0, 0, NULL, {0, 0, 0, 0, 0, 0, 0, 0, 0}}, -1, NULL, 0, NULL};

pCodeOpReg pc_sspbuf = {{PO_SSPBUF, "_SSPBUF", NULL, 0, 0, NULL, {0, 0, 0, 0, 0, 0, 0, 0, 0}}, -1, NULL, 0, NULL};

pCodeOpReg *pc_indf = &pc_indf0;
pCodeOpReg *pc_poinc = &pc_poinc0;
pCodeOpReg *pc_plusw = &pc_plusw0;

pCodeOpReg pc_wsave = {{PO_GPR_REGISTER, "WSAVE_LOCAL", NULL, 0, 0, NULL, {0, 0, 0, 0, 0, 0, 0, 0, 0}}, -1, NULL, 0, NULL};
// pCodeOpReg pc_ssave     = {{PO_GPR_REGISTER,  "SSAVE",NULL,0,0,NULL,{0,0,0,0,0,0,0,0,0}}, -1, NULL,0,NULL};
// pCodeOpReg pc_psave     = {{PO_GPR_REGISTER,  "PSAVE",NULL,0,0,NULL,{0,0,0,0,0,0,0,0,0}}, -1, NULL,0,NULL};

pFile *the_pFile = NULL;

#define SET_BANK_BIT (1 << 16)
#define CLR_BANK_BIT 0

#define SEARCHMAX 4096

static peepCommand peepCommands[] = {

    {NOTBITSKIP, "_NOTBITSKIP_"},
    {BITSKIP, "_BITSKIP_"},
    {INVERTBITSKIP, "_INVERTBITSKIP_"},

    {-1, NULL}};

static int mnemonics_initialized = 0;

static hTab *HY08AMnemonicsHash = NULL;
static hTab *HY08ApCodePeepCommandsHash = NULL;
// static hTab *HY08AMnemonicsHashAlt = NULL;

static pBlock *pb_dead_pcodes = NULL;

static int src_is_stkxx = 0; // 2016 dec optimize stkxx transfer!!
static int src_is_global = 0;

/* Hardcoded flags to change the behavior of the  port */
static int functionInlining = 1; /* inline functions if nonzero */

// static int GpCodeSequenceNumber = 1;
// static int GpcFlowSeq = 1;

/* statistics (code size estimation) */
static unsigned int pcode_insns = 0;
static unsigned int pcode_doubles = 0;
int removed_count = 0;

// static int var_stk_saved = 0;

// static unsigned peakIdx = 0; /* This keeps track of the peak register index for call tree register reuse */

/****************************************************************/
/*                      Forward declarations                    */
/****************************************************************/

static void genericDestruct(pCode *pc);
static void genericPrint(FILE *of, pCode *pc);

static void pBlockStats(FILE *of, pBlock *pb);
static pCode *findFunction(char *fname);
static void pCodePrintLabel(FILE *of, pCode *pc);
static void pCodePrintFunction(FILE *of, pCode *pc);
static void pCodeOpPrint(FILE *of, pCodeOp *pcop);
static char *get_op_from_instruction(pCodeInstruction *pcc);
static pBlock *newpBlock(void);
static int removepCode(pCode **pc_sta);
static int replaceTempOfCSym(pCodeFunction *pcf, pCodeOp *formerlypcop, pCodeOp *newpcop);

static int fsr0ana(pBlock *pb);
// static int fsr0ana2(pBlock *pb); // cast remat fail cases

extern int pCodeOpCompare(pCodeOp *pcops, pCodeOp *pcopd);

static int replaceLabelPtrTabInPb(pBlock *pb, pCode *old, pCode *new);

static int xorChainOptimize(pBlock *pb); //  for unsigned-char case-switch

static bool chkReferencedBeforeChange(pCode *pc, int tempid, reg_info *r, int index, int depth);

static inline int pcMVFFLIKE(pCode *pc)
{
    if (PCI(pc)->op == POC_MVFF || PCI(pc)->op == POC_MVSF || PCI(pc)->op == POC_MVSS)
        return 1;
    return 0;
}

#define STATUSCBIT popCopyGPR2Bit(PCOP(&pc_status), HYA_C_BIT)
#define STATUSZBIT popCopyGPR2Bit(PCOP(&pc_status), HYA_Z_BIT)

/****************************************************************/
/*                     Instructions                          */
/****************************************************************/

static pCodeInstruction pciADDWF = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_ADDWF,
    "ADDF",
    "ADDWF",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    3,    // num ops
    1,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    (PCC_W | PCC_REGISTER),                         // inCond
    (PCC_REGISTER | PCC_C | PCC_DC | PCC_Z | PCC_N) // outCond
    ,
    asm_FDA,
    NULL // OP2
    ,
    0,
    0};

static pCodeInstruction pciADDFW = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_ADDFW,
    "ADDF",
    "ADDFW",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    3,    // num ops
    0,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    (PCC_W | PCC_REGISTER),                  // inCond
    (PCC_W | PCC_C | PCC_DC | PCC_Z | PCC_N) // outCond
    ,
    asm_FDA,
    NULL,
    0,
    0};

static pCodeInstruction pciADCFW = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_ADCFW,
    "ADDC",
    "ADCFW",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    3,    // num ops
    0,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    (PCC_W | PCC_REGISTER | PCC_C),          // inCond
    (PCC_W | PCC_C | PCC_DC | PCC_Z | PCC_N) // outCond
    ,
    asm_FDA,
    NULL,
    0,
    0};
static pCodeInstruction pciADCWF = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_ADCWF,
    "ADDC",
    "ADCWF",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    3,    // num ops
    1,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    (PCC_W | PCC_REGISTER | PCC_C),                 // inCond
    (PCC_REGISTER | PCC_C | PCC_DC | PCC_Z | PCC_N) // outCond
    ,
    asm_FDA,
    NULL,
    0,
    0};

static pCodeInstruction pciSBCFWF = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_SBCFWF,
    "SUBC",
    "SBCFWF",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    3,    // num ops
    1,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    (PCC_W | PCC_REGISTER),                         // inCond
    (PCC_REGISTER | PCC_C | PCC_DC | PCC_N | PCC_Z) // outCond
    ,
    asm_FDA,
    NULL,
    0,
    0};
static pCodeInstruction pciSBCFWW = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_SBCFWW,
    "SUBC",
    "SBCFWW",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    3,    // num ops
    0,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    (PCC_W | PCC_REGISTER),                  // inCond
    (PCC_W | PCC_C | PCC_DC | PCC_Z | PCC_N) // outCond
    ,
    asm_FDA,
    NULL,
    0,
    0};

static pCodeInstruction pciMULWF = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_MULWF,
    "MULF",
    "MULFW",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    2,    // num ops
    0,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    (PCC_W | PCC_REGISTER), // inCond
    (PCC_PROD)              // outCond
    ,
    asm_FA,
    NULL,
    0,
    0};

static pCodeInstruction pciADDLW = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_ADDLW,
    "ADDL",
    "ADDLW",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    1,    // num ops
    0,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    1, // literal operand
    0,
    0,
    0,
    POC_NOP,
    (PCC_W | PCC_LITERAL),                   // inCond
    (PCC_W | PCC_Z | PCC_C | PCC_DC | PCC_N) // outCond
    ,
    asm_LIT,
    NULL,
    0,
    0};

static pCodeInstruction pciMULLW = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_MULLW,
    "MULL",
    "MULLW",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    1,    // num ops
    0,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    1, // literal operand
    0,
    0,
    0,
    POC_NOP,
    (PCC_W | PCC_LITERAL), // inCond
    (PCC_PROD)             // outCond
    ,
    asm_LIT,
    NULL,
    0,
    0};

static pCodeInstruction pciANDLW = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_ANDLW,
    "ANDL",
    "ANDLW",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    1,    // num ops
    0,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    1, // literal operand
    0,
    0,
    0,
    POC_NOP,
    (PCC_W | PCC_LITERAL),  // inCond
    (PCC_W | PCC_Z | PCC_N) // outCond
    ,
    asm_LIT,
    NULL,
    0,
    0};

static pCodeInstruction pciANDWF = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_ANDWF,
    "ANDF",
    "ANDWF",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    3,    // num ops
    1,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    (PCC_W | PCC_REGISTER),        // inCond
    (PCC_REGISTER | PCC_Z | PCC_N) // outCond
    ,
    asm_FDA,
    NULL,
    0,
    0};

static pCodeInstruction pciANDFW = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_ANDFW,
    "ANDF",
    "ANDFW",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    3,    // num ops
    0,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    (PCC_W | PCC_REGISTER), // inCond
    (PCC_W | PCC_Z | PCC_N) // outCond
    ,
    asm_FDA,
    NULL,
    0,
    0};

static pCodeInstruction pciBCF = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_BCF,
    "BCF",
    "BCF", // bit clear flag
    NULL,  // from branch
    NULL,  // to branch
    NULL,  // label
    NULL,  // operand
    NULL,  // flow block
    NULL,  // C source
    3,     // num ops
    1,
    1, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_BSF,
    (PCC_REGISTER | PCC_EXAMINE_PCOP), // inCond
    (PCC_REGISTER | PCC_EXAMINE_PCOP)  // outCond
    ,
    asm_FBA,
    NULL,
    0,
    0};

static pCodeInstruction pciBSF = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_BSF, // bit set flag
    "BSF",
    "BSF",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    3,    // num ops
    1,
    1, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_BCF,
    (PCC_REGISTER | PCC_EXAMINE_PCOP), // inCond, bsf is x&=0xyy FLAGS proc specially
    (PCC_REGISTER | PCC_EXAMINE_PCOP)  // outCond
    ,
    asm_FBA,
    NULL,
    0,
    0};

static pCodeInstruction pciBTSZ = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_BTSZ,
    "BTSZ",
    "BTFSC",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    3,    // num ops
    0,
    1, // dest, bit instruction
    1,
    1,
    1, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_BTSS,
    (PCC_REGISTER | PCC_EXAMINE_PCOP), // inCond
    PCC_NONE                           // outCond
    ,
    asm_FBA,
    NULL,
    0,
    0};

static pCodeInstruction pciCPSE = { // compare skip if equal
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_CPSE,
    "CPSE",
    "CPSE",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    2,    // num ops
    0,
    0, // dest, bit instruction
    1,
    1,
    0, // branch, skip, skipinv, we have no CPSNE
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    (PCC_REGISTER | PCC_W), // inCond
    PCC_NONE                // outCond
    ,
    asm_FA,
    NULL,
    0,
    0};

static pCodeInstruction pciCPSG = { // compare skip if F greater than w
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_CPSG,
    "CPSG",
    "CPSG",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    2,    // num ops
    0,
    0, // dest, bit instruction
    1,
    1,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    (PCC_REGISTER | PCC_W), // inCond
    PCC_NONE                // outCond
    ,
    asm_FA,
    NULL,
    0,
    0};
static pCodeInstruction pciCPSL = { // compare skip if F greater than w
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_CPSL,
    "CPSL",
    "CPSL",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    2,    // num ops
    0,
    0, // dest, bit instruction
    1,
    1,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    (PCC_REGISTER | PCC_W), // inCond
    PCC_NONE                // outCond
    ,
    asm_FA,
    NULL,
    0,
    0};

static pCodeInstruction pciTFSZ = { // compare skip if F greater than w
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_TFSZ,
    "TFSZ",
    "TFSZ",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    2,    // num ops
    0,
    0, // dest, bit instruction
    1,
    1,
    0, // branch, skip, no inv skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    (PCC_REGISTER | PCC_EXAMINE_PCOP), // inCond
    PCC_NONE                           // outCond
    ,
    asm_FA,
    NULL,
    0,
    0};

static pCodeInstruction pciBTSS = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_BTSS,
    "BTSS",
    "BTFSS",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    3,    // num ops
    0,
    1, // dest, bit instruction
    1,
    1,
    1, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_BTSZ,
    (PCC_REGISTER | PCC_EXAMINE_PCOP), // inCond
    PCC_NONE                           // outCond
    ,
    asm_FBA,
    NULL,
    0,
    0};
static pCodeInstruction pciBTGF = { // toggle a bit
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_BTGF,
    "BTGF",
    "BTGF",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    3,    // num ops
    1,
    1, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_BTSZ,
    (PCC_REGISTER | PCC_EXAMINE_PCOP), // inCond
    PCC_REGISTER | PCC_EXAMINE_PCOP    // outCond
    ,
    asm_FBA,
    NULL,
    0,
    0};

static pCodeInstruction pciCALL = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_CALL,
    "CALL",
    "CALL",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    1,    // num ops
    0,
    0, // dest, bit instruction
    1,
    0,
    0, // branch, skip // CALL is not a branch ??
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    (PCC_NONE),                                         // void has no PCCW
    (PCC_NONE | PCC_W | PCC_C | PCC_DC | PCC_Z | PCC_N) // outCond, flags are destroyed by called function
    ,
    asm_LABEL,
    NULL,
    0,
    0};

static pCodeInstruction pciCOMF = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_COMF,
    "COMF",
    "COMF",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    3,    // num ops
    1,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    PCC_REGISTER | PCC_EXAMINE_PCOP,                // inCond
    PCC_REGISTER | PCC_EXAMINE_PCOP | PCC_Z | PCC_N // outCond
    ,
    asm_FDA,
    NULL,
    0,
    0};

static pCodeInstruction pciCOMFW = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_COMFW,
    "COMF",
    "COMFW",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    3,    // num ops
    0,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    PCC_REGISTER | PCC_EXAMINE_PCOP, // inCond
    PCC_W | PCC_Z | PCC_N            // outCond
    ,
    asm_FDA,
    NULL,
    0,
    0};

static pCodeInstruction pciCLRF = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_CLRF,
    "CLRF",
    "CLRF",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    2,    // num ops
    1,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    PCC_NONE,                       // inCond
    PCC_REGISTER | PCC_EXAMINE_PCOP // outCond
    ,
    asm_FA,
    NULL,
    0,
    0};

static pCodeInstruction pciSETF = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_SETF,
    "SETF",
    "SETF",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    2,    // num ops
    1,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    PCC_NONE,                       // inCond
    PCC_REGISTER | PCC_EXAMINE_PCOP // outCond
    ,
    asm_FA,
    NULL,
    0,
    0};

// static pCodeInstruction pciCLRW = {
//	{PC_OPCODE, 0,0,0,NULL,NULL,NULL,
//		genericDestruct,
//		genericPrint},
//		POC_CLRW,
//		"CLRW",
//		"CLRW",
//		NULL, // from branch
//		NULL, // to branch
//		NULL, // label
//		NULL, // operand
//		NULL, // flow block
//		NULL, // C source
//		0,    // num ops
//		0,0,  // dest, bit instruction
//		0,0,  // branch, skip
//		0,    // literal operand
//	0,0,
//		POC_NOP,
//		PCC_NONE, // inCond
//		PCC_W | PCC_Z  // outCond
//		,asm_NONE
//,NULL,0 };

static pCodeInstruction pciCLRWDT = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_CLRWDT,
    "CLRWDT",
    "CLRWDT",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    0,    // num ops
    0,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    PCC_NONE, // inCond
    PCC_NONE  // outCond
    ,
    asm_NONE,
    NULL,
    0,
    0};

static pCodeInstruction pciDECF = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_DECF,
    "DCF",
    "DECF",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    3,    // num ops
    1,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    PCC_REGISTER,                                         // inCond
    PCC_REGISTER | PCC_Z | PCC_N | PCC_O | PCC_DC | PCC_C // outCond
    ,
    asm_FDA,
    NULL,
    0,
    0};

static pCodeInstruction pciDECFW = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_DECFW,
    "DCF",
    "DECFW",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    3,    // num ops
    0,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    PCC_REGISTER,                                  // inCond
    PCC_W | PCC_Z | PCC_N | PCC_O | PCC_DC | PCC_C // outCond
    ,
    asm_FDA,
    NULL,
    0,
    0};

static pCodeInstruction pciDCSZ = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_DCSZ,
    "DCSZ",
    "DECFSZ",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    3,    // num ops
    1,
    0, // dest, bit instruction
    1,
    1,
    1, // branch, skip, skipinv is dcsuz
    0, // literal operand
    0,
    0,
    0,
    POC_DCSUZ,                       // followed by BTFSC STATUS, Z --> also kills STATUS
    PCC_REGISTER | PCC_EXAMINE_PCOP, // inCond
    PCC_REGISTER | PCC_EXAMINE_PCOP  // outCond NONE, cool
    ,
    asm_FDA,
    NULL,
    0,
    0};

static pCodeInstruction pciDCSUZ = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_DCSUZ,
    "DCSUZ",
    "DECFSNZ",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    3,    // num ops
    1,
    0, // dest, bit instruction
    1,
    1,
    1, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_DCSZ,                        // followed by BTFSC STATUS, Z --> also kills STATUS
    PCC_REGISTER | PCC_EXAMINE_PCOP, // inCond
    PCC_REGISTER | PCC_EXAMINE_PCOP  // outCond
    ,
    asm_FDA,
    NULL,
    0,
    0};

static pCodeInstruction pciDCSZW = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_DCSZW,
    "DCSZ",
    "DECFSZW",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    3,    // num ops
    0,
    0, // dest, bit instruction
    1,
    1,
    1, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_DCSUZW,                      // followed by BTFSC STATUS, Z --> also kills STATUS
    PCC_REGISTER | PCC_EXAMINE_PCOP, // inCond
    PCC_W | PCC_EXAMINE_PCOP         // outCond
    ,
    asm_FDA,
    NULL,
    0,
    0};

static pCodeInstruction pciDCSUZW = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_DCSUZW,
    "DCSUZ",
    "DECFSNZW",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    3,    // num ops
    0,
    0, // dest, bit instruction
    1,
    1,
    1, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_DCSZW,                       // followed by BTFSC STATUS, Z --> also kills STATUS
    PCC_REGISTER | PCC_EXAMINE_PCOP, // inCond
    PCC_W                            // outCond W only no flags
    ,
    asm_FDA,
    NULL,
    0,
    0};

static pCodeInstruction pciJMP = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_JMP,
    "JMP",
    "GOTO",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    1,    // num ops
    0,
    0, // dest, bit instruction
    1,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    PCC_NONE, // inCond
    PCC_NONE  // outCond
    ,
    asm_NONE,
    NULL,
    0,
    0};
static pCodeInstruction pciRJ = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_RJ,
    "RJ",
    "RGOTO",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    1,    // num ops
    0,
    0, // dest, bit instruction
    1,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    PCC_NONE, // inCond
    PCC_NONE  // outCond
    ,
    asm_NONE,
    NULL,
    0,
    0};

static pCodeInstruction pciRCALL = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_RCALL,
    "RCALL",
    "RCALL",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    1,    // num ops
    0,
    0, // dest, bit instruction
    1,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    PCC_NONE, // inCond
    PCC_NONE  // outCond
    ,
    asm_NONE,
    NULL,
    0,
    0};
static pCodeInstruction pciINF = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_INF,
    "INF",
    "INCF",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    3,    // num ops
    1,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    PCC_REGISTER | PCC_EXAMINE_PCOP,                                         // inCond
    PCC_REGISTER | PCC_Z | PCC_C | PCC_DC | PCC_O | PCC_N | PCC_EXAMINE_PCOP // outCond
    ,
    asm_FDA,
    NULL,
    0,
    0};

static pCodeInstruction pciINFW = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_INFW,
    "INF",
    "INCFW",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    3,    // num ops
    0,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    PCC_REGISTER | PCC_EXAMINE_PCOP,               // inCond
    PCC_W | PCC_Z | PCC_C | PCC_DC | PCC_O | PCC_N // outCond
    ,
    asm_FDA,
    NULL,
    0,
    0};

static pCodeInstruction pciINSZ = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_INSZ,
    "INSZ",
    "INCFSZ",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    3,    // num ops
    1,
    0, // dest, bit instruction
    1,
    1,
    1, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_INSUZ,                                 // followed by BTFSC STATUS, Z --> also kills STATUS
    PCC_REGISTER | PCC_EXAMINE_PCOP,           // inCond
    PCC_REGISTER | PCC_NONE | PCC_EXAMINE_PCOP // outCond
    ,
    asm_FDA,
    NULL,
    0,
    0};

static pCodeInstruction pciINSZW = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_INSZW,
    "INSZ",
    "INCFSZW",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    3,    // num ops
    0,
    0, // dest, bit instruction
    1,
    1,
    1, // branch, skip, has skip inv
    0, // literal operand
    0,
    0,
    0,
    POC_INSUZW,                      // followed by BTFSC STATUS, Z --> also kills STATUS
    PCC_REGISTER | PCC_EXAMINE_PCOP, // inCond
    PCC_W | PCC_NONE                 // outCond
    ,
    asm_FDA,
    NULL,
    0,
    0};

static pCodeInstruction pciINSUZ = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_INSUZ,
    "INSUZ",
    "INCFSUZ",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    3,    // num ops
    1,
    0, // dest, bit instruction
    1,
    1,
    1, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_INSZ,                        // followed by BTFSC STATUS, Z --> also kills STATUS
    PCC_REGISTER | PCC_EXAMINE_PCOP, // inCond
    PCC_REGISTER | PCC_NONE          // outCond
    ,
    asm_FDA,
    NULL,
    0,
    0};

static pCodeInstruction pciINSUZW = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_INSUZW,
    "INSUZ",
    "INCFSUZW",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    3,    // num ops
    0,
    0, // dest, bit instruction
    1,
    1,
    1, // branch, skip, has inv
    0, // literal operand
    0,
    0,
    0,
    POC_INSZW,                       // followed by BTFSC STATUS, Z --> also kills STATUS
    PCC_REGISTER | PCC_EXAMINE_PCOP, // inCond
    PCC_W | PCC_NONE                 // outCond
    ,
    asm_FDA,
    NULL,
    0,
    0};

static pCodeInstruction pciIORWF = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_IORWF,
    "IORF",
    "IORWF",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    3,    // num ops
    1,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    (PCC_W | PCC_REGISTER | PCC_EXAMINE_PCOP),        // inCond
    (PCC_REGISTER | PCC_Z | PCC_N | PCC_EXAMINE_PCOP) // outCond
    ,
    asm_FDA,
    NULL,
    0,
    0};

static pCodeInstruction pciIORFW = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_IORFW,
    "IORF",
    "IORFW",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    3,    // num ops
    0,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    (PCC_W | PCC_REGISTER), // inCond
    (PCC_W | PCC_Z | PCC_N) // outCond
    ,
    asm_FDA,
    NULL,
    0,
    0};

static pCodeInstruction pciIORL = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_IORL,
    "IORL",
    "IORLW",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    1,    // num ops
    0,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    1, // literal operand
    0,
    0,
    0,
    POC_NOP,
    (PCC_W | PCC_LITERAL),  // inCond
    (PCC_W | PCC_Z | PCC_N) // outCond
    ,
    asm_LIT,
    NULL,
    0,
    0};

static pCodeInstruction pciMVWF = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_MVWF,
    "MVF",
    "MOVWF",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    3,    // num ops
    1,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    PCC_W,                          // inCond
    PCC_REGISTER | PCC_EXAMINE_PCOP // outCond
    ,
    asm_FDA,
    NULL,
    0,
    0};

// MVWF2 is for future simplify
static pCodeInstruction pciMVWF2 = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_MVWF2,
    "MVF",
    "MOVWF",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    3,    // num ops
    1,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    PCC_W,                          // inCond
    PCC_REGISTER | PCC_EXAMINE_PCOP // outCond
    ,
    asm_FDA,
    NULL,
    0,
    0};

static pCodeInstruction pciMVFW = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_MVFW,
    "MVF",
    "MOVFW",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    3,    // num ops
    0,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    PCC_REGISTER | PCC_EXAMINE_PCOP, // inCond
    (PCC_W)                          // outCond
    ,
    asm_FDA,
    NULL,
    0,
    0};

static pCodeInstruction pciMVFF = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_MVFF,
    "MVFF",
    "MVFF",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    2,    // num ops
    0,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    PCC_REGISTER | PCC_EXAMINE_PCOP, // inCond
    PCC_REGISTER | PCC_EXAMINE_PCOP  // outCond
    ,
    asm_FSFD,
    NULL,
    0,
    0};

static pCodeInstruction pciADDFSR = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_ADDFSR,
    "ADDFSR",
    "ADDFSR",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    2,    // num ops
    0,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    1, // literal operand
    0,
    0,
    0,
    POC_NOP,
    PCC_REGISTER | PCC_EXAMINE_PCOP, // inCond
    PCC_REGISTER | PCC_EXAMINE_PCOP  // outCond
    ,
    asm_FSFD,
    NULL,
    0,
    0};

static pCodeInstruction pciMVLP = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_MVLP,
    "MVLP",
    "MVLP",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    1,    // num ops, always 1
    0,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    0, // literal operand, implied *, or *+
    0,
    0,
    0,
    POC_NOP,
    PCC_REGISTER, // inCond
    PCC_REGISTER  // outCond
    ,
    asm_LABEL, // LP load label!!
    NULL,
    0,
    0};

static pCodeInstruction pciTBLR = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_TBLR,
    "TBLR",
    "TBLR",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    1,    // num ops, always *+
    0,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    0, // literal operand, implied *, or *+
    0,
    0,
    0,
    POC_NOP,
    PCC_REGISTER, // inCond
    PCC_REGISTER  // outCond
    ,
    asm_TBLR,
    NULL,
    0,
    0};

static pCodeInstruction pciPUSHL = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_PUSHL,
    "PUSHL",
    "PUSHL",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    1,    // num ops
    0,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    1, // literal operand
    0,
    0,
    0,
    POC_NOP,
    PCC_NONE,    // inCond
    PCC_REGISTER // outCond
    ,
    asm_LIT,
    NULL,
    0,
    0};

static pCodeInstruction pciMVSS = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_MVSS,
    "MVSS",
    "MVSS",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    2,    // num ops
    0,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    PCC_REGISTER, // inCond
    PCC_REGISTER  // outCond
    ,
    asm_FSFD,
    NULL,
    0,
    0};
static pCodeInstruction pciMVSF = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_MVSF,
    "MVSF",
    "MVSF",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    2,    // num ops
    0,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    PCC_REGISTER, // inCond
    PCC_REGISTER  // outCond
    ,
    asm_FSFD,
    NULL,
    0,
    0};

static pCodeInstruction pciLDPR = { // we fix to LDPR0
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_LDPR,
    "LDPR",
    "LDPR",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    2,    // num ops // LDPR0 no count other para
    0,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    PCC_EXAMINE_PCOP, // inCond
    PCC_FSR           // outCond
    ,
    asm_LDPR,
    NULL,
    0,
    0};

static pCodeInstruction pciMVL = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_MVL,
    "MVL",
    "MOVLW",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    1,    // num ops
    0,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    1, // literal operand
    0,
    0,
    0,
    POC_NOP,
    (PCC_NONE | PCC_LITERAL), // inCond
    PCC_W                     // outCond
    ,
    asm_LIT,
    NULL,
    0,
    0};

static pCodeInstruction pciNOP = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_NOP,
    "NOP",
    "NOP",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    0,    // num ops
    0,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    PCC_NONE, // inCond
    PCC_NONE  // outCond
    ,
    asm_NONE,
    NULL,
    0,
    0};

static pCodeInstruction pciRETI = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_RETI,
    "RETI",
    "RETFIE",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    1,    // num ops
    0,
    0, // dest, bit instruction
    1,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    PCC_NONE,                                   // inCond
    (PCC_NONE | PCC_C | PCC_DC | PCC_Z | PCC_N) // outCond (not true... affects the GIE bit too), STATUS bit are retored
    ,
    asm_ONE // there is 1 bit, but keep zero first
    ,
    NULL,
    0,
    0};

static pCodeInstruction pciRETL = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_RETL,
    "RETL",
    "RETLW",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    1,    // num ops
    0,
    0, // dest, bit instruction
    1,
    0,
    0, // branch, skip
    1, // literal operand
    0,
    0,
    0,
    POC_NOP,
    PCC_LITERAL, // inCond
    (PCC_W)      // outCond, STATUS bits are irrelevant after RETLW
    ,
    asm_LIT,
    NULL,
    0,
    0};

static pCodeInstruction pciRET = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_RET,
    "RET",
    "RETURN",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    0,    // num ops
    0,
    0, // dest, bit instruction
    1,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    PCC_NONE | PCC_W, // inCond, return value is in W, so it is W!!
    (PCC_NONE)        // outCond, STATUS bits are irrelevant after RETURN
    ,
    asm_NONE // there is 1 bit, keep 0 first
    ,
    NULL,
    0,
    0};

static pCodeInstruction pciRETV = { // return void, need no W
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_RETV,
    "RET",
    "RETV",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    0,    // num ops
    0,
    0, // dest, bit instruction
    1,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    PCC_NONE, // inCond, return value is in W, so it is W!!
    (PCC_W)   // outCond, assume W destroyed later
    ,
    asm_NONE // there is 1 bit, keep 0 first
    ,
    NULL,
    0,
    0};

static pCodeInstruction pciRLF = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_RLF,
    "RLF",
    "RLF",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    3,    // num ops
    1,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    (PCC_C | PCC_REGISTER),        // inCond
    (PCC_REGISTER | PCC_N | PCC_Z) // outCond
    ,
    asm_FDA,
    NULL,
    0,
    0};

static pCodeInstruction pciRLFW = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_RLFW,
    "RLF",
    "RLFW",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    3,    // num ops
    0,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    (PCC_REGISTER),         // RLFW has no relation with the output
    (PCC_W | PCC_N | PCC_Z) // outCond
    ,
    asm_FDA,
    NULL,
    0,
    0};

static pCodeInstruction pciRRF = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_RRF,
    "RRF",
    "RRF",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    3,    // num ops
    1,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    (PCC_REGISTER),                // inCond, RRF no C
    (PCC_REGISTER | PCC_Z | PCC_N) // outCond, only Z
    ,
    asm_FDA,
    NULL,
    0,
    0};

static pCodeInstruction pciRRFW = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_RRFW,
    "RRF",
    "RRFW",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    3,    // num ops
    0,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    (PCC_REGISTER),         // inCond, NO C
    (PCC_W | PCC_Z | PCC_N) // outCond, RRFW no effect on C
    ,
    asm_FDA,
    NULL,
    0,
    0};

static pCodeInstruction pciRLCF = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_RLCF,
    "RLFC",
    "RLCF",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    3,    // num ops
    1,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    (PCC_C | PCC_REGISTER),                // inCond
    (PCC_REGISTER | PCC_C | PCC_N | PCC_Z) // outCond
    ,
    asm_FDA,
    NULL,
    0,
    0};

static pCodeInstruction pciRLCFW = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_RLCFW,
    "RLFC", // this should be still RLFC!!
    "RLCFW",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    3,    // num ops
    0,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    (PCC_C | PCC_REGISTER),         // inCond
    (PCC_W | PCC_C | PCC_N | PCC_Z) // outCond
    ,
    asm_FDA,
    NULL,
    0,
    0};

static pCodeInstruction pciRRCF = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_RRCF,
    "RRFC",
    "RRCF",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    3,    // num ops
    1,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    (PCC_C | PCC_REGISTER),                // inCond
    (PCC_REGISTER | PCC_C | PCC_N | PCC_Z) // outCond
    ,
    asm_FDA,
    NULL,
    0,
    0};

static pCodeInstruction pciRRCFW = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_RRCFW,
    "RRFC", // this should be RRFC
    "RRCFW",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    3,    // num ops
    0,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    (PCC_C | PCC_REGISTER),         // inCond
    (PCC_W | PCC_C | PCC_Z | PCC_N) // outCond
    ,
    asm_FDA,
    NULL,
    0,
    0};

static pCodeInstruction pciARLCF = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_ARLCF,
    "ARLC",
    "ARLCF",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    3,    // num ops
    1,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    (PCC_C | PCC_REGISTER),                        // inCond
    (PCC_REGISTER | PCC_C | PCC_O | PCC_N | PCC_Z) // outCond
    ,
    asm_FDA,
    NULL,
    0,
    0};

static pCodeInstruction pciARLCFW = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_ARLCFW,
    "ARLC",
    "ARLCFW",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    3,    // num ops
    0,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    (PCC_C | PCC_REGISTER),                 // inCond
    (PCC_W | PCC_C | PCC_N | PCC_O | PCC_Z) // outCond
    ,
    asm_FDA,
    NULL,
    0,
    0};

static pCodeInstruction pciARRCF = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_ARRCF,
    "ARRC",
    "ARRCF",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    3,    // num ops
    1,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    (PCC_C | PCC_REGISTER),                // inCond
    (PCC_REGISTER | PCC_C | PCC_N | PCC_Z) // outCond
    ,
    asm_FDA,
    NULL,
    0,
    0};

static pCodeInstruction pciARRCFW = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_ARRCFW,
    "ARRC",
    "ARRCFW",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    3,    // num ops
    0,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    (PCC_C | PCC_REGISTER),                 // inCond
    (PCC_W | PCC_C | PCC_Z | PCC_N | PCC_O) // outCond
    ,
    asm_FDA,
    NULL,
    0,
    0};

// static pCodeInstruction pciSUBWF = {
//	{PC_OPCODE, 0,0,0,NULL,NULL,NULL,
//		genericDestruct,
//		genericPrint},
//		POC_SUBF,
//		"SUBWF",
//		"SUBWF",
//		NULL, // from branch
//		NULL, // to branch
//		NULL, // label
//		NULL, // operand
//		NULL, // flow block
//		NULL, // C source
//		3,    // num ops
//		1,0,  // dest, bit instruction
//		0,0,  // branch, skip
//		0,    // literal operand
//		POC_NOP,
//		(PCC_W | PCC_REGISTER),   // inCond
//		(PCC_REGISTER | PCC_C | PCC_DC | PCC_Z) // outCond
//	, asm_FDA
//,NULL,0 };

static pCodeInstruction pciSUBFWW = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_SUBFWW,
    "SUBF",
    "SUBFWW",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    3,    // num ops
    0,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    (PCC_W | PCC_REGISTER),                          // inCond
    (PCC_W | PCC_C | PCC_DC | PCC_Z | PCC_N | PCC_O) // outCond
    ,
    asm_FDA,
    NULL,
    0,
    0};

static pCodeInstruction pciSUBFWF = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_SUBFWF,
    "SUBF",
    "SUBFWF",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    3,    // num ops
    1,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    (PCC_W | PCC_REGISTER),                                 // inCond
    (PCC_REGISTER | PCC_C | PCC_DC | PCC_Z | PCC_N | PCC_O) // outCond
    ,
    asm_FDA,
    NULL,
    0,
    0};

static pCodeInstruction pciSUBL = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_SUBL,
    "SUBL",
    "SUBLW",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    1,    // num ops
    0,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    1, // literal operand
    0,
    0,
    0,
    POC_NOP,
    (PCC_W | PCC_LITERAL),                           // inCond
    (PCC_W | PCC_Z | PCC_C | PCC_DC | PCC_N | PCC_O) // outCond
    ,
    asm_LIT,
    NULL,
    0,
    0};

static pCodeInstruction pciSWPF = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_SWPF,
    "SWPF",
    "SWAPF",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    3,    // num ops
    1,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    (PCC_REGISTER), // inCond
    (PCC_REGISTER)  // outCond
    ,
    asm_FDA,
    NULL,
    0,
    0};

static pCodeInstruction pciSWPFW = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_SWPFW,
    "SWPF",
    "SWAPFW",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    2,    // num ops
    0,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    (PCC_REGISTER), // inCond
    (PCC_W)         // outCond
    ,
    asm_FDA,
    NULL,
    0,
    0};
/*
static pCodeInstruction pciTRIS = {
    {PC_OPCODE, 0,0,0,NULL,NULL,NULL,
        genericDestruct,
        genericPrint},
        POC_TRIS,
        "TRIS",
        NULL, // from branch
        NULL, // to branch
        NULL, // label
        NULL, // operand
        NULL, // flow block
        NULL, // C source
        1,    // num ops
        0,0,  // dest, bit instruction
        0,0,  // branch, skip
        0,    // literal operand
        POC_NOP,
        PCC_NONE,   // inCond  FIXME: what's TR
        PCC_REGISTER // outCond	 FIXME: what's TRIS doing
,NULL,0 };
*/
static pCodeInstruction pciXORF = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_XORF,
    "XORF",
    "XORWF",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    3,    // num ops
    1,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    (PCC_W | PCC_REGISTER),        // inCond
    (PCC_REGISTER | PCC_Z | PCC_N) // outCond
    ,
    asm_FDA,
    NULL,
    0,
    0};

static pCodeInstruction pciXORFW = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_XORFW,
    "XORF",
    "XORFW",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    3,    // num ops
    0,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    (PCC_W | PCC_REGISTER), // inCond
    (PCC_W | PCC_Z | PCC_N) // outCond
    ,
    asm_FDA,
    NULL,
    0,
    0};

static pCodeInstruction pciXORLW = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_XORLW,
    "XORL",
    "XORLW",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    1,    // num ops
    0,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    1, // literal operand
    0,
    0,
    0,
    POC_NOP,
    (PCC_W | PCC_LITERAL),  // inCond
    (PCC_W | PCC_Z | PCC_N) // outCond
    ,
    asm_LIT,
    NULL,
    0,
    0};

static pCodeInstruction pciLBSR = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_LBSR,
    "LBSR",
    "MOVLB",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    1,    // num ops
    0,
    0, // dest, bit instruction
    0,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    PCC_NONE, // inCond
    PCC_BSR   // outCond
    ,
    asm_F,
    NULL,
    0,
    0};

static pCodeInstruction pciJC = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_JC,
    "JC",
    "JC",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    1,    // num ops
    0,
    0, // dest, bit instruction
    1,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    PCC_C,   // inCond
    PCC_NONE // outCond
    ,
    asm_F,
    NULL,
    0,
    0};

static pCodeInstruction pciJNC = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_JNC,
    "JNC",
    "JNC",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    1,    // num ops
    0,
    0, // dest, bit instruction
    1,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    PCC_C,   // inCond
    PCC_NONE // outCond
    ,
    asm_F,
    NULL,
    0,
    0};

static pCodeInstruction pciJNZ = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_JNZ,
    "JNZ",
    "JNZ",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    1,    // num ops
    0,
    0, // dest, bit instruction
    1,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    PCC_Z,   // inCond
    PCC_NONE // outCond
    ,
    asm_F,
    NULL,
    0,
    0};

static pCodeInstruction pciJZ = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_JZ,
    "JZ",
    "JZ",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    1,    // num ops
    0,
    0, // dest, bit instruction
    1,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    PCC_Z,   // inCond
    PCC_NONE // outCond
    ,
    asm_F,
    NULL,
    0,
    0};
static pCodeInstruction pciJN = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_JN,
    "JN",
    "JN",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    1,    // num ops
    0,
    0, // dest, bit instruction
    1,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    PCC_N,   // inCond
    PCC_NONE // outCond
    ,
    asm_F,
    NULL,
    0,
    0};

static pCodeInstruction pciJNN = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_JNN,
    "JNN",
    "JNN",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    1,    // num ops
    0,
    0, // dest, bit instruction
    1,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    PCC_N,   // inCond
    PCC_NONE // outCond
    ,
    asm_F,
    NULL,
    0,
    0};

static pCodeInstruction pciJO = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_JO,
    "JO",
    "JO",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    1,    // num ops
    0,
    0, // dest, bit instruction
    1,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    PCC_O,   // inCond
    PCC_NONE // outCond
    ,
    asm_F,
    NULL,
    0,
    0};

static pCodeInstruction pciJNO = {
    {PC_OPCODE, 0, 0, 0, NULL, NULL, NULL,
     genericDestruct,
     genericPrint},
    POC_JNO,
    "JNO",
    "JNO",
    NULL, // from branch
    NULL, // to branch
    NULL, // label
    NULL, // operand
    NULL, // flow block
    NULL, // C source
    1,    // num ops
    0,
    0, // dest, bit instruction
    1,
    0,
    0, // branch, skip
    0, // literal operand
    0,
    0,
    0,
    POC_NOP,
    PCC_O,   // inCond
    PCC_NONE // outCond
    ,
    asm_F,
    NULL,
    0,
    0};

/*

static pCodeInstruction pciPAGESEL = {
    {PC_OPCODE, 0,0,0,NULL,NULL,NULL,
        genericDestruct,
        genericPrint},
        POC_PAGESEL,
        "PAGESEL",
        NULL, // from branch
        NULL, // to branch
        NULL, // label
        NULL, // operand
        NULL, // flow block
        NULL, // C source
        1,    // num ops
        0,0,  // dest, bit instruction
        0,0,  // branch, skip
        0,    // literal operand
        POC_NOP,
        PCC_NONE, // inCond
        PCC_NONE  // outCond
};
*/

pCodeInstruction *HY08AMnemonics[MAX_HY08AMNEMONICS];

// static int BlockJMPRET2RET(pBlock *pb);
static int BlockTempBridgeRemove(pBlock *pb);
static int jnnOptimize(pBlock *pb); // special case JNN
static int stkxxPromote(pBlock *pb);
static int stkxxPromote2(pBlock *pb);
static int simpleOptimize(pBlock *pb, int possibleFinal); // save time from peeph
static int dirShiftOptimize(pBlock *pb);
// static int stkxx2Temp(pBlock *pb);

// static int stkxxPromoted = 0;

int labelChangeLine = 1; // if label need change line, global , default is 1
/*-----------------------------------------------------------------*/
/* return a unique ID number to assist pCodes debuging             */
/*-----------------------------------------------------------------*/
static unsigned PCodeID(void)
{
    static unsigned int pcodeId = 1; /* unique ID number to be assigned to all pCodes */

    // static unsigned int stop=0;
    //     if (pcodeId == 1247)
    //   	stop++; // Place break point here

    return pcodeId++;
}

int pCodeInitRegisters(void)
{
    static int initialized = 0;
    int shareBankAddress, stkSize, haveShared;
    HYA_device *hycon;

    if (initialized)
        return 1;

    memset(dynProcessorRegs_map, 0, sizeof(dynProcessorRegs_map));
    memset(dynProcessorRegs_map, 0, sizeof(dynAllocRegs_map));
    memset(dynStackRegs_map, 0, sizeof(dynStackRegs));
    memset(dynDirectRegs_map, 0, sizeof(dynDirectRegs_map));

    hycon = init_hya(port->processor);
    if (!hycon)
        return 0;
    max_rom_addr = hycon->programMemSize;
    initialized = 1;

    haveShared = HY08A_getSharedStack(NULL, &shareBankAddress, &stkSize, !(0)); // generate .. give 0, for larger stack, change later
    /* Set pseudo stack size to SHAREBANKSIZE - 3.
     * On multi memory bank ICs this leaves room for WSAVE/SSAVE/PSAVE
     * (used for interrupts) to fit into the shared portion of the
     * memory bank. */

    stkSize = stkSize - 3;
    assert(stkSize >= 0);
    initStack(shareBankAddress, stkSize, haveShared);

    /* TODO: Read aliases for SFRs from regmap lines in device description. */
    pc_status.r = allocProcessorRegister(IDX_STATUS, "_STATUS", PO_STATUS, 0xf80);
    pc_pclatl.r = allocProcessorRegister(IDX_PCL, "_PCLATL", PO_PCL, 0xf80);
    pc_pclath.r = allocProcessorRegister(IDX_PCLATH, "_PCLATH", PO_PCLATH, 0xf80);
    pc_pclatu.r = allocProcessorRegister(IDX_PCLATU, "_PCLATU", PO_PCLATU, 0xf80);

    pc_fsr0l.r = allocProcessorRegister(IDX_FSR0L, "_FSR0L", PO_FSR, 0xf80);
    pc_fsr0h.r = allocProcessorRegister(IDX_FSR0H, "_FSR0H", PO_FSR, 0xf80);
    pc_indf0.r = allocProcessorRegister(IDX_INDF0, "_INDF0", PO_INDF, 0xf80);
    pc_plusw0.r = allocProcessorRegister(IDX_PLUSW0, "_PLUSW0", PO_PLUSW, 0xf80);
    pc_wreg.r = allocProcessorRegister(IDX_WREG, "_WREG", PO_W, 0xF80);
    pc_poinc0.r = allocProcessorRegister(IDX_POINC, "_POINC0", PO_POINC0, 0xf80); // POINC0 is special one
    pc_princ0.r = allocProcessorRegister(IDX_PRINC, "_PRINC0", PO_PRINC0, 0xf80); // POINC0 is special one
    pc_podec0.r = allocProcessorRegister(IDX_PODEC, "_PODEC0", PO_PODEC0, 0xf80); // POINC0 is special one
    pc_princ2.r = allocProcessorRegister(IDX_PRINC, "_PRINC2", PO_PRINC2, 0xf80); // POINC2 is special one
    pc_podec2.r = allocProcessorRegister(IDX_PODEC, "_PODEC2", PO_PODEC2, 0xf80); // POINC2 is special one
    pc_sspbuf.r = allocProcessorRegister(IDX_SSPBUF, "_SSPBUF", PO_SSPBUF, 0xf80);

    pc_fsr1l.r = allocProcessorRegister(IDX_FSR0L, "_FSR1L", PO_FSR, 0xf80);
    pc_fsr1h.r = allocProcessorRegister(IDX_FSR0H, "_FSR1H", PO_FSR, 0xf80);
    pc_indf1.r = allocProcessorRegister(IDX_INDF0, "_INDF1", PO_INDF, 0xf80);
    pc_plusw1.r = allocProcessorRegister(IDX_PLUSW0, "_PLUSW1", PO_PLUSW, 0xf80); // still use plusw0
    pc_poinc1.r = allocProcessorRegister(IDX_POINC, "_POINC1", PO_POINC, 0xf80);

    pc_fsr2l.r = allocProcessorRegister(IDX_FSR0L, "_FSR2L", PO_FSR, 0xf80);
    pc_fsr2h.r = allocProcessorRegister(IDX_FSR0H, "_FSR2H", PO_FSR, 0xf80);
    pc_indf2.r = allocProcessorRegister(IDX_INDF0, "_INDF2", PO_INDF, 0xf80);
    pc_plusw2.r = allocProcessorRegister(IDX_INDF0, "_PLUSW2", PO_INDF, 0xf80);
    pc_poinc2.r = allocProcessorRegister(IDX_POINC, "_POINC2", PO_POINC, 0xf80);

    pc_inte0.r = allocProcessorRegister(IDX_INTCON, "_INTE0", PO_INTE0, 0xf80); // since it is local
                                                                                /*if (HY08A_getPART()->isEnhancedCore >= 3)
    {
        pc_inte1.r->name[5] = '0';
        pc_inte1.pcop.name = strdup("_INTE0");
                                                                            
    }*/

    pc_prodl.r = allocProcessorRegister(IDX_PRODL, "_PRODL", PO_PRODL, 0xf80);
    pc_prodh.r = allocProcessorRegister(IDX_PRODH, "_PRODH", PO_PRODH, 0xf80);

    pc_tbldl.r = allocProcessorRegister(IDX_TBLD, "_TBLDL", PO_TBLDL, 0xf80);
    pc_tbldh.r = allocProcessorRegister(IDX_TBLD, "_TBLDH", PO_TBLDH, 0xf80);
    pc_tblptrl.r = allocProcessorRegister(IDX_TBLPTR, "_TBLPTRL", PO_TBLPTRL, 0xf80);
    pc_tblptrh.r = allocProcessorRegister(IDX_TBLPTR, "_TBLPTRH", PO_TBLPTRH, 0xf80);

    pc_fsr0ptr.r = allocProcessorRegister(IDX_FSR0PTR, "_FSR0", PO_FSRPTR0, 0xf80);
    pc_fsr0ptr.r->size = 2;
    pc_fsr1ptr.r = allocProcessorRegister(IDX_FSR1PTR, "_FSR1", PO_FSRPTR1, 0xf80);
    pc_fsr1ptr.r->size = 2;
    pc_fsr2ptr.r = allocProcessorRegister(IDX_FSR2PTR, "_FSR2", PO_FSRPTR2, 0xf80);
    pc_fsr2ptr.r->size = 2;

    pc_adcr.r = allocProcessorRegister(IDX_ADCR, "_ADCR", PO_ADCR, 0xf80);
    pc_fsr2ptr.r->size = 4;
    pc_adc1o.r = allocProcessorRegister(IDX_ADCO1, "_ADC1O", PO_ADCO1, 0xf80);
    pc_fsr2ptr.r->size = 4;
    pc_adc2o.r = allocProcessorRegister(IDX_ADCO2, "_ADC2O", PO_ADCO1, 0xf80);
    pc_fsr2ptr.r->size = 4;

    pc_status.rIdx = IDX_STATUS;
    // pc_fsr.rIdx = IDX_FSR;
    pc_fsr0l.rIdx = IDX_FSR0L;
    pc_fsr0h.rIdx = IDX_FSR0H;
    pc_indf0.rIdx = IDX_INDF0;
    pc_poinc0.rIdx = IDX_POINC;
    pc_princ0.rIdx = IDX_PRINC;
    pc_plusw0.rIdx = IDX_PLUSW0;
    pc_inte0.rIdx = IDX_INTCON;
    pc_pclatl.rIdx = IDX_PCL;
    pc_pclath.rIdx = IDX_PCLATH;

    /* Interrupt storage for working register - must be same address in all banks ie section SHAREBANK. */
    // 2020, change to local, named by eachfunction
    pc_wsave.r = allocInternalRegister(IDX_WSAVE, pc_wsave.pcop.name, pc_wsave.pcop.type, hycon ? hycon->bankMask : 0xf80);
    /* Interrupt storage for status register. */
    // pc_ssave.r = allocInternalRegister(IDX_SSAVE,pc_ssave.pcop.name,pc_ssave.pcop.type, (hycon && haveShared) ? hycon->bankMask : 0);
    /* Interrupt storage for pclath register. */
    // pc_psave.r = allocInternalRegister(IDX_PSAVE,pc_psave.pcop.name,pc_psave.pcop.type, (hycon && haveShared) ? hycon->bankMask : 0);

    // pc_wsave.rIdx = pc_wsave.r->rIdx;
    // pc_ssave.rIdx = pc_ssave.r->rIdx;
    // pc_psave.rIdx = pc_psave.r->rIdx;

    // pc_wsave.r->isFixed = 1; /* Some  ICs do not have a sharebank - this register needs to be reserved across all banks. */
    // pc_wsave.r->address = shareBankAddress - stkSize;
    // pc_ssave.r->isFixed = 1; /* This register must be in the first bank. */
    // pc_ssave.r->address = shareBankAddress-stkSize-1;
    // pc_psave.r->isFixed = 1; /* This register must be in the first bank. */
    // pc_psave.r->address = shareBankAddress-stkSize-2;

    /* probably should put this in a separate initialization routine */
    pb_dead_pcodes = newpBlock();
    return 1;
}

/*-----------------------------------------------------------------*/
/*  mnem2key - convert a hycon mnemonic into a hash key              */
/*   (BTW - this spreads the mnemonics quite well)                 */
/*                                                                 */
/*-----------------------------------------------------------------*/

static int mnem2key(unsigned char const *mnem)
{
    int key = 0;

    if (!mnem)
        return 0;

    while (*mnem)
    {

        key += toupper(*mnem++) + 1;
    }

    return (key & 0x1f);
}

static void HY08AinitMnemonics(void)
{
    int i = 0;
    int key;
    //  char *str;
    pCodeInstruction *pci;

    if (mnemonics_initialized)
        return;

    // FIXME - probably should NULL out the array before making the assignments
    // since we check the array contents below this initialization.

    HY08AMnemonics[POC_ADDLW] = &pciADDLW;
    HY08AMnemonics[POC_ADDWF] = &pciADDWF;
    HY08AMnemonics[POC_ADDFW] = &pciADDFW;
    HY08AMnemonics[POC_ANDLW] = &pciANDLW;
    HY08AMnemonics[POC_ANDWF] = &pciANDWF;
    HY08AMnemonics[POC_ANDFW] = &pciANDFW;
    HY08AMnemonics[POC_BCF] = &pciBCF;
    HY08AMnemonics[POC_BSF] = &pciBSF;
    HY08AMnemonics[POC_BTSZ] = &pciBTSZ;
    HY08AMnemonics[POC_BTGF] = &pciBTGF;
    HY08AMnemonics[POC_BTSS] = &pciBTSS;
    HY08AMnemonics[POC_CALL] = &pciCALL;
    HY08AMnemonics[POC_COMF] = &pciCOMF;
    HY08AMnemonics[POC_COMFW] = &pciCOMFW;
    HY08AMnemonics[POC_CLRF] = &pciCLRF;
    // HY08AMnemonics[POC_CLRW] = &pciCLRW;
    HY08AMnemonics[POC_SETF] = &pciSETF;
    HY08AMnemonics[POC_CLRWDT] = &pciCLRWDT;
    HY08AMnemonics[POC_DECF] = &pciDECF;
    HY08AMnemonics[POC_DECFW] = &pciDECFW;
    HY08AMnemonics[POC_DCSZ] = &pciDCSZ;
    HY08AMnemonics[POC_DCSUZ] = &pciDCSUZ;

    HY08AMnemonics[POC_DCSZW] = &pciDCSZW;
    HY08AMnemonics[POC_DCSUZW] = &pciDCSUZW;
    HY08AMnemonics[POC_JMP] = &pciJMP;
    HY08AMnemonics[POC_RJ] = &pciRJ;
    HY08AMnemonics[POC_RCALL] = &pciRCALL;
    HY08AMnemonics[POC_INF] = &pciINF;
    HY08AMnemonics[POC_INFW] = &pciINFW;
    HY08AMnemonics[POC_INSZ] = &pciINSZ;
    HY08AMnemonics[POC_INSZW] = &pciINSZW;
    HY08AMnemonics[POC_INSUZ] = &pciINSUZ;
    HY08AMnemonics[POC_INSUZW] = &pciINSUZW;
    HY08AMnemonics[POC_IORL] = &pciIORL;
    HY08AMnemonics[POC_IORWF] = &pciIORWF;
    HY08AMnemonics[POC_IORFW] = &pciIORFW;
    HY08AMnemonics[POC_MVFW] = &pciMVFW;
    HY08AMnemonics[POC_MVL] = &pciMVL;
    HY08AMnemonics[POC_MVWF] = &pciMVWF;
    HY08AMnemonics[POC_MVWF2] = &pciMVWF2;
    HY08AMnemonics[POC_MVFF] = &pciMVFF;
    HY08AMnemonics[POC_MVSF] = &pciMVSF;
    HY08AMnemonics[POC_MVSS] = &pciMVSS;
    HY08AMnemonics[POC_PUSHL] = &pciPUSHL;

    HY08AMnemonics[POC_NOP] = &pciNOP;
    HY08AMnemonics[POC_RETI] = &pciRETI;
    HY08AMnemonics[POC_RETL] = &pciRETL;
    HY08AMnemonics[POC_RET] = &pciRET;
    HY08AMnemonics[POC_RETV] = &pciRETV;
    HY08AMnemonics[POC_RLF] = &pciRLF;
    HY08AMnemonics[POC_RLFW] = &pciRLFW;
    HY08AMnemonics[POC_RRF] = &pciRRF;
    HY08AMnemonics[POC_RRFW] = &pciRRFW;

    HY08AMnemonics[POC_RLCF] = &pciRLCF;
    HY08AMnemonics[POC_RLCFW] = &pciRLCFW;
    HY08AMnemonics[POC_RRCF] = &pciRRCF;
    HY08AMnemonics[POC_RRCFW] = &pciRRCFW;

    HY08AMnemonics[POC_ARLCF] = &pciARLCF;
    HY08AMnemonics[POC_ARLCFW] = &pciARLCFW;
    HY08AMnemonics[POC_ARRCF] = &pciARRCF;
    HY08AMnemonics[POC_ARRCFW] = &pciARRCFW;

    HY08AMnemonics[POC_SUBL] = &pciSUBL;
    // HY08AMnemonics[POC_SUBF] = &pciSUBWF;
    HY08AMnemonics[POC_SUBFWW] = &pciSUBFWW;
    HY08AMnemonics[POC_SUBFWF] = &pciSUBFWF;
    HY08AMnemonics[POC_SWPF] = &pciSWPF;
    HY08AMnemonics[POC_SWPFW] = &pciSWPFW;
    // HY08AMnemonics[POC_TRIS] = &pciTRIS;
    HY08AMnemonics[POC_XORLW] = &pciXORLW;
    HY08AMnemonics[POC_XORF] = &pciXORF;
    HY08AMnemonics[POC_XORFW] = &pciXORFW;
    HY08AMnemonics[POC_LBSR] = &pciLBSR;
    // HY08AMnemonics[POC_PAGESEL] = &pciPAGESEL;
    HY08AMnemonics[POC_MULWF] = &pciMULWF;
    HY08AMnemonics[POC_MULLW] = &pciMULLW;

    HY08AMnemonics[POC_ADCWF] = &pciADCWF;
    HY08AMnemonics[POC_ADCFW] = &pciADCFW;
    HY08AMnemonics[POC_SBCFWF] = &pciSBCFWF;
    HY08AMnemonics[POC_SBCFWW] = &pciSBCFWW;

    HY08AMnemonics[POC_JC] = &pciJC;
    HY08AMnemonics[POC_JNC] = &pciJNC;
    HY08AMnemonics[POC_JZ] = &pciJZ;
    HY08AMnemonics[POC_JNZ] = &pciJNZ;
    HY08AMnemonics[POC_JO] = &pciJO;
    HY08AMnemonics[POC_JNO] = &pciJNO;
    HY08AMnemonics[POC_JN] = &pciJN;
    HY08AMnemonics[POC_JNN] = &pciJNN;

    HY08AMnemonics[POC_CPSE] = &pciCPSE;
    HY08AMnemonics[POC_CPSG] = &pciCPSG;
    HY08AMnemonics[POC_CPSL] = &pciCPSL;
    HY08AMnemonics[POC_TFSZ] = &pciTFSZ;
    HY08AMnemonics[POC_LDPR] = &pciLDPR;
    HY08AMnemonics[POC_ADDFSR] = &pciADDFSR;
    HY08AMnemonics[POC_TBLR] = &pciTBLR;
    HY08AMnemonics[POC_MVLP] = &pciMVLP;

    for (i = 0; i < MAX_HY08AMNEMONICS; i++)
        if (HY08AMnemonics[i])
        {
            hTabAddItem(&HY08AMnemonicsHash, mnem2key((unsigned char *)HY08AMnemonics[i]->mnemonic), HY08AMnemonics[i]);
            hTabAddItem(&HY08AMnemonicsHash, mnem2key((unsigned char *)HY08AMnemonics[i]->mne2), HY08AMnemonics[i]);
            // hTabAddItem(&HY08AMnemonicsHashAlt, mnem2key((unsigned char *)HY08AMnemonics[i]->mne2), HY08AMnemonics[i]);
        }
    pci = hTabFirstItem(HY08AMnemonicsHash, &key);

    while (pci)
    {
        DFPRINTF((stderr, "element %d key %d, mnem %s\n", i++, key, pci->mnemonic));
        pci = hTabNextItem(HY08AMnemonicsHash, &key);
    }

    mnemonics_initialized = 1;
}

int getpCode(char *mnem, unsigned dest, int *specialop)
{

    pCodeInstruction *pci;
    int key;
    int j = strlen(mnem);

    *specialop = 0;
    if (mnem[j - 1] == '@')
    {
        mnem[j - 1] = '\0';
        *specialop = 1;
    }

    key = mnem2key((unsigned char *)mnem);

    if (!mnemonics_initialized)
        HY08AinitMnemonics();

    pci = hTabFirstItemWK(HY08AMnemonicsHash, key);

    while (pci)
    {

        if (STRCASECMP(pci->mnemonic, mnem) == 0 || STRCASECMP(pci->mne2, mnem) == 0)
        {
            // if((pci->num_ops <= 1) || (pci->isModReg == dest) || (pci->isBitInst))
            return (pci->op);
        }

        pci = hTabNextItemWK(HY08AMnemonicsHash);
    }

    return -1;
}

/*-----------------------------------------------------------------*
 * HY08AinitpCodePeepCommands
 *
 *-----------------------------------------------------------------*/
void HY08AinitpCodePeepCommands(void)
{

    int key, i;
    peepCommand *pcmd;

    i = 0;
    do
    {
        hTabAddItem(&HY08ApCodePeepCommandsHash,
                    mnem2key((unsigned char *)peepCommands[i].cmd), &peepCommands[i]);
        i++;
    } while (peepCommands[i].cmd);

    pcmd = hTabFirstItem(HY08ApCodePeepCommandsHash, &key);

    while (pcmd)
    {
        // fprintf(stderr, "peep command %s  key %d\n",pcmd->cmd,pcmd->id);
        pcmd = hTabNextItem(HY08ApCodePeepCommandsHash, &key);
    }
}

/*-----------------------------------------------------------------
 *
 *
 *-----------------------------------------------------------------*/

int getpCodePeepCommand(char *cmd)
{

    peepCommand *pcmd;
    int key = mnem2key((unsigned char *)cmd);

    pcmd = hTabFirstItemWK(HY08ApCodePeepCommandsHash, key);

    while (pcmd)
    {
        // fprintf(stderr," comparing %s to %s\n",pcmd->cmd,cmd);
        if (STRCASECMP(pcmd->cmd, cmd) == 0)
        {
            return pcmd->id;
        }

        pcmd = hTabNextItemWK(HY08ApCodePeepCommandsHash);
    }
    // if not found we try skip 1

    return -1;
}

static char getpBlock_dbName(pBlock *pb)
{
    if (!pb)
        return 0;

    if (pb->cmemmap)
        return pb->cmemmap->dbName;

    return pb->dbName;
}

void pBlockConvert2ISR(pBlock *pb)
{
    if (!pb)
        return;

    if (pb->cmemmap)
        pb->cmemmap = NULL;

    pb->dbName = 'I';
}

// 2016 Nov, varopt related

varopt *newVarOpt(reg_info *src, reg_info *dst, int dstoffset, int lit, int justremove)
{
    //	char buf[1024];
    varopt *vop = Safe_calloc(1, sizeof(varopt));
    vop->source_reg = src;
    vop->dest_reg = dst;
    vop->dstOffset = dstoffset;
    vop->lit = lit;
    vop->sta_remove = justremove;
    return vop;
}
varopt *checkVarOptDouble(set *sset, varopt *vp)
{
    varopt *vp1;
    if (!vp)
        return NULL;
    for (vp1 = setFirstItem(sset); vp1; setNextItem(sset))
    {
        if (!memcmp(vp1, vp, sizeof(varopt)))
        {
            return vp1;
        }
    }
    return NULL;
}

/*-----------------------------------------------------------------*/
/* movepBlock2Head - given the dbname of a pBlock, move all        */
/*                   instances to the front of the doubly linked   */
/*                   list of pBlocks                               */
/*-----------------------------------------------------------------*/

void movepBlock2Head(char dbName)
{
    pBlock *pb;

    if (!the_pFile)
        return;

    pb = the_pFile->pbHead;

    while (pb)
    {

        if (getpBlock_dbName(pb) == dbName)
        {
            pBlock *pbn = pb->next;
            pb->next = the_pFile->pbHead;
            the_pFile->pbHead->prev = pb;
            the_pFile->pbHead = pb;

            if (pb->prev)
                pb->prev->next = pbn;

            // If the pBlock that we just moved was the last
            // one in the link of all of the pBlocks, then we
            // need to point the tail to the block just before
            // the one we moved.
            // Note: if pb->next is NULL, then pb must have
            // been the last pBlock in the chain.

            if (pbn)
                pbn->prev = pb->prev;
            else
                the_pFile->pbTail = pb->prev;

            pb = pbn;
        }
        else
            pb = pb->next;
    }
}

void copypCode(FILE *of, char dbName)
{
    pBlock *pb;

    if (!of || !the_pFile)
        return;

    for (pb = the_pFile->pbHead; pb; pb = pb->next)
    {
        if (getpBlock_dbName(pb) == dbName)
        {
            pBlockStats(of, pb);
            printpBlock(of, pb);
            fprintf(of, "\n");
        }
    }
}

void resetpCodeStatistics(void)
{
    pcode_insns = pcode_doubles = 0;
}

void dumppCodeStatistics(FILE *of)
{
    /* dump statistics */
    fprintf(of, "\n");
    fprintf(of, ";\tcode size estimation:\n");
    fprintf(of, ";\t%5u+%5u = %5u instructions (%5u byte)\n", pcode_insns, pcode_doubles, pcode_insns + pcode_doubles, 2 * (pcode_insns + 2 * pcode_doubles));
    fprintf(of, "\n");
}

void pcode_test(void)
{

    DFPRINTF((stderr, "pcode is alive!\n"));

    // initMnemonics();

    if (the_pFile)
    {

        pBlock *pb;
        FILE *pFile;
        char buffer[100];

        /* create the file name */
        strcpy(buffer, dstFileName);
        strcat(buffer, ".p");

        if (!(pFile = fopen(buffer, "w")))
        {
            werror(E_FILE_OPEN_ERR, buffer);
            exit(1);
        }

        fprintf(pFile, "pcode dump\n\n");

        for (pb = the_pFile->pbHead; pb; pb = pb->next)
        {
            fprintf(pFile, "\n\tNew pBlock\n\n");
            if (pb->cmemmap)
                fprintf(pFile, "%s", pb->cmemmap->sname);
            else
                fprintf(pFile, "internal pblock");

            fprintf(pFile, ", dbName =%c\n", getpBlock_dbName(pb));
            printpBlock(pFile, pb);
        }
    }
}

/*-----------------------------------------------------------------*/
/* int RegCond(pCodeOp *pcop) - if pcop points to the STATUS reg-  */
/*      ister, RegCond will return the bit being referenced.       */
/*                                                                 */
/* fixme - why not just OR in the pcop bit field                   */
/*-----------------------------------------------------------------*/

static int RegCond(pCodeOp *pcop)
{

    if (!pcop)
        return 0;

    if (pcop->type == PO_GPR_BIT)
    {
        char *name = pcop->name;
        if (!name)
            name = PCORB(pcop)->pcor.pcop.name;

        if ((!name && PCORB(pcop)->pcor.rIdx == IDX_STATUS) || (name && strcmp(name, pc_status.pcop.name) == 0))
        {
            switch (PCORB(pcop)->bit)
            {
            case HYA_C_BIT:
                pcop->flags.isC = 1;
                return PCC_C;
            case HYA_DC_BIT:
                pcop->flags.isDC = 1;
                return PCC_DC;
            case HYA_Z_BIT:
                pcop->flags.isZ = 1;
                return PCC_Z;
            case HYA_OV_BIT:
                pcop->flags.isOV = 1;
                return PCC_O;
            case HYA_N_BIT:
                pcop->flags.isN = 1;
                return PCC_N;
            default:
                return 0;
            }
        }
    }
    if (pcop->type == PO_W)
        return PCC_W;

    return 0;
}

/*-----------------------------------------------------------------*/
/* newpCode - create and return a newly initialized pCode          */
/*                                                                 */
/*  fixme - rename this                                            */
/*                                                                 */
/* The purpose of this routine is to create a new Instruction      */
/* pCode. This is called by gen.c while the assembly code is being */
/* generated.                                                      */
/*                                                                 */
/* Inouts:                                                         */
/*  HYA_OPCODE op - the assembly instruction we wish to create.    */
/*                  (note that the op is analogous to but not the  */
/*                  same thing as the opcode of the instruction.)  */
/*  pCdoeOp *pcop - pointer to the operand of the instruction.     */
/*                                                                 */
/* Outputs:                                                        */
/*  a pointer to the new malloc'd pCode is returned.               */
/*                                                                 */
/*                                                                 */
/*                                                                 */
/*-----------------------------------------------------------------*/
pCode *newpCode(HYA_OPCODE op, pCodeOp *pcop)
{
    pCodeInstruction *pci;

    if (!mnemonics_initialized)
        HY08AinitMnemonics();

    pci = Safe_calloc(1, sizeof(pCodeInstruction));

    /*if (op == POC_LDPR)
    {
        fprintf(stderr, "LDPR\n");
    }*/

    if ((op >= 0) && (op < MAX_HY08AMNEMONICS) && HY08AMnemonics[op])
    {
        memcpy(pci, HY08AMnemonics[op], sizeof(pCodeInstruction));
        pci->pc.id = PCodeID();
        pci->pcop = pcop;

        if (pci->inCond & PCC_EXAMINE_PCOP)
        {
            pci->inCond |= RegCond(pcop);
            // bcf/bsf special process
            if (pci->op == POC_BCF || pci->op == POC_BSF)
            {
                pci->inCond &= ~(PCC_Z | PCC_C | PCC_O | PCC_N | PCC_DC); // only at outcond
            }
        }

        if (pci->outCond & PCC_EXAMINE_PCOP)
            pci->outCond |= RegCond(pcop);

        pci->pc.prev = pci->pc.next = NULL;
        return (pCode *)pci;
    }

    fprintf(stderr, "pCode mnemonic error %s,%d\n", __FUNCTION__, __LINE__);
    exit(1);

    return NULL;
}
pCode *newpCode2(HYA_OPCODE op, pCodeOp *pcop, pCodeOp *pcop2)
{
    pCodeInstruction *pci;

    if (!mnemonics_initialized)
        HY08AinitMnemonics();

    pci = Safe_calloc(1, sizeof(pCodeInstruction));

    if ((op >= 0) && (op < MAX_HY08AMNEMONICS) && HY08AMnemonics[op])
    {
        memcpy(pci, HY08AMnemonics[op], sizeof(pCodeInstruction));
        pci->pc.id = PCodeID();
        pci->pcop = pcop;
        pci->pcop2 = pcop2;

        if (pci->inCond & PCC_EXAMINE_PCOP)
            pci->inCond |= RegCond(pcop);

        if (pci->outCond & PCC_EXAMINE_PCOP)
            pci->outCond |= RegCond(pcop2);

        pci->pc.prev = pci->pc.next = NULL;
        return (pCode *)pci;
    }

    fprintf(stderr, "pCode mnemonic error %s,%d\n", __FUNCTION__, __LINE__);
    exit(1);

    return NULL;
}
/*-----------------------------------------------------------------*/
/* newpCodeWild - create a "wild" as in wild card pCode            */
/*                                                                 */
/* Wild pcodes are used during the peep hole optimizer to serve    */
/* as place holders for any instruction. When a snippet of code is */
/* compared to a peep hole rule, the wild card opcode will match   */
/* any instruction. However, the optional operand and label are    */
/* additional qualifiers that must also be matched before the      */
/* line (of assembly code) is declared matched. Note that the      */
/* operand may be wild too.                                        */
/*                                                                 */
/*   Note, a wild instruction is specified just like a wild var:   */
/*      %4     ; A wild instruction,                               */
/*  See the peeph.def file for additional examples                 */
/*                                                                 */
/*-----------------------------------------------------------------*/

pCode *newpCodeWild(int pCodeID, pCodeOp *optional_operand, pCodeOp *optional_label)
{

    pCodeWild *pcw;

    pcw = Safe_calloc(1, sizeof(pCodeWild));

    pcw->pci.pc.type = PC_WILD;
    pcw->pci.pc.prev = pcw->pci.pc.next = NULL;
    pcw->id = PCodeID();
    pcw->pci.from = pcw->pci.to = pcw->pci.label = NULL;
    pcw->pci.pc.pb = NULL;

    pcw->pci.pc.destruct = genericDestruct;
    pcw->pci.pc.print = genericPrint;

    pcw->id = pCodeID; // this is the 'n' in %n
    pcw->operand = optional_operand;
    pcw->label = optional_label;

    pcw->mustBeBitSkipInst = 0;
    pcw->mustNotBeBitSkipInst = 0;
    pcw->invertBitSkipInst = 0;
    pcw->labelOK = 0;

    return ((pCode *)pcw);
}

/*-----------------------------------------------------------------*/
/* newPcodeCharP - create a new pCode from a char string           */
/*-----------------------------------------------------------------*/

pCode *newpCodeCharP(char *cP)
{

    pCodeComment *pcc;

    pcc = Safe_calloc(1, sizeof(pCodeComment));

    pcc->pc.type = PC_COMMENT;
    pcc->pc.prev = pcc->pc.next = NULL;
    pcc->pc.id = PCodeID();
    // pcc->pc.from = pcc->pc.to = pcc->pc.label = NULL;
    pcc->pc.pb = NULL;

    pcc->pc.destruct = genericDestruct;
    pcc->pc.print = genericPrint;

    if (cP)
        pcc->comment = Safe_strdup(cP);
    else
        pcc->comment = NULL;

    return ((pCode *)pcc);
}

/*-----------------------------------------------------------------*/
/* newpCodeFunction -                                              */
/*-----------------------------------------------------------------*/

pCode *newpCodeFunction(char *mod, char *f, int isPublic, symbol *symp)
{
    pCodeFunction *pcf;

    pcf = Safe_calloc(1, sizeof(pCodeFunction));
    //_ALLOC(pcf,sizeof(pCodeFunction));

    pcf->pc.type = PC_FUNCTION;
    pcf->pc.prev = pcf->pc.next = NULL;
    pcf->pc.id = PCodeID();
    // pcf->pc.from = pcf->pc.to = pcf->pc.label = NULL;
    pcf->pc.pb = NULL;

    pcf->pc.destruct = genericDestruct;
    pcf->pc.print = pCodePrintFunction;
    pcf->symp = symp;

    pcf->ncalled = 0;

    if (mod)
    {
        //_ALLOC_ATOMIC(pcf->modname,strlen(mod)+1);
        pcf->modname = Safe_calloc(1, strlen(mod) + 1);
        strcpy(pcf->modname, mod);
    }
    else
        pcf->modname = NULL;

    if (f)
    {
        //_ALLOC_ATOMIC(pcf->fname,strlen(f)+1);
        pcf->fname = Safe_calloc(1, strlen(f) + 1);
        strcpy(pcf->fname, f);
    }
    else
        pcf->fname = NULL;

    pcf->isPublic = (unsigned)isPublic;

    return ((pCode *)pcf);
}

/*-----------------------------------------------------------------*/
/* newpCodeFlow                                                    */
/*-----------------------------------------------------------------*/
// static void destructpCodeFlow(pCode *pc)
//{
//     if(!pc || !isPCFL(pc))
//         return;
//
//     /*
//     if(PCFL(pc)->from)
//     if(PCFL(pc)->to)
//     */
//     unlinkpCode(pc);
//
//     deleteSet(&PCFL(pc)->registers);
//     deleteSet(&PCFL(pc)->from);
//     deleteSet(&PCFL(pc)->to);
//     free(pc);
//
// }

// static pCode *newpCodeFlow(void )
//{
//     pCodeFlow *pcflow;
//
//     //_ALLOC(pcflow,sizeof(pCodeFlow));
//     pcflow = Safe_calloc(1,sizeof(pCodeFlow));
//
//     pcflow->pc.type = PC_FLOW;
//     pcflow->pc.prev = pcflow->pc.next = NULL;
//     pcflow->pc.pb = NULL;
//
//     pcflow->pc.destruct = destructpCodeFlow;
//     pcflow->pc.print = genericPrint;
//
//     pcflow->pc.seq = GpcFlowSeq++;
//
//     pcflow->from = pcflow->to = NULL;
//
//     pcflow->inCond = PCC_NONE;
//     pcflow->outCond = PCC_NONE;
//
//     pcflow->firstBank = 'U'; /* Undetermined */
//     pcflow->lastBank = 'U'; /* Undetermined */
//
//     pcflow->FromConflicts = 0;
//     pcflow->ToConflicts = 0;
//
//     pcflow->end = NULL;
//
//     pcflow->registers = newSet();
//
//     return ( (pCode *)pcflow);
//
// }

/*-----------------------------------------------------------------*/
/*-----------------------------------------------------------------*/
// static pCodeFlowLink *newpCodeFlowLink(pCodeFlow *pcflow)
//{
//     pCodeFlowLink *pcflowLink;
//
//     pcflowLink = Safe_calloc(1,sizeof(pCodeFlowLink));
//
//     pcflowLink->pcflow = pcflow;
//     pcflowLink->bank_conflict = 0;
//
//     return pcflowLink;
// }

/*-----------------------------------------------------------------*/
/* newpCodeCSource - create a new pCode Source Symbol              */
/*-----------------------------------------------------------------*/

pCode *newpCodeCSource(int ln, char *f, const char *l, int seq)
{

    pCodeCSource *pccs;

    pccs = Safe_calloc(1, sizeof(pCodeCSource));

    pccs->pc.type = PC_CSOURCE;
    pccs->pc.prev = pccs->pc.next = NULL;
    pccs->pc.id = PCodeID();
    pccs->pc.pb = NULL;

    pccs->pc.destruct = genericDestruct;
    pccs->pc.print = genericPrint;

    pccs->line_number = ln;
    pccs->seq_number = seq;
    if (l)
        pccs->line = Safe_strdup(l);
    else
        pccs->line = NULL;

    if (f)
        pccs->file_name = Safe_strdup(f);
    else
        pccs->file_name = NULL;

    return ((pCode *)pccs);
}

/*******************************************************************/
/* newpCodeAsmDir - create a new pCode Assembler Directive   */
/*                        added by VR 6-Jun-2003                   */
/*******************************************************************/

pCode *newpCodeAsmDir(char *asdir, char *argfmt, ...)
{
    pCodeAsmDir *pcad;
    va_list ap;
    char buffer[512];
    char *lbp = buffer;

    pcad = Safe_calloc(1, sizeof(pCodeAsmDir));
    pcad->pci.pc.type = PC_ASMDIR;
    pcad->pci.pc.prev = pcad->pci.pc.next = NULL;
    pcad->pci.pc.pb = NULL;
    pcad->pci.pc.destruct = genericDestruct;
    pcad->pci.pc.print = genericPrint;

    if (asdir && *asdir)
    {

        while (isspace((unsigned char)*asdir))
            asdir++; // strip any white space from the beginning

        pcad->directive = Safe_strdup(asdir);
    }

    va_start(ap, argfmt);

    memset(buffer, 0, sizeof(buffer));
    if (argfmt && *argfmt)
        vsprintf(buffer, argfmt, ap);

    va_end(ap);

    while (isspace((unsigned char)*lbp))
        lbp++;

    if (lbp && *lbp)
        pcad->arg = Safe_strdup(lbp);

    return ((pCode *)pcad);
}

/*-----------------------------------------------------------------*/
/* pCodeLabelDestruct - free memory used by a label.               */
/*-----------------------------------------------------------------*/
static void pCodeLabelDestruct(pCode *pc)
{

    if (!pc)
        return;

    if ((pc->type == PC_LABEL) && PCL(pc)->label)
        free(PCL(pc)->label);

    free(pc);
}

pCode *newpCodeLabel(char *name, int key)
{

    char *s = buffer;
    pCodeLabel *pcl;

    pcl = Safe_calloc(1, sizeof(pCodeLabel));

    pcl->pc.type = PC_LABEL;
    pcl->pc.prev = pcl->pc.next = NULL;
    pcl->pc.id = PCodeID();
    // pcl->pc.from = pcl->pc.to = pcl->pc.label = NULL;
    pcl->pc.pb = NULL;

    pcl->pc.destruct = pCodeLabelDestruct;
    pcl->pc.print = pCodePrintLabel;

    pcl->key = key;
    pcl->ishead = 0;

    pcl->label = NULL;
    if (key > 0)
    {
        sprintf(s, "_%05d_DS_", key); // _00150_DS_
    }
    else
    {
        char buf[20];
        s = name;

        if (name && strlen(name) == 10 && name[0] == '_' && !strcmp(name + 6, "_DS_"))
        {
            strcpy(buf, name);
            buf[6] = '\0';
            pcl->key = atoi(buf + 1);
        }
    }
    if (s)
        pcl->label = Safe_strdup(s);

    // fprintf(stderr,"newpCodeLabel: key=%d, name=%s\n",key, ((s)?s:""));
    return ((pCode *)pcl);
}

/*-----------------------------------------------------------------*/
/* newpBlock - create and return a pointer to a new pBlock         */
/*-----------------------------------------------------------------*/
static pBlock *newpBlock(void)
{

    pBlock *PpB;

    PpB = Safe_calloc(1, sizeof(pBlock));
    PpB->next = PpB->prev = NULL;

    PpB->function_entries = PpB->function_exits = PpB->function_calls = NULL;
    PpB->tregisters = NULL;
    PpB->txregisters = NULL;
    // PpB->visited = 0;
    PpB->FlowTree = NULL;

    return PpB;
}

/*-----------------------------------------------------------------*/
/* newpCodeChain - create a new chain of pCodes                    */
/*-----------------------------------------------------------------*
 *
 *  This function will create a new pBlock and the pointer to the
 *  pCode that is passed in will be the first pCode in the block.
 *-----------------------------------------------------------------*/

pBlock *newpCodeChain(memmap *cm, char c, pCode *pc)
{

    pBlock *pB = newpBlock();

    pB->pcHead = pB->pcTail = pc;
    pB->cmemmap = cm;
    pB->dbName = c;

    return pB;
}

/*-----------------------------------------------------------------*/
/* newpCodeOpLabel - Create a new label given the key              */
/*  Note, a negative key means that the label is part of wild card */
/*  (and hence a wild card label) used in the pCodePeep            */
/*   optimizations).                                               */
/*-----------------------------------------------------------------*/

pCodeOp *newpCodeOpLabel(char *name, int key)
{
    char *s = NULL;
    static int label_key = -1;

    pCodeOp *pcop;

    pcop = Safe_calloc(1, sizeof(pCodeOpLabel));
    pcop->type = PO_LABEL;

    pcop->name = NULL;

    if (key > 0)
        sprintf(s = buffer, "_%05d_DS_", key);
    else
        s = name, key = label_key--;

    PCOLAB(pcop)->offset = 0;
    if (s)
        pcop->name = Safe_strdup(s);

    ((pCodeOpLabel *)pcop)->key = key;

    // fprintf(stderr,"newpCodeOpLabel: key=%d, name=%s\n",key,((s)?s:""));
    return pcop;
}

/*-----------------------------------------------------------------*/
/*-----------------------------------------------------------------*/
pCodeOp *newpCodeOpLit(int lit)
{
    char *s = buffer;
    pCodeOp *pcop;

    pcop = Safe_calloc(1, sizeof(pCodeOpLit));
    pcop->type = PO_LITERAL;

    pcop->name = NULL;
    if (lit >= 0x100)
    {
        sprintf(s, "0x%04x", (unsigned int)lit);
        if (s)
            pcop->name = Safe_strdup(s);
    }
    else if (lit >= 0)
    {
        sprintf(s, "0x%02x", (unsigned int)lit);
        if (s)
            pcop->name = Safe_strdup(s);
    }

    ((pCodeOpLit *)pcop)->lit = (int)lit;

    return pcop;
}

pCodeOp *newpCodeOpLitSign(int lit)
{
    char *s = buffer;
    pCodeOp *pcop;

    pcop = Safe_calloc(1, sizeof(pCodeOpLit));
    pcop->type = PO_LITERAL;

    pcop->name = NULL;
    sprintf(s, "%d", (unsigned int)lit);
    if (s)
        pcop->name = Safe_strdup(s);

    ((pCodeOpLit *)pcop)->lit = (int)lit;

    return pcop;
}

/*-----------------------------------------------------------------*/
/*-----------------------------------------------------------------*/

pCodeOp *newpCodeOpImmd(char *name, int offset, int index, int code_space, int x_space, int is_func, int size, pBlock *pb)
{
    pCodeOp *pcop;

    pcop = Safe_calloc(1, sizeof(pCodeOpImmd));
    pcop->type = PO_IMMEDIATE;
    if (name)
    {
        reg_info *r = NULL;
        pcop->name = Safe_strdup(name);

        if (!is_func)
            r = dirregWithName(name);

        PCOI(pcop)->r = r;
        if (r)
        {
            // fprintf(stderr, " newpCodeOpImmd reg %s exists\n",name);
            PCOI(pcop)->rIdx = r->rIdx;
            if (pb)
            {
                if (!x_space && !isinSet(pb->tregisters, r))
                {
                    addSet(&pb->tregisters, r);
                }
                if (x_space && !isinSet(pb->txregisters, r))
                {
                    addSet(&pb->txregisters, r);
                }
            }
        }
        else if (!is_func)
        {
            // fprintf(stderr, " newpCodeOpImmd reg %s doesn't exist\n",name);
            //  we add it

            r = allocRegByName(name, size, x_space);
            PCOI(pcop)->r = r;
            PCOI(pcop)->rIdx = r->rIdx;
            if (x_space)
                r->pc_type = PO_XDATA;
            if (pb)
            {
                if (x_space)
                    addSet(&pb->txregisters, r);
                else
                    addSet(&pb->tregisters, r);
                // change to DIR?
            }

            // PCOI(pcop)->rIdx = -1;
        }
        // fprintf(stderr,"%s %s %d\n",__FUNCTION__,name,offset);
    }
    else
    {
        pcop->name = NULL;
    }

    PCOI(pcop)->index = index;
    PCOI(pcop)->offset = offset;
    PCOI(pcop)->_const = code_space;
    PCOI(pcop)->_function = is_func;
    PCOI(pcop)->_inX = x_space;

    return pcop;
}

/*-----------------------------------------------------------------*/
/*-----------------------------------------------------------------*/
pCodeOp *newpCodeOpWild(int id, pCodeWildBlock *pcwb, pCodeOp *subtype)
{
    char *s = buffer;
    pCodeOp *pcop;

    if (!pcwb || !subtype)
    {
        fprintf(stderr, "Wild opcode declaration error: %s-%d\n", __FILE__, __LINE__);
        exit(1);
    }

    pcop = Safe_calloc(1, sizeof(pCodeOpWild));
    pcop->type = PO_WILD;
    sprintf(s, "%%%d", id);
    pcop->name = Safe_strdup(s);

    PCOW(pcop)->id = id;
    PCOW(pcop)->pcwb = pcwb;
    PCOW(pcop)->subtype = subtype;
    PCOW(pcop)->matched = NULL;

    return pcop;
}
/*-----------------------------------------------------------------*/
/* Find a symbol with matching name                                */
/*-----------------------------------------------------------------*/
static symbol *symFindWithName(memmap *map, const char *name)
{
    symbol *sym;

    for (sym = setFirstItem(map->syms); sym; sym = setNextItem(map->syms))
    {
        if ((strcmp(sym->rname, name) == 0))
            return sym;
    }
    return 0;
}

/*-----------------------------------------------------------------*/

pCodeOp *newpCodeOpBitByOp(pCodeOp *pcop, int bit, int inBitSpace)
{
    if (pcop->str && strlen(pcop->str) > 0)
        return newpCodeOpBit(pcop->str, bit, inBitSpace);
    return newpCodeOpBit(pcop->name, bit, inBitSpace);
}

/*-----------------------------------------------------------------*/
pCodeOp *newpCodeOpBit(char *name, int ibit, int inBitSpace)
{
    pCodeOp *pcop;
    struct reg_info *r = 0;

    // #ifdef _DEBUG
    //	if (name==NULL || strcmp(name, "_STATUS"))
    //	{
    //		fprintf(stderr, "bit not status now\n");
    //	}
    // #endif

    pcop = Safe_calloc(1, sizeof(pCodeOpRegBit));
    pcop->type = PO_GPR_BIT;

    PCORB(pcop)->bit = ibit;
    PCORB(pcop)->inBitSpace = inBitSpace;
    if (name)
        r = regFindWithName(name);
    if (name && !r)
    {
        // Register has not been allocated - check for symbol information
        symbol *sym;
        sym = symFindWithName(bit, name);
        if (!sym)
            sym = symFindWithName(sfrbit, name);
        if (!sym)
            sym = symFindWithName(sfr, name);
        if (!sym)
            sym = symFindWithName(reg, name);
        // Hack to fix accesses to _INTCON_bits (e.g. GIE=0), see #1579535.
        // XXX: This ignores nesting levels, but works for globals...
        if (!sym)
            sym = findSym(SymbolTab, NULL, name);
        if (!sym && name && name[0] == '_')
            sym = findSym(SymbolTab, NULL, &name[1]);
        if (sym)
        {
            r = allocNewDirReg(sym->etype, name, sym->ival);
            if (!strcmp(name, "_PLUSW0"))
            {
                r->rIdx = IDX_PLUSW0;                
            }
        }
    }
    if (r)
    {
        pcop->name = NULL;
        PCOR(pcop)->r = r;
        PCOR(pcop)->rIdx = r->rIdx;
    }
    else if (name)
    {
        pcop->name = Safe_strdup(name);
        PCOR(pcop)->r = NULL;
        PCOR(pcop)->rIdx = 0;
    }
    else
    {
        // fprintf(stderr, "Unnamed register duplicated for bit-access?!? Hope for the best ...\n");
    }
    if(name)
    {
        if(!strcmp(name,"_WREG"))
            PCORB(pcop)->subtype = PO_W;
        if(!strncmp(name,"_INDF",4))
            PCORB(pcop)->subtype = PO_INDF;
        if(!strcmp(name,"_STATUS"))
            PCORB(pcop)->subtype = PO_STATUS;
        if (!strcmp(name, "_PLUSW0"))
            PCORB(pcop)->subtype = PO_PLUSW;
        if (!strcmp(name, "_POINC0"))
            PCORB(pcop)->subtype = PO_POINC0;

    }
    return pcop;
}

/*-----------------------------------------------------------------*
 * pCodeOp *newpCodeOpReg(int rIdx) - allocate a new register
 *
 * If rIdx >=0 then a specific register from the set of registers
 * will be selected. If rIdx <0, then a new register will be searched
 * for.
 *-----------------------------------------------------------------*/

pCodeOp *newpCodeOpReg(int rIdx)
{
    pCodeOp *pcop;

    pcop = Safe_calloc(1, sizeof(pCodeOpReg));

    pcop->name = NULL;

    if (rIdx >= 0)
    {
        PCOR(pcop)->rIdx = rIdx;
        PCOR(pcop)->r = HY08A_regWithIdx(rIdx);
    }
    else
    {
        PCOR(pcop)->r = HY08A_findFreeReg(REG_GPR);

        if (PCOR(pcop)->r)
            PCOR(pcop)->rIdx = PCOR(pcop)->r->rIdx;
    }

    if (PCOR(pcop)->r)
        pcop->type = PCOR(pcop)->r->pc_type;

    return pcop;
}

pCodeOp *newpCodeOpRegFromStr(char *name)
{
    pCodeOp *pcop;

    pcop = Safe_calloc(1, sizeof(pCodeOpReg));
    PCOR(pcop)->r = allocRegByName(name, 1, 0); // assume not X
    PCOR(pcop)->rIdx = PCOR(pcop)->r->rIdx;
    pcop->type = PCOR(pcop)->r->pc_type;
    pcop->name = PCOR(pcop)->r->name;

    return pcop;
}

static pCodeOp *newpCodeOpStr(char *name)
{
    pCodeOp *pcop;

    pcop = Safe_calloc(1, sizeof(pCodeOpStr));
    pcop->type = PO_STR;
    pcop->name = Safe_strdup(name);

    PCOS(pcop)->isPublic = 0;

    return pcop;
}

/*-----------------------------------------------------------------*/
/*-----------------------------------------------------------------*/

pCodeOp *newpCodeOp(char *name, HYA_OPTYPE type)
{
    pCodeOp *pcop;

    switch (type)
    {
    case PO_BIT:
    case PO_GPR_BIT:
        pcop = newpCodeOpBit(name, -1, 0);
        break;

    case PO_LITERAL:
        pcop = newpCodeOpLit(-1);
        break;

    case PO_LABEL:
        pcop = newpCodeOpLabel(NULL, -1);
        break;

    case PO_GPR_TEMP:
        pcop = newpCodeOpReg(-1);
        break;

    case PO_GPR_POINTER:
    case PO_GPR_REGISTER:
        if (name)
            pcop = newpCodeOpRegFromStr(name);
        else
            pcop = newpCodeOpReg(-1);
        break;

    case PO_STR:
        pcop = newpCodeOpStr(name);
        break;

    default:
        pcop = Safe_calloc(1, sizeof(pCodeOpReg)); // extend to REG size!!
        pcop->type = type;
        if (name)
            pcop->name = Safe_strdup(name);
        else
            pcop->name = NULL;
    }

    return pcop;
}

/*-----------------------------------------------------------------*/
/* addpCode2pBlock - place the pCode into the pBlock linked list   */
/*-----------------------------------------------------------------*/
void addpCode2pBlock(pBlock *pb, pCode *pc)
{

    if (!pb || !pc)
        return;

    if (!pb->pcHead)
    {
        /* If this is the first pcode to be added to a block that
         * was initialized with a NULL pcode, then go ahead and
         * make this pcode the head and tail */
        pb->pcHead = pb->pcTail = pc;
    }
    else
    {
        //    if(pb->pcTail)
        pb->pcTail->next = pc;

        pc->prev = pb->pcTail;
        pc->pb = pb;

        pb->pcTail = pc;
    }
}

/*-----------------------------------------------------------------*/
/* addpBlock - place a pBlock into the pFile                       */
/*-----------------------------------------------------------------*/
void addpBlock(pBlock *pb)
{
    // fprintf(stderr," Adding pBlock: dbName =%c\n",getpBlock_dbName(pb));

    if (!the_pFile)
    {
        /* First time called, we'll pass through here. */
        //_ALLOC(the_pFile,sizeof(pFile));
        the_pFile = Safe_calloc(1, sizeof(pFile));
        the_pFile->pbHead = the_pFile->pbTail = pb;
        the_pFile->functions = NULL;
        return;
    }

    the_pFile->pbTail->next = pb;
    pb->prev = the_pFile->pbTail;
    pb->next = NULL;
    the_pFile->pbTail = pb;
}

/*-----------------------------------------------------------------*/
/* removepBlock - remove a pBlock from the pFile                   */
/*-----------------------------------------------------------------*/
static void removepBlock(pBlock *pb)
{
    pBlock *pbs;

    if (!the_pFile)
        return;

    // fprintf(stderr," Removing pBlock: dbName =%c\n",getpBlock_dbName(pb));

    for (pbs = the_pFile->pbHead; pbs; pbs = pbs->next)
    {
        if (pbs == pb)
        {

            if (pbs == the_pFile->pbHead)
                the_pFile->pbHead = pbs->next;

            if (pbs == the_pFile->pbTail)
                the_pFile->pbTail = pbs->prev;

            if (pbs->next)
                pbs->next->prev = pbs->prev;

            if (pbs->prev)
                pbs->prev->next = pbs->next;

            return;
        }
    }

    fprintf(stderr, "Warning: call to %s:%s didn't find pBlock\n", __FILE__, __FUNCTION__);
}

/*-----------------------------------------------------------------*/
/* printpCode - write the contents of a pCode to a file            */
/*-----------------------------------------------------------------*/

void printpCode(FILE *of, pCode *pc)
{

    if (!pc || !of)
        return;

    if (pc->print)
    {
        pc->print(of, pc);
        return;
    }

    fprintf(of, "warning - unable to print pCode\n");
}

/*-----------------------------------------------------------------*/
/* printpBlock - write the contents of a pBlock to a file          */
/*-----------------------------------------------------------------*/
void printpBlock(FILE *of, pBlock *pb)
{
    pCode *pc;

    if (!pb)
        return;

    if (!of)
        of = stderr;

    for (pc = pb->pcHead; pc; pc = pc->next)
    {
        printpCode(of, pc);

        if (isPCI(pc))
        {
            // if (isPCI(pc) && (PCI(pc)->op == POC_PAGESEL || PCI(pc)->op == POC_LBSR)) {
            if (isPCI(pc) && (PCI(pc)->op == POC_LBSR))
            {
                pcode_doubles++;
            }
            else
            {
                pcode_insns++;
            }
        }
    } // for
    // after printed, we add a mark
    if ((0))
        fprintf(of, ";\t.ENDFUNC");
    else
        fprintf(of, "\t.ENDFUNC");
    pc = setFirstItem(pb->function_entries);
    if (!pc)
        fprintf(of, "\n");
    else
        fprintf(of, "\t%s\n", PCF(pc)->fname);
    fprintf(of, ";--------------------------------------------------------\n");
}

/*-----------------------------------------------------------------*/
/*                                                                 */
/*       pCode processing                                          */
/*                                                                 */
/*                                                                 */
/*                                                                 */
/*-----------------------------------------------------------------*/

void unlinkpCode(pCode *pc)
{

    if (pc)
    {
#ifdef PCODE_DEBUG
        fprintf(stderr, "Unlinking: ");
        printpCode(stderr, pc);
#endif
        if (pc->prev)
            pc->prev->next = pc->next;

        if (pc->next)
            pc->next->prev = pc->prev;
        if (pc->pb->pcHead == pc)
            pc->pb->pcHead = pc->next;
        if (pc->pb->pcTail == pc)
            pc->pb->pcTail = pc->prev;

#if 0
        /* RN: I believe this should be right here, but this did not
         *     cure the bug I was hunting... */
        /* must keep labels -- attach to following instruction */
        if (isPCI(pc) && PCI(pc)->label && pc->next)
        {
            pCodeInstruction *pcnext = PCI(findNextInstruction (pc->next));
            if (pcnext)
            {
                pBranchAppend (pcnext->label, PCI(pc)->label);
            }
        }
#endif
        pc->prev = pc->next = NULL;
    }
}

/*-----------------------------------------------------------------*/
/*-----------------------------------------------------------------*/

static void genericDestruct(pCode *pc)
{

    unlinkpCode(pc);

    if (isPCI(pc))
    {
        /* For instructions, tell the register (if there's one used)
         * that it's no longer needed */
        reg_info *reg = getRegFromInstruction(pc);
        if (reg)
            deleteSetItem(&(reg->reglives.usedpCodes), pc);
    }

    /* Instead of deleting the memory used by this pCode, mark
     * the object as bad so that if there's a pointer to this pCode
     * dangling around somewhere then (hopefully) when the type is
     * checked we'll catch it.
     */

    pc->type = PC_BAD;

    addpCode2pBlock(pb_dead_pcodes, pc);

    // free(pc);
}

/*-----------------------------------------------------------------*/
/*  Copies the pCodeInstruction flow pointer from source pCode     */
/*-----------------------------------------------------------------*/
// static void CopyFlow(pCodeInstruction *pcd, pCode *pcs)
// {
//     pCode *p;
//     pCodeFlow *pcflow = 0;
//     for (p = pcs; p; p = p->prev)
//     {
//         if (isPCI(p))
//         {
//             pcflow = PCI(p)->pcflow;
//             break;
//         }
//         if (isPCF(p))
//         {
//             pcflow = (pCodeFlow *)p;
//             break;
//         }
//     }
//     PCI(pcd)->pcflow = pcflow;
// }

/*-----------------------------------------------------------------*/
/*  pCodeInsertAfter - splice in the pCode chain starting with pc2 */
/*                     into the pCode chain containing pc1         */
/* pc1->next = pc2												*/
/*-----------------------------------------------------------------*/
void pCodeInsertAfter(pCode *pc1, pCode *pc2)
{

    if (!pc1 || !pc2)
        return;

    pc2->next = pc1->next;
    if (pc1->next)
        pc1->next->prev = pc2;

    pc2->pb = pc1->pb;
    pc2->prev = pc1;
    pc1->next = pc2;

    /* If this is an instrution type propogate the flow */
    // if (isPCI(pc2))
    //     CopyFlow(PCI(pc2), pc1);
}

/*------------------------------------------------------------------*/
/*  pCodeInsertBefore - splice in the pCode chain starting with pc2 */
/*                      into the pCode chain containing pc1         */
/*------------------------------------------------------------------*/
void pCodeInsertBefore(pCode *pc1, pCode *pc2)
{

    if (!pc1 || !pc2)
        return;

    pc2->prev = pc1->prev;
    if (pc1->prev)
        pc1->prev->next = pc2;

    pc2->pb = pc1->pb;
    pc2->next = pc1;
    pc1->prev = pc2;

    /* If this is an instrution type propogate the flow */
    // if (isPCI(pc2))
    //     CopyFlow(PCI(pc2), pc1);
}

/*-----------------------------------------------------------------*/
/* pCodeOpCopy - copy a pcode operator                             */
/*-----------------------------------------------------------------*/
pCodeOp *pCodeOpCopy(pCodeOp *pcop)
{
    pCodeOp *pcopnew = NULL;

    if (!pcop)
        return NULL;

    switch (pcop->type)
    {
    case PO_NONE:
    case PO_STR:
        pcopnew = Safe_calloc(1, sizeof(pCodeOp));
        memcpy(pcopnew, pcop, sizeof(pCodeOp));
        break;

    case PO_W:
    case PO_STATUS:
    case PO_FSR:
    case PO_FSR0H:
    case PO_FSR0L:
    case PO_INDF:
    case PO_POINC:
    case PO_SSPBUF:
    case PO_POINC0:
    case PO_PRINC0:
    case PO_PODEC0:
    case PO_PLUSW:

    case PO_PRINC2:
    case PO_PODEC2:

    case PO_INTE0:
    case PO_GPR_REGISTER:
    case PO_GPR_TEMP:
    case PO_GPR_POINTER:
    case PO_SFR_REGISTER:
    case PO_PCL:
    case PO_PCLATH:
    case PO_PCLATU:
    case PO_ADCRB:
    case PO_ADCO1B:
    case PO_ADCO2B:
    case PO_DIR:
        // #ifdef _DEBUG
        //		if (pcop->type == PO_DIR)
        //			fprintf(stderr, "PO_DIR COPY\n");
        // #endif
    case PO_PRODH:
    case PO_PRODL:
    case PO_XDATA:
    case PO_TBLPTRL:
    case PO_TBLDH:
    case PO_TBLDL:

        // DFPRINTF((stderr,"pCodeOpCopy GPR register\n"));
        pcopnew = Safe_calloc(1, sizeof(pCodeOpReg));
        memcpy(pcopnew, pcop, sizeof(pCodeOpReg));
        DFPRINTF((stderr, " register index %d\n", PCOR(pcop)->r->rIdx));
        break;

    case PO_LITERAL:
        // DFPRINTF((stderr,"pCodeOpCopy lit\n"));
        pcopnew = Safe_calloc(1, sizeof(pCodeOpLit));
        memcpy(pcopnew, pcop, sizeof(pCodeOpLit));
        break;

    case PO_IMMEDIATE:
        pcopnew = Safe_calloc(1, sizeof(pCodeOpImmd));
        memcpy(pcopnew, pcop, sizeof(pCodeOpImmd));
        break;

    case PO_GPR_BIT:
    case PO_CRY:
    case PO_BIT:
        // DFPRINTF((stderr,"pCodeOpCopy bit\n"));
        // if (removed_count >= 7)
        //{
        //	fprintf(stderr, "removed 7\n");
        // }
        pcopnew = Safe_calloc(1, sizeof(pCodeOpRegBit));
        memcpy(pcopnew, pcop, sizeof(pCodeOpRegBit));
        break;

    case PO_LABEL:
        // DFPRINTF((stderr,"pCodeOpCopy label\n"));
        pcopnew = Safe_calloc(1, sizeof(pCodeOpLabel));
        memcpy(pcopnew, pcop, sizeof(pCodeOpLabel));
        break;

    case PO_WILD:
        /* Here we expand the wild card into the appropriate type: */
        /* By recursively calling pCodeOpCopy */
        // DFPRINTF((stderr,"pCodeOpCopy wild\n"));
        if (PCOW(pcop)->matched)
            pcopnew = pCodeOpCopy(PCOW(pcop)->matched);
        else
        {
            // Probably a label
            pcopnew = pCodeOpCopy(PCOW(pcop)->subtype);
            pcopnew->name = Safe_strdup(PCOW(pcop)->pcwb->vars[PCOW(pcop)->id]);
            // DFPRINTF((stderr,"copied a wild op named %s\n",pcopnew->name));
        }

        return pcopnew;
        break;

    default:
        assert(!"unhandled pCodeOp type copied");
        break;
    } // switch

    if (pcop->name)
        pcopnew->name = Safe_strdup(pcop->name);
    else
        pcopnew->name = NULL;

    return pcopnew;
}

/*-----------------------------------------------------------------*/
/* popCopyReg - copy a pcode operator                              */
/*-----------------------------------------------------------------*/
pCodeOp *popCopyReg(pCodeOpReg *pc)
{
    pCodeOpReg *pcor;

    pcor = Safe_calloc(1, sizeof(pCodeOpReg));
    pcor->pcop.type = pc->pcop.type;
    if (pc->pcop.name)
    {
        if (!(pcor->pcop.name = Safe_strdup(pc->pcop.name)))
            fprintf(stderr, "oops %s %d", __FILE__, __LINE__);
    }
    else
        pcor->pcop.name = NULL;

    if (pcor->pcop.type == PO_IMMEDIATE)
    {
        PCOL(pcor)->lit = PCOL(pc)->lit;
    }
    else
    {
        pcor->r = pc->r;
        pcor->rIdx = pc->rIdx;
        if (pcor->r)
            pcor->r->wasUsed = 1;
    }
    // DEBUGHY08A_emitcode ("; ***","%s  , copying %s, rIdx=%d",__FUNCTION__,pc->pcop.name,pc->rIdx);

    return PCOP(pcor);
}

/*-----------------------------------------------------------------*/
/* pCodeInstructionCopy - copy a pCodeInstructionCopy              */
/*-----------------------------------------------------------------*/
pCode *pCodeInstructionCopy(pCodeInstruction *pci, int invert)
{
    pCodeInstruction *new_pci;
    pBranch *pbr;

    if (invert)
        new_pci = PCI(newpCode(pci->inverted_op, pci->pcop));
    else
        new_pci = PCI(newpCode(pci->op, pci->pcop));

    new_pci->pcop2 = pci->pcop2;

    new_pci->pc.pb = pci->pc.pb;
    new_pci->from = pci->from;
    new_pci->to = pci->to;
    new_pci->label = pci->label;

    replaceLabelPtrTabInPb(pci->pc.pb, (pCode *)pci, (pCode *)new_pci);

    new_pci->exp_para_num = pci->exp_para_num;

    // when a instruction is copied, the label index should also be replace
    for (pbr = new_pci->label; pbr; pbr = pbr->next)
    {
        int key = ((pCodeLabel *)(pbr->pc))->key;
        int n = getLabelKeyIndexInPb(pci->pc.pb, key);
        pci->pc.pb->labelPtrTab[n] = (pCode *)new_pci;
    }

    new_pci->pcflow = pci->pcflow;
    new_pci->cline = pci->cline;
    pci->cline = NULL; // if there is C line , we copy it

    return PCODE(new_pci);
}

/*-----------------------------------------------------------------*/
/*-----------------------------------------------------------------*/
void pCodeDeleteChain(pCode *f, pCode *t)
{
    pCode *pc;

    while (f && f != t)
    {
        DFPRINTF((stderr, "delete pCode:\n"));
        pc = f->next;
        // f->print(stderr,f);
        // f->delete(f);  this dumps core...
        f = pc;
    }
}

/*-----------------------------------------------------------------*/
/*-----------------------------------------------------------------*/

char *get_op(pCodeOp *pcop, char *buffer, size_t size)
{
    reg_info *r;
    static char b[128];
    char *s;
    int use_buffer = 1; // copy the string to the passed buffer pointer

    if (!buffer)
    {

        if (pcop->str)
            return pcop->str;
        buffer = b;
        size = sizeof(b);
        use_buffer = 0; // Don't bother copying the string to the buffer.
    }
    else if (pcop->str)
    {
        strncpy(buffer, pcop->str, size);
        return buffer;
    }

    if (pcop)
    {
        switch (pcop->type)
        {
        case PO_PRODL:
            strncpy(buffer, "_PRODL", size);
            return buffer;
        case PO_PRODH:
            strncpy(buffer, "_PRODH", size);
            return buffer;

        case PO_INDF:
        case PO_POINC:
        case PO_POINC0:
        case PO_PRINC0:
        case PO_PODEC0:
        case PO_PRINC2:
        case PO_PODEC2:
        case PO_SSPBUF:
        case PO_PLUSW:
        case PO_FSR:
        case PO_FSR0L:
        case PO_FSR0H:

            if (use_buffer)
            {
                SNPRINTF(buffer, size, "%s", PCOR(pcop)->r->name);
                pcop->str = strdup(buffer);
                pcop->strHash = hash(buffer);
                return buffer;
            }

            pcop->str = pcop->name;
            pcop->strHash = hash(pcop->str);
            return pcop->name;
            break;
        case PO_GPR_TEMP:
            if (PCOR(pcop)->r->type == REG_STK)
                r = typeRegWithIdx(PCOR(pcop)->r->rIdx, REG_STK, 1);
            else
                r = HY08A_regWithIdx(PCOR(pcop)->r->rIdx);

            if (use_buffer)
            {
                if (!r)
                    SNPRINTF(buffer, size, "%s", pcop->name);
                else
                    SNPRINTF(buffer, size, "%s", r->name);
                pcop->str = strdup(buffer);
                pcop->strHash = hash(pcop->str);
                return buffer;
            }
            if (r == NULL)
            {
                char buf[1024];
                SNPRINTF(buf, 1023, "(%s + %d)", PCOR(pcop)->r->name, PCOR(pcop)->instance);
                pcop->str = strdup(buf);
                pcop->strHash = hash(buf);
                return pcop->str;
            }
            pcop->str = r->name;
            pcop->strHash = hash(r->name);
            return r->name;
            break;

        case PO_IMMEDIATE:
            s = buffer;
            if (PCOI(pcop)->_const)
            {
                if (pcop->flags.imm2label)
                {
                    SNPRINTF(s, size, "(%s + %d)", pcop->name, PCOI(pcop)->index);
                }
                else if (PCOI(pcop)->offset >= 0 && PCOI(pcop)->offset < 4)
                {
                    switch (PCOI(pcop)->offset)
                    {
                    case 0:
                        SNPRINTF(s, size, "low (%s + %d)", pcop->name, PCOI(pcop)->index);
                        break;
                    case 1:
                        if (PCOI(pcop)->_cp2gp)
                            SNPRINTF(s, size, "0x80 | high (%s + %d) ", pcop->name, PCOI(pcop)->index);
                        else
                            SNPRINTF(s, size, "high (%s + %d)", pcop->name, PCOI(pcop)->index);
                        break;
                    case 2:
                        SNPRINTF(s, size, "0x%02x", PCOI(pcop)->_const ? GPTRTAG_CODE : GPTRTAG_DATA);
                        break;
                    default:
                        fprintf(stderr, "PO_IMMEDIATE/_const/offset=%d\n", PCOI(pcop)->offset);
                        assert(!"offset too large");
                        SNPRINTF(s, size, "(((%s + %d) >> %d)&0xff)",
                                 pcop->name,
                                 PCOI(pcop)->index,
                                 8 * PCOI(pcop)->offset);
                    }
                }
                else
                    SNPRINTF(s, size, "LOW (%s + %d)", pcop->name, PCOI(pcop)->index);
            }
            else
            {
                if (!PCOI(pcop)->offset)
                { // && PCOI(pcc->pcop)->offset<4)
                    SNPRINTF(s, size, "(%s + %d)",
                             pcop->name,
                             PCOI(pcop)->index);
                }
                else
                {
                    switch (PCOI(pcop)->offset)
                    {
                    case 0:
                        SNPRINTF(s, size, "(%s + %d)", pcop->name, PCOI(pcop)->index);
                        break;
                    case 1:
                        if (PCOI(pcop)->_cp2gp)
                            SNPRINTF(s, size, "0x80 | high (%s + %d) ", pcop->name, PCOI(pcop)->index);
                        else
                            SNPRINTF(s, size, "high (%s + %d)", pcop->name, PCOI(pcop)->index);
                        break;
                    case 2:
                        SNPRINTF(s, size, "0x%02x", PCOI(pcop)->_const ? GPTRTAG_CODE : GPTRTAG_DATA);
                        break;
                    default:
                        fprintf(stderr, "PO_IMMEDIATE/mutable/offset=%d\n", PCOI(pcop)->offset);
                        assert(!"offset too large");
                        SNPRINTF(s, size, "((%s + %d) >> %d)&0xff", pcop->name, PCOI(pcop)->index, 8 * PCOI(pcop)->offset);
                        break;
                    }
                }
            }
            pcop->str = strdup(buffer);
            pcop->strHash = hash(buffer);
            return buffer;
            break;

        case PO_DIR:
            s = buffer;
            if (PCOR(pcop)->instance)
            {
                SNPRINTF(s, size, "(%s + %d)",
                         pcop->name,
                         PCOR(pcop)->instance);
            }
            else
                SNPRINTF(s, size, "%s", pcop->name);
            pcop->str = strdup(buffer);
            pcop->strHash = hash(buffer);
            return buffer;
            break;

        case PO_LABEL:
            s = buffer;
            if (pcop->name)
            {
                if (PCOLAB(pcop)->offset == 1)
                {
                    if (PCOLAB(pcop)->div2)
                        SNPRINTF(s, size, "HIGHD2(%s)", pcop->name);
                    else
                        SNPRINTF(s, size, "HIGH(%s)", pcop->name);
                }
                else
                {
                    if (PCOLAB(pcop)->div2)
                        SNPRINTF(s, size, "D2(%s)", pcop->name);
                    else
                        SNPRINTF(s, size, "%s", pcop->name);
                }
            }
            pcop->str = strdup(buffer);
            pcop->strHash = hash(buffer);
            return buffer;
            break;

        case PO_GPR_BIT:
            if (PCORB(pcop)->pcor.r)
            {
                // if(use_buffer) {
                SNPRINTF(buffer, size, "%s,%d", PCORB(pcop)->pcor.r->name, PCORB(pcop)->bit);
                pcop->str = strdup(buffer);
                pcop->strHash = hash(buffer);
                return buffer;

                //}
                // SNPRINTF(s, size, "%s,%d", PCORB(pcop)->pcor.pcop.name, PCORB(pcop)->bit);
                // return s;
            }
            /* fall through to the default case */

        default:
            if (pcop->name)
            {
                char bufsecond[1024];
                if (use_buffer)
                {

                    if (pcop->TBLR_Reference)
                    {

                        SNPRINTF(buffer, size, "%s %s (%s & 1)", pcop->name, pcop->flags.needSub1 ? "-" : "+", get_op(PCI(pcop->TBLR_Reference)->pcop, bufsecond, 1023));
                    }
                    else
                        SNPRINTF(buffer, size, "%s", pcop->name);
                    pcop->str = strdup(buffer);
                    pcop->strHash = hash(buffer);
                    return buffer;
                }
                if (pcop->TBLR_Reference)
                {
                    char buftblr[1024];

                    SNPRINTF(buftblr, 1023, "%s %s (%s & 1)", pcop->name, pcop->flags.needSub1 ? "-" : "+", get_op(PCI(pcop->TBLR_Reference)->pcop, bufsecond, 1023));
                    pcop->str = strdup(buftblr);
                    pcop->strHash = hash(buftblr);
                    return pcop->str;
                }
                pcop->str = pcop->name;
                pcop->strHash = hash(pcop->str);
                return pcop->name;
            }
        }
    }

    printf("HYCON port internal warning: (%s:%d(%s)) %s not found\n",
           __FILE__, __LINE__, __FUNCTION__,
           pCodeOpType(pcop));

    return "NO operand";
}

/*-----------------------------------------------------------------*/
/*-----------------------------------------------------------------*/
static char *get_op_from_instruction(pCodeInstruction *pcc)
{

    if (pcc)
    {

        return get_op(pcc->pcop, NULL, 0);
    }

    return ("ERROR Null: get_op_from_instruction");
}

/*-----------------------------------------------------------------*/
/*-----------------------------------------------------------------*/
static void pCodeOpPrint(FILE *of, pCodeOp *pcop)
{
    fprintf(of, "pcodeopprint- not implemented\n");
}

/*-----------------------------------------------------------------*/
/* pCode2str - convert a pCode instruction to string               */
/*-----------------------------------------------------------------*/
char *pCode2str(char *str, size_t size, pCode *pc)
{
    char *s = str;
    char buf1[129];
    char buf2[129];
    int needCheckPara = 0;

    switch (pc->type)
    {

    case PC_OPCODE:
        // if (PCI(pc)->op == POC_TBLR)
        //	fprintf(stderr, "new compiler op code.\n");
        if (PCI(pc)->op == POC_CALL && PCI(pc)->exp_para_num >= 65535 && PCI(pc)->pcop->flags.needCheckPara)
        {
            needCheckPara = 1;
        }
        SNPRINTF(s, size, "\t%s\t", PCI(pc)->mnemonic);
        size -= strlen(s);
        s += strlen(s);
        // there is a special case

        if (PCI(pc)->num_ops == 2 && PCI(pc)->addrMode == asm_FSFD) // MVFF
        {

            // 2020, MVFF man call FPTR
            if (PCI(pc)->callFPTR)
            {
                SNPRINTF(s, size, "%s,%s\n\t.CALLFPTR",
                         get_op(PCI(pc)->pcop, buf1, 128),
                         (PCI(pc)->pcop2) ? get_op(PCI(pc)->pcop2, buf2, 128) : 0);
            }
            else
            {
                SNPRINTF(s, size, "%s,%s",
                         get_op(PCI(pc)->pcop, buf1, 128),
                         (PCI(pc)->pcop2) ? get_op(PCI(pc)->pcop2, buf2, 128) : 0);
            }
        }
        else if (PCI(pc)->num_ops == 2 && PCI(pc)->op == POC_LDPR)
        {
            SNPRINTF(s, size, "%s,%s",
                     get_op(PCI(pc)->pcop, buf1, 128),
                     (PCI(pc)->pcop2) ? get_op(PCI(pc)->pcop2, buf2, 128) : "0");
        }
        else if (PCI(pc)->num_ops == 1 && PCI(pc)->addrMode == asm_ONE)
        {
            SNPRINTF(s, size, " 1");
        }
        else if (PCI(pc)->num_ops == 1 && PCI(pc)->addrMode == asm_LDPR)
        {
            SNPRINTF(s, size, "%s,0", get_op(PCI(pc)->pcop, buf1, 128));
        }
        else if (PCI(pc)->addrMode == asm_TBLR)
        {
            SNPRINTF(s, size, "*+");
        }
        else if ((PCI(pc)->num_ops >= 1) && (PCI(pc)->pcop))
        {
            if (PCI(pc)->isBitInst)
            {
                if (PCI(pc)->pcop->type == PO_GPR_BIT)
                {
                    char *name = PCI(pc)->pcop->name;
                    if (!name)
                        name = PCOR(PCI(pc)->pcop)->r->name;
                    if ((((pCodeOpRegBit *)(PCI(pc)->pcop))->inBitSpace))
                        SNPRINTF(s, size, "(%s >> 3), (%s & 7)", name, name);
                    else
                    {
                        // there is instance!!
                        if (PCOR(PCI(pc)->pcop)->instance)
                            SNPRINTF(s, size, "(%s + %d),%d", name, PCOR(PCI(pc)->pcop)->instance, (((pCodeOpRegBit *)(PCI(pc)->pcop))->bit) & 7);
                        else
                            SNPRINTF(s, size, "%s,%d", name, (((pCodeOpRegBit *)(PCI(pc)->pcop))->bit) & 7);
                    }
                }
                else if (PCI(pc)->pcop->type == PO_GPR_BIT)
                {
                    SNPRINTF(s, size, "%s,%d,0", get_op_from_instruction(PCI(pc)), PCORB(PCI(pc)->pcop)->bit);
                }
                else
                    SNPRINTF(s, size, "%s,0 ; ?bug", get_op_from_instruction(PCI(pc)));
            }
            else
            {
                if (PCI(pc)->pcop->type == PO_GPR_BIT)
                {
                    if (PCI(pc)->num_ops >= 2)
                        SNPRINTF(s, size, "%s,%c,0", get_op_from_instruction(PCI(pc)), ((PCI(pc)->isModReg) ? '1' : '0')); // F is 1 , W is 0
                    else
                        SNPRINTF(s, size, "(1 << (%s & 7))", get_op_from_instruction(PCI(pc)));
                }
                else
                {
                    // if (PCI(pc)->op == POC_MVL && PCI(pc)->pcop->type == PO_LABEL)
                    //{
                    //	fprintf(stderr, "mvl label!!");
                    // }
                    // if(PCI(pc)->op == POC_CALL && PCI(pc)->exp_para_num>=65535)

                    SNPRINTF(s, size, "%s", get_op_from_instruction(PCI(pc)));
                    size -= strlen(s);
                    s += strlen(s);
                    if (PCI(pc)->num_ops >= 2)
                    {
                        switch (PCI(pc)->addrMode)
                        {
                        case asm_FDA:
                            SNPRINTF(s, size, ",%c,0", ((PCI(pc)->isModReg) ? '1' : '0')); // F is 1, W is 0
                            break;
                        case asm_FA:
                            SNPRINTF(s, size, ",0");
                            break;
                        default:
                            break;
                        }
                    }
                    else
                    {
                        if (PCI(pc)->addrMode == asm_ONE)
                        {
                            SNPRINTF(s, size, " 1");
                        }
                    }
                }
            }
        }
        // if (PCI(pc)->retOffP1)
        //{
        //	char retvstr[256]; // stack space
        //	SNPRINTF(retvstr, 256 - 1, "\t\t; $CRETV:%d", PCI(pc)->retOffP1 - 1);
        //	strncat(s, retvstr, size - 40);
        // }
        if (needCheckPara)
        {
            SNPRINTF(s, size, "\n\t.CHKPARA %s %d", get_op_from_instruction(PCI(pc)),
                     PCI(pc)->exp_para_num - 65536);
        }
        break;

    case PC_COMMENT:
        /* assuming that comment ends with a \n */
        SNPRINTF(s, size, ";%s", ((pCodeComment *)pc)->comment);
        break;

    case PC_INLINE:
        /* assuming that inline code ends with a \n */
        SNPRINTF(s, size, "%s", ((pCodeComment *)pc)->comment);
        break;

    case PC_LABEL:
        SNPRINTF(s, size, ";label=%s, key=%d\n", PCL(pc)->label, PCL(pc)->key);
        break;
    case PC_FUNCTION:
        SNPRINTF(s, size, ";modname=%s,function=%s: id=%d\n", PCF(pc)->modname, PCF(pc)->fname);
        break;
    case PC_WILD:
        SNPRINTF(s, size, ";\tWild opcode: id=%d\n", PCW(pc)->id);
        break;
    // case PC_FLOW:
    //     SNPRINTF(s, size, ";\t--FLOW change\n");
    //     break;
    case PC_CSOURCE:
        SNPRINTF(s, size, "%s\t;.line\t%d; \"%s\"\t%s;$SEQ:%d\n", (options.debug ? "" : ";"), PCCS(pc)->line_number, PCCS(pc)->file_name, PCCS(pc)->line, PCCS(pc)->seq_number);
        break;
    case PC_ASMDIR:
        if (PCAD(pc)->directive)
        {
            SNPRINTF(s, size, "\t%s%s%s\n", PCAD(pc)->directive, PCAD(pc)->arg ? "\t" : "", PCAD(pc)->arg ? PCAD(pc)->arg : "");
        }
        else if (PCAD(pc)->arg)
        {
            /* special case to handle inline labels without a tab */
            SNPRINTF(s, size, "%s\n", PCAD(pc)->arg);
        }
        break;

    case PC_BAD:
        SNPRINTF(s, size, ";A bad pCode is being used\n");
    }

    return str;
}

/*-----------------------------------------------------------------*/
/* genericPrint - the contents of a pCode to a file                */
/*-----------------------------------------------------------------*/
static void genericPrint(FILE *of, pCode *pc)
{
    if (!pc || !of)
        return;

    switch (pc->type)
    {
    case PC_COMMENT:
        fprintf(of, ";%s\n", ((pCodeComment *)pc)->comment);
        break;

    case PC_INLINE:
        fprintf(of, "%s\n", ((pCodeComment *)pc)->comment);
        break;

    case PC_OPCODE:
        // If the opcode has a label, print that first
        {
            char str[1024];
            // char typestr[1024];
            pCodeInstruction *pci = PCI(pc);
            pBranch *pbl = pci->label;
            while (pbl && pbl->pc)
            {
                if (pbl->pc->type == PC_LABEL)
                    pCodePrintLabel(of, pbl->pc);
                pbl = pbl->next;
            }

            if (pci->cline)
                genericPrint(of, PCODE(pci->cline));

            pCode2str(str, 1023, pc);

            fprintf(of, "%s", str);
            // #ifdef _DEBUG
            //             if (PCI(pc)->pcop)
            //                 fprintf(of, "\t;%s", pCodeOpType(PCI(pc)->pcop));
            // #endif
            /* Debug */
            if (debug_verbose)
            {
                pCodeOpReg *pcor = PCOR(pci->pcop);
                fprintf(of, "\t;id=%u,key=%03x,inCond:%x,outCond:%x", pc->id, pc->seq, pci->inCond, pci->outCond);

                if (pcor && pcor->pcop.type == PO_GPR_TEMP && !pcor->r->isFixed)
                    fprintf(of, ",rIdx=r0x%X", pcor->rIdx);
            }
        }
        fprintf(of, "\n");
        break;

    case PC_WILD:
        fprintf(of, ";\tWild opcode: id=%d\n", PCW(pc)->id);
        if (PCW(pc)->pci.label)
            pCodePrintLabel(of, PCW(pc)->pci.label->pc);

        if (PCW(pc)->operand)
        {
            fprintf(of, ";\toperand  ");
            pCodeOpPrint(of, PCW(pc)->operand);
        }
        break;

        // case PC_FLOW:
        //     if (debug_verbose)
        //     {
        //         fprintf(of, ";<>Start of new flow, seq=0x%x", pc->seq);
        //         if (PCFL(pc)->ancestor)
        //             fprintf(of, " ancestor = 0x%x", PCODE(PCFL(pc)->ancestor)->seq);
        //         fprintf(of, "\n");
        //         fprintf(of, ";  from: ");
        //         {
        //             pCodeFlowLink *link;
        //             for (link = setFirstItem(PCFL(pc)->from); link; link = setNextItem(PCFL(pc)->from))
        //             {
        //                 fprintf(of, "%03x ", link->pcflow->pc.seq);
        //             }
        //         }
        //         fprintf(of, "; to: ");
        //         {
        //             pCodeFlowLink *link;
        //             for (link = setFirstItem(PCFL(pc)->to); link; link = setNextItem(PCFL(pc)->to))
        //             {
        //                 fprintf(of, "%03x ", link->pcflow->pc.seq);
        //             }
        //         }
        //         fprintf(of, "\n");
        //     }
        //     break;

    case PC_CSOURCE:
        fprintf(of, "%s\t;.line\t%d; \"%s\"\t%s;$SEQ:%d\n", (options.debug ? "" : ";"), PCCS(pc)->line_number, PCCS(pc)->file_name, PCCS(pc)->line, PCCS(pc)->seq_number);
        break;

    case PC_ASMDIR:
    {
        pBranch *pbl = PCAD(pc)->pci.label;
        while (pbl && pbl->pc)
        {
            if (pbl->pc->type == PC_LABEL)
                pCodePrintLabel(of, pbl->pc);
            pbl = pbl->next;
        }
    }
        if (PCAD(pc)->directive)
        {
            if (PCAD(pc)->directive != NULL && (PCAD(pc)->directive[0] == ';'))
            {
                fprintf(of, "%s%s%s\n", PCAD(pc)->directive, PCAD(pc)->arg ? "\t" : "", PCAD(pc)->arg ? PCAD(pc)->arg : "");
            }
            else
            {
                fprintf(of, "\t%s%s%s\n", PCAD(pc)->directive, PCAD(pc)->arg ? "\t" : "", PCAD(pc)->arg ? PCAD(pc)->arg : "");
            }
        }
        else if (PCAD(pc)->arg)
        {
            /* special case to handle inline labels without tab */
            fprintf(of, "%s\n", PCAD(pc)->arg);
        }
        break;

    case PC_LABEL:
    default:
        fprintf(of, "unknown pCode type %d\n", pc->type);
    }
}

/*-----------------------------------------------------------------*/
/* pCodePrintFunction - prints function begin/end                  */
/*-----------------------------------------------------------------*/

static void pCodePrintFunction(FILE *of, pCode *pc)
{

    if (!pc || !of)
        return;

    if (((pCodeFunction *)pc)->modname)
        fprintf(of, "F_%s", ((pCodeFunction *)pc)->modname);

    // if next line has src, we print it here, too
    if (pc->next && isPCI(pc->next) && PCI(pc->next)->cline)
    {
        pCodeCSource *pcs = (PCI(pc->next)->cline);
        char buffer[2560];
        fprintf(of, "%s", pCode2str(buffer, 2559, (pCode *)pcs));
        PCI(pc->next)->cline = NULL; // remove it
    }

    if (PCF(pc)->fname)
    {
        pBranch *exits = PCF(pc)->to;
        int i = 0;

        fprintf(of, "%s:\t;Function start\n", PCF(pc)->fname);

        // HY08A (H08C/D) special operation here, we add OSCCN1 bit 7 set to 1 here if main
        if (!strcmp(PCF(pc)->fname, "_main") && HY08A_getPART()->isEnhancedCore >= 3 && HY08A_getPART()->isEnhancedCore != 5 &&
            !(HY08A_options.force_h08a) &&
            HY08A_getPART()->config[0] > 0 && HY08A_getPART()->config[0] < 0x1000)
        {
            fprintf(of, "\tBSF\t0x%X,7\n", HY08A_getPART()->config[0]); // CCOPT = 1 !!
        }

        while (exits)
        {
            i++;
            exits = exits->next;
        }
        // if(i) i--;
        // fprintf(of,"; %d exit point%c\n",i, ((i==1) ? ' ':'s'));
    }
    else
    {
        if ((PCF(pc)->from &&
             PCF(pc)->from->pc->type == PC_FUNCTION &&
             PCF(PCF(pc)->from->pc)->fname))
            fprintf(of, "; exit point of %s\n", PCF(PCF(pc)->from->pc)->fname);
        else
            fprintf(of, "; exit point [can't find entry point]\n");
    }
}

/*-----------------------------------------------------------------*/
/* pCodePrintLabel - prints label                                  */
/*-----------------------------------------------------------------*/

static void pCodePrintLabel(FILE *of, pCode *pc)
{

    if (!pc || !of)
        return;

    if (PCL(pc)->label)
        fprintf(of, "%s:", PCL(pc)->label);
    else if (PCL(pc)->key >= 0)
        fprintf(of, "_%05d_DS_:", PCL(pc)->key);
    else
        fprintf(of, ";wild card label: id=%d", -PCL(pc)->key);
    if (labelChangeLine)
        fprintf(of, "\n");
}

/*-----------------------------------------------------------------*/
/* unlinkpCodeFromBranch - Search for a label in a pBranch and     */
/*                         remove it if it is found.               */
/*-----------------------------------------------------------------*/
static void unlinkpCodeFromBranch(pCode *pcl, pCode *pc)
{
    pBranch *b, *bprev;

    bprev = NULL;

    if (pcl->type == PC_OPCODE || pcl->type == PC_INLINE || pcl->type == PC_ASMDIR)
        b = PCI(pcl)->label;
    else
    {
        fprintf(stderr, "LINE %d. can't unlink from non opcode\n", __LINE__);
        exit(1);
    }

    // fprintf (stderr, "%s \n",__FUNCTION__);
    // pcl->print(stderr,pcl);
    // pc->print(stderr,pc);
    while (b)
    {
        if (b->pc == pc)
        {
            // fprintf (stderr, "found label\n");

            /* Found a label */
            if (bprev)
            {
                bprev->next = b->next; /* Not first pCode in chain */
                free(b);
            }
            else
            {
                pc->destruct(pc);
                PCI(pcl)->label = b->next; /* First pCode in chain */
                free(b);
            }
            return; /* A label can't occur more than once */
        }
        bprev = b;
        b = b->next;
    }
}

/*-----------------------------------------------------------------*/
/*-----------------------------------------------------------------*/
pBranch *pBranchAppend(pBranch *h, pBranch *n)
{
    pBranch *b;
    int key = PCL(n->pc)->key;

    if (!h)
        return n;

    if (h == n)
        return n;

    b = h;
    if (PCL(b->pc)->key == key)
        return h;
    while (b->next)
    {

        b = b->next;
        if (PCL(b->pc)->key == key)
            return h;
    }

    b->next = n;

    return h;
}

/*-----------------------------------------------------------------*/
/* pBranchLink - given two pcodes, this function will link them    */
/*               together through their pBranches                  */
/*-----------------------------------------------------------------*/
static void pBranchLink(pCodeFunction *f, pCodeFunction *t)
{
    pBranch *b;

    // Declare a new branch object for the 'from' pCode.

    //_ALLOC(b,sizeof(pBranch));
    b = Safe_calloc(1, sizeof(pBranch));
    b->pc = PCODE(t); // The link to the 'to' pCode.
    b->next = NULL;

    f->to = pBranchAppend(f->to, b);

    // Now do the same for the 'to' pCode.

    //_ALLOC(b,sizeof(pBranch));
    b = Safe_calloc(1, sizeof(pBranch));
    b->pc = PCODE(f);
    b->next = NULL;

    t->from = pBranchAppend(t->from, b);
}

/*-----------------------------------------------------------------*/
/*-----------------------------------------------------------------*/
// static inline int compareLabel(pCode *pc, int key)
//{
//     pBranch *pbr;
//
//     if(pc->type == PC_LABEL) {
//         if( ((pCodeLabel *)pc)->key == key )
//             return TRUE;
//     }
//     if(pc->type == PC_OPCODE || pc->type == PC_ASMDIR) {
//         pbr = PCI(pc)->label;
//         while(pbr) {
//             if(pbr->pc->type == PC_LABEL) {
//                 if( ((pCodeLabel *)(pbr->pc))->key ==  key)
//                     return TRUE;
//             }
//             pbr = pbr->next;
//         }
//     }
//
//     return FALSE;
// }

/*-----------------------------------------------------------------*/
/*-----------------------------------------------------------------*/
// static int checkLabel(pCode *pc)
//{
//     pBranch *pbr;
//
//     if(pc && isPCI(pc)) {
//         pbr = PCI(pc)->label;
//         while(pbr) {
//             if(isPCL(pbr->pc) && (PCL(pbr->pc)->key >= 0))
//                 return TRUE;
//
//             pbr = pbr->next;
//         }
//     }
//
//     return FALSE;
// }

/*-----------------------------------------------------------------*/
/* findLabelinpBlock - Search the pCode for a particular label     */
/*-----------------------------------------------------------------*/

// 2020, add special operation for deleted code
static pCode *findLabelinpBlock(pBlock *pb, pCodeOpLabel *pcop_label)
{
    pCode *pc = NULL;
    // pBranch *pbr;
    int i;

    if (!pb)
        return NULL;

    // don't consider ASMDIR
    for (i = pb->labelTabSize - 1; i >= 0; i--) // from new to old
        if (pb->labelIndexTab[i] == pcop_label->key)
        {
            pc = pb->labelPtrTab[i];
            break;
        }

    if (pc == NULL || (pc->prev == NULL && pc->next == NULL))
    {
#ifdef __GNU__
        fprintf(stderr, "Warning, strange label found %s\n", pcop_label->name);
#endif
        for (pc = pb->pcHead; pc; pc = pc->next)
        {
            if (isPCI(pc) && PCI(pc)->label)
            {
                pBranch *pr = PCI(pc)->label;
                while (pr)
                {
                    if (PCL(pr->pc)->key == pcop_label->key)
                        return pc;
                    pr = pr->next;
                }
            }
        }
    }

    // if (pcop_label->key == 167)
    // fprintf(stderr, "167\n");
    // for (pc = pb->pcHead; pc; pc = pc->next)
    //{
    //	if (pc->type == PC_LABEL && PCOLAB(pc)->key == pcop_label->key)
    //		return pc;
    //	if (pc->type == PC_OPCODE || pc->type == PC_ASMDIR) {
    //		pbr = PCI(pc)->label;
    //		while (pbr) {
    //			if (pbr->pc->type == PC_LABEL) {
    //				if (((pCodeLabel *)(pbr->pc))->key == pcop_label->key)
    //					return pc;
    //			}
    //			pbr = pbr->next;
    //		}
    //	}
    //
    // }

    return pc;
}

/*-----------------------------------------------------------------*/
/* findNextpCode - given a pCode, find the next of type 'pct'      */
/*                 in the linked list                              */
/*-----------------------------------------------------------------*/
pCode *findNextpCode(pCode *pc, PC_TYPE pct)
{

    while (pc)
    {
        if (pc->type == pct)
            return pc;

        pc = pc->next;
    }

    return NULL;
}

#if 0
/*-----------------------------------------------------------------*/
/* findPrevpCode - given a pCode, find the previous of type 'pct'  */
/*                 in the linked list                              */
/*-----------------------------------------------------------------*/
static pCode * findPrevpCode(pCode *pc, PC_TYPE pct)
{

    while(pc) {
        if(pc->type == pct) {
            /*
            static unsigned int stop;
            if (pc->id == 524)
            	stop++; // Place break point here
            */
            return pc;
        }

        pc = pc->prev;
    }

    return NULL;
}
#endif

/*-----------------------------------------------------------------*/
/* findNextInstruction - given a pCode, find the next instruction  */
/*                       in the linked list                        */
/*-----------------------------------------------------------------*/

inline pCode *findNextInstruction(pCode *pci)
{
    pCode *pc = pci;

    while (pc)
    {
        if ((pc->type == PC_OPCODE) || (pc->type == PC_WILD) || (pc->type == PC_ASMDIR))
            return pc;

#ifdef PCODE_DEBUG
        fprintf(stderr, "findNextInstruction:  ");
        printpCode(stderr, pc);
#endif
        pc = pc->next;
    }
    // fprintf(stderr,"Couldn't find instruction\n");
    return NULL;
}

/*-----------------------------------------------------------------*/
/* findNextInstruction - given a pCode, find the next instruction  */
/*                       in the linked list                        */
/*-----------------------------------------------------------------*/
pCode *findPrevInstruction(pCode *pci)
{
    pCode *pc = pci;

    while (pc)
    {

        if ((pc->type == PC_OPCODE) || (pc->type == PC_WILD) || (pc->type == PC_ASMDIR))
            return pc;

#ifdef PCODE_DEBUG
        fprintf(stderr, "findPrevInstruction:  ");
        printpCode(stderr, pc);
#endif
        pc = pc->prev;
    }

    // fprintf(stderr,"Couldn't find instruction\n");
    return NULL;
}

/*-----------------------------------------------------------------*/
/*-----------------------------------------------------------------*/
reg_info *getRegFromInstruction(pCode *pc)
{
    reg_info *r;
    if (!pc ||
        !isPCI(pc) ||
        !PCI(pc)->pcop ||
        PCI(pc)->num_ops == 0)
        return NULL;

    switch (PCI(pc)->pcop->type)
    {
    case PO_STATUS:
    case PO_FSR:
    case PO_FSR0H:
    case PO_FSR0L:
    case PO_POINC0:
    case PO_PRINC0:
    case PO_PODEC0:
    case PO_SSPBUF:
    case PO_PLUSW:
    case PO_INDF:
    case PO_POINC:
    case PO_PRINC2:
    case PO_PODEC2:
    case PO_INTE0:
    case PO_BIT:
    case PO_GPR_TEMP:
    case PO_SFR_REGISTER:
    case PO_PCL:
    case PO_PCLATU:
    case PO_PCLATH:
        return PCOR(PCI(pc)->pcop)->r;

    case PO_GPR_REGISTER:
    case PO_GPR_BIT:
    case PO_DIR:
        r = PCOR(PCI(pc)->pcop)->r;
        if (r)
            return r;
        return dirregWithName(PCI(pc)->pcop->name);

    case PO_LITERAL:
        break;

    case PO_IMMEDIATE:
        r = PCOI(PCI(pc)->pcop)->r;
        if (r)
            return r;
        return dirregWithName(PCI(pc)->pcop->name);

    default:
        break;
    }

    return NULL;
}

/*-----------------------------------------------------------------*/
/*-----------------------------------------------------------------*/

static void AnalyzepBlock(pBlock *pb)
{
    pCode *pc;

    if (!pb)
        return;

    /* Find all of the registers used in this pBlock
     * by looking at each instruction and examining it's
     * operands
     */
    for (pc = pb->pcHead; pc; pc = pc->next)
    {

        /* Is this an instruction with operands? */
        if (pc->type == PC_OPCODE && PCI(pc)->pcop)
        {
            // #ifdef _DEBUG
            //			if (PCI(pc)->pcop->name && strstr(PCI(pc)->pcop->name, "_STK07"))
            //				fprintf(stderr, "STK07\n");
            // #endif
            if ((PCI(pc)->pcop->type == PO_GPR_TEMP) || ((PCI(pc)->pcop->type == PO_GPR_BIT) && PCOR(PCI(pc)->pcop)->r && (PCOR(PCI(pc)->pcop)->r->pc_type == PO_GPR_TEMP)))
            {

                /* Loop through all of the registers declared so far in
                this block and see if we find this one there */

                reg_info *r = setFirstItem(pb->tregisters);

                while (r)
                {
                    if ((r->rIdx == PCOR(PCI(pc)->pcop)->r->rIdx) && (r->type == PCOR(PCI(pc)->pcop)->r->type))
                    {
                        PCOR(PCI(pc)->pcop)->r = r;
                        break;
                    }
                    r = setNextItem(pb->tregisters);
                }

                if (!r)
                {
                    /* register wasn't found */
                    // r = Safe_calloc(1, sizeof(regs));
                    // memcpy(r,PCOR(PCI(pc)->pcop)->r, sizeof(regs));
                    // addSet(&pb->tregisters, r);
                    reg_info *r = PCOR(PCI(pc)->pcop)->r;
                    if (r->pc_type == PO_XDATA)
                        addSet(&pb->txregisters, PCOR(PCI(pc)->pcop)->r);
                    else
                    {
                        if (strncmp("STK", r->name, 3))
                            addSet(&pb->tregisters, PCOR(PCI(pc)->pcop)->r);
                    }
                    // PCOR(PCI(pc)->pcop)->r = r;
                    // fprintf(stderr,"added register to pblock: reg %d\n",r->rIdx);
                } /* else
                     fprintf(stderr,"found register in pblock: reg %d\n",r->rIdx);
                */
            }
            if (PCI(pc)->pcop->type == PO_GPR_REGISTER)
            {
                if (PCOR(PCI(pc)->pcop)->r)
                {
                    HY08A_allocWithIdx(PCOR(PCI(pc)->pcop)->r->rIdx);
                    DFPRINTF((stderr, "found register in pblock: reg 0x%x\n", PCOR(PCI(pc)->pcop)->r->rIdx));
                }
                else
                {
                    if (PCI(pc)->pcop->name)
                        fprintf(stderr, "ERROR: %s is a NULL register\n", PCI(pc)->pcop->name);
                    else
                        fprintf(stderr, "ERROR: NULL register\n");
                }
            }
        }
    }
    // after all GPR is found, we check if dir has it?

    // for (pc = pb->pcHead; pc; pc = pc->next) {

    //	/* Is this an instruction with operands? */
    //	if (pc->type == PC_OPCODE && PCI(pc)->pcop && PCI(pc)->pcop->type == PO_DIR && PCOR(PCI(pc)->pcop)->r)
    //	{
    //		if (isinSet(pb->tregisters, PCOR(PCI(pc)->pcop)->r))
    //		{
    //			PCI(pc)->pcop->type = PO_GPR_TEMP;
    //		}
    //	}
    //}
}

/*-----------------------------------------------------------------*/
/* */
/*-----------------------------------------------------------------*/
// static void InsertpFlow(pCode *pc, pCode **pflow)
//{
//     if(*pflow)
//         PCFL(*pflow)->end = pc;
//
//     if(!pc || !pc->next)
//         return;
//
//     *pflow = newpCodeFlow();
//     pCodeInsertAfter(pc, *pflow);
// }

/*-----------------------------------------------------------------*/
/* BuildFlow(pBlock *pb) - examine the code in a pBlock and build  */
/*                         the flow blocks.                        */
/*
 * BuildFlow inserts pCodeFlow objects into the pCode chain at each
 * point the instruction flow changes.
 */
/*-----------------------------------------------------------------*/
// static void BuildFlow(pBlock *pb)
//{
//     pCode *pc;
//     pCode *last_pci=NULL;
//     pCode *pflow=NULL;
//     int seq = 0;
//
//     if(!pb)
//         return;
//
//     //fprintf (stderr,"build flow start seq %d  ",GpcFlowSeq);
//     /* Insert a pCodeFlow object at the beginning of a pBlock */
//
//     InsertpFlow(pb->pcHead, &pflow);
//
//     //pflow = newpCodeFlow();    /* Create a new Flow object */
//     //pflow->next = pb->pcHead;  /* Make the current head the next object */
//     //pb->pcHead->prev = pflow;  /* let the current head point back to the flow object */
//     //pb->pcHead = pflow;        /* Make the Flow object the head */
//     //pflow->pb = pb;
//
//     for( pc = findNextInstruction(pb->pcHead);
//             pc != NULL;
//             pc=findNextInstruction(pc)) {
//
//         pc->seq = seq++;
//         PCI(pc)->pcflow = PCFL(pflow);
//
//         //fprintf(stderr," build: ");
//         //pc->print(stderr, pc);
//         //pflow->print(stderr,pflow);
//
//         if (checkLabel(pc)) {
//
//             /* This instruction marks the beginning of a
//             	* new flow segment */
//
//             pc->seq = 0;
//             seq = 1;
//
//             /* If the previous pCode is not a flow object, then
//             * insert a new flow object. (This check prevents
//             * two consecutive flow objects from being insert in
//             * the case where a skip instruction preceeds an
//             * instruction containing a label.) */
//
//             last_pci = findPrevInstruction (pc->prev);
//
//             if(last_pci && (PCI(last_pci)->pcflow == PCFL(pflow)))
//                 InsertpFlow(last_pci, &pflow);
//
//             PCI(pc)->pcflow = PCFL(pflow);
//
//         }
//
//         if(isPCI_SKIP(pc)) {
//
//             /* The two instructions immediately following this one
//             	* mark the beginning of a new flow segment */
//
//             while(pc && isPCI_SKIP(pc)) {
//
//                 PCI(pc)->pcflow = PCFL(pflow);
//                 pc->seq = seq-1;
//                 seq = 1;
//
//                 InsertpFlow(pc, &pflow);
//                 pc=findNextInstruction(pc->next);
//             }
//
//             seq = 0;
//
//             if(!pc)
//                 break;
//
//             PCI(pc)->pcflow = PCFL(pflow);
//             pc->seq = 0;
//             InsertpFlow(pc, &pflow);
//
//         } else if ( isPCI_BRANCH(pc) && !checkLabel(findNextInstruction(pc->next)))  {
//
//             InsertpFlow(pc, &pflow);
//             seq = 0;
//
//         }
//
//         last_pci = pc;
//         pc = pc->next;
//     }
//
//     //fprintf (stderr,",end seq %d",GpcFlowSeq);
//     if(pflow)
//         PCFL(pflow)->end = pb->pcTail;
// }

/*-------------------------------------------------------------------*/
/* unBuildFlow(pBlock *pb) - examine the code in a pBlock and build  */
/*                           the flow blocks.                        */
/*
 * unBuildFlow removes pCodeFlow objects from a pCode chain
 */
/*-----------------------------------------------------------------*/
// static void unBuildFlow(pBlock *pb)
//{
//     pCode *pc,*pcnext;
//
//     if(!pb)
//         return;
//
//     pc = pb->pcHead;
//
//     while(pc) {
//         pcnext = pc->next;
//
//         if(isPCI(pc)) {
//
//             pc->seq = 0;
//             if(PCI(pc)->pcflow) {
//                 //free(PCI(pc)->pcflow);
//                 PCI(pc)->pcflow = NULL;
//             }
//
//         } else if(isPCFL(pc) )
//             pc->destruct(pc);
//
//         pc = pcnext;
//     }
//
//
// }

#if 0
/*-----------------------------------------------------------------*/
/*-----------------------------------------------------------------*/
static void dumpCond(int cond)
{

    static char *pcc_str[] = {
        //"PCC_NONE",
        "PCC_REGISTER",
        "PCC_C",
        "PCC_Z",
        "PCC_DC",
        "PCC_W",
        "PCC_EXAMINE_PCOP",
        "PCC_REG_BANK0",
        "PCC_REG_BANK1",
        "PCC_REG_BANK2",
        "PCC_REG_BANK3"
    };

    int ncond = sizeof(pcc_str) / sizeof(char *);
    int i,j;

    fprintf(stderr, "0x%04X\n",cond);

    for(i=0,j=1; i<ncond; i++, j<<=1)
        if(cond & j)
            fprintf(stderr, "  %s\n",pcc_str[i]);

}
#endif

#if 0
/*-----------------------------------------------------------------*/
/*-----------------------------------------------------------------*/
static void FlowStats(pCodeFlow *pcflow)
{

    pCode *pc;

    if(!isPCFL(pcflow))
        return;

    fprintf(stderr, " FlowStats - flow block (seq=%d)\n", pcflow->pc.seq);

    pc = findNextpCode(PCODE(pcflow), PC_OPCODE);

    if(!pc) {
        fprintf(stderr, " FlowStats - empty flow (seq=%d)\n", pcflow->pc.seq);
        return;
    }


    fprintf(stderr, "  FlowStats inCond: ");
    dumpCond(pcflow->inCond);
    fprintf(stderr, "  FlowStats outCond: ");
    dumpCond(pcflow->outCond);

}
#endif

/*-----------------------------------------------------------------*
 * int isBankInstruction(pCode *pc) - examine the pCode *pc to determine
 *    if it affects the banking bits.
 *
 * return: -1 == Banking bits are unaffected by this pCode.
 *
 * return: > 0 == Banking bits are affected.
 *
 *  If the banking bits are affected, then the returned value describes
 * which bits are affected and how they're affected. The lower half
 * of the integer maps to the bits that are affected, the upper half
 * to whether they're set or cleared.
 *
 *-----------------------------------------------------------------*/
/*
static int isBankInstruction(pCode *pc)
{
    regs *reg;
    int bank = -1;

    if(!isPCI(pc))
        return -1;

    if( ( (reg = getRegFromInstruction(pc)) != NULL) && isSTATUS_REG(reg)) {

        // Check to see if the register banks are changing
        if(PCI(pc)->isModReg) {

            pCodeOp *pcop = PCI(pc)->pcop;
            switch(PCI(pc)->op) {

            case POC_BSF:
                if(PCORB(pcop)->bit == HYA_RP0_BIT) {
                    //fprintf(stderr, "  isBankInstruction - Set RP0\n");
                    return  SET_BANK_BIT | HYA_RP0_BIT;
                }

                if(PCORB(pcop)->bit == HYA_RP1_BIT) {
                    //fprintf(stderr, "  isBankInstruction - Set RP1\n");
                    return  CLR_BANK_BIT | HYA_RP0_BIT;
                }
                break;

            case POC_BCF:
                if(PCORB(pcop)->bit == HYA_RP0_BIT) {
                    //fprintf(stderr, "  isBankInstruction - Clr RP0\n");
                    return  CLR_BANK_BIT | HYA_RP1_BIT;
                }
                if(PCORB(pcop)->bit == HYA_RP1_BIT) {
                    //fprintf(stderr, "  isBankInstruction - Clr RP1\n");
                    return  CLR_BANK_BIT | HYA_RP1_BIT;
                }
                break;
            default:
                //fprintf(stderr, "  isBankInstruction - Status register is getting Modified by:\n");
                //genericPrint(stderr, pc);
                ;
            }
        }

                }

    return bank;
}
*/

/*-----------------------------------------------------------------*/
/*-----------------------------------------------------------------*/
/*
static void FillFlow(pCodeFlow *pcflow)
{
    pCode *pc;
    int cur_bank;

    if(!isPCFL(pcflow))
        return;

    //  fprintf(stderr, " FillFlow - flow block (seq=%d)\n", pcflow->pc.seq);

    pc = findNextpCode(PCODE(pcflow), PC_OPCODE);

    if(!pc) {
        //fprintf(stderr, " FillFlow - empty flow (seq=%d)\n", pcflow->pc.seq);
        return;
    }

    cur_bank = -1;

    do {
        isBankInstruction(pc);
        pc = pc->next;
    } while (pc && (pc != pcflow->end) && !isPCFL(pc));
    / *
        if(!pc ) {
            fprintf(stderr, "  FillFlow - Bad end of flow\n");
        } else {
            fprintf(stderr, "  FillFlow - Ending flow with\n  ");
            pc->print(stderr,pc);
        }

        fprintf(stderr, "  FillFlow inCond: ");
        dumpCond(pcflow->inCond);
        fprintf(stderr, "  FillFlow outCond: ");
        dumpCond(pcflow->outCond);
        * /
}
*/

/*-----------------------------------------------------------------*/
/*-----------------------------------------------------------------*/
// static void LinkFlow_pCode(pCodeInstruction *from, pCodeInstruction *to)
//{
//     pCodeFlowLink *fromLink, *toLink;
// #if 0
//     fprintf(stderr, "%s: linking ", __FUNCTION__ );
//     if (from) from->pc.print(stderr, &from->pc);
//     else fprintf(stderr, "(null)");
//     fprintf(stderr, " -(%u)-> with -(%u)-> ",
//             from && from->pcflow ? from->pcflow->pc.seq : 0,
//             to && to->pcflow ? to->pcflow->pc.seq : 0);
//     if (to) to->pc.print(stderr, &to->pc);
//     else fprintf(stderr, "(null)");
// #endif
//
//     if (!from || !to)
//         return;
//
//     if(!to->pcflow || !from->pcflow)
//         return;
//
//     fromLink = newpCodeFlowLink(from->pcflow);
//     toLink   = newpCodeFlowLink(to->pcflow);
//
//     addSetIfnotP(&(from->pcflow->to), toLink);   //to->pcflow);
//     addSetIfnotP(&(to->pcflow->from), fromLink); //from->pcflow);
//
// }

/*-----------------------------------------------------------------*
 * void LinkFlow(pBlock *pb)
 *
 * In BuildFlow, the  code has been partitioned into contiguous
 * non-branching segments. In LinkFlow, we determine the execution
 * order of these segments. For example, if one of the segments ends
 * with a skip, then we know that there are two possible flow segments
 * to which control may be passed.
 *-----------------------------------------------------------------*/
// static void LinkFlow(pBlock *pb)
//{
//     pCode *pc=NULL;
//     pCode *pcflow;
//     pCode *pct;
//
//     //fprintf(stderr,"linkflow \n");
//
//     if (!pb) return;
//
//     for( pcflow = findNextpCode(pb->pcHead, PC_FLOW);
//             pcflow != NULL;
//             pcflow = findNextpCode(pcflow->next, PC_FLOW) ) {
//
//         if(!isPCFL(pcflow))
//             fprintf(stderr, "LinkFlow - pcflow is not a flow object ");
//
//         //fprintf(stderr," link: ");
//         //pcflow->print(stderr,pcflow);
//
//         //FillFlow(PCFL(pcflow));
//
//         /* find last instruction in flow */
//         pc = findPrevInstruction (PCFL(pcflow)->end);
//         if (!pc) {
//             fprintf(stderr, "%s: flow without end (%u)?\n",
//                     __FUNCTION__, pcflow->seq );
//             continue;
//         }
//
//         //fprintf(stderr, "LinkFlow - flow block (seq=%d) ", pcflow->seq);
//         //pc->print(stderr, pc);
//         if(isPCI_SKIP(pc)) {
//             //fprintf(stderr, "ends with skip\n");
//             //pc->print(stderr,pc);
//             pct=findNextInstruction(pc->next);
//             LinkFlow_pCode(PCI(pc),PCI(pct));
//             pct=findNextInstruction(pct->next);
//             LinkFlow_pCode(PCI(pc),PCI(pct));
//             continue;
//         }
//
//         if(isPCI_BRANCH(pc)) {
//             pCodeOpLabel *pcol = PCOLAB(PCI(pc)->pcop);
//
//             //fprintf(stderr, "ends with branch\n  ");
//             //pc->print(stderr,pc);
//
//             if(!(pcol && isPCOLAB(pcol))) {
//                 if((PCI(pc)->op != POC_RETL)
//                         && (PCI(pc)->op != POC_RET)
//                         && (PCI(pc)->op != POC_CALL)
//                         && (PCI(pc)->op != POC_RETI) )
//                 {
//                     pc->print(stderr,pc);
//                     fprintf(stderr, "ERROR: %s, branch instruction doesn't have label\n",__FUNCTION__);
//                 }
//             } else {
//
//                 if( (pct = findLabelinpBlock(pb,pcol)) != NULL)
//                     LinkFlow_pCode(PCI(pc),PCI(pct));
//                 else
//                     fprintf(stderr, "ERROR: %s, couldn't find label. key=%d,lab=%s\n",
//                             __FUNCTION__,pcol->key,((PCOP(pcol)->name)?PCOP(pcol)->name:"-"));
//                 //fprintf(stderr,"newpCodeOpLabel: key=%d, name=%s\n",key,((s)?s:""));
//             }
//             /* link CALLs to next instruction */
//             if (PCI(pc)->op != POC_CALL) continue;
//         }
//
//         if(isPCI(pc)) {
//             //fprintf(stderr, "ends with non-branching instruction:\n");
//             //pc->print(stderr,pc);
//
//             LinkFlow_pCode(PCI(pc),PCI(findNextInstruction(pc->next)));
//
//             continue;
//         }
//
//         if(pc) {
//             //fprintf(stderr, "ends with unknown\n");
//             //pc->print(stderr,pc);
//             continue;
//         }
//
//         fprintf(stderr, "ends with nothing: ERROR\n");
//
//     }
// }

static void pCodeReplace(pCode *old, pCode *newp)
{
    pCodeInsertAfter(old, newp);

    /* special handling for pCodeInstructions */
    if (isPCI(newp) && isPCI(old))
    {
        // assert (!PCI(new)->from && !PCI(new)->to && !PCI(new)->label && /*!PCI(new)->pcflow && */!PCI(new)->cline);
        PCI(newp)->from = PCI(old)->from;
        PCI(newp)->to = PCI(old)->to;
        PCI(newp)->label = PCI(old)->label;
        replaceLabelPtrTabInPb(old->pb, old, newp);
        PCI(newp)->pcflow = PCI(old)->pcflow;
        PCI(newp)->cline = PCI(old)->cline;
    } // if

    // old->destruct (old);
    removepCode(&old);
}

/*-----------------------------------------------------------------*/
/*-----------------------------------------------------------------*/
/*
static void addpCodeComment(pCode *pc, const char *fmt, ...)
{
    va_list ap;
    char buffer[4096];
    pCode *newpc;

    va_start(ap, fmt);
    if (options.verbose || debug_verbose) {
        buffer[0] = ';';
        buffer[1] = ' ';
        vsprintf(&buffer[2], fmt, ap);

        newpc = newpCodeCharP(&buffer[0]); // strdup's the string
        pCodeInsertAfter(pc, newpc);
    }
    va_end(ap);
}
*/

/*-----------------------------------------------------------------*/
/* Inserts a new pCodeInstruction before an existing one           */
/*-----------------------------------------------------------------*/
static void insertPCodeInstruction(pCodeInstruction *pci, pCodeInstruction *new_pci)
{
    pCode *pcprev;

    pcprev = findPrevInstruction(pci->pc.prev);

    pCodeInsertAfter(pci->pc.prev, &new_pci->pc);

    /* Move the label, if there is one */

    if (pci->label)
    {
        new_pci->label = pci->label;
        replaceLabelPtrTabInPb(pci->pc.pb, (pCode *)pci, (pCode *)new_pci);
        pci->label = NULL;
    }

    /* Move the C code comment, if there is one */

    if (pci->cline)
    {
        new_pci->cline = pci->cline;
        pci->cline = NULL;
    }

    /* The new instruction has the same pcflow block */
    new_pci->pcflow = pci->pcflow;

    /* Arrrrg: is pci's previous instruction is a skip, we need to
     * change that into a jump (over pci and the new instruction) ... */
    if (pcprev && isPCI_SKIP(pcprev))
    {
        symbol *lbl = newiTempLabel(NULL);
        pCode *label = newpCodeLabel(NULL, lbl->key);
        pCode *jump = newpCode(POC_JMP, newpCodeOpLabel(NULL, lbl->key));

        pCodeInsertAfter(pcprev, jump);

        // Yuck: Cannot simply replace INCFSZ/INCFSZW/DECFSZ/DECFSZW
        // We replace them with INCF/INCFW/DECF/DECFW followed by 'BTFSS STATUS, Z'
        switch (PCI(pcprev)->op)
        {
        case POC_INSZ:
        case POC_INSZW:
        case POC_DCSZ:
        case POC_DCSZW:
            // These are turned into non-skipping instructions, so
            // insert 'BTFSC STATUS, Z' after pcprev
            pCodeInsertAfter(jump->prev, newpCode(POC_BTSZ, popCopyGPR2Bit(PCOP(&pc_status), HYA_Z_BIT)));
            break;
        default:
            // no special actions required
            break;
        }
        pCodeReplace(pcprev, pCodeInstructionCopy(PCI(pcprev), 1));
        pcprev = NULL;
        pCodeInsertAfter((pCode *)pci, label);
        pBlockMergeLabels(pci->pc.pb);
    }
}

/*-----------------------------------------------------------------*/
/*-----------------------------------------------------------------*/
/*
static int insertBankSel(pCodeInstruction  *pci, const char *name)
{
    pCode *new_pc;

    pCodeOp *pcop;

    // Never BANKSEL STATUS, this breaks all kinds of code (e.g., interrupt handlers).
    if (!strcmp("STATUS", name) || !strcmp("_STATUS", name)) return 0;

    pcop = popCopyReg(PCOR(pci->pcop));
    pcop->type = PO_GPR_REGISTER; // Sometimes the type is set to legacy 8051 - so override it
    if (pcop->name == 0)
        pcop->name = strdup(name);
    new_pc = newpCode(POC_LBSR, pcop);

    insertPCodeInstruction(pci, PCI(new_pc));
    return 1;
}
*/

/*
 * isValidIdChar - check if c may be present in an identifier
 */
/*
static int isValidIdChar (char c)
{
    if (c >= 'a' && c <= 'z') return 1;
    if (c >= 'A' && c <= 'Z') return 1;
    if (c >= '0' && c <= '9') return 1;
    if (c == '_') return 1;
    return 0;
}
*/

/*
 * bankcompare - check if two operand string refer to the same register
 * This functions handles NAME and (NAME + x) in both operands.
 * Returns 1 on same register, 0 on different (or unknown) registers.
 */
/*
static int bankCompare(const char *op1, const char *op2)
{
    int i;

    if (!op1 && !op2) return 0; // both unknown, might be different though!
    if (!op1 || !op2) return 0;

    // find start of operand name
    while (op1[0] == '(' || op1[0] == ' ') op1++;
    while (op2[0] == '(' || op2[0] == ' ') op2++;

    // compare till first non-identifier character
    for (i = 0; (op1[i] == op2[i]) && isValidIdChar(op1[i]); i++);
    if (!isValidIdChar(op1[i]) && !isValidIdChar(op2[i])) return 1;

    // play safe---assume different operands
    return 0;
}
*/

/*
 * Interface to BANKSEL generation.
 * This function should return != 0 iff str1 and str2 denote operands that
 * are known to be allocated into the same bank. Consequently, there will
 * be no BANKSEL emitted if str2 is accessed while str1 has been used to
 * select the current bank just previously.
 *
 * If in doubt, return 0.
 */
/*
static int
HY08A_operandsAllocatedInSameBank(const char *str1, const char *str2) {
    // see glue.c(HY08AprintLocals)

    if (getenv("SDCC_HY08A_SPLIT_LOCALS")) {
        // no clustering applied, each register resides in its own bank
    } else {
        // check whether BOTH names are local registers
        // XXX: This is some kind of shortcut, should be safe...
        // In this model, all r0xXXXX are allocated into a single section
        // per file, so no BANKSEL required if accessing a r0xXXXX after a
        // (different) r0xXXXX. Works great for multi-byte operands.
        if (str1 && str2 && str1[0] == 'r' && str2[0] == 'r') return (1);
    } // if

    // assume operands in different banks
    return (0);
}
*/

/*-----------------------------------------------------------------*/
/*-----------------------------------------------------------------*/
/*
static int sameBank(reg_info *reg, reg_info *previous_reg, const char *new_bank, const char *cur_bank, unsigned max_mask)
{
    if (!cur_bank) return 0;

    if (previous_reg && reg && previous_reg->isFixed && reg->isFixed && ((previous_reg->address & max_mask) == (reg->address & max_mask)))	// only if exists
        return 1;  // if we have address info, we use it for banksel optimization

    // regard '(regname + X)' and '(regname + Y)' as equal
    if (reg && reg->name && bankCompare(reg->name, cur_bank)) return 1;
    if (new_bank && bankCompare(new_bank, cur_bank)) return 1;

    // check allocation policy from glue.c
    if (reg && reg->name && HY08A_operandsAllocatedInSameBank(reg->name, cur_bank)) return 1;
    if (new_bank && HY08A_operandsAllocatedInSameBank(new_bank, cur_bank)) return 1;

    // seems to be a different operand--might be a different bank
    //printf ("BANKSEL from %s to %s/%s\n", cur_bank, reg->name, new_bank);
    return 0;
}
*/

/*-----------------------------------------------------------------*/
/*-----------------------------------------------------------------*/
// 2016 .. we don't use bank at small model
/*
static void FixRegisterBanking(pBlock *pb)
{
    pCode *pc;
    pCodeInstruction *pci;
    reg_info *reg;
    reg_info *previous_reg;		// contains the previous variable access info
    const char *cur_bank, *new_bank;
    unsigned cur_mask, new_mask, max_mask;
    int allRAMmshared;

    if (!pb) return;

    max_mask = HY08A_getPART()->bankMask;
    cur_mask = max_mask;
    cur_bank = NULL;
    previous_reg = NULL;

    allRAMmshared = HY08A_allRAMShared();

    for (pc = pb->pcHead; pc; pc = pc->next)
    {
        // this one has a label---might check bank at all jumps here...
        if (isPCI(pc) && (PCI(pc)->label || PCI(pc)->op == POC_CALL)) {
            addpCodeComment(pc->prev, "BANKOPT3 drop assumptions: PCI with label or call found");
            previous_reg = NULL;
            cur_bank = NULL; // start new flow
            cur_mask = max_mask;
        }

        // this one is/might be a label or BANKSEL---assume nothing
        if (isPCL(pc) || isPCASMDIR(pc)) {
            addpCodeComment(pc->prev, "BANKOPT4 drop assumptions: label or ASMDIR found");
            previous_reg = NULL;
            cur_bank = NULL;
            cur_mask = max_mask;
        }

        // this one modifies STATUS
        // XXX: this should be checked, but usually BANKSELs are not done this way in generated code

        if (isPCI(pc)) {
            pci = PCI(pc);
            if ((pci->inCond | pci->outCond) & PCC_REGISTER) {
                // might need a BANKSEL
                reg = getRegFromInstruction(pc);

                if (reg) {
                    new_bank = reg->name;
                    // reg->alias == 0: reg is in only one bank, we do not know which (may be any bank)
                    // reg->alias != 0: reg is in 2/4/8/2**N banks, we select one of them
                    new_mask = reg->alias;
                } else if (pci->pcop && pci->pcop->name) {
                    new_bank = pci->pcop->name;
                    new_mask = 0; // unknown, assume worst case
                } else {
                    assert(!"Could not get register from instruction.");
                    new_bank = "UNKNOWN";
                    new_mask = 0; // unknown, assume worst case
                }

                // optimizations...
                // XXX: add switch to disable these
                if (1) {
                    // reg present in all banks possibly selected?
                    if (new_mask == max_mask || (cur_mask && ((new_mask & cur_mask) == cur_mask))) {
                        // no BANKSEL required
                        addpCodeComment(pc->prev, "BANKOPT1 BANKSEL dropped; %s present in all of %s's banks", new_bank, cur_bank);
                        continue;
                    }

                    // only one bank of memory and no SFR accessed?
                    // XXX: We can do better with fixed registers.
                    if (allRAMmshared && reg && (reg->type != REG_SFR) && (!reg->isFixed)) {
                        // no BANKSEL required
                        addpCodeComment(pc->prev, "BANKOPT1b BANKSEL dropped; %s present in all (of %s's) banks", new_bank, cur_bank);
                        continue;
                    }

                    if (sameBank(reg, previous_reg, new_bank, cur_bank, max_mask)) {
                        // no BANKSEL required
                        addpCodeComment(pc->prev, "BANKOPT2 BANKSEL dropped; %s present in same bank as %s", new_bank, cur_bank);
                        continue;
                    }
                } // if

                if (insertBankSel(pci, new_bank)) {
                    cur_mask = new_mask;
                    cur_bank = new_bank;
                    previous_reg = reg;
                } // if
            } // if
        } // if
    } // for
}
*/

/*-----------------------------------------------------------------*/

// 3 kinds of instructions may modify temp reg

static inline int ispCodeSTA(pCode *pc)
{
    if (!isPCI(pc))
        return 0;
    if ((PCI(pc)->op == POC_MVWF) && PCI(pc)->pcop->type == PO_W) // very dummy!!
        return 3;
    if (((PCI(pc)->op == POC_MVWF) || PCI(pc)->op == POC_CLRF || PCI(pc)->op == POC_SETF)) // limit to MVWF
        return 1;
    if (((PCI(pc)->op == POC_INF) || PCI(pc)->op == POC_DECF) && PCI(pc)->pcop->type != PO_W &&
        (!PCI(pc->prev) || !isPCI(pc->prev) || !PCI(pc->prev)->isSkip || !(PCI(pc->prev)->outCond & PCC_REGISTER)) // if skip no modify register, it is applicable
        )                                                                                                          // limit to MVWF
        return 1;

    if (((PCI(pc)->op == POC_INF) || PCI(pc)->op == POC_DECF) && PCI(pc)->pcop->type != PO_W
        //(!PCI(pc->prev) || !isPCI(pc->prev) || !PCI(pc->prev)->isSkip || !(PCI(pc->prev)->outCond & PCC_REGISTER)) // if skip no modify register, it is applicable
        ) // limit to MVWF
        return 1;
    if (PCI(pc)->pcop2 != NULL) // MVFF
        return 2;

    // // 2021 another type, from w to f
    // but most instructions will effect Z/C flags, too complex and we skip it.
    // if( PCI(pc)->op == POC_ADDWF || PCI(pc)->op == POC_SUBFWF || PCI(pc)->op == POC_ADCWF ||
    //        PCI(pc)->op == POC_XORF || PCI(pc)->op == POC_IORWF || PCI(pc)->op == POC_ANDWF ||
    //        PCI(pc)->op == POC_COMF ||
    //        PCI(pc)->op == POC_RRCF || PCI(pc)->op == POC_RLCF || PCI(pc)->op == POC_SBCFWF)
    //     {
    //         if(pc->next)
    //         {
    //             if(isPCI(pc->next)&& PCI(pc->next)->isSkip) return 0;
    //             if(pc->next->next)
    //             {
    //                 if(isPCI(pc->next->next)&& PCI(pc->next->next)->isSkip)
    //                     return 0;
    //                 if(pc->next->next->next)
    //                 {
    //                     if(isPCI(pc->next->next->next)&& PCI(pc->next->next->next)->isSkip)
    //                         return 0;
    //                 }
    //             }
    //         }

    //        return 4;
    //     }

    return 0;
}

static inline int ispCodeLDA(pCode *pc)
{
    if (isPCI(pc) && PCI(pc)->outCond == PCC_W && !PCI(pc)->isBranch) // RETL not count
        return 1;
    // if (isPCI(pc) && PCI(pc)->pcop2 != NULL) // MVFF
    //	return 2;
    return 0;
}
static bool compare_temp_pcop(pCodeOp *pcop, int idx, reg_info *r, int index)
{
    if (!pcop)
        return FALSE;
    if (!r)
    {
        if (pcop->type == PO_GPR_BIT // we have bit var here!! the offset is the same
            || pcop->type == PO_GPR_TEMP)
        {
            if (!PCOR(pcop)->r)
                return FALSE;
            if (PCOR(pcop)->r->rIdx == idx)
                return TRUE;
        }
        else if (pcop->type == PO_DIR && pcop->name && pcop->name[0] == 'r')
        {
            fprintf(stderr, "strange dir op %s\n", pcop->name);
        }
    }
    else
    {
        if (pcop->type == PO_DIR && !STRCASECMP(pcop->name, r->name) // we use name compare here
            && PCOR(pcop)->instance == index)
            return TRUE;
    }
    return FALSE;
}

static pCode *findNextInstructionStaTemp(pCode *pci, reg_info *r, int index)
{
    pCode *pc = pci;

    while (pc)
    {

        // debug

        int statype = ispCodeSTA(pc);
        if (!statype)
        {
            pc = pc->next;
            continue;
        }
        if (statype == 3)
        {
            removepCode(&pc);
            continue;
        }
        if (!r)
        {

            if (((statype == 1) && PCI(pc)->pcop->type == PO_GPR_TEMP && PCOR(PCI(pc)->pcop)->r->rIdx >= 0x1000 // only > 100 is t
                 ) ||
                (statype == 2 && PCI(pc)->pcop2->type == PO_GPR_TEMP && PCOR(PCI(pc)->pcop2)->r->rIdx >= 0x1000 // only > 100 is t
                 ) ||
                ((statype == 1) && PCI(pc)->pcop->type == PO_PRODL) ||
                ((statype == 1) && PCI(pc)->pcop->type == PO_PRODH))
                return pc;
        }
        else
        {
            if (((statype == 1) && compare_temp_pcop(PCI(pc)->pcop, 0, r, index)) ||
                (statype == 2 && compare_temp_pcop(PCI(pc)->pcop, 0, r, index)))
                return pc;
        }

        pc = pc->next;
    }

    // fprintf(stderr,"Couldn't find instruction\n");
    return NULL;
}
static pCode *findNextInstructionMVLANDMVWF(pCode *pci)
{
    pCode *pc = pci;

    while (pc)
    {

        // debug
        if (pc->prev && isPCI(pc->prev) && PCI(pc->prev)->isSkip)
        {
            pc = pc->next;
            continue;
        }

        if (isPCI(pc) && PCI(pc)->op == POC_MVL &&
            isPCI(pc->next) && PCI(pc->next)->label == NULL &&
            PCI(pc->next)->op == POC_MVWF &&
            PCI(pc->next)->pcop->type == PO_GPR_TEMP)
        {
            return pc;
        }
        pc = pc->next;
    }
    return NULL;
}
static pCode *findNextInstructionCLRTEMPNOSKIP(pCode *pci)
{
    pCode *pc = pci;

    while (pc)
    {

        // debug
        if (pc->prev && isPCI(pc->prev) && PCI(pc->prev)->isSkip)
        {
            pc = pc->next;
            continue;
        }

        if (isPCI(pc) && PCI(pc)->op == POC_CLRF &&
            PCI(pc)->pcop->type == PO_GPR_TEMP)
        {
            return pc;
        }
        pc = pc->next;
    }
    return NULL;
}

static pCode *findNextInstructionSETFTEMPNOSKIP(pCode *pci)
{
    pCode *pc = pci;

    while (pc)
    {

        // debug
        if (pc->prev && isPCI(pc->prev) && PCI(pc->prev)->isSkip)
        {
            pc = pc->next;
            continue;
        }

        if (isPCI(pc) && PCI(pc)->op == POC_SETF &&
            PCI(pc)->pcop->type == PO_GPR_TEMP)
        {
            return pc;
        }
        pc = pc->next;
    }
    return NULL;
}
static pCode *findNextInstructionLDPR(pCode *pci)
{
    pCode *pc = pci;

    while (pc)
    {

        // debug
        if (pc->prev && isPCI(pc->prev) && PCI(pc->prev)->isSkip)
        {
            pc = pc->next;
            continue;
        }

        if (isPCI(pc) && PCI(pc)->op == POC_LDPR)
        {
            return pc;
        }
        pc = pc->next;
    }
    return NULL;
}
static pCode *findNextInstructionFSR0L(pCode *pci)
{
    pCode *pc = pci;

    while (pc)
    {

        // debug
        if (pc->prev && isPCI(pc->prev) && PCI(pc->prev)->isSkip)
        {
            pc = pc->next;
            continue;
        }

        if (isPCI(pc) && PCI(pc)->op == POC_MVWF && PCI(pc)->pcop->type == PO_FSR0L &&
            isPCI(pc->prev) && (PCI(pc->prev)->op == POC_MVFW || PCI(pc->prev)->op == POC_MVL))
        {
            return pc;
        }
        pc = pc->next;
    }
    return NULL;
}
static pCode *findNextInstructionFSR0H(pCode *pci)
{
    pCode *pc = pci;

    while (pc)
    {

        // debug
        if (pc->prev && isPCI(pc->prev) && PCI(pc->prev)->isSkip)
        {
            pc = pc->next;
            continue;
        }

        if (isPCI(pc) && PCI(pc)->op == POC_MVWF && PCI(pc)->pcop->type == PO_FSR0H &&
            isPCI(pc->prev) && (PCI(pc->prev)->op == POC_MVFW || PCI(pc->prev)->op == POC_MVL))
        {
            return pc;
        }
        pc = pc->next;
    }
    return NULL;
}
// see if it relates
static bool condRelates(pCode *prev, pCode *next)
{
    switch (PCI(next)->pcop->type)
    {
    case PO_GPR_BIT:
        if (PCORB(PCI(next)->pcop)->pcor.r && PCORB(PCI(next)->pcop)->pcor.r->pc_type == PO_STATUS)
        {
            switch (PCORB(PCI(next)->pcop)->bit)
            {
            case 0: // Z
                if (PCI(prev)->outCond & PCC_Z)
                    return true;
                return false;
            case 1: // OV
                if (PCI(prev)->outCond & PCC_O)
                    return true;
                return false;
            case 4: // C
                if (PCI(prev)->outCond & PCC_C)
                    return true;
                return false;
            default:
                return true;
            }
        }
    default:
        break;
    }
    return false;
}
static pCode *findNextInstructionLdaTemp(pCode *pci)
{
    pCode *pc = pci;

    while (pc)
    {
        // debug
        if (isPCI(pc) && PCI(pc)->op == POC_MVL) // if MVL next is skip , it is not good to remove
        {
            // MVL do not have effects on flags unless next reference WREG directly
            if (pc->next && isPCI(pc->next) && isPCI_SKIP(PCI(pc->next)) && ((PCI(pc->next)->pcop->type == PO_W) || (pc->next->next && isPCI(pc->next->next) && PCI(pc->next->next)->op == POC_MVL) || // dual MVL, skip
                                                                             ((pc->prev) && isPCI(pc->prev) && condRelates(pc->prev, pc->next))))
            {

                pc = pc->next;
                continue;
            }
            if (pc->prev && isPCI(pc->prev) && isPCI_SKIP(PCI(pc->prev)))
            {
                pc = pc->next;
                continue;
            }
            return pc; // also MVL
        }
        int statype = ispCodeLDA(pc);
        if (!statype)
        {
            pc = pc->next;
            continue;
        }
        // if (!r)
        // {
        if (statype == 1 && ((PCI(pc)->pcop->type == PO_GPR_TEMP
                              //&& PCOR(PCI(pc)->pcop)->r->rIdx >= 0x1000 // only > 100 is t
                              ) ||
                             PCI(pc)->pcop->type == PO_PRODH ||
                             PCI(pc)->pcop->type == PO_PRODL || // read of PRODH/L can be skipped
                             PCI(pc)->pcop->type == PO_INDF ||
                             ((PCI(pc)->pcop->type == PO_IMMEDIATE) // LDA count DIR?
                              && PCOI(PCI(pc)->pcop)->r &&
                              !PCOI(PCI(pc)->pcop)->r->isVolatile) ||
                             ((PCI(pc)->pcop->type == PO_DIR) // LDA count DIR?
                              && PCOR(PCI(pc)->pcop)->r &&
                              !PCOR(PCI(pc)->pcop)->r->isVolatile)))
            return pc;
        // }
        // else
        // {
        //     if ((statype == 1 && compare_temp_pcop(PCI(pc)->pcop, 0, NULL, 0)) ||
        //         (statype == 2 && compare_temp_pcop(PCI(pc)->pcop, 0, NULL, 0)))
        //         return pc;
        // }

        pc = pc->next;
    }

    // fprintf(stderr,"Couldn't find instruction\n");
    return NULL;
}

// remove no use ram write

// static bool pcodeInSearched(pCode *table[], pCode *pc, int depth)
// {
//     int i;
//     for (i = 0; i < depth; i++)
//         if (table[i] == pc)
//             return TRUE;
//     return FALSE;
// }
// next instructions, if PCL set with *pcallret is set, the next instruction should be *pcallret
// if call label, the pcallret will be set to the next instruction
// please see pCall , tricky one.
static int findNextInstructions(pCode *pc, pCode **pc1, pCode **pc2, pCode ***switchcases, pCode **pcallret) // int *waitret)
{
    // HY_OPCODE ii;
    int ii;
    // when jump table, here changes PCL ONLY
    // when PCL changes, it could be jump table or PCALL
    if (PCI(pc)->op == POC_RET || PCI(pc)->op == POC_RETI || PCI(pc)->op == POC_RETL)
    {
        *pc1 = NULL;
        *pc2 = NULL;
        return 0;
    }
    // jump table uses MVWF, pcall uses MVFF

    if (ispCodeSTA(pc) == 2 && PCI(pc)->pcop->type == PO_PCL)
    {
        if (*pcallret == NULL)
        {
            fprintf(stderr, "internal error, %s:%d not known style of PCL operation, abort.\n", __FUNCTION__, __LINE__);
            exit(-300);
        }
        *pc1 = *pcallret;
        *pcallret = NULL;
        *pc2 = NULL;
        return 1;
    }
    if ((ispCodeSTA(pc) == 1 && (PCI(pc)->pcop->type == PO_PCL))) //||(ispCodeSTA(pc)==2 && (PCI(pc)->pcop2->type == PO_PCL)))
    {
        int i = 0;
        int j;
        pCode *pcj;
        // if (!*waitret)
        //{
        //	*pc1 = NULL;
        //	*pc2 = NULL;
        //	return 0;
        // }
        //  now is case switch
        pcj = pc;
        // we find how many are they first
        while (pcj = findNextInstruction(pcj->next))
        {
            /*if (PCI(pcj)->op == MOC_BANK)
                continue;*/
            if (PCI(pcj)->op == POC_JMP || PCI(pcj)->op == POC_RJ) // we use RJ/JMP
            {
                if (PCI(pcj)->pcop->type == PO_LABEL)
                {
                    i++;
                    continue;
                }
            }
            break;
        }
        *switchcases = (pCode **)Safe_alloc(sizeof(pCode *) * i);

        pcj = pc;
        j = 0;
        // we find how many are they first
        while (pcj = findNextInstruction(pcj->next))
        {
            /*if (PCI(pcj)->op == MOC_BANK)
                continue;*/
            if (PCI(pcj)->op == POC_JMP || PCI(pcj)->op == POC_RJ)
            {
                if (PCI(pcj)->pcop->type == PO_LABEL)
                {
                    (*switchcases)[j++] = findNextInstruction(findLabelinpBlock(pcj->pb, PCOLAB(PCI(pcj)->pcop)));
                    //(findLabelinpBlock(pcj->pb, PCOLAB(PCI(pcj)->pcop)));
                    continue;
                }
            }
            break;
        }
        //*waitret = 0; // call+ret ==> waitret=0;
        if (i == 1) // in case 1 and 2
        {
            *pc1 = *switchcases[0];
            *pc2 = NULL;
            Safe_free(*switchcases);
            return 1;
        }
        if (i == 2)
        {
            *pc1 = *switchcases[1];
            *pc2 = *switchcases[2];
            Safe_free(*switchcases);
            return 2;
        }
        *pc1 = NULL;
        *pc2 = NULL;
        return i;
    }
    if (PCI(pc)->op == POC_JMP) // this is also branch, but need special operation
    {
        if (PCI(pc)->pcop->type != PO_LABEL)
        {
            *pc1 = NULL;
            *pc2 = NULL;
            return 0;
        }
        *pc1 = findNextInstruction(findLabelinpBlock(pc->pb, PCOLAB(PCI(pc)->pcop)));

        // it is cause by INLINE?
        //      if (!*pc1)
        //      {
        //          fprintf(stderr, "Internal ERROR at labels %s:%d, please contact HYCON with source code.\n",__FILE__,__LINE__);
        // exit( -__LINE__);
        //      }

        *pc2 = NULL;
        return 1;
    }
    if (PCI(pc)->op == POC_CALL) // here call is for functions only
                                 // pcall, another is from CASE SWITCH
    {
        if (PCI(pc)->pcop->type == PO_LABEL)
        {
            /*if (findNextInstruction(findLabelinpBlock(pc->pb, PCOLAB(PCI(pc)->pcop))) ==
                findNextInstruction(pc->next))
            {
                *waitret = 1;
                *pc1 = findNextInstruction(pc->next);
                *pc2 = NULL;
                if (pc1) return 1;
                return 0;
            }*/
            *pc1 = findNextInstruction(findLabelinpBlock(pc->pb, PCOLAB(PCI(pc)->pcop)));
            *pcallret = findNextInstruction(pc->next);
            *pc2 = NULL;
            return 1;
        }
        // otherwise wait it returns!!
        // consider it as normal?
        *pc1 = findNextInstruction(pc->next);
        *pc2 = NULL;
        return 1;
        // call subroutine will not change the temp
    }
    /*if (PCI(pc)->op == POC_BTSS)
    {
        fprintf(stderr, "btss\n");
    }*/
    if (PCI(pc)->isSkip)
    {
        // 2 branches
        *pc1 = findNextInstruction(pc->next);
        *pc2 = findNextInstruction((*pc1)->next);
        return 2;
    }
    ii = PCI(pc)->op;
    if (ii == POC_JC || ii == POC_JNC || ii == POC_JN || ii == POC_JNN || ii == POC_JZ || ii == POC_JNZ || ii == POC_JO || ii == POC_JNO)
    // if(PCI(pc)->isBranch)
    {
        *pc1 = findNextInstruction(pc->next);
        *pc2 = findNextInstruction(findLabelinpBlock(pc->pb, PCOLAB(PCI(pc)->pcop))); // incase first is comment
        if (!*pc2)
        {
            fprintf(stderr, "warning.. label not found.\n");
        }
        return 2;
    }
    *pc1 = findNextInstruction(pc->next);
    *pc2 = NULL;
    return 1;
}
static int findNextInstructionsCallStp(pCode *pc, pCode **pc1, pCode **pc2, pCode ***switchcases, pCode **pcallret) // int *waitret)
{
    // HY_OPCODE ii;
    int ii;
    // when jump table, here changes PCL ONLY
    // when PCL changes, it could be jump table or PCALL
    if (PCI(pc)->op == POC_RET || PCI(pc)->op == POC_RETI || PCI(pc)->op == POC_RETL)
    {
        *pc1 = NULL;
        *pc2 = NULL;
        return 0;
    }
    // jump table uses MVWF, pcall uses MVFF

    if (ispCodeSTA(pc) == 2 && PCI(pc)->pcop->type == PO_PCL)
    {
        if (*pcallret == NULL)
        {
            fprintf(stderr, "internal error, %s:%d not known style of PCL operation, abort.\n", __FUNCTION__, __LINE__);
            exit(-300);
        }
        *pc1 = *pcallret;
        *pcallret = NULL;
        *pc2 = NULL;
        return 1;
    }
    if ((ispCodeSTA(pc) == 1 && (PCI(pc)->pcop->type == PO_PCL))) //||(ispCodeSTA(pc)==2 && (PCI(pc)->pcop2->type == PO_PCL)))
    {
        int i = 0;
        int j;
        pCode *pcj;
        // if (!*waitret)
        //{
        //	*pc1 = NULL;
        //	*pc2 = NULL;
        //	return 0;
        // }
        //  now is case switch
        pcj = pc;
        // we find how many are they first
        while (pcj = findNextInstruction(pcj->next))
        {
            /*if (PCI(pcj)->op == MOC_BANK)
                continue;*/
            if (PCI(pcj)->op == POC_JMP || PCI(pcj)->op == POC_RJ) // we use RJ/JMP
            {
                if (PCI(pcj)->pcop->type == PO_LABEL)
                {
                    i++;
                    continue;
                }
            }
            break;
        }
        *switchcases = (pCode **)Safe_alloc(sizeof(pCode *) * (i + 1));

        pcj = pc;
        j = 0;
        // we find how many are they first
        while (pcj = findNextInstruction(pcj->next))
        {
            /*if (PCI(pcj)->op == MOC_BANK)
                continue;*/
            if (PCI(pcj)->op == POC_JMP || PCI(pcj)->op == POC_RJ)
            {
                if (PCI(pcj)->pcop->type == PO_LABEL)
                {
                    (*switchcases)[j++] = findNextInstruction(findLabelinpBlock(pcj->pb, PCOLAB(PCI(pcj)->pcop)));
                    //(findLabelinpBlock(pcj->pb, PCOLAB(PCI(pcj)->pcop)));
                    continue;
                }
            }
            break;
        }
        //*waitret = 0; // call+ret ==> waitret=0;
        if (i == 1) // in case 1 and 2
        {
            *pc1 = *switchcases[0];
            *pc2 = NULL;
            Safe_free(*switchcases);
            return 1;
        }
        if (i == 2)
        {
            *pc1 = *switchcases[1];
            *pc2 = *switchcases[2];
            Safe_free(*switchcases);
            return 2;
        }
        *pc1 = NULL;
        *pc2 = NULL;
        return i;
    }
    if (PCI(pc)->op == POC_JMP) // this is also branch, but need special operation
    {
        if (PCI(pc)->pcop->type != PO_LABEL)
        {
            *pc1 = NULL;
            *pc2 = NULL;
            return 0;
        }
        *pc1 = findNextInstruction(findLabelinpBlock(pc->pb, PCOLAB(PCI(pc)->pcop)));

        // it is cause by INLINE?
        //      if (!*pc1)
        //      {
        //          fprintf(stderr, "Internal ERROR at labels %s:%d, please contact HYCON with source code.\n",__FILE__,__LINE__);
        // exit( -__LINE__);
        //      }

        *pc2 = NULL;
        return 1;
    }
    if (PCI(pc)->op == POC_CALL) // here call is for functions only
                                 // pcall, another is from CASE SWITCH
    {
        if (PCI(pc)->pcop->type == PO_LABEL)
        {
            /*if (findNextInstruction(findLabelinpBlock(pc->pb, PCOLAB(PCI(pc)->pcop))) ==
                findNextInstruction(pc->next))
            {
                *waitret = 1;
                *pc1 = findNextInstruction(pc->next);
                *pc2 = NULL;
                if (pc1) return 1;
                return 0;
            }*/
            *pc1 = findNextInstruction(findLabelinpBlock(pc->pb, PCOLAB(PCI(pc)->pcop)));
            *pcallret = findNextInstruction(pc->next);
            *pc2 = NULL;
            return 1;
        }
        // otherwise wait it returns!!
        // consider it as normal?
        *pc1 = findNextInstruction(pc->next);
        *pc2 = NULL;
        return 1;
        // call subroutine will not change the temp
    }
    /*if (PCI(pc)->op == POC_BTSS)
    {
        fprintf(stderr, "btss\n");
    }*/
    if (PCI(pc)->isSkip)
    {
        // 2 branches
        *pc1 = findNextInstruction(pc->next);
        *pc2 = findNextInstruction((*pc1)->next);
        return 2;
    }
    ii = PCI(pc)->op;
    if (ii == POC_JC || ii == POC_JNC || ii == POC_JN || ii == POC_JNN || ii == POC_JZ || ii == POC_JNZ || ii == POC_JO || ii == POC_JNO)
    // if(PCI(pc)->isBranch)
    {
        *pc1 = findNextInstruction(pc->next);
        *pc2 = findNextInstruction(findLabelinpBlock(pc->pb, PCOLAB(PCI(pc)->pcop))); // incase first is comment
        if (!*pc2)
        {
            fprintf(stderr, "warning.. label not found.\n");
        }
        return 2;
    }
    *pc1 = findNextInstruction(pc->next);
    *pc2 = NULL;
    return 1;
}
static pCode *pcpath0[SEARCHMAX];
static int searched0_num = 0;
// tag if tempNotUsed
static int findNextInstructionRefTemp(pCode *pci, int idx, int depth, reg_info *r, int index)
{
    // static pCode *pcpath[SEARCHMAX];
    pCode *pc = pci;
    pCode *pcallret = NULL;

    pCode *pc1 = NULL, *pc2 = NULL, **pcs = NULL;
    int i, j;
    // int waitret = 0;
    //  int depth1=depth;

    if (!pci)
        return 0; // strange

    if (!depth)
    {
        for (i = 0; i < searched0_num; i++)
        {
            pcpath0[i]->auxFlag &= 0xfffffffe;
        }
        searched0_num = 0;
    }
    if (!isPCI(pc))
        pc = findNextInstruction(pc->next);

    while (pc)
    {

        if (searched0_num >= SEARCHMAX)
            return 1; // assume referenced

        // if (pcodeInSearched(pcpath, pc, searched_num))
        //     return 0;
        if ((pc->auxFlag) & 1)
            return 0;

        // pcpath[searched_num++] = pc;
        pc->auxFlag |= 1;
        pcpath0[searched0_num++] = pc;
        depth++;

        if (PCI(pc)->op == POC_MVWF || PCI(pc)->op == POC_CLRF || PCI(pc)->op == POC_SETF)
        {
            if (compare_temp_pcop(PCI(pc)->pcop, idx, r, index))
                return 0; // no use before this assign
        }
        else if (pcMVFFLIKE(pc))
        {
            if (compare_temp_pcop(PCI(pc)->pcop, idx, r, index))
                return 1; // in source means referenced

            if (compare_temp_pcop(PCI(pc)->pcop2, idx, r, index))
                return 0; // no use before this assign
        }
        else if (PCI(pc)->op == POC_LDPR)
        {
            if (compare_temp_pcop(PCI(pc)->pcop, idx, r, index))
                return 1;
        }
        else
        {
            // if(PCI(pc)->op==POC_BTSS)
            //{
            //	fprintf(stderr,"now subb\n");
            // }
            if (PCI(pc)->pcop && PCI(pc)->addrMode != asm_LIT && ((PCI(pc)->inCond & PCC_REGISTER)) && compare_temp_pcop(PCI(pc)->pcop, idx, r, index))
                return 1;
        }

        i = findNextInstructions(pc, &pc1, &pc2, &pcs, &pcallret); // &waitret);

        if (!i)
            return 0;
        if (i > 2)
        {
            int result = 0;
            for (j = 0; j < i; j++)
            {
                result |= findNextInstructionRefTemp(pcs[j], idx, depth, r, index);
                if (result)
                    break;
            }
            Safe_free(pcs);
            return result;
        }
        if (i == 2)
        {
            if (findNextInstructionRefTemp(pc1, idx, depth, r, index))
                return 1;
            return findNextInstructionRefTemp(pc2, idx, depth, r, index);
        }
        pc = pc1;
    }

    // fprintf(stderr,"Couldn't find instruction\n");
    return 0; // no found till the end
}
// we need no-use LDA remove .. though not very important

static pCode *nousewchk[1024];
static int chkNoUseW(pCode *pc, int level)
{
    pCode *pc1, *pc2, **pcn;
    int i, j;
    int result;
    pCode *pcallret = NULL;
    static int nouseWID = 1;
    if (!level)
        nouseWID++;
    do
    {
        // if (level >= 200)
        //     return 0; // give up
        for (j = 0; j < level; j++)
        {
            if (nousewchk[j] == pc)
                return 1; // try other path
        }

        nousewchk[level++] = pc;
        pc = findNextInstruction(pc);

        if (PCI(pc)->noUseWCheckedID == nouseWID)
            return PCI(pc)->noUseWCheckedResult;

        if ((PCI(pc)->inCond & PCC_W) || ((PCI(pc)->inCond & PCC_REGISTER) && PCI(pc)->pcop && PCI(pc)->pcop->type == PO_W)) // referenced, give up
        {
            PCI(pc)->noUseWCheckedID = nouseWID;
            PCI(pc)->noUseWCheckedResult = 0;
            return 0;
        }
        if (PCI(pc)->op == POC_CALL)
        {
            PCI(pc)->noUseWCheckedID = nouseWID;
            if (PCI(pc)->exp_para_num != 65536) // no 0 para means referenced
            {
                PCI(pc)->noUseWCheckedResult = 0;
                return 0;
            }
            PCI(pc)->noUseWCheckedResult = 1;
            return 1;
        }
        if (PCI(pc)->op == POC_RETV || PCI(pc)->op == POC_RETI) // return void, no use
        {
            PCI(pc)->noUseWCheckedID = nouseWID;
            PCI(pc)->noUseWCheckedResult = 1;
            return 1;
        }

        if (PCI(pc)->pcop && PCI(pc)->pcop->type == PO_PLUSW) // it uses W
        {
            PCI(pc)->noUseWCheckedID = nouseWID;
            PCI(pc)->noUseWCheckedResult = 0;
            return 0;
        }

        if (PCI(pc)->pcop2 && PCI(pc)->pcop2->type == PO_PLUSW) // it uses W
        {
            PCI(pc)->noUseWCheckedID = nouseWID;
            PCI(pc)->noUseWCheckedResult = 0;
            return 0;
        }

        if (PCI(pc)->pcop && PCI(pc)->pcop->type == PO_GPR_BIT && PCORB(PCI(pc)->pcop)->pcor.r &&
            PCORB(PCI(pc)->pcop)->pcor.r->rIdx == IDX_PLUSW0)
        {
            PCI(pc)->noUseWCheckedID = nouseWID;
            PCI(pc)->noUseWCheckedResult = 0;
            return 0;
        }

        if (PCI(pc)->outCond & PCC_W) // W && only W ? if w destroyed, it is no use
        {
            PCI(pc)->noUseWCheckedID = nouseWID;
            PCI(pc)->noUseWCheckedResult = 1;
            return 1;
        }

        j = findNextInstructionsCallStp(pc, &pc1, &pc2, &pcn, &pcallret);
        switch (j)
        {
        case 0:
            PCI(pc)->noUseWCheckedID = nouseWID;
            PCI(pc)->noUseWCheckedResult = 0;
            return 0; // give up, external reference
        case 1:
            pc = pc1;
            break;
        case 2:
            PCI(pc)->noUseWCheckedID = nouseWID;
            PCI(pc)->noUseWCheckedResult = chkNoUseW(pc2, level) & chkNoUseW(pc1, level);
            return PCI(pc)->noUseWCheckedResult;
        default:
            result = 1;
            for (i = 0; i < j; i++)
            {
                result &= chkNoUseW(pcn[i], level);
                if (!result)
                {
                    PCI(pc)->noUseWCheckedID = nouseWID;
                    PCI(pc)->noUseWCheckedResult = 0;
                    return 0;
                }
            }
            PCI(pc)->noUseWCheckedID = nouseWID;
            PCI(pc)->noUseWCheckedResult = result;

            return result;
        }
    } while (pc);
    return 0; // if end, it is not allowed
}

// in case ... MVL A MVWF b, ... MVFW b mvwf c ==> we replace mvfw b with mvl A, then B can be optimized
// by other analysis
static int replace_temp_literal_until_modified(pCode *start_pc, pCodeOp *thetemp, pCodeOp *thelit, int level)
{
    static pCode *searched[8192];
    pCode *pc = start_pc;
    int i;
    int replaced_count = 0;
    // int litv = PCOL(thelit)->lit;
    pCode *pc1, *pc2, **pcn;
    pCode *pcallret = NULL;
    int j;
    for (i = 0; i < level; i++)
        if (start_pc == searched[i])
            return replaced_count;

    do
    {
        while (pc && !isPCI(pc))
            pc = pc->next;
        searched[level++] = pc;
        assert(pc);
        if (PCI(pc)->isBranch && thelit->type == PO_LITERAL)
            return replaced_count;                                         // we don't process label
        if (PCI(pc)->label || PCI(pc)->op == POC_JMP || PCI(pc)->isBranch) // op==CALL || (PCI(pc)->isSkip && thelit->type!=PO_IMMEDIATE))
            return replaced_count;                                         // we don't process label
        // which shall be much faster
        if ((pcMVFFLIKE(pc) && pCodeOpCompare(thetemp, PCI(pc)->pcop2)) || (!pcMVFFLIKE(pc) && (PCI(pc)->outCond & PCC_REGISTER) &&
                                                                            pCodeOpCompare(thetemp, PCI(pc)->pcop)))
        {
            return replaced_count;
        }
        if (PCI(pc)->op == POC_MVFW && pCodeOpCompare(thetemp, PCI(pc)->pcop))
        {
            // replace this instruction
            pCode *pc2 = newpCode(POC_MVL, thelit);
            // PCI(pc2)->cline = PCI(pc)->cline; // no need
            // PCI(pc2)->label = PCI(pc)->label;
            pCodeReplace(pc, pc2);
            pc = pc2;
            replaced_count++;
        }
        // else if (PCI(pc)->op == POC_TFSZ && pCodeOpCompare(thetemp, PCI(pc)->pcop))
        // {
        //     pCode *pc2 = pc->next->next;
        //     pCode *pc1 = pc->next;
        //     if(litv==0) // skip both
        //     {
        //         removepCode(&pc1);
        //         removepCode(&pc);

        //     }else
        //     {
        //         removepCode(&pc); // remove single
        //     }
        //     pc=pc2;
        //     replaced_count++;
        // }

        j = (findNextInstructions(pc, &pc1, &pc2, &pcn, &pcallret));
        if (j != 1 && j != 2)
            return replaced_count;
        pc = pc1;
        if (j == 2)
        {
            return replaced_count + replace_temp_literal_until_modified(pc2, thetemp, thelit, level) +
                   replace_temp_literal_until_modified(pc1, thetemp, thelit, level);
        }
        // else
        //{
        //	int k;
        //	for (k = 0; k < j;k++)
        //		replaced_count += replace_temp_literal_until_modified(pcn[k], thetemp, thelit, level);
        //	return replaced_count;
        // }

    } while (pc);
    return replaced_count;
}

static int ReplaceMVL2FollowingMVFW(pBlock *pb)
{
    pCode *pc_mvl;
    //	int tempid;
    int count = 0;
    // int result = 0;
    pCode *pc;
    // pCode *pcallret = NULL;
    if (!pb || options.nopeep || removed_count >= HY08A_options.ob_count)
        return 0;
    pc_mvl = findNextInstruction(pb->pcHead);
    while ((pc_mvl = findNextInstructionMVLANDMVWF(pc_mvl)) != NULL)
    {

        pc = pc_mvl->next->next;
        count += replace_temp_literal_until_modified(pc, PCI(pc_mvl->next)->pcop, PCI(pc_mvl)->pcop, 0);
        pc_mvl = pc;
    }
    return count;
}

// 2020 add TFSZ
static int ReplaceMVL02FollowingMVFW(pBlock *pb)
{
    pCode *pc_mvl;
    //	int tempid;
    int count = 0;
    pCode *pc;
    if (!pb || options.nopeep || removed_count >= HY08A_options.ob_count)
        return 0;
    pc_mvl = findNextInstruction(pb->pcHead);
    while ((pc_mvl = findNextInstructionCLRTEMPNOSKIP(pc_mvl)) != NULL)
    {

        pc = pc_mvl->next;
        count += replace_temp_literal_until_modified(pc, PCI(pc_mvl)->pcop, newpCodeOpLit(0), 0);
        pc_mvl = pc;
    }
    pc_mvl = findNextInstruction(pb->pcHead);
    while ((pc_mvl = findNextInstructionSETFTEMPNOSKIP(pc_mvl)) != NULL)
    {

        pc = pc_mvl->next;
        count += replace_temp_literal_until_modified(pc, PCI(pc_mvl)->pcop, newpCodeOpLit(0xff), 0);
        pc_mvl = pc;
    }
    return count;
}
// 2023 oct ... sameldpr at final
static int removeSAMELDPR(pBlock *pb)
{
    pCode *pc_ldpr;
    //	int tempid;
    int count = 0;
    // int result = 0;
    pCode *pc;
    // pCode *pcallret = NULL;
    if (!pb || options.nopeep || removed_count >= HY08A_options.ob_count)
        return 0;
    pc_ldpr = findNextInstruction(pb->pcHead);
    while ((pc_ldpr = findNextInstructionLDPR(pc_ldpr)) != NULL)
    {

        for (pc = pc_ldpr->next; pc; pc = pc->next)
        {
            if (isPCI(pc) && (PCI(pc)->isBranch || PCI(pc)->label ||
                              (PCI(pc)->pcop && (

                                                    PCI(pc)->pcop->type == PO_FSR0H ||
                                                    PCI(pc)->pcop->type == PO_FSR0L ||
                                                    PCI(pc)->pcop->type == PO_FSRPTR0 ||
                                                    PCI(pc)->pcop->type == PO_FSR ||
                                                    PCI(pc)->pcop->type == PO_POINC0 ||
                                                    PCI(pc)->pcop->type == PO_PRINC0 ||
                                                    PCI(pc)->pcop->type == PO_POINC ||
                                                    PCI(pc)->pcop->type == PO_PODEC0
                                                    //||
                                                    // PCI(pc)->pcop->type == PO_PRINC2 ||
                                                    // PCI(pc)->pcop->type == PO_PODEC2

                                                    ))))
                break;
            if (isPCI(pc) && PCI(pc)->op == POC_LDPR)
            {
                if (pCodeOpCompare(PCI(pc_ldpr)->pcop, PCI(pc)->pcop))
                {
                    // remove this one
                    removepCode(&pc);

                    count++;

                    continue;
                }
                else
                    break;
            }
        }
        // remove following LDPR until  // simple method
        // different one loaded,
        // or branch
        pc_ldpr = pc_ldpr->next;
    }

    pc_ldpr = findNextInstruction(pb->pcHead);
    while ((pc_ldpr = findNextInstructionFSR0L(pc_ldpr)) != NULL)
    {

        for (pc = pc_ldpr->next; pc; pc = pc->next)
        {
            if (isPCI(pc) && (PCI(pc)->isBranch || PCI(pc)->label || PCI(pc)->op == POC_LDPR ||
                              (PCI(pc)->pcop && (

                                                    PCI(pc)->pcop->type == PO_POINC0 ||
                                                    PCI(pc)->pcop->type == PO_PRINC0 ||
                                                    PCI(pc)->pcop->type == PO_POINC ||
                                                    PCI(pc)->pcop->type == PO_PODEC0
                                                    // if source destroyed, we give up
                                                    || (pCodeOpCompare(PCI(pc_ldpr->prev)->pcop, PCI(pc)->pcop) &&
                                                        (PCI(pc)->outCond & PCC_REGISTER)) // if destroyed , we give up

                                                    //||
                                                    // PCI(pc)->pcop->type == PO_PRINC2 ||
                                                    // PCI(pc)->pcop->type == PO_PODEC2

                                                    ))))
                break;
            if (isPCI(pc) && PCI(pc)->op == POC_MVWF && PCI(pc)->pcop->type == PO_FSR0L)
            {
                if (isPCI(pc->prev) && PCI(pc->prev)->op == PCI(pc_ldpr->prev)->op && pCodeOpCompare(PCI(pc_ldpr->prev)->pcop, PCI(pc->prev)->pcop))
                {
                    // remove this one
                    removepCode(&pc);
                    count++;
                    if (++removed_count >= HY08A_options.ob_count)
                    {
                        options.nopeep = 1;
                        return count;
                    }
                    continue;
                }
                else
                    break;
            }
        }
        // remove following LDPR until  // simple method
        // different one loaded,
        // or branch
        pc_ldpr = pc_ldpr->next;
    }
    pc_ldpr = findNextInstruction(pb->pcHead);
    while ((pc_ldpr = findNextInstructionFSR0H(pc_ldpr)) != NULL)
    {

        for (pc = pc_ldpr->next; pc; pc = pc->next)
        {
            if (isPCI(pc) && (PCI(pc)->isBranch || PCI(pc)->label || PCI(pc)->op == POC_LDPR ||
                              (PCI(pc)->pcop && (

                                                    PCI(pc)->pcop->type == PO_POINC0 ||
                                                    PCI(pc)->pcop->type == PO_PRINC0 ||
                                                    PCI(pc)->pcop->type == PO_POINC ||
                                                    PCI(pc)->pcop->type == PO_PODEC0 || (pCodeOpCompare(PCI(pc_ldpr->prev)->pcop, PCI(pc)->pcop) && (PCI(pc)->outCond & PCC_REGISTER)) // if destroyed , we give up

                                                    //||
                                                    // PCI(pc)->pcop->type == PO_PRINC2 ||
                                                    // PCI(pc)->pcop->type == PO_PODEC2

                                                    ))))
                break;
            if (isPCI(pc) && PCI(pc)->op == POC_MVWF && PCI(pc)->pcop->type == PO_FSR0H)
            {
                if (isPCI(pc->prev) && PCI(pc->prev)->op == PCI(pc_ldpr->prev)->op && pCodeOpCompare(PCI(pc_ldpr->prev)->pcop, PCI(pc->prev)->pcop))
                {
                    // remove this one
                    removepCode(&pc);
                    if (++removed_count >= HY08A_options.ob_count)
                    {
                        options.nopeep = 1;
                        return count;
                    }
                    count++;
                    continue;
                }
                else
                    break;
            }
        }
        // remove following LDPR until  // simple method
        // different one loaded,
        // or branch
        pc_ldpr = pc_ldpr->next;
    }
    return count;
}

static int BlocknoUseLDARemove(pBlock *pb)
{
    pCode *pc_lda;
    //	int tempid;
    int count = 0;
    int result = 0;
    pCode *pcallret = NULL;
    if (!pb || options.nopeep || removed_count >= HY08A_options.ob_count)
        return 0;
    pc_lda = findNextInstruction(pb->pcHead);
    while ((pc_lda = findNextInstructionLdaTemp(pc_lda)) != NULL)
    {
        pCode *pc1, *pc2, **pcn;
        pCode *ldan = findNextInstruction(pc_lda->next);
        pCode *ldap = findPrevInstruction(pc_lda->prev);
        if (ldan == NULL)
            continue;
        // if prev is skip .. it cannot be removed

        // #ifdef _DEBUG
        //         pCode *ppre = pc_lda;
        //         while (ppre != NULL)
        //         {
        //             if (isPCI(ppre) && PCI(ppre)->cline)
        //                 break;
        //             ppre = ppre->prev;
        //         }
        //         if (PCI(ppre)->cline && PCI(ppre)->cline->line_number == 80 && PCI(pc_lda)->op == POC_MVL)
        //         {
        //
        //             fprintf(stderr, "line 80.MVL.\n");
        //         }
        // #endif

        if (ldap && isPCI(ldap) && PCI(ldap)->isSkip)
        {
            pc_lda = ldan;
            continue;
        }
        // if next is MVFF, we skip , it must be some necessary
        if (isPCI(ldan) && (pcMVFFLIKE(ldan) || (PCI(ldan)->op == POC_LDPR && isPCI(ldan->next) &&
                                                 pcMVFFLIKE(ldan->next))))
        {
            pc_lda = ldan;
            continue;
        }

        if ((PCI(ldan)->pcop && PCI(ldan)->pcop->type == PO_PLUSW) || // plusw needs W!!
            (PCI(ldan)->pcop && PCI(ldan)->pcop->type == PO_GPR_BIT && PCORB(PCI(ldan)->pcop)->pcor.r &&
             (PCORB(PCI(ldan)->pcop)->pcor.r->rIdx == IDX_PLUSW0 || PCORB(PCI(ldan)->pcop)->pcor.r->rIdx == IDX_WREG)) ||
            ((PCI(ldan)->pcop2) && PCI(ldan)->pcop2->type == PO_PLUSW) || ((PCI(ldan)->pcop) && PCI(ldan)->pcop->type == PO_W) // WREG cannot optimize!!!
        )
        {
            pc_lda = ldan;
            continue;
        }

        int j = (findNextInstructions(pc_lda, &pc1, &pc2, &pcn, &pcallret));

        if (j == 0)
        {
            pc_lda = pc_lda->next;
            continue;
        }
        if (j == 1)
        {
            result = chkNoUseW(pc1, 0);
        }
        else if (j == 2)
            result = chkNoUseW(pc1, 0) & chkNoUseW(pc2, 0);
        else
        {
            int i;
            result = 1;
            for (i = 0; i < j; i++)
            {
                result &= chkNoUseW(pcn[i], 0);
                if (!result)
                    break;
            }
        }
        if (!result)
        {
            pc_lda = pc_lda->next;
            continue;
        }
        // if(removed_count==11)
        //     fprintf(stderr,"11\n");
        //  here we can remove that LDA?
        if (pc_lda->pb == NULL)
            pc_lda->pb = pb;
        removepCode(&pc_lda);
        ++count;
        if (++removed_count >= HY08A_options.ob_count)
        {
            options.nopeep = 1;
            return count;
        }
    }
    return count;
}

// if temp STA means the F is not used in the following code,
// the STA can be removed

// 2020 sta add clr btss decf

static int BlockTempSTARemove(pBlock *pb, reg_info *r, int index)
{

    pCode *pc_sta;
    int tempid;
    int count = 0;
    // int i;
    // static int time=0;
    // if(ana_stage>=2)
    // return 0;
    // if(++time>0)
    // return 0;
    if (!pb || options.nopeep || removed_count >= HY08A_options.ob_count) // || pb->call_level >= ms_pcstack_level)
        return 0;
    pc_sta = findNextInstruction(pb->pcHead);
    if (!pc_sta)
        return 0;
    DFPRINTF((stderr, " Trying removing STA at pBlock: %c\n", getpBlock_dbName(pb)));

    // head will not be an op code (yet)
    pc_sta = pc_sta->prev;
    while (pc_sta && (pc_sta = findNextInstructionStaTemp(pc_sta->next, r, index)) != NULL)
    {

        if (PCI(pc_sta)->pcop && (PCI(pc_sta)->pcop->type == PO_PRODL ||
                                  PCI(pc_sta)->pcop->type == PO_PRODH))
        {
            removepCode(&pc_sta);
            continue;
        }
        if (r)
            tempid = 0;
        else
        {
            if (PCI(pc_sta)->pcop2)
                tempid = PCOR(PCI(pc_sta)->pcop2)->r->rIdx;
            else
                tempid = PCOR(PCI(pc_sta)->pcop)->r->rIdx;
        }

        // if(tempid==0x1008)
        //{
        // fprintf(stderr,"check 0x1009 temp\n");
        // }
        //		if (tempid == 4277)
        //			fprintf(stderr, "4277\n");
        if (!findNextInstructionRefTemp(pc_sta->next, tempid, 0, r, index))
        { // remove this STA!!
          // in case there is label!!
          // debug
#ifdef DEBUGOPT1
            char show[200];
            sprintf(show, "op%d_rem_%X", optid++, tempid);
            PCI(pc_sta)->op = MOC_LDAP;
            strcat(PCI(pc_sta)->mnemonic, show);

#else
            varopt *vp = PCI(pc_sta)->pcop2 ? newVarOpt(PCOR(PCI(pc_sta)->pcop2)->r, NULL, 0, -1, 1) : newVarOpt(PCOR(PCI(pc_sta)->pcop)->r, NULL, 0, -1, 1);
            // if (!checkVarOptDouble(pb->opt_list, vp))
            addSet(&pb->opt_list, vp);
            // else
            // Safe_free(vp);
            // if (removed_count == 37)
            // fprintf(stderr, "37");
            // if (PCI(pc_sta)->label)
            //{
            //	// it should have only 1 entry
            //	pCode *tmp = pc_sta->next;
            //	pCode *pp = PCI(pc_sta)->label->pc;
            //	pc_sta->next = pp;
            //	pp->next = tmp;
            //	tmp->prev = pp;
            //	pp->prev = pc_sta;
            //	PCI(pc_sta)->label = NULL;

            //	for (i = pb->labelTabSize - 1; i >= 0; i--) // from new to old
            //		if (pb->labelIndexTab[i] == PCL(pp)->key)
            //		{
            //			pb->labelPtrTab[i] = pp;
            //			break;
            //		}
            //}
            // 2020 special case is prev skip, only bit 7

            if (isPCI(pc_sta->prev) && PCI(pc_sta->prev)->isSkip)
            {
                pCode *pcstapre = pc_sta->prev;
                if (!(PCI(pc_sta->prev)->outCond & PCC_REGISTER))
                {
                    removepCode(&pcstapre); // it can remove directly
                }
                else
                {
                    // otherwise it need to replace
                    pCode *pcn = pc_sta->prev;
                    switch (PCI(pcn)->op)
                    {
                    case POC_INSZ:
                    case POC_INSUZ:
                        pCodeReplace(pcn, newpCode(POC_INF, PCI(pcn)->pcop));
                        break;
                    case POC_DCSUZ:
                    case POC_DCSZ:
                        pCodeReplace(pcn, newpCode(POC_DECF, PCI(pcn)->pcop));
                        break;
                    default:
                        fprintf(stderr, "internal error %s:%d\nplease contact HYCON.\n", __FILE__, __LINE__);
                        return -__LINE__;
                    }
                }
            }
            removepCode(&pc_sta);

#endif
            count++;
            removed_count++;
            if (removed_count >= HY08A_options.ob_count)
            {
                options.nopeep = 1;
                return count;
            }
        }
    }
    return count;

    // return 0;
}

/*-----------------------------------------------------------------*/
static int OptimizepBlock(pBlock *pb)
{
    pCode *pc, *pcprev;
    int matches = 0;

    if (!pb || options.nopeep)
        return 0;

    DFPRINTF((stderr, " Optimizing pBlock: %c\n", getpBlock_dbName(pb)));
    /*
    for(pc = pb->pcHead; pc; pc = pc->next)
    matches += pCodePeepMatchRule(pc);
    */

    pc = findNextInstruction(pb->pcHead);
    if (!pc)
        return 0;

    pcprev = pc->prev;
    do
    {

        if (pCodePeepMatchRule(pc))
        {

            matches++;
            if (options.nopeep)
                return matches;

            if (pcprev)
                pc = findNextInstruction(pcprev->next);
            else
                pc = findNextInstruction(pb->pcHead);
        }
        else
            pc = findNextInstruction(pc->next);

    } while (pc);

    if (matches)
        DFPRINTF((stderr, " Optimizing pBlock: %c - matches=%d\n", getpBlock_dbName(pb), matches));
    return matches;
}

/*-----------------------------------------------------------------*/
/* pBlockRemoveUnusedLabels - remove the pCode labels from the     */
/*-----------------------------------------------------------------*/
static pCode *findInstructionUsingLabel(pCodeLabel *pcl, pCode *pcs)
{
    pCode *pc;

    for (pc = pcs; pc; pc = pc->next)
    {

        if (isPCI(pc))
        {
            if ((PCI(pc)->pcop) &&
                (PCI(pc)->pcop->type == PO_LABEL) &&
                (PCOLAB(PCI(pc)->pcop)->key == pcl->key))
                return pc;
            /*
            if ((PCI(pc)->pcop) &&
                PCI(pc)->op == POC_CALL &&
                isPCI(pc->next) &&
                PCI(pc->next)->op == POC_CALL)
                fprintf(stderr, "strange call\n");
                */
        }
    }

    return NULL;
}

/*-----------------------------------------------------------------*/
/*-----------------------------------------------------------------*/
static void exchangeLabels(pCodeLabel *pcl, pCode *pc)
{

    char *s = NULL;

    if (isPCI(pc) &&
        (PCI(pc)->pcop) &&
        (PCI(pc)->pcop->type == PO_LABEL))
    {

        pCodeOpLabel *pcol = PCOLAB(PCI(pc)->pcop);

        // fprintf(stderr,"changing label key from %d to %d\n",pcol->key, pcl->key);
        if (pcol->pcop.name)
            free(pcol->pcop.name);

        /* If the key is negative, then we (probably) have a label to
         * a function and the name is already defined */

        if (pcl->key > 0)
            sprintf(s = buffer, "_%05d_DS_", pcl->key);
        else
            s = pcl->label;

        // sprintf(buffer,"_%05d_DS_",pcl->key);
        if (!s)
        {
            fprintf(stderr, "ERROR %s:%d function label is null\n", __FUNCTION__, __LINE__);
        }
        pcol->pcop.name = Safe_strdup(s);
        pcol->key = pcl->key;
        // pc->print(stderr,pc);
    }
}

/*-----------------------------------------------------------------*/
/* pBlockRemoveUnusedLabels - remove the pCode labels from the     */
/*                            pCode chain if they're not used.     */
/*-----------------------------------------------------------------*/
void pBlockRemoveUnusedLabels(pBlock *pb)
{
    pCode *pc;
    pCodeLabel *pcl;

    if (!pb || !pb->pcHead)
        return;

    for (pc = pb->pcHead; (pc = findNextInstruction(pc->next)) != NULL;)
    {

        pBranch *pbr = PCI(pc)->label;
        if (pbr && pbr->next)
        {
            pCode *pcd = pb->pcHead;

            // fprintf(stderr, "multiple labels\n");
            // pc->print(stderr,pc);

            pbr = pbr->next;
            while (pbr)
            {

                while ((pcd = findInstructionUsingLabel(PCL(PCI(pc)->label->pc), pcd)) != NULL)
                {
                    // fprintf(stderr,"Used by:\n");
                    // pcd->print(stderr,pcd);

                    exchangeLabels(PCL(pbr->pc), pcd);

                    pcd = pcd->next;
                }
                pbr = pbr->next;
            }
        }
    }

    for (pc = pb->pcHead; pc; pc = pc->next)
    {
        int n;
        if (isPCL(pc)) // Label pcode
            pcl = PCL(pc);
        else if (isPCI(pc) && PCI(pc)->label) // pcode instruction with a label
            pcl = PCL(PCI(pc)->label->pc);
        else
            continue;

        // fprintf(stderr," found  A LABEL !!! key = %d, %s\n", pcl->key,pcl->label);

        /* This pCode is a label, so search the pBlock to see if anyone
         * refers to it */
        /*static int k397 = 0;
        if (pcl->key > 0 && pcl->label && strstr(pcl->label, "397"))
        {
            if(++k397>=9)
                fprintf(stderr, "k397\n");

        }*/
        // if (pcl->label && !STRCASECMP(pcl->label, "_00150_DS_"))
        // fprintf(stderr, "00150\n");
        if ((pcl->key > 0) && (!findInstructionUsingLabel(pcl, pb->pcHead)))
        {
            /* Couldn't find an instruction that refers to this label
             * So, unlink the pCode label from it's pCode chain
             * and destroy the label */
            // fprintf(stderr," removed  A LABEL !!! key = %d, %s\n", pcl->key,pcl->label);

            DFPRINTF((stderr, " !!! REMOVED A LABEL !!! key = %d, %s\n", pcl->key, pcl->label));
            // if (pcl->key>0 && pcl->label && strstr(pcl->label, "167"))
            // fprintf(stderr, "167\n");
            n = getLabelKeyIndexInPb(pb, pcl->key);
            pb->labelPtrTab[n] = NULL;
            if (pc->type == PC_LABEL)
            {
                pCode *pcpre = pc->prev;
                unlinkpCode(pc);
                pCodeLabelDestruct(pc);
                if (pcpre == NULL)
                    pc = pb->pcHead;
                else
                    pc = pcpre;
            }
            else
            {
                unlinkpCodeFromBranch(pc, PCODE(pcl));
                /*if(pc->label->next == NULL && pc->label->pc == NULL) {
                free(pc->label);
                }*/
            }
            // make sure unlink
        }
    }
}

/*-----------------------------------------------------------------*/
/* pBlockMergeLabels - remove the pCode labels from the pCode      */
/*                     chain and put them into pBranches that are  */
/*                     associated with the appropriate pCode       */
/*                     instructions.                               */
/*-----------------------------------------------------------------*/
void pBlockMergeLabels(pBlock *pb)
{
    pBranch *pbr;
    pCode *pc, *pcnext = NULL;
    int tabIndex = -1;

    if (!pb)
        return;

    /* First, Try to remove any unused labels */
    // pBlockRemoveUnusedLabels(pb);

    /* Now loop through the pBlock and merge the labels with the opcodes */

    pc = pb->pcHead;

    while (pc)
    {
        pCode *pcn = findNextInstruction(pc); // we counts label here
        if (pc->pb == NULL)
            pc->pb = pb;
        if (pc->type == PC_LABEL)
        {

            // fprintf(stderr," checking merging label %s\n",PCL(pc)->label);
            // fprintf(stderr,"Checking label key = %d\n",PCL(pc)->key);

            if ((pcnext = findNextInstruction(pc)))
            {
                pCode *pre = pc->prev;
                // Unlink the pCode label from it's pCode chain
                unlinkpCode(pc); // loop uses pcn->next ok
                tabIndex = getLabelKeyIndexInPb(pb, ((pCodeLabel *)(pc))->key);
                pb->labelPtrTab[tabIndex] = pcnext; // shift
                // fprintf(stderr,"Merged label key = %d\n",PCL(pc)->key);
                //  And link it into the instruction's pBranch labels. (Note, since
                //  it's possible to have multiple labels associated with one instruction
                //  we must provide a means to accomodate the additional labels. Thus
                //  the labels are placed into the singly-linked list "label" as
                //  opposed to being a single member of the pCodeInstruction.)

                //_ALLOC(pbr,sizeof(pBranch));
                pbr = Safe_calloc(1, sizeof(pBranch));
                pbr->pc = pc;
                pbr->next = NULL;

                // if (PCI(pcnext)->label == NULL)
                // PCI(pcnext)->label = pbr;
                // else
                // if (PCI(pcnext)->label)
                // fprintf(stderr, "label is not null.\n");
                PCI(pcnext)->label = pBranchAppend(PCI(pcnext)->label, pbr);
                if (pre == NULL)
                {
                    pc = pb->pcHead; // usuall head is func
                    continue;
                }
                pc = pre;
            }
            else
            {
                fprintf(stderr, "WARNING: couldn't associate label %s with an instruction\n", PCL(pc)->label);
            }
        }
        else if (pc->type == PC_CSOURCE)
        {

            /* merge the source line symbolic info into the next instruction */
            if ((pcnext = findNextInstruction(pc)))
            {
                pCode *pcpre = pc->prev;

                // Unlink the pCode label from it's pCode chain

                unlinkpCode(pc);

                PCI(pcnext)->cline = PCCS(pc);

                if (pcpre == NULL)
                {
                    pc = pb->pcHead;
                    continue;
                }
                else
                    pc = pcpre;
                // fprintf(stderr, "merging CSRC\n");
                // genericPrint(stderr,pcnext);
            }
        }
        if (pcn == NULL)
            break;
        pc = pc->next;
    }
    pBlockRemoveUnusedLabels(pb);
}

/*-----------------------------------------------------------------*/

static int removeMVWF2(pBlock *pb)
{
    pCode *pc, *pc1;
    pCodeOp *pcop1;
    pCodeOp *pcop2;
    pCodeFunction *pcf = NULL;
    pCode *pcpre;
    int count = 0;
    // we find pcf first
    for (pc = pb->pcHead; pc; pc = pc->next)
    {
        if (isPCF(pc))
        {
            pcf = (pCodeFunction *)pc;
            break;
        }
    }
    if (pcf == NULL)
    {
        return 0; // there must be a head
    }

    for (pc = pb->pcHead; pc; pc = pc->next)
    {
        if (isPCI(pc) && PCI(pc)->op == POC_MVWF2)
        {
            if (!isPCI(pc->prev) || PCI(pc->prev)->op != POC_MVFW || PCI(pc)->pcop->type != PO_GPR_REGISTER)
            {
                PCI(pc)->op = POC_MVWF; // first is W
                continue;
            }
            pcop1 = PCI(pc)->pcop;
            pcop2 = PCI(pc->prev)->pcop; // target 1, replace to 2
            // 2 is a general temp register,
            replaceTempOfCSym(PCF(pcf), pcop1, pcop2);
            for (pc1 = pc; pc1; pc1 = pc1->next)
            {
                if (isPCI(pc1) && PCI(pc1)->pcop && pCodeOpCompare(PCI(pc1)->pcop, pcop1))
                {
                    PCI(pc1)->pcop = pCodeOpCopy(pcop2);
                }
                else if (isPCI(pc1) && PCI(pc1)->pcop2 && pCodeOpCompare(PCI(pc1)->pcop2, pcop1))
                {
                    PCI(pc1)->pcop2 = pCodeOpCopy(pcop2);
                }
                else if (isPCI(pc1) && PCI(pc1)->pcop && PCI(pc1)->pcop->type == PO_GPR_BIT &&
                         compare_temp_pcop(PCI(pc1)->pcop, PCOR(pcop1)->rIdx, NULL, PCOR(pcop1)->instance))
                {
                    PCOR(PCI(pc1)->pcop)->r = PCOR(pcop2)->r;
                }
            }
            PCI(pc)->op = POC_MVWF; // replace it
            pcpre = pc->prev;
            unlinkpCode(pc);
            pCodeLabelDestruct(pc);
            pc = pcpre;
            pcpre = pc->prev;
            unlinkpCode(pc);
            pCodeLabelDestruct(pc);
            pc = pcpre;
            count++;
        }
    }
    return count;
}

// we need to re-write the fsr0/fsr1/fsr2 operations
extern pCodeOp *get_argument_pcop(int idx);

static int getcsrcWriteFlg(pBlock *pb)
{
    pCode *pc;
    pCode *pcn;
    int i = 0;
    if (pb->cSrcState)
        return 0;
    pb->cSrcState = 1;

    for (pc = pb->pcHead; pc; pc = pc->next)
    {
        if (isPCI(pc) && PCI(pc)->cline)
        {
            for (pcn = pc; pcn; pcn = pcn->next)
            {
                if (isPCI(pcn))
                {
                    if (pcn != pc && PCI(pcn)->cline)
                        break;
                    if (PCI(pcn)->outCond & PCC_REGISTER) // memory modified
                    {
                        PCI(pc)->cline->hasRAMWR = 1;
                        i++;
                        break;
                    }
                }
            }
        }
    }
    return i;
}
static int arrangeCsrc(pBlock *pb)
{
    pCode *pc;
    pCode *pcn;
    int i = 0;
    if (pb->cSrcState != 1)
        return 0;
    for (pc = pb->pcHead; pc; pc = pc->next)
    {
        if (!isPCI(pc))
            continue;
        if (PCI(pc)->cline && !PCI(pc)->cline->hasRAMWR && (PCI(pc)->outCond & PCC_REGISTER))
        {
            pcn = findNextInstruction(pc->next);
            if (pcn && PCI(pcn)->cline == NULL)
            {
                PCI(pcn)->cline = PCI(pc)->cline; // shift 1
                PCI(pc)->cline = NULL;
            }
        }
    }
    return i;
}

// even fsr not reshape, ADCR has to be reshape
static int re_shape_adcr(pBlock *pb)
{
    pCode *pc; // *pc2;
    pCodeOp *pcop;

    // pCodeOp *fsr0l_assign, *fsr0h_assign;
    //	int assign_found;
    reg_info *reg;
    // immed and dir change
    // first step is switch h/l
    for (pc = pb->pcHead; pc; pc = pc->next)
    {
        if (isPCI(pc) && PCI(pc)->pcop)
        {
            pcop = PCI(pc)->pcop;
            switch (pcop->type)
            {
            case PO_GPR_TEMP:
            case PO_DIR:
                reg = PCOR(pcop)->r;
                break;
            case PO_IMMEDIATE:
                reg = PCOI(pcop)->r;
                break;
            case PO_GPR_BIT:
                reg = PCORB(pcop)->pcor.r;
                break;

            default:
                reg = NULL;
                break;
            }
            if (!reg)
                continue;

            if (reg->rIdx == IDX_ADCR)
            {
                // split it
                // only direct
                pCode *pc2;
                // pCodeOp *hop = NULL; // adch op
                if (pcop->type != PO_DIR)
                {
                    fprintf(stderr, "Virtual %s register cannot be used that way.\n", reg->name);
                    exit(-__LINE__);
                }
                switch (PCOR(pcop)->instance)
                {
                case 0:
                    PCI(pc)->pcop = PCOP(&pc_adcrl);
                    break;
                case 1:
                    PCI(pc)->pcop = PCOP(&pc_adcrm);
                    break;
                case 2:
                    PCI(pc)->pcop = PCOP(&pc_adcrh);
                    break;
                default:
                    // it should be MVFW and ADCFW
                    // others should be wrong
                    if (PCI(pc)->op == POC_MVFW)
                    {
                        pCode *pc3 = pc->prev->prev;
                        pCodeOp *pcopb;
                        if (PCI(pc3)->pcop->type != PO_ADCRB)
                        {
                            fprintf(stderr, "Internal error, %s convert error %s:%d.\n", "ADCRHH", __FILE__, __LINE__);
                            exit(-__LINE__);
                        }
                        pc3 = pc3->next;
                        if (PCI(pc3)->op != POC_MVWF)
                        {
                            // there is ADD!!
                            // assume ADCRH not changed yet
                            pcopb = newpCodeOpBit("_ADCRH", 7, 0);
                        }
                        else
                        {
                            pcopb = newpCodeOpBitByOp(PCI(pc3)->pcop, 7, 0);
                            memcpy(&(PCORB(pcopb)->pcor), PCI(pc3)->pcop, sizeof(pCodeOpReg));
                            if (PCI(pc3)->pcop->type == PO_GPR_TEMP)
                                PCORB(pcopb)->pcor.instance = 0;
                            else if (PCI(pc3)->pcop->type == PO_IMMEDIATE)
                            {
                                // memcpy(&(PCORB(pcopb)->pcor), PCOI(PCI(pc3)->pcop), sizeof(pCodeOpReg));
                                pcopb = newpCodeOpBitByOp(PCI(pc3)->pcop, 7, 0);           // keep
                                PCORB(pcopb)->pcor.instance = PCOI(PCI(pc3)->pcop)->index; // use index, not offset
                            }
                            PCORB(pcopb)->subtype = PCI(pc3)->pcop->type;
                            PCORB(pcopb)->pcor.pcop.type = PO_GPR_BIT;
                            PCORB(pcopb)->bit = 7;
                        }
                        if (isPCI(pc->next) && PCI(pc->next)->op == POC_MVWF)
                        {
                            pc2 = newpCode(POC_CLRF, PCI(pc->next)->pcop);
                            pCodeReplace(pc, pc2);
                            pc = pc2->next;
                            pCodeReplace(pc, newpCode(POC_BTSZ, pcopb));
                            pCodeInsertBefore(pc2->next->next, newpCode(POC_SETF, PCI(pc2)->pcop));
                            pc = pc2->next->next; // need bug fix!!
                            break;
                        }
                        pc2 = newpCode(POC_MVL, popGetLit(0));
                        // PCI(pc2)->cline = PCI(pc)->cline;
                        // PCI(pc2)->label = PCI(pc)->label; no need
                        pCodeReplace(pc, pc2);
                        pc = pc2;

                        pCodeInsertBefore(pc->next, newpCode(POC_BTSZ, pcopb));
                        pCodeInsertBefore(pc->next->next, newpCode(POC_MVL, popGetLit(0xff)));
                        break;
                    }

                    else
                    {
                        pCode *pcs = pc;
                        while (pcs)
                        {
                            if (isPCI(pcs) && PCI(pcs)->cline != NULL)
                            {
                                break;
                            }
                            pcs = pcs->prev;
                        }
                        if (pcs && isPCI(pcs) && PCI(pcs)->cline)
                            fprintf(stderr, "ADCR convertion error at %s:%d. Please do not use operators other than =/+= to ADCR.\n",
                                    PCI(pcs)->cline->file_name, PCI(pcs)->cline->line_number);
                        else
                            fprintf(stderr, "Internal error. ADCR conversion error at %s:%d\nPlease do not operators other than +/+= to ADCR!!", __FILE__, __LINE__);
                        exit(-__LINE__);
                        // break;
                    }
                }
            }

            else if (reg->rIdx == IDX_ADCO1)
            {
                // split it
                // only direct
                pCode *pc2;
                // pCodeOp *hop = NULL; // adch op
                if (pcop->type != PO_DIR)
                {
                    fprintf(stderr, "Virtual %s register cannot be used that way.\n", reg->name);
                    exit(-__LINE__);
                }
                switch (PCOR(pcop)->instance)
                {
                case 0:
                    PCI(pc)->pcop = PCOP(&pc_adco1l);
                    break;
                case 1:
                    PCI(pc)->pcop = PCOP(&pc_adco1m);
                    break;
                case 2:
                    PCI(pc)->pcop = PCOP(&pc_adco1h);
                    break;
                default:
                    // it should be MVFW and ADCFW
                    // others should be wrong
                    if (PCI(pc)->op == POC_MVFW)
                    {
                        pCode *pc3 = pc->prev->prev;
                        pCodeOp *pcopb;
                        if (PCI(pc3)->pcop->type != PO_ADCO1B)
                        {
                            fprintf(stderr, "Internal error, %s convert error %s:%d.\n", "ADCO1HH", __FILE__, __LINE__);
                            exit(-__LINE__);
                        }
                        pc3 = pc3->next;
                        if (PCI(pc3)->op != POC_MVWF)
                        {
                            // there is ADD!!
                            // assume ADCRH not changed yet
                            pcopb = newpCodeOpBit("_ADCO1H", 7, 0);
                        }
                        else
                        {
                            pcopb = newpCodeOpBitByOp(PCI(pc3)->pcop, 7, 0);
                            memcpy(&(PCORB(pcopb)->pcor), PCI(pc3)->pcop, sizeof(pCodeOpReg));
                            if (PCI(pc3)->pcop->type == PO_GPR_TEMP)
                                PCORB(pcopb)->pcor.instance = 0;
                            PCORB(pcopb)->subtype = PCI(pc3)->pcop->type;
                            PCORB(pcopb)->pcor.pcop.type = PO_GPR_BIT;
                            PCORB(pcopb)->bit = 7;
                        }
                        if (isPCI(pc->next) && PCI(pc->next)->op == POC_MVWF)
                        {
                            pc2 = newpCode(POC_CLRF, PCI(pc->next)->pcop);
                            pCodeReplace(pc, pc2);
                            pc = pc2->next;
                            pCodeReplace(pc, newpCode(POC_BTSZ, pcopb));
                            pc = pc2;
                            pCodeInsertBefore(pc2->next->next, newpCode(POC_SETF, PCI(pc2)->pcop));
                            break;
                        }
                        pc2 = newpCode(POC_MVL, popGetLit(0));
                        // PCI(pc2)->cline = PCI(pc)->cline;
                        // PCI(pc2)->label = PCI(pc)->label;
                        pCodeReplace(pc, pc2);
                        pc = pc2;

                        pCodeInsertBefore(pc->next, newpCode(POC_BTSZ, pcopb));
                        pCodeInsertBefore(pc->next->next, newpCode(POC_MVL, popGetLit(0xff)));
                        break;
                    }

                    else
                    {
                        fprintf(stderr, "Internal error, ADCO1 conversion error at %s:%d\n", __FILE__, __LINE__);
                        exit(-__LINE__);
                        // break;
                    }
                }
            }
            else if (reg->rIdx == IDX_ADCO2)
            {
                // split it
                // only direct
                pCode *pc2;
                // pCodeOp *hop = NULL; // adch op
                if (pcop->type != PO_DIR)
                {
                    fprintf(stderr, "Virtual %s register cannot be used that way.\n", reg->name);
                    exit(-__LINE__);
                }
                switch (PCOR(pcop)->instance)
                {
                case 0:
                    PCI(pc)->pcop = PCOP(&pc_adco2l);
                    break;
                case 1:
                    PCI(pc)->pcop = PCOP(&pc_adco2m);
                    break;
                case 2:
                    PCI(pc)->pcop = PCOP(&pc_adco2h);
                    break;
                default:
                    // it should be MVFW and ADCFW
                    // others should be wrong
                    if (PCI(pc)->op == POC_MVFW)
                    {
                        pCode *pc3 = pc->prev->prev;
                        pCodeOp *pcopb;
                        if (PCI(pc3)->pcop->type != PO_ADCO2B)
                        {
                            fprintf(stderr, "Internal error, %s convert error %s:%d.\n", "ADCO2HH", __FILE__, __LINE__);
                            exit(-__LINE__);
                        }
                        pc3 = pc3->next;
                        if (PCI(pc3)->op != POC_MVWF)
                        {
                            // there is ADD!!
                            // assume ADCRH not changed yet
                            pcopb = newpCodeOpBit("_ADCO2H", 7, 0);
                        }
                        else
                        {
                            pcopb = newpCodeOpBitByOp(PCI(pc3)->pcop, 7, 0);
                            memcpy(&(PCORB(pcopb)->pcor), PCI(pc3)->pcop, sizeof(pCodeOpReg));
                            if (PCI(pc3)->pcop->type == PO_GPR_TEMP)
                                PCORB(pcopb)->pcor.instance = 0;
                            PCORB(pcopb)->subtype = PCI(pc3)->pcop->type;
                            PCORB(pcopb)->pcor.pcop.type = PO_GPR_BIT;
                            PCORB(pcopb)->bit = 7;
                        }
                        if (isPCI(pc->next) && PCI(pc->next)->op == POC_MVWF)
                        {
                            pc2 = newpCode(POC_CLRF, PCI(pc->next)->pcop);
                            pCodeReplace(pc, pc2);
                            pc = pc2->next;
                            pCodeReplace(pc, newpCode(POC_BTSZ, pcopb));
                            pc = pc2;
                            pCodeInsertBefore(pc2->next->next, newpCode(POC_SETF, PCI(pc2)->pcop));
                            break;
                        }
                        pc2 = newpCode(POC_MVL, popGetLit(0));
                        // PCI(pc2)->cline = PCI(pc)->cline;
                        // PCI(pc2)->label = PCI(pc)->label;
                        pCodeReplace(pc, pc2);
                        pc = pc2;

                        pCodeInsertBefore(pc->next, newpCode(POC_BTSZ, pcopb));
                        pCodeInsertBefore(pc->next->next, newpCode(POC_MVL, popGetLit(0xff)));
                        break;
                    }

                    else
                    {
                        fprintf(stderr, "Internal error, ADCO2 conversion error at %s:%d\n", __FILE__, __LINE__);
                        exit(-__LINE__);
                        // break;
                    }
                }
            }
        }
    }
    return 0;
}

static int re_shape_fsr(pBlock *pb)
{
    // here we change FSR0/1/2 to FSR0/1 and later++ deleted
    pCode *pc; // *pc2;
    pCodeOp *pcop;

    // pCodeOp *fsr0l_assign, *fsr0h_assign;
    //	int assign_found;
    reg_info *reg;
    int useldpr = 0;
    // immed and dir change
    // first step is switch h/l
    for (pc = pb->pcHead; pc; pc = pc->next)
    {
        if (isPCI(pc) && PCI(pc)->pcop)
        {
            pcop = PCI(pc)->pcop;
            switch (pcop->type)
            {
            case PO_GPR_TEMP:
            case PO_DIR:
                reg = PCOR(pcop)->r;
                break;
            case PO_IMMEDIATE:
                reg = PCOI(pcop)->r;
                break;
            case PO_GPR_BIT:
                reg = PCORB(pcop)->pcor.r;
                break;

            default:
                reg = NULL;
                break;
            }
            if (!reg)
                continue;
            if (reg->rIdx == IDX_FSR0PTR ||
                reg->rIdx == IDX_FSR1PTR ||
                reg->rIdx == IDX_FSR2PTR)
            {
                // fprintf(stderr, "FSRPTR found.\n");
                if (pcop->type != PO_DIR)
                {
                    fprintf(stderr, "Internel error .. fsr-ptr not at dir .\n");
                    exit(-2002);
                }
                if (HY08A_getPART()->isEnhancedCore >= 3 && HY08A_getPART()->isEnhancedCore != 5 &&
                    isPCI(pc->prev) && PCI(pc->prev)->pcop &&
                    PCI(pc->prev)->pcop->type == PO_IMMEDIATE &&
                    PCOI(PCI(pc->prev)->pcop)->r && PCOI(PCI(pc->prev)->pcop)->r->isFuncLocal)
                {
                    fprintf(stderr, "Warning: Local variable %s address assign to FSR has to change to static for LDPR instruction.\n", PCOI(PCI(pc->prev)->pcop)->r->name);
                    PCOI(PCI(pc->prev)->pcop)->r->isFuncLocal = 0;
                    // exit(-__LINE__);
                }
                useldpr = 0;
                // we change the name and index
                if (PCOR(pcop)->instance == 1)
                {
                    switch (reg->rIdx)
                    {
                    case IDX_FSR0PTR:
                        PCI(pc)->pcop = ((pCodeOp *)&pc_fsr0h);
                        break;
                    case IDX_FSR1PTR:
                        PCI(pc)->pcop = ((pCodeOp *)&pc_fsr1h);
                        break;
                    case IDX_FSR2PTR:
                        PCI(pc)->pcop = ((pCodeOp *)&pc_fsr2h);
                        break;
                    }
                    PCI(pc)->pcop->type = PO_FSR; // special type
                }
                else if (PCOR(pcop)->instance == 0)
                {

                    switch (reg->rIdx)
                    {
                    case IDX_FSR0PTR:
                        PCI(pc)->pcop = ((pCodeOp *)&pc_fsr0l);
                        if (isPCI(pc->prev) && (PCI(pc->prev)->op == POC_MVL) && (isPCI(pc->next) && PCI(pc->next)->op == POC_MVL) && isPCI(pc->next->next) && PCI(pc->next->next)->pcop->type == PO_DIR &&
                            PCI(pc->prev)->pcop->type == PO_IMMEDIATE)
                        {
                            pCode *p = pc->prev->prev;
                            PCOI(PCI(pc->prev)->pcop)->offset = 0;
                            p->next = newpCode(POC_LDPR, PCI(pc->prev)->pcop);
                            PCI(p->next)->cline = PCI(pc->prev)->cline;
                            PCI(p->next)->label = (PCI(pc->prev)->label);
                            replaceLabelPtrTabInPb(p->pb, pc->prev, p->next);

                            p->next->prev = p;
                            p->next->next = pc->next->next->next;

                            if (pc->next->next->next)
                                pc->next->next->next->prev = p->next;
                            pc = p;
                            useldpr = 1;
                        }
                        break;
                    case IDX_FSR1PTR:
                        PCI(pc)->pcop = ((pCodeOp *)&pc_fsr1l);
                        if (isPCI(pc->prev) && (PCI(pc->prev)->op == POC_MVL) && (isPCI(pc->next) && PCI(pc->next)->op == POC_MVL) && isPCI(pc->next->next) && PCI(pc->next->next)->pcop->type == PO_DIR &&
                            PCI(pc->prev)->pcop->type == PO_IMMEDIATE)
                        {
                            pCode *p = pc->prev->prev;
                            PCOI(PCI(pc->prev)->pcop)->offset = 0;
                            p->next = newpCode2(POC_LDPR, PCI(pc->prev)->pcop, popGetLit(1));
                            PCI(p->next)->cline = PCI(pc->prev)->cline;
                            PCI(p->next)->label = (PCI(pc->prev)->label);
                            replaceLabelPtrTabInPb(p->pb, pc->prev, p->next);

                            p->next->prev = p;
                            p->next->next = pc->next->next->next;

                            if (pc->next->next->next)
                                pc->next->next->next->prev = p->next;
                            pc = p;
                            useldpr = 1;
                        }
                        break;
                    case IDX_FSR2PTR:
                        PCI(pc)->pcop = ((pCodeOp *)&pc_fsr2l);
                        if (isPCI(pc->prev) && (PCI(pc->prev)->op == POC_MVL) && (isPCI(pc->next) && PCI(pc->next)->op == POC_MVL) && isPCI(pc->next->next) && PCI(pc->next->next)->pcop->type == PO_DIR &&
                            PCI(pc->prev)->pcop->type == PO_IMMEDIATE)
                        {
                            pCode *p = pc->prev->prev;
                            PCOI(PCI(pc->prev)->pcop)->offset = 0;
                            p->next = newpCode2(POC_LDPR, PCI(pc->prev)->pcop, popGetLit(2));
                            PCI(p->next)->cline = PCI(pc->prev)->cline;
                            PCI(p->next)->label = (PCI(pc->prev)->label);

                            replaceLabelPtrTabInPb(p->pb, pc->prev, p->next);

                            p->next->prev = p;
                            p->next->next = pc->next->next->next;

                            if (pc->next->next->next)
                                pc->next->next->next->prev = p->next;
                            pc = p;
                            useldpr = 1;
                        }
                        break;
                    }
                    if (!useldpr)
                        PCI(pc)->pcop->type = PO_FSR; // special type
                }
                else
                {
                    PCI(pc)->pcop = get_argument_pcop(4);
                    pc = pc->prev;
                    pc->next = pc->next->next;
                }
            }
        }
    }
    // after change to FSRL/FSRH, we search for POINC, and see if its source is PO_FSRHL

    // for (pc = pb->pcHead; pc; pc = pc->next)
    //{
    //	if (!(isPCI(pc) && PCI(pc)->pcop && PCI(pc)->pcop->type == PO_INDF)) // if found a match, INDF change to INC
    //		continue;
    //		// search back for PO_FSR, twice, there should be 2
    //	assign_found = 0;
    //	for (pc2 = pc; pc2; pc2 = pc2->prev)
    //	{
    //		if(isPCI(pc2) && PCI(pc2)->op==POC_MVWF && )
    //	}
    // }

    return 0;
}
/*-----------------------------------------------------------------*/
static void resetOptimizeCount(char dbName)
{
    pBlock *pb;
    if (!the_pFile)
        return;
    for (pb = the_pFile->pbHead; pb; pb = pb->next)
    {
        if ('*' == dbName || getpBlock_dbName(pb) == dbName)
        {
            pb->lastOptCount = -1;
        }
    }
}
static int replaceFileReg(reg_info *newReg, reg_info *oldReg)
{
    int count = 0;
    pBlock *pb;
    pCode *pc;
    //	symbol *sp;
    // int i,j;
    // memmap * chkms[] = { data,istack };
    if (!the_pFile)
        return 0;

    // for (i = 0; i < 2; i++)
    //{
    //	for (sp = setFirstItem(chkms[i]->syms); sp; sp = setNextItem(chkms[i]->syms))
    //	{
    //		//if (!sp->reqv)
    //		//	continue;
    //		if (sp->nRegs == 0)
    //			continue;
    //		for (j = 0; j < sp->nRegs; j++)
    //			if (sp->regs[j] == oldReg)
    //				sp->regs[j] = newReg;
    //
    //	}
    // }
    for (pb = the_pFile->pbHead; pb; pb = pb->next)
    {

        for (pc = pb->pcHead; pc; pc = pc->next)
        {
            if (!isPCI(pc))
                continue;
            // if  (PCI(pc)->pcop && PCI(pc)->pcop->type == PO_DIR && PCOR(PCI(pc)->pcop)->r == newReg)
            // PCI(pc)->pcop->type = PO_GPR_TEMP;
            if (PCI(pc)->pcop && PCI(pc)->pcop->type == PO_GPR_TEMP && PCOR(PCI(pc)->pcop)->r == oldReg)
            {
                PCI(pc)->pcop = newpCodeOpReg(newReg->rIdx);

                PCI(pc)->pcop->name = newReg->name;
                PCI(pc)->pcop->type = PO_GPR_TEMP;
                PCI(pc)->pcop->flags.parapcop = 1;
                count++;
            }
            if (PCI(pc)->pcop && PCI(pc)->pcop->type == PO_GPR_BIT && PCOR(PCI(pc)->pcop)->r == oldReg)
            {
                PCORB(PCI(pc)->pcop)->pcor.r = newReg;
                PCORB(PCI(pc)->pcop)->pcor.pcop.name = newReg->name;
                PCI(pc)->pcop->flags.parapcop = 1;
                count++;
            }
            if (PCI(pc)->pcop2 && PCI(pc)->pcop2->type == PO_GPR_TEMP && PCOR(PCI(pc)->pcop2)->r == oldReg)
            {
                PCI(pc)->pcop2 = newpCodeOpReg(newReg->rIdx);

                PCI(pc)->pcop2->name = newReg->name;
                PCI(pc)->pcop2->flags.parapcop = 1; // if previously no change, here cannot change!!
                PCI(pc)->pcop2->type = PO_GPR_TEMP;
                count++;
            }
        }
    }
    return count;
}

// macro
#define NXTNOTMVFFPC(pc, pc1)                                                                       \
    for (pc1 = pc->next; pc1 && isPCI(pc1) && !PCI(pc1)->label && pcMVFFLIKE(pc1); pc1 = pc1->next) \
        ;                                                                                           \
    if (!pc1 || !isPCI(pc1) || PCI(pc1)->label)                                                     \
        continue;
// this optimize should be strait forward
// 2018 Feb, fix optimize remove csrc problem

// 2 cases :
// cond 1: mvwf a; mvfw b ; mvwf c ; mvfw a ==> remove final with mvff ==> no need temp
// cond 2: addwf t; mvfw b ; mvwf c ; mvfw t ==> change op , with mvff ==> need be temp
// 2021 AUG: try to optimize consecutive mvff
static int mvxxOptimize(pBlock *pb, int core) // core is A or D..we skip C now
{
    // assume we use A
    int count = 0;
    pCode *pc, *pc1, *pc2, *pc3, *pc5;
    if (options.nopeep)
        return 0;
    for (pc = pb->pcHead; pc; pc = pc->next)
    {
        NXTNOTMVFFPC(pc, pc1);
        NXTNOTMVFFPC(pc1, pc2);
        NXTNOTMVFFPC(pc2, pc3);

        // more mvff?

        if (((PCI(pc)->op == POC_MVWF) || ((PCI(pc)->op == POC_INF || PCI(pc)->op == POC_DECF || PCI(pc)->op == POC_COMF ||
                                            PCI(pc)->op == POC_ADDWF || PCI(pc)->op == POC_SUBFWF ||
                                            PCI(pc)->op == POC_ADCWF || PCI(pc)->op == POC_SBCFWF) &&
                                           PCI(pc)->pcop->type == PO_GPR_TEMP)) &&
            PCI(pc1)->op == POC_MVFW && PCI(pc2)->op == POC_MVWF &&
            PCI(pc3)->op == POC_MVFW && PCI(pc1)->label == NULL && PCI(pc2)->label == NULL &&
            PCI(pc3)->label == NULL && pCodeOpCompare(PCI(pc)->pcop, PCI(pc3)->pcop) && !pCodeOpCompare(PCI(pc1)->pcop, PCI(pc2)->pcop))
        {
            // we change the second to MVFF and remove pc3
            // pc4 = pc3->next;

            if (pc->prev && isPCI(pc->prev) && PCI(pc->prev)->isSkip)
                continue;
            if (PCI(pc)->op == POC_INF || PCI(pc)->op == POC_DECF || PCI(pc)->op == POC_ADDWF || PCI(pc)->op == POC_SUBFWF ||
                PCI(pc)->op == POC_COMF || PCI(pc)->op == POC_ADCWF || PCI(pc)->op == POC_SBCFWF)
            {
                pCode *tmpp = pc->prev;
                if (PCI(pc)->pcop->type != PO_GPR_TEMP ||
                    // findNextInstructionRefTemp(pc3->next, PCOR(PCI(pc3)->pcop)->rIdx, 0, PCOR(PCI(pc3)->pcop)->r, PCOR(PCI(pc3)->pcop)->instance)||
                    chkReferencedBeforeChange(pc3->next, PCOR(PCI(pc)->pcop)->r->rIdx, NULL, 0, 0))
                    continue;
                switch (PCI(pc)->op)
                {
                case POC_COMF:
                    pCodeReplace(pc, newpCode(POC_COMFW, PCI(pc)->pcop));
                    break;
                case POC_INF:
                    pCodeReplace(pc, newpCode(POC_INFW, PCI(pc)->pcop));
                    break;
                case POC_DECF:
                    pCodeReplace(pc, newpCode(POC_DECFW, PCI(pc)->pcop));
                    break;
                case POC_ADDWF:
                    pCodeReplace(pc, newpCode(POC_ADDFW, PCI(pc)->pcop));
                    break;
                case POC_SUBFWF:
                    pCodeReplace(pc, newpCode(POC_SUBFWW, PCI(pc)->pcop));
                    break;
                case POC_ADCWF:
                    pCodeReplace(pc, newpCode(POC_ADCFW, PCI(pc)->pcop));
                    break;
                case POC_SBCFWF:
                    pCodeReplace(pc, newpCode(POC_SBCFWW, PCI(pc)->pcop));
                    break;
                default:
                    fprintf(stderr, "internal error at %s:%d\n", __FILE__, __LINE__);
                    exit(-__LINE__);
                }
                // if (PCI(pc)->op == POC_INF)
                //     pCodeReplace(pc, newpCode(POC_INFW, PCI(pc)->pcop));
                // else
                //     pCodeReplace(pc, newpCode(POC_DECFW, PCI(pc)->pcop));
                pc = tmpp;
            }
            if (core == 0)
            {
                pc5 = newpCode2(POC_MVFF, pCodeOpCopy(PCI(pc1)->pcop), pCodeOpCopy(PCI(pc2)->pcop));
            }
            else
            {
                // decide the op
                int pc1stk = 0;
                int pc2stk = 0;
                pCodeOp *pcop1 = PCI(pc1)->pcop;
                pCodeOp *pcop2 = PCI(pc2)->pcop;

                if (!pcop1->flags.calleePara)
                {
                    if (pcop1->flags.parapcop)
                        pc1stk = 1;
                    if (pcop1->type == PO_GPR_TEMP && ((PCOR(pcop1)->r->isFuncLocal && !PCOR(pcop1)->r->isLocalStatic) || (PCOR(pcop1)->r->rIdx >= 0x1000)))
                        pc1stk = 1;
                    if (pcop1->type == PO_IMMEDIATE && (PCOI(pcop1)->r->isFuncLocal && !PCOI(pcop1)->r->isLocalStatic))
                        pc1stk = 1;
                    if (pcop1->type == PO_DIR && PCOR(pcop1)->r && isinSet(pb->tregisters, PCOR(pcop1)->r) && !PCOR(pcop1)->r->isLocalStatic)
                        pc1stk = 1;
                }
                if (!pcop2->flags.calleePara)
                {
                    if (pcop2->flags.parapcop)
                        pc2stk = 1;
                    // see if it is temp
                    if (pcop2->type == PO_GPR_TEMP && ((PCOR(pcop2)->r->isFuncLocal && !PCOR(pcop2)->r->isLocalStatic) || (PCOR(pcop2)->r->rIdx >= 0x1000)))
                        pc2stk = 1;

                    if (pcop2->type == PO_IMMEDIATE && (PCOI(pcop2)->r->isFuncLocal && !PCOI(pcop2)->r->isLocalStatic))
                        pc2stk = 1;

                    if (pcop2->type == PO_DIR && PCOR(pcop2)->r && isinSet(pb->tregisters, PCOR(pcop2)->r) && !PCOR(pcop2)->r->isLocalStatic) // DIR is special case not converted to stack!!
                        pc2stk = 1;
                }
                if (pc1stk && pc2stk)
                    pc5 = newpCode2(POC_MVSS, pCodeOpCopy(PCI(pc1)->pcop), pCodeOpCopy(PCI(pc2)->pcop));
                else if (pc1stk && !pc2stk)
                    pc5 = newpCode2(POC_MVSF, pCodeOpCopy(PCI(pc1)->pcop), pCodeOpCopy(PCI(pc2)->pcop));
                else if (!pc1stk && !pc2stk)
                    pc5 = newpCode2(POC_MVFF, pCodeOpCopy(PCI(pc1)->pcop), pCodeOpCopy(PCI(pc2)->pcop));
                else
                    continue;
            }
            pc1->prev->next = pc5;
            pc1->next->prev = pc5;
            pc5->prev = pc1->prev;
            pc5->next = pc1->next;
            if (PCI(pc1)->cline)
            {
                PCI(pc5)->cline = PCI(pc1)->cline;
                PCI(pc1)->cline = NULL;
            }
            else if (PCI(pc2)->cline)
            {
                PCI(pc5)->cline = PCI(pc2)->cline;
                PCI(pc2)->cline = NULL;
            }
            else if (PCI(pc3)->cline)
            {
                PCI(pc5)->cline = PCI(pc3)->cline;
                PCI(pc3)->cline = NULL;
            }
            unlinkpCode(pc2);
            unlinkpCode(pc3);

            count++;
            removed_count++;
            if (removed_count >= HY08A_options.ob_count)
            {
                options.nopeep = 1;
                return count;
            }
            pc = pc5;
        }
    }
    return count;
}

static int removeNoUseMVFF(pBlock *pb) // core is A or D..we skip C now
{
    // assume we use A
    int count = 0;
    pCode *pc;
    if (options.nopeep)
        return 0;
    for (pc = pb->pcHead; pc; pc = pc->next)
    {
        if (isPCI(pc) && (pcMVFFLIKE(pc)))

            if (pCodeOpCompare(PCI(pc)->pcop, PCI(pc)->pcop2))
            {
                removepCode(&pc);
                count++;

                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
            }
    }
    return count;
}

static reg_info *findSTKRegReplace(reg_info *regstk)
{
    pCode *pc;
    pBlock *pb;
    for (pb = the_pFile->pbHead; pb; pb = pb->next)
    {
        for (pc = pb->pcHead; pc; pc = pc->next)
        {
            if (isPCI(pc) && PCI(pc)->op == POC_MVFW)
                if ((PCI(pc)->pcop->type == PO_GPR_REGISTER || PCI(pc)->pcop->type == PO_GPR_TEMP) && PCOR(PCI(pc)->pcop)->r->rIdx == regstk->rIdx)
                {
                    if (isPCI(pc->next) && (PCI(pc->next)->op == POC_MVWF || PCI(pc->next)->op == POC_MVWF2) && (PCI(pc->next)->pcop->type == PO_GPR_REGISTER || PCI(pc->next)->pcop->type == PO_GPR_TEMP))
                    {

                        return PCOR(PCI(pc->next)->pcop)->r;
                    }
                }
        }
    }
    return NULL;
}

static int OptimizepCode(char dbName)
{
    static int firstOptmize = 1;
#define MAX_PASSES 100

    int matches = 0;
    int passes = 0;
    int final = 0;
    int lastmatch = 100000;
    int sameMatchCount = 0;
    // static int btcount = 0;
    pBlock *pb;
    symbol *sp, *spreq;
    int blockthiscount;
    int specialFlag;

    if (!the_pFile)
        return 0;
    resetOptimizeCount(dbName);
    DFPRINTF((stderr, " Optimizing pCode\n"));

    // replace first to STKXX
    // in new version sdcc reuse
    // it is possible 2 var will use the same reg
    if (firstOptmize && !options.nopeep)
    {
        firstOptmize = 0;
        if ((1))
        {
            // replace para with stack
            symbol *fsym;

            reg_info *rip;
            reg_info *ripDelete;
            //			value *args;
            for (pb = the_pFile->pbHead; pb; pb = pb->next)
                removeMVWF2(pb);
            for (fsym = setFirstItem(code->syms); fsym; fsym = setNextItem(code->syms))
            {
                if (IS_PTR(fsym->type) || !IFFUNC_HASBODY(fsym->type))
                    continue;
                value *paraVal;
                symbol *paramSym, *paramSym2;
                char buf[1024];
                int size, j, idx = 0;
                int skipFirstParaByte = 1;
                for (paraVal = fsym->type->funcAttrs.args; paraVal; paraVal = paraVal->next)
                {
                    paramSym = paraVal->sym;
                    if (paramSym->reqv == NULL)
                        continue;
                    paramSym2 = OP_SYMBOL(paramSym->reqv);
                    if (!paramSym2)
                        continue;
                    size = getSize(paramSym->type);
                    for (j = 0; j < size; j++)
                    {
                        if (skipFirstParaByte)
                        {
                            skipFirstParaByte = 0; // first is by W!!
                            if (paramSym2->regs[size - 1] == NULL && paramSym->assigned_from_id)
                            {
                                paramSym2->regs[size - 1] = paramSym->assigned_from[0]->regs[size - 1];
                            }

                            continue;
                        }

                        SNPRINTF(buf, 1023, "%s_STK%02d", fsym->rname, idx++);
                        rip = regFindWithName(buf);
                        if (rip == NULL)
                            continue;
                        // 2017 change to the first MVFW of the rip 's next MVWF

                        // ripDelete = paramSym2->regs[size - 1 - j];
                        // very very special case
                        ripDelete = findSTKRegReplace(rip);
                        /*
                        if (ripDelete == NULL && paramSym->assigned_from_id)
                        {
                            ripDelete = paramSym->assigned_from[0]->regs[size - 1 - j];
                        }
                        */

                        if (rip == NULL || ripDelete == NULL)
                            continue;
                        replaceFileReg(rip, ripDelete);

                        // need to replace other symbols
                        for (sp = setFirstItem(data->syms); sp; sp = setNextItem(data->syms))
                        {
                            int ii;
                            if (sp->reqv == NULL)
                                continue;
                            spreq = OP_SYMBOL(sp->reqv);
                            for (ii = 0; ii < spreq->nRegs; ii++)
                            {
                                if (spreq->regs[ii] == ripDelete)
                                    spreq->regs[ii] = rip;
                            }
                        }

                        paramSym2->regs[size - 1 - j] = rip;
                    }
                    // if(idx>=2)
                    // break;
                }
            }
        }
    }
    if (options.nopeep)
    {
        // return 0

        // fsr re-shape is a must, once is enough

        for (pb = the_pFile->pbHead; pb; pb = pb->next)
        {

            re_shape_fsr(pb);
            re_shape_adcr(pb);
            pBlockMergeLabels(pb);
        }
        return 0;
    }

    specialFlag = 0;
    final = 0;
    // 2021 Aug, change loop order
    do
    {
        matches = 0;
        final = (lastmatch == 0);

        for (pb = the_pFile->pbHead; pb; pb = pb->next)
        {
            if ('*' == dbName || getpBlock_dbName(pb) == dbName)
            {
                if (pb->lastOptCount == 0 && final == 0)
                    continue;

                blockthiscount = 0;

                blockthiscount += re_shape_fsr(pb);
                blockthiscount += re_shape_adcr(pb);
                getcsrcWriteFlg(pb); // memwrite of c-src marked
                pBlockMergeLabels(pb);
                pBlockRemoveUnusedLabels(pb); // merge will remove unuse

                if (HY08A_options.ob_count == 0)
                    return 0;
                /*if (removed_count >= 49)
                    fprintf(stderr, "49\n");*/
                blockthiscount += simpleOptimize(pb, final);
                blockthiscount += OptimizepBlock(pb);
                // pBlockMergeLabels(pb);
                // pBlockRemoveUnusedLabels(pb);

                blockthiscount += BlockTempSTARemove(pb, NULL, 0);
                // matches += BlockJMPRET2RET(pb);

                blockthiscount += BlockTempBridgeRemove(pb);
                blockthiscount += BlocknoUseLDARemove(pb);
                blockthiscount += ReplaceMVL2FollowingMVFW(pb);
                blockthiscount += ReplaceMVL02FollowingMVFW(pb);

                blockthiscount += fsr0ana(pb);

                blockthiscount += dirShiftOptimize(pb);
                if (blockthiscount == 0)
                {
                    blockthiscount += removeSAMELDPR(pb);
                    blockthiscount += xorChainOptimize(pb);
                    // if (!pb->stkxxNowTemp)
                    //{
                    // blockthiscount += stkxx2Temp(pb);
                    // if (!pb->stkxxNowTemp)
                    {
                        blockthiscount += stkxxPromote(pb); // finally
                        blockthiscount += stkxxPromote2(pb);
                    }
                    //}
                    if (HY08A_getPART()->isEnhancedCore < 3 || HY08A_getPART()->isEnhancedCore == 5)
                        blockthiscount += mvxxOptimize(pb, 0);
                    else if (HY08A_getPART()->isEnhancedCore == 4)
                        blockthiscount += mvxxOptimize(pb, 1);
                    blockthiscount += jnnOptimize(pb);
                    specialFlag = 1;
                }
                else
                    specialFlag = 0;
                blockthiscount += removeNoUseMVFF(pb);

                // matches += fsr0ana2(pb);
                pb->lastOptCount = blockthiscount;
                matches += blockthiscount;
                if (options.nopeep)
                    return matches;
            }
        }
        // if (matches == 1) // twice equal to 1 is allowed
        // fprintf(stderr, "match1\n");
        if (!specialFlag && matches >= lastmatch && matches < 2)
        {
            if (++sameMatchCount > 10)
            {
                // fprintf(stderr, "Warning, optimize halt.. contact MSHINE with the source code.\n");
                break;
            }
            // break;
        }
        lastmatch = matches;
    } while (((!final) || matches) && ++passes < MAX_PASSES);

    return matches;
}

/*-----------------------------------------------------------------*/
/* popCopyGPR2Bit - copy a pcode operator                          */
/*-----------------------------------------------------------------*/

pCodeOp *popCopyGPR2Bit(pCodeOp *pc, int bitval)
{
    pCodeOp *pcop;

    pcop = newpCodeOpBitByOp(pc, bitval, 0);

    if (!((pcop->type == PO_LABEL) ||
          (pcop->type == PO_LITERAL) ||
          (pcop->type == PO_STR)))
        PCOR(pcop)->r = PCOR(pc)->r; /* This is dangerous... */

    return pcop;
}

/*-----------------------------------------------------------------*/
/*-----------------------------------------------------------------*/
static void pBlockDestruct(pBlock *pb)
{

    if (!pb)
        return;

    free(pb);
}

/*-----------------------------------------------------------------*/
/* void mergepBlocks(char dbName) - Search for all pBlocks with the*/
/*                                  name dbName and combine them   */
/*                                  into one block                 */
/*-----------------------------------------------------------------*/
static void mergepBlocks(char dbName)
{

    pBlock *pb, *pbmerged = NULL, *pbn;

    pb = the_pFile->pbHead;

    // fprintf(stderr," merging blocks named %c\n",dbName);
    while (pb)
    {

        pbn = pb->next;
        // fprintf(stderr,"looking at %c\n",getpBlock_dbName(pb));
        if (getpBlock_dbName(pb) == dbName)
        {

            // fprintf(stderr," merged block %c\n",dbName);

            if (!pbmerged)
            {
                pbmerged = pb;
            }
            else
            {
                addpCode2pBlock(pbmerged, pb->pcHead);
                /* addpCode2pBlock doesn't handle the tail: */
                pbmerged->pcTail = pb->pcTail;

                pb->prev->next = pbn;
                if (pbn)
                    pbn->prev = pb->prev;

                pBlockDestruct(pb);
            }
            // printpBlock(stderr, pbmerged);
        }
        pb = pbn;
    }
}

/*-----------------------------------------------------------------*/
/* AnalyzeFlow - Examine the flow of the code and optimize         */
/*                                                                 */
/* level 0 == minimal optimization                                 */
/*   optimize registers that are used only by two instructions     */
/* level 1 == maximal optimization                                 */
/*   optimize by looking at pairs of instructions that use the     */
/*   register.                                                     */
/*-----------------------------------------------------------------*/

// static void AnalyzeFlow(int level)
//{
//     static int times_called=0;
//
//     pBlock *pb;
//
//     if(!the_pFile)
//         return;
//
//
//     /* if this is not the first time this function has been called,
//        then clean up old flow information */
//     if(times_called++) {
//         for(pb = the_pFile->pbHead; pb; pb = pb->next)
//             unBuildFlow(pb);
//
//         RegsUnMapLiveRanges();
//
//     }
//
//     GpcFlowSeq = 1;
//
//     /* Phase 2 - Flow Analysis - Register Banking
//      *
//      * In this phase, the individual flow blocks are examined
//      * and register banking is fixed.
//      */
//
//     //for(pb = the_pFile->pbHead; pb; pb = pb->next)
//     //FixRegisterBanking(pb);
//
//     /* Phase 2 - Flow Analysis
//      *
//      * In this phase, the pCode is partition into pCodeFlow
//      * blocks. The flow blocks mark the points where a continuous
//      * stream of instructions changes flow (e.g. because of
//      * a call or goto or whatever).
//      */
//
//     for (pb = the_pFile->pbHead; pb; pb = pb->next)
//     {
//         pBlockMergeLabels(pb);
//         //pBlockRemoveUnusedLabels(pb); merge will remove unuse
//         BuildFlow(pb);
//     }
//
//
//     /* Phase 2 - Flow Analysis - linking flow blocks
//      *
//      * In this phase, the individual flow blocks are examined
//      * to determine their order of excution.
//      */
//
//     for(pb = the_pFile->pbHead; pb; pb = pb->next)
//         LinkFlow(pb);
//
//     /* Phase 3 - Flow Analysis - Flow Tree
//      *
//      * In this phase, the individual flow blocks are examined
//      * to determine their order of excution.
//      */
//
//     for(pb = the_pFile->pbHead; pb; pb = pb->next)
//         BuildFlowTree(pb);
//
//
//     /* Phase x - Flow Analysis - Used Banks
//      *
//      * In this phase, the individual flow blocks are examined
//      * to determine the Register Banks they use
//      */
//
////  for(pb = the_pFile->pbHead; pb; pb = pb->next)
////      FixBankFlow(pb);
//
//
//    for(pb = the_pFile->pbHead; pb; pb = pb->next)
//        pCodeRegMapLiveRanges(pb);
//
//    RemoveUnusedRegisters();
//
////  for(pb = the_pFile->pbHead; pb; pb = pb->next)
//    pCodeRegOptimizeRegUsage(level);
//
//    OptimizepCode('*');
//
//    /*
//        for(pb = the_pFile->pbHead; pb; pb = pb->next)
//        DumpFlow(pb);
//     */
//    /* debug stuff */
//
//    /*
//    for(pb = the_pFile->pbHead; pb; pb = pb->next) {
//        pCode *pcflow;
//        for( pcflow = findNextpCode(pb->pcHead, PC_FLOW);
//            (pcflow = findNextpCode(pcflow, PC_FLOW)) != NULL;
//            pcflow = pcflow->next) {
//
//                FillFlow(PCFL(pcflow));
//            }
//        }
//     */
//    /*
//    for(pb = the_pFile->pbHead; pb; pb = pb->next) {
//        pCode *pcflow;
//        for( pcflow = findNextpCode(pb->pcHead, PC_FLOW);
//            (pcflow = findNextpCode(pcflow, PC_FLOW)) != NULL;
//            pcflow = pcflow->next) {
//
//                FlowStats(PCFL(pcflow));
//            }
//        }
//     */
//}

/*-----------------------------------------------------------------*/
/* AnalyzeBanking - Called after the memory addresses have been    */
/*                  assigned to the registers.                     */
/*                                                                 */
/*-----------------------------------------------------------------*/

// void AnalyzeBanking(void)
//{
//     //pBlock  *pb;
//
//     if(!hyaIsInitialized()) {
//         werror(E_FILE_OPEN_ERR, "no memory size is known for this processor");
//         exit(1);
//     }
//
//     if (!the_pFile) return;
//
//     /* Phase x - Flow Analysis - Used Banks
//     *
//     * In this phase, the individual flow blocks are examined
//     * to determine the Register Banks they use
//     */
//
//     AnalyzeFlow(0);
//     AnalyzeFlow(1);
//
//     /*
//     for(pb = the_pFile->pbHead; pb; pb = pb->next)
//     	FixRegisterBanking(pb);
//
//     AnalyzeFlow(0);
//     AnalyzeFlow(1);
//     */
// }

/*-----------------------------------------------------------------*/
/*-----------------------------------------------------------------*/
// static DEFSETFUNC (resetrIdx)
//{
//     reg_info *r = (reg_info *)item;
//     if (!r->isFixed) {
//         r->rIdx = 0;
//     }
//
//     return 0;
// }

/*-----------------------------------------------------------------*/
/* InitRegReuse - Initialises variables for code analyzer          */
/*-----------------------------------------------------------------*/
// static void InitReuseReg(void)
//{
//     /* Find end of statically allocated variables for start idx */
//     /* Start from begining of GPR. Note may not be 0x20 on some chips */
//     /* XXX: Avoid clashes with fixed registers, start late. */
//     unsigned maxIdx = 0x1000;
//     reg_info *r;
//     for (r = setFirstItem(dynDirectRegs); r; r = setNextItem(dynDirectRegs)) {
//         if (r->type != REG_SFR) {
//             maxIdx += r->size; /* Increment for all statically allocated variables */
//         }
//     }
//     peakIdx = maxIdx;
//     applyToSet(dynAllocRegs,resetrIdx); /* Reset all rIdx to zero. */
// }

static void buildCallTree(void)
{
    pBranch *pbr;
    pBlock *pb;
    pCode *pc;

    if (!the_pFile)
        return;

    /* Now build the call tree.
       First we examine all of the pCodes for functions.
       Keep in mind that the function boundaries coincide
       with pBlock boundaries.

       The algorithm goes something like this:
       We have two nested loops. The outer loop iterates
       through all of the pBlocks/functions. The inner
       loop iterates through all of the pCodes for
       a given pBlock. When we begin iterating through
       a pBlock, the variable pc_fstart, pCode of the start
       of a function, is cleared. We then search for pCodes
       of type PC_FUNCTION. When one is encountered, we
       initialize pc_fstart to this and at the same time
       associate a new pBranch object that signifies a
       branch entry. If a return is found, then this signifies
       a function exit point. We'll link the pCodes of these
       returns to the matching pc_fstart.

       When we're done, a doubly linked list of pBranches
       will exist. The head of this list is stored in
       `the_pFile', which is the meta structure for all
       of the pCode. Look at the printCallTree function
       on how the pBranches are linked together.
     */

    for (pb = the_pFile->pbHead; pb; pb = pb->next)
    {
        pCode *pc_fstart = NULL;
        for (pc = pb->pcHead; pc; pc = pc->next)
        {
            if (isPCF(pc))
            {
                pCodeFunction *pcf = PCF(pc);
                if (pcf->fname)
                {

                    if (STRCASECMP(pcf->fname, "_main") == 0)
                    {
                        // fprintf(stderr," found main \n");
                        pb->cmemmap = NULL; /* FIXME do we need to free ? */
                        pb->dbName = 'M';
                    }

                    pbr = Safe_calloc(1, sizeof(pBranch));
                    pbr->pc = pc_fstart = pc;
                    pbr->next = NULL;

                    the_pFile->functions = pBranchAppend(the_pFile->functions, pbr);

                    // Here's a better way of doing the same:
                    addSet(&pb->function_entries, pc);
                }
                else
                {
                    // Found an exit point in a function, e.g. return
                    // (Note, there may be more than one return per function)
                    if (pc_fstart)
                        pBranchLink(PCF(pc_fstart), pcf);

                    addSet(&pb->function_exits, pc);
                }
            }
            else if (isCALL(pc))
            {

                if (!isinSet(pb->function_calls, pc))
                    addSet(&pb->function_calls, pc);
            }
        }
    }
}

/*-----------------------------------------------------------------*/
/* AnalyzepCode - parse the pCode that has been generated and form */
/*                all of the logical connections.                  */
/*                                                                 */
/* Essentially what's done here is that the pCode flow is          */
/* determined.                                                     */
/*-----------------------------------------------------------------*/

void AnalyzepCode(char dbName)
{
    pBlock *pb;
    int i, changes;

    if (!the_pFile)
        return;

    mergepBlocks('D');

    /* Phase 1 - Register allocation and peep hole optimization
     *
     * The first part of the analysis is to determine the registers
     * that are used in the pCode. Once that is done, the peep rules
     * are applied to the code. We continue to loop until no more
     * peep rule optimizations are found (or until we exceed the
     * MAX_PASSES threshold).
     *
     * When done, the required registers will be determined.
     *
     */
    i = 0;
    do
    {

        DFPRINTF((stderr, " Analyzing pCode: PASS #%d\n", i + 1));

        /* First, merge the labels with the instructions */
        for (pb = the_pFile->pbHead; pb; pb = pb->next)
        {
            if ('*' == dbName || getpBlock_dbName(pb) == dbName)
            {

                DFPRINTF((stderr, " analyze and merging block %c\n", dbName));
                pBlockMergeLabels(pb);
                AnalyzepBlock(pb);
            }
            else
            {
                DFPRINTF((stderr, " skipping block analysis dbName=%c blockname=%c\n", dbName, getpBlock_dbName(pb)));
            }
        }

        changes = OptimizepCode(dbName);

    } while (changes && (i++ < 2));

    for (pb = the_pFile->pbHead; pb; pb = pb->next)
        arrangeCsrc(pb); // only once

    buildCallTree();
}

/*-----------------------------------------------------------------*/
/* findFunction - Search for a function by name (given the name)   */
/*                in the set of all functions that are in a pBlock */
/* (note - I expect this to change because I'm planning to limit   */
/*  pBlock's to just one function declaration                      */
/*-----------------------------------------------------------------*/
static pCode *findFunction(char *fname)
{
    pBlock *pb;
    pCode *pc;
    if (!fname)
        return NULL;

    for (pb = the_pFile->pbHead; pb; pb = pb->next)
    {

        pc = setFirstItem(pb->function_entries);
        while (pc)
        {

            if ((pc->type == PC_FUNCTION) &&
                (PCF(pc)->fname) &&
                (strcmp(fname, PCF(pc)->fname) == 0))
                return pc;

            pc = setNextItem(pb->function_entries);
        }
    }
    return NULL;
}

static int getTotalParamSize(symbol *symf)
{
    sym_link *sl = symf->type;
    int totalSize = 0;
    struct value *args = sl->funcAttrs.args;
    while (args)
    {
        totalSize += getSize(args->type);
        args = args->next;
    }

    return totalSize;
}

static void pBlockStats(FILE *of, pBlock *pb)
{

    pCode *pc;
    reg_info *r;
    pCode *funcHead;
    int exitNum = 0;
    pCode *lastcalls[1024]; // no show double
    int totalCall = 0;
    int localVarAddrReferenced;
    char funcHeadBuf[20480];
    char *funcHeadp = funcHeadBuf;
    int bufsize = sizeof(funcHeadBuf) - 1;
    //	int totalParamSize;
    int i;
    int len;

    // int hasLocal;

    // fprintf(of,";***\n;  pBlock Stats: dbName = %c\n;***\n",getpBlock_dbName(pb));

    // for now just print the first element of each set
    pc = setFirstItem(pb->function_entries);
    if (pc)
    {
        //		fprintf(of,";entry:  ");
        //		pc->print(of,pc);
        funcHead = pc;
        if (PCF(pc)->isPublic)
        {
            fprintf(of, ".globl %s\n", PCF(pc)->fname);
        }
    }
    else
        return;
    pc = setFirstItem(pb->function_exits);
    if (pc)
    {
        //		fprintf(of,";has an exit\n");
        // pc->print(of,pc);
        exitNum++;
    }

    pc = setFirstItem(pb->function_calls);
    if (pc)
    {
        // fprintf(of,";functions called:\n");
        // fprintf(of,"\t.FUNC %s:C:",pb->function_entries

        while (pc)
        {
            if (pc->type == PC_OPCODE && (PCI(pc)->op == POC_CALL || PCI(pc)->op == POC_JMP) && PCI(pc)->pcop->type == PO_STR)
            {
                int j;
                int found = 0;
                for (j = 0; j < totalCall; j++)
                    if (!strncmp(get_op_from_instruction(PCI(lastcalls[j])),
                                 get_op_from_instruction(PCI(pc)), 128))
                    {
                        found = 1;
                        break;
                    }
                if (!found)
                {
                    // fprintf(of, ";   %s\n", get_op_from_instruction(PCI(pc)));
                    lastcalls[totalCall++] = pc;
                }
            }
            pc = setNextItem(pb->function_calls);
        }
    }
    if (PCF(funcHead)->isParaOnStack)
        SNPRINTF(funcHeadp, bufsize, ".FUNC_PSTK %s", PCF(funcHead)->fname); // means para on stack!!
    else
    {
        int funcRetSize = getSizeRet(PCF(funcHead)->symp->type->next); // first must be a function declarator!!
        // it is possible void
        // 2021 change with return value size
        SNPRINTF(funcHeadp, bufsize, ".FUNC %s:$r:%d:%d", PCF(funcHead)->fname,
                 // getSize(PCF(funcHead)->symp->type), // type, not etype
                 funcRetSize,
                 getTotalParamSize(PCF(funcHead)->symp));
        // SNPRINTF(funcHeadp, bufsize, ".FUNC %s:%d", PCF(funcHead)->fname, getTotalParamSize(PCF(funcHead)->symp));
    }
    bufsize -= strlen(funcHeadBuf);
    funcHeadp += strlen(funcHeadBuf);

    for (i = 0; i < totalCall; i++)
    {
        pCodeOp *pcop = PCI(lastcalls[i])->pcop;
        SNPRINTF(funcHeadp, bufsize, ":$C:%s", PCOS(pcop)->pcop.name);
        len = strlen(funcHeadp);
        funcHeadp += len;
        bufsize -= len;
        if (i % 5 == 3)
        {
            SNPRINTF(funcHeadp, bufsize, "\\\n");
            len = strlen(funcHeadp);
            funcHeadp += len;
            bufsize -= len;
            if ((0))
            {
                SNPRINTF(funcHeadp, bufsize, ";");
                len = strlen(funcHeadp);
                funcHeadp += len;
                bufsize -= len;
            }
        }
    }
    if (i % 5 != 4 && i != 0)
    {
        SNPRINTF(funcHeadp, bufsize, "\\\n");
        len = strlen(funcHeadp);
        funcHeadp += len;
        bufsize -= len;
        if ((0))
        {
            SNPRINTF(funcHeadp, bufsize, ";");
            len = strlen(funcHeadp);
            funcHeadp += len;
            bufsize -= len;
        }
    }
    i = 0;

    r = setFirstItem(pb->tregisters);
    if (r)
    {
        // int n = elementsInSet(pb->tregisters);

        // fprintf(of,";%d compiler assigned register%c:\n",n, ( (n!=1) ? 's' : ' '));
        localVarAddrReferenced = 0;

        while (r)
        {
            // if (strstr(r->name, "byte"))
            //{
            // fprintf(stderr, "have byte\n");
            // }
            if (r->wasUsed && !(r->name[0] == 'S' && r->name[1] == 'T' && r->name[2] == 'K' && isdigit(r->name[3]) && isdigit(r->name[4]) && r->name[5] == '\0') &&
                ((r->isFuncLocal && !r->isLocalStatic) || r->pc_type == PO_GPR_REGISTER || r->pc_type == PO_GPR_TEMP ||
                 (r->pc_type == PO_DIR && r->isVolatile)) //|| !r->isPublic)
                )                                         // we cannot use STKXX directly
            {
                // fprintf(of, ";   %s\n", r->name);
                SNPRINTF(funcHeadp, bufsize, ":$L:%s", r->name);
                if (r->addrReferenced)
                    localVarAddrReferenced = 1;
                len = strlen(funcHeadp);
                funcHeadp += len;
                bufsize -= len;
                if (i % 5 == 4)
                {
                    SNPRINTF(funcHeadp, bufsize, "\\\n");
                    len = strlen(funcHeadp);
                    funcHeadp += len;
                    bufsize -= len;
                    if ((0))
                    {
                        SNPRINTF(funcHeadp, bufsize, ";");
                        len = strlen(funcHeadp);
                        funcHeadp += len;
                        bufsize -= len;
                    }
                }
                i++;
            }
            r = setNextItem(pb->tregisters);
        }
        if (localVarAddrReferenced && HY08A_getPART()->isEnhancedCore == 4)
        {
            SNPRINTF(funcHeadp, bufsize, ":$L:%s__addrtmp__", PCF(funcHead)->fname);
        }
    }
    r = setFirstItem(pb->txregisters);
    if (r)
    {
        // int n = elementsInSet(pb->txregisters);

        // fprintf(of,";%d compiler assigned register%c:\n",n, ( (n!=1) ? 's' : ' '));

        while (r)
        {
            if (r->wasUsed && strncmp(r->name, "STK0", 4) && ((r->isFuncLocal && !r->isLocalStatic) || r->type == REG_TMP || r->type == REG_GPR)) // we cannot use STKXX directly
            {
                // fprintf(of, ";   %s\n", r->name);
                SNPRINTF(funcHeadp, bufsize, ":$XL:%s", r->name);
                len = strlen(funcHeadp);
                funcHeadp += len;
                bufsize -= len;
                if (i % 5 == 4)
                {
                    SNPRINTF(funcHeadp, bufsize, "\\\n");
                    len = strlen(funcHeadp);
                    funcHeadp += len;
                    bufsize -= len;
                    if ((0))
                    {
                        SNPRINTF(funcHeadp, bufsize, ";");
                        len = strlen(funcHeadp);
                        funcHeadp += len;
                        bufsize -= len;
                    }
                }
                i++;
            }
            r = setNextItem(pb->txregisters);
        }
    }
    // add in 2017 Oct
    // SNPRINTF(funcHeadp, bufsize, ":$S:%d", getTotalParamSize(PCF(funcHead)->symp));

    fprintf(of, "\n;--------------------------------------------------------\n");
    if (!(0))
        fprintf(of, "\t%s\n", funcHeadBuf); // linker will remap the variable used
    else
        fprintf(of, "\t;%s\n", funcHeadBuf);
}

/*-----------------------------------------------------------------*/
/*                                                                 */
/*-----------------------------------------------------------------*/

static void InlineFunction(pBlock *pb)
{
    pCode *pc;
    pCode *pc_call;

    if (!pb)
        return;

    pc = setFirstItem(pb->function_calls);

    for (; pc; pc = setNextItem(pb->function_calls))
    {

        if (isCALL(pc))
        {
            pCode *pcn = findFunction(get_op_from_instruction(PCI(pc)));
            pCode *pcp = pc->prev;
            pCode *pct;
            pCode *pce;

            pBranch *pbr;

            if (pcn && isPCF(pcn) && (PCF(pcn)->ncalled == 1) && !PCF(pcn)->isPublic && (pcp && (isPCI_BITSKIP(pcp) || !isPCI_SKIP(pcp))))
            { /* Bit skips can be inverted other skips can not */

                InlineFunction(pcn->pb);

                /*
                At this point, *pc points to a CALL mnemonic, and
                *pcn points to the function that is being called.

                  To in-line this call, we need to remove the CALL
                  and RETURN(s), and link the function pCode in with
                  the CALLee pCode.

                */

                pc_call = pc;

                /* Check if previous instruction was a bit skip */
                if (isPCI_BITSKIP(pcp))
                {
                    pCodeLabel *pcl;
                    /* Invert skip instruction and add a goto */
                    PCI(pcp)->op = (PCI(pcp)->op == POC_BTSS) ? POC_BTSZ : POC_BTSS;

                    if (isPCL(pc_call->next))
                    { // Label pcode
                        pcl = PCL(pc_call->next);
                    }
                    else if (isPCI(pc_call->next) && PCI(pc_call->next)->label)
                    { // pcode instruction with a label
                        pcl = PCL(PCI(pc_call->next)->label->pc);
                    }
                    else
                    {
                        pcl = PCL(newpCodeLabel(NULL, newiTempLabel(NULL)->key + 100));
                        if (PCI(pc_call->next)->label == NULL)
                            PCI(pc_call->next)->label = (pBranch *)Safe_calloc(1, sizeof(pBranch));
                        PCI(pc_call->next)->label->pc = (struct pCode *)pcl;
                    }
                    pCodeInsertAfter(pcp, newpCode(POC_JMP, newpCodeOp(pcl->label, PO_STR)));
                }

                /* remove callee pBlock from the pBlock linked list */
                removepBlock(pcn->pb);

                assert(pcn);
                pce = pcn;
                while (pce)
                {
                    pce->pb = pb;
                    pce = pce->next;
                }

                /* Remove the Function pCode */
                pct = findNextInstruction(pcn->next);

                /* Link the function with the callee */

                if (pcp)
                    pcp->next = pcn->next;
                assert(pcn->next);
                pcn->next->prev = pcp;

                /* Convert the function name into a label */

                pbr = Safe_calloc(1, sizeof(pBranch));
                pbr->pc = newpCodeLabel(PCF(pcn)->fname, -1);
                pbr->next = NULL;
                PCI(pct)->label = pBranchAppend(PCI(pct)->label, pbr);
                if (PCI(pc_call)->label)
                    PCI(pct)->label = pBranchAppend(PCI(pct)->label, PCI(pc_call)->label);

                /* turn all of the return's except the last into goto's */
                /* check case for 2 instruction pBlocks */
                pce = findNextInstruction(pcn->next);
                while (pce)
                {
                    pCode *pce_next = findNextInstruction(pce->next);

                    if (pce_next == NULL)
                    {
                        /* found the last return */
                        pCode *pc_call_next = findNextInstruction(pc_call->next); // label counts

                        // fprintf(stderr,"found last return\n");
                        // pce->print(stderr,pce);
                        pce->prev->next = pc_call->next;
                        pc_call->next->prev = pce->prev;
                        if (PCI(pce)->label)
                            PCI(pc_call_next)->label = pBranchAppend(PCI(pc_call_next)->label,
                                                                     PCI(pce)->label);
                    }

                    pce = pce_next;
                }
            }
        }
        else
            fprintf(stderr, "BUG? pCode isn't a POC_CALL %d\n", __LINE__);
    }
}

/*-----------------------------------------------------------------*/
/*                                                                 */
/*-----------------------------------------------------------------*/

void InlinepCode(void)
{

    pBlock *pb;
    pCode *pc;

    if (!the_pFile)
        return;

    if (!functionInlining)
        return;

    /* Loop through all of the function definitions and count the
     * number of times each one is called */
    // fprintf(stderr,"inlining %d\n",__LINE__);

    for (pb = the_pFile->pbHead; pb; pb = pb->next)
    {

        pc = setFirstItem(pb->function_calls);

        for (; pc; pc = setNextItem(pb->function_calls))
        {

            if (isCALL(pc))
            {
                pCode *pcn = findFunction(get_op_from_instruction(PCI(pc)));
                if (pcn && isPCF(pcn))
                {
                    PCF(pcn)->ncalled++;
                }
            }
            else
                fprintf(stderr, "BUG? pCode isn't a POC_CALL %d\n", __LINE__);
        }
    }

    // fprintf(stderr,"inlining %d\n",__LINE__);

    /* Now, Loop through the function definitions again, but this
     * time inline those functions that have only been called once. */

    InlineFunction(the_pFile->pbHead);
    // fprintf(stderr,"inlining %d\n",__LINE__);

    // for(pb = the_pFile->pbHead; pb; pb = pb->next)
    // unBuildFlow(pb);
}

static int removepCode(pCode **pc_sta)
{
    pCode *pc_prev, *pcn = NULL;
    if ((*pc_sta)->prev == NULL)
        fprintf(stderr, "strange pc\n");
    if ((*pc_sta)->next)
        pcn = findNextInstruction((*pc_sta)->next); // labe counts
    if (!pcn && (*pc_sta)->prev)
        pcn = findPrevInstruction((*pc_sta)->prev);

    if (PCI(*pc_sta)->label)
    {
        // we insert those labels
        pBranch *labelsp = PCI(*pc_sta)->label;

        PCI(pcn)->label = pBranchAppend(PCI(pcn)->label, labelsp);

        // modify the entry in table , too
        while (labelsp)
        {
            int n = getLabelKeyIndexInPb((*pc_sta)->pb, ((pCodeLabel *)(labelsp->pc))->key);
            if (!pcn->pb)
                pcn->pb = pcn->prev->pb; // strange
            pcn->pb->labelPtrTab[n] = pcn;
            labelsp = labelsp->next;
        }

        // pBlockRemoveUnusedLabels((*pc_sta)->pb);
        // while (labelsp)
        //{
        //	(*pc_sta)->prev->next = labelsp->pc; // change them back and merge later
        //	labelsp->pc->prev = (*pc_sta)->prev;
        //	labelsp->pc->next = (*pc_sta);
        //	(*pc_sta)->prev = labelsp->pc;
        //	labelsp = labelsp->next;
        // }
    }
    if (PCI((*pc_sta))->cline)
    {

        if (!PCI(pcn)->cline)
            PCI(pcn)->cline = PCI((*pc_sta))->cline; // try keep it
    }

    pc_prev = (*pc_sta)->prev;

    pc_prev->next = (*pc_sta)->next;

    // free((*pc_sta)); // keep it is a good idea?
    (*pc_sta)->prev = NULL;
    (*pc_sta)->next = NULL; // un-link it!!

    // if it ha

    if (pc_prev == NULL)
    {
        fprintf(stderr, "remove a null prev?\n");
    }
    (*pc_sta) = pc_prev; // back to prev
    if (pc_prev && pc_prev->next)
        pc_prev->next->prev = pc_prev; // fix the pointer
    return 1;
}

// recursive check if source modified before next STA + temp
//
#define MAX_REF_SEARCH_DEPTH 16384

// pCode *searched_pcode[MAX_REF_SEARCH_DEPTH];

// there are some kinds of situations
// LDA a
// sta b
// ...
// lda b
// ...
// lda b
// ...
// ret        ==> ok to replace

// LDA a
// sta b
// ...
// sta a
// ...
// lda b
// ...
// ret        ==> not ok to replace

// there are some kinds of situations
// LDA a
// sta b
// ...
// lda b
// ...
// lda b
// ...
// ... sta a
// ...
// ret        ==> ok to replace, too

// but ... there are something important:
// lda a
// sta b
// ..
// jn xx
// lda c
// sta b
// xx:
// add b
// .................it is wrong to replace
// that is, for every reference of the bridge temp
// we will construct a list of pointer
// from reference to the sources

// the search need to check branches
// instructions: return 0 means no instruction, 1 means 1 instruction,
// ...
// waitret means waiting ret by call used for case switch statements

static void clean_branch(pBranch **pbr)
{
    if (!*pbr)
        return;
    if ((*pbr)->next)
        clean_branch(&((*pbr)->next));
    Safe_free(*pbr);
    *pbr = NULL;
}

static void cleanBeforeBridgeOptimize(pBlock *pb)
{
    pCode *pc;
    for (pc = pb->pcHead; pc; pc = pc->next)
    {
        if (!isPCI(pc))
            continue;
        if (PCI(pc)->from)
            clean_branch(&(PCI(pc)->from));
        if (PCI(pc)->to)
            clean_branch(&(PCI(pc)->to));
        PCI(pc)->locked = 0;
        PCI(pc)->srcislit = 0;
        // special case, if STK, it already have source from outside
        // if (PCI(pc)->pcop && PCI(pc)->pcop->type == PO_GPR_TEMP && PCI(pc)->pcop->name == NULL) // temp always 1 byte!!
        // PCI(pc)->pcop->name = PCOR(PCI(pc)->pcop)->r->name;

        // if (PCI(pc)->pcop && PCI(pc)->pcop->type == PO_GPR_TEMP && PCI(pc)->pcop->name && strstr(PCI(pc)->pcop->name, "_STK"))
        //{
        //	PCI(pc)->source_cnt = 1;
        // }else
        PCI(pc)->source_cnt = 0;
    }
}

// enter stage 2 means source modified, it should be fail if destination referenced
static pCode *history1[MAX_REF_SEARCH_DEPTH];
static int searched1 = 0;

// static bool checkIfModifiedAfter(pCode *pc, int tempid, int depth) // only for temp
// {
//     int nextpathn;
//     pCode **pcarray = NULL;
//     pCode *pc1, *pc2;
//     pCode *pcallret = NULL;
//     //int waitret = 0;
//     if (!depth)
//     {
//         int i;
//         for (i = 0; i < searched1; i++)
//             history1[i]->auxFlag &= 0xffffff7f;
//         searched1 = 0;
//     }
//     while (pc)
//     {
//         //if (pcodeInSearched(history, pc, searched))
//         if (pc->auxFlag & 0x80)
//             return FALSE;
//         history1[searched1++] = pc;
//         pc->auxFlag |= 0x80;
//         depth++;
//         if (searched1 >= MAX_REF_SEARCH_DEPTH)
//             return TRUE;             // assume possible modified
//         if (PCI(pc)->outCond & PCC_REGISTER)
//         {
//             if(PCI(pc)->pcop2)
//             {
//                 if(PCI(pc)->pcop2->type==PO_GPR_TEMP && PCOR(PCI(pc)->pcop2)->r->rIdx == tempid )
//                     return TRUE;
//             }
//             else
//             {
//                 if(PCI(pc)->pcop )
//                 {
//                     if(PCI(pc)->pcop->type==PO_GPR_TEMP && PCOR(PCI(pc)->pcop)->r->rIdx==tempid)
//                         return TRUE;
//                     if(PCI(pc)->pcop->type==PO_GPR_BIT && PCORB(PCI(pc)->pcop)->pcor.r->rIdx == tempid)
//                         return TRUE;
//                 }
//             }

//         }

//         nextpathn = findNextInstructions(pc, &pc1, &pc2, &pcarray, &pcallret);
//         if (nextpathn == 0)
//             return FALSE;

//         if (nextpathn == 1)
//         {
//             pc = pc1;
//             continue;
//         }
//         // here we have multi solutions
//         if (nextpathn > 2)
//         {
//             int i;
//             bool result = FALSE;
//             for (i = 0; i < nextpathn; i++)
//             {
//                 result |= checkIfModifiedAfter(pcarray[i], tempid, depth);
//                 if (result)
//                     break;
//             }
//             Safe_free(pcarray);
//             return result;
//         }
//         // followint must be =2
//         if (checkIfModifiedAfter(pc1, tempid,  depth))
//             return TRUE;
//         return checkIfModifiedAfter(pc2, tempid, depth);
//     }
//     return FALSE;

// }

// static int replace2NewTempAfter(pCode *pc, int oldtempid, pCodeOp *newpcop, int depth) // only for temp
// {
//     int nextpathn;
//     pCode **pcarray = NULL;
//     pCode *pc1, *pc2;
//     pCode *pcallret = NULL;
//     int count=0;
//     //int waitret = 0;
//     if (!depth)
//     {
//         int i;
//         for (i = 0; i < searched1; i++)
//             history1[i]->auxFlag &= 0xfffffeff;
//         searched1 = 0;
//     }
//     while (pc)
//     {
//         //if (pcodeInSearched(history, pc, searched))
//         if (pc->auxFlag & 0x100)
//             return count;
//         history1[searched1++] = pc;
//         pc->auxFlag |= 0x100;
//         depth++;
//         if (searched1 >= MAX_REF_SEARCH_DEPTH)
//             return count;             // assume possible modified

//         if(PCI(pc)->pcop )
//         {
//             if(PCI(pc)->pcop->type==PO_GPR_TEMP && PCOR(PCI(pc)->pcop)->r->rIdx == oldtempid)
//             {
//                 //PCI(pc)->pcop = pCodeOpCopy(newpcop); // diret change?
//                 pCode *pc2=pc->prev;
//                 pCodeReplace(pc, newpCode(PCI(pc)->op, newpcop));
//                 pc=pc2;
//                 ++count;

//             }
//             else if(PCI(pc)->pcop->type==PO_GPR_BIT && PCORB(PCI(pc)->pcop)->pcor.r->rIdx == oldtempid    )
//             {
//                 PCORB(PCI(pc)->pcop)->pcor.r = PCOR(newpcop)->r;
//                 PCORB(PCI(pc)->pcop)->pcor.rIdx = PCOR(newpcop)->r->rIdx;
//                 ++count;

//             }

//         }

//         nextpathn = findNextInstructions(pc, &pc1, &pc2, &pcarray, &pcallret);
//         if (nextpathn == 0)
//             return count;

//         if (nextpathn == 1)
//         {
//             pc = pc1;
//             continue;
//         }
//         // here we have multi solutions
//         if (nextpathn > 2)
//         {
//             int i;
//             for (i = 0; i < nextpathn; i++)
//             {
//                 count += replace2NewTempAfter(pcarray[i], oldtempid, newpcop, depth);
//             }
//             Safe_free(pcarray);
//             return count;
//         }
//         // followint must be =2
//         count  += replace2NewTempAfter(pc1, oldtempid, newpcop, depth);
//         count  += replace2NewTempAfter(pc2, oldtempid, newpcop, depth);
//     }
//     return count;

// }

static bool chkReferencedBeforeChange(pCode *pc, int tempid, reg_info *r, int index, int depth)
{

    // second stage, really not possible if referernced before change
    // only change after referenced will return true

    int nextpathn;
    pCode **pcarray = NULL;
    pCode *pc1, *pc2;
    pCode *pcallret = NULL;
    // int waitret = 0;
    if (!depth)
    {
        int i;
        for (i = 0; i < searched1; i++)
            history1[i]->auxFlag &= 0xfffffffd;
        searched1 = 0;
    }
    while (pc)
    {
        // if (pcodeInSearched(history, pc, searched))
        if (pc->auxFlag & 2)
            return FALSE;
        history1[searched1++] = pc;
        pc->auxFlag |= 2;
        depth++;
        if (searched1 >= MAX_REF_SEARCH_DEPTH)
        {
            searched1 = MAX_REF_SEARCH_DEPTH;
            return TRUE; // assume possible modified
        }
        if (PCI(pc)->op == POC_MVWF || PCI(pc)->op == POC_CLRF || PCI(pc)->op == POC_SETF) // if just write
        {
            // 2 kinds if source or dest
            if (compare_temp_pcop(PCI(pc)->pcop, tempid, r, index)) // this is dest
                return FALSE;
        }
        else if (pcMVFFLIKE(pc))
        {
            if (compare_temp_pcop(PCI(pc)->pcop2, tempid, r, index)) // this is dest
                return FALSE;
            if (compare_temp_pcop(PCI(pc)->pcop, tempid, r, index)) // this is src
                return TRUE;
        }
        else if ((PCI(pc)->inCond & PCC_REGISTER) && PCI(pc)->pcop && compare_temp_pcop(PCI(pc)->pcop, tempid, r, index))
        {
            return TRUE;
        }

        nextpathn = findNextInstructions(pc, &pc1, &pc2, &pcarray, &pcallret);
        if (nextpathn == 0)
            return FALSE;

        if (nextpathn == 1)
        {
            pc = pc1;
            continue;
        }
        // here we have multi solutions
        if (nextpathn > 2)
        {
            int i;
            bool result = FALSE;
            for (i = 0; i < nextpathn; i++)
            {
                result |= chkReferencedBeforeChange(pcarray[i], tempid, r, index, depth);
                if (result)
                    break;
            }
            Safe_free(pcarray);
            return result;
        }
        // followint must be =2
        if (chkReferencedBeforeChange(pc1, tempid, r, index, depth))
            return TRUE;
        return chkReferencedBeforeChange(pc2, tempid, r, index, depth);
    }
    return FALSE;
}

// STA replacement needs to check
// 1. Dest reference is locked by other assignment
// 2. source modified before next destination reference
//    but is is ok that source is modified and dest not references anymore
// first stage is check if source modified before next "STA dest" or dest ref instruction locked
// for literal source, we check if dest is locked only

static bool canInstrOpReplaceByLit(int opcode)
{
    if (opcode == POC_MULWF || opcode == POC_ANDFW || opcode == POC_IORFW || opcode == POC_XORFW || opcode == POC_MVFW || opcode == POC_XORFW)
        return true;
    return false;
}

extern int pCodeOpCompare(pCodeOp *pcops, pCodeOp *pcopd);
static pCode *searched4_pcode[MAX_REF_SEARCH_DEPTH];
static int searched4_num = 0;

static bool chkSrcModifiedBeforeNextStaOrLock(pCode *pc, int tempid, reg_info *r, int index, pCodeOp *src, int depth)
{
    int nextpathn;
    pCode **pcarray = NULL;
    pCode *pc1, *pc2;
    // int waitret = 0;
    pCode *pcallret = NULL;

    if (!pc)
        return FALSE;

    if (!depth)
    {
        int i;
        for (i = 0; i < searched4_num; i++)
            searched4_pcode[i]->auxFlag &= 0xfffffffb;
        searched4_num = 0;
    }
    if (!isPCI(pc))
        pc = findNextInstruction(pc);

    while (pc)
    {
        // if new pcode already searched
        // if (pcodeInSearched(searched_pcode, pc, searched_num))
        if (pc->auxFlag & 0x04)
            return FALSE;
        pc->auxFlag |= 0x04;
        searched4_pcode[searched4_num++] = pc;
        depth++;
        if (searched4_num >= MAX_REF_SEARCH_DEPTH)
        {
            searched4_num = MAX_REF_SEARCH_DEPTH;
            return TRUE; // assume possible modified
        }
        if ((src_is_stkxx || src_is_global) && PCI(pc)->op == POC_CALL) // cannot have call
        {
            // depends what is checking
            // return TRUE; // modified
            return chkReferencedBeforeChange(pc, tempid, r, index, 0);
        }
        if (src->type == PO_PRODL || src->type == PO_PRODH)
        {
            if (PCI(pc)->op == POC_MULLW || PCI(pc)->op == POC_MULWF)
                return TRUE; // it is modified!!
        }
        if ((PCI(pc)->op == POC_MVWF || pcMVFFLIKE(pc) || PCI(pc)->op == POC_CLRF || PCI(pc)->op == POC_SETF)) // skip literal, only MVFF+MVWF
        {
            // 2 kinds if source or dest
            pCodeOp *pcop = (!pcMVFFLIKE(pc)) ? PCI(pc)->pcop : PCI(pc)->pcop2;
            if (compare_temp_pcop(pcop, tempid, r, index)) // if modified before change, the source is not referenced
                return FALSE;
            if (src->type != PO_LITERAL && pCodeOpCompare(pcop, src)) // literal will not sta, this means ref is modified?
            {
                // return TRUE; // 1 stage is enough
                //  enter stage2 means source modified
                return chkReferencedBeforeChange(pc, tempid, r, index, 0);
            } // special case, if MVFF a,b; and a is from literal.. it cannot be optimized!!

            if (pcMVFFLIKE(pc))
            {
                if ((src->type == PO_LITERAL || (src->type == PO_IMMEDIATE && !src->flags.imm2dir)) && pcMVFFLIKE(pc) && compare_temp_pcop(PCI(pc)->pcop, tempid, r, index))
                {
                    return TRUE;
                }
            }

            // any "lock" will return TRUE, we need to check STA temp
            // we need to check src change more carefully
        }
        else if ((PCI(pc)->outCond & PCC_REGISTER) && pCodeOpCompare(PCI(pc)->pcop, src))
        {
            return chkReferencedBeforeChange(pc, tempid, r, index, 0);
        }

#ifdef NO_OPTIMIZE1
        else if (PCI(pc)->addrMode == asm_FDA && PCI(pc)->isModReg && pCodeOpCompare(PCI(pc)->pcop, src))
        {
            return TRUE;
        }
#endif
        // else if(PCI(pc)->pcop && pCodeOpCompare(src,PCI(pc)->pcop) && PCI(pc)->locked)
        //	return TRUE;
        else // we need to check the referenced ones if lock
        {
            // debug
            /*
            if(PCI(pc)->pcop && PCI(pc)->pcop->type==MO_LITERAL &&
            PCOL(PCI(pc)->pcop)->lit==2)
            {
            fprintf(stderr,"lit 2\n");
            }
            */
            /*
            if((PCI(pc)->op==MOC_ROMPHP1 || PCI(pc)->op==MOC_ROMPLP1))
            {
            fprintf(stderr,"compare romplpxx.\n");
            }
            */
            if (PCI(pc)->pcop && compare_temp_pcop(PCI(pc)->pcop, tempid, r, index))
            {
                if (PCI(pc)->op == POC_MVWF || PCI(pc)->op == POC_CLRF || PCI(pc)->op == POC_SETF)
                    return FALSE;
                // usually ROMPHP1/ROMPLP1 is locked, because there are calculations
                // if in/out cond have all the target ... failed
                if (PCI(pc)->op == POC_LDPR || ((PCI(pc)->inCond & PCC_REGISTER) && (PCI(pc)->outCond & PCC_REGISTER)) || ((src->type == PO_LITERAL || (src->type == PO_IMMEDIATE && !src->flags.imm2dir)) && (PCI(pc)->inCond & PCC_REGISTER) && !canInstrOpReplaceByLit(PCI(pc)->op))) // ?? special case
                                                                                                                                                                                                                                                                                         // if (PCI(pc)->op == POC_LDPR || (src->type==PO_LITERAL && (PCI(pc)->inCond & PCC_REGISTER) && !canInstrOpReplaceByLit(PCI(pc)->op)))// ?? special case
                {

                    return TRUE;
                }
                // special case when H08D imm only mvfw
                if (HY08A_getPART()->isEnhancedCore >= 3 &&
                    (src->type == PO_IMMEDIATE && !src->flags.imm2dir) && PCI(pc)->op != POC_MVFW)
                    return TRUE;
                if (PCI(pc)->locked || PCI(pc)->source_cnt > 1)
                    return TRUE;
            }
        }
        nextpathn = findNextInstructions(pc, &pc1, &pc2, &pcarray, &pcallret);
        if (nextpathn == 0)
            return FALSE;

        if (nextpathn == 1)
        {
            pc = pc1;
            continue;
        }
        // here we have multi solutions
        if (nextpathn > 2)
        {
            int i;
            bool result = FALSE;
            for (i = 0; i < nextpathn; i++)
            {
                result |= chkSrcModifiedBeforeNextStaOrLock(pcarray[i], tempid, r, index, src, depth);
                if (result)
                    break;
            }
            Safe_free(pcarray);
            return result;
        }
        // followint must be =2
        if (chkSrcModifiedBeforeNextStaOrLock(pc1, tempid, r, index, src, depth))
            return TRUE;
        return chkSrcModifiedBeforeNextStaOrLock(pc2, tempid, r, index, src, depth);
    }
    return FALSE;
}

// to prevent branch sources ... we just add from/to in them

static pCode *searchedpc8[MAX_REF_SEARCH_DEPTH];
static int searched8 = 0;

static int replaceOpUntilSta(pCode *lda, pCode *pc, int tempid, reg_info *r, int index, pCodeOp *newpcop, int depth)
{

    //	int i;
    int n = 0;

    int nextpathn;
    pCode *pcallret = NULL;
    pCode **pcarray = NULL;
    pCode *pc1, *pc2;
    // int waitret = 0;
    if (!depth)
    {
        int i;
        for (i = 0; i < searched8; i++)
        {
            searchedpc8[i]->auxFlag &= 0xfffffff7;
        }
        searched8 = 0;
    }
    depth++;
    while (pc)
    {
        // if (pcodeInSearched(searchedpc, pc, searched))
        if (pc->auxFlag & 8)
            return n;
        pc->auxFlag |= 8;
        searchedpc8[searched8++] = pc;
        if (searched8 >= MAX_REF_SEARCH_DEPTH)
        {
            searched8 = MAX_REF_SEARCH_DEPTH; // this is must
            return n;                         // assume possible modified
        }
        // for final STA temp ==> break, because there are new replaces
        if ((PCI(pc)->op == POC_MVWF && searched8 > 1 && compare_temp_pcop(PCI(pc)->pcop, tempid, r, index)) ||
            (PCI(pc)->op == POC_SETF && searched8 > 1 && compare_temp_pcop(PCI(pc)->pcop, tempid, r, index)) ||
            (PCI(pc)->op == POC_CLRF && searched8 > 1 && compare_temp_pcop(PCI(pc)->pcop, tempid, r, index)) ||
            (pcMVFFLIKE(pc) && searched8 > 1 && compare_temp_pcop(PCI(pc)->pcop2, tempid, r, index)))
            return n; // finish

        // if (removed_count >= HY08A_options.ob_count - 1)
        //{
        //	fprintf(stderr, "now removed cnt = %d\n", removed_count);
        // }

        if (compare_temp_pcop(PCI(pc)->pcop, tempid, r, index) && PCI(pc)->source_cnt < 2)
        {
            // it shall be ok to copy
            pCodeOp *src;
            // const		pCodeOpLit zeroop = {{MO_LITERAL,"0x00"},0};

            // if (PCI(lda)->op == POC_MVL)
            // src = PCI(lda)->pcop
            // else
            if (lda != NULL)
            {
                src = PCI(lda)->pcop; // it could be MVWF or MVFW .. or MVL ??

                // if ( PCI(pc)->op == POC_MVWF)
                //{
                if (PCI(pc)->op == POC_LDPR)
                {
                    fprintf(stderr, "warning .. LDPR operand replaced..\n");
                    if (src->type == PO_LITERAL)
                    {
                        fprintf(stderr, "internal error %s:%d LDPR LIT REPLACE. Please contact related members.\n",
                                __FUNCTION__, __LINE__);
                        exit(-100);
                    }
                }
                if (PCI(pc)->isBitInst)
                {
                    if (src->type == PO_LITERAL)
                    {
                        fprintf(stderr, "internal error BIT LIT REPLACE. Please contact related members.\n");
                        exit(-101);
                    }
                    if (src->type == PO_IMMEDIATE)
                    {
                        PCORB(PCI(pc)->pcop)->pcor.r = PCOI(src)->r;
                        PCORB(PCI(pc)->pcop)->pcor.instance = PCOI(src)->index; // there is index!!
                        PCORB(PCI(pc)->pcop)->pcor.rIdx = PCOI(src)->rIdx;      // bit no change
                    }
                    else
                    {
                        PCORB(PCI(pc)->pcop)->pcor.r = PCOR(src)->r;
                        if (src->type == PO_GPR_TEMP)
                            PCORB(PCI(pc)->pcop)->pcor.instance = 0;
                        else
                            PCORB(PCI(pc)->pcop)->pcor.instance = PCOR(src)->instance; // this is important!!
                        PCORB(PCI(pc)->pcop)->pcor.rIdx = PCOR(src)->rIdx;             // bit no change
                        PCI(pc)->pcop->name = PCOR(src)->pcop.name;
                    }
                }
                else
                {
                    if (src->type == PO_LITERAL || (src->type == PO_IMMEDIATE && PCI(lda)->op == POC_MVL))
                    {
                        pCode *pcodenew;
                        switch (PCI(pc)->op)
                        {

                        case POC_MVFW:
                            // options.nopeep = 1;
                            // break;

                            pcodenew = newpCode(POC_MVL, pCodeOpCopy(src));
                            pcodenew->next = pc->next;
                            pcodenew->prev = pc->prev;
                            if (pc->next)
                                pc->next->prev = pcodenew;
                            else
                                pc->pb->pcTail = pcodenew;
                            if (pc->prev)
                                pc->prev->next = pcodenew;
                            else
                                pc->pb->pcHead = pcodenew;
                            PCI(pcodenew)->label = PCI(pc)->label;
                            replaceLabelPtrTabInPb(pc->pb, pc, pcodenew);
                            PCI(pcodenew)->cline = PCI(pc)->cline;
                            pc->prev = pc->next = NULL;

                            pc = pcodenew;
                            break;
                        case POC_IORFW:
                            pcodenew = newpCode(POC_IORL, pCodeOpCopy(src));
                            pcodenew->next = pc->next;
                            pcodenew->prev = pc->prev;
                            if (pc->next)
                                pc->next->prev = pcodenew;
                            else
                                pc->pb->pcTail = pcodenew;
                            if (pc->prev)
                                pc->prev->next = pcodenew;
                            else
                                pc->pb->pcHead = pcodenew;
                            PCI(pcodenew)->label = PCI(pc)->label;
                            replaceLabelPtrTabInPb(pc->pb, pc, pcodenew);
                            PCI(pcodenew)->cline = PCI(pc)->cline;
                            pc->prev = pc->next = NULL;
                            pc = pcodenew;
                            break;
                        case POC_XORFW:
                            pcodenew = newpCode(POC_XORLW, pCodeOpCopy(src));
                            pcodenew->next = pc->next;
                            pcodenew->prev = pc->prev;
                            if (pc->next)
                                pc->next->prev = pcodenew;
                            else
                                pc->pb->pcTail = pcodenew;
                            if (pc->prev)
                                pc->prev->next = pcodenew;
                            else
                                pc->pb->pcHead = pcodenew;
                            PCI(pcodenew)->label = PCI(pc)->label;
                            replaceLabelPtrTabInPb(pc->pb, pc, pcodenew);
                            PCI(pcodenew)->cline = PCI(pc)->cline;
                            // unlinkpCode(pc);
                            pc->prev = pc->next = NULL;
                            pc = pcodenew;
                            break;
                        case POC_ANDFW:
                            pcodenew = newpCode(POC_ANDLW, pCodeOpCopy(src));
                            pcodenew->next = pc->next;
                            pcodenew->prev = pc->prev;
                            if (pc->next)
                                pc->next->prev = pcodenew;
                            else
                                pc->pb->pcTail = pcodenew;
                            if (pc->prev)
                                pc->prev->next = pcodenew;
                            else
                                pc->pb->pcHead = pcodenew;
                            PCI(pcodenew)->label = PCI(pc)->label;
                            replaceLabelPtrTabInPb(pc->pb, pc, pcodenew);
                            PCI(pcodenew)->cline = PCI(pc)->cline;
                            // unlinkpCode(pc);
                            pc->prev = pc->next = NULL;

                            pc = pcodenew;
                            break;
                        case POC_MULWF:
                            pcodenew = newpCode(POC_MULLW, pCodeOpCopy(src));
                            pcodenew->next = pc->next;
                            pcodenew->prev = pc->prev;
                            if (pc->next)
                                pc->next->prev = pcodenew;
                            else
                                pc->pb->pcTail = pcodenew;
                            if (pc->prev)
                                pc->prev->next = pcodenew;
                            else
                                pc->pb->pcHead = pcodenew;
                            PCI(pcodenew)->label = PCI(pc)->label;
                            replaceLabelPtrTabInPb(pc->pb, pc, pcodenew);
                            PCI(pcodenew)->cline = PCI(pc)->cline;
                            // unlinkpCode(pc);
                            pc->prev = pc->next = NULL;

                            pc = pcodenew;
                            break;
                        default:
                            // literal replace fail
                            n--;                                                                               // later n++
                            if (PCI(pc)->op != POC_CLRF && PCI(pc)->op != POC_SETF && PCI(pc)->op != POC_MVWF) // some literal isreally MVL+WF
                            {
                                fprintf(stderr, "[internal error!!] unknown literal replace error.\n");
                                exit(-102);
                            }
                        }
                    }
                    else

                        PCI(pc)->pcop = pCodeOpCopy(src);
                }

                n++;
            }
        }
        nextpathn = findNextInstructions(pc, &pc1, &pc2, &pcarray, &pcallret);
        if (nextpathn == 0)
        {
            // if(n==2)
            // return 0;
            return n;
        }

        if (nextpathn == 1)
        {
            pc = pc1;
            continue;
        }
        // here we have multi solutions
        if (nextpathn > 2)
        {
            int i;
            for (i = 0; i < nextpathn; i++)
            {
                n += replaceOpUntilSta(lda, pcarray[i], tempid, r, index, newpcop, depth);
            }
            Safe_free(pcarray);
            // if(n==2) return 0;
            return n;
        }
        // followint must be =2
        return n + replaceOpUntilSta(lda, pc1, tempid, r, index, newpcop, depth) +
               replaceOpUntilSta(lda, pc2, tempid, r, index, newpcop, depth);
    }
    // if(n==2) return 0;
    return n;
}

/*
static pCode *find_sta_tempr(pCode* pc, reg_info *r, int index)
{
    while (pc)
    {
        if (
            (pc->type == PC_OPCODE &&
             PCI(pc)->op == POC_MVWF &&
             compare_temp_pcop(PCI(pc)->pcop, 0, r, index))
            ||
            //(pc->type == PC_OPCODE &&
            //	PCI(pc)->op == POC_MVFF &&
            //	compare_temp_pcop(PCI(pc)->pcop2, 0, r, index) )
            //||
            (pc->type == PC_OPCODE &&
             PCI(pc)->addrMode==asm_FDA && PCI(pc)->isModReg && !(PCI(pc)->inCond&PCC_REGISTER) && PCI(pc)->outCond==PCC_REGISTER &&
             compare_temp_pcop(PCI(pc)->pcop, 0, r, index)
             ||
             (pc->type == PC_OPCODE &&
              PCI(pc)->addrMode == asm_FA && PCI(pc)->isModReg && !(PCI(pc)->inCond&PCC_REGISTER) && PCI(pc)->outCond == PCC_REGISTER &&
              compare_temp_pcop(PCI(pc)->pcop, 0, r, index) // compare
             )

            )

        )
            return pc;
        pc = pc->next;
    }
    return NULL;
}
*/
static pCode *find_sta_tempid(pCode *pc, unsigned int regid)
{
    while (pc)
    {
        if (
            isPCI(pc) && !pcMVFFLIKE(pc) &&
            !(PCI(pc)->inCond & PCC_REGISTER) && // it should be ACC or literal
            (PCI(pc)->outCond & PCC_REGISTER) &&
            compare_temp_pcop(PCI(pc)->pcop, regid, NULL, 0))
            return pc;
        if (
            isPCI(pc) && pcMVFFLIKE(pc) &&
            compare_temp_pcop(PCI(pc)->pcop2, regid, NULL, 0))
            return pc;

        pc = pc->next;
    }
    return NULL;
}

// old versions use linear address STA temp check
// however, this is not correct because CPU don't run
// in that way

// after 2017 may, r is NULL instance is 0, max is 1024 (fixed)
static int getStaTempArray(pBlock *pb, pCode **pcarray, int id)
{
    pCode *pc = pb->pcHead;
    int n = 0;
    /* if (r)
    {
        while ((pc = find_sta_tempr(pc, r, instance)) != NULL)
        {
            pcarray[n++] = pc;
            if (n >= max)
                return n;
            pc = pc->next;
        }

        return n;
    }*/
    while ((pc = find_sta_tempid(pc, id)) != NULL)
    {
        pcarray[n++] = pc;
        if (n >= 1024)
            return n;
        pc = pc->next;
    }
    return n;
}

static pCode *findPrevAccSrc(pCode *pci)
{
    if (pcMVFFLIKE(pci))
        return pci; // return myself!!
    if (PCI(pci)->op == POC_CLRF)
    {
        pCode *pcb = newpCode(POC_MVL, newpCodeOpLit(0));
        return pcb;
    }
    if (PCI(pci)->op == POC_SETF)
    {
        pCode *pcb = newpCode(POC_MVL, newpCodeOpLit(0xff));
        return pcb;
    }
    if (PCI(pci)->label)
        return NULL; // we don't optimize label
    while ((pci = findPrevInstruction(pci->prev)) != NULL)
    {
        pCode *pp = findPrevInstruction(pci->prev);
        if (pp && PCI(pp)->isSkip)
            return NULL; // if skip in the source... we give up

        if (PCI(pci)->op == POC_MVWF || pcMVFFLIKE(pci))
        {
            if (PCI(pci)->label)
                return NULL;     // we don't optimize label
            if (pcMVFFLIKE(pci)) //&& PCI(pci)->pcop2->type == PO_PRODL)
                return NULL;     // this is a branch

            // if (PCI(pci)->op == POC_MVWF)
            // return pci; // this can be a source, too?
            continue;
        }
        if (PCI(pci)->isBranch)
            return NULL;

        return pci;
    }
    return pci;
}

// if STA referenced to itself, we lock that LDA/ADD/SUBB
/*
static void lock_self_ref(pCode *pc)
{
    pCode *pcs[5];
    pCode *pc1;
    int i, j;

    memset(pcs, 0, sizeof(pcs));
    // we check previous 5 instructions
    // if there is LDA, we check lda to this pc if target is referenced
    // if there is no LDA, we check if target is referneced
    pc1 = findPrevAccSrc(pc);
    for (i = 0; i<5; i++)
    {
        pcs[i] = pc1;
        if (!pc1)
            break;
        pc1 = findPrevInstruction(pc1->prev);
    }
    for (i = 0; i<5; i++)
    {
        if (pcs[i] && PCI(pcs[i])->op == POC_MVFW)
            break;
    }
    for (j = 0; j <= i && j<5; j++)
    {
        if (pcs[j] && PCI(pcs[j])->pcop &&
                pCodeOpCompare(PCI(pcs[j])->pcop, PCI(pc)->pcop))
            PCI(pcs[j])->locked = 1; // self reference, we need to lock!!
    }

}
*/

bool ifpcodeCanReplaceFollowingSta(pCodeInstruction *pci)
{
    //	reg_info *r;
    src_is_stkxx = 0;
    src_is_global = 0;
    if (!pci)
        return FALSE;

    // if (pci->op == MOC_CLRA)
    // return TRUE;
    //  if there is a skip before, it is a NO NO

    if (pci->pc.prev && isPCI(pci->pc.prev) && PCI(pci->pc.prev)->isSkip)
        return FALSE; // this is important!!

    if (pci->op == POC_MVL && (pci->pcop->type == PO_LITERAL || pci->pcop->type == PO_IMMEDIATE)) // only literal?
        return TRUE;

    if (pci->op != POC_MVFW)
        return FALSE;
    // if (pci->pcop->type == PO_GPR_TEMP && PCOR(pci->pcop)->r->rIdx>0x1000)
    if ((pci->pcop->type == PO_PRODL || pci->pcop->type == PO_PRODH) &&
        HY08A_getPART()->isEnhancedCore >= 3 && HY08A_getPART()->isEnhancedCore != 5) // only new part can write!!
        return TRUE;
    if (pci->pcop->type == PO_GPR_TEMP)
    {
        if (PCOR(pci->pcop)->r->rIdx <= 0x1000)
        {
            // if (stkxxPromoted)
            // return FALSE; // STKXX cannot replace following STA after promoted!!
            src_is_stkxx = 1; // this is stkxx
        }
        return TRUE;
    }

    if (pci->pcop->type == PO_DIR) // some times there are only dir name with instance
    {
        if (!PCOR(PCI(pci)->pcop)->r) // it is not volatile
        {
            src_is_global = 1;
            return TRUE;
        }
        if (PCOR(PCI(pci)->pcop)->r->isVolatile || PCOR(PCI(pci)->pcop)->r->addrReferenced)
            return FALSE;
        src_is_global = 1;
        return TRUE;
    }
    if (pci->pcop->type == PO_IMMEDIATE)
        if (PCOI(pci->pcop)->r && !PCOI(pci->pcop)->r->isVolatile && !PCOI(pci->pcop)->r->addrReferenced)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }

    return FALSE;
}

// if some var is "calculated" or from special registers
// they cannot be optimized in following scope
// until new (temp) value is assigned
static pCode *searched10_pcode[8192];
static int checked10num = 0;

static void LockFollowingReference(pCode *pc, pCodeOp *pcop, int depth)
{
    pCode *pc1, *pc2;
    pCode **pcs;
    //	pCode *pcd1,*pcd2;
    int n;
    // int waitret = 0;
    pCode *pcallret = NULL;
    if (!depth)
    {
        int i;
        for (i = 0; i < checked10num; i++)
            searched10_pcode[i]->auxFlag &= 0xffffffef;
        checked10num = 0;
    }
    depth++;
    // for debug
    //	pcd1=pc->prev;
    //	pcd2=pc->next;

    /*
    if(isPCI(pcd2) && PCI(pcd2)->op==MOC_JMP)
    {
    fprintf(stderr,"next jmp here.\n");
    }
    */

    while (pc)
    {
        // don't write too large code!!
        // if (pcodeInSearched(searched_pcode, pc, checkednum))
        if (pc->auxFlag & 0x10)
            return;
        pc->auxFlag |= 0x10;
        searched10_pcode[checked10num++] = pc;

        // special case
        if (pcMVFFLIKE(pc))
        {
            if (pCodeOpCompare(pcop, PCI(pc)->pcop2))
                return;
            if (pCodeOpCompare(pcop, PCI(pc)->pcop))
                PCI(pc)->locked = 1;
        }
        else if (PCI(pc)->op == POC_MVWF || PCI(pc)->op == POC_CLRF || PCI(pc)->op == POC_SETF)
        {
            if (checked10num > 1 && pCodeOpCompare(pcop, PCI(pc)->pcop))
                return;
        }
        else
        {
            if (PCI(pc)->pcop && pCodeOpCompare(pcop, PCI(pc)->pcop))
                PCI(pc)->locked = 1;
        }
        n = findNextInstructions(pc, &pc1, &pc2, &pcs, &pcallret);
        if (!n)
            return;
        if (n > 2)
        {
            int i;
            for (i = 0; i < n; i++)
                LockFollowingReference(pcs[i], pcop, depth);
            Safe_free(pcs);
            return;
        }
        else if (n == 2)
        {
            LockFollowingReference(pc1, pcop, depth);
            LockFollowingReference(pc2, pcop, depth);
            return;
        }
        pc = pc1;
    }
}

// r is null, instance is 0
static pCode *searched20[16384];
static int searched20_cnt = 0;

static int giveCountToReferences(pCode *pc, int id, int depth, int srcislit, int srclit)
{
    pCode *pc1, *pc2, **pcs;
    int n;
    pCode *pcallret = NULL;
    // int waitret = 0;

    if (!depth)
    {
        int i;
        for (i = 0; i < searched20_cnt; i++)
            searched20[i]->auxFlag &= 0xffffffdf; // (~0x20)
        searched20_cnt = 0;
    }
    depth++;

    while (pc)
    {
        // if (pcodeInSearched(searched, pc, searched_cnt))
        if (pc->auxFlag & 0x20)
            return 0;
        if (searched20_cnt >= 16384)
            return 1;
        pc->auxFlag |= 0x20;
        searched20[searched20_cnt++] = pc;
        if (depth == 1 && searched20_cnt == 1)
        {
            n = findNextInstructions(pc, &pc1, &pc2, &pcs, &pcallret);
            pc = pc1;
            continue;
        }
        if (pcMVFFLIKE(pc))
        {

            if (compare_temp_pcop(PCI(pc)->pcop, id, NULL, 0))
            {
                PCI(pc)->source_cnt++;
            }
            if (compare_temp_pcop(PCI(pc)->pcop2, id, NULL, 0))
                return 0;
        }

        else if ((PCI(pc)->inCond & PCC_REGISTER) && PCI(pc)->pcop &&
                 compare_temp_pcop(PCI(pc)->pcop, id, NULL, 0) // add then return is ok
        )
        {
            PCI(pc)->source_cnt++;
            PCI(pc)->srcislit |= srcislit;
        }
        else if ((PCI(pc)->outCond & PCC_REGISTER) && PCI(pc)->pcop &&
                 compare_temp_pcop(PCI(pc)->pcop, id, NULL, 0))
            return 0;

        n = findNextInstructions(pc, &pc1, &pc2, &pcs, &pcallret);
        if (!n)
            return 0;
        if (n == 2)
        {
            return giveCountToReferences(pc1, id, depth, srcislit, srclit) |
                   giveCountToReferences(pc2, id, depth, srcislit, srclit);
        }
        else if (n > 2)
        {
            int i;
            int result = 0;
            for (i = 0; i < n; i++)
                result |= giveCountToReferences(pcs[i], id, depth, srcislit, srclit);
            return result;
        }
        pc = pc1;
    }
    return 0;
}

// after 2017.. r is null, instance is 0
static int BlockTempVarLock(pBlock *pb, int id)
{
    pCode *sta_pos[1024];
    pCode *pc;
    pCode *pc1;
    int i;
    int staN;

    staN = getStaTempArray(pb, sta_pos, id);
    for (i = 0; i < staN; i++)
    {
        // if the source is volatile/sfr/calculated ... following reference should be locked
        pc = sta_pos[i];
        // pc1=pc->prev;
#if PREVPPNNOPT
        lock_self_ref(pc); // is this necessary???
#endif

        pc1 = findPrevAccSrc(pc); // this will skip the STA and up
        // if not literal and TEMP, we lock!!

        if (ifpcodeCanReplaceFollowingSta(PCI(pc1)))
            continue;

        LockFollowingReference(pc, PCI(pc)->pcop, 0);
    }
    if (staN > 0 && PCI(sta_pos[0])->pcop->flags.parapcop) // it is para, we need to give source count from outside
    {
        giveCountToReferences(pb->pcHead, id, 0, 0, 0);
    }

    for (i = 0; i < staN; i++)
    {
        int srcislit = 0, srclit = 0;
        pc1 = findPrevAccSrc(sta_pos[i]); // this will skip the STA and up

        if (pc1 && PCI(pc1)->op == POC_MVL)
        {
            srcislit = 1;
            srclit = PCOL(PCI(pc1)->pcop)->lit;

            // if (isPCI(pc->next) && PCI(pc->next)->op == POC_JMP && removed_count>=22)
            //{
            //	fprintf(stderr, "clrf+jmp?\n");
            // }
        }
        giveCountToReferences(sta_pos[i], id, 0, srcislit, srclit);
    }
    return 0;
}

// Correct the algorithm:
// if the temp has 2 more possible source, the reference must be locked
// its locking is not because its reference is calculated

// but it has more than 1 sta sources
// 2016 Nov,
// we combines STKXX to formerly the temp var,
// there is a possibility
//
//        if (xx)
//				stk=0;
//        return stk+yyy;
// the stk has STA source from outside the function
// we have to add them

/*
static int getBranchNumber(pBranch *pbr)
{
    int i = 0;
    while (pbr)
    {
        i++;
        pbr = pbr->next;
    }
    return i;
}
*/
static pCode *history40[MAX_REF_SEARCH_DEPTH];
static int searched40 = 0;

static int tempNotUsedAnyMoreWithDestNotLockAndRefCntL2(pCode *pc, int destid, int tempid, reg_info *r, int index, int level)
{
    // second stage, really not possible if referernced before change
    // only change after referenced will return true

    int nextpathn;
    pCode **pcarray = NULL;
    pCode *pc1, *pc2;
    pCode *pcallret = NULL;
    // int waitret = 0;
    if (!level)
    {
        int i;
        for (i = 0; i < searched40; i++)
            history40[i]->auxFlag &= 0xffffffbf; //~0x40
        searched40 = 0;
    }
    while (pc)
    {
        // if (pcodeInSearched(history, pc, searched))
        if (pc->auxFlag & 0x40)
            return FALSE;
        pc->auxFlag |= 0x40;
        history40[searched40++] = pc;
        level++;
        if (searched40 >= MAX_REF_SEARCH_DEPTH)
        {
            searched40 = MAX_REF_SEARCH_DEPTH; // cannot exceed
            return FALSE;                      // assume refrenced
        }
        if ((compare_temp_pcop(PCI(pc)->pcop, destid, NULL, -1) || compare_temp_pcop(PCI(pc)->pcop2, destid, NULL, -1)) && (PCI(pc)->locked || PCI(pc)->source_cnt > 1))
            return FALSE;
        if (compare_temp_pcop(PCI(pc)->pcop, tempid, r, index) || compare_temp_pcop(PCI(pc)->pcop2, tempid, r, index))
            return FALSE;
        nextpathn = findNextInstructions(pc, &pc1, &pc2, &pcarray, &pcallret);
        if (nextpathn == 0)
            return TRUE; // not referenced!!

        if (nextpathn == 1)
        {
            pc = pc1;
            continue;
        }
        // here we have multi solutions
        if (nextpathn > 2)
        {
            int i;
            bool result = FALSE;
            for (i = 0; i < nextpathn; i++)
            {
                result |= tempNotUsedAnyMoreWithDestNotLockAndRefCntL2(pcarray[i], destid, tempid, r, index, level);
                if (result)
                    break;
            }
            Safe_free(pcarray);
            return result;
        }
        // followint must be =2
        if (!tempNotUsedAnyMoreWithDestNotLockAndRefCntL2(pc1, destid, tempid, r, index, level))
            return FALSE;
        return tempNotUsedAnyMoreWithDestNotLockAndRefCntL2(pc2, destid, tempid, r, index, level);
    }
    return TRUE;
}

// if a->b b->c , we can say a->c is ok, b replaced by a

static int BlockTempBridgeRemove(pBlock *pb)
{
    unsigned int temp;
    set *regset;
    // set *tempset;

    pCode *pc;
    //	pCode *pc1;
    pCode *pc2;
    pCode *pc3;
    //	pCode *ref_final;
    pCodeOp *srcOp;
    /* TODO: Recursion detection missing, should emit a warning as recursive code will fail. */
    // return 0;
    int staN = 0;
    int n = 0;
    //	int nn=0;
    int i;

    varopt *vop;
    // static int times=0;
    // int checkedreg=0;

    pCode *sta_pos[1024];

    if (!pb || options.nopeep || removed_count >= HY08A_options.ob_count) // || pb->call_level >= ms_pcstack_level)
        return 0;

        /*if (removed_count == 2)
    {
        fprintf(stderr, "BR2\n");
    }*/

        // return 0;
#if NOBRIATSTG2
    if (ana_stage == 2)
        return 0;
#endif
    // if(++times>1)
    // return 0;

    // if (pb->call_level >= ms_pcstack_level)
    // return 0;

    //  Find the highest rIdx used by this function for return.
    regset = pb->tregisters;

    while (regset)
    {
        reg_info *tinfo = ((reg_info *)regset->item);
        temp = tinfo->rIdx;
        // we check if it has "STA"

        // 2017 NOV, STKXX include in Optimize list, only
        // prevent CALL modify STKXX

        if (temp < 1000)
        {
            // write to STKXX can only be promote, not be replace
            // this is different direction
            regset = regset->next;
            // fprintf(stderr, "id=0x%X\n", temp);
            continue;
        }

        cleanBeforeBridgeOptimize(pb);

        BlockTempVarLock(pb, temp); // lock the required reference

        pc = pb->pcHead;
        staN = 0;
        staN = getStaTempArray(pb, sta_pos, temp);
        if (!staN)
        {
            regset = regset->next;
            continue;
        }
        // now we analysis the code between sta

        for (i = 0; i < staN; i++) // bottom to top
        {

            //			int skipblock;
            pc = sta_pos[i];
            // the first STA has the following conditions we should give up
            // 1. this sta has no label
            // 2. prev instruction must be LDA  (or STA, it is possible double STA)
            // 3. the LDA source should be a generic register (temp is fine)

            if (PCI(sta_pos[i])->op != POC_MVWF && !pcMVFFLIKE(sta_pos[i])

                && PCI(sta_pos[i])->op != POC_CLRF && PCI(sta_pos[i])->op != POC_SETF)
                continue;

            pc2 = findPrevAccSrc(pc);
            /*
            if(ana_stage==2 && pc2 && isPCI(pc2) && PCI(pc2)->pcop && PCI(pc2)->pcop->name &&
            strstr(PCI(pc2)->pcop->name,"_the_state") && PCOR(PCI(pc2)->pcop)->instance==0)
            {
            fprintf(stderr,"ref the_state\n");
            pb->stopOptForCheck=1;
            }*/
            /*
            if(pc2 && isPCI(pc2) && PCI(pc2)->pcop && PCI(pc2)->pcop->type==MO_NONE)
            {
            fprintf(stderr,"special pc2\n");
            }
            */

            if (!ifpcodeCanReplaceFollowingSta(PCI(pc2)))
                continue;

            // pc2=pc->prev;

            // if (PCI(pc2)->op == POC_MVL)
            // theop = popGetLit(0);
            // else
            srcOp = PCI(pc2)->pcop; // the op is the source
            if (srcOp->type == PO_IMMEDIATE)
                srcOp->flags.imm2dir = (PCI(pc2)->op != POC_MVL);
            if (pCodeOpCompare(srcOp, PCI(pc)->pcop)) // lda/sta same ==> skip
                continue;

            /*
            if(srcOp->type==MO_LITERAL )
            {
            fprintf(stderr,"liter bridge\n");
            }*/
            // here is possible the STA can be optimized
            // we try to get final reference of the temp
            // there must be at least 1 reference, or it will be optimized out before this function
            do
            {
                // special case!!!
                pc3 = findNextInstruction(pc->next); //

            } while (pc3 && isPCI(pc3) && PCI(pc3)->op == POC_MVWF && pCodeOpCompare(PCI(pc3)->pcop, PCI(pc)->pcop));

            // temp is the bridge var, srcOp is its src

            // if source is not used anymore, it can be replaced
            if (srcOp->type == PO_GPR_TEMP && tempNotUsedAnyMoreWithDestNotLockAndRefCntL2(pc3, temp,
                                                                                           PCOR(srcOp)->r->rIdx, NULL, -1, 0))
            {
                n += replaceOpUntilSta(pc2, pc, temp, NULL, 0, srcOp, 0);
                vop = newVarOpt(tinfo, PCOR(srcOp)->r, PCOR(srcOp)->instance, -1, 0);
                addSet(&pb->opt_list, vop);
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return n;
                }
                continue;
            }

            // if src modified before bridge var modified, we cannot replace it
            // another kind is that

            if (chkSrcModifiedBeforeNextStaOrLock(pc3, temp, NULL, 0, srcOp, 0))
                continue;
            // following is recursive function

            switch (srcOp->type)
            {
            case PO_GPR_REGISTER:
            case PO_GPR_TEMP:
            case PO_GPR_BIT:
                vop = newVarOpt(tinfo, PCOR(srcOp)->r, PCOR(srcOp)->instance, -1, 0);
                break;
            case PO_LITERAL:
                vop = newVarOpt(tinfo, NULL, 0, PCOL(srcOp)->lit, 0);
                break;
            case PO_IMMEDIATE:
                vop = newVarOpt(tinfo, NULL, 0, -1, 0);
                break;
            default:
                vop = NULL;
            }
            if (vop)
            {
                // if (!checkVarOptDouble(pb->opt_list, vop))
                addSet(&pb->opt_list, vop);
                // else
                // Safe_free(vop);
                // addSet(&pb->opt_list, (void*)vop);
            }
            n += replaceOpUntilSta(pc2, pc, temp, NULL, 0, srcOp, 0);
            removed_count++;
            if (removed_count >= HY08A_options.ob_count)
            {
                options.nopeep = 1;
                return n;
            }
        }
        // after all graph is construct, we check for "all to has single from, then do it"
        // if (n)
        // return n; // after a var is changed, we need peephole optimization again!!
        regset = regset->next;

    } // while
    return n;
}

// static int BlockJMPRET2RET(pBlock *pb) // leave to linker!!
//{
//     pCode *pc;
//     int n = 0;
//     pc = pb->pcHead;
//     while (pc)
//     {
//         if (pc->type == PC_OPCODE &&
//                 PCI(pc)->op == POC_JMP && PCI(pc)->pcop->type != PO_STR) // if changed from call, it will be MO_STR
//         {
//             pCode *jumpto = findLabelinpBlock(pc->pb, PCOLAB(PCI(pc)->pcop));
//             jumpto = findNextInstruction(jumpto);
//             if (jumpto && PCI(jumpto)->op == POC_RET)
//             {
//                 // change it to RET!!
//                 pCode *pc1 = pc->prev;
//                 pCode *pc2 = pc->next;
//                 pCode *pc3 = newpCode(POC_RET, NULL);
//                 pc3->next = pc2;
//                 pc3->prev = pc1;
//                 pc3->pb = pc->pb;
//                 PCI(pc3)->label = PCI(pc)->label;// the branch will not be unlinked.
//                 PCI(pc3)->cline = PCI(pc)->cline;
//
//                 unlinkpCode(pc);
//
//                 if (pc1 == NULL)
//                 {
//					assert(pc3);
//                     pc3->pb->pcHead = pc3;
//                 }
//                 else
//                 {
//                     pc1->next = pc3;
//                 }
//                 if (pc2 == NULL)
//                     pc3->pb->pcTail = pc3;
//                 else
//                     pc2->prev = pc3;
//                 pc = pc1; // because pc is unlinked!!
//
//
//
//
//                 //PCI(pc)->op = POC_RET;
//                 //PCI(pc)->num_ops = 0;
//                 //strcpy(PCI(pc)->mnemonic, "RET");
//                 //strcpy(PCI(pc)->mne2, "RET");
//                 //free(PCI(pc)->pcop); // the op is no need anymore
//                 //PCI(pc)->pcop = NULL;
//                 n++;
//             }
//         }
//		assert(pc);
//         pc = pc->next;
//     }
//     return n;
// }

static void set_reg_use(pCodeOp *pcop)
{
    switch (pcop->type)
    {
    case (PO_GPR_REGISTER): // A general purpose register
    case (PO_GPR_TEMP):     // A general purpose temporary register
    case (PO_GPR_POINTER):  // A general purpose pointer
    case (PO_SFR_REGISTER): // A special function register (e.g. PORTA)
    case (PO_DIR):          // Direct memory (8051 legacy)

    case (PO_XDATA): // for memory >= 0x200): assume XDATA

        if (PCOR(pcop)->r)
            PCOR(pcop)->r->wasUsed = 1;
        return;
    case (PO_GPR_BIT): // SPECIAL!!
        if (PCORB(pcop)->pcor.r)
            PCORB(pcop)->pcor.r->wasUsed = 1; // !! important!!
        return;
    case (PO_IMMEDIATE):
        if (PCOI(pcop)->r)
            PCOI(pcop)->r->wasUsed = 1;

    default:
        return;
    }
}

void reCheckLocalVar(pFile *pf)
{
    pBlock *pb;
    pCode *pc;
    set *allregs[] = {dynAllocRegs /*, dynStackRegs, dynProcessorRegs*/,
                      dynDirectRegs, dynDirectBitRegs /*, dynInternalRegs */, NULL};

    set *rset;
    int i;
    if (!pf)
        return;
    for (i = 0; allregs[i]; i++)
    {
        rset = allregs[i];
        reg_info *dReg;

        for (dReg = setFirstItem(rset); dReg; dReg = setNextItem(rset))
        {
            dReg->wasUsed = 0;
        }
    }
    for (pb = pf->pbHead; pb; pb = pb->next)
    {
        for (pc = pb->pcHead; pc; pc = pc->next)
        {
            if (!isPCI(pc))
                continue;
            if (PCI(pc)->pcop)
                set_reg_use(PCI(pc)->pcop);
            if (PCI(pc)->pcop2)
                set_reg_use(PCI(pc)->pcop2);
        }
    }
}

static int replaceTempOfCSym(pCodeFunction *pcf, pCodeOp *formerlypcop, pCodeOp *newpcop)
{
    int replacecount = 0;
    if (pcf == NULL)
        return 0;

    char *fname = PCF(pcf)->fname;

    symbol *csym;
    for (csym = setFirstItem(data->syms); csym; csym = setNextItem(data->syms))
    {
        if (!csym->level || !csym->localof)
            continue;
        if (strcmp(fname + 1, csym->localof->name)) // there is _
            continue;
        if (!csym->allocreq && csym->reqv)
        {
            symbol *TempSym = OP_SYMBOL(csym->reqv);

            if (!TempSym->isspilt || TempSym->remat || TempSym->rematX)
            {

                int a;
                for (a = 0; a < 4; a++)
                {
                    reg_info *r = TempSym->regs[a];
                    if (!r)
                        continue;
                    if (!strcmp(r->name, PCOR(formerlypcop)->r->name))
                    {
                        TempSym->regs[a] = PCOR(newpcop)->r;
                        replacecount++;
                    }
                }
            }
        }
    }
    return replacecount;
}

// this function we analyse the operation of FSR0
// it is possible that
// 1. the access can be re-mat
// 2. the pointer later ++ can be optimize
// 3. it is possible to use PLUSW0

typedef struct poincInfo
{
    unsigned int isrd : 1;
    unsigned int iswr : 1;
    unsigned int isFollowing : 1;
    unsigned int hasFollowing : 1;
    pCode *poincAcc;
    pCode *fsr0lassign;
    pCode *fsr0lsrc;
    pCode *fsr0hassign;
    pCode *fsr0hsrc;
    struct poincInfo *next;
    struct poincInfo *prev;
} poincInf;

poincInf *newPoincInf(pCode *pc)
{
    pCode *pc1;
    poincInf *p = calloc(1, sizeof(poincInf));

    assert(p);
    // memset(p, 0, sizeof(poincInf));

    p->poincAcc = pc;
    if (PCI(pc)->isModReg)
        p->iswr = 1;
    else
        p->isrd = 1;

    // search back to see if there is poinc before fsr assign
    for (pc1 = pc; pc1; pc1 = pc1->prev)
    {
        if (isPCI(pc1) && (PCI(pc1)->op == POC_MVWF || PCI(pc1)->op == POC_MVFW) && PCI(pc1)->pcop->type == PO_POINC)
        {
            p->isFollowing = 1;
            break;
        }
        if (isPCI(pc1) && PCI(pc1)->op == POC_MVWF && PCI(pc1)->pcop->type == PO_FSR0H)
        {
            p->fsr0hassign = pc1;
            if (p->fsr0lassign)
                break;
        }
        else if (isPCI(pc1) && PCI(pc1)->op == POC_MVWF && PCI(pc1)->pcop->type == PO_FSR0L)
        {
            p->fsr0lassign = pc1;
            if (p->fsr0hassign)
                break;
        }
    }
    if (p->fsr0lassign)
    {
        for (pc1 = p->fsr0lassign; pc1; pc1 = pc1->prev)
        {
            if (isPCI(pc1) && (PCI(pc1)->outCond & PCC_W))
            {
                p->fsr0lsrc = pc1;
                break;
            }
        }
    }
    if (p->fsr0hassign)
    {
        for (pc1 = p->fsr0hassign; pc1; pc1 = pc1->prev)
        {
            if (isPCI(pc1) && (PCI(pc1)->outCond & PCC_W))
            {
                p->fsr0hsrc = pc1;
                break;
            }
        }
    }

    return p;
}

void delPoincInfLst(poincInf *p) // i think it will not exceed 100, generally
{
    if (!p)
        return;
    if (p->next)
        delPoincInfLst(p->next);
    free((void *)p);
}

static int fsr0ana(pBlock *pb)
{
    // step0, if there is POINC, we find the FSR0 source
    // it can be MVL or MVFW
    poincInf *poincs = NULL;
    poincInf *poiTail = NULL;
    poincInf *poi = NULL;

    pCode *pc;
    pCode *npc;
    pCode *npct; // tail
    pCode *p1c;
    pCode *pcPrev;
    pCode *pcNext;
    pCodeOpImmd *immOp; // there will be a IMM!!
    // pCodeOpReg *tmpop; // otherwise ... remat fail , it is gpreg
    reg_info *r;
    int count = 0;

    if (!pb || options.nopeep || removed_count >= HY08A_options.ob_count || HY08A_options.dis_fsr_opt)
        return 0;
    // first we construct the poinc list
    for (pc = pb->pcHead; pc; pc = pc->next)
    {
        if (isPCI(pc) && (PCI(pc)->op == POC_MVWF || PCI(pc)->op == POC_MVFW) && PCI(pc)->pcop->type == PO_POINC0)
        {
            poi = newPoincInf(pc);
            if (poincs == NULL)
            {
                poincs = poiTail = poi;
            }
            else
            {
                if (poi->isFollowing)
                    poiTail->hasFollowing = 1;
                poi->prev = poiTail;
                poiTail->next = poi;
                poiTail = poi;
            }
        }
    }
    // after poinc0 searched
    // its source should be a pointer
    // we analyze the pointer's behavior in the pBlock
    // we check the low byte first
    for (poi = poincs; poi; poi = poi->next)
    {
        // for PLUSW0 .. it must be single byte read
        int flag = 0;
        if (!poi->fsr0lsrc || poi->hasFollowing)
            continue;
        // here we check if it is a small array (<128) access,
        // and the pointer is not used in the following code
        // we can optimize to PLUSW0

        // we must make sure the source will be destroyed before next
        // reference
        // we do it for GPR_TEMP register only
        if (PCI(poi->fsr0lsrc)->pcop->type != PO_GPR_TEMP)
            continue; // we optimize temp only
        r = PCOR(PCI(poi->fsr0lsrc)->pcop)->r;

        if (chkReferencedBeforeChange(poi->poincAcc, r->rIdx, 0, 0, 0))
        {
            continue;
        }

        // then we find the pointer (temp reg)'s source
        for (pc = poi->fsr0lsrc; pc; pc = pc->prev)
        {
            if (isPCI(pc) && PCI(pc)->outCond == PCC_REGISTER && PCI(pc)->pcop &&
                pCodeOpCompare(PCI(poi->fsr0lsrc)->pcop, PCI(pc)->pcop))
            {
                break;
            }
        }

        if (!pc) // this is where low byte assigned or modified
            continue;
        // for fixed array case, it is literal + literal
        if (PCI(pc)->op == POC_MVWF && pc->prev && pc->prev->prev &&
            isPCI(pc->prev) && isPCI(pc->prev->prev) &&
            PCI(pc->prev)->op == POC_ADDLW //&&
                                           // PCI(pc->prev->prev)->op == POC_MVFW  // MVFW+ADDL+MVWF
        )
        {
            //			pCodeOp *pcop1 = PCI(pc->prev->prev)->pcop;
            pCodeOp *pcop2 = PCI(pc->prev)->pcop;
            immOp = NULL;
            // tmpop = NULL;
            if (pcop2->type == PO_IMMEDIATE)
            {
                immOp = PCOI(pcop2);
                if (immOp->_inX)
                    immOp = NULL;
            }
            // else if (pcop2->type == PO_LITERAL && PCI(pc->prev->prev)->pcop->type == PO_IMMEDIATE)
            // immOp = PCOI(PCI(pc->prev->prev)->pcop);
        }
        else
            continue;

        // size must be less than 128

        if (immOp == NULL || immOp->r->size > 127)
            continue;

        // then we make sure high byte is as we want, MVL, BTSZ, ADDL, MVWF

        if (!(pc->next && pc->next->next && pc->next->next->next && pc->next->next->next->next &&
              isPCI(pc->next) && PCI(pc->next)->op == POC_MVL &&
              isPCI(pc->next->next) && PCI(pc->next->next)->op == POC_BTSZ &&
              isPCI(pc->next->next->next) && PCI(pc->next->next->next)->op == POC_ADDLW &&
              isPCI(pc->next->next->next->next) && PCI(pc->next->next->next->next)->op == POC_MVWF))
            continue;
        // if (PCI(pc->prev->prev)->label ) // it cannot has label!!
        // continue;

        // now we are very sure we can replace with PLUSW0 now
        pcPrev = pc->prev->prev;
        removepCode(&(poi->fsr0lassign));
        removepCode(&(poi->fsr0hassign));

        if (PCI(pcPrev)->op != POC_MVFW)
        {
            // save the ACC to a new temp
            npct = npc = newpCode(POC_MVWF, newpCodeOpReg(-1));
            // p1c = newpCode(POC_MVL, pCodeOpCopy((pCodeOp*)immOp)); npct->next = p1c; p1c->prev = npct; npct = p1c; poi->fsr0lsrc = p1c;
            // p1c = newpCode(POC_MVWF, pCodeOpCopy((pCodeOp*)&pc_fsr0l));			npct->next = p1c; p1c->prev = npct; npct = p1c; poi->fsr0lassign = p1c;
            // p1c = newpCode(POC_MVL, pCodeOpCopy(PCI(pc->next)->pcop)); npct->next = p1c; p1c->prev = npct; npct = p1c; poi->fsr0hsrc = p1c;
            // p1c = newpCode(POC_MVWF, pCodeOpCopy((pCodeOp*)&pc_fsr0h));			npct->next = p1c; p1c->prev = npct; npct = p1c; poi->fsr0hassign = p1c;
            p1c = newpCode(POC_LDPR, (pCodeOp *)immOp);
            npct->next = p1c;
            p1c->prev = npct;
            npct = p1c;
            poi->fsr0hassign = poi->fsr0lassign = p1c;
            flag = 1;
        }
        else
        {
            pcPrev = pcPrev->prev; //.. one more instruction lda ... removed later
            // we make another pcode chain
            // npct = npc = newpCode(POC_MVL, pCodeOpCopy((pCodeOp*)immOp));		poi->fsr0lsrc = npc;
            // p1c = newpCode(POC_MVWF, pCodeOpCopy((pCodeOp*)&pc_fsr0l));			npct->next = p1c; p1c->prev = npct; npct = p1c; poi->fsr0lassign = p1c;
            // p1c = newpCode(POC_MVL, pCodeOpCopy(PCI(pc->next)->pcop)); npct->next = p1c; p1c->prev = npct; npct = p1c; poi->fsr0hsrc = p1c;
            // p1c = newpCode(POC_MVWF, pCodeOpCopy((pCodeOp*)&pc_fsr0h));			npct->next = p1c; p1c->prev = npct; npct = p1c; poi->fsr0hassign = p1c;
            npct = npc = newpCode(POC_LDPR, pCodeOpCopy((pCodeOp *)immOp));
            poi->fsr0lsrc = npc;
            poi->fsr0hassign = poi->fsr0lassign = npct;
        }

        // some times we need to change to MVFF
        if (poi->iswr) // that is an issue
        {
            pCodeOp *kk;
            insertPCodeInstruction(PCI(poi->poincAcc), (pCodeInstruction *)newpCode(POC_MVWF, kk = (pCodeOp *)(newpCodeOpReg(-1))));
            if (flag)
                insertPCodeInstruction(PCI(poi->poincAcc), (pCodeInstruction *)newpCode(POC_MVFW, pCodeOpCopy(PCI(npc)->pcop)));
            else
                insertPCodeInstruction(PCI(poi->poincAcc), (pCodeInstruction *)newpCode(POC_MVFW, pCodeOpCopy(PCI(pc->prev->prev)->pcop)));

            insertPCodeInstruction(PCI(poi->poincAcc), (pCodeInstruction *)newpCode2(POC_MVFF, pCodeOpCopy(kk), pCodeOpCopy((pCodeOp *)&pc_plusw0)));

            removepCode(&(poi->poincAcc)); // the pointer will point to the previous one
        }
        else
        {
            if (flag)
                insertPCodeInstruction(PCI(poi->poincAcc), (pCodeInstruction *)newpCode(POC_MVFW, pCodeOpCopy(PCI(npc)->pcop)));
            else
                insertPCodeInstruction(PCI(poi->poincAcc), (pCodeInstruction *)newpCode(POC_MVFW, pCodeOpCopy(PCI(pc->prev->prev)->pcop)));
            PCI(poi->poincAcc)->pcop = pCodeOpCopy((pCodeOp *)&pc_plusw0);
        }

        // change to chain

        if (PCI(pc->prev->prev)->cline)
            PCI(npc)->cline = PCI(pc->prev->prev)->cline;
        if (PCI(pc->prev->prev)->label)
        {
            PCI(npc)->label = PCI(pc->prev->prev)->label;
            replaceLabelPtrTabInPb(pc->pb, pc->prev->prev, npc);
        }

        pcNext = pc->next->next->next->next->next;

        pcPrev->next = npc;
        npc->prev = pcPrev;

        npct->next = pcNext;
        pcNext->prev = npct;

        count++;
    }

    // some times FSR0LH no need modify, we need to check that
    // if (count>0)
    //{
    //	//options.nopeep = 1;
    //	int prohibit ;
    //	pCodeOp *lSrc=NULL;
    //	pCodeOp *hSrc=NULL;
    //	poincInf *lastpoi=NULL;
    //	for (poi = poincs; poi; poi = poi->next)
    //	{
    //		if (lastpoi == NULL)
    //		{
    //			lSrc = PCI(poi->fsr0lsrc)->pcop;
    //			hSrc = PCI(poi->fsr0hsrc)->pcop;
    //			lastpoi = poi;
    //			continue;
    //		}
    //		else if (!pCodeOpCompare(lSrc, PCI(poi->fsr0lsrc)->pcop) ||
    //			!pCodeOpCompare(hSrc, PCI(poi->fsr0hsrc)->pcop))
    //		{
    //			lSrc = PCI(poi->fsr0lsrc)->pcop;
    //			hSrc = PCI(poi->fsr0hsrc)->pcop;
    //			lastpoi = poi;
    //			continue;
    //		}

    //		prohibit = 0;
    //		for (pc = lastpoi->poincAcc; pc != poi->poincAcc; pc = pc->next)
    //		{
    //			if (!isPCI(pc))
    //				continue;
    //			if (PCI(pc)->label || PCI(pc)->isBranch || PCI(pc)->pcop->type==PO_POINC0 )
    //			{
    //				prohibit = 1;
    //				break;
    //			}
    //		}
    //		if (!prohibit)
    //		{
    //			removepCode(&poi->fsr0lassign);
    //			poi->fsr0lassign = NULL;
    //			removepCode(&poi->fsr0hassign);
    //			poi->fsr0hassign = NULL;
    //		}
    //	}

    //}
    // next is optimize ++ ?
    // the problem is that move from FSR to RAM needs 4 words...
    // but pointer++ needs 3 words
    // so we skip that optimization ... low possibility working one..

    delPoincInfLst(poincs);
    return count;
}

// fsr0ana is for xx[var]
// fsf0ana2 is for xx[const] ... sometimes it just ...
// long story
//
// static int fsr0ana2(pBlock *pb)
//{
//	// step0, if there is POINC, we find the FSR0 source
//	// it can be MVL or MVFW
//	poincInf *poincs = NULL;
//	poincInf *poiTail = NULL;
//	poincInf *poi = NULL;
//
//	pCode *pc;
//	pCode *npc;
//	pCode *npct; // tail
//	pCode *p1c;
//	pCode *pcPrev;
//	pCode *pcNext;
//	pCodeOpImmd *immOp; // there will be a IMM!!
//						//pCodeOpReg *tmpop; // otherwise ... remat fail , it is gpreg
//	reg_info *r;
//	int count = 0;
//
//	if (!pb || options.nopeep || removed_count >= HY08A_options.ob_count || isPointer2Stack(IC_LEFT(ic)))
//		return 0;
//	// first we construct the poinc list
//	for (pc = pb->pcHead; pc; pc = pc->next)
//	{
//		if (isPCI(pc) && (PCI(pc)->op == POC_MVWF || PCI(pc)->op == POC_MVFW)
//			&& PCI(pc)->pcop->type == PO_POINC0)
//		{
//			poi = newPoincInf(pc);
//			if (poincs == NULL)
//			{
//				poincs = poiTail = poi;
//			}
//			else
//			{
//				if (poi->isFollowing)
//					poiTail->hasFollowing = 1;
//				poi->prev = poiTail;
//				poiTail->next = poi;
//				poiTail = poi;
//			}
//
//		}
//	}
//	// after poinc0 searched
//	// its source should be a pointer
//	// we analyze the pointer's behavior in the pBlock
//	// we check the low byte first
//	for (poi = poincs; poi; poi = poi->next)
//	{
//		// for PLUSW0 .. it must be single byte read
//		int flag = 0;
//		if (!poi->fsr0lsrc || poi->hasFollowing)
//			continue;
//		// here we check if it is a small array (<128) access,
//		// and the pointer is not used in the following code
//		// we can optimize to PLUSW0
//
//		// we must make sure the source will be destroyed before next
//		// reference
//		// we do it for GPR_TEMP register only
//		if (PCI(poi->fsr0lsrc)->pcop->type != PO_GPR_TEMP)
//			continue; // we optimize temp only
//		r = PCOR(PCI(poi->fsr0lsrc)->pcop)->r;
//
//		if (chkReferencedBeforeChange(poi->poincAcc, r->rIdx, 0, 0, 0))
//		{
//			continue;
//		}
//
//		// then we find the pointer (temp reg)'s source
//		for (pc = poi->fsr0lsrc; pc; pc = pc->prev)
//		{
//			if (isPCI(pc) && PCI(pc)->outCond == PCC_REGISTER && PCI(pc)->pcop &&
//				pCodeOpCompare(PCI(poi->fsr0lsrc)->pcop, PCI(pc)->pcop))
//			{
//				break;
//			}
//		}
//
//		if (!pc)// this is where low byte assigned or modified
//			continue;
//		// for fixed array case, it is literal + literal
//		if (PCI(pc)->op == POC_MVWF && pc->prev && pc->prev->prev &&
//			isPCI(pc->prev) && isPCI(pc->prev->prev) &&
//			PCI(pc->prev)->op == POC_ADDLW //&&
//										   //PCI(pc->prev->prev)->op == POC_MVFW  // MVFW+ADDL+MVWF
//			)
//		{
//			//			pCodeOp *pcop1 = PCI(pc->prev->prev)->pcop;
//			pCodeOp *pcop2 = PCI(pc->prev)->pcop;
//			pCodeOp *pcop3 = PCI(pc->prev->prev)->pcop;
//			immOp = NULL;
//			if (pcop2->type == PO_LITERAL && (PCI(pc->prev->prev)->op == POC_MVFW))
//			{
//				if (pcop3->type == PO_GPR_TEMP && PCI(pc->prev->prev)->label==NULL)
//				{
//					pCode *pcsrc;
//					for (pcsrc = pc->prev->prev->prev; pcsrc; pcsrc = pcsrc->prev)
//					{
//						if (!isPCI(pcsrc) || PCI(pcsrc)->label)
//						{
//							break;
//						}
//						if (PCI(pcsrc)->op == POC_MVWF && pCodeOpCompare(PCI(pcsrc)->pcop, pcop3))
//						{
//							if (isPCI(pcsrc->prev) && PCI(pcsrc->prev)->op == POC_MVL &&
//								PCI(pcsrc->prev)->pcop->type == PO_IMMEDIATE)
//							{
//								immOp = PCOI(PCI(pcsrc->prev)->pcop);
//								break;
//							}
//							else
//								break;
//						}
//					}
//				}
//				else
//					continue;
//			}
//			//else if (pcop2->type == PO_LITERAL && PCI(pc->prev->prev)->pcop->type == PO_IMMEDIATE)
//			//immOp = PCOI(PCI(pc->prev->prev)->pcop);
//
//		}
//		else
//			continue;
//
//		// size must be less than 128
//
//		if (immOp == NULL || immOp->r->size > 127)
//			continue;
//
//		// then we make sure high byte is as we want, MVL, BTSZ, ADDL, MVWF
//
//		if (!(pc->next && pc->next->next && pc->next->next->next && pc->next->next->next->next &&
//			isPCI(pc->next) && PCI(pc->next)->op == POC_MVL &&
//			isPCI(pc->next->next) && PCI(pc->next->next)->op == POC_BTSZ &&
//			isPCI(pc->next->next->next) && PCI(pc->next->next->next)->op == POC_ADDLW &&
//			isPCI(pc->next->next->next->next) && PCI(pc->next->next->next->next)->op == POC_MVWF
//			))
//			continue;
//		//if (PCI(pc->prev->prev)->label ) // it cannot has label!!
//		//continue;
//
//		// now we are very sure we can replace with PLUSW0 now
//		pcPrev = pc->prev->prev;
//		removepCode(&(poi->fsr0lassign));
//		removepCode(&(poi->fsr0hassign));
//
//		if (PCI(pcPrev)->op != POC_MVFW)
//		{
//			// save the ACC to a new temp
//			npct = npc = newpCode(POC_MVWF, newpCodeOpReg(-1));
//			p1c = newpCode(POC_MVL, pCodeOpCopy((pCodeOp*)immOp)); npct->next = p1c; p1c->prev = npct; npct = p1c; poi->fsr0lsrc = p1c;
//			p1c = newpCode(POC_MVWF, pCodeOpCopy((pCodeOp*)&pc_fsr0l));			npct->next = p1c; p1c->prev = npct; npct = p1c; poi->fsr0lassign = p1c;
//			p1c = newpCode(POC_MVL, pCodeOpCopy(PCI(pc->next)->pcop)); npct->next = p1c; p1c->prev = npct; npct = p1c; poi->fsr0hsrc = p1c;
//			p1c = newpCode(POC_MVWF, pCodeOpCopy((pCodeOp*)&pc_fsr0h));			npct->next = p1c; p1c->prev = npct; npct = p1c; poi->fsr0hassign = p1c;
//			//			p1c = newpCode(POC_MVFW, pCodeOpCopy(PCI(npc)->pcop));			npct->next = p1c; p1c->prev = npct; npct = p1c;
//
//			flag = 1;
//		}
//		else {
//			pcPrev = pcPrev->prev; //.. one more instruction lda ... removed later
//								   // we make another pcode chain
//			npct = npc = newpCode(POC_MVL, pCodeOpCopy((pCodeOp*)immOp));		poi->fsr0lsrc = npc;
//			p1c = newpCode(POC_MVWF, pCodeOpCopy((pCodeOp*)&pc_fsr0l));			npct->next = p1c; p1c->prev = npct; npct = p1c; poi->fsr0lassign = p1c;
//			p1c = newpCode(POC_MVL, pCodeOpCopy(PCI(pc->next)->pcop)); npct->next = p1c; p1c->prev = npct; npct = p1c; poi->fsr0hsrc = p1c;
//			p1c = newpCode(POC_MVWF, pCodeOpCopy((pCodeOp*)&pc_fsr0h));			npct->next = p1c; p1c->prev = npct; npct = p1c; poi->fsr0hassign = p1c;
//			//			p1c = newpCode(POC_MVFW, pCodeOpCopy(PCI(pc->prev->prev)->pcop));	npct->next = p1c; p1c->prev = npct; npct = p1c;
//		}
//
//		// some times we need to change to MVFF
//		if (poi->iswr) // that is an issue
//		{
//			pCodeOp *kk;
//			insertPCodeInstruction(PCI(poi->poincAcc), (pCodeInstruction*)newpCode(POC_MVWF, kk = (pCodeOp*)(newpCodeOpReg(-1))));
//			if (flag)
//				insertPCodeInstruction(PCI(poi->poincAcc), (pCodeInstruction*)newpCode(POC_MVFW, pCodeOpCopy(PCI(npc)->pcop)));
//			else
//				insertPCodeInstruction(PCI(poi->poincAcc), (pCodeInstruction*)newpCode(POC_MVFW, pCodeOpCopy(PCI(pc->prev->prev)->pcop)));
//
//			insertPCodeInstruction(PCI(poi->poincAcc), (pCodeInstruction*)newpCode2(POC_MVFF, pCodeOpCopy(kk), pCodeOpCopy((pCodeOp*)&pc_plusw0)));
//
//			removepCode(&(poi->poincAcc)); // the pointer will point to the previous one
//		}
//		else
//		{
//			if (flag)
//				insertPCodeInstruction(PCI(poi->poincAcc), (pCodeInstruction*)newpCode(POC_MVFW, pCodeOpCopy(PCI(npc)->pcop)));
//			else
//				insertPCodeInstruction(PCI(poi->poincAcc), (pCodeInstruction*)newpCode(POC_MVFW, pCodeOpCopy(PCI(pc->prev->prev)->pcop)));
//			PCI(poi->poincAcc)->pcop = pCodeOpCopy((pCodeOp*)&pc_plusw0);
//		}
//
//		// change to chain
//
//		if (PCI(pc->prev->prev)->cline)
//			PCI(npc)->cline = PCI(pc->prev->prev)->cline;
//		if (PCI(pc->prev->prev)->label)
//			PCI(npc)->label = PCI(pc->prev->prev)->label;
//
//
//		pcNext = pc->next->next->next->next->next;
//
//		pcPrev->next = npc;
//		npc->prev = pcPrev;
//
//		npct->next = pcNext;
//		pcNext->prev = npct;
//
//
//		count++;
//
//
//	}
//
//	// some times FSR0LH no need modify, we need to check that
//	if (count>0)
//	{
//		//options.nopeep = 1;
//		int prohibit;
//		pCodeOp *lSrc = NULL;
//		pCodeOp *hSrc = NULL;
//		poincInf *lastpoi = NULL;
//		for (poi = poincs; poi; poi = poi->next)
//		{
//			if (lastpoi == NULL)
//			{
//				lSrc = PCI(poi->fsr0lsrc)->pcop;
//				hSrc = PCI(poi->fsr0hsrc)->pcop;
//				lastpoi = poi;
//				continue;
//			}
//			else if (!pCodeOpCompare(lSrc, PCI(poi->fsr0lsrc)->pcop) ||
//				!pCodeOpCompare(hSrc, PCI(poi->fsr0hsrc)->pcop))
//			{
//				lSrc = PCI(poi->fsr0lsrc)->pcop;
//				hSrc = PCI(poi->fsr0hsrc)->pcop;
//				lastpoi = poi;
//				continue;
//			}
//
//			prohibit = 0;
//			for (pc = lastpoi->poincAcc; pc != poi->poincAcc; pc = pc->next)
//			{
//				if (!isPCI(pc))
//					continue;
//				if (PCI(pc)->label || PCI(pc)->isBranch || PCI(pc)->pcop->type == PO_POINC0)
//				{
//					prohibit = 1;
//					break;
//				}
//			}
//			if (!prohibit)
//			{
//				removepCode(&poi->fsr0lassign);
//				poi->fsr0lassign = NULL;
//				removepCode(&poi->fsr0hassign);
//				poi->fsr0hassign = NULL;
//			}
//		}
//
//	}
//	// next is optimize ++ ?
//	// the problem is that move from FSR to RAM needs 4 words...
//	// but pointer++ needs 3 words
//	// so we skip that optimization ... low possibility working one..
//
//
//
//
//	delPoincInfLst(poincs);
//	return count;
//}

int getLabelKeyIndexInPb(pBlock *pb, int key)
{
    int i;
    for (i = pb->labelTabSize - 1; i >= 0; i--)
        if (pb->labelIndexTab[i] == key)
            return i;
    fprintf(stderr, "Internal Error, label not in table error %s:%d\n", __FILE__, __LINE__);
    exit(-__LINE__);
    return -1;
}

// advanced STKXX promote
// when call int yy = foo(...)+/- kk;
// followed with return yy
// the assignment to yy can be optimized
// stkxx also used for long shift, we should be careful
static pCode *isTempSrcSTKXXOP(pCode *pc, int id, int rejectId, pCode **pcs) // pcs[0] is add/addc, pcs[1] is mvwf
{
    pCode *pc2 = pc->next;
    // #ifdef _DEBUG
    //	if (removed_count >= 20)
    //		fprintf(stderr, "20\n");
    // #endif
    for (; pc; pc = pc->prev)
    {
        if (!isPCI(pc))
        {

            continue;
        }
        if (PCI(pc)->label || PCI(pc)->op == POC_CALL || PCI(pc)->op == POC_JMP)
        {
            return NULL;
        }

        if ((PCI(pc)->outCond & PCC_REGISTER) && !(PCI(pc)->outCond & PCC_W) && PCI(pc)->pcop && !PCI(pc)->pcop2 &&
            PCI(pc)->pcop->type == PO_GPR_TEMP && PCOR(PCI(pc)->pcop)->r->rIdx == id)
        {

            // then, we search if this has source of STKXX
            for (pc2 = pc->prev; pc2; pc2 = pc2->prev)
            {
                if (!isPCI(pc2))
                    continue;
                if (PCI(pc2)->label || PCI(pc2)->op == POC_CALL || PCI(pc2)->op == POC_JMP || PCI(pc2)->op == POC_TBLR || PCI(pc2)->op == POC_MVLP)
                {
                    return NULL;
                }
                if (PCI(pc2)->op == POC_MVWF && PCI(pc2)->pcop->type == PO_GPR_TEMP && PCOR(PCI(pc2)->pcop)->r->rIdx == id)
                {
                    pCode *pc3 = pc2->prev;
                    if (isPCI(pc3) && PCI(pc3)->op == POC_MVFW && PCI(pc3)->pcop->type == PO_GPR_TEMP && PCOR(PCI(pc3)->pcop)->r->rIdx == rejectId)
                    {
                        // this is very special special case  return foo(x)+KK; or return foo(x) >> kk;
                        pcs[0] = pc2;
                        pcs[1] = pc3;
                        return pc;
                    }
                }
                if (PCI(pc2)->pcop && PCI(pc2)->pcop->type == PO_GPR_TEMP &&
                    (PCOR(PCI(pc2)->pcop)->r->rIdx == id || PCOR(PCI(pc2)->pcop)->r->rIdx == rejectId))
                    return NULL;
                if (PCI(pc2)->pcop && PCI(pc2)->pcop->type == PO_GPR_BIT && PCORB(PCI(pc2)->pcop)->pcor.r && PCORB(PCI(pc2)->pcop)->pcor.r->rIdx == id)
                    return NULL;
                if (PCI(pc2)->pcop2 && PCI(pc2)->pcop2->type == PO_GPR_TEMP && PCOR(PCI(pc2)->pcop2)->r->rIdx == id)
                    return NULL;
            }
            return NULL;
        }
        if (PCI(pc)->pcop && PCI(pc)->pcop->type == PO_GPR_TEMP &&
            (PCOR(PCI(pc)->pcop)->r->rIdx == id || PCOR(PCI(pc)->pcop)->r->rIdx == rejectId))
            return NULL;
        if (PCI(pc)->pcop && PCI(pc)->pcop->type == PO_GPR_BIT && PCORB(PCI(pc)->pcop)->pcor.r && PCORB(PCI(pc)->pcop)->pcor.r->rIdx == id)
            return NULL;
        if (PCI(pc)->pcop2 && PCI(pc)->pcop2->type == PO_GPR_TEMP && PCOR(PCI(pc)->pcop2)->r->rIdx == id)
            return NULL;
    }
    return NULL;
}
/*
static int stkxx2Temp(pBlock* pb)
{
    // we try simple method is ok
    pCode *pc;
    pCode *pc2;
    pCode *pcPre;
    pCode *pcS1;
    pCode *pcs[2];
    int srcID;
    int count = 0;
    if (!pb)
        return 0;
    if (pb->function_calls != NULL) // if no other function calls, we do the optimization
        return 0;
    if (pb->function_exits == NULL || pb->function_exits->next != NULL) // single exit!!
        return 0;

    if (options.nopeep)
        return 0;
    for (pc = pb->pcHead; pc; pc = pc->next)
    {
        if (!isPCI(pc) || PCI(pc)->op != POC_MVWF || PCI(pc)->label ||
            PCI(pc)->pcop->type != PO_GPR_TEMP || PCOR(PCI(pc)->pcop)->r->rIdx >= 1000)
            continue;
        pcPre = findPrevAccSrc(pc);
        // we skip para!!
        if (!pcPre || !isPCI(pcPre) || PCI(pcPre)->op != POC_MVFW || PCI(pcPre)->pcop->type != PO_GPR_TEMP ||
            PCI(pcPre)->label)
            continue;
        pb->stkxxNowTemp = 1; // that is stkxx temp
        // replace all of them
        for (pc2 = pb->pcHead; pc2; pc2 = pc2->next)
        {
            if (pc2 == pcPre) continue;// final change
            if (isPCI(pc2) && PCI(pc2)->pcop && pCodeOpCompare(PCI(pc2)->pcop, PCI(pcPre)->pcop))
                PCI(pc2)->pcop = pCodeOpCopy(PCI(pc)->pcop);
            if (isPCI(pc2) && PCI(pc2)->pcop2 && pCodeOpCompare(PCI(pc2)->pcop2, PCI(pcPre)->pcop))
                PCI(pc2)->pcop2 = pCodeOpCopy(PCI(pc)->pcop);
        }
        PCI(pcPre)->pcop = pCodeOpCopy(PCI(pc)->pcop);


        count++;
        removed_count++;
        if (removed_count >= HY08A_options.ob_count)
        {
            options.nopeep = 1;
            return count;
        }


    }
    return count;

}
*/
static int stkxxPromote2(pBlock *pb)
{
    // we try simple method is ok
    pCode *pc;
    pCode *pc2;
    pCode *pcPre;
    pCode *pcS1;
    pCode *pcs[2];
    int srcID;
    int count = 0;
    if (options.nopeep)
        return 0;
    if (!pb)
        return 0;
    for (pc = pb->pcHead; pc; pc = pc->next)
    {
        if (!isPCI(pc) || PCI(pc)->op != POC_MVWF || PCI(pc)->label ||
            PCI(pc)->pcop->type != PO_GPR_TEMP || PCOR(PCI(pc)->pcop)->r->rIdx >= 1000)
            continue;
        pcPre = findPrevAccSrc(pc);
        // we skip para!!
        if (!pcPre || !isPCI(pcPre) || PCI(pcPre)->op != POC_MVFW || PCI(pcPre)->pcop->type != PO_GPR_TEMP ||
            PCI(pcPre)->label)
            continue;
        srcID = PCOR(PCI(pcPre)->pcop)->r->rIdx;

        for (pc2 = pc; pc2; pc2 = pc2->next)
        {
            if (!isPCI(pc2))
                continue;

            // if it is jmp, we make sure it is RET, then it is OK to optimize
            if (PCI(pc2)->op == POC_RET)
            {
                pc2 = NULL;
                break; // it is ok
            }
            if (PCI(pc2)->op == POC_JMP)
            {
                pc2 = findLabelinpBlock(pc2->pb, PCOLAB(PCI(pc2)->pcop));
                if (pc2 == NULL)
                    break;
                continue;
            }
            if ((PCI(pc2)->isSkip && isPCI(pc2->next) && PCI(pc2->next)->op == POC_JMP))
                break; // keep pc2
            if (PCI(pc2)->pcop && PCI(pc2)->pcop->type == PO_GPR_TEMP && PCOR(PCI(pc2)->pcop)->r->rIdx == srcID)
            {
                if (PCI(pc2)->inCond & PCC_REGISTER)
                {
                    break;
                }
                if (PCI(pc2)->outCond & PCC_REGISTER)
                {
                    pc2 = NULL; // it os ok
                    break;
                }
            }
        }
        // we make sure src not used in following code
        if (pc2)
            continue;

        // now back to r's source, hope it have no
        if ((pcS1 = isTempSrcSTKXXOP(pcPre->prev, srcID, PCOR(PCI(pc)->pcop)->r->rIdx, pcs)) != NULL)
        {
            PCI(pcS1)->pcop = PCI(pc)->pcop; // replace
            PCI(pcS1)->retOffP1 = PCI(pc)->retOffP1;
            PCI(pcs[0])->pcop = PCI(pc)->pcop;
            PCI(pcs[0])->retOffP1 = PCI(pc)->retOffP1;

            PCI(pcPre)->pcop = PCI(pc)->pcop;
            PCI(pcPre)->retOffP1 = PCI(pc)->retOffP1;
            //			removepCode(&pc);
            //			removepCode(&pcPre);
            count++;
            removed_count++;
            if (removed_count >= HY08A_options.ob_count)
            {
                options.nopeep = 1;
                return count;
            }
        }
    }
    return count;
}

// only strait-forward STKXX is used for optimization
static pCode *isTempSrcSimple(pCode *pc, int id, int rejectId, int allowaddf, int calleepara)
{
    // #ifdef _DEBUG
    //	if (removed_count >= 20)
    //		fprintf(stderr, "20\n");
    // #endif
    // pCode *pc2=pc->next;
    for (; pc; pc = pc->prev)
    {
        if (!isPCI(pc))
        {

            continue;
        }
        // it can have label here
        if (PCI(pc)->op == POC_CALL || PCI(pc)->op == POC_JMP || PCI(pc)->op == POC_MVLP || PCI(pc)->op == POC_TBLR)
        {
            return NULL;
        }
        if (((PCI(pc)->op == POC_MVWF && PCI(pc)->pcop->type == PO_GPR_TEMP && PCOR(PCI(pc)->pcop)->r->rIdx == id) || ((PCI(pc)->op == POC_ADDWF || PCI(pc)->op == POC_SUBFWF || PCI(pc)->op == POC_INF ||
                                                                                                                        PCI(pc)->op == POC_XORF || PCI(pc)->op == POC_IORWF || PCI(pc)->op == POC_RRCF || PCI(pc)->op == POC_COMF) &&
                                                                                                                       allowaddf &&
                                                                                                                       PCI(pc)->pcop->type == PO_GPR_TEMP && PCOR(PCI(pc)->pcop)->r->rIdx == id &&
                                                                                                                       chkNoUseW(pc->next, 0))))
        {
            // check this is not used again

            return pc;
        }
        if (PCI(pc)->label) // if not match but label, give up
            return NULL;
        if (calleepara && ((PCI(pc)->pcop2 && PCI(pc)->pcop2->flags.calleePara) ||
                           ((PCI(pc)->outCond & PCC_REGISTER) && PCI(pc)->pcop && PCI(pc)->pcop->flags.calleePara)))
            return NULL; // no callee para modified between [though not important for 08A]
        if (PCI(pc)->pcop && PCI(pc)->pcop->type == PO_GPR_TEMP &&
            (PCOR(PCI(pc)->pcop)->r->rIdx == id || PCOR(PCI(pc)->pcop)->r->rIdx == rejectId))
            return NULL;
        if (PCI(pc)->pcop && PCI(pc)->pcop->type == PO_GPR_BIT && PCORB(PCI(pc)->pcop)->pcor.r && PCORB(PCI(pc)->pcop)->pcor.r->rIdx == id)
            return NULL;
        if (PCI(pc)->pcop2 && PCI(pc)->pcop2->type == PO_GPR_TEMP && PCOR(PCI(pc)->pcop2)->r->rIdx == id)
            return NULL;
    }
    return NULL;
}

// simple stkxx promote
// we try to see if write to STK0X can be earlier
static int stkxxPromote(pBlock *pb)
{
    // we try simple method is ok
    pCode *pc;
    pCode *pc2;
    pCode *pcPre;
    pCode *pcS1;
    int srcID;
    int count = 0;
    if (options.nopeep)
        return 0;
    if (!pb)
        return 0;
    for (pc = pb->pcHead; pc; pc = pc->next)
    {
        if (!isPCI(pc) || PCI(pc)->label || ((PCI(pc)->op != POC_MVWF || PCI(pc)->pcop->type != PO_GPR_TEMP || PCOR(PCI(pc)->pcop)->r->rIdx >= 1000)))
            continue;
        pcPre = findPrevAccSrc(pc);
        // we skip para!!
        if (!pcPre || !isPCI(pcPre) || PCI(pcPre)->op != POC_MVFW || PCI(pcPre)->pcop->type != PO_GPR_TEMP ||
            PCI(pcPre)->label)
            continue;

        srcID = PCOR(PCI(pcPre)->pcop)->r->rIdx;
        for (pc2 = pc; pc2; pc2 = pc2->next)
        {
            if (!isPCI(pc2))
                continue;

            // if it is jmp, we make sure it is RET, then it is OK to optimize
            if (PCI(pc2)->op == POC_RET || PCI(pc2)->op == POC_CALL) // special functions use STKxx as para
            {
                pc2 = NULL;
                break; // it is ok
            }
            if (PCI(pc2)->op == POC_JMP)
            {
                pc2 = findLabelinpBlock(pc2->pb, PCOLAB(PCI(pc2)->pcop));
                if (pc2 == NULL)
                    break;
                continue;
            }
            // fail when skip + jmp, label is ok, because the flow is fixed
            if ((PCI(pc2)->isSkip && isPCI(pc2->next) && PCI(pc2->next)->op == POC_JMP))
                break; // keep pc2
            if (PCI(pc2)->pcop && PCI(pc2)->pcop->type == PO_GPR_TEMP && PCOR(PCI(pc2)->pcop)->r->rIdx == srcID)
            {
                if (PCI(pc2)->inCond & PCC_REGISTER)
                {
                    break;
                }
                if (PCI(pc2)->outCond & PCC_REGISTER)
                {
                    pc2 = NULL; // it os ok
                    break;
                }
            }
        }
        // we make sure src not used in following code
        if (pc2)
            continue;
        // now back to r's source, hope it have no

        if ((pcS1 = isTempSrcSimple(pcPre->prev, srcID, PCOR(PCI(pc)->pcop)->r->rIdx, 0, 0)) != NULL && !chkReferencedBeforeChange(pc->next, srcID, NULL, 0, 0))
        {
            PCI(pcS1)->pcop = PCI(pc)->pcop; // replace
            PCI(pcS1)->retOffP1 = PCI(pc)->retOffP1;
            removepCode(&pc);
            removepCode(&pcPre);
            count++;
            removed_count++;
            if (removed_count >= HY08A_options.ob_count)
            {
                options.nopeep = 1;
                return count;
            }
        }
    }
    // another kind .. MVFF
    // mvff also optimize function para before call
    for (pc = pb->pcHead; pc; pc = pc->next)
    {
        if (!isPCI(pc) || !pcMVFFLIKE(pc) || PCI(pc)->label ||
            ((PCI(pc)->pcop2->type != PO_GPR_TEMP || PCOR(PCI(pc)->pcop2)->r->rIdx >= 1000)
             // mvff too xxx_STK00 sometimes will destroy the stkxx order, we skip it ?
             && (!PCI(pc)->pcop2->flags.calleePara)))
            continue;

        // we skip para!!
        if (PCI(pc)->pcop->type != PO_GPR_TEMP)
            continue;
        srcID = PCOR(PCI(pc)->pcop)->r->rIdx;
        // now back to r's source, hope it have no
        if ((pcS1 = isTempSrcSimple(pc->prev, srcID, PCOR(PCI(pc)->pcop2)->r->rIdx, 1, PCI(pc)->pcop2->flags.calleePara)) != NULL &&
            !chkReferencedBeforeChange(pc->next, srcID, NULL, 0, 0))
        {
            if (PCI(pcS1)->op == POC_ADDWF) // special case
            {
                pCode *pcn1;
                pCodeReplace(pcS1, pcn1 = newpCode(POC_ADDFW, PCI(pcS1)->pcop));
                insertPCodeInstruction(PCI(pcn1->next), PCI(newpCode(POC_MVWF, PCI(pc)->pcop2)));
                removepCode(&pc); // -2 +1 words !!
            }
            else if (PCI(pcS1)->op == POC_SUBFWF) // special case
            {
                pCode *pcn1;
                pCodeReplace(pcS1, pcn1 = newpCode(POC_SUBFWW, PCI(pcS1)->pcop));
                insertPCodeInstruction(PCI(pcn1->next), PCI(newpCode(POC_MVWF, PCI(pc)->pcop2)));
                removepCode(&pc); // -2 +1 words !!
            }
            else if (PCI(pcS1)->op == POC_XORF) // special case
            {
                pCode *pcn1;
                pCodeReplace(pcS1, pcn1 = newpCode(POC_XORFW, PCI(pcS1)->pcop));
                insertPCodeInstruction(PCI(pcn1->next), PCI(newpCode(POC_MVWF, PCI(pc)->pcop2)));
                removepCode(&pc); // -2 +1 words !!
            }
            else if (PCI(pcS1)->op == POC_IORWF) // special case
            {
                pCode *pcn1;
                pCodeReplace(pcS1, pcn1 = newpCode(POC_IORFW, PCI(pcS1)->pcop));
                insertPCodeInstruction(PCI(pcn1->next), PCI(newpCode(POC_MVWF, PCI(pc)->pcop2)));
                removepCode(&pc); // -2 +1 words !!
            }
            else if (PCI(pcS1)->op == POC_RRCF) // special case
            {
                pCode *pcn1;
                pCodeReplace(pcS1, pcn1 = newpCode(POC_RRCFW, PCI(pcS1)->pcop));
                insertPCodeInstruction(PCI(pcn1->next), PCI(newpCode(POC_MVWF, PCI(pc)->pcop2)));
                removepCode(&pc); // -2 +1 words !!
            }
            else if (PCI(pcS1)->op == POC_INF) // special case
            {
                pCode *pcn1;
                pCodeReplace(pcS1, pcn1 = newpCode(POC_INFW, PCI(pcS1)->pcop));
                insertPCodeInstruction(PCI(pcn1->next), PCI(newpCode(POC_MVWF, PCI(pc)->pcop2)));
                removepCode(&pc); // -2 +1 words !!
            }
            else if (PCI(pcS1)->op == POC_COMF) // special case
            {
                pCode *pcn1;
                pCodeReplace(pcS1, pcn1 = newpCode(POC_COMFW, PCI(pcS1)->pcop));
                insertPCodeInstruction(PCI(pcn1->next), PCI(newpCode(POC_MVWF, PCI(pc)->pcop2)));
                removepCode(&pc); // -2 +1 words !!
            }
            else
            {
                PCI(pcS1)->pcop = PCI(pc)->pcop2;        // replace
                PCI(pcS1)->retOffP1 = PCI(pc)->retOffP1; // return mark [it is possible to be optimized later]
                removepCode(&pc);
            }
            count++;
            removed_count++;
            if (removed_count >= HY08A_options.ob_count)
            {
                options.nopeep = 1;
                return count;
            }
        }
    }
    return count;
}

// we need to find
// MVFW
// XORL
// BTSZ
// JMP
// return value is the XORL
static pCode *matchXORLpattern(pCode *pc, int firstAllowLabel, pCodeOp *matchop)
{
    pCode *pc1, *pc2, *pc3;
    if (!pc || !(pc->next) || !(pc->next->next) || !(pc->next->next->next))
        return NULL;
    pc1 = pc->next;
    pc2 = pc1->next;
    pc3 = pc2->next;
    if (firstAllowLabel == 0 && isPCI(pc) && PCI(pc)->label != NULL)
        return NULL;
    if (isPCI(pc) && PCI(pc)->op == POC_MVFW && ((matchop == NULL) || pCodeOpCompare(matchop, PCI(pc)->pcop)) &&
        isPCI(pc1) && PCI(pc1)->op == POC_XORLW && PCI(pc1)->label == NULL &&
        PCI(pc1)->pcop->type == PO_LITERAL &&
        isPCI(pc2) && PCI(pc2)->op == POC_BTSZ && PCI(pc2)->label == NULL &&
        isPCI(pc3) && PCI(pc3)->op == POC_JMP && PCI(pc3)->label == NULL)
        return pc1;
    return NULL;
}

// for byte level case switch
// the code is
//		MVFW A
//		XORL L1
//		BTSZ status,Z
//		JMP  label
//      MVFW A
//		XORL L2
//		BTSZ status,Z
//		JMP  label2
// can be optimized by remove second MVFW
//
//		MVFW A
//		XORL L1
//		BTSZ status,Z
//		JMP  label
//      ;MVFW A
//		XORL L1^L2
//		BTSZ status,Z
//		JMP  label2
static int xorChainOptimize(pBlock *pb) //  for unsigned-char case-switch
{
    pCode *xorLpos[256];
    pCode *pc, *pc1;
    int count = 0;
    pCode *pp;
    unsigned char xorv;
    char buf[20]; // special case 1, we change to ADD that SUBL can be optimized more
    int i = 0, j, k;
    if (options.nopeep)
        return 0;
    if (!pb)
        return 0;
    for (pc = pb->pcHead; pc; pc = pc->next)
    {
        // RRFW+BTSS STATUS,Z ==> TFSZ
        if (pc && pc->next && pc->next->next && isPCI(pc->next) && isPCI(pc->next->next) &&
            (!isPCI(pc) || !PCI(pc)->isSkip) && PCI(pc->next->next)->label == NULL &&
            PCI(pc->next)->op == POC_RRFW && PCI(pc->next->next)->op == POC_BTSS && (PCI(pc->next->next)->inCond & PCC_Z) && chkNoUseW(pc->next->next, 0))
        {
            // 2016 it is possible use Z more later
            if (!(pc->next->next->next && pc->next->next->next->next &&
                  isPCI(pc->next->next->next->next) && (PCI(pc->next->next->next->next)->inCond & PCC_Z)))
            {
                pp = pc->next->next;
                pCodeReplace(pc->next, newpCode(POC_TFSZ, PCI(pc->next)->pcop));
                removepCode(&pp);
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
            }
        }

        // MVL+CPSE+JMP+MVL
        // final once

        if (pc && pc->next && isPCI(pc) && isPCI(pc->next) &&
            PCI(pc)->op == POC_MVL && PCI(pc)->pcop->type == PO_LITERAL &&
            PCI(pc->next)->op == POC_CPSE && PCI(pc)->label == NULL && PCI(pc->next)->label == NULL)
        {
            // search backword
            int thismatch = 0;
            pc1 = pc;
            do
            {
                pc1 = pc1->prev;
                if (pc1 && isPCI(pc1) && PCI(pc1)->op == POC_MVL && PCI(pc1)->pcop->type == PO_LITERAL &&
                    PCOL(PCI(pc1)->pcop)->lit == PCOL(PCI(pc)->pcop)->lit
                    //&&
                    //!(pc1->prev && pc1->prev->prev && pc1->prev->prev->prev && isPCI(pc1->prev) && isPCI(pc1->prev->prev)
                    //&&
                    // isPCI(pc1->prev->prev->prev) &&
                    //(PCI(pc1->prev)->op == POC_CALL || PCI(pc1->prev->prev)->op==POC_CALL || PCI(pc1->prev->prev->prev)->op==POC_CALL))
                    ) // final can be CALL
                {
                    removepCode(&pc);
                    thismatch = 1;
                    break;
                }
                if (!pc1 || !isPCI(pc1) || PCI(pc1)->label || (PCI(pc1)->outCond & PCC_W))
                    break;
            } while (pc1);

            if (thismatch)
            {
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                continue; // prevent to end, re-check
            }
        }

        if (!isPCI(pc) || PCI(pc)->op != POC_MVFW)
            continue;

        // after above code, first MVFW is found
        // follow

        if ((pc1 = matchXORLpattern(pc, 1, NULL)) == NULL)
            continue;
        i = 0;
        xorLpos[i++] = pc1;
        do
        {
            pc1 = matchXORLpattern(pc1->next->next->next, 0, PCI(pc1->prev)->pcop);
            if (pc1 != NULL)
                xorLpos[i++] = pc1;
        } while (pc1);
        if (i < 2)
            continue;
        // here are 2 more PC1
        // 0 need also change
        xorv = -(PCOL(PCI(xorLpos[0])->pcop)->lit); // change it!!
        snprintf(buf, 19, "0x%02X", (xorv) & 0xff);
        PCI(xorLpos[0])->pcop->name = strdup(buf);
        PCOL(PCI(xorLpos[0])->pcop)->lit = xorv;
        pp = newpCode(POC_ADDLW, pCodeOpCopy(PCI(xorLpos[0])->pcop));
        pCodeReplace(xorLpos[0], pp);
        xorLpos[0] = pp;
        for (j = 1; j < i; j++)
        {

            for (k = 0, xorv = 0; k < j; k++)
            {
                xorv = xorv + PCOL(PCI(xorLpos[k])->pcop)->lit;
            }
            xorv = -(PCOL(PCI(xorLpos[j])->pcop)->lit + xorv); // change it!!
            snprintf(buf, 19, "0x%02X", (xorv) & 0xff);
            PCI(xorLpos[j])->pcop->name = strdup(buf);
            PCOL(PCI(xorLpos[j])->pcop)->lit = xorv;
            // also change the "name"

            unlinkpCode(xorLpos[j]->prev);
            pp = newpCode(POC_ADDLW, pCodeOpCopy(PCI(xorLpos[j])->pcop));
            pCodeReplace(xorLpos[j], pp);
            xorLpos[j] = pp;
        }
        count++;
        removed_count++;
        if (removed_count >= HY08A_options.ob_count)
        {
            options.nopeep = 1;
            return count;
        }
    }
    return count;
}
#define PCPCOP(x) (PCI(x)->pcop)
// simple optimize to reduce the load of peeph optimization

// next pci means no label, no count head
static inline int hasContNextPCI(pCode *pc, int count)
{
    int i;
    for (i = 0; i < count; i++)
    {
        pc = pc->next;
        if (!pc || !isPCI(pc))
            return 0;
        if (PCI(pc)->label)
            return 0;
    }
    return 1;
}

static inline int isBIT2TEMP(pCode *pc, pCodeOp **thebit, pCodeOp **thetemp, pCode **tailmvwf)
{
    // MVL0
    // BTSZ bit
    // MVL 1
    // MVWF temp
    if (PCI(pc)->op == POC_MVL && hasContNextPCI(pc, 4) &&
        PCPCOP(pc)->type == PO_LITERAL && PCOL(PCPCOP(pc))->lit == 0 &&
        PCI(pc->next)->op == POC_BTSZ &&
        PCI(pc->next->next)->op == POC_MVL && PCPCOP(pc->next->next)->type == PO_LITERAL &&
        PCOL(PCPCOP(pc->next->next))->lit == 1 &&
        PCI(pc->next->next->next)->op == POC_MVWF &&
        PCPCOP(pc->next->next->next)->type == PO_GPR_TEMP)
    {
        *thebit = PCI(pc->next)->pcop;
        *thetemp = PCI(pc->next->next->next)->pcop;
        *tailmvwf = pc->next->next->next;
        return 1;
    }

    return 0;
}

static inline int isBIT2TEMPinv(pCode *pc, pCodeOp **thebit, pCodeOp **thetemp, pCode **tailmvwf)
{
    // MVL0
    // BTSZ bit
    // MVL 1
    // MVWF temp
    if (PCI(pc)->op == POC_MVL && hasContNextPCI(pc, 4) &&
        PCPCOP(pc)->type == PO_LITERAL && PCOL(PCPCOP(pc))->lit == 1 &&
        PCI(pc->next)->op == POC_BTSZ &&
        PCI(pc->next->next)->op == POC_MVL && PCPCOP(pc->next->next)->type == PO_LITERAL &&
        PCOL(PCPCOP(pc->next->next))->lit == 0 &&
        PCI(pc->next->next->next)->op == POC_MVWF &&
        PCPCOP(pc->next->next->next)->type == PO_GPR_TEMP)
    {
        *thebit = PCI(pc->next)->pcop;
        *thetemp = PCI(pc->next->next->next)->pcop;
        *tailmvwf = pc->next->next->next;
        return 1;
    }

    return 0;
}


static inline int isBIT2TEMPCOMF(pCode *pc, pCodeOp **thebit, pCodeOp **thetemp, pCode **tailmvwf)
{
    if (PCI(pc)->op == POC_MVL && hasContNextPCI(pc, 4) &&
        PCPCOP(pc)->type == PO_LITERAL && PCOL(PCPCOP(pc))->lit == 0 &&
        PCI(pc->next)->op == POC_BTSZ &&
        PCI(pc->next->next)->op == POC_MVL && PCPCOP(pc->next->next)->type == PO_LITERAL &&
        PCOL(PCPCOP(pc->next->next))->lit == 1 &&
         ((PCI(pc->next->next->next)->op == POC_COMFW &&
        PCPCOP(pc->next->next->next)->type == PO_W)||(
         PCI(pc->next->next->next)->op == POC_XORLW &&
        PCPCOP(pc->next->next->next)->type == PO_LITERAL &&
        PCOL(PCPCOP(pc->next->next->next))->lit == 1
        )||(
         PCI(pc->next->next->next)->op == POC_INFW &&
        PCPCOP(pc->next->next->next)->type == PO_W 
        )
        ) &&
        PCI(pc->next->next->next->next)->op == POC_MVWF &&
        PCPCOP(pc->next->next->next->next)->type == PO_GPR_TEMP
        
        ) 
    {
        *thebit = PCI(pc->next)->pcop;
        *thetemp = PCI(pc->next->next->next->next)->pcop;
        *tailmvwf = pc->next->next->next->next;
        return 1;
    }

    return 0;
}
// // if bit2c by rrfc

static inline int isBIT2CShort(pCode *pc, pCodeOp **thebit, pCode **tailbcf, HYA_OPCODE *btxx)
{
    // BSF C
    // BTSZ bit
    // BSF C
    if (PCI(pc)->op == POC_BSF && hasContNextPCI(pc, 2))
        if ((PCI(pc->next)->op == POC_BTSZ || PCI(pc->next)->op == POC_BTSS) &&
            PCI(pc->next->next)->op == POC_BCF &&
            (PCI(pc)->outCond & PCC_C) &&
            (PCI(pc->next->next)->outCond & PCC_C))
        {
            *thebit = PCI(pc->next)->pcop;
            *tailbcf = pc->next->next;
            *btxx = PCI(pc->next)->op;
            return 1;
        }
    return 0;
}

static inline int isBIT2C(pCode *pc, pCodeOp **thebit, pCode **tailrrfc)
{
    // MVL 0/0xff
    // BTSZ bit
    // MVL 1/0xfe
    // xorl+rrfc or rrfc directly
    if (PCI(pc)->op == POC_MVL && hasContNextPCI(pc, 4) &&
        PCPCOP(pc)->type == PO_LITERAL &&
        PCI(pc->next)->op == POC_BTSZ &&
        PCI(pc->next->next)->op == POC_MVL && PCPCOP(pc->next->next)->type == PO_LITERAL &&
        ((PCI(pc->next->next->next)->op == POC_RRCFW && PCI(pc->next->next->next)->pcop->type == PO_W) ||
         (PCI(pc->next->next->next->next)->op == POC_RRCFW && PCI(pc->next->next->next->next)->pcop->type == PO_W)) &&
        (PCOL(PCI(pc)->pcop)->lit ^ PCOL(PCI(pc->next->next)->pcop)->lit) == 1)
    {
        *thebit = PCI(pc->next)->pcop;
        if (PCI(pc->next->next->next)->op == POC_RRCFW)
            *tailrrfc = pc->next->next->next;
        else
            *tailrrfc = pc->next->next->next->next;
        // fprintf(stderr,"------------bit2c found -------------------\n");
        return 1;
    }
    // CLRF BTSZ INF RRFCW
    if (PCI(pc)->op == POC_CLRF && hasContNextPCI(pc, 4) &&
        PCI(pc->next)->op == POC_BTSZ && PCI(pc->next->next)->op == POC_INF &&
        (PCI(pc->next->next->next)->op == POC_RRCFW || PCI(pc->next->next->next)->op == POC_RRCF) &&
        PCI(pc)->pcop->type == PO_GPR_TEMP &&
        pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next->next)->pcop) &&
        !chkReferencedBeforeChange(pc->next->next->next->next, PCOR(PCI(pc)->pcop)->r->rIdx, NULL, 0, 0))
    {
        *thebit = PCI(pc->next)->pcop;
        *tailrrfc = pc->next->next->next;
        return 1;
    }

    return 0;
}

// z to W

static inline int isZ2W(pCode *pc, pCode **tail)
{
    if (pc && isPCI(pc) && hasContNextPCI(pc, 3) && PCI(pc)->op == POC_MVL &&
        PCPCOP(pc)->type == PO_LITERAL &&
        PCI(pc->next)->op == POC_BTSZ && (PCI(pc->next)->inCond & PCC_Z) &&
        PCI(pc->next->next)->op == POC_MVL && PCPCOP(pc->next->next)->type == PO_LITERAL &&
        ((PCOL(PCI(pc)->pcop)->lit == 0 &&
          PCOL(PCI(pc->next->next)->pcop)->lit == 1))

    )
    {
        *tail = pc->next->next->next;
        return 1;
    }
    return 0;
}

static inline int isZ2WINV(pCode *pc, pCode **tail)
{
    if (pc && isPCI(pc) && hasContNextPCI(pc, 3) && PCI(pc)->op == POC_MVL &&
        PCI(pc)->pcop->type == PO_LITERAL &&
        PCI(pc->next)->op == POC_BTSZ && (PCI(pc->next)->inCond & PCC_Z) &&
        PCI(pc->next->next)->op == POC_MVL && PCPCOP(pc->next->next)->type == PO_LITERAL &&
        ((PCOL(PCI(pc)->pcop)->lit == 1 &&
          PCOL(PCI(pc->next->next)->pcop)->lit == 0))

    )
    {
        *tail = pc->next->next->next;
        return 1;
    }
    return 0;
}

// tail is the instruction after IORL
static inline int isBIT2Z(pCode *pc, pCodeOp **thebit, pCode **tail)
{
    //    MVL	0x00
    //	BTSZ	_bbb,0
    //	MVL	0x01
    //	IORL	0x00
    //
    if (PCI(pc)->op == POC_MVL && hasContNextPCI(pc, 4) &&
        PCPCOP(pc)->type == PO_LITERAL &&
        PCI(pc->next)->op == POC_BTSZ &&
        PCI(pc->next->next)->op == POC_MVL && PCPCOP(pc->next->next)->type == PO_LITERAL &&
        //        ((PCI(pc->next->next->next)->op == POC_RRCFW && PCI(pc->next->next->next)->pcop->type == PO_W) ||
        //         (PCI(pc->next->next->next->next)->op == POC_RRCFW && PCI(pc->next->next->next->next)->pcop->type == PO_W)) &&
        (((PCI(pc->next->next->next)->op == POC_IORL && PCI(pc->next->next->next)->pcop->type == PO_LITERAL) &&
          PCOL(PCI(pc->next->next->next)->pcop)->lit == 0) ||
         (PCI(pc->next->next->next)->op == POC_RRFW && PCI(pc->next->next->next)->pcop->type == PO_W) // 2021 it comes to rrfw?
         ) &&
        (PCOL(PCI(pc)->pcop)->lit ^ PCOL(PCI(pc->next->next)->pcop)->lit) == 1)
    {
        *thebit = PCI(pc->next)->pcop;
        *tail = pc->next->next->next->next;
        // fprintf(stderr,"------------bit2z found -------------------\n");
        return 1;
    }
    return 0;
}

static inline int isC2BITINV(pCode *pc, pCodeOp **thebit)
{
    if (PCI(pc)->op == POC_BTSS && hasContNextPCI(pc, 3) &&
        (PCI(pc)->inCond & PCC_C) &&
        (PCI(pc->next)->op == POC_BCF) &&
        PCI(pc->next->next)->op == POC_BTSZ &&
        (PCI(pc->next->next)->inCond & PCC_C) &&
        PCI(pc->next->next->next)->op == POC_BSF &&
        pCodeOpCompare(PCI(pc->next)->pcop, PCI(pc->next->next->next)->pcop))
    {
        *thebit = PCI(pc->next)->pcop;
        return 1;
    }
    return 0;
}

static inline int isZ2BIT(pCode *pc, pCodeOp **thebit)
{

    //	BTSS	_STATUS
    //  BCF	_bbb,2
    //  BTSZ	_STATUS
    //  BSF	_bbb,2

    if (PCI(pc)->op == POC_BTSS && hasContNextPCI(pc, 3) &&
        (PCI(pc)->inCond & PCC_Z) &&
        (PCI(pc->next)->op == POC_BCF) &&
        PCI(pc->next->next)->op == POC_BTSZ &&
        (PCI(pc->next->next)->inCond & PCC_Z) &&
        PCI(pc->next->next->next)->op == POC_BSF &&
        pCodeOpCompare(PCI(pc->next)->pcop, PCI(pc->next->next->next)->pcop))
    {
        *thebit = PCI(pc->next)->pcop;
        return 1;
    }
    return 0;
}

// static inline int isZ2C(pCode *pc)
// {
//     /*
//     MVL	0x01
// 	BTSZ	_STATUS,0
// 	MVL	0x00
// 	RRFC	_WREG,0,0
//     */

//     if (PCI(pc)->op == POC_MVL && hasContNextPCI(pc, 3) &&
//         (PCI(pc->next)->inCond & PCC_Z) &&
//         (PCI(pc->next)->op == POC_BTSZ || PCI(pc->next)->op == POC_BTSS) &&
//         PCI(pc->next->next)->op == POC_MVL &&
//         PCI(pc->next->next->next)->op == POC_RRCFW &&
//         PCI(pc->next->next->next)->pcop->type == PO_W &&
//         PCOL(PCI(pc)->pcop)->lit==1 &&
//         PCOL(PCI(pc->next->next)->pcop)->lit==0
//         )
//     {
//         return 1;
//     }
//     return 0;
// }

static inline int isTEMP2BIT(pCode *pc, pCodeOp **thebit, pCodeOp **thetemp, pCode **tailmvwf)
{
    /*RRFC	r0x1001,0,0
    BTSS	_STATUS,4
    BCF	_bbb,0
    BTSZ	_STATUS,4
    BSF	_bbb,0
*/
    if (PCI(pc)->op == POC_RRCFW && hasContNextPCI(pc, 4) &&
        PCI(pc->next)->op == POC_BTSS &&
        PCI(pc->next->next)->op == POC_BCF &&
        PCI(pc->next->next->next)->op == POC_BTSZ &&
        PCI(pc->next->next->next->next)->op == POC_BSF &&
        // PCPCOP(pc->next)->type == PO_GPR_BIT &&
        PCPCOP(pc->next->next)->type == PO_GPR_BIT &&
        (PCI(pc->next)->inCond & PCC_C) &&
        (PCI(pc->next->next->next)->inCond & PCC_C)
        // PCORB(PCPCOP(pc->next))->bit == 4 &&
        // PCORB(PCPCOP(pc->next->next->next))->bit == 4 &&
        // PCORB(PCPCOP(pc->next))->pcor.r &&
        // PCORB(PCPCOP(pc->next))->pcor.r->rIdx == IDX_STATUS
    )
    {
        *thebit = PCI(pc->next->next)->pcop;
        *thetemp = PCI(pc)->pcop;
        *tailmvwf = pc->next->next->next->next;
        return 1;
    }
    return 0;
}

static inline int isTEMPBIT2BIT(pCode *pc, pCodeOp **dest, pCodeOp **src, pCode **tail)
{
    if (PCI(pc)->op == POC_BTSS && hasContNextPCI(pc, 4) &&
        PCI(pc->next)->op == POC_BCF &&
        PCI(pc->next->next)->op == POC_BTSZ &&
        PCI(pc->next->next->next)->op == POC_BSF )
    {
        //fprintf(stderr,"b2b\n?");

        
        if( // possible MVFW x btss w.0 bcf a btsz x.0 bsf a
        ((pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next->next)->pcop) ||
        (pc->prev && isPCI(pc->prev) && PCI(pc->prev)->op==POC_MVWF && 
        PCI(pc)->pcop->type == PO_GPR_BIT && PCORB(PCI(pc)->pcop)->subtype==PO_W &&
        PCI(pc->prev)->pcop->type == PO_GPR_TEMP && PCOR(PCI(pc->prev)->pcop)->r->rIdx
        == PCORB(PCI(pc->next->next)->pcop)->pcor.r->rIdx
        )
        )
        )
        &&
        pCodeOpCompare(PCI(pc->next)->pcop, PCI(pc->next->next->next)->pcop) &&
        
        PCORB(PCI(pc->next->next)->pcop)->pcor.r &&
        PCORB(PCI(pc->next->next)->pcop)->pcor.rIdx>0x1000

        )
        {
            *dest = PCI(pc->next)->pcop;
            *src = PCI(pc->next->next)->pcop;
            *tail = pc->next->next->next; //bsf
            return 1;
        }
    }
    return 0;
}

static inline int isW2BIT(pCode *pc, pCodeOp **thebit, pCode **tailpc)
{
    /*
    RRFC     _WREG
    BTSS	_STATUS,4
    BCF	_bbb,0
    BTSZ	_STATUS,4
    BSF	_bbb,0
*/
    if (isPCI(pc) && PCI(pc)->op == POC_RRCFW &&
        PCI(pc)->pcop->type == PO_W &&
        hasContNextPCI(pc, 4) &&
        PCI(pc->next)->op == POC_BTSS &&
        PCI(pc->next->next)->op == POC_BCF &&
        PCI(pc->next->next->next)->op == POC_BTSZ &&
        PCI(pc->next->next->next->next)->op == POC_BSF &&
        // PCPCOP(pc->next)->type == PO_GPR_BIT &&
        PCPCOP(pc->next->next)->type == PO_GPR_BIT &&
        (PCI(pc->next)->inCond & PCC_C) &&
        (PCI(pc->next->next->next)->inCond & PCC_C)

        // PCORB(PCPCOP(pc->next))->bit == 4 &&
        // PCORB(PCPCOP(pc->next->next->next))->bit == 4 &&
        // PCORB(PCPCOP(pc->next))->pcor.r &&
        // PCORB(PCPCOP(pc->next))->pcor.r->rIdx == IDX_STATUS
    )
    {
        *thebit = PCI(pc->next->next)->pcop;
        *tailpc = pc->next->next->next->next;
        return 1;
    }
    return 0;
}

#define PCOLIT0(pc) (PCI(pc)->pcop->type == PO_LITERAL && PCOL(PCI(pc)->pcop)->lit == 0)

static pCodeOp *getSTKXXOP(int rIdx)
{
    pCodeOp *pcop;

    DEBUGHY08A_emitcode("; ***", "%s,%d  , rIdx=0x%x", __FUNCTION__, __LINE__, rIdx);

    pcop = Safe_calloc(1, sizeof(pCodeOpReg));

    PCOR(pcop)->rIdx = rIdx;
    PCOR(pcop)->r = typeRegWithIdx(rIdx, REG_STK, 1);
    if (!(PCOR(pcop)->r))
    {
        fprintf(stderr, "%s at %s %d\n", "internal error", __FUNCTION__, __LINE__);
        exit(-1100);
    }
    PCOR(pcop)->r->isFree = 0;
    PCOR(pcop)->r->wasUsed = 1;
    if (PCOR(pcop)->r->type == REG_TMP)
        PCOR(pcop)->r->isFuncLocal = 1; // it should be func local

    pcop->type = PCOR(pcop)->r->pc_type;

    // dbg
    // if (strstr(PCOR(pcop)->r->name, "_input"))
    //{
    //	fprintf(stderr, "special input\n");
    //}

    return pcop;
}
static inline int hasFollowngPCI(pCode *pc, int contpci)
{
    int i = 0;
    do
    {
        pc = pc->next;
        if (!pc || !PCI(pc))
            return 0;
        i++;
    } while (i < contpci);
    return 1;
}

// 2020 MAY when covid 19 seems to be over soon...
static int dirShiftOptimize(pBlock *pb)
{
    // in some ADC operation,  adcarr[2]=adcarr[1]>>3
    // is not good to be optimized directly because pointer set is at final steps
    pCode *pc;
    pCode *pc1;
    int count = 0;
    if (options.nopeep)
        return 0;

    for (pc = pb->pcHead->next; pc; pc = pc->next)
    {
        int replaceable = -1;

        if (isPCI(pc) && PCI(pc)->pcop && PCI(pc)->pcop->type == PO_IMMEDIATE &&
            PCI(pc)->op == POC_MVWF && PCI(pc)->label == NULL && pc->prev &&
            isPCI(pc->prev) && PCI(pc->prev)->label == NULL &&
            PCI(pc->prev)->op == POC_MVFW &&
            PCI(pc->prev)->pcop->type == PO_GPR_TEMP && !(PCOI(PCI(pc)->pcop)->r && PCOI(PCI(pc)->pcop)->r->isVolatile))
        {
            pCodeOp *pcopDir = PCI(pc)->pcop;
            pCodeOp *pcopTemp = PCI(pc->prev)->pcop;
            pCode *tempPCArray[5]; // >> ..3 only
            int shiftOpCount = 0;

            // first, make sure temp not reference after this instruction
            for (pc1 = pc->next; pc1; pc1 = pc1->next)
            {
                if (isPCI(pc1) && (PCI(pc1)->inCond & PCC_REGISTER) && pCodeOpCompare(pcopTemp, PCI(pc1)->pcop))
                {
                    replaceable = 0;
                    break;
                }
            }
            if (!replaceable)
                continue;
            for (pc1 = pc->prev->prev; pc1; pc1 = pc1->prev)
            {
                // direct flow
                if (!isPCI(pc1)) // don't care it
                    continue;
                if (PCI(pc1)->label || PCI(pc1)->op == POC_CALL || isPCI_BRANCH(pc1))
                {
                    replaceable = 0;
                    break;
                }
                // destimation must not appear here
                if (((PCI(pc1)->inCond & PCC_REGISTER) || (PCI(pc1)->outCond & PCC_REGISTER)))
                {
                    if (pCodeOpCompare(pcopDir, PCI(pc1)->pcop))
                    {
                        replaceable = 0;
                        break;
                    }
                    if (pCodeOpCompare(pcopTemp, PCI(pc1)->pcop))
                    {
                        if (PCI(pc1)->op == POC_MVWF)
                        {
                            replaceable = 1; // it should be the source
                            break;
                        }
                        if (PCI(pc1)->op == POC_RRCF || PCI(pc1)->op == POC_RLCF || PCI(pc1)->op == POC_ARRCF)
                        {
                            if (shiftOpCount > 3)
                            {
                                replaceable = 0;
                                break;
                            }
                            tempPCArray[shiftOpCount++] = pc1;
                            continue;
                        }
                        replaceable = 0; // OTHER op code not support yet
                        break;
                    }
                }
            }
            if (replaceable == 1 && shiftOpCount)
            {
                // it should meet our expectation
                int i;
                PCI(pc1)->pcop = pcopDir;
                for (i = 0; i < shiftOpCount; i++)
                    PCI(tempPCArray[i])->pcop = pcopDir;
                PCI(pc->prev)->pcop = pcopDir;

                //
                count++;
                if (++removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
            }
        }
    }

    return count;
}

static int ifTempFormerlyZero(pCode *pc, int tempid)
{
    // search backward
    do
    {
        if (!isPCI(pc) || PCI(pc)->label) // no label for loop
            return 0;
        if ((PCI(pc)->outCond | PCC_REGISTER))
        {
            if (PCI(pc)->pcop2)
            {
                if (PCI(pc)->pcop2->type == PO_GPR_TEMP &&
                    PCOR(PCI(pc)->pcop2)->r->rIdx == tempid)
                    return 0;
            }
            else if (PCI(pc)->pcop)
            {
                if (PCI(pc)->pcop->type == PO_GPR_TEMP &&
                    PCOR(PCI(pc)->pcop)->r->rIdx == tempid)
                {
                    if (PCI(pc)->op == POC_CLRF &&
                        (pc->prev == NULL || !isPCI(pc->prev) || !PCI(pc->prev)->isSkip))
                        return 1;
                    return 0;
                }
            }
        }
    } while (pc = pc->prev);
    return 0;
}

// static  bool pcopVolatile(pCodeOp *pcop)
// {
//     switch (pcop->type)
//     {
//     case PO_NONE: return false;
//     case PO_STR:
//         if(PCOR(pcop)->r && PCOR(pcop)->r->isVolatile)
//             return true;
//         return false;

//     case PO_W: return false;
//     case PO_STATUS:
//     case PO_FSR:
//     case PO_FSR0H:
//     case PO_FSR0L:
//     case PO_INDF:
//     case PO_POINC:
//     case PO_SSPBUF:
//     case PO_POINC0:
//     case PO_PRINC0:
//     case PO_PODEC0:
//     case PO_PLUSW:

//     case PO_PRINC2:
//     case PO_PODEC2:

//     case PO_INTE0:
//     return false;

//     case PO_GPR_REGISTER:
//         if(PCOR(pcop)->r && PCOR(pcop)->r->isVolatile)
//             return true;
//         return false;
//     case PO_GPR_TEMP: return false;
//     case PO_GPR_POINTER: return false;
//     case PO_SFR_REGISTER: return false; // it is ok checked
//     case PO_PCL:
//     case PO_PCLATH:
//     case PO_PCLATU:
//     case PO_ADCRB:
//     case PO_ADCO1B:
//     case PO_ADCO2B:
//         return false;
//     case PO_DIR:
//         //#ifdef _DEBUG
//         //		if (pcop->type == PO_DIR)
//         //			fprintf(stderr, "PO_DIR COPY\n");
//         //#endif
//         if(PCOR(pcop)->r && PCOR(pcop)->r->isVolatile)
//             return true;
//         return false;

//     case PO_PRODH:
//     case PO_PRODL:
//         return false;
//     case PO_XDATA:
//     case PO_TBLPTRL:
//     case PO_TBLDH:
//     case PO_TBLDL:
//         return false;

//     case PO_LITERAL:
//         return false;

//     case PO_IMMEDIATE:
//         if(PCOI(pcop)->r && PCOI(pcop)->r->isVolatile)
//             return true;
//         return false;

//     case PO_GPR_BIT:
//     case PO_CRY:
//     case PO_BIT:

//         //DFPRINTF((stderr,"pCodeOpCopy bit\n"));
//         //if (removed_count >= 7)
//         //{
//         //	fprintf(stderr, "removed 7\n");
//         //}
//         return false;

//     case PO_LABEL:
//         return false;

//     case PO_WILD:
//         return false;
//     default:
//         return false;
//     } // switch
//     return false;
// }

static char *newImmedOffsetStr(char *old, int inc)
{
    // old is (main_aa + a )
    // new is (main_aa + a + inc)
    char buf0b[512];
    strncpy(buf0b, old, 511);
    char *buf0 = &buf0b[0];
    char *plusp = strchr(buf0, '+');
    char buf[1024];
    if (plusp == NULL)
    {
        while (!isalpha(*buf0) && *buf0 != '_')
            buf0++;
        if (isspace(buf0[strlen(buf0) - 1]))
            buf0[strlen(buf0) - 1] = '\0';
        snprintf(buf, 1023, "(%s + %d)", buf0, inc);
    }
    else
    {
        int off = 0;
        if (!sscanf(plusp + 1, " %d", &off))
        {
            fprintf(stderr, "internal error:strange string immed at %s:%d\n", __FILE__, __LINE__);
            exit(-__LINE__);
        }
        *plusp = '\0';
        while (!isalpha(*buf0) && *buf0 != '_')
            buf0++;
        if (isspace(buf0[strlen(buf0) - 1]))
            buf0[strlen(buf0) - 1] = '\0';
        snprintf(buf, 1023, "(%s + %d)", buf0, off + inc);
    }
    return Safe_strdup(buf);
}
static int simpleOptimize(pBlock *pb, int possibleFinal) // save time from peeph
{
    pCode *pc, *pc1, *pc2, *pc3;
    int lit;
    int bitid;
    int count = 0;

    if (options.nopeep)
        return 0;

    for (pc = pb->pcHead; pc; pc = pc->next)
    {
        // // 2024 4.4.4 SWPFW, ANDL INFW ANDL => SWPFW INFW W
        if ((!(pc->prev) || !isPCI(pc->prev) || !(PCI(pc->prev)->isSkip)) 
            && isPCI(pc) && hasContNextPCI(pc, 4) &&
            PCI(pc->next)->label == NULL &&
            PCI(pc->next->next)->label == NULL &&
            PCI(pc->next->next->next)->label == NULL &&
            PCI(pc)->op == POC_SWPFW && PCI(pc->next)->op == POC_ANDLW &&
            PCI(pc->next->next)->op == POC_INFW &&
            PCI(pc->next->next->next)->op == POC_ANDLW &&            
            PCI(pc->next)->pcop->type==PO_LITERAL &&
            PCI(pc->next->next->next)->pcop->type==PO_LITERAL &&
            PCOL(PCI(pc->next)->pcop)->lit == PCOL(PCI(pc->next->next->next)->pcop)->lit &&
            (PCOL(PCI(pc->next)->pcop)->lit & 1) &&
            PCI(pc->next->next)->pcop->type==PO_W )
        {
            pc1=pc->next;
            removepCode(&pc1);
            count++;
            removed_count++;
            if (removed_count >= HY08A_options.ob_count)
            {
                options.nopeep = 1;
                return count;
            }
            continue;
        }
        // 2024 4.4.4 MVFW, ANDL INFW ANDL => INFW ANDL
        if ((!(pc->prev) || !isPCI(pc->prev) || !(PCI(pc->prev)->isSkip)) 
            && isPCI(pc) && hasContNextPCI(pc, 4) &&
            PCI(pc->next)->label == NULL &&
            PCI(pc->next->next)->label == NULL &&
            PCI(pc->next->next->next)->label == NULL &&
            PCI(pc)->op == POC_MVFW && PCI(pc->next)->op == POC_ANDLW &&
            PCI(pc->next->next)->op == POC_INFW &&
            PCI(pc->next->next->next)->op == POC_ANDLW &&            
            PCI(pc->next)->pcop->type==PO_LITERAL &&
            PCI(pc->next->next->next)->pcop->type==PO_LITERAL &&
            PCOL(PCI(pc->next)->pcop)->lit == PCOL(PCI(pc->next->next->next)->pcop)->lit &&
            (PCOL(PCI(pc->next)->pcop)->lit & 1) &&
            PCI(pc->next->next)->pcop->type==PO_W )
        {
            pc1=pc->prev;
            
            pCodeReplace(pc,newpCode(POC_INFW, PCI(pc)->pcop));
            pc=pc1->next->next->next;
            removepCode(&pc);
            removepCode(&pc);
            count++;
            removed_count++;
            if (removed_count >= HY08A_options.ob_count)
            {
                options.nopeep = 1;
                return count;
            }
            continue;
        }
        // 2024 4.4.4 MVWF x INF x=> INFW _W MVWFx
        if ((!(pc->prev) || !isPCI(pc->prev) || !(PCI(pc->prev)->isSkip)) 
            && isPCI(pc) && hasContNextPCI(pc, 2) &&
            PCI(pc)->op == POC_MVWF && PCI(pc->next)->op == POC_INF &&
            PCI(pc->next)->label == NULL &&
            PCI(pc)->pcop->type == PO_GPR_TEMP &&
            pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next)->pcop))
        {
            pc1=pc->prev;
            pCodeReplace(pc1->next, newpCode(POC_INFW, (pCodeOp*)&pc_wreg));
            pCodeReplace(pc1->next->next,newpCode(POC_MVWF, PCI(pc)->pcop));
            pc=pc1;
            count++;
            removed_count++;
            if (removed_count >= HY08A_options.ob_count)
            {
                options.nopeep = 1;
                return count;
            }
            continue;
        }


        // 2024 4.4.4 btgf simple
        if ((!(pc->prev) || !isPCI(pc->prev) || !(PCI(pc->prev)->isSkip)) 
            && isPCI(pc) && hasContNextPCI(pc, 3) &&
            PCI(pc->next)->label == NULL && PCI(pc->next->next)->label == NULL && PCI(pc->next->next->next)->label == NULL && 
            PCI(pc)->op == POC_BTSS && PCI(pc->next)->op == POC_BCF && 
            PCI(pc->next->next)->op == POC_BTSZ && PCI(pc->next->next->next)->op == POC_BSF )
            if(
            pCodeOpCompare(PCI(pc)->pcop ,PCI(pc->next)->pcop) &&
            pCodeOpCompare(PCI(pc->next)->pcop ,PCI(pc->next->next)->pcop) &&
            pCodeOpCompare(PCI(pc->next->next)->pcop ,PCI(pc->next->next->next)->pcop) )
        {
            pc2=pc->next->next->next;
            pc1=pc->prev;
            pCodeReplace(pc,newpCode(POC_BTGF, PCI(pc)->pcop));
            pc=pc1;
            removepCode(&pc2);
            removepCode(&pc2);
            removepCode(&pc2);
            count++;
            removed_count++;
            if (removed_count >= HY08A_options.ob_count)
            {
                options.nopeep = 1;
                return count;
            }
            continue;
        }
        
        // 2024 4.4.4 LDPR add back
        if ((!(pc->prev) || !isPCI(pc->prev) || !(PCI(pc->prev)->isSkip)) && isPCI(pc) && hasContNextPCI(pc, 2) &&
            PCI(pc)->op == POC_LDPR && !((PCI(pc)->pcop->type == PO_IMMEDIATE && PCOI(PCI(pc)->pcop)->_inX) || (PCI(pc)->pcop->type == PO_DIR && PCOR(PCI(pc)->pcop)->r->isInX == 1)))
        {
            if (((PCI(pc->next)->pcop &&
                      ((PCI(pc->next)->pcop->type == PO_INDF)) ||
                  ((pc->next->next && PCI(pc->next->next)->pcop &&
                    ((PCI(pc->next->next)->pcop->type == PO_INDF))))) ||
                 ((PCI(pc->next)->pcop->type == PO_GPR_BIT &&
                   PCORB(PCI(pc->next)->pcop)->pcor.r &&
                   PCORB(PCI(pc->next)->pcop)->pcor.r->pc_type == PO_INDF) ||
                  (pc->next->next && isPCI(pc->next->next) && PCI(pc->next->next)->pcop &&
                   PCI(pc->next->next)->pcop->type == PO_GPR_BIT &&
                   PCORB(PCI(pc->next->next)->pcop)->pcor.r &&
                   PCORB(PCI(pc->next->next)->pcop)->pcor.r->pc_type == PO_INDF))))
            {
                for (pc1 = pc->next; isPCI(pc1); pc1 = pc1->next)
                {
                    if (PCI(pc1)->label || PCI(pc1)->op == POC_CALL || PCI(pc1)->op == POC_LDPR || PCI(pc1)->op == POC_RET || PCI(pc1)->op == POC_RETI ||
                        ((PCI(pc1)->outCond & PCC_REGISTER) && PCI(pc1)->pcop && PCI(pc1)->pcop->type == PO_FSR0L))
                        break;
                    if (PCI(pc1)->pcop && PCI(pc1)->pcop->type == PO_INDF)
                        PCI(pc1)->pcop = PCI(pc)->pcop;
                    if (PCI(pc1)->pcop && PCI(pc1)->pcop->type == PO_GPR_BIT && PCORB(PCI(pc1)->pcop)->pcor.r && PCORB(PCI(pc1)->pcop)->pcor.r->pc_type == PO_INDF)
                    {
                        PCI(pc1)->pcop = newpCodeOpBit(PCI(pc)->pcop->name, PCORB(PCI(pc1)->pcop)->bit,
                                                       PCORB(PCI(pc1)->pcop)->inBitSpace);
                        if (PCI(pc)->pcop->type == PO_IMMEDIATE && PCOI(PCI(pc)->pcop)->r)
                            PCORB(PCI(pc1)->pcop)->pcor.r = PCOI(PCI(pc)->pcop)->r;
                    }
                }

                removepCode(&pc);
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                continue;

                // fprintf(stderr, "ldpr!!\n");
            }
            else // POINC case
                if (((PCI(pc->next)->pcop && PCI(pc)->pcop->type == PO_IMMEDIATE &&
                          ((PCI(pc->next)->pcop->type == PO_POINC0)) ||
                      ((pc->next->next && PCI(pc->next->next)->pcop &&
                        ((PCI(pc->next->next)->pcop->type == PO_POINC0))))) ||
                     ((PCI(pc->next)->pcop->type == PO_GPR_BIT &&
                       PCORB(PCI(pc->next)->pcop)->pcor.r &&
                       PCORB(PCI(pc->next)->pcop)->pcor.r->pc_type == PO_POINC0) ||
                      (pc->next->next && isPCI(pc->next->next) && PCI(pc->next->next)->pcop &&
                       PCI(pc->next->next)->pcop->type == PO_GPR_BIT &&
                       PCORB(PCI(pc->next->next)->pcop)->pcor.r &&
                       PCORB(PCI(pc->next->next)->pcop)->pcor.r->pc_type == PO_POINC0))))
                {
                    int i = 0;
                    for (pc1 = pc->next; isPCI(pc1); pc1 = pc1->next)
                    {
                        if (PCI(pc1)->label || PCI(pc1)->op == POC_CALL || PCI(pc1)->op == POC_LDPR || PCI(pc1)->op == POC_RET || PCI(pc1)->op == POC_RETI ||
                            ((PCI(pc1)->outCond & PCC_REGISTER) && PCI(pc1)->pcop && PCI(pc1)->pcop->type == PO_FSR0L))
                            break;
                        if (PCI(pc1)->pcop && PCI(pc1)->pcop->type == PO_INDF)
                        {
                            PCI(pc1)->pcop = pCodeOpCopy(PCI(pc)->pcop);
                            PCOI(PCI(pc1)->pcop)->index += i;
                            if (PCI(pc1)->pcop->str && strlen(PCI(pc1)->pcop->str))
                            {
                                PCI(pc1)->pcop->str = newImmedOffsetStr(PCI(pc1)->pcop->str, i);
                                PCI(pc1)->pcop->strHash = hash(PCI(pc1)->pcop->str);
                            }
                        }
                        if (PCI(pc1)->pcop && PCI(pc1)->pcop->type == PO_POINC0)
                        {
                            PCI(pc1)->pcop = pCodeOpCopy(PCI(pc)->pcop);
                            PCOI(PCI(pc1)->pcop)->index += i;
                            if (PCI(pc1)->pcop->str && strlen(PCI(pc1)->pcop->str))
                            {
                                PCI(pc1)->pcop->str = newImmedOffsetStr(PCI(pc1)->pcop->str, i);
                                PCI(pc1)->pcop->strHash = hash(PCI(pc1)->pcop->str);
                            }
                            i++;
                        }

                        if (PCI(pc1)->pcop && PCI(pc1)->pcop->type == PO_GPR_BIT && PCORB(PCI(pc1)->pcop)->pcor.r && PCORB(PCI(pc1)->pcop)->pcor.r->pc_type == PO_INDF)
                        {
                            PCI(pc1)->pcop = newpCodeOpBitByOp(PCI(pc)->pcop, PCORB(PCI(pc1)->pcop)->bit,
                                                               PCORB(PCI(pc1)->pcop)->inBitSpace);
                            if (PCOI(PCI(pc)->pcop)->r)
                            {
                                PCORB(PCI(pc1)->pcop)->pcor.r = PCOI(PCI(pc)->pcop)->r;
                                PCORB(PCI(pc)->pcop)->pcor.instance += i;
                                if (PCI(pc1)->pcop->str && strlen(PCI(pc1)->pcop->str))
                                {
                                    PCI(pc1)->pcop->str = newImmedOffsetStr(PCI(pc1)->pcop->str, i);
                                    PCI(pc1)->pcop->strHash = hash(PCI(pc1)->pcop->str);
                                }
                            }
                        }
                        if (PCI(pc1)->pcop && PCI(pc1)->pcop->type == PO_GPR_BIT && PCORB(PCI(pc1)->pcop)->pcor.r && PCORB(PCI(pc1)->pcop)->pcor.r->pc_type == PO_POINC0)
                        {
                            PCI(pc1)->pcop = newpCodeOpBitByOp(PCI(pc)->pcop, PCORB(PCI(pc1)->pcop)->bit,
                                                               PCORB(PCI(pc1)->pcop)->inBitSpace);
                            if (PCOI(PCI(pc)->pcop)->r)
                            {
                                PCORB(PCI(pc1)->pcop)->pcor.r = PCOI(PCI(pc)->pcop)->r;
                                PCORB(PCI(pc)->pcop)->pcor.instance += i;
                                if (PCI(pc1)->pcop->str && strlen(PCI(pc1)->pcop->str))
                                {
                                    PCI(pc1)->pcop->str = newImmedOffsetStr(PCI(pc1)->pcop->str, i);
                                    PCI(pc1)->pcop->strHash = hash(PCI(pc1)->pcop->str);
                                }
                                i++;
                            }
                        }
                    }
                    removepCode(&pc);
                    count++;
                    removed_count++;
                    if (removed_count >= HY08A_options.ob_count)
                    {
                        options.nopeep = 1;
                        return count;
                    }
                    continue;
                }
        }
        // 2024 mvl0 btss mvl1 mvwf -> clrf btss incf
        // if ((!(pc->prev) || !isPCI(pc->prev) || !(PCI(pc->prev)->isSkip)) && isPCI(pc) && hasContNextPCI(pc, 5) && PCI(pc)->op == POC_MVL && PCI(pc)->pcop->type == PO_LITERAL && PCOL(PCI(pc)->pcop)->lit == 0 &&
        //     PCI(pc->next)->label == NULL && (PCI(pc->next)->op == POC_BTSS || PCI(pc->next)->op == POC_BTSZ) &&
        //     PCI(pc->next->next)->op == POC_MVL && PCI(pc->next->next)->pcop->type == PO_LITERAL && PCOL(PCI(pc->next->next)->pcop)->lit == 1 &&
        //     PCI(pc->next->next->next)->op == POC_MVWF && chkNoUseW(pc->next->next->next->next,0))
        // {
        //     pc2 = pc->next->next->next;
        //     pc1 = pc->prev;
        //     pCodeReplace(pc, newpCode(POC_CLRF, PCI(pc->next->next->next)->pcop));
        //     pc = pc1->next;
        //     pCodeReplace(pc->next->next,newpCode(POC_INF, PCI(pc->next->next->next)->pcop));
        //     removepCode(&pc2);
        //     count++;
        //     removed_count++;
        //     if (removed_count >= HY08A_options.ob_count)
        //     {
        //         options.nopeep = 1;
        //         return count;
        //     }
        //     continue;
        // }
        // 2024 oct try temp to w
        if ((!(pc->prev) || !isPCI(pc->prev) || !(PCI(pc->prev)->isSkip)) && isPCI(pc) && hasContNextPCI(pc, 3) &&
            PCI(pc)->op == POC_MVWF && PCI(pc)->pcop->type==PO_GPR_TEMP && PCI(pc->next)->op == POC_INF &&
            PCI(pc->next)->label == NULL && PCI(pc->next->next)->label == NULL &&
            PCI(pc->next->next)->op == POC_RRCFW && pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next)->pcop) &&
            pCodeOpCompare(PCI(pc)->pcop,PCI(pc->next->next)->pcop) &&
            findNextInstructionRefTemp(pc->next->next->next,PCOR(PCI(pc)->pcop)->rIdx, 0, NULL,0)==0)
        {
            PCI(pc->next)->pcop = (pCodeOp*)&pc_wreg;
            PCI(pc->next->next)->pcop = (pCodeOp*)&pc_wreg;
            removepCode(&pc);
            count++;
            removed_count++;
            if (removed_count >= HY08A_options.ob_count)
            {
                options.nopeep = 1;
                return count;
            }
            continue;
        }

        if ((!(pc->prev) || !isPCI(pc->prev) || !(PCI(pc->prev)->isSkip)) && isPCI(pc) && hasContNextPCI(pc, 6) &&
            PCI(pc)->op == POC_MVL && PCI(pc)->pcop->type == PO_IMMEDIATE && !PCOI(PCI(pc)->pcop)->_inX &&
            PCI(pc->next)->op == POC_MVWF && PCI(pc->next)->pcop->type == PO_FSR0L &&
            PCI(pc->next->next->next)->op == POC_MVWF && PCI(pc->next->next->next)->pcop->type == PO_FSR0H)
            if (
                PCOI(PCI(pc)->pcop)->offset == 0 &&
                PCOI(PCI(pc->next->next)->pcop)->offset == 1 &&
                PCOI(PCI(pc)->pcop)->r &&
                PCOI(PCI(pc->next->next)->pcop)->r &&
                PCOI(PCI(pc)->pcop)->r == PCOI(PCI(pc->next->next)->pcop)->r // r should be the same!!
            )

            {
                pc2 = pc->next->next->next;
                pCodeOp *imm = PCI(pc)->pcop;
                if (((PCI(pc2->next)->pcop &&
                          ((PCI(pc2->next)->pcop->type == PO_INDF)) ||
                      ((pc2->next->next && PCI(pc2->next->next)->pcop &&
                        ((PCI(pc2->next->next)->pcop->type == PO_INDF))))) ||
                     ((PCI(pc2->next)->pcop->type == PO_GPR_BIT &&
                       PCORB(PCI(pc2->next)->pcop)->pcor.r &&
                       PCORB(PCI(pc2->next)->pcop)->pcor.r->pc_type == PO_INDF) ||
                      (pc2->next->next && isPCI(pc2->next->next) && PCI(pc2->next->next)->pcop &&
                       PCI(pc2->next->next)->pcop->type == PO_GPR_BIT &&
                       PCORB(PCI(pc2->next->next)->pcop)->pcor.r &&
                       PCORB(PCI(pc2->next->next)->pcop)->pcor.r->pc_type == PO_INDF))))
                {
                    for (pc1 = pc2->next; isPCI(pc1); pc1 = pc1->next)
                    {
                        if (PCI(pc1)->label || PCI(pc1)->op == POC_CALL || PCI(pc1)->op == POC_LDPR || PCI(pc1)->op == POC_RET || PCI(pc1)->op == POC_RETI ||
                            ((PCI(pc1)->outCond & PCC_REGISTER) && PCI(pc1)->pcop && PCI(pc1)->pcop->type == PO_FSR0L))
                            break;
                        if (PCI(pc1)->pcop && PCI(pc1)->pcop->type == PO_INDF)
                            PCI(pc1)->pcop = imm;
                        if (PCI(pc1)->pcop && PCI(pc1)->pcop->type == PO_GPR_BIT && PCORB(PCI(pc1)->pcop)->pcor.r && PCORB(PCI(pc1)->pcop)->pcor.r->pc_type == PO_INDF)
                        {
                            PCI(pc1)->pcop = newpCodeOpBit(imm->name, PCORB(PCI(pc1)->pcop)->bit,
                                                           PCORB(PCI(pc1)->pcop)->inBitSpace);
                            if (imm->type == PO_IMMEDIATE && PCOI(imm)->r)
                                PCORB(PCI(pc1)->pcop)->pcor.r = PCOI(imm)->r;
                        }
                    }

                    removepCode(&pc);
                    pc = pc->next;
                    removepCode(&pc);
                    pc = pc->next;
                    removepCode(&pc);
                    pc = pc->next;
                    removepCode(&pc);
                    count++;
                    removed_count++;
                    if (removed_count >= HY08A_options.ob_count)
                    {
                        options.nopeep = 1;
                        return count;
                    }
                    continue;

                    // fprintf(stderr, "ldpr!!\n");
                }
                else // POINC case
                    if (((PCI(pc2->next)->pcop && imm->type == PO_IMMEDIATE &&
                              ((PCI(pc2->next)->pcop->type == PO_POINC0)) ||
                          ((pc2->next->next && PCI(pc2->next->next)->pcop &&
                            ((PCI(pc2->next->next)->pcop->type == PO_POINC0))))) ||
                         ((PCI(pc2->next)->pcop->type == PO_GPR_BIT &&
                           PCORB(PCI(pc2->next)->pcop)->pcor.r &&
                           PCORB(PCI(pc2->next)->pcop)->pcor.r->pc_type == PO_POINC0) ||
                          (pc2->next->next && isPCI(pc2->next->next) && PCI(pc2->next->next)->pcop &&
                           PCI(pc2->next->next)->pcop->type == PO_GPR_BIT &&
                           PCORB(PCI(pc2->next->next)->pcop)->pcor.r &&
                           PCORB(PCI(pc2->next->next)->pcop)->pcor.r->pc_type == PO_POINC0))))
                    {
                        int i = 0;
                        for (pc1 = pc2->next; isPCI(pc1); pc1 = pc1->next)
                        {
                            if (PCI(pc1)->label || PCI(pc1)->op == POC_CALL || PCI(pc1)->op == POC_LDPR || PCI(pc1)->op == POC_RET || PCI(pc1)->op == POC_RETI ||
                                ((PCI(pc1)->outCond & PCC_REGISTER) && PCI(pc1)->pcop && PCI(pc1)->pcop->type == PO_FSR0L))
                                break;
                            if (PCI(pc1)->pcop && PCI(pc1)->pcop->type == PO_INDF)
                            {
                                PCI(pc1)->pcop = pCodeOpCopy(imm);
                                PCOI(PCI(pc1)->pcop)->index += i;
                                if (PCI(pc1)->pcop->str && strlen(PCI(pc1)->pcop->str))
                                {
                                    PCI(pc1)->pcop->str = newImmedOffsetStr(PCI(pc1)->pcop->str, i);
                                    PCI(pc1)->pcop->strHash = hash(PCI(pc1)->pcop->str);
                                }
                            }
                            if (PCI(pc1)->pcop && PCI(pc1)->pcop->type == PO_POINC0)
                            {
                                PCI(pc1)->pcop = pCodeOpCopy(imm);
                                PCOI(PCI(pc1)->pcop)->index += i;
                                if (PCI(pc1)->pcop->str && strlen(PCI(pc1)->pcop->str))
                                {
                                    PCI(pc1)->pcop->str = newImmedOffsetStr(PCI(pc1)->pcop->str, i);
                                    PCI(pc1)->pcop->strHash = hash(PCI(pc1)->pcop->str);
                                }
                                i++;
                            }

                            if (PCI(pc1)->pcop && PCI(pc1)->pcop->type == PO_GPR_BIT && PCORB(PCI(pc1)->pcop)->pcor.r && PCORB(PCI(pc1)->pcop)->pcor.r->pc_type == PO_INDF)
                            {
                                PCI(pc1)->pcop = newpCodeOpBitByOp(imm, PCORB(PCI(pc1)->pcop)->bit,
                                                                   PCORB(PCI(pc1)->pcop)->inBitSpace);
                                if (PCOI(imm)->r)
                                {
                                    PCORB(PCI(pc1)->pcop)->pcor.r = PCOI(imm)->r;
                                    PCORB(imm)->pcor.instance += i;
                                    if (PCI(pc1)->pcop->str && strlen(PCI(pc1)->pcop->str))
                                    {
                                        PCI(pc1)->pcop->str = newImmedOffsetStr(PCI(pc1)->pcop->str, i);
                                        PCI(pc1)->pcop->strHash = hash(PCI(pc1)->pcop->str);
                                    }
                                }
                            }
                            if (PCI(pc1)->pcop && PCI(pc1)->pcop->type == PO_GPR_BIT && PCORB(PCI(pc1)->pcop)->pcor.r && PCORB(PCI(pc1)->pcop)->pcor.r->pc_type == PO_POINC0)
                            {
                                PCI(pc1)->pcop = newpCodeOpBitByOp(imm, PCORB(PCI(pc1)->pcop)->bit,
                                                                   PCORB(PCI(pc1)->pcop)->inBitSpace);
                                if (PCOI(imm)->r)
                                {
                                    PCORB(PCI(pc1)->pcop)->pcor.r = PCOI(imm)->r;
                                    PCORB(imm)->pcor.instance += i;
                                    if (PCI(pc1)->pcop->str && strlen(PCI(pc1)->pcop->str))
                                    {
                                        PCI(pc1)->pcop->str = newImmedOffsetStr(PCI(pc1)->pcop->str, i);
                                        PCI(pc1)->pcop->strHash = hash(PCI(pc1)->pcop->str);
                                    }
                                    i++;
                                }
                            }
                        }
                        removepCode(&pc);
                        pc = pc->next;
                        removepCode(&pc);
                        pc = pc->next;
                        removepCode(&pc);
                        pc = pc->next;
                        removepCode(&pc);
                        count++;
                        removed_count++;
                        if (removed_count >= HY08A_options.ob_count)
                        {
                            options.nopeep = 1;
                            return count;
                        }
                        continue;
                    }
            }

        // change to mvff from STKxx to calleepara
        if (possibleFinal && (!(pc->prev) || !isPCI(pc->prev) || !(PCI(pc->prev)->isSkip)) && isPCI(pc) && hasContNextPCI(pc, 2) &&
            PCI(pc)->op == POC_MVFW && PCI(pc->next)->op == POC_MVWF &&
            PCI(pc)->pcop->type == PO_GPR_TEMP && PCOR(PCI(pc)->pcop)->r->type == REG_STK &&

            PCI(pc->next)->pcop->flags.calleePara && chkNoUseW(pc->next->next, 0))
        {
            // change to MVFF to reduce the number LBSR
            pc1 = pc->next;

            pCodeReplace(pc, newpCode2(POC_MVFF, PCI(pc)->pcop, PCI(pc->next)->pcop));
            removepCode(&pc1);
            pc = pc1;
            count++;
            removed_count++;
            if (removed_count >= HY08A_options.ob_count)
            {
                options.nopeep = 1;
                return count;
            }

            continue;
        }
        // 2024 BTSZ C, ADDL 1, MVWF X ==> CLRF X, ADCWF X
        if ((!(pc->prev) || !isPCI(pc->prev) || !(PCI(pc->prev)->isSkip)) && isPCI(pc) && hasContNextPCI(pc, 3) &&
            PCI(pc)->op == POC_BTSZ && (PCI(pc)->inCond & PCC_C) && PCI(pc->next)->op == POC_ADDLW &&
            PCI(pc->next)->pcop->type == PO_LITERAL && PCOL(PCI(pc->next)->pcop)->lit == 1 &&
            PCI(pc->next->next)->op == POC_MVWF &&
            PCI(pc->next->next)->pcop->type == PO_GPR_TEMP &&
            // we have stack address remapping
            !(isPCI(pc->prev) && PCI(pc->prev)->op == POC_MVL && PCI(pc->prev)->pcop->type != PO_LITERAL) &&
            PCI(pc->next)->label == NULL && PCI(pc->next->next)->label == NULL &&
            chkNoUseW(pc->next->next->next, 0))

        {
            pc1 = newpCode(POC_CLRF, pCodeOpCopy(PCI(pc->next->next)->pcop));
            pc2 = newpCode(POC_ADCWF, pCodeOpCopy(PCI(pc->next->next)->pcop));
            pCodeReplace(pc->next, pc2);
            pCodeReplace(pc, pc1);
            pc2 = pc2->next;
            removepCode(&pc2);
            pc = pc1;
            count++;
            removed_count++;
            if (removed_count >= HY08A_options.ob_count)
            {
                options.nopeep = 1;
                return count;
            }

            continue;
        }

        // 2024 MVWF x, MVL y, ADDWF x ==> ADDLW y MVWF x

        if ((!(pc->prev) || !isPCI(pc->prev) || !(PCI(pc->prev)->isSkip)) && isPCI(pc) && hasContNextPCI(pc, 3) &&
            PCI(pc)->op == POC_MVWF && PCI(pc)->pcop->type == PO_GPR_TEMP && PCI(pc->next)->op == POC_MVL &&
            (PCI(pc->next->next)->op == POC_ADDWF || PCI(pc->next->next)->op == POC_XORF || PCI(pc->next->next)->op == POC_IORWF ||
             PCI(pc->next->next)->op == POC_ANDWF) &&
            pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next->next)->pcop) &&
            PCI(pc->next)->label == NULL && PCI(pc->next->next)->label == NULL &&
            chkNoUseW(pc->next->next->next, 0))
        {
            switch (PCI(pc->next->next)->op)
            {
            case POC_ADDWF:
                pc1 = newpCode(POC_ADDLW, pCodeOpCopy(PCI(pc->next)->pcop));
                break;
            case POC_XORF:
                pc1 = newpCode(POC_XORLW, pCodeOpCopy(PCI(pc->next)->pcop));
                break;
            case POC_IORWF:
                pc1 = newpCode(POC_IORL, pCodeOpCopy(PCI(pc->next)->pcop));
                break;
            case POC_ANDWF:
                pc1 = newpCode(POC_ANDLW, pCodeOpCopy(PCI(pc->next)->pcop));
                break;
            default:
                pc1 = NULL;
            }
            pc2 = newpCode(POC_MVWF, pCodeOpCopy(PCI(pc)->pcop));
            if (pc1 != NULL)
                PCI(pc1)->label = PCI(pc)->label;
            pCodeReplace(pc->next->next, pc2);
            pCodeReplace(pc, pc1);
            pc = pc1->next;
            removepCode(&pc);
            count++;
            removed_count++;
            if (removed_count >= HY08A_options.ob_count)
            {
                options.nopeep = 1;
                return count;
            }

            continue;
        }
        // 2022 CLRF RLFC from _Bit
        if ((!(pc->prev) || !isPCI(pc->prev) || !(PCI(pc->prev)->isSkip)) && isPCI(pc) && hasContNextPCI(pc, 2) &&
            PCI(pc)->op == POC_CLRF && PCI(pc->next)->op == POC_RLCF && PCI(pc)->pcop->type == PO_GPR_TEMP)
        {
            if (((PCI(pc->next->next)->op == POC_TFSZ && pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next->next)->pcop)) ||
                 ((PCI(pc->next->next)->op == POC_BTSS || PCI(pc->next->next)->op == POC_BTSZ) && PCI(pc->next->next)->inCond & PCC_Z)) &&
                pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next)->pcop) &&
                !chkReferencedBeforeChange(pc->next->next->next, PCOR(PCI(pc)->pcop)->r->rIdx, NULL, 0, 0) &&
                !chkReferencedBeforeChange(pc->next->next->next->next, PCOR(PCI(pc)->pcop)->r->rIdx, NULL, 0, 0)) // 2024 bug found, because p->n->n is skip, we need check 2
            {
                pc1 = NULL;
                if (PCI(pc->next->next)->op == POC_BTSZ)
                    pc1 = newpCode(POC_BTSS, STATUSCBIT);
                else
                    pc1 = newpCode(POC_BTSZ, STATUSCBIT);
                pCodeReplace(pc, pc1);
                pc1 = pc1->next;
                pc2 = pc1->next;
                removepCode(&pc1);
                removepCode(&pc2);
                pc = pc1;
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }

                continue;
            }
        }

        // 2022 feb MVL 0 RLCF _WREG MVWF x ==> CLRF x RLCF x
        if ((!(pc->prev) || !isPCI(pc->prev) || !(PCI(pc->prev)->isSkip))
            // following method skip first label
            && isPCI(pc) && hasContNextPCI(pc, 2) &&
            PCI(pc)->op == POC_MVL && PCI(pc)->pcop->type == PO_LITERAL &&
            PCOL(PCI(pc)->pcop)->lit == 0 &&
            (PCI(pc->next)->op == POC_RLCFW || PCI(pc->next)->op == POC_RRCFW ||
             PCI(pc->next)->op == POC_RLCF || PCI(pc->next)->op == POC_RRCF) &&
            PCI(pc->next)->pcop->type == PO_W &&
            PCI(pc->next->next)->op == POC_MVWF && PCI(pc->next->next)->pcop->type == PO_GPR_TEMP &&
            chkNoUseW(pc->next->next->next, 0))
        {
            if (PCI(pc->next)->op == POC_RLCFW)
                pc1 = newpCode(POC_RLCF, PCI(pc->next->next)->pcop);
            else
                pc1 = newpCode(POC_RRCF, PCI(pc->next->next)->pcop);
            pc2 = newpCode(POC_CLRF, PCI(pc->next->next)->pcop);
            pCodeReplace(pc, pc2);
            pCodeReplace(pc2->next, pc1);
            pc = pc1->next;
            removepCode(&pc);
            count++;
            removed_count++;
            if (removed_count >= HY08A_options.ob_count)
            {
                options.nopeep = 1;
                return count;
            }

            continue;
        }

        // 2022 feb
        if ((!(pc->prev) || !isPCI(pc->prev) || !(PCI(pc->prev)->isSkip))
            // following method skip first label
            && isPCI(pc) && hasContNextPCI(pc, 3) &&
            PCI(pc)->op == POC_MVWF && PCI(pc)->pcop->type == PO_GPR_TEMP &&
            (PCI(pc->next)->op == POC_BTSZ || PCI(pc->next)->op == POC_BTSS) &&
            (PCI(pc->next->next)->op == POC_SETF ||
             PCI(pc->next->next)->op == POC_CLRF) &&
            PCI(pc->next->next)->pcop->type != PO_W &&
            PCI(pc->next->next->next)->op == POC_MVFW &&
            !pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next->next)->pcop) &&
            pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next->next->next)->pcop))
        {
            pc1 = pc->next->next->next;
            removepCode(&pc1);
            count++;
            removed_count++;
            if (removed_count >= HY08A_options.ob_count)
            {
                options.nopeep = 1;
                return count;
            }
            continue;
        }

        // 2022 FEB optimize COMF _WREG
        if (isPCI(pc) && (PCI(pc)->op == POC_COMF || PCI(pc)->op == POC_COMFW) &&
            PCI(pc)->pcop->type == PO_W)
        {
            int found = 0;
            // we search back for the source
            for (pc1 = pc->prev; pc1; pc1 = pc1->prev)
            {
                if (isPCI(pc1) && (PCI(pc1)->outCond & PCC_W) && !(isPCI(pc1->prev) && PCI(pc1->prev)->isSkip))
                {
                    found = 1;
                    break;
                }
                if ((isPCI(pc1) && (PCI(pc1)->label ||
                                    (PCI(pc1)->inCond & PCC_W) ||
                                    (PCI(pc1)->inCond & PCC_Z) ||
                                    PCI(pc1)->isSkip)) ||
                    !isPCI(pc1))
                {
                    found = 0;
                    break;
                }
            }
            if (found && PCI(pc1)->op == POC_MVFW)
            {
                pc2 = newpCode(POC_COMFW, PCI(pc1)->pcop);
                pCodeReplace(pc1, pc2);
                removepCode(&pc);
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                continue;
            }
            if (found && PCI(pc1)->op == POC_MVL && PCI(pc1)->pcop->type == PO_LITERAL)
            {
                lit = PCOL(PCI(pc1)->pcop)->lit;
                pc2 = newpCode(POC_MVL, newpCodeOpLit(lit ^ 0xff));
                pCodeReplace(pc1, pc2);
                removepCode(&pc);
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                continue;
            }
        }

        // 2022 MVWF a, bcf c, rrfc a/rlfca/rrfcw a/rlfcw a
        if ((!(pc->prev) || !isPCI(pc->prev) || !(PCI(pc->prev)->isSkip)) &&
            isPCI(pc) &&
            hasContNextPCI(pc, 3) && PCI(pc->next)->label == NULL && PCI(pc->next->next)->label == NULL && PCI(pc)->op == POC_MVWF && PCI(pc)->pcop->type == PO_GPR_TEMP &&
            PCI(pc->next)->op == POC_BCF && (PCI(pc->next)->outCond & PCC_C) &&
            PCI(pc->next->next)->pcop && pCodeOpCompare(PCI(pc->next->next)->pcop, PCI(pc)->pcop) &&
            chkNoUseW(pc->next->next->next, 0))
        {
            pc2 = pc->next->next;
            pc3 = NULL;
            lit = 0;
            switch (PCI(pc2)->op)
            {
            case POC_RLCF:
                pc1 = newpCode(POC_ADDFW, (pCodeOp *)&pc_wreg);
                pc3 = newpCode(POC_MVWF, PCI(pc)->pcop);
                pCodeReplace(pc->next, pc3);
                pCodeReplace(pc, pc1);
                pc = pc3->next;
                removepCode(&pc);

                lit = 1;
                break;
            case POC_RLCFW:
                // change to W
                pCodeReplace(pc2, newpCode(POC_RLCFW, (pCodeOp *)&pc_wreg));
                lit = 1;
                break;
            case POC_RRCFW:
                pCodeReplace(pc2, newpCode(POC_RRCFW, (pCodeOp *)&pc_wreg));
                lit = 1;
                break;
            default:
                break;
            }
            if (lit)
            {
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                continue;
            }
        }

        // 2022 MVFW + RLCFW/RRCFW WREG ==>RLCFW/RRCFW x, volatile free
        if ((!(pc->prev) || !isPCI(pc->prev) || !(PCI(pc->prev)->isSkip)) && isPCI(pc) &&
            hasContNextPCI(pc, 2) && PCI(pc)->op == POC_MVFW &&
            (PCI(pc->next)->op == POC_RLCFW || PCI(pc->next)->op == POC_RRCFW ||
             PCI(pc->next)->op == POC_RLCF || PCI(pc->next)->op == POC_RRCF) &&
            PCI(pc->next)->pcop->type == PO_W &&
            PCI(pc->next)->label == NULL)
        {
            pCode *pcn;
            pCode *pcn1 = pc->next;
            if (PCI(pc->next)->op == POC_RLCFW || PCI(pc->next)->op == POC_RLCF)
                pcn = newpCode(POC_RLCFW, PCI(pc)->pcop);
            else
                pcn = newpCode(POC_RRCFW, PCI(pc)->pcop);
            pCodeReplace(pc, pcn);
            removepCode(&pcn1);
            pc = pcn1;
            count++;
            removed_count++;
            if (removed_count >= HY08A_options.ob_count)
            {
                options.nopeep = 1;
                return count;
            }
            continue;
        }

        // 2022 mvfw andl swpf ==> swpfw andl
        if ((!(pc->prev) || !isPCI(pc->prev) || !(PCI(pc->prev)->isSkip)) && isPCI(pc) &&
            hasContNextPCI(pc, 3) && PCI(pc)->op == POC_MVFW &&
            PCI(pc->next)->op == POC_ANDLW && PCI(pc->next)->pcop->type == PO_LITERAL &&
            (PCI(pc->next->next)->op == POC_SWPF ||
             PCI(pc->next->next)->op == POC_SWPFW) &&
            PCI(pc->next->next)->pcop->type == PO_W &&
            PCI(pc->next)->label == NULL && PCI(pc->next->next)->label == NULL)
        {
            int lit = PCOL(PCI(pc->next)->pcop)->lit;
            int litn = ((lit & 0x0f) << 4) | (lit >> 4);
            pCode *pcn = pc->next;
            pCodeReplace(pc, newpCode(POC_SWPFW, PCI(pc)->pcop));
            pc = pcn;
            pcn = pc->next;
            pCodeReplace(pc, newpCode(POC_ANDLW, newpCodeOpLit(litn)));
            pc = pcn;
            removepCode(&pc);
            count++;
            removed_count++;
            if (removed_count >= HY08A_options.ob_count)
            {
                options.nopeep = 1;
                return count;
            }
            continue;
        }

        // 2021 INF/DECF/COMF after MVFF
        // 2023, MVWF operand must not be volatile!!
        if ((!(pc->prev) || !isPCI(pc->prev) || !(PCI(pc->prev)->isSkip)) && isPCI(pc) &&
            hasContNextPCI(pc, 3) && PCI(pc)->op == POC_MVFW &&
            PCI(pc->next)->op == POC_MVWF && !((PCI(pc->next)->pcop->type == PO_DIR && PCOR(PCI(pc->next)->pcop)->r && PCOR(PCI(pc->next)->pcop)->r->isVolatile) || (PCI(pc->next)->pcop->type == PO_IMMEDIATE && PCOI(PCI(pc->next)->pcop)->r && PCOI(PCI(pc->next)->pcop)->r->isVolatile)) &&
            (PCI(pc->next->next)->op == POC_INF ||
             PCI(pc->next->next)->op == POC_DECF || PCI(pc->next->next)->op == POC_COMF) &&
            //            PCI(pc->next)->label==NULL && PCI(pc->next->next)->label==NULL && // label already counted
            (PCI(pc->next->next)->pcop->type != PO_POINC &&
             PCI(pc->next->next)->pcop->type != PO_POINC0 &&
             PCI(pc->next->next)->pcop->type != PO_PLUSW &&
             PCI(pc->next->next)->pcop->type != PO_SFR_REGISTER &&
             !PCI(pc->next->next)->pcop->flags.parapcop) &&
            pCodeOpCompare(PCI(pc->next)->pcop, PCI(pc->next->next)->pcop) &&
            chkNoUseW(pc->next->next->next, 0))
        {
            pCode *pcn = pc->next;
            switch (PCI(pc->next->next)->op)
            {
            case POC_INF:
                pCodeReplace(pc, newpCode(POC_INFW, PCI(pc)->pcop));
                break;
            case POC_DECF:
                pCodeReplace(pc, newpCode(POC_DECFW, PCI(pc)->pcop));
                break;
            case POC_COMF:
                pCodeReplace(pc, newpCode(POC_COMFW, PCI(pc)->pcop));
                break;
            default: // default do nothing
                break;
            }
            pc = pcn->next;
            removepCode(&pc);
            count++;
            removed_count++;
            if (removed_count >= HY08A_options.ob_count)
            {
                options.nopeep = 1;
                return count;
            }
            continue;
        }

        // first is CLRF
        // CLRF X RLCFW X applied even volatile
        if ((!(pc->prev) || !isPCI(pc->prev) || !(PCI(pc->prev)->isSkip)) && isPCI(pc) &&
            hasContNextPCI(pc, 2) && PCI(pc)->op == POC_CLRF &&
            PCI(pc->next)->op == POC_RLCFW && PCI(pc)->pcop->type == PO_GPR_TEMP &&
            PCI(pc->next->next)->op == POC_MVWF &&
            (PCI(pc->next->next)->pcop->type != PO_POINC &&
             PCI(pc->next->next)->pcop->type != PO_POINC0 &&
             PCI(pc->next->next)->pcop->type != PO_PLUSW &&
             PCI(pc->next->next)->pcop->type != PO_SFR_REGISTER &&
             !PCI(pc->next->next)->pcop->flags.parapcop) &&
            pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next)->pcop) &&
            pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next->next)->pcop) //&&
            //! chkReferencedBeforeChange(pc->next->next,PCOR(PCI(pc)->pcop)->r->rIdx, NULL, 0, 0)
        )
        {

            pc1 = pc->prev;
            pc2 = pc->next->next;
            pCodeReplace(pc->next, newpCode(POC_RLCF, PCI(pc2)->pcop));
            pCodeReplace(pc, newpCode(POC_CLRF, PCI(pc2)->pcop));
            pCodeReplace(pc2, newpCode(POC_MVFW, PCI(pc1->next)->pcop));
            pc = pc1;
            count++;
            removed_count++;
            if (removed_count >= HY08A_options.ob_count)
            {
                options.nopeep = 1;
                return count;
            }
            continue;
        }

        // CLRF itemp; ???;  INF X MVFW X ==> MVL0 kkk mvl 1 , volatile free
        if ((!(pc->prev) || !isPCI(pc->prev) || !(PCI(pc->prev)->isSkip)) && isPCI(pc) &&
            hasContNextPCI(pc, 3) && PCI(pc)->op == POC_CLRF &&
            PCI(pc->next)->isSkip && PCI(pc)->pcop->type == PO_GPR_TEMP &&
            !(PCI(pc->next)->outCond & PCC_W) &&
            PCI(pc->next->next)->op == POC_INF && pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next->next)->pcop))
        {
            if ((PCI(pc->next->next->next)->op == POC_MVFW || PCI(pc->next->next->next)->op == POC_RRFW ||
                 PCI(pc->next->next->next)->op == POC_TFSZ) &&
                pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next->next->next)->pcop) &&
                !chkReferencedBeforeChange(pc->next->next->next->next, PCOR(PCI(pc)->pcop)->r->rIdx, NULL, 0, 0) &&
                !(PCI(pc->next->next->next)->op == POC_TFSZ && chkReferencedBeforeChange(pc->next->next->next->next->next, PCOR(PCI(pc)->pcop)->r->rIdx, NULL, 0, 0)))
            {
                pCode *pc4 = pc->next->next->next;
                pc1 = pc->prev;

                pc3 = pc->next->next;
                pCodeReplace(pc, newpCode(POC_MVL, newpCodeOpLit(0)));
                pCodeReplace(pc3, newpCode(POC_MVL, newpCodeOpLit(1)));
                switch (PCI(pc4)->op)
                {
                case POC_MVFW:
                    removepCode(&pc4);
                    break;
                case POC_RRFW:
                    pCodeReplace(pc4, newpCode(POC_RRFW, PCOP(&pc_wreg)));
                    break;
                case POC_TFSZ:
                    pCodeReplace(pc4, newpCode(POC_TFSZ, PCOP(&pc_wreg)));
                    break;
                default:
                    fprintf(stderr, "Internal Error at %s:%d\n", __FILE__, __LINE__);
                    exit(-__LINE__);
                }
                pc = pc1;
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                continue;
            }
        }

        // special case insuzw a inf b mvwf a=> insuz a inf b, volatile free

        if ((!(pc->prev) || !isPCI(pc->prev) || !(PCI(pc->prev)->isSkip)) && isPCI(pc) &&
            hasContNextPCI(pc, 2) && PCI(pc)->op == POC_INSUZW &&
            PCI(pc->next)->op == POC_INF && PCI(pc->next->next)->op == POC_MVWF &&
            PCI(pc)->pcop->type == PO_GPR_TEMP &&
            pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next->next)->pcop) &&
            chkNoUseW(pc->next->next->next, 0))
        {

            pc1 = pc->prev;
            pc2 = pc->next;
            pc3 = pc->next->next;
            pCodeReplace(pc, newpCode(POC_INSUZ, PCI(pc)->pcop));
            removepCode(&pc3);
            pc = pc1;
            count++;
            removed_count++;
            if (removed_count >= HY08A_options.ob_count)
            {
                options.nopeep = 1;
                return count;
            }
            continue;
        }

        // 2021 INSUZ A INF B MVFW A, and A not used again, volatile free
        // change to INSUZW A INFB
        if ((!(pc->prev) || !isPCI(pc->prev) || !(PCI(pc->prev)->isSkip)) && isPCI(pc) &&
            hasContNextPCI(pc, 2) && PCI(pc)->op == POC_INSUZ &&
            PCI(pc->next)->op == POC_INF && PCI(pc->next->next)->op == POC_MVFW &&
            PCI(pc)->pcop->type == PO_GPR_TEMP &&
            pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next->next)->pcop) &&
            !chkReferencedBeforeChange(pc->next->next->next, PCOR(PCI(pc)->pcop)->r->rIdx, NULL, 0, 0))
        {
            if (!(pc->prev && isPCI(pc->prev) && PCI(pc->prev)->op == POC_MVWF && pCodeOpCompare(PCI(pc->prev)->pcop, PCI(pc->next)->pcop)))
            {
                pc1 = pc->prev;
                pc2 = pc->next->next;
                pCodeReplace(pc, newpCode(POC_INSUZW, PCI(pc)->pcop));
                removepCode(&pc2);
                pc = pc1;
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                continue;
            }
        }

        // 2021 BTSZ C, INF A, MVWF B, MVFW A, A is temp, no more use
        // 2021 sep we check if A formerly z
        // change to MVWF B, MVL 0, ADCFW A, 4 words -> 3 words, volatile free
        if ((!(pc->prev) || !isPCI(pc->prev) || !(PCI(pc->prev)->isSkip)) && isPCI(pc) &&
            hasContNextPCI(pc, 3) && PCI(pc)->op == POC_BTSZ && (PCI(pc)->inCond & PCC_C) &&
            PCI(pc->next)->op == POC_INF && PCI(pc->next->next)->op == POC_MVWF && PCI(pc->next->next->next)->op == POC_MVFW &&
            PCI(pc->next)->pcop->type == PO_GPR_TEMP && pCodeOpCompare(PCI(pc->next)->pcop, PCI(pc->next->next->next)->pcop) &&
            !chkReferencedBeforeChange(pc->next->next->next->next, PCOR(PCI(pc->next)->pcop)->r->rIdx, NULL, 0, 0))
        {
            pCode *pc4 = pc->prev;
            pc1 = pc->next;
            pc2 = pc->next->next;
            pc3 = pc->next->next->next;

            pCodeReplace(pc, newpCode(PCI(pc->next->next)->op, PCI(pc->next->next)->pcop));
            pCodeReplace(pc1, newpCode(POC_MVL, newpCodeOpLit(0)));
            pCodeReplace(pc2, newpCode(POC_ADCFW, PCI(pc3)->pcop));
            removepCode(&pc3);
            pc = pc4;
            count++;
            removed_count++;
            if (removed_count >= HY08A_options.ob_count)
            {
                options.nopeep = 1;
                return count;
            }
            continue;
        }

        // 2021 BTSZ C, INF A,  MVFW A, A is temp, no more use
        // change to MVL 0, ADCFW A, 3 words -> 2 words, volatile free
        if ((!(pc->prev) || !isPCI(pc->prev) || !(PCI(pc->prev)->isSkip)) && isPCI(pc) &&
            hasContNextPCI(pc, 2) && PCI(pc)->op == POC_BTSZ && (PCI(pc)->inCond & PCC_C) &&
            PCI(pc->next)->op == POC_INF && PCI(pc->next->next)->op == POC_MVFW &&
            PCI(pc->next)->pcop->type == PO_GPR_TEMP && pCodeOpCompare(PCI(pc->next)->pcop, PCI(pc->next->next)->pcop) &&
            !chkReferencedBeforeChange(pc->next->next->next, PCOR(PCI(pc->next)->pcop)->r->rIdx, NULL, 0, 0))
        {
            pCode *pc4 = pc->prev;
            pc1 = pc->next;
            pc2 = pc->next->next;
            pCodeReplace(pc, newpCode(POC_MVL, newpCodeOpLit(0)));
            pCodeReplace(pc1, newpCode(POC_ADCFW, PCI(pc1)->pcop));
            removepCode(&pc2);
            pc = pc4;
            count++;
            removed_count++;
            if (removed_count >= HY08A_options.ob_count)
            {
                options.nopeep = 1;
                return count;
            }
            continue;
        }

        // 2021 clrf btxx setf/decf + mvfw
        // for temp, volatile free
        if ((!(pc->prev) || !isPCI(pc->prev) || !(PCI(pc->prev)->isSkip)) && isPCI(pc) &&
            hasContNextPCI(pc, 3) && PCI(pc)->op == POC_CLRF && PCI(pc->next)->isSkip &&
            (PCI(pc->next->next)->op == POC_DECF || PCI(pc->next->next)->op == POC_SETF) &&
            PCI(pc->next->next->next)->op == POC_MVFW &&
            PCI(pc)->pcop->type == PO_GPR_TEMP &&
            pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next->next)->pcop) &&
            pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next->next->next)->pcop) &&
            !chkReferencedBeforeChange(pc->next->next->next->next, PCOR(PCI(pc)->pcop)->r->rIdx, NULL, 0, 0))
        {
            pCode *pc4 = pc->next->next;
            pc1 = pc->next->next->next;
            pc2 = pc->prev;

            // PCI(pc)->pcop = PCOP(&pc_wreg);
            pCodeReplace(pc, newpCode(POC_MVL, newpCodeOpLit(0)));
            pCodeReplace(pc4, newpCode(POC_MVL, newpCodeOpLit(255)));
            // PCI(pc->next->next->next)->pcop = PCOP(&pc_wreg);
            removepCode(&pc1);
            pc = pc2;
            count++;
            removed_count++;
            if (removed_count >= HY08A_options.ob_count)
            {
                options.nopeep = 1;
                return count;
            }
            continue;
        }

        //--------------------------first is MVWF
        // 2021 aug hard-code peep for same op second special
        if ((!(pc->prev) || !isPCI(pc->prev) || !(PCI(pc->prev)->isSkip)) &&
            pc->next && isPCI(pc) && isPCI(pc->next) &&
            PCI(pc)->op == POC_MVWF)
        {
            // MVWF a, bts? a.?, first is temp, volatile free
            if (PCI(pc->next)->label == NULL &&
                (PCI(pc->next)->op == POC_BTSS ||
                 PCI(pc->next)->op == POC_BTSZ) &&
                PCI(pc)->pcop->type == PO_GPR_TEMP)
                if (
                    PCORB(PCI(pc->next)->pcop)->pcor.r &&
                    PCORB(PCI(pc->next)->pcop)->pcor.r->rIdx == PCOR(PCI(pc)->pcop)->r->rIdx)
                {
                    pCodeReplace(pc->next, newpCode(PCI(pc->next)->op, newpCodeOpBit("_WREG",
                                                                                     PCORB(PCI(pc->next)->pcop)->bit, 0)));
                    count++;
                    removed_count++;
                    if (removed_count >= HY08A_options.ob_count)
                    {
                        options.nopeep = 1;
                        return count;
                    }
                    continue;
                }

            // for struct members there are rrf cases
            // we do it on wreg then write it, volatile free
            if (PCI(pc->next)->label == NULL &&
                (PCI(pc->next)->op == POC_RRF ||
                 PCI(pc->next)->op == POC_RLF ||
                 PCI(pc->next)->op == POC_RLCF ||
                 PCI(pc->next)->op == POC_RRCF ||
                 // PCI(pc->next)->op == POC_ARRCF ||
                 PCI(pc->next)->op == POC_SWPF ||
                 PCI(pc->next)->op == POC_COMF) &&
                PCI(pc)->pcop->type == PO_GPR_TEMP && // only for temps
                pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next)->pcop))
            {
                pc1 = pc->next;
                pc2 = pc->prev;
                switch (PCI(pc->next)->op)
                {
                case POC_RRF:
                    pCodeReplace(pc, newpCode(POC_RRFW, PCOP(&pc_wreg)));
                    break;
                case POC_RLF:
                    pCodeReplace(pc, newpCode(POC_RLFW, PCOP(&pc_wreg)));
                    break;
                case POC_RLCF:
                    pCodeReplace(pc, newpCode(POC_RLCFW, PCOP(&pc_wreg)));
                    break;
                case POC_RRCF:
                    pCodeReplace(pc, newpCode(POC_RRCFW, PCOP(&pc_wreg)));
                    break;
                // case POC_ARRCF: pCodeReplace(pc, newpCode(POC_ARRCFW, PCOP(&pc_wreg))); break;
                case POC_SWPF:
                    pCodeReplace(pc, newpCode(POC_SWPFW, PCOP(&pc_wreg)));
                    break;
                case POC_COMF:
                    pCodeReplace(pc, newpCode(POC_COMFW, PCOP(&pc_wreg)));
                    break;
                default:
                    fprintf(stderr, "Internal error at %s:%d\n", __FILE__, __LINE__);
                    exit(-__LINE__);
                }
                pCodeReplace(pc1, newpCode(POC_MVWF, PCI(pc1)->pcop));
                pc = pc2;
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                continue;
            }

            // mvwf a rrfw a ==> rrfw wreg, limited on gpr-temp volatile free
            if (PCI(pc->next)->label == NULL &&
                    (PCI(pc->next)->op == POC_COMFW ||
                     PCI(pc->next)->op == POC_RLCFW ||
                     PCI(pc->next)->op == POC_RRCFW ||
                     PCI(pc->next)->op == POC_RRFW ||
                     PCI(pc->next)->op == POC_RLFW ||
                     PCI(pc->next)->op == POC_INFW ||
                     PCI(pc->next)->op == POC_DECFW ||
                     PCI(pc->next)->op == POC_INSUZW ||
                     PCI(pc->next)->op == POC_DCSUZW ||
                     PCI(pc->next)->op == POC_DCSZW ||
                     PCI(pc->next)->op == POC_INSZW) &&
                    pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next)->pcop) &&
                    PCI(pc)->pcop->type == PO_GPR_TEMP ||
                PCI(pc)->pcop->type == PO_GPR_REGISTER)
            {
                pCodeReplace(pc->next, newpCode(PCI(pc->next)->op, PCOP(&pc_wreg)));
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                continue;
            }
        }

        // 2021 aug hard-code peep for symmetric operators
        // MVWF X
        // MVFW Y
        // IORFW X
        // ==>
        // MVWF X
        // IORFW Y
        // volatile free
        if ((!(pc->prev) || !isPCI(pc->prev) || !(PCI(pc->prev)->isSkip)) &&
            pc->next && pc->next->next && isPCI(pc) && isPCI(pc->next) &&
            isPCI(pc->next->next) &&
            PCI(pc->next)->label == NULL &&
            PCI(pc->next->next)->label == NULL &&
            PCI(pc)->op == POC_MVWF &&
            PCI(pc->next)->op == POC_MVFW &&
            (PCI(pc->next->next)->op == POC_IORFW ||
             PCI(pc->next->next)->op == POC_ANDFW ||
             PCI(pc->next->next)->op == POC_XORFW ||
             PCI(pc->next->next)->op == POC_ADDFW ||
             PCI(pc->next->next)->op == POC_ADCFW) &&
            pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next->next)->pcop))
        {
            pc1 = pc->next;
            pCodeReplace(pc->next->next, newpCode(PCI(pc->next->next)->op, PCI(pc->next)->pcop));
            removepCode(&pc1);
            count++;
            removed_count++;
            if (removed_count >= HY08A_options.ob_count)
            {
                options.nopeep = 1;
                return count;
            }
            continue;
        }

        // 2021 Aug MVWF xxx MVFW with xxx no use W
        // limited on gpr-temp, volatile free
        if ((!(pc->prev) || !isPCI(pc->prev) || !(PCI(pc->prev)->isSkip)) &&
            isPCI(pc) && PCI(pc)->op == POC_MVWF && PCI(pc)->pcop->type == PO_GPR_TEMP)
        {
            int instrCount = 0;
            int found = 0;
            for (pc1 = pc->next; pc1 && isPCI(pc1); pc1 = pc1->next)
            {
                if (PCI(pc1)->op == POC_MVFW && PCI(pc1)->label == NULL) // no label!!
                {
                    if (pCodeOpCompare(PCI(pc1)->pcop, PCI(pc)->pcop))
                        found = 1;

                    break;
                }
                if (PCI(pc1)->label || (PCI(pc1)->outCond & PCC_W) ||
                    PCI(pc1)->op == POC_JMP ||
                    PCI(pc1)->op == POC_CALL || PCI(pc1)->isBitInst) // we don't manipulate bits yet skip?
                {
                    instrCount = 0;
                    break;
                }
                if (PCI(pc1)->outCond & PCC_REGISTER)
                {
                    if (PCI(pc1)->pcop2)
                    {
                        if (pCodeOpCompare(PCI(pc1)->pcop2, PCI(pc)->pcop))
                        {
                            instrCount = 0;
                            break;
                        }
                    }
                    else
                    {
                        if (pCodeOpCompare(PCI(pc1)->pcop, PCI(pc)->pcop))
                        {
                            instrCount = 0;
                            break;
                        }
                    }
                }
                instrCount++;
            }
            if (found && instrCount)
            {
                removepCode(&pc1);
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                continue;
            }
        }

        // 2021 MVWF + INFW/DEFW, limited on gpr-temp, volatile free // doubled?
        // if ((!(pc->prev) || !isPCI(pc->prev) || !(PCI(pc->prev)->isSkip)) &&
        //     isPCI(pc) && pc->next && isPCI(pc->next) && PCI(pc->next)->label == NULL &&
        //     pc->next->next && isPCI(pc->next->next) && PCI(pc->next->next)->isSkip && // only for skip operations
        //     PCI(pc)->op == POC_MVWF && (PCI(pc->next)->op == POC_INFW || PCI(pc->next)->op == POC_DECFW) &&
        //     (PCI(pc->next)->pcop->type == PO_GPR_REGISTER || PCI(pc->next)->pcop->type == PO_GPR_TEMP) &&
        //     pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next)->pcop))
        // {
        //     pCodeReplace(pc->next, newpCode(PCI(pc->next)->op, PCOP(&pc_wreg)));
        //     count++;
        //     removed_count++;
        //     if (removed_count >= HY08A_options.ob_count)
        //     {
        //         options.nopeep = 1;
        //         return count;
        //     }
        //     continue;
        // }

        // 2021 STA+RRFW optimize
        // RRFW is always checkable , volatile free **
        if ((!(pc->prev) || !isPCI(pc->prev) || !(PCI(pc->prev)->isSkip)) &&
            pc->next && isPCI(pc->next) && isPCI(pc) &&
            PCI(pc)->op == POC_MVWF && PCI(pc->next)->op == POC_RRFW &&
            pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next)->pcop) &&
            PCI(pc->next)->label == NULL)
        {
            pCode *pcn = pc->next;
            pCodeReplace(pc, newpCode(POC_IORL, popGetLit(0)));
            pCodeReplace(pcn, newpCode(POC_MVWF, PCI(pcn)->pcop));
            count++;
            removed_count++;
            pc = pcn;
            if (removed_count >= HY08A_options.ob_count)
            {
                options.nopeep = 1;
                return count;
            }
            continue;
        }

        // mfwf x + swpf x + mvfw x
        // if target can swap, it is volatile free
        if ((!(pc->prev) || !isPCI(pc->prev) || !PCI(pc->prev)->isSkip) && isPCI(pc) && pc->next && isPCI(pc->next) && PCI(pc->next)->label == NULL &&
            pc->next->next && isPCI(pc->next->next) && PCI(pc->next->next)->label == NULL)
        {
            if (PCI(pc)->op == POC_MVWF && PCI(pc->next)->op == POC_SWPF && PCI(pc->next->next)->op == POC_MVFW &&
                pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next)->pcop) && pCodeOpCompare(PCI(pc->next->next)->pcop, PCI(pc->next)->pcop))
            {
                pc1 = pc->next;
                pc2 = pc->next->next;
                pCodeReplace(pc1, newpCode(POC_SWPFW, PCOP(&pc_wreg)));
                pCodeReplace(pc2, newpCode(POC_MVWF, PCI(pc1)->pcop));
                pc1 = pc->next;
                removepCode(&pc);
                pc = pc1;
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                continue;

            } // only 2  case
            else
            {
                if (PCI(pc)->op == POC_MVWF && PCI(pc->next)->op == POC_SWPF &&
                    pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next)->pcop))
                {
                    pc1 = pc->next;
                    pCodeReplace(pc1, newpCode(POC_MVWF, PCI(pc1)->pcop));
                    pc1 = pc->next;
                    pCodeReplace(pc, newpCode(POC_SWPFW, PCOP(&pc_wreg)));
                    pc = pc1;
                    count++;
                    removed_count++;
                    if (removed_count >= HY08A_options.ob_count)
                    {
                        options.nopeep = 1;
                        return count;
                    }
                    continue;
                }
            }
        }

        // 2021 peep 2 hard-coded , mvwf a xxx mvfw a, if xxx no change w, next mvfw a can be removed
        // note that this is volatile dangerous, sfr registers excluded!!
        if ((!(pc->prev) || !isPCI(pc->prev) || !(PCI(pc->prev)->isSkip)) &&
            isPCI(pc) && pc->next && pc->next->next &&
            PCI(pc)->op == POC_MVWF &&
            PCI(pc)->pcop->type != PO_POINC0 &&
            PCI(pc)->pcop->type != PO_POINC &&
            PCI(pc)->pcop->type != PO_PLUSW &&
            PCI(pc)->pcop->type != PO_INDF && // 2024 oct 4.4.4

            (PCI(pc)->pcop->type != PO_SFR_REGISTER) && // we skip sfr first
            isPCI(pc->next) && isPCI(pc->next->next) &&
            PCI(pc->next)->label == NULL && PCI(pc->next->next)->label == NULL &&
            PCI(pc->next->next)->op == POC_MVFW &&
            pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next->next)->pcop) &&
            !PCI(pc->next)->isBitInst && !PCI(pc->next)->isSkip &&
            !PCI(pc->next)->isBranch &&
            !(PCI(pc->next)->outCond & PCC_W) && // the next is to retore w, cannot optimize
            !((PCI(pc->next)->pcop2 && pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next)->pcop2)) ||
              ((!PCI(pc->next)->pcop2) && (PCI(pc->next)->outCond & PCC_REGISTER) &&
               pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next)->pcop))))
        {
            pc1 = pc->next->next;
            removepCode(&pc1);
            count++;
            removed_count++;
            if (removed_count >= HY08A_options.ob_count)
            {
                options.nopeep = 1;
                return count;
            }
            continue;
        }

        //  because later mvwf sometimes can be optimized later
        // if changed to ADDFW, optimization is hard
        // so we don't implement it for temp, but for other variables
        // we change no-use sta to fix this
        // 2021 aug peeph 2 hard-coded
        // addfw a ; mvwf a ==> addwf a; mvfw a; others can be optimized later
        // this is volatile free
        if ((!(pc->prev) || !isPCI(pc->prev) || !(PCI(pc->prev)->isSkip)) &&
            isPCI(pc) && pc->next && pc->next->next && isPCI(pc->next) &&
            PCI(pc->next)->label == NULL &&
            (PCI(pc)->op == POC_ADDFW || PCI(pc)->op == POC_SUBFWW || PCI(pc)->op == POC_ADCFW ||
             PCI(pc)->op == POC_XORFW || PCI(pc)->op == POC_IORFW || PCI(pc)->op == POC_ANDFW ||
             PCI(pc)->op == POC_COMFW || PCI(pc)->op == POC_INFW || PCI(pc)->op == POC_DECFW ||
             PCI(pc)->op == POC_RRCFW || PCI(pc)->op == POC_RLCFW || PCI(pc)->op == POC_SBCFWW) &&
            PCI(pc->next)->op == POC_MVWF
            //! chkReferencedBeforeChange(pc->next->next, PCOR(PCI(pc)->pcop)->r->rIdx,0,0,0))
            //&& pc->pb && pc->pb->lastOptCount
        )
        {
            if ((possibleFinal || (PCI(pc)->pcop->type != PO_GPR_TEMP)) &&
                pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next)->pcop))
            {
                HYA_OPCODE p = 0;
                switch (PCI(pc)->op)
                {
                case POC_ADDFW:
                    p = POC_ADDWF;
                    break;
                case POC_SUBFWW:
                    p = POC_SUBFWF;
                    break;
                case POC_ADCFW:
                    p = POC_ADCWF;
                    break;
                case POC_XORFW:
                    p = POC_XORF;
                    break;
                case POC_IORFW:
                    p = POC_IORWF;
                    break;
                case POC_ANDFW:
                    p = POC_ANDWF;
                    break;
                case POC_COMFW:
                    p = POC_COMF;
                    break;
                case POC_INFW:
                    p = POC_INF;
                    break;
                case POC_DECFW:
                    p = POC_DECF;
                    break;
                case POC_RRCFW:
                    p = POC_RRCF;
                    break;
                case POC_RLCFW:
                    p = POC_RLCF;
                    break;
                case POC_SBCFWW:
                    p = POC_SBCFWF;
                    break;
                default:
                    fprintf(stderr, "Internal error at %s:%d. Give up\n", __FILE__, __LINE__);
                    exit(-__LINE__);
                }
                pc1 = pc->prev;
                pc2 = pc->next;
                pCodeReplace(pc, newpCode(p, PCI(pc)->pcop));
                if (chkNoUseW(pc2->next, 0))
                    removepCode(&pc2);
                else
                    pCodeReplace(pc2, newpCode(POC_MVFW, PCI(pc2)->pcop));
                pc = pc1;
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                continue;
            }
        }

        // 2021 INFW/DCFW + BTSS/BTSZ
        //  this is volatile free
        if ((!(pc->prev) || !isPCI(pc->prev) || !(PCI(pc->prev)->isSkip)) &&
            isPCI(pc) && pc->next && isPCI(pc->next) && PCI(pc->next)->label == NULL &&
            (PCI(pc)->op == POC_INFW || PCI(pc)->op == POC_DECFW) &&
            (PCI(pc->next)->inCond & PCC_Z))
        {
            HYA_OPCODE newop = 0;
            pCode *pc1 = pc->next;
            if (PCI(pc)->op == POC_INFW)
            {
                if (PCI(pc->next)->op == POC_BTSS) // skip if z
                    newop = POC_INSZ;
                else
                    newop = POC_INSUZ;
            }
            else
            {
                if (PCI(pc->next)->op == POC_BTSS) // skip if z
                    newop = POC_DCSZ;
                else
                    newop = POC_DCSUZ;
            }
            pCodeReplace(pc, newpCode(newop, PCI(pc)->pcop));
            removepCode(&pc1);
            count++;
            removed_count++;
            if (removed_count >= HY08A_options.ob_count)
            {
                options.nopeep = 1;
                return count;
            }
            continue;
        }

        // ------------------------ first is MVFW ---------------------
        // 2021
        // mvfw y +btxx _WREG,x => btxx y,x
        // this is volatile free
        if ((!(pc->prev) || !isPCI(pc->prev) || !(PCI(pc->prev)->isSkip)) &&
            isPCI(pc) && pc->next && isPCI(pc->next) && PCI(pc->next)->label == NULL &&
            PCI(pc)->op == POC_MVFW && (PCI(pc->next)->op == POC_BTSS || PCI(pc->next)->op == POC_BTSZ) &&
            PCI(pc->next)->pcop->type == PO_GPR_BIT && PCORB(PCI(pc->next)->pcop)->pcor.r &&
            PCORB(PCI(pc->next)->pcop)->pcor.r->rIdx == IDX_WREG &&
            chkNoUseW(pc->next->next,0))
        {
            int skip = 0;
            pCodeOp *pcop = newpCodeOpBitByOp(PCI(pc)->pcop, PCORB(PCI(pc->next)->pcop)->bit, 0);
            // if(PCI(pc)->pcop->name==NULL )
            //{
            if (PCI(pc)->pcop->type == PO_DIR || PCI(pc)->pcop->type == PO_GPR_REGISTER || PCI(pc)->pcop->type == PO_GPR_TEMP ||
                PCI(pc)->pcop->type == PO_SFR_REGISTER)
            {
                // copy it
                PCORB(pcop)->pcor = *((pCodeOpReg *)(PCI(pc)->pcop));
                PCORB(pcop)->pcor.pcop.type = PO_GPR_BIT;
                PCORB(pcop)->pcor.instance = 0;
                PCORB(pcop)->subtype = PCI(pc)->pcop->type;
            }
            else if (PCI(pc)->pcop->type == PO_IMMEDIATE)
            {
                PCORB(pcop)->pcor.pcop = (PCOI(PCI(pc)->pcop)->pcop); // copy
                PCORB(pcop)->pcor.pcop.type = PO_GPR_BIT;
                PCORB(pcop)->pcor.r = PCOI(PCI(pc)->pcop)->r;
                PCORB(pcop)->pcor.instance = PCOI(PCI(pc)->pcop)->index;
                PCORB(pcop)->subtype = PO_GPR_REGISTER;
                PCORB(pcop)->pcor.rIdx = PCOI(PCI(pc)->pcop)->rIdx;
                PCORB(pcop)->bit = PCORB(PCI(pc->next)->pcop)->bit;
            }
            else
            {
                skip = 1;
            }
            //}
            if (!skip)
            {
                pCodeReplace(pc->next, newpCode(PCI(pc->next)->op, pcop));
                removepCode(&pc); // it comes to pc->prev
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }

                continue;
            }
        }

        // 2021 MVFW + TFSZ w
        // this is volatile free
        if ((!(pc->prev) || !isPCI(pc->prev) || !(PCI(pc->prev)->isSkip)) &&
            isPCI(pc) && pc->next && isPCI(pc->next) && PCI(pc->next)->label == NULL &&
            PCI(pc)->op == POC_MVFW && PCI(pc->next)->op == POC_TFSZ && PCI(pc->next)->pcop->type == PO_W)
        {
            pCode *pc1 = pc->next;
            pCode *pc2 = pc1->next;
            pCodeReplace(pc, newpCode(POC_TFSZ, PCI(pc)->pcop));
            pc = pc1;
            removepCode(&pc);
            pc = pc2; // make it continue
            count++;
            removed_count++;
            if (removed_count >= HY08A_options.ob_count)
            {
                options.nopeep = 1;
                return count;
            }
            continue;
        }

        // MVFW a addfw b mvwf a ==> mvfw b addwf a, w not used again
        // volatile free
        if ((!(pc->prev) || !isPCI(pc->prev) || !(PCI(pc->prev)->isSkip)) &&
            isPCI(pc) && pc->next && pc->next->next &&
            isPCI(pc->next) && PCI(pc->next)->label == NULL &&
            isPCI(pc->next->next) && PCI(pc->next->next)->label == NULL &&
            PCI(pc)->op == POC_MVFW && PCI(pc->next->next)->op == POC_MVWF &&
            (PCI(pc->next)->op == POC_ADDFW || PCI(pc->next)->op == POC_ADCFW ||
             PCI(pc->next)->op == POC_XORFW || PCI(pc->next)->op == POC_IORFW || PCI(pc->next)->op == POC_ANDFW) &&
            pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next->next)->pcop) &&
            PCI(pc)->pcop->type != PO_GPR_TEMP // we don't directly write it yet
            && chkNoUseW(pc->next->next->next, 0))
        {
            HYA_OPCODE p = 0;
            switch (PCI(pc->next)->op)
            {
            case POC_ADDFW:
                p = POC_ADDWF;
                break; // only these 5 can is symmetric
            case POC_ADCFW:
                p = POC_ADCWF;
                break;
            case POC_XORFW:
                p = POC_XORF;
                break;
            case POC_IORFW:
                p = POC_IORWF;
                break;
            case POC_ANDFW:
                p = POC_ANDWF;
                break;
            default:
                fprintf(stderr, "Internal error at %s:%d. Give up\n", __FILE__, __LINE__);
                exit(-__LINE__);
            }
            pc1 = pc->prev;
            pc2 = pc->next;
            pc3 = pc->next->next;
            pCodeReplace(pc, newpCode(POC_MVFW, PCI(pc2)->pcop));
            pCodeReplace(pc2, newpCode(p, PCI(pc3)->pcop));
            removepCode(&pc3);
            pc = pc1;
            count++;
            removed_count++;
            if (removed_count >= HY08A_options.ob_count)
            {
                options.nopeep = 1;
                return count;
            }
            continue;
            continue;
        }

        // 2021 some times mvfw a andl x mvwf a
        // ==>blocked because and/or optimization is effected.
        // this is volatile dangerous, but if w/r is ok , then it should be ok
        if ((!(pc->prev) || !isPCI(pc->prev) || !(PCI(pc->prev)->isSkip)) &&
            isPCI(pc) && pc->next && pc->next->next &&
            isPCI(pc->next) && PCI(pc->next)->label == NULL &&
            isPCI(pc->next->next) && PCI(pc->next->next)->label == NULL &&
            PCI(pc)->op == POC_MVFW && PCI(pc->next->next)->op == POC_MVWF &&
            (PCI(pc->next)->op == POC_ADDLW || PCI(pc->next)->op == POC_ANDLW ||
             PCI(pc->next)->op == POC_XORLW || PCI(pc->next)->op == POC_IORL) &&
            // PCI(pc)->pcop->type!=PO_GPR_TEMP && PCI(pc)->pcop->type!=PO_W &&
            PCI(pc)->pcop->type != PO_W && PCI(pc)->pcop->type != PO_POINC && PCI(pc)->pcop->type != PO_POINC0 &&
            // PCI(pc)->pcop->type==PO_DIR && // we don't optimize temp, because it usually further optimized
            !(pc->next->next->next && isPCI(pc->next->next->next) && PCI(pc->next->next->next)->op == POC_MVFW &&
              pCodeOpCompare(PCI(pc->next->next->next)->pcop, PCI(pc)->pcop)) &&
            !(pc->prev && isPCI(pc->prev) && PCI(pc->prev)->op == POC_MVWF &&
              pCodeOpCompare(PCI(pc->prev)->pcop, PCI(pc)->pcop)) &&
            pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next->next)->pcop) &&
            chkNoUseW(pc->next->next->next, 0))
        {
            pc1 = pc->prev;
            pc2 = pc->next;
            pc3 = pc->next->next;
            if (PCI(pc)->cline == NULL && PCI(pc->next)->cline)
            {
                PCI(pc)->cline = PCI(pc->next)->cline;
                PCI(pc->next)->cline = NULL;
            }

            // special case addlw 1
            if (PCI(pc->next)->op == POC_ADDLW && PCI(pc->next)->pcop->type == PO_LITERAL &&
                PCOL(PCI(pc->next)->pcop)->lit == 1)
            {
                pCodeReplace(pc3, newpCode(POC_INF, PCI(pc)->pcop));
                removepCode(&pc2);
                removepCode(&pc);
            }
            else
            {
                pCodeReplace(pc, newpCode(POC_MVL, PCI(pc2)->pcop));
                switch (PCI(pc2)->op)
                {
                case POC_ADDLW:
                    pCodeReplace(pc2, newpCode(POC_ADDWF, PCI(pc3)->pcop));
                    break;
                case POC_ANDLW:
                    pCodeReplace(pc2, newpCode(POC_ANDWF, PCI(pc3)->pcop));
                    break;
                case POC_XORLW:
                    pCodeReplace(pc2, newpCode(POC_XORF, PCI(pc3)->pcop));
                    break;
                case POC_IORL:
                    pCodeReplace(pc2, newpCode(POC_IORWF, PCI(pc3)->pcop));
                    break;
                default:
                    fprintf(stderr, "Internal error at %s:%d\n", __FILE__, __LINE__);
                    exit(-__LINE__);
                    break;
                }
                removepCode(&pc3);
            }
            pc = pc1;
            count++;
            removed_count++;
            if (removed_count >= HY08A_options.ob_count)
            {
                options.nopeep = 1;
                return count;
            }
            continue;
        }

        // // 2021 _Bool optimization , brute-force

        // if ((!pc->prev || !isPCI(pc->prev) || !(PCI(pc->prev)->isSkip)) && pc->next &&
        //     pc->next->next && pc->next->next->next && pc->next->next->next->next &&
        //     pc->next->next->next->next->next && isPCI(pc->next) && isPCI(pc->next->next) && isPCI(pc->next->next->next) && isPCI(pc->next->next->next->next) &&
        //     isPCI(pc->next->next->next->next->next))
        // {
        //     pCode *pcn1 = pc->next;
        //     pCode *pcn2 = pc->next->next;
        //     pCode *pcn3 = pc->next->next->next;
        //     pCode *pcn4 = pcn3->next;
        //     pCode *pcn5 = pcn4->next;
        //     if ((PCI(pc)->op == POC_BTSS || PCI(pc)->op == POC_BTSZ) && PCI(pcn1)->op == POC_JMP && (PCI(pcn2)->op == POC_BCF || PCI(pcn2)->op == POC_BSF) &&
        //         PCI(pcn3)->op == POC_JMP && (PCI(pcn4)->op == POC_BCF || PCI(pcn4)->op == POC_BSF) &&
        //         PCI(pcn2)->pcop->name != NULL &&
        //         strncmp("_PT", PCI(pcn2)->pcop->name, 3)) // we don't dobble PTn
        //     {

        //         // there is jmp to return case pcn5 is jmp, no label
        //         if (PCI(pcn1)->pcop->type == PO_LABEL && PCI(pcn3)->pcop->type == PO_LABEL &&
        //             PCI(pcn4)->label && PCI(pcn5)->op == POC_JMP && PCI(pcn5)->pcop->type == PO_LABEL &&
        //             PCOLAB(PCI(pcn1)->pcop)->key == PCL(PCI(pcn4)->label->pc)->key &&
        //             PCOLAB(PCI(pcn3)->pcop)->key == PCOLAB(PCI(pcn5)->pcop)->key && // jump to same label
        //             pCodeOpCompare(PCI(pcn2)->pcop, PCI(pcn4)->pcop) && PCI(pcn2)->inverted_op == PCI(pcn4)->op)
        //         {
        //             int found = 0;
        //             pCode *pcLab;
        //             for (pcLab = pb->pcHead; pcLab; pcLab = pcLab->next)
        //             {
        //                 if (pcLab == pcn1) // skip pcn1
        //                     continue;
        //                 if (isPCI(pcLab) && PCI(pcLab)->pcop && PCI(pcLab)->pcop->type == PO_LABEL &&
        //                     PCOLAB(PCI(pcLab)->pcop)->key == PCL(PCI(pcn4)->label->pc)->key)
        //                 {
        //                     found = 1;
        //                     break;
        //                 }
        //             }
        //             if (!found)
        //             {
        //                 // let's replace, first is check if pc and pcn2 are the same flag.
        //                 // if yes, we just remove all or add toggle of the flag
        //                 if (pCodeOpCompare(PCI(pc)->pcop, PCI(pcn2)->pcop))
        //                 {
        //                     // consider following --> it is dummy
        //                     //    BTSZ A
        //                     //    JMP  B
        //                     //    BCF  A
        //                     //    JMP C
        //                     //    BSF A
        //                     if ((PCI(pc)->op == POC_BTSZ && PCI(pcn2)->op == POC_BCF) ||
        //                         (PCI(pc)->op == POC_BTSS && PCI(pcn2)->op == POC_BSF))
        //                     {
        //                         pc = pcn4;
        //                         removepCode(&pc);
        //                         removepCode(&pc);
        //                         removepCode(&pc);
        //                         removepCode(&pc);
        //                         removepCode(&pc);
        //                     }
        //                     else
        //                     {
        //                         pc = pcn4;
        //                         removepCode(&pc);
        //                         removepCode(&pc);
        //                         removepCode(&pc);
        //                         removepCode(&pc);
        //                         pCodeReplace(pc, newpCode(POC_BTGF, PCI(pc)->pcop));
        //                     }
        //                 }
        //                 else
        //                 {
        //                     // rearrange
        //                     pCode *pcpre = pc->prev;
        //                     if (!pcpre)
        //                         continue;
        // pCodeInsertAfter(pc, newpCode(PCI(pc)->op, PCI(pc)->pcop));
        // pCodeReplace(pc, newpCode(PCI(pcn4)->op, PCI(pcn4)->pcop));
        // removepCode(&pcn1); // now pc->xxx->pcn2->pcn3->pcn4->pcn5
        // removepCode(&pcn3);
        // removepCode(&pcn4);
        //                     //pcpre->next = pcn4;
        //                     //pcn4->prev = pcpre;
        //                     //pcn4->next = pc;
        //                     //pc->prev = pcn4;
        //                     //pc->next = pcn2;
        //                     //pcn2->prev = pc;
        //                     //pcn2->next = pcn5;
        //                     //pcn5->prev = pcn2;
        //                     //if (PCI(pc)->label) // pcn4 formerly label used no more
        //                     //{
        //                     //    PCI(pcn4)->label = PCI(pc)->label;
        //                     //}
        //                 }
        //                 count++;
        //                 removed_count++;
        //                 if (removed_count >= HY08A_options.ob_count)
        //                 {
        //                     options.nopeep = 1;
        //                     return count;
        //                 }
        //                 continue;
        //             }
        //         } // then direct return case
        //         else if (PCI(pcn1)->pcop->type == PO_LABEL && PCI(pcn3)->pcop->type == PO_LABEL &&
        //                  PCI(pcn4)->label && PCI(pcn5)->label && PCOLAB(PCI(pcn1)->pcop)->key == PCL(PCI(pcn4)->label->pc)->key &&
        //                  PCOLAB(PCI(pcn3)->pcop)->key == PCL(PCI(pcn5)->label->pc)->key &&
        //                  pCodeOpCompare(PCI(pcn2)->pcop, PCI(pcn4)->pcop) && PCI(pcn2)->inverted_op == PCI(pcn4)->op)
        //         {
        //             // we don't remove pcn5's label, but we shall remove pcn4'slabel
        //             // so we make sure pcn4's label is not used in other places, before we remove it
        //             int found = 0;
        //             pCode *pcLab;
        //             for (pcLab = pb->pcHead; pcLab; pcLab = pcLab->next)
        //             {
        //                 if (pcLab == pcn1) // skip pcn1
        //                     continue;
        //                 if (isPCI(pcLab) && PCI(pcLab)->pcop && PCI(pcLab)->pcop->type == PO_LABEL &&
        //                     PCOLAB(PCI(pcLab)->pcop)->key == PCL(PCI(pcn4)->label->pc)->key)
        //                 {
        //                     found = 1;
        //                     break;
        //                 }
        //             }
        //             if (!found)
        //             {
        //                 // let's replace, first is check if pc and pcn2 are the same flag.
        //                 // if yes, we just remove all or add toggle of the flag
        //                 if (pCodeOpCompare(PCI(pc)->pcop, PCI(pcn2)->pcop))
        //                 {
        //                     // consider following --> it is dummy
        //                     //    BTSZ A
        //                     //    JMP  B
        //                     //    BCF  A
        //                     //    JMP C
        //                     //    BSF A
        //                     if ((PCI(pc)->op == POC_BTSZ && PCI(pcn2)->op == POC_BCF) ||
        //                         (PCI(pc)->op == POC_BTSS && PCI(pcn2)->op == POC_BSF))
        //                     {
        //                         pc = pcn4;
        //                         removepCode(&pc);
        //                         removepCode(&pc);
        //                         removepCode(&pc);
        //                         removepCode(&pc);
        //                         removepCode(&pc);
        //                     }
        //                     else
        //                     {
        //                         pc = pcn4;
        //                         removepCode(&pc);
        //                         removepCode(&pc);
        //                         removepCode(&pc);
        //                         removepCode(&pc);
        //                         pCodeReplace(pc, newpCode(POC_BTGF, PCI(pc)->pcop));
        //                     }
        //                 }
        //                 else
        //                 {
        //                     // rearrange
        //                     pCode *pcpre = pc->prev;
        //                     if (!pcpre)
        //                         continue;
        //// if pc has label, it will have problem

        // pCodeInsertAfter(pc, newpCode(PCI(pc)->op, PCI(pc)->pcop));
        // pCodeReplace(pc, newpCode(PCI(pcn4)->op, PCI(pcn4)->pcop));
        // removepCode(&pcn1); // now pc->xxx->pcn2->pcn3->pcn4->pcn5
        // removepCode(&pcn3);
        // removepCode(&pcn4);
        //                     //pcpre->next = pcn4;
        //                     //pcn4->prev = pcpre;
        //                     //pcn4->next = pc;
        //                     //pc->prev = pcn4;
        //                     //pc->next = pcn2;
        //                     //pcn2->prev = pc;
        //                     //pcn2->next = pcn5;
        //                     //pcn5->prev = pcn2;
        //                     //if (PCI(pc)->label) // old label on pcn4 no more used
        //                     //{
        //                     //    PCI(pcn4)->label = PCI(pc)->label;

        //                     //}
        //                 }
        //                 count++;
        //                 removed_count++;
        //                 if (removed_count >= HY08A_options.ob_count)
        //                 {
        //                     options.nopeep = 1;
        //                     return count;
        //                 }
        //                 continue;
        //             }
        //         }
        //     }
        // }
        // first is ANDWF/IORWF/XORWF
        // if only for temp, volatile ok
        if ((!pc->prev || !isPCI(pc->prev) || !(PCI(pc->prev)->isSkip)) && isPCI(pc) &&
            pc->next && isPCI(pc->next) && pc->next->next && isPCI(pc->next->next) &&
            PCI(pc->next)->label == NULL &&
            (PCI(pc)->op == POC_ANDWF || PCI(pc)->op == POC_IORWF || PCI(pc)->op == POC_XORF ||
             // PCI(pc)->op == POC_RLCF || PCI(pc)->op == POC_RRCF || PCI(pc)->op == POC_ARRCF ||
             PCI(pc)->op == POC_INF || PCI(pc)->op == POC_DECF || PCI(pc)->op == POC_COMF) &&
            PCI(pc->next)->op == POC_MVFW &&
            PCI(pc)->pcop->type == PO_GPR_TEMP && PCOR(PCI(pc)->pcop)->r->rIdx >= 0x1000 && pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next)->pcop) &&
            // very special case gcc-torture-execute-20020307-1.c
            !(pc->next->next && isPCI(pc->next->next) && PCI(pc->next->next)->op == POC_MVWF &&
              pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next->next)->pcop)) &&
            !chkReferencedBeforeChange(pc->next->next, PCOR(PCI(pc)->pcop)->r->rIdx, NULL, 0, 0))
        {
            pc1 = pc->next;
            pc2 = pc->prev;
            switch (PCI(pc)->op)
            {
            case POC_ANDWF:
                pCodeReplace(pc, newpCode(POC_ANDFW, PCI(pc->next)->pcop));
                break;
            case POC_IORWF:
                pCodeReplace(pc, newpCode(POC_IORFW, PCI(pc->next)->pcop));
                break;
            case POC_XORF:
                pCodeReplace(pc, newpCode(POC_XORFW, PCI(pc->next)->pcop));
                break;
            // case POC_RLCF:          pCodeReplace(pc, newpCode(POC_RLCFW, PCI(pc->next)->pcop)); break;
            // case POC_RRCF:          pCodeReplace(pc, newpCode(POC_RRCFW, PCI(pc->next)->pcop)); break;
            // case POC_ARRCF:         pCodeReplace(pc, newpCode(POC_ARRCFW, PCI(pc->next)->pcop)); break;
            case POC_INF:
                pCodeReplace(pc, newpCode(POC_INFW, PCI(pc->next)->pcop));
                break;
            case POC_DECF:
                pCodeReplace(pc, newpCode(POC_DECFW, PCI(pc->next)->pcop));
                break;
            case POC_COMF:
                pCodeReplace(pc, newpCode(POC_COMFW, PCI(pc->next)->pcop));
                break;
            default:
                fprintf(stderr, "Internal Error at %s:%d\n", __FILE__, __LINE__);
                exit(-__LINE__);
            }
            removepCode(&pc1);
            pc = pc2;
            count++;
            removed_count++;
            if (removed_count >= HY08A_options.ob_count)
            {
                options.nopeep = 1;
                return count;
            }
            continue;
        }

        // first is MVL
        // MVL 0 ADDC X, where X is formerly zero
        // volatile free
        if ((!pc->prev || !isPCI(pc->prev) || !(PCI(pc->prev)->isSkip)) &&
            isPCI(pc) && PCI(pc)->op == POC_MVL && PCI(pc)->pcop->type == PO_LITERAL &&
            PCOL(PCI(pc)->pcop)->lit == 0 &&
            pc->next && isPCI(pc->next) && PCI(pc->next)->op == POC_ADCFW && PCI(pc->next)->label == NULL &&
            PCI(pc->next)->pcop->type == PO_GPR_TEMP && ifTempFormerlyZero(pc->prev, PCOR(PCI(pc->next)->pcop)->r->rIdx))
        {
            pc1 = pc->prev;
            pc2 = pc->next;
            pCodeReplace(pc->next, newpCode(POC_RLCFW, PCOP(&pc_wreg)));
            pCodeReplace(pc, newpCode(POC_MVL, newpCodeOpLit(0)));

            pc = pc1;
            count++;
            removed_count++;
            if (removed_count >= HY08A_options.ob_count)
            {
                options.nopeep = 1;
                return count;
            }
            continue;
        }

        // MVL, xxx mvl, w not used ... volatile free
        if ((!pc->prev || !isPCI(pc->prev) || !(PCI(pc->prev)->isSkip)) &&
            isPCI(pc) && PCI(pc)->op == POC_MVL && PCI(pc)->pcop->type == PO_LITERAL &&
            pc->next && isPCI(pc->next) && PCI(pc->next)->isSkip &&
            pc->next->next && isPCI(pc->next->next) && PCI(pc->next->next)->op == POC_MVL && PCI(pc->next->next)->pcop->type == PO_LITERAL &&
            pc->next->next->next && isPCI(pc->next->next->next) && PCI(pc->next->next->next)->op == POC_MVWF && PCI(pc->next->next->next)->label == NULL &&
            (!(pc->next->next->next->next) || !isPCI(pc->next->next->next->next) || PCI(pc->next->next->next->next)->op != POC_MVWF) &&
            (pc->next->next->next->next == NULL || chkNoUseW(pc->next->next->next->next, 0)))
        {
            int lit0 = PCOL(PCI(pc)->pcop)->lit;
            int lit1 = PCOL(PCI(pc->next->next)->pcop)->lit;
            pCodeOp *pcop = PCI(pc->next->next->next)->pcop;
            // we transfer 00/ff case for sign only,
            // not for bit to temp
            // if(( lit0==0 || lit0==0xff) && ( lit1==0 || lit1==0xff || (lit1-lit0)==1 || (lit0-lit1)==1))
            if ((lit0 == 0 || lit0 == 0xff) && (lit1 == 0 || lit1 == 0xff))
            {
                pc1 = pc->prev;
                pc2 = pc->next->next;
                pc3 = pc->next->next->next;
                switch (lit0)
                {
                case 0:
                    pCodeReplace(pc, newpCode(POC_CLRF, pcop));
                    break;
                case 0xff:
                    pCodeReplace(pc, newpCode(POC_SETF, pcop));
                    break;
                default:
                    fprintf(stderr, "Internal error at %s:%d\n", __FILE__, __LINE__);
                    exit(-__LINE__);
                }
                if (lit1 == 0x00)
                {
                    pCodeReplace(pc2, newpCode(POC_CLRF, pcop));
                }
                else if (lit1 == 0xff)
                {
                    pCodeReplace(pc2, newpCode(POC_SETF, pcop));
                }
                else if (lit1 == 1)
                {
                    pCodeReplace(pc2, newpCode(POC_INF, pcop));
                }
                else if (lit1 == 0xfe)
                {
                    pCodeReplace(pc2, newpCode(POC_DECF, pcop));
                }
                else
                {
                    fprintf(stderr, "Internal error at %s:%d\n", __FILE__, __LINE__);
                    exit(-__LINE__);
                }
                removepCode(&pc3);
                pc = pc1;
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                continue;
            }
        }
        // 2021 oct and or xor a bit optimization
        // volatile free
        if (
            (!pc->prev || !isPCI(pc->prev) || !(PCI(pc->prev)->isSkip)) &&
            isPCI(pc) && PCI(pc)->op == POC_MVL && PCI(pc)->pcop->type == PO_LITERAL)
        {

            lit = PCOL(PCI(pc)->pcop)->lit;
            bitid = -1;
            switch (lit)
            {
            case 0x01:
                bitid = 0;
                break;
            case 0x02:
                bitid = 1;
                break;
            case 0x04:
                bitid = 2;
                break;
            case 0x08:
                bitid = 3;
                break;
            case 0x10:
                bitid = 4;
                break;
            case 0x20:
                bitid = 5;
                break;
            case 0x40:
                bitid = 6;
                break;
            case 0x80:
                bitid = 7;
                break;
            case 0xFE:
                bitid = 8;
                break;
            case 0xfd:
                bitid = 9;
                break;
            case 0xfb:
                bitid = 10;
                break;
            case 0xf7:
                bitid = 11;
                break;
            case 0xef:
                bitid = 12;
                break;
            case 0xdf:
                bitid = 13;
                break;
            case 0xbf:
                bitid = 14;
                break;
            case 0x7f:
                bitid = 15;
                break;
            default:
                break;
            }
            if (bitid >= 0 && bitid < 8) // 0..7
            {
                if (pc->next && isPCI(pc->next) && PCI(pc->next)->label == NULL &&
                    (PCI(pc->next)->op == POC_IORWF || PCI(pc->next)->op == POC_XORF))
                {
                    // change to BSF+MVL
                    pCode *npc = newpCode(POC_MVL, newpCodeOpLit(lit));
                    pc1 = pc->next;
                    if (PCI(pc->next)->op == POC_XORF)
                        pCodeReplace(pc, newpCode(POC_BTGF, newpCodeOpBitByOp(PCI(pc->next)->pcop, bitid, 0)));
                    else
                        pCodeReplace(pc, newpCode(POC_BSF, newpCodeOpBitByOp(PCI(pc->next)->pcop, bitid, 0)));
                    if (chkNoUseW(pc1->next, 0))
                    {
                        pc = pc1->next;
                        removepCode(&pc1);
                    }
                    else
                    {
                        pCodeReplace(pc1, npc);
                        pc = npc->prev;
                    }

                    count++;
                    removed_count++;
                    if (removed_count >= HY08A_options.ob_count)
                    {
                        options.nopeep = 1;
                        return count;
                    }
                    continue;
                }
                else if (pc->next && pc->next->next && isPCI(pc->next) && isPCI(pc->next->next) && PCI(pc->next)->label == NULL &&
                         PCI(pc->next->next)->label == NULL &&
                         PCI(pc->next)->op == POC_IORFW && PCI(pc->next->next)->op == POC_MVWF && pCodeOpCompare(PCI(pc->next)->pcop, PCI(pc->next->next)->pcop))
                {
                    pc1 = pc->next;
                    pc2 = pc->next->next;
                    pc3 = pc->next->next->next;

                    pCodeReplace(pc, newpCode(POC_BSF, newpCodeOpBitByOp(PCI(pc->next)->pcop, bitid, 0)));
                    removepCode(&pc1);
                    if (chkNoUseW(pc3, 0))
                    {
                        removepCode(&pc2);
                    }
                    else
                    {
                        pCodeReplace(pc2, newpCode(POC_MVFW, PCI(pc2)->pcop));
                    }
                    pc = pc3->prev;
                    count++;
                    removed_count++;
                    if (removed_count >= HY08A_options.ob_count)
                    {
                        options.nopeep = 1;
                        return count;
                    }
                    continue;
                }
            }
            else if (bitid >= 8 && bitid < 16) // 0..7
            {
                if (pc->next && isPCI(pc->next) && PCI(pc->next)->label == NULL &&
                    PCI(pc->next)->op == POC_ANDWF && PCI(pc->next)->pcop->name)
                {
                    // change to BSF+MVL
                    pCode *npc = newpCode(POC_MVL, newpCodeOpLit(lit));
                    pc1 = pc->next;
                    pCodeReplace(pc, newpCode(POC_BCF, newpCodeOpBitByOp(PCI(pc->next)->pcop, bitid - 8, 0)));
                    if (chkNoUseW(pc1->next, 0))
                    {
                        pc = pc1->next;
                        removepCode(&pc1);
                    }
                    else
                    {
                        pCodeReplace(pc1, npc);
                        pc = npc->prev;
                    }
                    count++;
                    removed_count++;
                    if (removed_count >= HY08A_options.ob_count)
                    {
                        options.nopeep = 1;
                        return count;
                    }
                    continue;
                }
                else if (pc->next && pc->next->next && isPCI(pc->next) && isPCI(pc->next->next) && PCI(pc->next)->label == NULL &&
                         PCI(pc->next->next)->label == NULL &&
                         PCI(pc->next)->op == POC_ANDFW && PCI(pc->next->next)->op == POC_MVWF && pCodeOpCompare(PCI(pc->next)->pcop, PCI(pc->next->next)->pcop))
                {
                    pc1 = pc->next;
                    pc2 = pc->next->next;
                    pc3 = pc->next->next->next;

                    pCodeReplace(pc, newpCode(POC_BCF, newpCodeOpBitByOp(PCI(pc->next)->pcop, bitid - 8, 0)));
                    removepCode(&pc1);
                    if (chkNoUseW(pc3, 0))
                    {
                        removepCode(&pc2);
                    }
                    else
                    {
                        pCodeReplace(pc2, newpCode(POC_MVFW, PCI(pc2)->pcop));
                    }
                    pc = pc3->prev;
                    count++;
                    removed_count++;
                    if (removed_count >= HY08A_options.ob_count)
                    {
                        options.nopeep = 1;
                        return count;
                    }
                    continue;
                }
            }
        }

        // IORL
        if (
            (!pc->prev || !isPCI(pc->prev) || !(PCI(pc->prev)->isSkip)) &&
            isPCI(pc) && PCI(pc)->op == POC_MVFW &&
            pc->next && isPCI(pc->next) && PCI(pc->next)->label == NULL &&
            (PCI(pc->next)->op == POC_IORL || PCI(pc->next)->op == POC_XORLW) &&
            PCI(pc->next)->pcop->type == PO_LITERAL &&
            pc->next->next && isPCI(pc->next->next) && PCI(pc->next->next)->label == NULL &&
            PCI(pc->next->next)->op == POC_MVWF && pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next->next)->pcop))
        {
            // special case, move cline to first instruction if is in second one
            if (PCI(pc->next)->cline && !(PCI(pc)->cline))
            {
                PCI(pc)->cline = PCI(pc->next)->cline;
                PCI(pc->next)->cline = NULL; // move it
            }
            lit = PCOL(PCI(pc->next)->pcop)->lit;
            bitid = -1;
            switch (lit)
            {
            case 0x01:
                bitid = 0;
                break;
            case 0x02:
                bitid = 1;
                break;
            case 0x04:
                bitid = 2;
                break;
            case 0x08:
                bitid = 3;
                break;
            case 0x10:
                bitid = 4;
                break;
            case 0x20:
                bitid = 5;
                break;
            case 0x40:
                bitid = 6;
                break;
            case 0x80:
                bitid = 7;
                break;

            default:
                break;
            }
            if (bitid >= 0 && bitid < 8)
            {

                pc1 = pc->next;
                pc2 = pc->next->next;
                if (PCI(pc->next)->op == POC_XORLW)
                    pCodeReplace(pc, newpCode(POC_BTGF, newpCodeOpBitByOp(PCI(pc)->pcop, bitid, 0)));
                else
                    pCodeReplace(pc, newpCode(POC_BSF, newpCodeOpBitByOp(PCI(pc)->pcop, bitid, 0)));
                if (chkNoUseW(pc2->next, 0))
                {
                    pc = pc1->prev;
                    removepCode(&pc1);
                }
                else
                {
                    pCode *npc = newpCode(POC_MVFW, PCI(pc)->pcop); // pc is still there

                    pCodeReplace(pc1, npc);
                    pc = npc->prev;
                }
                removepCode(&pc2);

                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                continue;
            }
        }
        // ANDL
        if (
            (!pc->prev || !isPCI(pc->prev) || !(PCI(pc->prev)->isSkip)) &&
            isPCI(pc) && PCI(pc)->op == POC_MVFW &&
            pc->next && isPCI(pc->next) && PCI(pc->next)->label == NULL &&
            PCI(pc->next)->op == POC_ANDLW &&
            PCI(pc->next)->pcop->type == PO_LITERAL)
        {
            if (pc->next->next && isPCI(pc->next->next) && PCI(pc->next->next)->label == NULL &&
                PCI(pc->next->next)->op == POC_MVWF && pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next->next)->pcop))
            {
                if (PCI(pc->next)->cline && !(PCI(pc)->cline))
                {
                    PCI(pc)->cline = PCI(pc->next)->cline;
                    PCI(pc->next)->cline = NULL; // move it
                }
                lit = PCOL(PCI(pc->next)->pcop)->lit;
                bitid = -1;
                switch (lit)
                {
                case 0xFE:
                    bitid = 0;
                    break;
                case 0xFD:
                    bitid = 1;
                    break;
                case 0xFB:
                    bitid = 2;
                    break;
                case 0xF7:
                    bitid = 3;
                    break;
                case 0xEF:
                    bitid = 4;
                    break;
                case 0xDF:
                    bitid = 5;
                    break;
                case 0xBF:
                    bitid = 6;
                    break;
                case 0x7F:
                    bitid = 7;
                    break;

                default:
                    break;
                }
                if (bitid >= 0 && bitid < 8)
                {

                    pc1 = pc->next;
                    pc2 = pc->next->next;
                    pCodeReplace(pc, newpCode(POC_BCF, newpCodeOpBitByOp(PCI(pc)->pcop, bitid, 0)));
                    if (chkNoUseW(pc2->next, 0))
                    {
                        pc = pc1->prev;
                        removepCode(&pc1);
                    }
                    else
                    {
                        pCode *npc = newpCode(POC_MVFW, PCI(pc)->pcop);

                        pCodeReplace(pc1, npc);
                        pc = npc->prev;
                    }
                    removepCode(&pc2);

                    count++;
                    removed_count++;
                    if (removed_count >= HY08A_options.ob_count)
                    {
                        options.nopeep = 1;
                        return count;
                    }
                    continue;
                }
            }
            else if (pc->next->next && isPCI(pc->next->next) && PCI(pc->next->next)->label == NULL &&
                     //(PCI(pc->next->next)->op == POC_BTSS || PCI(pc->next->next)->op == POC_BTSZ) &&
                     (PCI(pc->next->next)->inCond & PCC_Z) // it is Z
            )
            {
                lit = PCOL(PCI(pc->next)->pcop)->lit;
                bitid = -1;
                switch (lit)
                {
                case 0x1:
                    bitid = 0;
                    break;
                case 0x2:
                    bitid = 1;
                    break;
                case 0x4:
                    bitid = 2;
                    break;
                case 0x8:
                    bitid = 3;
                    break;
                case 0x10:
                    bitid = 4;
                    break;
                case 0x20:
                    bitid = 5;
                    break;
                case 0x40:
                    bitid = 6;
                    break;
                case 0x80:
                    bitid = 7;
                    break;

                default:
                    break;
                }
                if (bitid >= 0 && bitid < 8)
                {
                    pCode *newp = newpCode(PCI(pc->next->next)->inverted_op,
                                           newpCodeOpBitByOp(PCI(pc)->pcop, bitid, 0));
                    pCode *newp0 = newpCode(POC_MVL, popGetLit(0));
                    pCodeReplace(pc->next->next, newp);
                    pCodeReplace(pc, newp0);
                    pc = newp0->next;
                    removepCode(&pc);

                    count++;
                    removed_count++;
                    if (removed_count >= HY08A_options.ob_count)
                    {
                        options.nopeep = 1;
                        return count;
                    }
                    continue;
                }
            }
        }

        // clrf/setf optimizations
        // volatile free
        if (
            (!pc->prev || !isPCI(pc->prev) || !(PCI(pc->prev)->isSkip)) &&
            isPCI(pc) && PCI(pc)->op == POC_MVL && PCI(pc)->pcop->type == PO_LITERAL)
        {

            int found = 0;
            int needCont = 0;
            int lit = PCOL(PCI(pc)->pcop)->lit;
            for (pc2 = pc->next; pc2 && isPCI(pc2) && PCI(pc2)->label == NULL; pc2 = pc2->next)
            {

                if (lit == 0 && PCI(pc2)->op == POC_MVWF)
                {
                    pCodeReplace(pc2, newpCode(POC_CLRF, PCI(pc2)->pcop));
                    count++;
                    removed_count++;
                    if (removed_count >= HY08A_options.ob_count)
                    {
                        options.nopeep = 1;
                        return count;
                    }
                    needCont = 1;
                    break; // need a break here
                }
                if (lit == 0xff && PCI(pc2)->op == POC_MVWF)
                {
                    pCodeReplace(pc2, newpCode(POC_SETF, PCI(pc2)->pcop));
                    count++;
                    removed_count++;
                    if (removed_count >= HY08A_options.ob_count)
                    {
                        options.nopeep = 1;
                        return count;
                    }
                    needCont = 1;
                    break; // need a break here
                }
                if (PCI(pc2)->op == POC_MVL && PCI(pc2)->pcop->type == PO_LITERAL &&
                    PCOL(PCI(pc2)->pcop)->lit == PCOL(PCI(pc)->pcop)->lit)
                {
                    found = 1;
                    break;
                }
                if (PCI(pc2)->isSkip || PCI(pc2)->isBranch || (PCI(pc2)->isBitInst && PCORB(PCI(pc2)->pcop)->subtype == PO_W) || (PCI(pc2)->outCond & PCC_W) || ((PCI(pc2)->pcop2 == NULL && PCI(pc2)->outCond & PCC_REGISTER) && PCI(pc2)->pcop && (PCI(pc2)->pcop->type == PO_W || PCI(pc2)->pcop->type == PO_PCL)) || (PCI(pc2)->pcop2 && (PCI(pc2)->pcop2->type == PO_W || PCI(pc2)->pcop2->type == PO_PCL)))
                {
                    break; // no found, give up
                }
            }
            if (needCont)
                continue;
            if (found)
            {
                removepCode(&pc2);
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                continue;
            }
        }

        // 2021 _Bool optimization
        // volatile free
        if (pc->next && pc->next->next && isPCI(pc) && isPCI(pc->next) && isPCI(pc->next->next) &&
            PCI(pc)->op == POC_MVL && PCI(pc)->pcop->type == PO_LITERAL && PCOL(PCI(pc)->pcop)->lit == 0 &&
            PCI(pc->next)->op == POC_RLCFW && PCI(pc->next)->pcop->type == PO_W &&
            (PCI(pc->next->next)->op == POC_BTSS || PCI(pc->next->next)->op == POC_BTSZ) && (PCI(pc->next->next)->inCond & PCC_Z))
        {

            HYA_OPCODE z2c = PCI(pc->next->next)->inverted_op;
            pc = pc->next->next;

            removepCode(&pc);
            removepCode(&pc);
            pCodeReplace(pc, newpCode(z2c, popCopyGPR2Bit(PCOP(&pc_status), HYA_C_BIT)));
            count++;
            removed_count++;
            if (removed_count >= HY08A_options.ob_count)
            {
                options.nopeep = 1;
                return count;
            }
            continue;
        }

        // first is MVL
        // 2021 mvl to FSR0/FSR1 ==> LDPR because packregister optimization skipped

        if ((!(pc->prev) || !isPCI(pc->prev) || !(PCI(pc->prev)->isSkip)) && isPCI(pc) &&
            hasContNextPCI(pc, 3) && PCI(pc)->op == POC_MVL && PCI(pc->next->next)->op == POC_MVL &&
            PCI(pc->next)->op == POC_MVWF && PCI(pc->next->next->next)->op == POC_MVWF &&
            PCI(pc)->pcop->type == PO_IMMEDIATE && PCI(pc->next->next)->pcop->type == PO_IMMEDIATE && !(HY08A_getPART()->isEnhancedCore >= 3 && HY08A_getPART()->isEnhancedCore != 5 && PCOI(PCI(pc)->pcop)->r && PCOI(PCI(pc)->pcop)->r->isFuncLocal) // no modify for 08d local!!
            && ((PCI(pc->next)->pcop->type == PO_FSR && PCI(pc->next->next->next)->pcop->type == PO_FSR) ||
                (PCI(pc->next)->pcop->type == PO_FSR0L && PCI(pc->next->next->next)->pcop->type == PO_FSR0H)))
        {

            if (PCI(pc->next)->pcop->type == PO_FSR)
            {
                if (!strcmp(PCI(pc->next)->pcop->name, "_FSR1L") &&
                    !strcmp(PCI(pc->next->next->next)->pcop->name, "_FSR1H"))
                {
                    pc1 = pc->next->next->next;
                    pCodeReplace(pc, newpCode2(POC_LDPR, PCI(pc)->pcop, newpCodeOpLit(1)));
                    removepCode(&pc1);
                    removepCode(&pc1);
                    removepCode(&pc1);
                    pc = pc1;
                    ++count;
                    if (++removed_count >= HY08A_options.ob_count)
                    {
                        options.nopeep = 1;
                        return count;
                    }
                    continue;
                }
            }
            else if (PCI(pc->next)->pcop->type == PO_FSR0L)
            {
                pc1 = pc->next->next->next;
                pCodeReplace(pc, newpCode2(POC_LDPR, PCI(pc)->pcop, newpCodeOpLit(0)));
                removepCode(&pc1);
                removepCode(&pc1);
                removepCode(&pc1);
                pc = pc1;
                ++count;
                if (++removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                continue;
            }
        }

        // mvl 0 rlcfw wreg tfsz ==> volatile free
        if (pc->next && pc->next->next && isPCI(pc) && isPCI(pc->next) && isPCI(pc->next->next) &&
            PCI(pc)->op == POC_MVL && PCI(pc)->pcop->type == PO_LITERAL && PCOL(PCI(pc)->pcop)->lit == 0 &&
            PCI(pc->next)->op == POC_RLCFW && PCI(pc->next)->pcop->type == PO_W &&
            (PCI(pc->next->next)->op == POC_TFSZ && PCI(pc->next->next)->pcop->type == PO_W)) // skip if z means skip if not C
        {

            pc = pc->next->next;

            removepCode(&pc);
            removepCode(&pc);
            pCodeReplace(pc, newpCode(POC_BTSZ, popCopyGPR2Bit(PCOP(&pc_status), HYA_C_BIT)));
            count++;
            removed_count++;
            if (removed_count >= HY08A_options.ob_count)
            {
                options.nopeep = 1;
                return count;
            }
            continue;
        }

        // mvl0 iorwf .. volatile free
        if ((!(pc->prev) || !isPCI(pc->prev) || !PCI(pc->prev)->isSkip) && isPCI(pc) && pc->next && isPCI(pc->next) && PCI(pc->next)->label == NULL &&
            PCI(pc)->op == POC_MVL && PCI(pc)->pcop->type == PO_LITERAL && PCOL(PCI(pc)->pcop)->lit == 0)
        {
            if (PCI(pc->next)->op == POC_IORWF || PCI(pc->next)->op == POC_XORF)
            {
                pc1 = pc->next;
                removepCode(&pc1); // 0 no effect on this op
                pc = pc1;
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                continue;
            }
            if (PCI(pc->next)->op == POC_ANDWF) // 0->andwf = clrf
            {
                pc1 = pc->next;
                pCodeReplace(pc1, newpCode(POC_CLRF, PCI(pc1)->pcop));
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                continue;
            }
        }
        // 2020 mvl ff and or

        // and or should not be volatile, volatile ok
        if ((!(pc->prev) || !isPCI(pc->prev) || !PCI(pc->prev)->isSkip) && isPCI(pc) && pc->next && isPCI(pc->next) && PCI(pc->next)->label == NULL &&
            PCI(pc)->op == POC_MVL && PCI(pc)->pcop->type == PO_LITERAL && PCOL(PCI(pc)->pcop)->lit == 255)
        {
            if (PCI(pc->next)->op == POC_ANDWF)
            {
                pc1 = pc->next;
                removepCode(&pc1); // mvl ff + andwf .. remove second, test z is using TFSZ/RRF, so we remove it
                pc = pc1;
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                continue;
            }
            if (PCI(pc->next)->op == POC_IORWF) // ff+iorwf = setf
            {
                pc1 = pc->next;
                pCodeReplace(pc1, newpCode(POC_SETF, PCI(pc1)->pcop));
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                continue;
            }
        }

        // 2021 oct mvfw iorfw/andfw reordering
        // only for gpr-temp, volatile ok
        if (
            (!(pc->prev) || !isPCI(pc->prev) || !PCI(pc->prev)->isSkip) && isPCI(pc) &&
            hasContNextPCI(pc, 2) &&
            PCI(pc)->op == POC_IORWF && PCI(pc->next)->op == POC_MVFW && PCI(pc->next->next)->op == POC_IORFW)
            if (PCI(pc)->pcop->type == PO_GPR_TEMP &&
                pCodeOpCompare(PCI(pc->next->next)->pcop, PCI(pc)->pcop))
            {
                pCode *pcn = newpCode(POC_MVWF, PCI(pc)->pcop);
                pc1 = pc->next;
                pc2 = pc->next->next;
                pc3 = pc->next->next->next;
                pCodeReplace(pc, newpCode(POC_IORFW, PCI(pc)->pcop));
                pCodeReplace(pc1, newpCode(POC_IORFW, PCI(pc1)->pcop));
                pCodeReplace(pc2, pcn); // optimized later
                pc = pcn->prev;
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                continue;
            }

        // 2021 ADD/ADDC/AND/IOR/XOR/COMF/INF/DECF/SUBF/SUBC +temp + mvfw temp + ret ... change it to w
        // volatile ok

        // following is 3 pci
        if (
            (!(pc->prev) || !isPCI(pc->prev) || !PCI(pc->prev)->isSkip) &&
            pc->next && pc->next->next && isPCI(pc) && isPCI(pc->next) && isPCI(pc->next->next))
        {
            if ( // no need RETV
                !(PCI(pc)->isSkip) &&
                //(PCI(pc)->inCond & PCC_W) && // no need incond W !!
                (PCI(pc)->inCond & PCC_REGISTER) &&
                (PCI(pc)->outCond & PCC_REGISTER) &&
                PCI(pc)->pcop &&
                PCI(pc->next)->op == POC_MVFW &&
                PCI(pc)->pcop->type == PO_GPR_TEMP && // it is a temp!!
                PCOR(PCI(pc)->pcop)->r->rIdx >= 0x1000 &&
                PCI(pc->next)->label == NULL && PCI(pc->next->next)->label == NULL &&
                pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next)->pcop) &&
                !chkReferencedBeforeChange(pc->next->next, PCOR(PCI(pc)->pcop)->r->rIdx, NULL, 0, 0))
            {
                HYA_OPCODE p = 1000; // it is impossible
                switch (PCI(pc)->op)
                {
                case POC_ADDWF:
                    p = POC_ADDFW;
                    break;
                case POC_ADCWF:
                    p = POC_ADCFW;
                    break;
                case POC_SUBFWF:
                    p = POC_SUBFWW;
                    break;
                case POC_SBCFWF:
                    p = POC_SBCFWW;
                    break;
                case POC_INF:
                    p = POC_INFW;
                    break;
                case POC_DECF:
                    p = POC_DECFW;
                    break;
                case POC_IORWF:
                    p = POC_IORFW;
                    break;
                case POC_XORF:
                    p = POC_XORFW;
                    break;
                case POC_ANDWF:
                    p = POC_ANDFW;
                    break;
                case POC_COMF:
                    p = POC_COMFW;
                    break;
                case POC_RLCF:
                    p = POC_RLCFW;
                    break;
                case POC_RRCF:
                    p = POC_RRCFW;
                    break;
                case POC_ARRCF:
                    p = POC_ARRCFW;
                    break;
                case POC_RRF:
                    p = POC_RRFW;
                    break;
                default:
                    break;
                }
                if (p < 1000)
                {
                    pc2 = pc->next;
                    pCodeReplace(pc, newpCode(p, PCI(pc)->pcop));
                    removepCode(&pc2);
                    count++;
                    removed_count++;
                    if (removed_count >= HY08A_options.ob_count)
                    {
                        options.nopeep = 1;
                        return count;
                    }
                    continue;
                }
            }
        }

        // wf rrfw same --> only for z test
        // RRFW is not for volatile registers, it is volatile OK
        if ((!(pc->prev) || !isPCI(pc->prev) || !PCI(pc->prev)->isSkip) && isPCI(pc) && pc->next && isPCI(pc->next) && PCI(pc->next)->label == NULL &&
            ((PCI(pc)->outCond & (PCC_REGISTER | PCC_Z)) == (PCC_REGISTER | PCC_Z)) && PCI(pc->next)->op == POC_RRFW && pc->next->next && isPCI(pc->next->next) &&
            (PCI(pc->next->next)->inCond & PCC_Z) && pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next)->pcop))
        {
            pc1 = pc->next;
            removepCode(&pc1); // 0 no effect on this op
            pc = pc1;
            count++;
            removed_count++;
            if (removed_count >= HY08A_options.ob_count)
            {
                options.nopeep = 1;
                return count;
            }
        }

        // 2020 no use  BCF STATUS,C generated by CLFR/ADDFW/ADDWF
        // volatile free

        if (isPCI(pc) && PCI(pc)->op == POC_BCF && (PCI(pc)->outCond & PCC_C) &&
            (!isPCI(pc->prev) || !PCI(pc->prev)->isSkip) && isPCI(pc->next) && !PCI(pc->next)->isSkip)
        {
            int opt = 0;
            for (pc1 = pc->next; isPCI(pc1); pc1 = pc1->next)
            {
                if (PCI(pc1)->op == POC_ADCFW)
                {
                    opt = 1;
                    pc2 = pc->next;
                    pCodeReplace(pc1, newpCode(POC_ADDFW, PCI(pc1)->pcop));
                    removepCode(&pc);
                    pc = pc2;
                    break;
                }
                if (PCI(pc1)->op == POC_ADCWF)
                {
                    opt = 1;
                    pc2 = pc->next;
                    pCodeReplace(pc1, newpCode(POC_ADDWF, PCI(pc1)->pcop));
                    removepCode(&pc);
                    pc = pc2;
                    break;
                }

                if (PCI(pc1)->inCond & PCC_C)
                    break;
                if (PCI(pc1)->isSkip)
                    break;
                if (PCI(pc1)->isBranch) // return C
                    break;
                if ((PCI(pc1)->outCond & PCC_C)) // branch cannot remove C!!, 2021 for return C
                {
                    pc1 = pc->next;
                    removepCode(&pc);
                    pc = pc1;
                    opt = 1;
                    break;
                }
            }
            if (opt)
            {
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                continue;
            }
        }

        // 2020 check if IORWF/ADDWF has F cleared

        // ADDWF ==> MVWF + CLRC
        // ADDFW ==> CLRC
        // IORWF ==> MVWF
        // limited to gpr-temp, volatile ok
        if (isPCI(pc) && (PCI(pc)->op == POC_ADDFW || PCI(pc)->op == POC_ADDWF || PCI(pc)->op == POC_IORWF) && PCI(pc)->pcop->type == PO_GPR_TEMP)
        {
            // search backward
            int opt = 0;
            for (pc1 = pc->prev; isPCI(pc1); pc1 = pc1->prev)
            {
                if (PCI(pc1)->isSkip || PCI(pc1)->isBranch || PCI(pc1)->label)
                    break;
                if (PCI(pc1)->op != POC_CLRF && ((PCI(pc1)->pcop && pCodeOpCompare(PCI(pc1)->pcop, PCI(pc)->pcop))) || (PCI(pc1)->pcop2 && pCodeOpCompare(PCI(pc1)->pcop2, PCI(pc)->pcop)))
                    break;
                if (PCI(pc1)->op == POC_CLRF && pCodeOpCompare(PCI(pc1)->pcop, PCI(pc)->pcop))
                {
                    opt = 1;
                    if (PCI(pc)->op == POC_ADDWF)
                    {
                        pc1 = newpCode(POC_MVWF, PCI(pc)->pcop);
                        pc2 = newpCode(POC_BCF, STATUSCBIT);
                        pCodeReplace(pc, pc1);
                        pc = pc1;
                        insertPCodeInstruction(PCI(pc->next), PCI(pc2));
                    }
                    else if (PCI(pc)->op == POC_ADDFW)
                    {
                        pc2 = newpCode(POC_BCF, STATUSCBIT);
                        pCodeReplace(pc, pc2);
                        pc = pc2;
                    }
                    else if (PCI(pc)->op == POC_IORWF)
                    {
                        pc2 = newpCode(POC_MVWF, PCI(pc)->pcop);
                        pCodeReplace(pc, pc2);
                        pc = pc2;
                    }
                    break;
                }
            }
            if (opt)
            {
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                } // it can proceed?
            }
        }

        // 2020 simplify ANDL 0
        // first is ANDL
        // we don't use this method to set Z flag
        // volatile free
        if (isPCI(pc) && PCI(pc)->op == POC_ANDLW && PCI(pc)->pcop->type == PO_LITERAL && PCOL(PCI(pc)->pcop)->lit == 0)
        {
            pc1 = pc->prev;
            pCodeReplace(pc, newpCode(POC_MVL, PCI(pc)->pcop));
            pc = pc1;
            count++;
            removed_count++;
            if (removed_count >= HY08A_options.ob_count)
            {
                options.nopeep = 1;
                return count;
            } // it can proceed
        }

        // 2021 ANDL 0xf0 + SWPF _W, there is not volatile for and+xor
        if ((!(pc->prev) || !isPCI(pc->prev) || !PCI(pc->prev)->isSkip) &&
            pc->next && pc->next->next && pc->next->next->next &&
            isPCI(pc) && isPCI(pc->next) && isPCI(pc->next->next) && isPCI(pc->next->next->next) &&
            PCI(pc)->op == POC_ANDLW && PCI(pc->next)->op == POC_SWPFW &&
            PCI(pc->next)->label == NULL && PCI(pc->next->next)->label == NULL && PCI(pc->next->next->next)->label == NULL &&
            PCI(pc->next)->pcop->type == PO_W)
        {
            // and swap compare
            if (PCI(pc->next->next)->op == POC_XORLW && PCI(pc->next->next)->pcop->type == PO_LITERAL && PCI(pc->next->next->next)->isSkip &&
                PCI(pc->next->next->next)->inCond & PCC_Z)
            {
                // remove swap and compare swapped value
                int lit = PCOL(PCI(pc->next->next)->pcop)->lit;
                lit = ((lit & 0x0f) << 4) | (lit >> 4); // swap
                pc1 = pc->next;
                pCodeReplace(pc->next->next, newpCode(POC_XORLW, newpCodeOpLit(lit)));
                removepCode(&pc1);
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                } // it can proceed
                continue;
            }
            // andl swp addl andl swpf
            // it should not be volatile
            if (pc->next->next->next->next && isPCI(pc->next->next->next->next) && PCI(pc->next->next->next->next)->label == NULL && PCI(pc->next->next)->op == POC_ADDLW && PCI(pc->next->next->next)->op == POC_ANDLW &&
                PCI(pc->next->next)->pcop->type == PO_LITERAL && PCI(pc->next->next->next)->pcop->type == PO_LITERAL &&
                PCI(pc->next->next->next->next)->op == POC_SWPFW && PCI(pc->next->next->next->next)->pcop->type == PO_W &&
                PCI(pc)->pcop->type == PO_LITERAL)
            {
                int lit0 = PCOL(PCI(pc)->pcop)->lit;
                int lit1 = PCOL(PCI(pc->next->next)->pcop)->lit;
                int lit2 = PCOL(PCI(pc->next->next->next)->pcop)->lit;
                if (lit0 == 0xf0 && lit2 == 0x0f)
                {
                    pc1 = pc->next->next->next->next;
                    pCodeReplace(pc->next, newpCode(POC_ADDLW, newpCodeOpLit(lit1 << 4)));
                    while (pc1 != pc->next)
                        removepCode(&pc1);
                    count++;
                    removed_count++;
                    if (removed_count >= HY08A_options.ob_count)
                    {
                        options.nopeep = 1;
                        return count;
                    } // it can proceed
                    continue;
                }
            }
        }

        // following 3 PCI, first not skip
        if (isPCI(pc) && !PCI(pc)->isSkip && pc->next && pc->next->next && isPCI(pc->next) && isPCI(pc->next->next))
        {
            // 2020 mvfw + ADDL 1/ADDL 255/XORL 255
            // same result, volatile free
            if ((!isPCI(pc->prev) || !PCI(pc->prev)->isSkip) &&
                PCI(pc)->op == POC_MVFW && (PCI(pc->next)->op == POC_ADDLW || PCI(pc->next)->op == POC_XORLW) &&
                PCI(pc->next)->pcop->type == PO_LITERAL &&
                PCI(pc->next)->label == NULL)
            {
                if (PCOL(PCI(pc->next)->pcop)->lit == 1 && PCI(pc->next)->op == POC_ADDLW)
                {
                    pc1 = newpCode(POC_INFW, PCI(pc)->pcop);
                    pCodeReplace(pc, pc1);
                    pc = pc1;
                    pc1 = pc->next;
                    removepCode(&pc1);
                    count++;
                    removed_count++;
                    if (removed_count >= HY08A_options.ob_count)
                    {
                        options.nopeep = 1;
                        return count;
                    }
                    continue;
                }
                if (PCOL(PCI(pc->next)->pcop)->lit == 255 && PCI(pc->next)->op == POC_ADDLW)
                {
                    pc1 = newpCode(POC_DECFW, PCI(pc)->pcop);
                    pCodeReplace(pc, pc1);
                    pc = pc1;
                    pc1 = pc->next;
                    removepCode(&pc1);
                    count++;
                    removed_count++;
                    if (removed_count >= HY08A_options.ob_count)
                    {
                        options.nopeep = 1;
                        return count;
                    }
                    continue;
                }
                if (PCOL(PCI(pc->next)->pcop)->lit == 255 && PCI(pc->next)->op == POC_XORLW)
                {
                    pc1 = newpCode(POC_COMFW, PCI(pc)->pcop);
                    pCodeReplace(pc, pc1);
                    pc = pc1;
                    pc1 = pc->next;
                    removepCode(&pc1);
                    count++;
                    removed_count++;
                    if (removed_count >= HY08A_options.ob_count)
                    {
                        options.nopeep = 1;
                        return count;
                    }
                    continue;
                }
            }

            // MVFW PRODH + LDPR ==> volatile free
            if ((!isPCI(pc->prev) || !PCI(pc->prev)->isSkip) &&
                PCI(pc)->op == POC_MVFW &&
                PCI(pc)->pcop->type == PO_PRODH &&
                PCI(pc->next)->op == POC_LDPR &&
                PCI(pc->next)->label == NULL)
            {
                pc1 = pc->prev;
                removepCode(&pc);
                pc = pc1;
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                continue;
            }

            // 2020 hard code double mvfw
            // limited source, should be volatile ok
            if ((!isPCI(pc->prev) || !PCI(pc->prev)->isSkip) &&
                PCI(pc)->op == POC_MVFW && PCI(pc->next)->op == POC_MVFW &&
                (!(PCI(pc->next)->pcop->type == PO_PLUSW)) &&
                (!(PCI(pc)->pcop->type == PO_ADCRB)) &&
                (!(PCI(pc)->pcop->type == PO_ADCO1B)) &&
                (!(PCI(pc)->pcop->type == PO_ADCO2B)) &&
                (!(PCI(pc)->pcop->type == PO_POINC)) &&
                (!(PCI(pc)->pcop->type == PO_POINC0)) &&
                (!(PCI(pc)->pcop->type == PO_PRINC0)) &&
                (!(PCI(pc)->pcop->type == PO_PODEC0)) &&
                (!(PCI(pc)->pcop->type == PO_PRINC)) &&
                (!(PCI(pc)->pcop->type == PO_SFR_REGISTER)) &&
                !((PCI(pc)->pcop->type == PO_IMMEDIATE) // LDA count DIR?
                  && PCOI(PCI(pc)->pcop)->r &&
                  PCOI(PCI(pc)->pcop)->r->isVolatile) &&
                !((PCI(pc)->pcop->type == PO_DIR) // LDA count DIR?
                  && PCOR(PCI(pc)->pcop)->r &&
                  PCOR(PCI(pc)->pcop)->r->isVolatile) &&
                PCI(pc->next)->label == NULL)
            {
                pc1 = newpCode(POC_MVFW, PCI(pc->next)->pcop);
                pc2 = pc->next;
                pCodeReplace(pc, pc1);
                removepCode(&pc2);
                pc = pc1;
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                continue;
            }

            // 2020 MVFW+MVWF
            // limited source, volatile ok
            if ((!isPCI(pc->prev) || !PCI(pc->prev)->isSkip) &&
                PCI(pc)->op == POC_MVFW && PCI(pc->next)->op == POC_MVWF &&
                pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next)->pcop2) &&
                (!(PCI(pc)->pcop->type == PO_SFR_REGISTER)) &&
                !(((PCI(pc)->pcop->type == PO_IMMEDIATE) // LDA count DIR?
                   && PCOI(PCI(pc)->pcop)->r &&
                   !PCOI(PCI(pc)->pcop)->r->isVolatile) ||
                  ((PCI(pc)->pcop->type == PO_DIR) // LDA count DIR?
                   && PCOR(PCI(pc)->pcop)->r &&
                   !PCOR(PCI(pc)->pcop)->r->isVolatile)) &&
                PCI(pc->next)->label == NULL)
            {
                pc1 = pc->next;
                removepCode(&pc1);
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                continue;
            }

            // 2020 mvfw + xxxzw volatile free
            if ((!isPCI(pc->prev) || !PCI(pc->prev)->isSkip) &&
                PCI(pc)->op == POC_MVFW && PCI(pc->next)->label == NULL &&
                (PCI(pc->next)->op == POC_INSZW || PCI(pc->next)->op == POC_INSUZW ||
                 PCI(pc->next)->op == POC_DCSZW || PCI(pc->next)->op == POC_DCSUZW) &&
                pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next)->pcop))
            {
                pCode *pc1 = newpCode(PCI(pc->next)->op, PCI(pc)->pcop);
                pc2 = pc->next;
                pCodeReplace(pc, pc1);
                removepCode(&pc2);
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                continue;
            }

            // 2020 mvwf + xxxzw volatile free
            if ((!isPCI(pc->prev) || !PCI(pc->prev)->isSkip) &&
                PCI(pc)->op == POC_MVWF &&
                PCI(pc->next)->label == NULL &&
                (PCI(pc->next)->op == POC_INSZW || PCI(pc->next)->op == POC_INSUZW ||
                 PCI(pc->next)->op == POC_DCSZW || PCI(pc->next)->op == POC_DCSUZW) &&
                pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next)->pcop))
            {
                pCode *pc1 = newpCode(PCI(pc->next)->op, pCodeOpCopy(PCOP(&pc_wreg)));
                pCodeReplace(pc->next, pc1);
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                continue;
            }

            // try MVL 0 + ADDWF volatile free

            if ((!isPCI(pc->prev) || !PCI(pc)->isSkip) && PCI(pc)->op == POC_MVL &&
                PCI(pc->next)->label == NULL &&
                PCI(pc->next)->op == POC_ADDWF && PCI(pc)->pcop->type == PO_LITERAL &&
                PCOLIT0(pc))
            {
                // modify ADDWF to clrc
                pCodeReplace(pc->next, newpCode(POC_BCF, STATUSCBIT));
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
            }

            // BCF C +MVL x+ ADDCWF -> remove BCF and change to ADDWF
            // volatile free
            if ((!isPCI(pc->prev) || !PCI(pc->prev)->isSkip) && PCI(pc)->op == POC_BCF &&
                PCI(pc->next)->label == NULL && PCI(pc->next->next)->label == NULL &&
                (PCI(pc)->outCond & PCC_C) && !(PCI(pc->next)->outCond & PCC_C) &&
                !(PCI(pc->next)->inCond & PCC_C) &&
                PCI(pc->next->next)->op == POC_ADCWF)
            {
                pc1 = newpCode(POC_ADDWF, PCI(pc->next->next)->pcop);
                pc2 = pc->next;
                removepCode(&pc);
                pc = pc2;
                pCodeReplace(pc->next, pc1);
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
            }

            // 2020 simple tfsz
            // volatile free
            if (PCI(pc->next)->op == POC_MVWF && PCI(pc->next->next)->op == POC_TFSZ &&
                pCodeOpCompare(PCI(pc->next)->pcop, PCI(pc->next->next)->pcop) &&
                PCI(pc->next->next)->label == NULL)
            {
                pCodeReplace(pc->next->next, newpCode(POC_TFSZ, pCodeOpCopy(PCOP(&pc_wreg))));
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                } // it can proceed
            }

            // 2020 NOV MVFW+CALL label ... usually W can be destroyed
            // volatile dangerous, limited volatile OK
            if (isPCI(pc) && PCI(pc)->label == NULL && isPCI(pc->next) && PCI(pc->next)->label == NULL &&
                PCI(pc)->op == POC_MVFW && PCI(pc->next)->op == POC_CALL &&
                (PCI(pc)->pcop->type == PO_GPR_TEMP || PCI(pc)->pcop->type == PO_DIR || PCI(pc)->pcop->type == PO_GPR_REGISTER) &&
                PCI(pc->next)->pcop->type == PO_LABEL)
            {
                pc1 = findLabelinpBlock(pc->pb, (pCodeOpLabel *)(PCI(pc->next)->pcop));
                if (pc1)
                {
                    pc2 = pc1->next;
                    if (isPCI(pc1) && isPCI(pc2) && !(PCI(pc1)->inCond & PCC_W) &&
                        !(PCI(pc2)->inCond & PCC_W) && ((PCI(pc1)->outCond & PCC_W) || (PCI(pc2)->outCond & PCC_W)))
                    {
                        removepCode(&pc);
                        count++;
                        removed_count++;
                        if (removed_count >= HY08A_options.ob_count)
                        {
                            options.nopeep = 1;
                            return count;
                        } // it can proceed?
                    }
                }
            }

            // 2020 NOV IORL 0 + BTSZ Z + INSZ/INSUZ/DESZ/DESUZ + STK00
            // volatile free
            // OR condition can switch order?
            if ((!isPCI(pc->prev) || !(PCI(pc->prev)->isSkip)) &&
                PCI(pc)->op == POC_CALL &&
                PCI(pc->next)->op == POC_IORL &&
                PCI(pc->next)->pcop->type == PO_LITERAL &&
                PCOL(PCI(pc->next)->pcop)->lit == 0 &&
                PCI(pc->next->next)->op == POC_BTSZ &&
                PCI(pc->next->next)->label == NULL &&
                PCI(pc->next->next->next) &&
                PCI(pc->next->next->next)->label == NULL &&
                PCI(pc->next->next->next)->isSkip &&
                PCI(pc->next->next->next)->pcop &&
                PCI(pc->next->next->next)->pcop->type == PO_GPR_TEMP &&
                PCI(pc->next->next->next)->pcop->strHash == 3381733926591119507ULL // STK00

            )
            {
                // fprintf(stderr,"IORL 0 + skip ..\n");
                HYA_OPCODE nousew = POC_NOP;

                pc1 = pc->next;             // IORL 0
                pc2 = pc->next->next;       // BTSZ
                pc3 = pc->next->next->next; // INSUZW/DCSUZW ...

                switch (PCI(pc3)->op)
                {
                case POC_INSUZW:
                    nousew = POC_INSZ;
                    break;
                case POC_INSZW:
                    nousew = POC_INSUZ;
                    break;
                case POC_DCSZW:
                    nousew = POC_DCSUZ;
                    break;
                case POC_DCSUZW:
                    nousew = POC_DCSZ;
                    break;
                default:
                    nousew = POC_NOP;
                }
                if (nousew != POC_NOP)
                {
                    pCodeReplace(pc1, newpCode(nousew, pCodeOpCopy(PCI(pc3)->pcop)));
                    pCodeReplace(pc2, newpCode(POC_TFSZ, pCodeOpCopy((pCodeOp *)&pc_wreg)));
                    removepCode(&pc3);
                    count++;
                    removed_count++;
                    if (removed_count >= HY08A_options.ob_count)
                    {
                        options.nopeep = 1;
                        return count;
                    }
                    continue;
                }
            }

            // 2020 call iorl 0 btss z
            // volatile free
            // 2022 it no need to have call
            if ((!isPCI(pc->prev) || !(PCI(pc->prev)->isSkip)) &&
                // PCI(pc)->op == POC_CALL &&
                PCI(pc)->op == POC_IORL &&
                PCI(pc)->pcop->type == PO_LITERAL &&
                PCOL(PCI(pc)->pcop)->lit == 0 &&
                PCI(pc->next)->op == POC_BTSS &&
                (PCI(pc->next)->inCond & PCC_Z))
            {
                // change to tfsz _WREG
                pc1 = pc->prev;
                pCodeReplace(pc, newpCode(POC_TFSZ, pCodeOpCopy(PCOP(&pc_wreg))));
                pc = pc1->next->next;
                removepCode(&pc);

                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                continue;
            }

            // 2020 MVL + IORL  very special cases
            // volatile free
            if ((!isPCI(pc->prev) || !(PCI(pc->prev)->isSkip)) &&
                PCI(pc)->op == POC_MVL &&
                (PCI(pc->next)->label == NULL) &&
                (PCI(pc->next)->op == POC_IORL) &&
                PCI(pc)->pcop->type == PO_LITERAL &&
                PCI(pc->next)->pcop->type == PO_LITERAL &&
                PCI(pc->next->next)->isSkip)
            {
                int val = PCOL(PCI(pc)->pcop)->lit | PCOL(PCI(pc->next)->pcop)->lit;
                if (PCI(pc->next->next)->op == POC_BTSS && (PCI(pc->next->next)->inCond & PCC_Z))
                {
                    pCode *pc4;
                    pc1 = pc->next;
                    pc2 = pc->next->next;
                    pc3 = pc->next->next->next;
                    pc4 = pc->next->next->next->next;

                    removepCode(&pc);
                    removepCode(&pc1);
                    removepCode(&pc2);
                    if (val & 0xff) // not zero, it must not skip
                    {
                        pc = pc3;
                    }
                    else
                    {
                        removepCode(&pc3);
                        pc = pc4;
                    }
                    count++;
                    removed_count++;
                    if (removed_count >= HY08A_options.ob_count)
                    {
                        options.nopeep = 1;
                        return count;
                    } // it can proceed
                    continue;
                }
                if (PCI(pc->next->next)->op == POC_BTSZ && (PCI(pc->next->next)->inCond & PCC_Z))
                {
                    pCode *pc4;
                    pc1 = pc->next;
                    pc2 = pc->next->next;
                    pc3 = pc->next->next->next;
                    pc4 = pc->next->next->next->next;

                    removepCode(&pc);
                    removepCode(&pc1);
                    removepCode(&pc2);
                    if (!(val & 0xff)) // not zero, it must not skip
                    {
                        pc = pc3;
                    }
                    else
                    {
                        removepCode(&pc3);
                        pc = pc4;
                    }
                    count++;
                    removed_count++;
                    if (removed_count >= HY08A_options.ob_count)
                    {
                        options.nopeep = 1;
                        return count;
                    } // it can proceed
                    continue;
                }
            }

            // 2020 special special case mvwf+mvwf+dcszw/insuzw
            // if inc/dec it should be volatile OK
            if ((!isPCI(pc->prev) || !(PCI(pc->prev)->isSkip)) &&
                PCI(pc)->op == POC_MVWF &&
                PCI(pc->next)->op == POC_MVWF &&
                PCI(pc->next->next)->pcop &&
                PCI(pc->next->next)->label == NULL &&
                PCI(pc->next)->label == NULL &&
                pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next->next)->pcop) &&
                !(PCI(pc->next->next)->isLit) &&
                !(PCI(pc->next->next)->op == POC_MVFW) &&
                !(PCI(pc->next->next)->op == POC_ARRCFW) &&
                !(PCI(pc->next->next)->op == POC_RRCFW) &&
                !(PCI(pc->next->next)->outCond & PCC_REGISTER))
            {
                // move to W
                pCodeReplace(pc->next->next, newpCode(PCI(pc->next->next)->op, pCodeOpCopy(PCOP(&pc_wreg))));
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                } // it can proceed
            }

            // 2020 special case after optimizaton (testxor1)
            // limited source, volatile ok
            if (PCI(pc->next)->op == POC_MVFF && (PCI(pc->next->next)->inCond & PCC_REGISTER) &&
                (PCI(pc->next->next)->outCond & PCC_W) && PCI(pc->next)->label == NULL &&
                PCI(pc->next->next)->label == NULL &&
                pCodeOpCompare(PCI(pc->next)->pcop2, PCI(pc->next->next)->pcop) &&
                !PCI(pc->next)->pcop->flags.isX &&
                (PCI(pc->next)->pcop->type == PO_GPR_REGISTER ||
                 PCI(pc->next)->pcop->type == PO_GPR_TEMP))
            {
                // just change the pcop
                pCodeReplace(pc->next->next, newpCode(PCI(pc->next->next)->op, PCI(pc->next)->pcop));
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                } // it can proceed
            }

            // 2020 mvwf a comf a mvfw a ==> XORL 0xff mvwf a
            // can comf should be volatile ok
            if ((!isPCI(pc->prev) || !PCI(pc->prev)->isSkip) &&
                PCI(pc)->op == POC_MVWF &&
                (PCI(pc->next)->op == POC_COMF || PCI(pc->next)->op == POC_INF || PCI(pc->next)->op == POC_DECF) &&
                PCI(pc->next->next)->op == POC_MVFW &&
                PCI(pc->next)->label == NULL &&
                PCI(pc->next->next)->label == NULL &&
                pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next)->pcop) &&
                pCodeOpCompare(PCI(pc->next)->pcop, PCI(pc->next->next)->pcop))
            {
                if (PCI(pc->next)->op == POC_COMF)
                    pc1 = newpCode(POC_XORLW, newpCodeOpLit(0xff));
                else if (PCI(pc->next)->op == POC_INF)
                    pc1 = newpCode(POC_ADDLW, newpCodeOpLit(0x01));
                else // DECF
                    pc1 = newpCode(POC_ADDLW, newpCodeOpLit(0xff));
                pc2 = pc->next->next;
                pCodeReplace(pc, pc1);
                pc = pc1;
                pCodeReplace(pc->next, newpCode(POC_MVWF, PCI(pc->next)->pcop));

                removepCode(&pc2);
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                continue; // it cannot proceed
            }

            // 2020
            // MVWF a + decf b/inf b + mvl + iorf/andf/xorf a==> remove mvl and change to iorf/andf/xorf
            // volatile ok
            if (PCI(pc)->op == POC_MVWF && (PCI(pc->next)->op == POC_INF || PCI(pc->next)->op == POC_DECF) && PCI(pc->next->next)->op == POC_MVL &&
                PCI(pc->next)->label == NULL && PCI(pc->next->next)->label == NULL && PCI(pc->next->next->next)->label == NULL &&

                isPCI(pc->next->next->next) && canInstrOpReplaceByLit(PCI(pc->next->next->next)->op) &&
                !pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next)->pcop) && // must be different
                pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next->next->next)->pcop))
            {
                pc1 = pc->next->next->next;
                // if (opcode == POC_MULWF || opcode == POC_ANDFW || opcode == POC_IORFW || opcode == POC_XORFW || opcode == POC_MVFW)
                pc2 = NULL;
                switch (PCI(pc->next->next->next)->op)
                {
                case POC_MULWF:
                    pc2 = newpCode(POC_MULLW, PCI(pc->next->next)->pcop);
                    break;
                case POC_ANDFW:
                    pc2 = newpCode(POC_ANDLW, PCI(pc->next->next)->pcop);
                    break;
                case POC_IORFW:
                    pc2 = newpCode(POC_IORL, PCI(pc->next->next)->pcop);
                    break;
                case POC_MVFW:
                    pc2 = newpCode(POC_MVL, PCI(pc->next->next)->pcop);
                    break;
                case POC_XORFW:
                    pc2 = newpCode(POC_XORLW, PCI(pc->next->next)->pcop);
                    break;

                default:
                    fprintf(stderr, "interrnal error (simpleopt)%s:%d\n", __FILE__, __LINE__);
                    exit(-__LINE__);
                }
                pc3 = pc->next->next->next;
                removepCode(&pc3);
                pCodeReplace(pc->next->next, pc2);
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                } // it can proceed
            }

            // 2020 mvwf a inf/decf b rrf a ==> change rrfa to IORL 0
            // volatile ok
            if (PCI(pc)->op == POC_MVWF && (PCI(pc->next)->op == POC_INF || PCI(pc->next)->op == POC_DECF) &&
                PCI(pc->next)->label == NULL && PCI(pc->next->next)->label == NULL &&

                PCI(pc->next->next)->op == POC_RRFW && chkNoUseW(pc->next->next->next, 0) &&
                pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next->next)->pcop) &&
                !pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next)->pcop)

            )
            {
                pCodeReplace(pc->next->next, newpCode(POC_IORL, newpCodeOpLit(0)));
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                // it can proceed forward
            }

            // 2020  MVF x DECF x IORL 0 JNZ(while(x--))
            // ==> DCF + INSZ/INSUZ
            // DEC ==> not volatile, volatile ok

            if (PCI(pc)->op == POC_MVFW && PCI(pc->next)->op == POC_DECF && PCI(pc->next->next)->op == POC_IORL &&
                isPCI(pc->prev) && !PCI(pc->prev)->isSkip &&
                PCI(pc->next->next)->pcop->type == PO_LITERAL &&
                pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next)->pcop) &&
                PCI(pc->next)->label == NULL &&
                PCI(pc->next->next)->label == NULL &&
                isPCI(pc->next->next->next) &&
                (PCI(pc->next->next->next)->op == POC_BTSS || PCI(pc->next->next->next)->op == POC_BTSZ) &&
                (PCI(pc->next->next->next)->inCond & PCC_Z) &&
                PCOL(PCI(pc->next->next)->pcop)->lit == 0 && chkNoUseW(pc->next->next->next, 0))
            {
                pCode *pcp = pc->prev;
                pc1 = pc->next;
                pc2 = pc->next->next;
                pc3 = pc->next->next->next;

                pCodeReplace(pc, newpCode(POC_DECF, PCI(pc)->pcop));
                if (PCI(pc3)->op == POC_BTSS) // skip if z ==> change to INSZ
                    pCodeReplace(pc1, newpCode(POC_INSZW, PCI(pc)->pcop));
                else
                    pCodeReplace(pc1, newpCode(POC_INSUZW, PCI(pc)->pcop));
                removepCode(&pc2);
                removepCode(&pc3);
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                pc = pcp;
                continue; // prevent to end, re-check
            }

            // INF IORL ==>volatile ok because INF
            if (PCI(pc)->op == POC_MVFW && PCI(pc->next)->op == POC_INF && PCI(pc->next->next)->op == POC_IORL &&
                isPCI(pc->prev) && !PCI(pc->prev)->isSkip &&
                PCI(pc->next->next)->pcop->type == PO_LITERAL &&
                pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next)->pcop) &&
                PCI(pc->next)->label == NULL &&
                PCI(pc->next->next)->label == NULL &&
                isPCI(pc->next->next->next) &&
                (PCI(pc->next->next->next)->op == POC_BTSS || PCI(pc->next->next->next)->op == POC_BTSZ) &&
                // PCI(pc->next->next->next)->pcop->type == PO_GPR_BIT &&
                // PCORB(PCI(pc->next->next->next)->pcop)->pcor.r->pc_type == PO_STATUS &&
                // PCORB(PCI(pc->next->next->next)->pcop)->bit == 0 &&
                (PCI(pc->next->next->next)->inCond & PCC_Z) &&
                PCOL(PCI(pc->next->next)->pcop)->lit == 0 && chkNoUseW(pc->next->next->next, 0))
            {
                pCode *pcp = pc->prev;
                pc1 = pc->next;
                pc2 = pc->next->next;
                pc3 = pc->next->next->next;

                pCodeReplace(pc, newpCode(POC_INF, PCI(pc)->pcop));
                if (PCI(pc3)->op == POC_BTSS) // skip if z ==> change to INSZ
                    pCodeReplace(pc1, newpCode(POC_DCSZW, PCI(pc)->pcop));
                else
                    pCodeReplace(pc1, newpCode(POC_DCSUZW, PCI(pc)->pcop));
                removepCode(&pc2);
                removepCode(&pc3);
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                pc = pcp;
                continue; // prevent to end, re-check
            }

            // DECF + RRFW + JNZ/JZ -> DCSZ/DCSUZ volatile ok because DECF
            if (PCI(pc)->op == POC_DECF && PCI(pc->next)->op == POC_RRFW &&
                isPCI(pc->prev) && !PCI(pc->prev)->isSkip &&
                pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next)->pcop) &&
                PCI(pc->next)->label == NULL &&
                PCI(pc->next->next)->label == NULL &&
                (PCI(pc->next->next)->op == POC_BTSS || PCI(pc->next->next)->op == POC_BTSZ) &&
                // PCI(pc->next->next)->pcop->type == PO_GPR_BIT &&
                // PCORB(PCI(pc->next->next)->pcop)->pcor.r->pc_type == PO_STATUS &&
                // PCORB(PCI(pc->next->next)->pcop)->bit == 0
                (PCI(pc->next->next)->inCond & PCC_Z))
            {
                pCode *pcp = pc->prev;
                pc1 = pc->next;
                pc2 = pc->next->next;

                if (PCI(pc2)->op == POC_BTSS) // skip if z ==> change to INSZ
                    pCodeReplace(pc, newpCode(POC_DCSZ, PCI(pc)->pcop));
                else
                    pCodeReplace(pc, newpCode(POC_DCSUZ, PCI(pc)->pcop));
                removepCode(&pc1);
                removepCode(&pc2);
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                pc = pcp;
                continue; // prevent to end, re-check
            }

            // INF + RRFW + JNZ/JZ -> INSZ/INSUZ, RRF is volatile free
            if (PCI(pc)->op == POC_INF && PCI(pc->next)->op == POC_RRFW &&
                isPCI(pc->prev) && !PCI(pc->prev)->isSkip &&
                pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next)->pcop) &&
                PCI(pc->next)->label == NULL &&
                PCI(pc->next->next)->label == NULL &&
                (PCI(pc->next->next)->op == POC_BTSS || PCI(pc->next->next)->op == POC_BTSZ) &&
                // PCI(pc->next->next)->pcop->type == PO_GPR_BIT &&
                // PCORB(PCI(pc->next->next)->pcop)->pcor.r->pc_type == PO_STATUS &&
                // PCORB(PCI(pc->next->next)->pcop)->bit == 0
                (PCI(pc->next->next)->inCond & PCC_Z))
            {
                pCode *pcp = pc->prev;
                pc1 = pc->next;
                pc2 = pc->next->next;

                if (PCI(pc2)->op == POC_BTSS) // skip if z ==> change to INSZ
                    pCodeReplace(pc, newpCode(POC_INSZ, PCI(pc)->pcop));
                else
                    pCodeReplace(pc, newpCode(POC_INSUZ, PCI(pc)->pcop));
                removepCode(&pc1);
                removepCode(&pc2);
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                pc = pcp;
                continue; // prevent to end, re-check
            }

            // movfw iorlw 0... or mvl 0 iorfw volatile free
            if (
                ((!isPCI(pc->prev) || !PCI(pc->prev)->isSkip)) &&
                    ((PCI(pc)->op == POC_MVFW &&
                      PCI(pc->next)->label == NULL &&
                      PCI(pc->next)->op == POC_IORL &&
                      PCI(pc->next)->pcop->type == PO_LITERAL &&
                      PCOL(PCI(pc->next)->pcop)->lit == 0)) ||
                (PCI(pc)->op == POC_MVL &&
                 PCI(pc->next)->op == POC_IORFW &&
                 PCI(pc)->pcop->type == PO_LITERAL &&
                 PCOL(PCI(pc)->pcop)->lit == 0))
            {
                // possible TFSZ
                pCodeOp *pcop = (PCI(pc)->op == POC_MVFW) ? PCI(pc)->pcop : PCI(pc->next)->pcop;
                if (PCI(pc->next->next)->op == POC_BTSS && (PCI(pc->next->next)->inCond & PCC_Z) &&
                    chkNoUseW(pc->next->next, 0))
                {
                    pCode *pc1 = newpCode(POC_TFSZ, pcop);
                    pCodeReplace(pc, pc1);
                    pc = pc1;
                    pc1 = pc->next;
                    pc2 = pc->next->next;
                    removepCode(&pc1);
                    removepCode(&pc2);
                    count++;
                    removed_count++;
                    if (removed_count >= HY08A_options.ob_count)
                    {
                        options.nopeep = 1;
                        return count;
                    }
                    continue; // prevent to end, re-check
                }

                if (
                    (PCI(pc->next->next)->inCond & PCC_Z) &&
                    chkNoUseW(pc->next->next, 0))
                {
                    pc1 = newpCode(POC_RRFW, pcop);
                    pCodeReplace(pc, pc1);
                    pc = pc1;
                    pc1 = pc->next;
                    removepCode(&pc1);
                    count++;
                    removed_count++;
                    if (removed_count >= HY08A_options.ob_count)
                    {
                        options.nopeep = 1;
                        return count;
                    }
                    continue; // prevent to end, re-check
                }
            }

            // promote decf

            if (PCI(pc)->op == POC_MVWF &&
                (PCI(pc->next)->op == POC_DECF || PCI(pc->next)->op == POC_INF ||
                 PCI(pc->next)->op == POC_COMF) &&
                PCI(pc->next->next)->op == POC_MVFW && PCI(pc->next)->label == NULL &&
                PCI(pc->next->next)->label == NULL &&
                pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next->next)->pcop) &&
                !pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next)->pcop))
            {
                // promte MVFW
                pc1 = pc->next;
                pc2 = pc->next->next;
                pc3 = pc->next->next->next;

                pc->next = pc2;
                pc2->next = pc1;
                pc1->next = pc3;

                pc3->prev = pc1;
                pc1->prev = pc2;
                pc2->prev = pc;
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                // continue; it can proceed
            }

            // redundant RRF _WREG

            if ((PCI(pc->next)->outCond & PCC_Z) && PCI(pc->next->next)->op == POC_RRFW && PCI(pc->next->next)->pcop->type == PO_W //  only when RRFW _WREG
                && !PCI(pc->next)->isBranch && PCI(pc->next->next)->label == NULL && isPCI(pc->next->next->next) && isPCI_SKIP(pc->next->next->next) && (PCI(pc->next->next->next)->inCond & PCC_Z))
            {
                pc1 = pc->next->next;
                removepCode(&pc1);
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                continue;
            }

            // mvfw + xorlw + btfss Z
            // don't change if following is MVFW+ IORF
            // volatile free

            if (PCI(pc)->op == POC_MVFW && PCI(pc)->pcop->type != PO_PLUSW &&
                PCI(pc->next)->op == POC_XORLW &&
                PCI(pc->next)->label == NULL &&
                PCI(pc->next->next)->label == NULL &&
                PCI(pc->next->next)->op == POC_BTSS &&
                // PCI(pc->next->next)->pcop->type == PO_GPR_BIT &&
                PCI(pc->next->next)->label == NULL &&
                // PCORB(PCI(pc->next->next)->pcop)->pcor.r->pc_type == PO_STATUS &&
                // PCORB(PCI(pc->next->next)->pcop)->bit == 0  &&
                (PCI(pc->next->next)->inCond & PCC_Z) &&
                // prevent dual JMP
                !(pc->next->next->next && pc->next->next->next->next &&
                  isPCI(pc->next->next->next) && isPCI(pc->next->next->next->next) &&
                  PCI(pc->next->next->next)->op == POC_JMP &&
                  PCI(pc->next->next->next->next)->op == POC_JMP) &&
                !(pc->next->next->next && pc->next->next->next->next && pc->next->next->next->next->next && isPCI(pc->next->next->next) &&
                  isPCI(pc->next->next->next->next) &&
                  isPCI(pc->next->next->next->next->next) &&
                  PCI(pc->next->next->next)->op == POC_JMP &&
                  PCI(pc->next->next->next->next)->op == POC_MVFW &&
                  PCI(pc->next->next->next->next->next)->op == POC_IORFW)

            )
            {
                pCodeOp *tmp = PCI(pc)->pcop;

                /*if (removed_count >= 117)
                {
                    fprintf(stderr, "118\n");
                }*/
                pc1 = pc->next;
                pc2 = pc->next->next;
                pCodeReplace(pc, newpCode(POC_MVL, PCI(pc->next)->pcop));
                pc = pc1->prev;
                pCodeReplace(pc1, newpCode(POC_CPSE, tmp));

                removepCode(&pc2);
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                continue;
            }

            // xorfw + btfss
            // volatile free
            if (PCI(pc->next)->op == POC_XORFW &&
                PCI(pc->next->next)->op == POC_BTSS &&
                // PCI(pc->next->next)->pcop->type == PO_GPR_BIT &&
                PCI(pc->next->next)->label == NULL &&
                // PCORB(PCI(pc->next->next)->pcop)->pcor.r->pc_type == PO_STATUS &&
                // PCORB(PCI(pc->next->next)->pcop)->bit == 0
                (PCI(pc->next->next)->inCond & PCC_Z))
            {
                pCodeReplace(pc->next, newpCode(POC_CPSE, PCI(pc->next)->pcop));
                pc1 = pc->next->next;
                removepCode(&pc1);
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                continue;
            }

            // jmp ret (volatile free)
            if (PCI(pc->next)->op == POC_JMP &&
                (PCI(pc->next->next)->op == POC_RET || PCI(pc->next->next)->op == POC_RETV) && PCI(pc->next->next)->label == NULL)
            {
                pc1 = pc->next->next;
                removepCode(&pc1);
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                continue;
            }

            // mvl ret volatile free

            if (PCI(pc->next)->op == POC_MVL && PCI(pc->next->next)->op == POC_RET &&
                PCI(pc->next->next)->label == NULL) // cannot label at ret
            {
                pc1 = pc->next;
                pc2 = pc->next->next;
                pCodeReplace(pc2, newpCode(POC_RETL, PCI(pc1)->pcop));
                removepCode(&pc1);
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                continue;
            }

            // MVL TFSZ W volatile free
            if (PCI(pc->next)->op == POC_MVL && PCI(pc->next)->pcop->type == PO_LITERAL &&
                PCI(pc->next->next)->label == NULL &&
                PCI(pc->next->next)->op == POC_TFSZ && PCI(pc->next->next)->pcop->type == PO_W && PCI(pc->next->next)->label == NULL)
            {
                if (PCOL(PCI(pc->next)->pcop)->lit == 0) // W is 0, must skip
                {
                    if (PCI(pc->next->next->next)->label) // there is a label
                    {
                        if (PCI(pc->next->next->next->next)->label == NULL)
                        {
                            pCodeLabel *pcl = PCL(newpCodeLabel(NULL, newiTempLabel(NULL)->key + 100));
                            PCI(pc->next->next->next->next)->label = (pBranch *)Safe_calloc(1, sizeof(pBranch));
                            PCI(pc->next->next->next->next)->label->pc = (struct pCode *)pcl;

                            pb->labelIndexTab[pb->labelTabSize] = pcl->key;
                            pb->labelPtrTab[pb->labelTabSize++] = (pCode *)pcl;
                        }
                        pCodeReplace(pc->next, newpCode(POC_JMP, newpCodeOpLabel(NULL, ((pCodeLabel *)PCI(pc->next->next->next->next)->label->pc)->key)));
                        pc1 = pc->next->next;
                        removepCode(&pc1);
                    }
                    else
                    {
                        // remove all
                        pc1 = pc->next;
                        pc2 = pc->next->next;
                        pc3 = pc->next->next->next;
                        removepCode(&pc1);
                        removepCode(&pc2);
                        removepCode(&pc3);
                    }
                }
                else
                {
                    pc1 = pc->next;
                    pc2 = pc->next->next;
                    removepCode(&pc1);
                    removepCode(&pc2);
                }
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                continue;
            }

            // mvl xorl (volatile free)
            if (
                PCI(pc->next)->op == POC_MVL && PCI(pc->next)->pcop->type == PO_LITERAL &&
                PCI(pc->next->next)->label == NULL &&
                PCI(pc->next->next)->op == POC_XORLW && PCI(pc->next->next)->pcop->type == PO_LITERAL)
            {
                int a = PCOL(PCI(pc->next)->pcop)->lit;
                int b = PCOL(PCI(pc->next->next)->pcop)->lit;
                PCI(pc->next)->pcop = newpCodeOpLit(a ^ b);
                // special case if following is SUBC
                if (pc->next->next->next && isPCI(pc->next->next->next) &&
                    !PCI(pc->next->next->next)->isSkip &&
                    (PCI(pc->next->next->next)->outCond & PCC_Z))
                {
                    pc2 = pc->next->next;
                    removepCode(&pc2);
                }
                else
                {
                    pCodeReplace(pc->next->next, newpCode(POC_IORL, newpCodeOpLit(0)));
                }
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
            }

            if ((PCI(pc->next)->op == POC_COMFW || PCI(pc->next)->op == POC_ARRCFW || PCI(pc->next)->op == POC_RRCFW ||
                 PCI(pc->next)->op == POC_RLCFW || PCI(pc->next)->op == POC_RLFW || PCI(pc->next)->op == POC_RRFW) &&
                PCI(pc->next->next)->op == POC_MVWF &&
                PCI(pc->next->next)->label == NULL &&
                pCodeOpCompare(PCI(pc->next)->pcop, PCI(pc->next->next)->pcop))
            {
                switch (PCI(pc->next)->op)
                {
                case POC_COMFW:
                    pCodeReplace(pc->next, newpCode(POC_COMF, PCI(pc->next)->pcop));
                    break;
                case POC_ARRCFW:
                    pCodeReplace(pc->next, newpCode(POC_ARRCF, PCI(pc->next)->pcop));
                    break;
                case POC_RRCFW:
                    pCodeReplace(pc->next, newpCode(POC_RRCF, PCI(pc->next)->pcop));
                    break;
                case POC_RLCFW:
                    pCodeReplace(pc->next, newpCode(POC_RLCF, PCI(pc->next)->pcop));
                    break;
                case POC_RLFW:
                    pCodeReplace(pc->next, newpCode(POC_RLF, PCI(pc->next)->pcop));
                    break;
                case POC_RRFW:
                    pCodeReplace(pc->next, newpCode(POC_RRF, PCI(pc->next)->pcop));
                    break;
                default:
                    break;
                }
                pCodeReplace(pc->next->next, newpCode(POC_MVFW, PCI(pc->next->next)->pcop));
                // no need continue
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
            }

            // mvfw + mvfw, skip middle one
            // it is doubled

            /*if (PCI(pc->next)->op == POC_MVFW &&
                PCI(pc->next->next)->label == NULL &&
                PCI(pc->next->next)->op == POC_MVFW)
            {
                pc1 = pc->next;
                removepCode(&pc1);
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                continue;
            }*/

            // movlw 0 + cpse ==> tfsz, volatile free
            if (PCI(pc->next)->op == POC_MVL && PCI(pc->next)->pcop->type == PO_LITERAL &&
                PCOL(PCI(pc->next)->pcop)->lit == 0 &&
                PCI(pc->next->next)->label == NULL &&
                PCI(pc->next->next)->op == POC_CPSE)
            {
                pc2 = pc->next->next;
                pc1 = pc->next;
                pCodeReplace(pc2, newpCode(POC_TFSZ, PCI(pc2)->pcop));
                removepCode(&pc1);
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
            }

            // no skip + movwf + movfw
            // this is volatile dangerous
            if (PCI(pc->next)->op == POC_MVWF &&
                PCI(pc->next->next)->op == POC_MVFW &&
                PCI(pc->next->next)->label == NULL &&
                !(PCI(pc->next)->pcop->type == PO_SFR_REGISTER ||
                  (PCI(pc->next)->pcop->type == PO_DIR && PCOR(PCI(pc->next)->pcop)->r && PCOR(PCI(pc->next)->pcop)->r->isVolatile) ||
                  (PCI(pc->next)->pcop->type == PO_IMMEDIATE && PCOI(PCI(pc->next)->pcop)->r && PCOI(PCI(pc->next)->pcop)->r->isVolatile)) &&
                pCodeOpCompare(PCI(pc->next)->pcop, PCI(pc->next->next)->pcop))
            {
                // if(removed_count==123)
                // pCodeOpCompare(PCI(pc->next)->pcop, PCI(pc->next->next)->pcop);
                pc1 = pc->next->next;
                removepCode(&pc1);
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                continue;
            }
            // IORL 0, volatile free
            if ((PCI(pc->next)->outCond & (PCC_Z | PCC_W)) == (PCC_Z | PCC_W) &&
                !PCI(pc->next)->isSkip &&
                !PCI(pc->next)->isBranch &&
                PCI(pc->next->next)->label == NULL &&
                PCI(pc->next->next)->op == POC_IORL &&
                PCI(pc->next->next)->pcop->type == PO_LITERAL &&
                PCOL(PCI(pc->next->next)->pcop)->lit == 0)
            {
                pc1 = pc->next->next;
                removepCode(&pc1);
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                continue;
            }

            // movlw 0 iorfw, volatile free
            if (!isPCI_SKIP(pc->next->next) &&
                PCI(pc)->op == POC_MVL && PCI(pc)->pcop->type == PO_LITERAL &&
                PCOL(PCI(pc)->pcop)->lit == 0 && PCI(pc->next)->label == NULL &&
                PCI(pc->next)->op == POC_IORFW)
            {

                pCode *pc1 = pc->next;
                pCodeReplace(pc->next, newpCode(POC_MVFW, PCI(pc->next)->pcop));
                removepCode(&pc);
                pc = pc1;
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                continue;
            }

            // change movlw iorfw to movfw iorlw, where IORLW have more optimizations
            // volatile free
            if (PCI(pc->next)->op == POC_MVL &&
                (PCI(pc->next->next)->op == POC_IORFW || PCI(pc->next->next)->op == POC_XORFW ||
                 PCI(pc->next->next)->op == POC_ANDFW || PCI(pc->next->next)->op == POC_ADDFW) &&
                PCI(pc->next->next)->label == NULL)
            {
                pc1 = pc->next;
                pc2 = pc->next->next;
                pCodeReplace(pc->next, newpCode(POC_MVFW, PCI(pc2)->pcop));
                switch (PCI(pc->next->next)->op)
                {
                case POC_IORFW:
                    pCodeReplace(pc->next->next, newpCode(POC_IORL, PCI(pc1)->pcop));
                    break;
                case POC_XORFW:
                    pCodeReplace(pc->next->next, newpCode(POC_XORLW, PCI(pc1)->pcop));
                    break;
                case POC_ANDFW:
                    pCodeReplace(pc->next->next, newpCode(POC_ANDLW, PCI(pc1)->pcop));
                    break;
                case POC_ADDFW:
                    pCodeReplace(pc->next->next, newpCode(POC_ADDLW, PCI(pc1)->pcop));
                    break;
                default:
                    break;
                }

                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                } // further opt is ok
            }

            if ((!(pc->prev) || !isPCI(pc->prev) || !PCI(pc->prev)->isSkip))
            {

                // mvl mvwf mvl mvwf ... remove same mvl, volatile free

                if (PCI(pc)->op == POC_MVL && PCI(pc)->pcop->type == PO_LITERAL && PCI(pc->next)->op == POC_MVWF && PCI(pc->next)->label == NULL)
                {
                    int lit0;
                    lit0 = PCOL(PCI(pc)->pcop)->lit;
                    for (pc1 = pc->next->next; pc1 && isPCI(pc1) && (PCI(pc1)->op == POC_MVL || PCI(pc1)->op == POC_MVWF) && PCI(pc1)->label == NULL; pc1 = pc1->next)
                    {
                        if (PCI(pc1)->op == POC_MVWF)
                            continue;
                        if (PCI(pc1)->pcop->type == PO_LITERAL && PCOL(PCI(pc1)->pcop)->lit == lit0)
                        {
                            removepCode(&pc1);
                            count++;
                            removed_count++;
                            if (removed_count >= HY08A_options.ob_count)
                            {
                                options.nopeep = 1;
                                return count;
                            }
                            continue;
                        }
                        break;
                    }
                }

                // 2019 JUNE
                // MVL+ADDL+CPSG/CPSL, volatile free
                if (PCI(pc)->op == POC_MVL && PCI(pc->next)->op == POC_ADDLW &&
                    PCI(pc->next)->label == NULL && PCI(pc->next->next)->label == NULL && (PCI(pc->next->next)->op == POC_CPSL || PCI(pc->next->next)->op == POC_CPSG) && PCI(pc->next)->label == NULL && PCI(pc->next->next)->label == NULL && PCI(pc)->pcop->type == PO_LITERAL && PCI(pc->next)->pcop->type == PO_LITERAL)
                {
                    int lit0 = PCOL(PCI(pc)->pcop)->lit;
                    int lit1 = PCOL(PCI(pc->next)->pcop)->lit;
                    PCI(pc)->pcop = newpCodeOpLit((lit0 + lit1) & 0xff);
                    pc1 = pc->next;
                    removepCode(&pc1);
                    count++;
                    removed_count++;
                    if (removed_count >= HY08A_options.ob_count)
                    {
                        options.nopeep = 1;
                        return count;
                    }
                    continue;
                }

                // 2019 JUNE
                // MVL A ADDFW B BTSZ C ==> B<(256-A) will skip,
                // MVL A ADDFW B BTSS C ==> B>=(256-A) will skip, or b>255-A will skip volatile free
                // or MVFW A ADDL B BTSZ C
                if (((PCI(pc)->op == POC_MVL && PCI(pc->next)->op == POC_ADDFW && PCI(pc)->pcop->type == PO_LITERAL) ||
                     (PCI(pc)->op == POC_MVFW && PCI(pc->next)->op == POC_ADDLW && PCI(pc->next)->pcop->type == PO_LITERAL)) &&
                    (PCI(pc->next->next)->op == POC_BTSZ || PCI(pc->next->next)->op == POC_BTSS) &&
                    PCI(pc->next)->label == NULL && PCI(pc->next->next)->label == NULL &&
                    (PCI(pc->next->next)->inCond & PCC_C) && chkNoUseW(pc->next->next->next, 0) // 2021 new optimization may use W later
                )
                {
                    int lit32;
                    pCodeOp *pcop;
                    if (PCI(pc)->op == POC_MVL)
                    {
                        lit32 = 256 - PCOL(PCI(pc)->pcop)->lit;
                        pcop = PCI(pc->next)->pcop;
                    }
                    else
                    {
                        lit32 = 256 - PCOL(PCI(pc->next)->pcop)->lit;
                        pcop = PCI(pc)->pcop;
                    }
                    if (PCI(pc->next->next)->op == POC_BTSS)
                        lit32--;

                    pc1 = newpCode(POC_MVL, newpCodeOpLit(lit32));
                    pCodeReplace(pc, pc1);
                    pc = pc1;

                    if (PCI(pc->next->next)->op == POC_BTSZ)
                        pc1 = newpCode(POC_CPSL, pcop);
                    else
                        pc1 = newpCode(POC_CPSG, pcop);
                    pCodeReplace(pc->next, pc1);
                    pc2 = pc->next->next;
                    removepCode(&pc2);

                    count++;
                    removed_count++;
                    if (removed_count >= HY08A_options.ob_count)
                    {
                        options.nopeep = 1;
                        return count;
                    }
                    continue;
                }
            }
            // 2022 SEP MVWF a MVL k CPSG/CPSL a ==>mvwf a ; mvwf a; ADDL (256-k) BTSS/BTSZ, volatile free
            if (PCI(pc)->op == POC_MVWF && PCI(pc->next)->op == POC_MVL && PCI(pc->next)->pcop->type == PO_LITERAL &&
                (PCI(pc->next->next)->op == POC_CPSG || PCI(pc->next->next)->op == POC_CPSL) &&
                PCI(pc->next)->label == NULL && PCI(pc->next->next)->label == NULL &&
                pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next->next)->pcop))
            {
                int k = PCOL(PCI(pc->next)->pcop)->lit;
                // consider MVL 10, CPSG X, means skip if x>10, skip if x + 245 >= 256
                // consider MVL 10, CPSL X, means skip if x<10, x + 246 < 256
                pCodeReplace(pc->next, newpCode(POC_ADDLW, newpCodeOpLit((PCI(pc->next->next)->op == POC_CPSG)
                                                                             ? 255 - k
                                                                             : 256 - k)));
                pCodeReplace(pc->next->next, newpCode((PCI(pc->next->next)->op == POC_CPSG) ? POC_BTSS : POC_BTSZ,
                                                      STATUSCBIT));
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                continue;
            }
            // double ANDL/ORL
            if (PCI(pc->next)->op == POC_ANDLW && PCI(pc->next->next)->op == POC_ANDLW && PCI(pc->next->next)->label == NULL && PCI(pc->next)->pcop->type == PO_LITERAL &&
                PCI(pc->next->next)->pcop->type == PO_LITERAL)
            {
                int k = PCOL(PCI(pc->next)->pcop)->lit;
                int k2 = PCOL(PCI(pc->next->next)->pcop)->lit;
                pCode *pc3 = pc->next->next;
                removepCode(&pc3);
                PCI(pc->next)->pcop = newpCodeOpLit(k & k2);
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                continue;
            }
            if (PCI(pc->next)->op == POC_IORL && PCI(pc->next->next)->op == POC_IORL && PCI(pc->next->next)->label == NULL && PCI(pc->next)->pcop->type == PO_LITERAL &&
                PCI(pc->next->next)->pcop->type == PO_LITERAL)
            {
                int k = PCOL(PCI(pc->next)->pcop)->lit;
                int k2 = PCOL(PCI(pc->next->next)->pcop)->lit;
                pCode *pc3 = pc->next->next;
                removepCode(&pc3);
                PCI(pc->next)->pcop = newpCodeOpLit(k | k2);
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                continue;
            }
            // 2022 sep, adjust outcond -> incond
            // if(!PCI(pc)->isSkip && PCI(pc->next)->pcop->type==PO_W && PCI(pc->next)->outCond==(PCC_W|PCC_Z) &&
            // (PCI(pc->next->next)->inCond & (PCC_W|PCC_Z))==0 && !PCI(pc->next->next)->isSkip &&
            // !PCI(pc->next->next)->isBranch && (PCI(pc->next->next)->outCond & PCC_W)==0 && PCI(pc->next->next)->label==NULL)
            // {
            //     // swap the 2 instructions
            //     pc1 = pc->next;
            //     pCodeReplace(pc->next, newpCode(PCI(pc->next->next)->op, PCI(pc->next->next)->pcop));
            //     pCodeReplace(pc->next->next, newpCode(PCI(pc1)->op, PCI(pc1)->pcop));
            //     count++;
            //     removed_count++;
            //     if (removed_count >= HY08A_options.ob_count)
            //     {
            //         options.nopeep = 1;
            //         return count;
            //     }
            //     continue;

            // }

            // 2019 june
            // MVFW +DECFW/INCFW WREG should be combined
            // 2021 add RRF
            // volatile free
            if ((!pc->prev || !isPCI(pc->prev) || !PCI(pc->prev)->isSkip) &&
                PCI(pc->next)->label == NULL &&
                PCI(pc)->op == POC_MVFW &&
                (PCI(pc->next)->op == POC_INFW || PCI(pc->next)->op == POC_DECFW ||
                 PCI(pc->next)->op == POC_DCSUZW || PCI(pc->next)->op == POC_DCSZW ||
                 PCI(pc->next)->op == POC_INSUZW || PCI(pc->next)->op == POC_INSZW ||
                 PCI(pc->next)->op == POC_RRFW) &&
                PCI(pc->next)->pcop->type == PO_W)
            {

                // PCI(pc->next->next)->pcop = PCI(pc->next)->pcop;
                pCodeReplace(pc->next, newpCode(PCI(pc->next)->op, PCI(pc)->pcop));

                removepCode(&pc);

                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                continue;
            }
        }

        if (isPCI(pc) && !PCI(pc)->isSkip && pc->next && pc->next->next && pc->next->next->next && isPCI(pc->next) && isPCI(pc->next->next) && isPCI(pc->next->next->next))
        {

            // this optimization has bug, we skip it @ 2021 Oct
            //     // 2019 june CPSL JMP a xxx a: => ADDL 0xff CPSG xxx a:
            //     if ((PCI(pc->next)->op == POC_CPSL || PCI(pc->next)->op == POC_CPSG) && PCI(pc->next->next)->op == POC_JMP && pc->next->next->next->next && isPCI(pc->next->next->next->next) && PCI(pc->next->next->next->next)->label && PCI(pc->next->next)->pcop->type == PO_LABEL && PCOLAB(PCI(pc->next->next)->pcop)->key == PCL(PCI(pc->next->next->next->next)->label->pc)->key)
            //     {
            //         if (PCI(pc->next)->op == POC_CPSL) // <1 skip change to >0 skip
            //         {
            //             pc1 = newpCode(POC_ADDLW, popGetLit(0xff));
            //             pc2 = newpCode(POC_CPSG, PCI(pc->next)->pcop);
            //             pCodeReplace(pc->next, pc1);
            //             pCodeReplace(pc->next->next, pc2);
            //         }
            //         else
            //         {
            //             pc1 = newpCode(POC_ADDLW, popGetLit(0x1));
            //             pc2 = newpCode(POC_CPSL, PCI(pc->next)->pcop);
            //             pCodeReplace(pc->next, pc1);
            //             pCodeReplace(pc->next->next, pc2);
            //         }
            //         count++;
            //         removed_count++;
            //         if (removed_count >= HY08A_options.ob_count)
            //         {
            //             options.nopeep = 1;
            //             return count;
            //         }
            //         continue;
            //     }

            // 2019 june
            // incfw r0/decfw r0+ mvwf r0 + mvl like instruction
            // inc is not volatile, volatile ok

            if (!PCI(pc)->isSkip && (PCI(pc->next)->op == POC_INFW || PCI(pc->next)->op == POC_DECFW
                                     //|| PCI(pc->next)->op == POC_ADCFW
                                     ) &&
                PCI(pc->next->next)->op == POC_MVWF && PCI(pc->next->next)->label == NULL

                && pCodeOpCompare(PCI(pc->next)->pcop, PCI(pc->next->next)->pcop) // operator cannot have wreg!!
            )
            {
                switch (PCI(pc->next)->op)
                {
                case POC_INFW:
                    pc1 = newpCode(POC_INF, PCI(pc->next)->pcop);
                    break; // later remove no use LDA
                case POC_DECFW:
                    pc1 = newpCode(POC_DECF, PCI(pc->next)->pcop);
                    break;
                default:
                    fprintf(stderr, "unknown internel error at %s:%d\n", __FILE__, __LINE__);
                    exit(-__LINE__);
                    break;
                    // case POC_ADCFW: pc1 = newpCode(POC_ADCWF, PCI(pc->next)->pcop); break;
                }
                pCodeReplace(pc->next->next, newpCode(POC_MVFW, PCI(pc->next)->pcop)); // later remove this LDA by further check
                pCodeReplace(pc->next, pc1);
                // pc3 = pc->next->next;

                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                continue;
            }
        }

        // change to lda x sta y lda x (again here), prev not skip, no label at sta and second lda
        // volatile dangerous, but types limited ok
        if (isPCI(pc) && !PCI(pc)->isSkip && pc->next && pc->next->next && pc->next->next->next && isPCI(pc->next) && isPCI(pc->next->next) && isPCI(pc->next->next->next))
        {
            if (PCI(pc->next)->op == POC_MVFW && PCI(pc->next->next)->op == POC_MVWF && PCI(pc->next->next->next)->op == POC_MVFW &&
                pCodeOpCompare(PCI(pc->next)->pcop, PCI(pc->next->next->next)->pcop) && PCI(pc->next->next)->label == NULL && PCI(pc->next->next->next)->label == NULL)
            {
                if (PCI(pc->next)->pcop->type == PO_GPR_REGISTER || PCI(pc->next)->pcop->type == PO_GPR_TEMP ||
                    (PCI(pc->next)->pcop->type == PO_DIR && (!PCOR(PCI(pc->next)->pcop)->r || !PCOR(PCI(pc->next)->pcop)->r->isVolatile)))
                {
                    pc3 = pc->next->next->next;
                    removepCode(&pc3);
                    count++;
                    removed_count++;
                    if (removed_count >= HY08A_options.ob_count)
                    {
                        options.nopeep = 1;
                        return count;
                    }
                    continue;
                }
            }
        }

        // 2019 march, dcf+tfsz -> dcsz
        // volatile ok
        if (
            (!(pc->prev) || !isPCI(pc->prev) || !PCI(pc->prev)->isSkip) &&
            isPCI(pc) && PCI(pc)->op == POC_DECF && pc->next && isPCI(pc->next) &&
            PCI(pc->next)->op == POC_TFSZ && pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next)->pcop) && !(isPCI(pc->prev) && PCI(pc->prev)->isSkip) && PCI(pc->next)->label == NULL)
        {
            pCodeReplace(pc->next, newpCode(POC_DCSZ, pCodeOpCopy(PCI(pc)->pcop)));
            removepCode(&pc);
            count++;
            removed_count++;
            if (removed_count >= HY08A_options.ob_count)
            {
                options.nopeep = 1;
                return count;
            }
            continue;
        }

        // 2018 DEC, try to optimize IORL 0
        // volatile ok
        if (isPCI(pc) && (PCI(pc)->op == POC_IORL || PCI(pc)->op == POC_XORLW) && PCI(pc)->pcop->type == PO_LITERAL &&
            PCOL(PCI(pc)->pcop)->lit == 0 && PCI(pc)->label == NULL) // no label
        {
            // if prev have already the Z of W, we remove
            if (isPCI(pc->prev) && PCI(pc->prev)->op != POC_CALL && ((PCI(pc->prev)->outCond & (PCC_Z | PCC_W)) == (PCC_Z | PCC_W)) && (!pc->prev->prev || !isPCI(pc->prev->prev) || !PCI(pc->prev->prev)->isSkip))
            {
                removepCode(&pc);
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                continue;
            }
            // if later destroy z and not reference z ==> destroy it, too
            if ((!isPCI(pc->prev) || !PCI(pc->prev)->isSkip) && isPCI(pc->next) && !(PCI(pc->next)->inCond & PCC_Z) &&
                (PCI(pc->next)->outCond & PCC_Z))
            {
                removepCode(&pc);
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                continue;
            }
            // if later don't reference Z, we skip
            if (pc->next && pc->next->next && isPCI(pc->next) && isPCI(pc->next->next) && !PCI(pc->next)->isSkip &&
                !(PCI(pc->next)->inCond & PCC_Z) && !PCI(pc->next->next)->isSkip && !(PCI(pc->next->next)->inCond & PCC_Z))
            {
                removepCode(&pc);
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                continue;
            }
        }

        // 2018 nov, we try to optimize MULL
        // volatile ok
        // MVF	__mulint_STK02,1,0	;PO_DIR
        /*
        122 003B 6688           	MVF	__mulint_STK02,1,0	;PO_DIR
        123 003C 0C87           	CLRF	__mulint_STK01, 0; PO_DIR
            124 003D 0611           	MVL	0x11; PO_LITERAL
            125 003E 6686           	MVF	__mulint_STK00, 1, 0; PO_DIR
            126 003F 0600           	MVL	0x00; PO_LITERAL
            127 0040 C83D           	CALL	__mulint; PO_STR
            128 0041 6683           	MVF(_ku + 1), 1, 0; PO_DIR
            129 0042 6480           	MVF	STK00, 0, 0; PO_GPR_TEMP
            130 0043 6682           	MVF	_ku, 1, 0; PO_DIR
            */

        if (
            // total 9 instruction, following 8 pci
            hasFollowngPCI(pc, 8) && PCI(pc->next->next->next->next->next)->op == POC_CALL)
        {
            pCode *pccall = pc->next->next->next->next->next;
            if (HY08A_getPART()->isEnhancedCore != 5 &&
                PCI(pccall)->pcop->type == PO_STR && !strcmp(PCI(pccall)->pcop->name, "__mulint") &&
                PCI(pc->next)->op == POC_CLRF &&
                PCI(pc)->op == POC_MVWF &&
                PCI(pc->next->next)->op == POC_MVL &&
                PCI(pc->next->next->next)->op == POC_MVWF &&
                PCI(pc->next->next->next->next)->op == POC_MVL &&
                PCI(pc->next->next->next->next)->pcop->type == PO_LITERAL &&
                PCOL(PCI(pc->next->next->next->next)->pcop)->lit == 0)
            {
                pCode *pcr = pccall->next;
                if (PCI(pcr)->op == POC_MVWF && PCI(pcr->next)->op == POC_MVFW && PCI(pcr->next)->pcop->strHash == 0x2eee5516cb59c093ULL)
                {
                    pCode *pcr1 = pcr->next;
                    PCI(pcr1)->pcop = PCOP(&pc_prodl); // replace
                    pcr = pccall->prev;
                    pCodeReplace(pccall, newpCode(POC_MVFW, PCOP(&pc_prodh)));
                    pCodeReplace(pc->next->next, newpCode(POC_MULLW, PCI(pc->next->next)->pcop));

                    removepCode(&pcr); // mvwf ..stk0 after this intr
                    removepCode(&pcr); // clrf
                    pcr = pc->next;
                    removepCode(&pcr); //
                    removepCode(&pcr); //
                    pc = pcr;
                    count++;
                    removed_count++;
                    if (removed_count >= HY08A_options.ob_count)
                    {
                        options.nopeep = 1;
                        return count;
                    }
                }
                else if (HY08A_getPART()->isEnhancedCore != 5)
                {
                    // result optimized
                    pCodeReplace(pc->next->next, newpCode(POC_MULLW, PCI(pc->next->next)->pcop));
                    pCodeReplace(pc->next->next->next, newpCode(POC_MVFW, PCOP(&pc_prodl)));
                    pCodeReplace(pc->next->next->next->next, newpCode(POC_MVWF, getSTKXXOP(Gstack_base_addr)));
                    pCodeReplace(pc->next->next->next->next->next, newpCode(POC_MVFW, PCOP(&pc_prodh)));
                    pcr = pc->next;
                    removepCode(&pcr);
                    removepCode(&pcr);
                    pc = pcr;
                    count++;
                    removed_count++;
                    if (removed_count >= HY08A_options.ob_count)
                    {
                        options.nopeep = 1;
                        return count;
                    }
                }
            }
        }

        // next is mvwf a mvl b addwf/andwf/iorwf/xorwf a
        // change to addlw b mvwf a mvlw b

        // if (pc->prev && isPCI(pc->prev) && PCI(pc->prev)->op == POC_CALL)
        // fprintf(stderr, "call\n");

        // volatile dangerous, type limited
        if ((pc->prev == NULL || !isPCI(pc->prev) || !PCI(pc->prev)->isSkip) &&
            pc->next && pc->next->next && isPCI(pc) && isPCI(pc->next) && isPCI(pc->next->next) &&
            PCI(pc->next)->label == NULL && PCI(pc->next->next)->label == NULL &&
            PCI(pc)->op == POC_MVWF && PCI(pc->next)->op == POC_MVL && (PCI(pc->next->next)->op == POC_ADDWF || PCI(pc->next->next)->op == POC_IORWF || PCI(pc->next->next)->op == POC_ANDWF || PCI(pc->next->next)->op == POC_XORF) && pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next->next)->pcop) &&
            !(PCI(pc)->pcop->type == PO_DIR && PCOR(PCI(pc)->pcop)->r != NULL &&
              PCOR(PCI(pc)->pcop)->r->isVolatile) &&
            (PCI(pc)->pcop->type == PO_IMMEDIATE && PCOI(PCI(pc)->pcop)->r != NULL &&
             PCOI(PCI(pc)->pcop)->r->isVolatile) &&
            !(PCI(pc)->pcop->type == PO_SFR_REGISTER))
        {
            pCode *npc = NULL;
            int isADDL = 0;
            switch (PCI(pc->next->next)->op)
            {
            case POC_IORWF:
                npc = newpCode(POC_IORL, PCI(pc->next)->pcop);
                break;
            case POC_ADDWF:
                npc = newpCode(POC_ADDLW, PCI(pc->next)->pcop);
                isADDL = 1;
                break;
            case POC_XORF:
                npc = newpCode(POC_XORLW, PCI(pc->next)->pcop);
                break;
            case POC_ANDWF:
                npc = newpCode(POC_ANDLW, PCI(pc->next)->pcop);
                break;
            default:
                break;
            }

            // special case is ADDL PCOI, because currently linker support
            // local address of MVL local-address add var
            // but not support ADDL local-address..
            if (HY08A_getPART()->isEnhancedCore == 4 && isADDL && PCI(npc)->pcop->type == PO_IMMEDIATE)
            {
                // only for previous instruction is mvfw !!, if not mvfw, we don't optimize here!!
                if (isPCI(pc->prev) && PCI(pc->prev)->op == POC_MVFW)
                {
                    pCode *npc2, *npc3;
                    // thus we can replace mvfw addlw to mvl addfw
                    pCodeReplace(pc->next->next, newpCode(POC_MVL, PCI(pc->next)->pcop));
                    pCodeReplace(pc->next, newpCode(POC_MVWF, PCI(pc)->pcop));
                    pCodeReplace(pc, npc);
                    pc = npc;

                    // then change mvfw addlw to mvl addfw
                    npc2 = newpCode(POC_MVL, PCI(npc)->pcop);
                    npc3 = newpCode(POC_ADDFW, PCI(pc->prev)->pcop);
                    pCodeReplace(pc->prev, npc2);
                    pCodeReplace(pc, npc3);
                    pc = npc3;
                    count++;
                    removed_count++;
                    if (removed_count >= HY08A_options.ob_count)
                    {
                        options.nopeep = 1;
                        return count;
                    }
                }
            }

            else
            {

                pCodeReplace(pc->next->next, newpCode(POC_MVL, PCI(pc->next)->pcop));
                pCodeReplace(pc->next, newpCode(POC_MVWF, PCI(pc)->pcop));
                pCodeReplace(pc, npc);
                pc = npc;
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
            }
        }

        // 2018 June, add clr %1 bcf status,c rlfc %1 ... rlfc can be removed
        // because it is zero volatile ok

        if (isPCI(pc) && pc->next && isPCI(pc->next) &&
            PCI(pc)->op == POC_BCF && (PCI(pc->next)->op == POC_RLCF || PCI(pc->next)->op == POC_RRCF) &&
            // PCI(pc)->pcop->type == PO_GPR_BIT &&
            // PCORB(PCI(pc)->pcop)->bit == 4 &&
            // PCORB(PCI(pc)->pcop)->pcor.rIdx == IDX_STATUS &&
            (PCI(pc)->outCond & PCC_C) &&
            PCI(pc->next)->label == NULL &&
            PCI(pc)->label == NULL) // this is a bug of last versions
        {
            // fprintf(stderr, "special1\n");
            pc2 = pc->prev;

            while (pc2 && isPCI(pc2) && PCI(pc2)->label == NULL && (PCI(pc2)->op == POC_CLRF || (PCI(pc2)->op == POC_BCF && (PCI(pc2)->outCond & PCC_C)
                                                                                                 // PCI(pc2)->pcop->type == PO_GPR_BIT && PCORB(PCI(pc2)->pcop)->bit == 4 && PCORB(PCI(pc2)->pcop)->pcor.rIdx == IDX_STATUS
                                                                                                 ) ||
                                                                    (PCI(pc2)->op == PCI(pc->next)->op && !pCodeOpCompare(PCI(pc->next)->pcop, PCI(pc2)->pcop))

                                                                        ))
            {
                if (pCodeOpCompare(PCI(pc2)->pcop, PCI(pc->next)->pcop))
                {
                    pc = pc->next;
                    removepCode(&pc);
                    count++;
                    removed_count++;
                    if (removed_count >= HY08A_options.ob_count)
                    {
                        options.nopeep = 1;
                        return count;
                    }
                    break;
                }
                if (PCI(pc2)->label)
                    break;
                pc2 = pc2->prev; // look backward
            }
        }

        // 2018 may mvwf incf + bitskip status,1, incf means volatile ok
        if (isPCI(pc) && pc->next && pc->next->next &&
            isPCI(pc->next) && isPCI(pc->next->next) &&
            PCI(pc->next)->label == NULL &&
            PCI(pc->next->next)->label == NULL &&
            PCI(pc)->op == POC_MVWF &&
            (PCI(pc->next)->op == POC_INFW || PCI(pc->next)->op == POC_DECFW) &&
            (PCI(pc->next->next)->op == POC_BTSS || PCI(pc->next->next)->op == POC_BTSZ) &&
            pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next)->pcop) &&
            // PCI(pc->next->next)->pcop->type == PO_GPR_BIT && PCORB(PCI(pc->next->next)->pcop)->pcor.r &&
            // PCORB(PCI(pc->next->next)->pcop)->pcor.r->rIdx == IDX_STATUS && PCORB(PCI(pc->next->next)->pcop)->bit == 1)
            (PCI(pc)->inCond & PCC_O))
        {
            PCI(pc->next)->pcop = (pCodeOp *)&pc_wreg;
            // this is not a direct optimize!!
        }

        // 2018 May move first 2 peep rules to here
        // bitskip+jmp b+xx+b:yy
        // change to inverse bitskip +xx +b:yy volatile ok
        if (!(isPCI(pc) && PCI(pc)->isSkip) &&
            pc->next && isPCI(pc->next) &&
            pc->next->next && isPCI(pc->next->next) &&
            pc->next->next->next && isPCI(pc->next->next->next) &&
            pc->next->next->next->next && isPCI(pc->next->next->next->next) &&
            PCI(pc->next)->hasSkipInv && PCI(pc->next->next)->op == POC_JMP &&
            PCI(pc->next->next)->label == NULL && // the jmp skipped must not have label!!
            PCI(pc->next->next->next->next)->label)
        {
            int key1 = PCL(PCI(pc->next->next->next->next)->label->pc)->key;
            int key2 = PCOLAB(PCI(pc->next->next)->pcop)->key;
            if (key1 > 0 && key1 == key2)
            {
                pCodeReplace(pc->next, newpCode(PCI(pc->next)->inverted_op, PCI(pc->next)->pcop));
                pc = pc->next->next;
                removepCode(&pc);
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
            }
        }

        // 2018 April add a simple rule lda0 orl0 tfsz _wreg, volatile ok
        if (pc->next && pc->next->next && pc->next->next->next &&
            isPCI(pc) && isPCI(pc->next) && isPCI(pc->next->next) && isPCI(pc->next->next->next))
        {
            if (PCI(pc)->op == POC_MVL && PCI(pc)->pcop->type == PO_LITERAL && PCOL(PCI(pc)->pcop)->lit == 0 &&
                PCI(pc->next)->op == POC_IORL && PCOL(PCI(pc->next)->pcop)->lit == 0 && PCI(pc->next)->label == NULL &&
                PCI(pc->next->next)->op == POC_TFSZ && PCI(pc->next->next)->pcop->type == PO_W && PCI(pc->next->next)->label == NULL &&
                PCI(pc->next->next->next)->label == NULL)
            {
                pc = pc->next->next->next;
                removepCode(&pc);
                removepCode(&pc);
                removepCode(&pc);
                removepCode(&pc);
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
            }
        }
        //}

        /*
    replace{
    _NOTBITSKIP_ % 1
    % 3:	movfw % 2
    movwf % 2
    } by{
    % 1
    % 3:	movfw % 2
    }
    */
        // 3 PCI
        if (!(isPCI(pc) && PCI(pc)->isSkip) && pc->next && isPCI(pc->next) && pc->next->next && isPCI(pc->next->next))
        {
            pc1 = pc->next;
            pc2 = pc1->next;
            // this optimization applies to addr referenced ones
            if (PCI(pc1)->op == POC_MVFW && PCI(pc2)->op == POC_MVWF && pCodeOpCompare(PCI(pc1)->pcop, PCI(pc2)->pcop) &&
                PCI(pc2)->label == NULL && !(PCI(pc1)->pcop->type == PO_DIR && (PCOR(PCI(pc1)->pcop)->r && PCOR(PCI(pc1)->pcop)->r->isVolatile)))
            {

                removepCode(&pc2);
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                continue;
            }
            // second is movwf+movfw
            if (PCI(pc1)->op == POC_MVWF && PCI(pc2)->op == POC_MVFW && pCodeOpCompare(PCI(pc1)->pcop, PCI(pc2)->pcop) &&
                PCI(pc2)->label == NULL && !(PCI(pc1)->pcop->type == PO_DIR && (PCOR(PCI(pc1)->pcop)->r && PCOR(PCI(pc1)->pcop)->r->isVolatile)))
            {

                removepCode(&pc2);
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                continue;
            }
            // there will be FW+FW
            // if (PCI(pc1)->op == POC_MVFW && PCI(pc2)->op == POC_MVFW && PCI(pc2)->label == NULL)
            //{
            //    removepCode(&pc1); // only second has effect
            //    count++;
            //    removed_count++;
            //    if (removed_count >= HY08A_options.ob_count)
            //    {
            //        options.nopeep = 1;
            //        return count;
            //    }
            //}
        }
        //}
        //

        if (isPCI(pc) && pc->next && isPCI(pc->next))
        {
            pc1 = pc->next;
            // double mvfw same op
            if (PCI(pc)->op == POC_MVFW && PCI(pc1)->op == POC_MVFW && pCodeOpCompare(PCI(pc1)->pcop, PCI(pc)->pcop) && !PCI(pc1)->pcop->flags.parapcop &&
                // only for true non-volatile
                (PCI(pc)->pcop->type == PO_GPR_TEMP || PCI(pc)->pcop->type == PO_GPR_REGISTER ||
                 (PCI(pc)->pcop->type == PO_DIR && (PCOR(PCI(pc)->pcop)->r == NULL || PCOR(PCI(pc)->pcop)->r->isVolatile == 0)) ||
                 (PCI(pc)->pcop->type == PO_IMMEDIATE && (PCOI(PCI(pc)->pcop)->r == NULL || PCOI(PCI(pc)->pcop)->r->isVolatile == 0))

                     ))
            {
                pc2 = pc->prev;
                if (PCI(pc1)->label == NULL)
                    removepCode(&pc1);
                else if (PCI(pc)->label == NULL)
                    removepCode(&pc);
                pc = pc2;
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                continue;
            }

            // double mvwf same op, possible label in one of it, skip sfr should be OK
            if (PCI(pc)->op == POC_MVWF && PCI(pc1)->op == POC_MVWF && pCodeOpCompare(PCI(pc1)->pcop, PCI(pc)->pcop) && !PCI(pc1)->pcop->flags.parapcop &&
                (PCI(pc)->pcop->type == PO_GPR_TEMP || PCI(pc)->pcop->type == PO_GPR_REGISTER ||
                 (PCI(pc)->pcop->type == PO_DIR && (PCOR(PCI(pc)->pcop)->r == NULL || PCOR(PCI(pc)->pcop)->r->isVolatile == 0)) ||
                 (PCI(pc)->pcop->type == PO_IMMEDIATE && (PCOI(PCI(pc)->pcop)->r == NULL || PCOI(PCI(pc)->pcop)->r->isVolatile == 0))))
            {
                pc2 = pc->prev;
                if (PCI(pc1)->label == NULL)
                    removepCode(&pc1);
                else if (PCI(pc)->label == NULL)
                    removepCode(&pc);
                pc = pc2;
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                continue;
            }

            if ((!(pc->prev) || !isPCI(pc->prev) || !PCI(pc->prev)->isSkip) && PCI(pc)->op == POC_MVL && PCI(pc->next)->op == POC_XORLW &&
                PCI(pc->next)->label == NULL && (!isPCI(pc->next->next) || !PCI(pc->next->next)->isSkip) &&
                PCI(pc)->pcop->type == PO_LITERAL && PCI(pc->next)->pcop->type == PO_LITERAL)
            {
                int newlit = PCOL(PCI(pc)->pcop)->lit ^ PCOL(PCI(pc->next)->pcop)->lit;
                pc = pc->next;
                removepCode(&pc);
                PCI(pc)->pcop = newpCodeOpLit(newlit);
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
            }
        }
        //}

        // for (pc = pb->pcHead; pc; pc = pc->next) // accept no pc
        //{
        //     pCode *pc1, *pc2;
        if (pc->next && pc->next->next && isPCI(pc->next) && isPCI(pc->next->next))
        {
            if (isPCI(pc) && PCI(pc)->isSkip)
                continue;
            pc1 = pc->next;
            pc2 = pc->next->next;
            if (PCI(pc2)->label)
                continue;
            // fw and wf is done above
            // if (PCI(pc1)->op == POC_MVFW && PCI(pc2)->op == POC_MVWF && PCI(pc1)->pcop->type != PO_DIR &&
            //     pCodeOpCompare(PCI(pc1)->pcop, PCI(pc2)->pcop))
            // {
            //     removepCode(&pc2);
            //     count++;
            //     removed_count++;
            //     if (removed_count >= HY08A_options.ob_count)
            //     {
            //         options.nopeep = 1;
            //         return count;
            //     }
            // }

            // volatile free
            if (PCI(pc1)->op == POC_MVL && PCI(pc1)->pcop->type == PO_LITERAL && (PCOL(PCI(pc1)->pcop)->lit == 0 || PCOL(PCI(pc1)->pcop)->lit == 0xff) &&
                PCI(pc2)->op == POC_MVWF)
            {
                if (PCOL(PCI(pc1)->pcop)->lit == 0)
                {
                    pCodeReplace(pc1, newpCode(POC_CLRF, PCI(pc2)->pcop));
                    pCodeReplace(pc2, newpCode(POC_MVL, PCI(pc1)->pcop));
                }
                else
                {
                    pCodeReplace(pc1, newpCode(POC_SETF, PCI(pc2)->pcop));
                    pCodeReplace(pc2, newpCode(POC_MVL, PCI(pc1)->pcop));
                }
                // this operation dont reduce code, so count no increase
            }
        }
        //}

        // following is btss/sz + jmp + btss/sz + JMP volatile free
        // for (pc = pb->pcHead; pc; pc = (pc->next)) // first can count
        //{
        if (pc->next && pc->next->next && pc->next->next->next && pc->next->next->next->next &&
            isPCI(pc->next) && isPCI(pc->next->next) && isPCI(pc->next->next->next) && isPCI(pc->next->next->next->next) && (!isPCI(pc) || !(PCI(pc)->isSkip)) && (PCI(pc->next)->op == POC_BTSS || PCI(pc->next)->op == POC_BTSZ) &&
            PCI(pc->next->next)->op == POC_JMP &&
            (PCI(pc->next->next->next)->op == POC_BTSS || PCI(pc->next->next->next)->op == POC_BTSZ) &&
            PCI(pc->next->next->next->next)->op == POC_JMP &&
            // the special condition is first operand cannot have _STATUS
            pCodeOpCompare(PCI(pc->next->next)->pcop, PCI(pc->next->next->next->next)->pcop)

        )
        {
            if (PCORB(PCI(pc->next)->pcop)->pcor.rIdx != IDX_STATUS)
            {
                // reverse first one
                pCodeReplace(pc->next, newpCode(PCI(pc->next)->inverted_op, PCI(pc->next)->pcop));
                pc = pc->next->next;
                removepCode(&pc);
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
            }
        }
        //}
        // mvl btss mvl hang tag: hang
        // for (pc = pb->pcHead; pc; pc = (pc->next))
        //{
        // very very special case, kiip as it is 2020
        // volatile free
        if ((!isPCI(pc) || !PCI(pc)->isSkip) && pc->next && pc->next->next && pc->next->next->next && pc->next->next->next->next &&
            isPCI(pc->next) && isPCI(pc->next->next) && isPCI(pc->next->next->next) &&
            PCI(pc->next)->op == POC_MVL && PCI(pc->next->next->next)->op == POC_MVL && PCI(pc->next->next)->isSkip &&
            ((!(PCI(pc->next->next->next->next)->inCond & PCC_W))))
        // we search following instructions for incond w
        {
            int found = 0;
            for (pc1 = pc->next->next->next->next; pc1 && isPCI(pc1); pc1 = pc1->next)
            {
                if (PCI(pc1)->label || (PCI(pc1)->inCond & PCC_W) || PCI(pc1)->isBitInst ||
                    PCI(pc1)->op == POC_JMP || // we don't use jxx here, left to linker
                    PCI(pc1)->op == POC_CALL ||
                    (PCI(pc1)->pcop && PCI(pc1)->pcop->type == PO_W && (PCI(pc1)->inCond & PCC_REGISTER)) ||
                    (PCI(pc1)->pcop && PCI(pc1)->pcop->type == PO_PLUSW) ||
                    (PCI(pc1)->pcop2 && PCI(pc1)->pcop2->type == PO_PLUSW))
                    //|| PCI(pc1)->isSkip)
                    break;
                if (PCI(pc1)->outCond & PCC_W)
                {
                    found = 1;
                    break;
                }
            }
            if (found)
            {
                pc = pc->next->next->next;
                removepCode(&pc);
                removepCode(&pc);
                removepCode(&pc);
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
            }
        }
        //        2022 sep MVL 0 btsz xxx,0 mvl 1 ==> mvfw xxx, ANDL 1
        // volatile free
        // if(
        //     (!pc->prev || !isPCI(pc->prev) || !PCI(pc->prev)->isSkip)
        //     && hasContNextPCI(pc,2) && !count &&
        //     PCI(pc)->op==POC_MVL && PCI(pc->next)->op==POC_BTSZ &&
        // PCI(pc->next->next)->op==POC_MVL &&
        // PCI(pc)->pcop->type==PO_LITERAL &&
        // PCI(pc->next->next)->pcop->type==PO_LITERAL &&
        // PCOL(PCI(pc)->pcop)->lit == 0 &&
        // PCOL(PCI(pc->next->next)->pcop)->lit == 1 &&
        // PCI(pc->next)->pcop->type==PO_GPR_BIT &&
        // !(PCORB(PCI(pc->next)->pcop)->inBitSpace) &&
        // PCORB(PCI(pc->next)->pcop)->bit == 0 )
        // {
        //     pCodeOp *pcopbit = PCI(pc->next)->pcop;
        //     pc1=pc->prev;
        //     pc2=pc->next->next;
        //     pc3=pc->next;
        //     pCodeReplace(pc->next,newpCode(POC_ANDLW, newpCodeOpLit(1)));

        //     pCodeReplace(pc,newpCode(POC_MVFW, pCodeOpCopy((pCodeOp*) (& (PCORB(pcopbit)->pcor)))));
        //     if(PCORB(pcopbit)->pcor.r)
        //     {
        //         PCI(pc1->next)->pcop->type=PCORB(pcopbit)->pcor.r->pc_type;
        //         PCI(pc1->next)->pcop->name = strdup(PCORB(pcopbit)->pcor.r->name);
        //         PCI(pc1->next)->pcop->name_hash = hash(PCORB(pcopbit)->pcor.r->name);
        //     }
        //     removepCode(&pc2);
        //     pc=pc1->next;

        //     count++;
        //     removed_count++;
        //     if (removed_count >= HY08A_options.ob_count)
        //     {
        //         options.nopeep = 1;
        //         return count;
        //     }
        //     continue;
        // }
    }

    // fourth is status,C with BCF/BSF
    // 2020 need consider special case btss C ... btsc

    for (pc = findNextInstruction(pb->pcHead); pc; pc = findNextInstruction(pc->next))
    {
        // if(isPCI(pc)&&PCI(pc)->pcop && pcopVolatile(PCI(pc)->pcop))
        // continue;
        if ( // PCI(pc)->pcop && PCI(pc)->pcop->type == PO_GPR_BIT && PCORB(PCI(pc)->pcop)->pcor.r &&
             // PCORB(PCI(pc)->pcop)->pcor.r->rIdx == IDX_STATUS && PCORB(PCI(pc)->pcop)->bit == 4 &&
            (PCI(pc)->outCond & PCC_C) &&
            (PCI(pc)->op == POC_BCF || PCI(pc)->op == POC_BSF)) // C bit
        {
            pCode *pc4;
            pCode *pc5;
            pCode *pc6;
            pc1 = pc->next;
            pc2 = pc1 ? pc1->next : NULL;
            pc3 = pc2 ? pc2->next : NULL;
            pc4 = pc3 ? pc3->next : NULL;
            pc5 = pc4 ? pc4->next : NULL;
            pc6 = pc5 ? pc5->next : NULL;

            if (pc1 && pc2 && pc3 && isPCI(pc1) && isPCI(pc2) && isPCI(pc3) && PCI(pc1)->label == NULL && PCI(pc2)->label == NULL &&
                PCI(pc3)->label == NULL && PCI(pc2)->op == PCI(pc)->inverted_op &&
                PCI(pc1)->isSkip && PCI(pc3)->isSkip && PCI(pc3)->pcop &&
                // PCI(pc3)->pcop->type == PO_GPR_BIT &&
                // PCORB(PCI(pc3)->pcop)->bit == 4 && PCORB(PCI(pc3)->pcop)->pcor.r &&
                // PCORB(PCI(pc3)->pcop)->pcor.r->rIdx == IDX_STATUS
                (PCI(pc3)->inCond & PCC_C) // BTSS r BTSZ
            )
            {
                // bcf C
                // btss COND   ==> if cond is true, C is clear following code will execute
                // bsf C
                // btss C
                if (pc5 && isPCI(pc5) && (PCI(pc)->op == POC_BCF && PCI(pc3)->op == POC_BTSS) ||
                    (PCI(pc)->op == POC_BSF && PCI(pc3)->op == POC_BTSZ)) // the code is to prevent glitch?
                {
                    // consider dual C check with BSF/BCF
                    if (PCI(pc5)->op == PCI(pc3)->inverted_op &&
                        // PCI(pc5)->pcop->type == PO_GPR_BIT &&
                        // PCORB(PCI(pc5)->pcop)->bit == 4 && PCORB(PCI(pc5)->pcop)->pcor.r &&
                        // PCORB(PCI(pc5)->pcop)->pcor.r->rIdx == IDX_STATUS
                        (PCI(pc5)->inCond & PCC_C)) // dual C
                    {
                        // no prevent glitch
                        pCode *tmpp = pc->prev;
                        pCodeReplace(pc, newpCode(PCI(pc6)->op, PCI(pc6)->pcop));
                        pc = tmpp;
                        pCodeReplace(pc1, newpCode(PCI(pc1)->inverted_op, PCI(pc1)->pcop));
                        removepCode(&pc3);
                        removepCode(&pc2);
                        removepCode(&pc5);
                        removepCode(&pc6);

                        count++;
                        removed_count++;
                        if (removed_count >= HY08A_options.ob_count)
                        {
                            options.nopeep = 1;
                            return count;
                        }
                    }
                    else
                    {
                        removepCode(&pc3);
                        removepCode(&pc2);
                        removepCode(&pc);
                        // replace pc1
                        pCodeReplace(pc1, newpCode(PCI(pc1)->inverted_op, PCI(pc1)->pcop));
                        count++;
                        removed_count++;
                        if (removed_count >= HY08A_options.ob_count)
                        {
                            options.nopeep = 1;
                            return count;
                        }
                    }
                }
                else if (
                    (PCI(pc)->op == POC_BSF && PCI(pc3)->op == POC_BTSS) ||
                    (PCI(pc)->op == POC_BCF && PCI(pc3)->op == POC_BTSZ))
                {
                    if (pc5 && isPCI(pc5) && PCI(pc5)->op == PCI(pc3)->inverted_op &&
                        (PCI(pc5)->inCond & PCC_C)
                        // PCI(pc5)->pcop->type == PO_GPR_BIT &&
                        // PCORB(PCI(pc5)->pcop)->bit == 4 && PCORB(PCI(pc5)->pcop)->pcor.r &&
                        // PCORB(PCI(pc5)->pcop)->pcor.r->rIdx == IDX_STATUS
                        ) // dual C
                    {
                        pCode *tmpp = pc->prev;
                        pCodeReplace(pc, newpCode(PCI(pc6)->op, PCI(pc6)->pcop));
                        pc = tmpp;
                        removepCode(&pc3);
                        removepCode(&pc2);
                        removepCode(&pc5);
                        removepCode(&pc6);
                        count++;
                        removed_count++;
                        if (removed_count >= HY08A_options.ob_count)
                        {
                            options.nopeep = 1;
                            return count;
                        }
                    }
                    else
                    {
                        removepCode(&pc3);
                        removepCode(&pc2);
                        removepCode(&pc);
                        count++;
                        removed_count++;
                        if (removed_count >= HY08A_options.ob_count)
                        {
                            options.nopeep = 1;
                            return count;
                        }
                    }
                }
            }
        }
    }

    // single bit ++/^=1 in struct, volatile free
    for (pc = findNextInstruction(pb->pcHead); pc; pc = findNextInstruction(pc->next))
    {
        pCodeOp *thebit1, *thebit2;
        pCodeOp *thetemp1, *thetemp2;
        pCode *tail1 = NULL, *tail2 = NULL;
        HYA_OPCODE opcode = POC_BTSS;

        if (isZ2WINV(pc, &tail1) && tail1 && isPCI(tail1))
        {
            if (PCI(tail1)->op == POC_RRCFW && isC2BITINV(tail1->next, &thebit1))
            {
                pc1 = pc->prev;
                pc3 = tail1->next->next->next->next;
                pCodeReplace(pc->next->next, pc2 = newpCode(POC_BCF, thebit1));
                pCodeReplace(pc, newpCode(POC_BSF, thebit1));
                while (pc3 != pc2)
                    removepCode(&pc3);
                pc = pc1;
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }

                continue;
            }
        }

        // struct bit ^=1 inverse, etc
        if (isBIT2C(pc, &thebit1, &tail1))
        {
            if (isC2BITINV(tail1->next, &thebit2))
            {
                // there are some cases
                if (PCI(pc)->op == POC_MVL)
                {
                    int lit0 = PCOL(PCI(pc)->pcop)->lit;
                    if (lit0 == 0 && PCI(tail1->prev)->op != POC_XORLW && PCI(tail1->prev)->op != POC_COMFW) // assignment case
                    {
                        // special case, same bit and comfw _wreg

                        // change the c to the bit
                        for (pc1 = pc; pc1 != tail1->next; pc1 = pc1->next)
                        {
                            if (PCI(pc1)->label)
                                if (PCI(tail1->next)->label)
                                    pBranchAppend(PCI(tail1->next)->label, PCI(pc1)->label);
                                else
                                {
                                    PCI(tail1->next)->label = PCI(pc1)->label;
                                    replaceLabelPtrTabInPb(tail1->pb, pc1, tail1->next);
                                }
                            if (PCI(pc1)->cline)
                                PCI(tail1->next)->cline = PCI(pc1)->cline;
                        }
                        pc1 = pc->prev;
                        pCodeReplace(tail1->next, newpCode(POC_BTSS, thebit1));
                        pCodeReplace(tail1->next->next->next, newpCode(POC_BTSZ, thebit1));
                        pc1->next = tail1->next;
                        tail1->next->prev = pc1;
                        pc = pc1;
                        count++;
                        removed_count++;
                        if (removed_count >= HY08A_options.ob_count)
                        {
                            options.nopeep = 1;
                            return count;
                        }

                        continue;
                    }

                    // inv case
                    if ((lit0 == 0 && PCI(tail1->prev)->op == POC_XORLW && PCI(tail1->prev)->pcop->type == PO_LITERAL &&
                         PCOL(PCI(tail1->prev)->pcop)->lit == 1) ||
                        (lit0 == 0 && PCI(tail1->prev)->op == POC_COMFW && PCI(tail1->prev)->pcop->type == PO_W) ||
                        ((lit0 == 0xff || lit0 == 0x01) && PCI(tail1->prev)->op != POC_XORLW))
                    {

                        // it is possible same bit, use btgf
                        if (pCodeOpCompare(thebit1, thebit2))
                        {
                            tail2 = tail1->next->next->next->next->next;
                            pc1 = newpCode(POC_BTGF, thebit1);
                            pCodeReplace(pc, pc1);
                            pc = pc1;
                            for (pc1 = pc->next; pc1 != tail2; pc1 = pc1->next)
                            {
                                if (PCI(pc1)->label)
                                    if (PCI(pc)->label)
                                        pBranchAppend(PCI(pc)->label, PCI(pc1)->label);
                                    else
                                    {
                                        PCI(pc)->label = PCI(pc1)->label;
                                        replaceLabelPtrTabInPb(pc1->pb, pc1, pc);
                                    }
                                if (PCI(pc1)->cline)
                                    PCI(pc)->cline = PCI(pc1)->cline;
                            }
                            pc->next = tail2;
                            tail2->prev = pc;
                            count++;
                            removed_count++;
                            if (removed_count >= HY08A_options.ob_count)
                            {
                                options.nopeep = 1;
                                return count;
                            }
                            continue;
                        }
                        else
                        {

                            for (pc1 = pc; pc1 != tail1->next; pc1 = pc1->next)
                            {
                                if (PCI(pc1)->label)
                                    if (PCI(tail1->next)->label)
                                        pBranchAppend(PCI(tail1->next)->label, PCI(pc1)->label);
                                    else
                                    {
                                        PCI(tail1->next)->label = PCI(pc1)->label;
                                        replaceLabelPtrTabInPb(pc1->pb, pc1, tail1->next);
                                    }
                                if (PCI(pc1)->cline)
                                    PCI(tail1->next)->cline = PCI(pc1)->cline;
                            }
                            pc1 = pc->prev;
                            pCodeReplace(tail1->next, newpCode(POC_BTSZ, thebit1));
                            pCodeReplace(tail1->next->next->next, newpCode(POC_BTSS, thebit1));
                            pc1->next = tail1->next;
                            tail1->next->prev = pc1;
                            pc = pc1;
                            count++;
                            removed_count++;
                            if (removed_count >= HY08A_options.ob_count)
                            {
                                options.nopeep = 1;
                                return count;
                            }

                            continue;
                        }
                    }
                } // end of if MVL
                else // first is CLRF
                {
                    // depends on tail1's next
                    if (PCI(tail1->next)->op == POC_BTSS)
                    {
                        // no reverse
                        pc1 = pc->prev;
                        pCodeReplace(pc->next->next, pc3 = newpCode(POC_BSF, thebit2));
                        pCodeReplace(pc, newpCode(POC_BCF, thebit2));
                        tail2 = tail1->next->next->next->next;
                        while (tail2 != pc3)
                            removepCode(&tail2);
                        pc = pc1;
                        count++;
                        removed_count++;
                        if (removed_count >= HY08A_options.ob_count)
                        {
                            options.nopeep = 1;
                            return count;
                        }

                        continue;
                    }
                    else if (PCI(tail1->next)->op == POC_BTSZ)
                    {
                        // no reverse
                        pc1 = pc->prev;
                        pCodeReplace(pc->next->next, pc3 = newpCode(POC_BCF, thebit2));
                        pCodeReplace(pc, newpCode(POC_BSF, thebit2));
                        tail2 = tail1->next->next->next->next;
                        while (tail2 != pc3)
                            removepCode(&tail2);
                        pc = pc1;
                        count++;
                        removed_count++;
                        if (removed_count >= HY08A_options.ob_count)
                        {
                            options.nopeep = 1;
                            return count;
                        }

                        continue;
                    }
                }
            }
        }

        if (isBIT2Z(pc, &thebit1, &tail1))
        {
            if (isZ2BIT(tail1, &thebit2))
            {
                if (!pCodeOpCompare(thebit1, thebit2)) // if not same it is inv bit to  bit
                {
                    pCode *pc2 = tail1; // the instruction after IORL

                    // after remove, it will be prev
                    while (pc != pc2)
                    {
                        removepCode(&pc);
                        pc = pc->next;
                    }
                    pc2 = pc->prev;
                    pCodeReplace(pc, newpCode(PCI(pc)->inverted_op, thebit1));
                    pc = pc2->next->next->next;
                    pCodeReplace(pc, newpCode(PCI(pc)->inverted_op, thebit1));
                    pc = pc2;
                    count++;
                    removed_count++;
                    if (removed_count >= HY08A_options.ob_count)
                    {
                        options.nopeep = 1;
                        return count;
                    }
                }
                else // otherwise it is same bit inv
                {
                    pCode *pc2 = tail1->next->next->next; // point to final one
                    while (pc != pc2)
                    {
                        removepCode(&pc);
                        pc = pc->next;
                    }
                    pc2 = pc->prev;
                    pCodeReplace(pc, newpCode(POC_BTGF, thebit1));
                    pc = pc2;
                    count++;
                    removed_count++;
                    if (removed_count >= HY08A_options.ob_count)
                    {
                        options.nopeep = 1;
                        return count;
                    }
                }
            }
            else if (isZ2W(tail1, &tail2))
            {
                // reverse 0,1
                pc1 = pc->prev;
                pCodeReplace(pc, newpCode(POC_MVL, newpCodeOpLit(1)));
                pc = pc1->next->next->next;
                pc2 = pc->next;
                pCodeReplace(pc, newpCode(POC_MVL, newpCodeOpLit(0)));
                while (pc2->next != tail2)
                {
                    removepCode(&pc2);
                    pc2 = pc2->next;
                }
                removepCode(&pc2); // one more!!
                pc = pc2;
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
            }
        }

        if (isBIT2TEMP(pc, &thebit1, &thetemp1, &tail1))
        {
            // 2024 4.4.4 bit copy to bit
            if(isTEMPBIT2BIT(tail1->next,&thebit2,&thetemp2,&tail2))
            {
                if(!pCodeOpCompare(thebit1,thebit2) && 
                PCOR(thetemp1)->r->rIdx == PCORB(thetemp2)->pcor.r->rIdx &&
                !findNextInstructionRefTemp(tail2->next,PCOR(thetemp1)->r->rIdx,0,NULL,0))                
                {
                    pc1=pc->prev;
                    pc2=pc->next->next->next->next->next->next->next;
                    pCodeReplace(pc1->next,newpCode(POC_BTSS, thebit1));
                    pCodeReplace(pc1->next->next, newpCode(POC_BCF, thebit2));
                    pCodeReplace(pc1->next->next->next,newpCode(POC_BTSZ, thebit1));
                    pCodeReplace(pc1->next->next->next->next, newpCode(POC_BSF, thebit2));
                    removepCode(&pc2);
                    removepCode(&pc2);
                    removepCode(&pc2);
                    removepCode(&pc2);
                    count++;
                    removed_count++;
                    if (removed_count >= HY08A_options.ob_count)
                    {
                        options.nopeep = 1;
                        return count;
                    }
                    pc=pc1;
                }
            }
            else if (tail1->next && tail1->next->next && isPCI(tail1->next->next) &&
                isTEMP2BIT(tail1->next->next, &thebit2, &thetemp2, &tail2) &&
                !findNextInstructionRefTemp(tail2, PCOR(PCI(tail1)->pcop)->r->rIdx, 0, NULL, -1) && pCodeOpCompare(thetemp1, thetemp2))
            {
                // we need to check the temp not used after it
                // if it is the same bit, we btgf
                if (pCodeOpCompare(thebit1, thebit2))
                {

                    // 2021 aug there are cased BTGF directly
                    if (isPCI(tail1->next) && (PCI(tail1->next)->op == POC_BTGF) &&
                        PCORB(PCI(tail1->next)->pcop)->bit == 0 &&
                        PCORB(PCI(tail1->next)->pcop)->pcor.rIdx == PCOR(thetemp1)->rIdx)
                    {
                        pCode *pc2 = pc->prev;

                        pc2->next = newpCode(POC_BTGF, thebit1);
                        PCI(pc2->next)->cline = PCI(pc)->cline;
                        PCI(pc2->next)->label = PCI(pc)->label;
                        replaceLabelPtrTabInPb(pc->pb, pc, pc2->next);
                        pc2->next->next = tail2->next;
                        if (pc->pb->pcTail == tail2)
                            pc->pb->pcTail = pc2->next;
                        if (pc2->next->next)
                            pc2->next->next->prev = pc2->next; //?? bug fix 2020 SEP
                        pc2->next->prev = pc2;
                        count++;
                        removed_count++;
                        if (removed_count >= HY08A_options.ob_count)
                        {
                            options.nopeep = 1;
                            return count;
                        }
                    }

                    else if (isPCI(tail1->next) && (PCI(tail1->next)->op == POC_INF || PCI(tail1->next)->op == POC_DECF || PCI(tail1->next)->op == POC_COMF) &&
                             pCodeOpCompare(PCI(tail1->next)->pcop, thetemp1)) // increase then back to bit
                    {
                        // we change to toggle
                        pCode *pc2 = pc->prev;

                        pc2->next = newpCode(POC_BTGF, thebit1);
                        PCI(pc2->next)->cline = PCI(pc)->cline;
                        PCI(pc2->next)->label = PCI(pc)->label;
                        replaceLabelPtrTabInPb(pc->pb, pc, pc2->next);
                        pc2->next->next = tail2->next;
                        if (pc->pb->pcTail == tail2)
                            pc->pb->pcTail = pc2->next;
                        if (pc2->next->next)
                            pc2->next->next->prev = pc2->next; //?? bug fix 2020 SEP
                        pc2->next->prev = pc2;
                        count++;
                        removed_count++;
                        if (removed_count >= HY08A_options.ob_count)
                        {
                            options.nopeep = 1;
                            return count;
                        }
                    }
                }
                else // different bits, depends on what tail1->next do
                {
                    if (isPCI(tail1->next) &&
                        (PCI(tail1->next)->op == POC_INF || PCI(tail1->next)->op == POC_DECF || PCI(tail1->next)->op == POC_COMF) &&
                        pCodeOpCompare(PCI(tail1->next)->pcop, thetemp1)) // increase then back to bit
                    {
                        // we change to toggle different bit
                        pCode *pc2 = tail1->next->next->next;

                        // after remove, it will be prev
                        while (pc != pc2)
                        {
                            removepCode(&pc);
                            pc = pc->next;
                        }
                        pc2 = pc->prev;
                        pCodeReplace(pc, newpCode(PCI(pc)->inverted_op, thebit1));
                        pc = pc2->next->next->next;
                        pCodeReplace(pc, newpCode(PCI(pc)->inverted_op, thebit1));
                        pc = pc2;
                        count++;
                        removed_count++;
                        if (removed_count >= HY08A_options.ob_count)
                        {
                            options.nopeep = 1;
                            return count;
                        }
                    }
                }
            }
            // there is another situation that post ++/--, it will save to a var then do the ++/--
            else if (tail1->next && tail1->next->next && tail1->next->next->next && isPCI(tail1->next->next->next) &&
                     isTEMP2BIT(tail1->next->next, &thebit2, &thetemp2, &tail2)) // after mvwf optimzed to wreg
            {

                if ((PCI(tail1->next)->op == POC_INFW || PCI(tail1->next)->op == POC_DECFW ||
                     PCI(tail1->next)->op == POC_COMFW) &&
                    // PCI(tail1->next->next)->op == POC_MVWF && // this optimized
                    tail2->next && tail2->next->next && tail2->next->next->next &&
                    PCI(tail2->next)->op == POC_TFSZ && PCI(tail2->next->next)->op == POC_JMP &&
                    PCI(tail1->next)->pcop->type == PO_W && pCodeOpCompare(PCI(tail2->next)->pcop, thetemp1)) // RRCFW same to mvwf

                { // btgf btss jmp -> jump after ++/-- ==0 -> jump before ++-- =1
                  // ==> do state before ++-- ==0
                    pCode *pc2 = tail2->next;
                    int i;

                    for (i = 0; i < 9; i++)
                        removepCode(&pc2);
                    pc2 = pc->prev;
                    pCodeReplace(pc, newpCode(POC_BTGF, PCI(pc->next)->pcop));
                    pCodeReplace(pc2->next->next, newpCode(POC_BTSS, PCI(pc2->next->next)->pcop));
                    pc = pc2;
                    count++;
                    removed_count++;
                    if (removed_count >= HY08A_options.ob_count)
                    {
                        options.nopeep = 1;
                        return count;
                    }
                } // second is --==0 case
                // else if ((PCI(tail1->next)->op == POC_INFW || PCI(tail1->next)->op == POC_DECFW || PCI(tail1->next)->op == POC_COMFW) &&
                //          PCI(tail1->next->next)->op == POC_MVWF &&
                //          tail2->next && tail2->next->next && tail2->next->next->next &&
                //          PCI(tail2->next)->op == POC_RRFW && PCI(tail2->next->next)->op == POC_BTSZ &&
                //          PCI(tail2->next->next->next)->op == POC_JMP &&

                //          pCodeOpCompare(PCI(tail2->next)->pcop, PCI(tail1->next)->pcop)) // RRCFW same to mvwf
                // {
                //     pCode *pc2 = tail2->next->next;
                //     int i;
                //     for (i = 0; i < 11; i++)
                //         removepCode(&pc2);
                //     pc2 = pc->prev;
                //     pCodeReplace(pc, newpCode(POC_BTGF, PCI(pc->next)->pcop));

                //     pc = pc2;
                //     count++;
                //     removed_count++;
                //     if (removed_count >= HY08A_options.ob_count)
                //     {
                //         options.nopeep = 1;
                //         return count;
                //     }
                //}
            }

            else if (tail1->next && tail1->next->next && tail1->next->next->next && isPCI(tail1->next->next->next) &&
                     isTEMP2BIT(tail1->next->next->next, &thebit2, &thetemp2, &tail2))
            {
                /*
                ;	;.line	28; "testpt1.c"	if(bbb.bitb--)
                MVL	0x00	;PO_LITERAL
                BTSZ	_bbb,1	;PO_GPR_BIT
                MVL	0x01	;PO_LITERAL
                MVF	r0x1143,1,0	;PO_GPR_TEMP ...tai1
                DCF	r0x1143,0,0	;PO_GPR_TEMP
                MVF	r0x1144,1,0	;PO_GPR_TEMP
                RRFC	r0x1144,0,0	;PO_GPR_TEMP
                //BTSS	_STATUS,4	;PO_GPR_BIT
                BCF	_bbb,1	;PO_GPR_BIT
                BTSZ	_STATUS,4	;PO_GPR_BIT
                BSF	_bbb,1	;PO_GPR_BIT          ....tail2
                RRF	r0x1143,0,0	;PO_GPR_TEMP
                BTSZ	_STATUS,0	;PO_GPR_BIT
                JMP	_00110_DS_	;PO_LABEL

                ;	;.line	28; "testpt1.c"	if(bbb.bitb--==0)
                MVL	0x00	;PO_LITERAL
                BTSZ	_bbb,1	;PO_GPR_BIT
                MVL	0x01	;PO_LITERAL
                MVF	r0x1143,1,0	;PO_GPR_TEMP... tail1
                DCF	r0x1143,0,0	;PO_GPR_TEMP
                MVF	r0x1144,1,0	;PO_GPR_TEMP
                RRFC	r0x1144,0,0	;PO_GPR_TEMP
                //BTSS	_STATUS,4	;PO_GPR_BIT
                BCF	_bbb,1	;PO_GPR_BIT
                BTSZ	_STATUS,4	;PO_GPR_BIT
                BSF	_bbb,1	;PO_GPR_BIT         ... tail2
                TFSZ	r0x1143,0	;PO_GPR_TEMP
                JMP	_00110_DS_	;PO_LABEL

                ;	;.line	28; "testpt1.c"	if(bbb.bitb--==1)
                MVL	0x00	;PO_LITERAL
                BTSZ	_bbb,1	;PO_GPR_BIT
                MVL	0x01	;PO_LITERAL
                MVF	r0x1143,1,0	;PO_GPR_TEMP
                MVF	r0x1144,1,0	;PO_GPR_TEMP
                DCF	r0x1143,1,0	;PO_GPR_TEMP
                RRFC	r0x1143,0,0	;PO_GPR_TEMP
                //BTSS	_STATUS,4	;PO_GPR_BIT
                BCF	_bbb,1	;PO_GPR_BIT
                BTSZ	_STATUS,4	;PO_GPR_BIT
                BSF	_bbb,1	;PO_GPR_BIT
                DCSZ	r0x1144,0,0	;PO_GPR_TEMP
                JMP	_00110_DS_	;PO_LABEL


                */
                // first is if(--) case
                if ((PCI(tail1->next)->op == POC_INFW || PCI(tail1->next)->op == POC_DECFW ||
                     PCI(tail1->next)->op == POC_COMFW) &&
                    PCI(tail1->next->next)->op == POC_MVWF &&
                    tail2->next && tail2->next->next && tail2->next->next->next &&
                    PCI(tail2->next)->op == POC_TFSZ && PCI(tail2->next->next)->op == POC_JMP &&

                    pCodeOpCompare(PCI(tail2->next)->pcop, PCI(tail1->next)->pcop) // RRCFW same to mvwf
                )
                {
                    pCode *pc2 = tail2->next;
                    int i;

                    for (i = 0; i < 10; i++)
                        removepCode(&pc2);
                    pc2 = pc->prev;
                    pCodeReplace(pc, newpCode(POC_BTGF, PCI(pc->next)->pcop));
                    pCodeReplace(pc2->next->next, newpCode(POC_BTSS, PCI(pc2->next->next)->pcop));
                    pc = pc2;
                    count++;
                    removed_count++;
                    if (removed_count >= HY08A_options.ob_count)
                    {
                        options.nopeep = 1;
                        return count;
                    }
                } // second is --==0 case
                else if ((PCI(tail1->next)->op == POC_INFW || PCI(tail1->next)->op == POC_DECFW || PCI(tail1->next)->op == POC_COMFW) &&
                         PCI(tail1->next->next)->op == POC_MVWF &&
                         tail2->next && tail2->next->next && tail2->next->next->next &&
                         PCI(tail2->next)->op == POC_RRFW && PCI(tail2->next->next)->op == POC_BTSZ &&
                         PCI(tail2->next->next->next)->op == POC_JMP &&

                         pCodeOpCompare(PCI(tail2->next)->pcop, PCI(tail1->next)->pcop)) // RRCFW same to mvwf
                {
                    pCode *pc2 = tail2->next->next;
                    int i;
                    for (i = 0; i < 11; i++)
                        removepCode(&pc2);
                    pc2 = pc->prev;
                    pCodeReplace(pc, newpCode(POC_BTGF, PCI(pc->next)->pcop));

                    pc = pc2;
                    count++;
                    removed_count++;
                    if (removed_count >= HY08A_options.ob_count)
                    {
                        options.nopeep = 1;
                        return count;
                    }
                }
                // fprintf(stderr, "shift\n");
            }
            else if (tail1->next && tail1->next->next && tail1->next->next->next && tail1->next->next->next->next && isPCI(tail1->next->next->next) &&
                     isPCI(tail1->next->next->next->next) &&
                     isTEMP2BIT(tail1->next->next->next->next, &thebit2, &thetemp2, &tail2))
            {
                // if (isPCI(tail1->next) && PCI(tail1->next)->op == POC_MVL && PCI(tail1->next->next)->op == POC_XORFW && PCI(tail1->next->next->next)->op == POC_MVWF &&
                //     pCodeOpCompare(PCI(tail1->next->next)->pcop, thetemp1)) // increase then back to bit
                if (isPCI(tail1->next) && PCI(tail1->next)->op == POC_COMF && PCI(tail1->next->next)->op == POC_MVFW && PCI(tail1->next->next->next)->op == POC_MVWF &&
                    pCodeOpCompare(PCI(tail1->next)->pcop, thetemp1) && // finally the bit2 should the same to the bit1
                    pCodeOpCompare(thebit1, thebit2)

                        ) // increase then back to bit
                {
                    pCode *pc2 = pc->prev;

                    pc2->next = newpCode(POC_BTGF, thebit1);
                    PCI(pc2->next)->cline = PCI(pc)->cline;
                    PCI(pc2->next)->label = PCI(pc)->label;
                    replaceLabelPtrTabInPb(pc->pb, pc, pc2->next);
                    pc2->next->next = tail2->next;
                    if (pc->pb->pcTail == tail2)
                        pc->pb->pcTail = pc2->next;
                    if (pc2->next->next)
                        pc2->next->next->prev = pc2->next; // bug fix at 2020 Sep?
                    pc2->next->prev = pc2;
                    count++;
                    removed_count++;
                    if (removed_count >= HY08A_options.ob_count)
                    {
                        options.nopeep = 1;
                        return count;
                    }
                }
            }
        }

        //}
        // 2024 4.4.4
        if (isBIT2TEMPinv(pc,&thebit1,&thetemp1,&tail1))
        {
            if(hasContNextPCI(tail1,4) && 
            isTEMPBIT2BIT(tail1->next,&thebit2,&thetemp2,&tail2)
            )
            {
                if( thetemp1->type==PO_GPR_TEMP && PCOR(thetemp1)->r->rIdx ==
                PCORB(thetemp2)->pcor.r->rIdx &&
                    PCORB(thetemp2)->bit==0 
                    // &&pCodeOpCompare(thebit1,thebit2)
                    )
                {
                    // it is bit toggle
                    if(! findNextInstructionRefTemp(tail2->next,PCOR(thetemp1)->r->rIdx,0,NULL,0))
                    {
                        if(pCodeOpCompare(thebit1,thebit2))// same bit means bit toggle
                        {
                            pc1=pc->prev;
                            pc2=pc->next->next->next->next->next->next->next;
                            pCodeReplace(pc, newpCode(POC_BTGF, PCI(pc->next)->pcop));
                            removepCode(&pc2);
                            removepCode(&pc2);
                            removepCode(&pc2);
                            removepCode(&pc2);
                            removepCode(&pc2);
                            removepCode(&pc2);
                            removepCode(&pc2);
                        
                            count++;
                            removed_count++;
                            if (removed_count >= HY08A_options.ob_count)
                            {
                                options.nopeep = 1;
                                return count;
                            }
                            pc=pc1;
                            continue;
                        }
                        else // bit inv 2 bit
                        {
                            pc1=pc->prev;
                            pCodeReplace(pc1->next,newpCode(POC_BTSZ,thebit1));
                            pCodeReplace(pc1->next->next,
                                    newpCode(POC_BCF,thebit2));
                            pCodeReplace(pc1->next->next->next,
                                    newpCode(POC_BTSS,thebit1));
                            pCodeReplace(pc1->next->next->next->next,
                                    newpCode(POC_BSF,thebit2));
                            pc=pc1->next;
                            pc2=pc->next->next->next->next->next->next->next;
                            removepCode(&pc2);
                            removepCode(&pc2);
                            removepCode(&pc2);
                            removepCode(&pc2);
                            count++;
                            removed_count++;
                            if (removed_count >= HY08A_options.ob_count)
                            {
                                options.nopeep = 1;
                                return count;
                            }
                            pc=pc1;
                            continue;
                        }
                    }
                    
                }
            }
        }

        if (isBIT2TEMPCOMF(pc,&thebit1,&thetemp1,&tail1))
        {
            if(isTEMPBIT2BIT(tail1->next, &thebit2, &thetemp2, &tail2))
            {
                if( thetemp1->type==PO_GPR_TEMP && PCOR(thetemp1)->r->rIdx ==
                PCORB(thetemp2)->pcor.r->rIdx &&
                    PCORB(thetemp2)->bit==0 &&
                    ! findNextInstructionRefTemp(tail2->next,PCOR(thetemp1)->r->rIdx,0,NULL,0)
                    // 
                    )
                {
                        if(pCodeOpCompare(thebit1,thebit2))// same bit means bit toggle
                        {
                            pc1=pc->prev;
                            pc2=pc->next->next->next->next->next->next->next->next;
                            pCodeReplace(pc, newpCode(POC_BTGF, PCI(pc->next)->pcop));
                            removepCode(&pc2);
                            removepCode(&pc2);
                            removepCode(&pc2);
                            removepCode(&pc2);
                            removepCode(&pc2);
                            removepCode(&pc2);
                            removepCode(&pc2);
                            removepCode(&pc2);
                            count++;
                            removed_count++;
                            if (removed_count >= HY08A_options.ob_count)
                            {
                                options.nopeep = 1;
                                return count;
                            }
                            pc=pc1;
                            continue;
                        }
                        else // bit inv 2 bit
                        {
                            pc1=pc->prev;
                            pCodeReplace(pc1->next,newpCode(POC_BTSZ,thebit1));
                            pCodeReplace(pc1->next->next,
                                    newpCode(POC_BCF,thebit2));
                            pCodeReplace(pc1->next->next->next,
                                    newpCode(POC_BTSS,thebit1));
                            pCodeReplace(pc1->next->next->next->next,
                                    newpCode(POC_BSF,thebit2));
                            pc=pc1->next;
                            pc2=pc->next->next->next->next->next->next->next;
                            removepCode(&pc2);
                            removepCode(&pc2);
                            removepCode(&pc2);
                            removepCode(&pc2);
                            removepCode(&pc2);
                            count++;
                            removed_count++;
                            if (removed_count >= HY08A_options.ob_count)
                            {
                                options.nopeep = 1;
                                return count;
                            }
                            pc=pc1;
                            continue;
                        }

                }
            }
        }
        if (isBIT2CShort(pc, &thebit1, &tail1, &opcode)) // usually cse like <0
        {
            // 2021 new kind of rlcf because optimized
            /*
            BSF	_STATUS,4
    BTSS	(_v + 1),7
    BCF	_STATUS,4
    CLRF	r0x1086,0
    RLFC	r0x1086,0,0
    RRFC	_WREG,0,0
            */
            if (tail1->next && tail1->next->next && tail1->next->next->next && isPCI(tail1->next) && isPCI(tail1->next->next) &&
                isPCI(tail1->next->next->next) &&
                PCI(tail1->next)->op == POC_CLRF && PCI(tail1->next->next)->op == POC_RLCFW &&
                PCI(tail1->next)->label == NULL && PCI(tail1->next->next)->label == NULL &&
                PCI(tail1->next)->pcop->type == PO_GPR_TEMP && pCodeOpCompare(PCI(tail1->next)->pcop, PCI(tail1->next->next)->pcop) &&
                isW2BIT(tail1->next->next->next, &thebit2, &tail2))
            {
                pc1 = pc->prev;
                pc2 = pc->next->next;

                pCodeReplace(pc, newpCode(POC_BSF, thebit2));
                pCodeReplace(pc2, pc3 = newpCode(POC_BCF, thebit2));
                while (tail2->prev != pc3)
                    removepCode(&tail2);
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                pc = pc3;
            }

            else if (tail1->next && tail1->next->next && isPCI(tail1->next) && isPCI(tail1->next->next) &&
                     PCI(tail1->next)->op == POC_CLRF && PCI(tail1->next->next)->op == POC_RLCF &&
                     PCI(tail1->next)->label == NULL && PCI(tail1->next->next)->label == NULL &&
                     PCI(tail1->next)->pcop->type == PO_GPR_TEMP && pCodeOpCompare(PCI(tail1->next)->pcop, PCI(tail1->next->next)->pcop))
            {
                // simplify bit2temp
                pCode *pc1 = pc->prev;
                pCode *pc2 = pc->next;
                // pCode *pc3=tail1->next;
                pCode *pc4 = tail1->next->next;
                pCodeReplace(pc, newpCode(POC_CLRF, PCI(tail1->next)->pcop));
                pCodeReplace(pc2, newpCode((opcode == POC_BTSS) ? POC_BTSZ : POC_BTSS, PCI(pc2)->pcop));
                pCodeReplace(tail1, newpCode(POC_INF, PCI(tail1->next)->pcop));

                removepCode(&pc4);
                removepCode(&pc4);
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
                pc = pc1->next;
            }
        }

        // DECSZ/DCSZ optimization
        // if(hasContNextPCI(pc,3) && PCI(pc)->op == POC_MVFW &&
        //     PCI(pc->next->next)->op == POC_TFSZ &&
        //     !(PCI(pc->next)->outCond&PCC_W) && PCI(pc->next)->label==NULL &&
        //     PCI(pc->next->next)->label == NULL &&
        //     !PCI(pc->next)->isSkip &&
        //     !PCI(pc->next->next)->isBranch &&
        //     pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next->next)->pcop))
        // {
        //     // change to W
        //     pCodeReplace(pc->next->next, newpCode(POC_TFSZ, (pCodeOp*)&pc_wreg));
        //     count++;
        //     removed_count++;
        //     if (removed_count >= HY08A_options.ob_count)
        //     {
        //         options.nopeep = 1;
        //         return count;
        //     }
        // }
        // 2022 oct seems another form:

        // try
        // DCFW a
        // MVWF b
        // MVWF a
        // TFSZ b

        // to DCSZ a
        // for (pc = findNextInstruction(pb->pcHead); pc; pc = findNextInstruction(pc->next))
        //{
        if (hasContNextPCI(pc, 3) && isPCI(pc) && PCI(pc)->op == POC_DECFW &&
            PCI(pc->next)->op == POC_MVWF &&
            PCI(pc->next->next)->op == POC_MVWF &&
            // PCI(pc->next)->label == NULL &&
            // PCI(pc->next->next)->label == NULL &&
            // PCI(pc->next->next->next)->label == NULL &&
            PCI(pc->next->next->next)->op == POC_TFSZ &&
            pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next->next)->pcop) &&
            pCodeOpCompare(PCI(pc->next)->pcop, PCI(pc->next->next->next)->pcop) &&
            PCI(pc->next)->pcop->type == PO_GPR_TEMP)
        {
            // second operand should be ... used once?
            pCode *tmpp = pc->prev;
            pc1 = pc->next;
            pc2 = pc->next->next;
            pc3 = pc->next->next->next;
            pCodeReplace(pc, newpCode(POC_DCSZ, pCodeOpCopy(PCI(pc)->pcop)));
            pc = tmpp;
            removepCode(&pc1);
            removepCode(&pc2);
            removepCode(&pc3);

            count++;
            removed_count++;
            if (removed_count >= HY08A_options.ob_count)
            {
                options.nopeep = 1;
                return count;
            }
        }
        // 2022 oct TFSZ _wreg + JMP with prev w&z ==> replace tfsz with btss
        // because link may optimize it to jz

        if (hasContNextPCI(pc, 2) && isPCI(pc) && PCI(pc->next)->op == POC_TFSZ && !PCI(pc)->isBranch &&
            PCI(pc->next)->pcop->type == PO_W && PCI(pc->next->next)->op == POC_JMP &&
            ((PCI(pc)->outCond & (PCC_W | PCC_Z)) == (PCC_W | PCC_Z)))
        //&& PCI(pc->next)->label==NULL &&
        // PCI(pc->next->next)->label==NULL)
        {
            pCodeReplace(pc->next, newpCode(POC_BTSS, popCopyGPR2Bit(PCOP(&pc_status), HYA_Z_BIT)));
            count++;
            removed_count++;
            if (removed_count >= HY08A_options.ob_count)
            {
                options.nopeep = 1;
                return count;
            }
        }

        // 2022 oct different kind ++/--, MVFW A INF A/DECF A TFSZ A
        if (hasContNextPCI(pc, 2) && isPCI(pc) && PCI(pc)->op == POC_MVFW &&
            (PCI(pc->next)->op == POC_INF || PCI(pc->next)->op == POC_DECF) && PCI(pc->next->next)->op == POC_TFSZ &&
            PCI(pc->next->next)->pcop->type == PO_W &&
            pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next)->pcop)
            //&&
            // PCI(pc->next)->label == NULL &&
            // PCI(pc->next->next)->label == NULL
        )
        {
            if (PCI(pc->next)->op == POC_INF)
            {
                pCodeReplace(pc->next->next, newpCode(POC_DCSZW, PCI(pc)->pcop));
                removepCode(&pc);
            }
            else
            {
                pCodeReplace(pc->next->next, newpCode(POC_INSZW, PCI(pc)->pcop));
                removepCode(&pc);
            }
            count++;
            removed_count++;
            if (removed_count >= HY08A_options.ob_count)
            {
                options.nopeep = 1;
                return count;
            }
        }

        // ^=, |= optimization ... seems &= is done, we need not to do it

        // MVFW  X
        // MVWF  tmp
        // BSF/BCF   tmp,3
        // MVFW  tmp
        // MVWF  X
        // for (pc = findNextInstruction(pb->pcHead); pc; pc = findNextInstruction(pc->next))
        //{

        if ((isPCI(pc) && (PCI(pc)->op == POC_BTGF || PCI(pc)->op == POC_BSF || PCI(pc)->op == POC_BCF)) &&
            PCI(pc)->pcop->type == PO_GPR_BIT &&
            pc->prev && pc->prev->prev && isPCI(pc->prev) && isPCI(pc->prev->prev) &&
            pc->next && pc->next->next && isPCI(pc->next) && isPCI(pc->next->next))
        {
            // assume it will not be used anymore

            if (
                PCI(pc->prev->prev)->pcop->name &&
                PCI(pc->prev->prev)->op == POC_MVFW &&
                PCI(pc->prev)->op == POC_MVWF &&
                PCI(pc->next)->op == POC_MVFW &&
                PCI(pc->next->next)->op == POC_MVWF)
                if (pCodeOpCompare(PCI(pc->next)->pcop, PCI(pc->prev)->pcop) &&
                    pCodeOpCompare(PCI(pc->next->next)->pcop, PCI(pc->prev->prev)->pcop) &&
                    PCI(pc->next)->pcop->type == PO_GPR_TEMP && PCORB(PCI(pc)->pcop)->pcor.r &&
                    PCORB(PCI(pc)->pcop)->pcor.r->rIdx ==
                        PCOR(PCI(pc->next)->pcop)->r->rIdx &&

                    !findNextInstructionRefTemp(pc->next->next, PCOR(PCI(pc->next)->pcop)->rIdx, 0, NULL, 0) // give r as null , compare ridx is ok
                )
                {
                    PCI(pc)->pcop = newpCodeOpBitByOp(PCI(pc->prev->prev)->pcop, PCORB(PCI(pc)->pcop)->bit, 0);

                    // depends on the next instruction?
                    // previous have W as new value, so we keep the new value
                    // if (pc->next->next->next && isPCI(pc->next->next->next) && (PCI(pc->next->next->next)->inCond & PCC_W))
                    //{

                    insertPCodeInstruction((pCodeInstruction *)(pc->next), (pCodeInstruction *)newpCode(POC_MVFW, pCodeOpCopy(PCI(pc->prev->prev)->pcop)));
                    pc1 = pc->next;
                    pc = pc->next->next->next;
                    removepCode(&pc);
                    removepCode(&pc);
                    pc = pc->prev->prev;
                    removepCode(&pc);
                    removepCode(&pc);
                    if (chkNoUseW(pc1->next, 0))
                    {
                        removepCode(&pc1);
                    }

                    pc = pc->next->next->next;
                    /* }
                    else
                    {
                        pc = pc->next->next;
                        removepCode(&pc);
                        removepCode(&pc);
                        pc = pc->prev;
                        removepCode(&pc);
                        removepCode(&pc);
                        pc = pc->next->next;
                    }*/

                    count++;
                    removed_count++;
                    if (removed_count >= HY08A_options.ob_count)
                    {
                        options.nopeep = 1;
                        return count;
                    }
                }
        }
        //

        // 2022 FEB, MVWF a, BSF a,? MVFW a ==> ANL y MVWF a

        if ((pc->prev == NULL || !isPCI(pc->prev) || !PCI(pc->prev)->isSkip) &&
            hasContNextPCI(pc, 3) && isPCI(pc) &&
            // PCI(pc->next)->label==NULL &&
            // PCI(pc->next->next)->label == NULL &&
            PCI(pc)->op == POC_MVWF &&
            (PCI(pc->next)->op == POC_BTGF || PCI(pc->next)->op == POC_BSF || PCI(pc->next)->op == POC_BCF) &&
            PCI(pc->next->next)->op == POC_MVFW &&
            (PCI(pc)->pcop->type == PO_GPR_TEMP || PCI(pc)->pcop->type == PO_GPR_REGISTER) &&
            PCI(pc->next)->pcop->type == PO_GPR_BIT &&
            pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next->next)->pcop) &&
            PCORB(PCI(pc->next)->pcop)->pcor.rIdx == PCOR(PCI(pc)->pcop)->rIdx)
        {
            pCode *pc4;
            int bitn = PCORB(PCI(pc->next)->pcop)->bit;
            int lit = (PCI(pc->next)->op == POC_BTGF || PCI(pc->next)->op == POC_BSF) ? (1 << bitn) : 0xff ^ (1 << bitn);
            pc1 = NULL;

            switch (PCI(pc->next)->op)
            {
            case POC_BCF:
                pc1 = newpCode(POC_ANDLW, newpCodeOpLit(lit));
                break;
            case POC_BSF:
                pc1 = newpCode(POC_IORL, newpCodeOpLit(lit));
                break;
            case POC_BTGF:
                pc1 = newpCode(POC_XORLW, newpCodeOpLit(lit));
                break;
            default:
                break;
            }
            pc2 = pc->next;
            pc3 = pc->next->next;
            pc4 = newpCode(POC_MVWF, PCI(pc)->pcop);
            pCodeReplace(pc, pc1);
            pCodeReplace(pc2, pc4);
            pc = pc1;
            count++;
            removed_count++;
            if (removed_count >= HY08A_options.ob_count)
            {
                options.nopeep = 1;
                return count;
            }
        }
        // 2022 FEB
        // mvwf a clrf/comf/setf b ... ==> change their order, z flag no change
        if ((pc->prev == NULL || !isPCI(pc->prev) || !PCI(pc->prev)->isSkip) &&
            hasContNextPCI(pc, 2) && isPCI(pc) &&
            // PCI(pc->next)->label==NULL &&
            (PCI(pc)->op == POC_MVWF || PCI(pc)->op == POC_MVFW) &&
            (PCI(pc->next)->op == POC_COMF || PCI(pc->next)->op == POC_SETF || PCI(pc->next)->op == POC_CLRF) &&
            (PCI(pc)->pcop->type == PO_GPR_TEMP || PCI(pc)->pcop->type == PO_GPR_REGISTER) &&
            (PCI(pc->next)->pcop->type == PO_GPR_TEMP || PCI(pc->next)->pcop->type == PO_GPR_REGISTER || PCI(pc->next)->pcop->type == PO_SFR_REGISTER) &&
            !PCI(pc->next)->pcop->flags.parapcop && // order not change if para pcop!!
            !PCI(pc)->pcop->flags.parapcop &&
            !pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next)->pcop))
        {
            // swap them
            pc1 = newpCode(PCI(pc)->op, PCI(pc)->pcop);
            pc2 = newpCode(PCI(pc->next)->op, PCI(pc->next)->pcop);

            if (PCI(pc->next)->pcop->type == PO_SFR_REGISTER) // move to below
            {
                pc3 = pc->next->next;
                insertPCodeInstruction(PCI(pc3), PCI(pc1));
                removepCode(&pc);
                pc = pc1;
            }
            else
            {
                pCodeReplace(pc->next, pc1);
                pCodeReplace(pc, pc2);
                pc = pc2;
            }
            count++;
            removed_count++;
            if (removed_count >= HY08A_options.ob_count)
            {
                options.nopeep = 1;
                return count;
            }
        }

        //}

        // next is struct member operation optimizaton, like
        // ANDLW
        // ADDLW 1
        // ANDLW
        // for (pc = findNextInstruction(pb->pcHead); pc; pc = findNextInstruction(pc->next))
        //{
        if (pc->prev && pc->next && pc->next->next && isPCI(pc->next) && isPCI(pc->next->next) &&
            isPCI(pc->prev) && !PCI(pc->prev)->isSkip &&
            PCI(pc->next)->label == NULL &&
            PCI(pc)->op == POC_ANDLW && PCI(pc->next)->op == POC_ADDLW && PCI(pc->next->next)->op == POC_ANDLW &&
            PCI(pc)->pcop->type == PO_LITERAL && PCI(pc->next)->pcop->type == PO_LITERAL && PCI(pc->next->next)->pcop->type == PO_LITERAL)
        {
            int lit1 = PCOL(PCI(pc)->pcop)->lit;
            int lit2 = PCOL(PCI(pc->next)->pcop)->lit;
            int lit3 = PCOL(PCI(pc->next->next)->pcop)->lit;
            if (lit2 == 1 && lit1 == lit3 && (lit1 == 1 || lit1 == 3 || lit1 == 7 || lit1 == 0xf || lit1 == 0x1f || lit1 == 0x3f || lit1 == 0x7f))
            {
                removepCode(&pc);
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
            }
        }
        //}

        // when people write if( (a&k)==k ) and k is 1,2,4,..
        // it is compiled as MVFW, ANDLW, XORLW, BTSS/BTSZ it can be optized directly
        // for (pc = findNextInstruction(pb->pcHead); pc; pc = findNextInstruction(pc->next))
        //{
        if (!(pc->prev && isPCI(pc->prev) && PCI(pc->prev)->isSkip) &&
            pc->next && pc->next->next && pc->next->next->next &&
            isPCI(pc->next) && isPCI(pc->next->next) && isPCI(pc->next->next->next) &&
            PCI(pc->next)->label == NULL && PCI(pc->next->next)->label == NULL &&
            PCI(pc)->op == POC_MVFW && PCI(pc->next)->op == POC_ANDLW && PCI(pc->next->next)->op == POC_XORLW && PCI(pc)->pcop->name &&
            PCI(pc->next)->pcop->type == PO_LITERAL && PCI(pc->next->next)->pcop->type == PO_LITERAL &&
            (((PCI(pc->next->next->next)->op == POC_BTSS || PCI(pc->next->next->next)->op == POC_BTSZ) &&
              PCORB(PCI(pc->next->next->next)->pcop)->pcor.rIdx == IDX_STATUS) || // it must be Z!!)
             (PCI(pc->next->next->next)->op) == POC_TFSZ && PCI(pc->next->next->next)->pcop->type == PO_W))

        {
            int lit1 = PCOL(PCI(pc->next)->pcop)->lit;
            int lit2 = PCOL(PCI(pc->next->next)->pcop)->lit;
            if (lit1 == lit2 && (lit1 == 1 || lit1 == 2 || lit1 == 4 || lit1 == 8 || lit1 == 0x10 || lit1 == 0x20 || lit1 == 0x40 || lit1 == 0x80))
            {
                int bit = 0;
                pCodeOp *pcop;
                switch (lit1)
                {
                case 1:
                    bit = 0;
                    break;
                case 2:
                    bit = 1;
                    break;
                case 4:
                    bit = 2;
                    break;
                case 8:
                    bit = 3;
                    break;
                case 0x10:
                    bit = 4;
                    break;
                case 0x20:
                    bit = 5;
                    break;
                case 0x40:
                    bit = 6;
                    break;
                case 0x80:
                    bit = 7;
                    break;
                }
                pcop = (pCodeOp *)newpCodeOpBitByOp(PCI(pc)->pcop, bit, 0);
                if (PCI(pc->next->next->next)->op == POC_TFSZ)
                {
                    pCodeReplace(pc->next->next->next, newpCode(POC_BTSS, pcop));
                }
                else
                {
                    // just change it directly
                    PCI(pc->next->next->next)->pcop = pcop;
                }
                pc = pc->next->next;
                removepCode(&pc);
                removepCode(&pc);
                removepCode(&pc);
                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
            }
        }
        //}

        // sometimes var += literal becomes mvfw addlw mvwf,
        // and cases it can be optimized to mvlw addwf
        // the only exception is that following instruction must not reference W
        // later we also found it must not be MVFF, because MVFF is used to keep W!!

        // for (pc = findNextInstruction(pb->pcHead); pc; pc = findNextInstruction(pc->next))
        //{
        // pCode *pc1, *pc2, *pc3;
        if (isPCI(pc) && pc->next && pc->next->next && pc->next->next->next && isPCI(pc->next) &&
            isPCI(pc->next->next) && isPCI(pc->next->next->next) && !(PCI(pc)->isSkip))
        {
            pc1 = pc->next;
            pc2 = pc1->next;
            pc3 = pc2->next;

            // some changes
            if (PCI(pc1)->op == POC_MVFW && PCI(pc2)->op == POC_IORFW && PCI(pc3)->op == POC_MVWF &&
                pCodeOpCompare(PCI(pc2)->pcop, PCI(pc3)->pcop))
            {
                pCodeReplace(pc2, newpCode(POC_IORWF, PCI(pc2)->pcop));
                pCodeReplace(pc3, newpCode(POC_MVFW, PCI(pc3)->pcop));
            }

            if (PCI(pc1)->op == POC_MVFW && PCI(pc2)->op == POC_ANDFW && PCI(pc3)->op == POC_MVWF &&
                pCodeOpCompare(PCI(pc2)->pcop, PCI(pc3)->pcop))
            {
                pCodeReplace(pc2, newpCode(POC_ANDWF, PCI(pc2)->pcop));
                pCodeReplace(pc3, newpCode(POC_MVFW, PCI(pc3)->pcop));
            }

            if (pc3->next && isPCI(pc3->next) && ((PCI(pc3->next)->inCond & PCC_W) || PCI(pc3->next)->op == POC_MVFF || (PCI(pc3->next)->pcop && PCI(pc3->next)->pcop->type == PO_W)))
                continue;

            if (PCI(pc1)->op == POC_MVFW && PCI(pc2)->op == POC_ADDLW && PCI(pc3)->op == POC_MVWF &&
                pCodeOpCompare(PCI(pc1)->pcop, PCI(pc3)->pcop))
            {

                pCodeReplace(pc2, newpCode(POC_MVL, PCI(pc2)->pcop));
                pCodeReplace(pc3, newpCode(POC_ADDWF, PCI(pc3)->pcop));

                removepCode(&pc1);

                count++;
                removed_count++;
                if (removed_count >= HY08A_options.ob_count)
                {
                    options.nopeep = 1;
                    return count;
                }
            }
        }
        //}

        // RLCFW+MOVWF, with following not W related

        // for (pc = findNextInstruction(pb->pcHead); pc; pc = findNextInstruction(pc->next))
        //{
        // pCode *pc1;
        if (pc->next && pc->next->next && (!isPCI(pc) || !(PCI(pc)->isSkip)) && PCI(pc->next)->op == POC_RLCFW &&
            PCI(pc->next->next)->op == POC_MVWF && pCodeOpCompare(PCI(pc->next)->pcop, PCI(pc->next->next)->pcop) &&
            PCI(pc->next->next)->label == NULL &&
            ((!isPCI(pc->next->next->next)) || !(PCI(pc->next->next->next)->inCond & PCC_W)))
        {
            pCodeReplace(pc->next, newpCode(POC_RLCF, PCI(pc->next)->pcop));
            pc1 = pc->next->next;
            removepCode(&pc1);
            count++;
            removed_count++;
            if (removed_count >= HY08A_options.ob_count)
            {
                options.nopeep = 1;
                return count;
            }
        }

        // 2022 FEB optimize MVWF x BCF/BSF RLFCW/RRFCW x to _WREG
        if ((!(pc->prev) || !isPCI(pc->prev) || !(PCI(pc->prev)->isSkip)) && isPCI(pc) && hasContNextPCI(pc, 2) && PCI(pc)->op == POC_MVWF && PCI(pc)->pcop->type == PO_GPR_TEMP &&
            (PCI(pc->next)->op == POC_BSF || PCI(pc->next)->op == POC_BCF) &&
            (PCI(pc->next)->outCond & PCC_C) &&
            (PCI(pc->next->next)->op == POC_RLCFW || PCI(pc->next->next)->op == POC_RRCFW) &&
            PCI(pc->next)->label == NULL && PCI(pc->next->next)->label == NULL &&
            pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next->next)->pcop))
        {
            pc1 = newpCode(PCI(pc->next->next)->op, PCOP(&pc_wreg));
            pCodeReplace(pc->next->next, pc1);
            count++;
            removed_count++;
            if (removed_count >= HY08A_options.ob_count)
            {
                options.nopeep = 1;
                return count;
            }
            continue;
        }
        // above rule generate MVFW BCF/BSF RLFCW x to mvwf x optimization
        if ((!(pc->prev) || !isPCI(pc->prev) || !(PCI(pc->prev)->isSkip))
            // following method skip first label
            && isPCI(pc) && hasContNextPCI(pc, 3) && PCI(pc)->op == POC_MVFW && // PCI(pc)->pcop->type == PO_GPR_TEMP &&
            (PCI(pc->next)->op == POC_BSF || PCI(pc->next)->op == POC_BCF) &&
            (PCI(pc->next)->outCond & PCC_C) &&
            (PCI(pc->next->next)->op == POC_RLCFW || PCI(pc->next->next)->op == POC_RRCFW) &&
            PCI(pc->next->next)->pcop->type == PO_W &&
            PCI(pc->next->next->next)->op == POC_MVWF &&
            PCI(pc->next)->label == NULL && PCI(pc->next->next)->label == NULL &&
            PCI(pc->next->next->next)->label == NULL &&
            pCodeOpCompare(PCI(pc)->pcop, PCI(pc->next->next->next)->pcop))
        {
            if (PCI(pc->next->next)->op == POC_RLCFW)
                pc1 = newpCode(POC_RLCF, PCI(pc)->pcop);
            else
                pc1 = newpCode(POC_RRCF, PCI(pc)->pcop);
            pc3 = newpCode(POC_MVFW, PCI(pc)->pcop);
            pCodeReplace(pc->next->next, pc1);
            pCodeReplace(pc->next->next->next, pc3);
            removepCode(&pc);
            count++;
            removed_count++;
            if (removed_count >= HY08A_options.ob_count)
            {
                options.nopeep = 1;
                return count;
            }
            continue;
        }
        // 2022 FEB opt MVWF a BTXX YYY MVFW a ==> the next mvfw a can be skip if only yyy no destroy W
    }

    // mvfw twmp + mvwf tmp if only previous is not skip

    return count;
}

static int jnnOptimize(pBlock *pb) // special case JN
{
    // we check "JMP with previous XXX,7, to see if it can be promote as ..N
    pCode *pcj, *pcbtxx, *opern;
    int count = 0;
    int found = 0;

    for (pcj = pb->pcHead; pcj; pcj = pcj->next)
    {
        if (!isPCI(pcj) || PCI(pcj)->op != POC_JMP)
            continue;
        pcbtxx = findPrevInstruction(pcj->prev);
        if (!pcbtxx || (PCI(pcbtxx)->op != POC_BTSS && PCI(pcbtxx)->op != POC_BTSZ) || PCI(pcbtxx)->pcop->type != PO_GPR_BIT ||
            PCORB(PCI(pcbtxx)->pcop)->bit != 7 || PCI(pcbtxx)->label || (PCORB(PCI(pcbtxx)->pcop)->pcor.r == NULL)) // sfr is fixed
            continue;
        found = 0;
        for (opern = findPrevInstruction(pcbtxx->prev); opern; opern = findPrevInstruction(opern->prev))
        {
            pCodeOp *pcop;
            if (!PCI(opern)->pcop)
            {
                continue;
            }
            if (PCI(opern)->isBranch)
                break;
            pcop = PCI(opern)->pcop;
            if ((pcop->type == PO_GPR_REGISTER || pcop->type == PO_GPR_TEMP) &&
                PCOR(pcop)->r->rIdx == PCORB(PCI(pcbtxx)->pcop)->pcor.r->rIdx &&
                PCI(opern)->outCond & PCC_N)
            {
                pCode *pp = findPrevInstruction(opern->prev);
                if (pp && PCI(pp)->isSkip)
                    break;
                found = 1;
                break;
            }
            if (PCI(opern)->label || (PCI(opern)->outCond & PCC_N))
                break; // fail
        }
        if (found)
        {
            count++;
            PCI(pcbtxx)->pcop = newpCodeOpBit("_STATUS", 2, 0);
        }
    }
    return count;
}

static int replaceLabelPtrTabInPb(pBlock *pb, pCode *old, pCode *newp)
{
    int i;
    int n = 0;
    if (!pb)
        return 0;
    if (!isPCI(old))
        return 0;
    if (PCI(old)->label == NULL)
        return 0;
    for (i = 0; i < pb->labelTabSize; i++)
    {
        if (pb->labelPtrTab[i] == old)
        {
            pb->labelPtrTab[i] = newp;
            n++;
        }
    }
    return n;
}
