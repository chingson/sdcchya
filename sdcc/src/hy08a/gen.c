/*-------------------------------------------------------------------------
  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

  2016/2017: modified by chingsonchen@hycontek.com.tw for HYCON 8-bit CPU
-------------------------------------------------------------------------*/
//#define DEBUGLINENO 36
#ifndef _WIN32
//#define DEBUGLINENO 46
#endif


#define NEARPOINTER_DIR 1
/*
  Notes:
  000123 mlh  Moved aopLiteral to SDCCglue.c to help the split
              Made everything static
*/

/*
 * This is the down and dirty file with all kinds of
 * kludgy & hacky stuff. This is what it is all about
 * CODE GENERATION for a specific MCU . some of the
 * routines may be reusable, will have to see.
 */

#define SIGN_CMP_USE_OV

#include "device.h"
#include "gen.h"
#include "glue.h"
#include "dbuf_string.h"

/*
 * Imports
 */
extern struct dbuf_s *codeOutBuf;
extern set *externs;
extern pCodeOp *newpCodeOpReg(int rIdx);

static pCodeOp *popGetImmd (char *name, unsigned int offset, int index, int is_func, int size, int inX, pBlock *pb);
static pCodeOp *popRegFromString (char *str, int size, int offset);
static int aop_isLitLike (asmop * aop);
static void genCritical (iCode * ic);
static void genEndCritical (iCode * ic);
static asmop *newAsmop(short type);
static void setup_fsr(operand * ptr);

static void genAssign(iCode * ic);

static int canAssignSelf(char *regname);


//static void
// genAssign2(iCode * ic, int offset, int offset3);
// HYCON need special
#define HYA_IS_DATA_PTR(x)  (IS_DATA_PTR(x) || IS_FARPTR(x))

/*
 * max_key keeps track of the largest label number used in
 * a function. This is then used to adjust the label offset
 * for the next function.
 */
static int max_key = 0;
static int labelOffset = 0;
static int GpsuedoStkPtr = 0;
static int HY08A_inISR = 0;

static char *zero = "0x00";
static char *one = "0x01";
//static char *spname = "sp";

unsigned fReturnSizeHYA = 4;    /* shared with ralloc.c */
static char *fReturnHY08A[] = { "temp1", "temp2", "temp3", "temp4" };

static char **fReturn = fReturnHY08A;

static int var_param_len = 0; // variable parameter length

static struct
{
    short accInUse;
    short nRegsSaved;
    set *sendSet;
} _G;

/*
 * Resolved ifx structure. This structure stores information
 * about an iCode ifx that makes it easier to generate code.
 */
typedef struct resolvedIfx
{
    symbol *lbl;                  /* pointer to a label */
    int condition;                /* true or false ifx */
    int generated;                /* set true when the code associated with the ifx
                                 * is generated */
} resolvedIfx;

static pBlock *pb;


// in_x=1
#define XDATAA2PTR(x) 	emitpcode(POC_LDPR,popGetImmd( AOP(x)->aopu.aop_dir,0,0,0,1,1,0));
#define PUSHW emitpcode(POC_MVWF, PCOP(&pc_princ2))



/*-----------------------------------------------------------------*/
/*  my_powof2(n) - If `n' is an integaer power of 2, then the      */
/*                 exponent of 2 is returned, otherwise -1 is      */
/*                 returned.                                       */
/* note that this is similar to the function `powof2' in SDCCsymt  */
/* if(n == 2^y)                                                    */
/*   return y;                                                     */
/* return -1;                                                      */
/*-----------------------------------------------------------------*/

static iCode * // special ifxForOpSpecial, not mixed with sdcc old version
ifxForOpSpecial(operand * op, const iCode * ic)
{
	iCode *ifxIc;

	/* if true symbol then needs to be assigned */
	if (!IS_TRUE_SYMOP(op))
	{
		/* if this has register type condition and
		while skipping ipop's (see bug 1509084),
		the next instruction is ifx with the same operand
		and live to of the operand is upto the ifx only then */
		for (ifxIc = ic->next; ifxIc && ifxIc->op == IPOP; ifxIc = ifxIc->next)
			;

		if (ifxIc && ifxIc->op == IFX &&
			IC_COND(ifxIc)->key == op->key &&
			OP_SYMBOL(op)->liveFrom >= ic->seq &&
			OP_SYMBOL(op)->liveTo <= ifxIc->seq)
			return ifxIc;
		// for bug-2627.c if there is no ifx, but the result used more than once
		// we need to assign AOP
		if (ifxIc && ifxIc->op == IFX &&
			IC_COND(ifxIc)->key == op->key &&
			IC_COND(ifxIc)->aop == NULL &&
			(OP_SYMBOL(op)->liveFrom < ic->seq ||
				OP_SYMBOL(op)->liveTo > ifxIc->seq))
		{
			operand *cond = IC_COND(ifxIc);
			symbol *sym = OP_SYMBOL(cond);
			if (sym->aop)
			{
				cond->aop = sym->aop;
				return NULL;
			}
			cond->aop = sym->aop = newAsmop(AOP_REG);
			
			if (sym->nRegs == 0)
			{
				sym->nRegs = 1;
				cond->aop->size = 1;
				sym->regs[0] = HY08A_findFreeReg(REG_GPR);
				cond->aop->aopu.aop_reg[0] = sym->regs[0];
			}
			else
			{
				int i;
				cond->aop->size = sym->nRegs;
				//cond->aop->aopu.aop_reg[0] = sym->regs[0];
				for (i = 0; i < sym->nRegs; i++)
					cond->aop->aopu.aop_reg[i] = sym->regs[i];
			}
		}
		
	}

	return NULL;
}

static int
my_powof2 (unsigned long num)
{
    if (num)
    {
        if ((num & (num - 1)) == 0)
        {
            int nshifts = -1;
            while (num)
            {
                num >>= 1;
                nshifts++;
            }
            return nshifts;
        }
    }

    return -1;
}


#ifdef DEBUG_
void
DEBUGHY08A_AopType (int line_no, operand * left, operand * right, operand * result)
{

    DEBUGHY08A_emitcode ("; ", "line = %d result %s=%s, size=%d, left %s=%s, size=%d, right %s=%s, size=%d",
                         line_no,
                         ((result) ? AopType (AOP_TYPE (result)) : "-"),
                         ((result) ? aopGet (AOP (result), 0, TRUE, FALSE) : "-"),
                         ((result) ? AOP_SIZE (result) : 0),
                         ((left) ? AopType (AOP_TYPE (left)) : "-"),
                         ((left) ? aopGet (AOP (left), 0, TRUE, FALSE) : "-"),
                         ((left) ? AOP_SIZE (left) : 0),
                         ((right) ? AopType (AOP_TYPE (right)) : "-"),
                         ((right) ? aopGet (AOP (right), 0, FALSE, FALSE) : "-"), ((right) ? AOP_SIZE (right) : 0));

}

static void
DEBUGHY08A_AopTypeSign (int line_no, operand * left, operand * right, operand * result)
{

    DEBUGHY08A_emitcode ("; ", "line = %d, signs: result %s=%c, left %s=%c, right %s=%c",
                         line_no,
                         ((result) ? AopType (AOP_TYPE (result)) : "-"),
                         ((result) ? (SPEC_USIGN (operandType (result)) ? 'u' : 's') : '-'),
                             ((left) ? AopType (AOP_TYPE (left)) : "-"),
                             ((left) ? (SPEC_USIGN (operandType (left)) ? 'u' : 's') : '-'),
                             ((right) ? AopType (AOP_TYPE (right)) : "-"),
                             ((right) ? (SPEC_USIGN (operandType (right)) ? 'u' : 's') : '-'));

}

void
DEBUGHY08A_emitcode (char *inst, char *fmt, ...)
{
    va_list ap;

    if (!debug_verbose && !options.debug)
        return;

    va_start (ap, fmt);
    va_emitcode (inst, fmt, ap);
    va_end (ap);

    addpCode2pBlock (pb, newpCodeCharP (genLine.lineCurr->line));
}
#endif

void
emitpComment (const char *fmt, ...)
{
#ifdef DEBUG_
    va_list ap;
    struct dbuf_s dbuf;
    const char *line;

    dbuf_init (&dbuf, INITIAL_INLINEASM);

    dbuf_append_char (&dbuf, ';');
    va_start (ap, fmt);
    dbuf_vprintf (&dbuf, fmt, ap);
    va_end (ap);

    line = dbuf_detach_c_str (&dbuf);
    emit_raw (line);
    dbuf_free (line);

    addpCode2pBlock (pb, newpCodeCharP (genLine.lineCurr->line));
#endif
}

void
emitpLabel (int key)
{
	pCode *pc = newpCodeLabel(NULL, labelKey2num(key + labelOffset));
    addpCode2pBlock (pb, pc);
	pb->labelIndexTab[pb->labelTabSize] =(( pCodeLabel*)(pc))->key;
	pb->labelPtrTab[pb->labelTabSize++] = pc;
	if (pb->labelTabSize >= MAX_LAB_COUNT)
	{
		fprintf(stderr, "Internal Error, too many labels in pblock at %s:%d\n", __FILE__, __LINE__);
		exit(-__LINE__);
	}
}

/* gen.h defines a macro emitpcode that should be used to call emitpcode
 * as this allows for easy debugging (ever asked the question: where was
 * this instruction geenrated? Here is the answer... */
void
emitpcode_real (HYA_OPCODE poc, pCodeOp * pcop)
{
	
    if (pcop)
        addpCode2pBlock (pb, newpCode (poc, pcop));
    else
    {
        static int has_warned = 0;

        DEBUGHY08A_emitcode (";", "%s  ignoring NULL pcop", __FUNCTION__);
        if (!has_warned)
        {
            has_warned = 1;
            fprintf (stderr, "WARNING: encountered NULL pcop--this is probably a compiler bug...\n");
        }
    }
}
void
emitpcode_real2(HYA_OPCODE poc, pCodeOp * pcop, pCodeOp *pcop2, int whichx)
{
	if (pcop && pcop2)
	{
		addpCode2pBlock(pb, newpCode2(poc, pcop, pcop2));
		if (whichx & 1)
			PCI(pb->pcTail)->pcop->flags.isX = 1;
		if (whichx & 2)
			PCI(pb->pcTail)->pcop2->flags.isX = 1;
	}
    else
    {
        static int has_warned = 0;

        DEBUGHY08A_emitcode(";", "%s  ignoring NULL pcop", __FUNCTION__);
        if (!has_warned)
        {
            has_warned = 1;
            fprintf(stderr, "WARNING: encountered NULL pcop--this is probably a compiler bug...\n");
        }
    }
}

static void
emitpcodeNULLop (HYA_OPCODE poc)
{
    addpCode2pBlock (pb, newpCode (poc, NULL));
}

/*-----------------------------------------------------------------*/
/* HY08A_emitcode - writes the code into a file : for now it is simple    */
/*-----------------------------------------------------------------*/
#ifdef DEBUG_
void
HY08A_emitcode (char *inst, char *fmt, ...)
{
    va_list ap;

    va_start (ap, fmt);
    va_emitcode (inst, fmt, ap);
    va_end (ap);

    if (debug_verbose)
        addpCode2pBlock (pb, newpCodeCharP (genLine.lineCurr->line));

}

/*-----------------------------------------------------------------*/
/* HY08A_emitDebuggerSymbol - associate the current code location  */
/*   with a debugger symbol                                        */
/*-----------------------------------------------------------------*/
void
HY08A_emitDebuggerSymbol (const char *debugSym)
{
    genLine.lineElement.isDebug = 1;
    HY08A_emitcode ("", ";%s ==.", debugSym);
    genLine.lineElement.isDebug = 0;
}
#endif
/*-----------------------------------------------------------------*/
/* newAsmop - creates a new asmOp                                  */
/*-----------------------------------------------------------------*/
static asmop *
newAsmop (short type)
{
    asmop *aop;

    aop = Safe_calloc (1, sizeof (asmop));
    aop->type = type;
    return aop;
}

/*-----------------------------------------------------------------*/
/* resolveIfx - converts an iCode ifx into a form more useful for  */
/*              generating code                                    */
/*-----------------------------------------------------------------*/
static void
resolveIfx (resolvedIfx * resIfx, iCode * ifx)
{
    if (!resIfx)
        return;

    //  DEBUGHY08A_emitcode("; ***","%s %d",__FUNCTION__,__LINE__);

    resIfx->condition = 1;        /* assume that the ifx is true */
    resIfx->generated = 0;        /* indicate that the ifx has not been used */

    if (!ifx)
    {
        resIfx->lbl = NULL;       /* this is wrong: newiTempLabel(NULL);  / * oops, there is no ifx. so create a label */
    }
    else
    {
        if (IC_TRUE (ifx))
        {
            resIfx->lbl = IC_TRUE (ifx);
        }
        else
        {
            resIfx->lbl = IC_FALSE (ifx);
            resIfx->condition = 0;
        }
    }

    //  DEBUGHY08A_emitcode("; ***","%s lbl->key=%d, (lab offset=%d)",__FUNCTION__,resIfx->lbl->key,labelOffset);

}

/*-----------------------------------------------------------------*/
/* aopForSym - for a true symbol                                   */
/*-----------------------------------------------------------------*/
static asmop *
aopForSym (iCode * ic, symbol * sym, bool result)
{
    asmop *aop;
    memmap *space = SPEC_OCLS (sym->etype);

    DEBUGHY08A_emitcode ("; ***", "%s %d", __FUNCTION__, __LINE__);
    /* if already has one */
    if (sym->aop)
        return sym->aop;
	//sym->used = 1; // mark it as used
    //DEBUGHY08A_emitcode(";","%d",__LINE__);
    /* if it is in direct space */
    if (IN_XSPACE(space))
    {
        sym->aop = aop = newAsmop(AOP_XDATA);
        aop->aopu.aop_dir = sym->rname;
        aop->size = getSize(sym->type);
        aop->isvolatile = isVolatile(sym->type);

        // XSPACE has no volatile one
        //aop->isvolatile = isVolatile(sym->type);
        DEBUGHY08A_emitcode(";", "%d sym->rname = %s, size = %d, xdata", __LINE__, sym->rname, aop->size);

        return aop;

    }
    if (IN_DIRSPACE (space))
    {
		//if (IS_BIT(sym->type) && !(sym->ival)) // ival treat as char --> chingson modify 2017 MARCH!!
		if (IS_BIT(sym->type)) // try to support bit val init
		{
			sym->aop = aop = newAsmop(AOP_CRY); // bit var!!
			aop->aopu.aop_dir = sym->rname;
			aop->isvolatile = isVolatile(sym->type);
			aop->size = 1; // a it is not a byte
		}
		else
		{
			sym->aop = aop = newAsmop(AOP_DIR);
			// special special case stack no ini stru?
			if (strlen(sym->rname) == 0 && sym->ismyparm ) // special stack parameter struct
			{
				aop->type = AOP_STK;
				aop->aopu.aop_stk = sym->stack;
				//static kk = 0;
				//snprintf(sym->rname, sizeof(sym->rname)-1,"_%s_%d_", sym->name, sym->lineDef);// it must be local
				aop->size = getSize(sym->type);
				return aop;
				
			}
			aop->aopu.aop_dir = sym->rname;
			aop->size = getSize(sym->type);
			aop->isvolatile = isVolatile(sym->type);
		}
        DEBUGHY08A_emitcode (";", "%d sym->rname = %s, size = %d", __LINE__, sym->rname, aop->size);
        return aop;
    }
    /* special case for a function */
    if (IS_FUNC (sym->type))
    {

        sym->aop = aop = newAsmop (AOP_PCODE);
        aop->aopu.pcop = popGetImmd (sym->rname, 0, 0, 1, 1,0,NULL);// default is 1
        PCOI (aop->aopu.pcop)->_const = IN_CODESPACE (space);
        PCOI(aop->aopu.pcop)->_inX = IN_XSPACE(space);
        PCOI (aop->aopu.pcop)->_function = 1;
        PCOI (aop->aopu.pcop)->index = 0;
        aop->size = FPTRSIZE;
        DEBUGHY08A_emitcode (";", "%d size = %d, name =%s", __LINE__, aop->size, sym->rname);
        return aop;
    }

    if (IS_ARRAY (sym->type))
    {
        sym->aop = aop = newAsmop (AOP_PCODE);
        aop->aopu.pcop = popGetImmd (sym->rname, 0, 0, 1, getSize(sym->type), IN_XSPACE(space),  sym->onStack?pb:NULL);
        PCOI (aop->aopu.pcop)->_const = IN_CODESPACE (space);
        PCOI(aop->aopu.pcop)->_inX = IN_XSPACE(space);
        PCOI (aop->aopu.pcop)->_function = 0;
        PCOI (aop->aopu.pcop)->index = 0;
        aop->size = (short) getSize (sym->etype) * (short)DCL_ELEM (sym->type);

        DEBUGHY08A_emitcode (";", "%d size = %d, name =%s", __LINE__, aop->size, sym->rname);
        return aop;
    }

    /* only remaining is far space */
    /* in which case DPTR gets the address */
    sym->aop = aop = newAsmop (AOP_PCODE);

    aop->aopu.pcop = popGetImmd (sym->rname, 0, 0, 0,1, IN_XSPACE(space),sym->onStack?pb:NULL);
    PCOI (aop->aopu.pcop)->_const = IN_CODESPACE (space) ;
        
    PCOI(aop->aopu.pcop)->_inX = IN_XSPACE(space);
    PCOI (aop->aopu.pcop)->index = 0;

    DEBUGHY08A_emitcode (";", "%d: rname %s, val %d, const = %d", __LINE__, sym->rname, 0, PCOI (aop->aopu.pcop)->_const);

    allocDirReg (IC_LEFT (ic));

    aop->size = FPTRSIZE;

    /* if it is in code space */
    if (IN_CODESPACE (space) )
        aop->code = 1;

    return aop;
}

/*-----------------------------------------------------------------*/
/* aopForRemat - rematerialzes an object                           */
/*-----------------------------------------------------------------*/
//extern int isStatic(sym_link *type);
int isStatic(sym_link * type)
{
	if(!type)
		return 0;
	while(IS_ARRAY(type))
		type = type->next;
	return IS_STATIC(type);
}
static asmop *
aopForRemat (operand * op, int inX, bool paramPass)      // x symbol *sym)
{
    symbol *sym = OP_SYMBOL (op);
    iCode *ic = NULL;
    asmop *aop = newAsmop (AOP_PCODE);
    int val = 0;
    int isArray = 0;
	int isPtr = 0;
    int isStruct = 0;
    int size;
    int topLev=1;
    symbol *opsym;

    ic = sym->rematiCode;

    DEBUGHY08A_emitcode (";", "%s %d", __FUNCTION__, __LINE__);
    if (IS_OP_POINTER (op))
    {
        DEBUGHY08A_emitcode (";", "%s %d IS_OP_POINTER", __FUNCTION__, __LINE__);
    }
    for (;;)
    {
        if (ic->op == '+')
        {
            val += (int) operandLitValue (IC_RIGHT (ic));
            topLev=0;
        }
        else if (ic->op == '-')
        {
            val -= (int) operandLitValue (IC_RIGHT (ic));
            topLev=0;
        }
        else
            break;

        ic = OP_SYMBOL (IC_LEFT (ic))->rematiCode;

    }
    opsym = OP_SYMBOL(IC_LEFT(ic));
    isArray = IS_ARRAY(opsym->type);
	isPtr = IS_PTR(opsym->type);
    isStruct = IS_STRUCT(opsym->type);
    

    if (opsym->type->next && !isPtr)
        size = getSize(opsym->type->next);
    else
        size = getSize(opsym->type);
    if (isArray)
        size *= opsym->type->select.d.num_elem;

    // remat shall not to X
    aop->aopu.pcop = popGetImmd (OP_SYMBOL (IC_LEFT (ic))->rname, 0, val, 0, size , inX,
                                 (!IS_CONSTANT(opsym->type) && !isStatic(opsym->type))&&(opsym->onStack||opsym->islocal)?pb:NULL);

    //PCOI(aop->aopu.pcop)->r->size=getSize(sym->etype);// type can be pointer, remat is the pointed one
    
    PCOI (aop->aopu.pcop)->_const = IS_PTR_CONST (operandType (op));
    PCOI (aop->aopu.pcop)->index = val;

    

    DEBUGHY08A_emitcode (";", "%d: rname %s, val %d, const = %d",
                         __LINE__, OP_SYMBOL (IC_LEFT (ic))->rname, val, IS_PTR_CONST (operandType (op)));

    //  DEBUGHY08A_emitcode(";","aop type  %s",AopType(AOP_TYPE(IC_LEFT(ic))));

    allocDirReg (IC_LEFT (ic));
    if(isStruct && topLev && paramPass)
    {
        aop->imm2dir=1; // use dir from imm?
        aop->size= size;
    }

    return aop;
}

static int
aopIdx (asmop * aop, int offset)
{
    if (!aop)
        return -1;

    if (aop->type != AOP_REG)
        return -2;

    return aop->aopu.aop_reg[offset]->rIdx;

}

/*-----------------------------------------------------------------*/
/* regsInCommon - two operands have some registers in common       */
/*-----------------------------------------------------------------*/
static bool
regsInCommon (operand * op1, operand * op2)
{
    symbol *sym1, *sym2;
    int i;

    /* if they have registers in common */
    if (!IS_SYMOP (op1) || !IS_SYMOP (op2))
        return FALSE;

    sym1 = OP_SYMBOL (op1);
    sym2 = OP_SYMBOL (op2);

    if (sym1->nRegs == 0 || sym2->nRegs == 0)
        return FALSE;

    for (i = 0; i < sym1->nRegs; i++)
    {
        int j;
        if (!sym1->regs[i])
            continue;

        for (j = 0; j < sym2->nRegs; j++)
        {
            if (!sym2->regs[j])
                continue;

            if (sym2->regs[j] == sym1->regs[i])
                return TRUE;
        }
    }

    return FALSE;
}

/*-----------------------------------------------------------------*/
/* operandsEqu - equivalent                                        */
/*-----------------------------------------------------------------*/
static bool
operandsEqu (operand * op1, operand * op2)
{
    symbol *sym1, *sym2;

    /* if they not symbols */
    if (!IS_SYMOP (op1) || !IS_SYMOP (op2))
        return FALSE;

    sym1 = OP_SYMBOL (op1);
    sym2 = OP_SYMBOL (op2);

    /* if both are itemps & one is spilt
       and the other is not then false */
    if (IS_ITEMP (op1) && IS_ITEMP (op2) && sym1->isspilt != sym2->isspilt)
        return FALSE;

    /* if they are the same */
    //if (sym1->rname && canAssignSelf(sym1->rname))
    if (sym1 && canAssignSelf(sym1->rname))
        return FALSE;

    if (sym1 == sym2)
        return TRUE;

    if (sym1->rname[0] && sym2->rname[0] && strcmp (sym1->rname, sym2->rname) == 0)
        return TRUE;


    /* if left is a tmp & right is not */
    if (IS_ITEMP (op1) && !IS_ITEMP (op2) && sym1->isspilt && (sym1->usl.spillLoc == sym2))
        return TRUE;

    if (IS_ITEMP (op2) && !IS_ITEMP (op1) && sym2->isspilt && sym1->level > 0 && (sym2->usl.spillLoc == sym1))
        return TRUE;

    return FALSE;
}

/*-----------------------------------------------------------------*/
/* HY08A_sameRegs - two asmops have the same registers             */
/*-----------------------------------------------------------------*/
// 2023, change to binary, bit means corresponding bit
bool
HY08A_sameRegs (asmop * aop1, asmop * aop2)
{
    int i;

	if (aop1 == aop2)
		return TRUE;

    if (aop1->type != AOP_REG || aop2->type != AOP_REG)
        return FALSE;

    if (aop1->size != aop2->size)
        return FALSE;

	
	for (i = 0; i < aop1->size; i++)
		if (aop1->aopu.aop_reg[i]->rIdx != aop2->aopu.aop_reg[i]->rIdx)
			return FALSE;

	return TRUE;
}

bool
HY08A_sameRegByte(asmop * aop1, asmop * aop2, int offset)
{

	if (aop1 == aop2)
		return TRUE;

	if (aop1->type != AOP_REG || aop2->type != AOP_REG)
		return FALSE;

	if (aop1->size != aop2->size)
		return FALSE;

	if (aop1->aopu.aop_reg[offset]->rIdx == aop2->aopu.aop_reg[offset]->rIdx)
		return TRUE;

	return FALSE;
}


/*-----------------------------------------------------------------*/
/* aopOp - allocates an asmop for an operand  :                    */
/*-----------------------------------------------------------------*/

// 2022 aopOpSend branched for struct parameter 

static void
aopOpSend (operand * op, iCode * ic, bool result, bool sendStruct)
{
    asmop *aop;
    symbol *sym;
    int i;

    if (!op)
        return;

    /* if this a literal */
    if (IS_OP_LITERAL (op))
    {
        op->aop = aop = newAsmop (AOP_LIT);
        aop->aopu.aop_lit = OP_VALUE (op);
        aop->size = getSize (operandType (op));
        return;
    }

    {
        sym_link *type = operandType (op);
        if (IS_PTR_CONST (type))
            DEBUGHY08A_emitcode (";", "%d aop type is const pointer", __LINE__);
    }

    /* if already has a asmop then continue */
    if (op->aop)
        return;

    /* if the underlying symbol has a aop */
    if (IS_SYMOP (op) && OP_SYMBOL (op)->aop)
    {
        DEBUGHY08A_emitcode (";", "%d", __LINE__);
        op->aop = OP_SYMBOL (op)->aop;
        return;
    }

    /* if this is a true symbol */
	//if (OP_SYM_TYPE(op) && OP_SYMBOL(op)->isreqv)
	//{
	//	fprintf(stderr, "special op\n");
	//}
    if (IS_TRUE_SYMOP (op))
    {
        //DEBUGHY08A_emitcode(";","%d - true symop",__LINE__);
	
		if (op->isParm && op->svt.symOperand->onStack &&
			op->svt.symOperand->ismyparm)
		{
			op->aop = aopForSym(ic, OP_SYMBOL(op), result); // here is critical
			op->aop->type = AOP_STK;
			//if (MS322STKLARGE) // -2 ==> -3
				op->aop->aopu.aop_stk = op->svt.symOperand->stack;
			//else
				//op->aop->aopu.aop_stk = op->svt.symOperand->stack + 1;
			op->aop->size = getSize(op->svt.symOperand->type);
			return;
		}

        op->aop = aopForSym (ic, OP_SYMBOL (op), result);
        return;
    }

    /* this is a temporary : this has
       only four choices :
       a) register
       b) spillocation
       c) rematerialize
       d) conditional
       e) can be a return use only */

    sym = OP_SYMBOL (op);


    /* if the type is a conditional */
    if (sym->regType == REG_CND)
    {// special condition
		// cond is used for  "same" cond for decision .. rare case
		//if (sym->isitmp && result) // result need write
		//{
		//	aop = op->aop = sym->aop = newAsmop(AOP_REG);
		//	aop->size = 1;
		//	if (sym->nRegs==0)
		//	{
		//		sym->nRegs = 1;
		//		sym->regs[0] = HY08A_findFreeReg(REG_GPR);
		//		aop->aopu.aop_reg[0] = sym->regs[0];
		//	}
		//	else
		//	{
		//		aop->aopu.aop_reg[0] = sym->regs[0];
		//	}
		//}
		//else
		{
			aop = op->aop = sym->aop = newAsmop(AOP_CRY);
			aop->size = 0;
		}
        return;
    }


    /* if it is spilt then two situations
       a) is rematerialize
       b) has a spill location */
    if (sym->isspilt || sym->nRegs == 0)
    {

        DEBUGHY08A_emitcode (";", "%d", __LINE__);
        /* rematerialize it NOW */
        if (sym->remat || sym->rematX)
        {
            // special copy only for Send!!
            // others will have cast first
            sym->aop = op->aop = aop = aopForRemat (op, sym->rematX, sendStruct);
            if(!aop->size ) // if struct, it will have size !!
                aop->size = getSize (sym->type);
            //DEBUGHY08A_emitcode(";"," %d: size %d, %s\n",__LINE__,aop->size,aop->aopu.aop_immd);
            return;
        }

        if (sym->ruonly)
        {
            if (sym->isptr)       // && sym->uptr
            {
                aop = op->aop = sym->aop = newAsmop (AOP_PCODE);
                aop->aopu.pcop = newpCodeOp (NULL, PO_GPR_POINTER);

                //PCOI(aop->aopu.pcop)->_const = 0;
                //PCOI(aop->aopu.pcop)->index = 0;
                /*
                   DEBUGHY08A_emitcode(";","%d: rname %s, val %d, const = %d",
                   __LINE__,sym->rname, 0, PCOI(aop->aopu.pcop)->_const);
                 */
                //allocDirReg (IC_LEFT(ic));

                aop->size = getSize (sym->type);
                DEBUGHY08A_emitcode (";", "%d", __LINE__);
                return;

            }
            else
            {

                unsigned i;

                aop = op->aop = sym->aop = newAsmop (AOP_STR);
                aop->size = getSize (sym->type);
                for (i = 0; i < fReturnSizeHYA; i++)
                    aop->aopu.aop_str[i] = fReturn[i];

                DEBUGHY08A_emitcode (";", "%d", __LINE__);
                return;
            }
        }

        /* else spill location  */
        if (sym->isspilt && sym->usl.spillLoc)
        {
            asmop *oldAsmOp = NULL;

            if (getSize (sym->type) != getSize (sym->usl.spillLoc->type))
            {
                /* force a new aop if sizes differ */
                oldAsmOp = sym->usl.spillLoc->aop;
                sym->usl.spillLoc->aop = NULL;
            }
            DEBUGHY08A_emitcode (";", "%s %d %s sym->rname = %s, offset %d",
                                 __FUNCTION__, __LINE__, sym->usl.spillLoc->rname, sym->rname, sym->usl.spillLoc->offset);

            sym->aop = op->aop = aop = newAsmop (AOP_PCODE);
            if (getSize (sym->type) != getSize (sym->usl.spillLoc->type))
            {
                /* Don't reuse the new aop, go with the last one */
                sym->usl.spillLoc->aop = oldAsmOp;
            }
            //aop->aopu.pcop = popGetImmd(sym->usl.spillLoc->rname,0,sym->usl.spillLoc->offset);
            aop->aopu.pcop = popRegFromString (sym->usl.spillLoc->rname, getSize (sym->type), sym->usl.spillLoc->offset);
            aop->size = getSize (sym->type);

            return;
        }
    }

    {
        sym_link *type = operandType (op);
        if (IS_PTR_CONST (type))
            DEBUGHY08A_emitcode (";", "%d aop type is const pointer", __LINE__);
    }

    /* must be in a register */
    DEBUGHY08A_emitcode (";", "%d register type nRegs=%d", __LINE__, sym->nRegs);
    sym->aop = op->aop = aop = newAsmop (AOP_REG);
    aop->size = sym->nRegs;
	for (i = 0; i < sym->nRegs; i++)
	{
		aop->aopu.aop_reg[i] = sym->regs[i];
	}
}




void
aopOp (operand * op, iCode * ic, bool result)
{
    asmop *aop;
    symbol *sym;
    int i;

    if (!op)
        return;

    /* if this a literal */
    if (IS_OP_LITERAL (op))
    {
        op->aop = aop = newAsmop (AOP_LIT);
        aop->aopu.aop_lit = OP_VALUE (op);
        aop->size = getSize (operandType (op));
        return;
    }

    {
        sym_link *type = operandType (op);
        if (IS_PTR_CONST (type))
            DEBUGHY08A_emitcode (";", "%d aop type is const pointer", __LINE__);
    }

    /* if already has a asmop then continue */
    if (op->aop)
        return;

    /* if the underlying symbol has a aop */
    if (IS_SYMOP (op) && OP_SYMBOL (op)->aop)
    {
        DEBUGHY08A_emitcode (";", "%d", __LINE__);
        op->aop = OP_SYMBOL (op)->aop;
        return;
    }

    /* if this is a true symbol */
	//if (OP_SYM_TYPE(op) && OP_SYMBOL(op)->isreqv)
	//{
	//	fprintf(stderr, "special op\n");
	//}
    if (IS_TRUE_SYMOP (op))
    {
        //DEBUGHY08A_emitcode(";","%d - true symop",__LINE__);
	
		if (op->isParm && op->svt.symOperand->onStack &&
			op->svt.symOperand->ismyparm)
		{
			op->aop = aopForSym(ic, OP_SYMBOL(op), result); // here is critical
			op->aop->type = AOP_STK;
			//if (MS322STKLARGE) // -2 ==> -3
				op->aop->aopu.aop_stk = op->svt.symOperand->stack;
			//else
				//op->aop->aopu.aop_stk = op->svt.symOperand->stack + 1;
			op->aop->size = getSize(op->svt.symOperand->type);
			return;
		}

        op->aop = aopForSym (ic, OP_SYMBOL (op), result);
        return;
    }

    /* this is a temporary : this has
       only four choices :
       a) register
       b) spillocation
       c) rematerialize
       d) conditional
       e) can be a return use only */

    sym = OP_SYMBOL (op);


    /* if the type is a conditional */
    if (sym->regType == REG_CND)
    {// special condition
		// cond is used for  "same" cond for decision .. rare case
		//if (sym->isitmp && result) // result need write
		//{
		//	aop = op->aop = sym->aop = newAsmop(AOP_REG);
		//	aop->size = 1;
		//	if (sym->nRegs==0)
		//	{
		//		sym->nRegs = 1;
		//		sym->regs[0] = HY08A_findFreeReg(REG_GPR);
		//		aop->aopu.aop_reg[0] = sym->regs[0];
		//	}
		//	else
		//	{
		//		aop->aopu.aop_reg[0] = sym->regs[0];
		//	}
		//}
		//else
		{
			aop = op->aop = sym->aop = newAsmop(AOP_CRY);
			aop->size = 0;
		}
        return;
    }


    /* if it is spilt then two situations
       a) is rematerialize
       b) has a spill location */
    if (sym->isspilt || sym->nRegs == 0) 
    {

        DEBUGHY08A_emitcode (";", "%d", __LINE__);
        /* rematerialize it NOW */
        if (sym->remat || sym->rematX)
        {
            // special copy only for Send!!
            // others will have cast first
            sym->aop = op->aop = aop = aopForRemat (op, sym->rematX, 0);
            if(!aop->size ) // if struct, it will have size !!
                aop->size = getSize (sym->type);
            //DEBUGHY08A_emitcode(";"," %d: size %d, %s\n",__LINE__,aop->size,aop->aopu.aop_immd);
            return;
        }

        if (sym->ruonly)
        {
            if (sym->isptr)       // && sym->uptr
            {
                aop = op->aop = sym->aop = newAsmop (AOP_PCODE);
                aop->aopu.pcop = newpCodeOp (NULL, PO_GPR_POINTER);

                //PCOI(aop->aopu.pcop)->_const = 0;
                //PCOI(aop->aopu.pcop)->index = 0;
                /*
                   DEBUGHY08A_emitcode(";","%d: rname %s, val %d, const = %d",
                   __LINE__,sym->rname, 0, PCOI(aop->aopu.pcop)->_const);
                 */
                //allocDirReg (IC_LEFT(ic));

                aop->size = getSize (sym->type);
                DEBUGHY08A_emitcode (";", "%d", __LINE__);
                return;

            }
            else
            {

                unsigned i;

                aop = op->aop = sym->aop = newAsmop (AOP_STR);
                aop->size = getSize (sym->type);
                for (i = 0; i < fReturnSizeHYA; i++)
                    aop->aopu.aop_str[i] = fReturn[i];

                DEBUGHY08A_emitcode (";", "%d", __LINE__);
                return;
            }
        }

        /* else spill location  */
        if (sym->isspilt && sym->usl.spillLoc)
        {
            asmop *oldAsmOp = NULL;

            if (getSize (sym->type) != getSize (sym->usl.spillLoc->type))
            {
                /* force a new aop if sizes differ */
                oldAsmOp = sym->usl.spillLoc->aop;
                sym->usl.spillLoc->aop = NULL;
            }
            DEBUGHY08A_emitcode (";", "%s %d %s sym->rname = %s, offset %d",
                                 __FUNCTION__, __LINE__, sym->usl.spillLoc->rname, sym->rname, sym->usl.spillLoc->offset);

            sym->aop = op->aop = aop = newAsmop (AOP_PCODE);
            if (getSize (sym->type) != getSize (sym->usl.spillLoc->type))
            {
                /* Don't reuse the new aop, go with the last one */
                sym->usl.spillLoc->aop = oldAsmOp;
            }
            //aop->aopu.pcop = popGetImmd(sym->usl.spillLoc->rname,0,sym->usl.spillLoc->offset);
            aop->aopu.pcop = popRegFromString (sym->usl.spillLoc->rname, getSize (sym->type), sym->usl.spillLoc->offset);
            aop->size = getSize (sym->type);

            return;
        }
    }

    {
        sym_link *type = operandType (op);
        if (IS_PTR_CONST (type))
            DEBUGHY08A_emitcode (";", "%d aop type is const pointer", __LINE__);
    }

    /* must be in a register */
    DEBUGHY08A_emitcode (";", "%d register type nRegs=%d", __LINE__, sym->nRegs);
    sym->aop = op->aop = aop = newAsmop (AOP_REG);
    aop->size = sym->nRegs;
	for (i = 0; i < sym->nRegs; i++)
	{
		aop->aopu.aop_reg[i] = sym->regs[i];
	}
}

/*-----------------------------------------------------------------*/
/* freeAsmop - free up the asmop given to an operand               */
/*----------------------------------------------------------------*/
void
freeAsmop (operand * op, asmop * aaop, iCode * ic, bool pop)
{
    asmop *aop;

    if (!op)
        aop = aaop;
    else
        aop = op->aop;

    if (!aop)
        return;

    aop->freed = 1;

    /* all other cases just dealloc */
    if (op)
    {
        op->aop = NULL;
        if (IS_SYMOP (op))
        {
            OP_SYMBOL (op)->aop = NULL;
            /* if the symbol has a spill */
            if (SPIL_LOC (op))
                SPIL_LOC (op)->aop = NULL;
        }
    }
}
static unsigned int
HY08AaopLiteral2(value * val, int offset)
{
	union
	{
		float f;
		unsigned char c[4];
	} fl;

	/* if it is a float then it gets tricky */
	/* otherwise it is fairly simple */
	if (!IS_FLOAT(val->type))
	{
		unsigned long long v = ullFromVal(val); // modify!! from ul to ull
        // 2021 oct modify
        return (v+offset)&0xffff;
		//return ((v >> (offset * 8)) & 0xffff);
	}

	/* it is type float */
	fl.f = (float)floatFromVal(val);
#ifdef WORDS_BIGENDIAN
	return fl.c[3 - offset];
#else
	return fl.c[offset];
#endif
}

/*-----------------------------------------------------------------*/
/* HY08AaopLiteral - string from a literal value                   */
/*-----------------------------------------------------------------*/
static unsigned int
HY08AaopLiteral (value * val, int offset)
{
    union
    {
        float f;
        unsigned char c[4];
    } fl;

    /* if it is a float then it gets tricky */
    /* otherwise it is fairly simple */
    if (!IS_FLOAT (val->type))
    {
        unsigned long long v = ullFromVal (val); // modify!! from ul to ull

        return ((v >> (offset * 8)) & 0xff);
    }

    /* it is type float */
    fl.f = (float) floatFromVal (val);
#ifdef WORDS_BIGENDIAN
    return fl.c[3 - offset];
#else
    return fl.c[offset];
#endif
}

/*-----------------------------------------------------------------*/
/* aopGet - for fetching value of the aop                          */
/*-----------------------------------------------------------------*/
char *
aopGet (asmop * aop, int offset, bool bit16, bool dname)
{
    char *s = buffer;
    char *rs;

    //DEBUGHY08A_emitcode ("; ***","%s  %d",__FUNCTION__,__LINE__);
    /* offset is greater than
       size then zero */
    assert (aop);
    if (offset > (aop->size - 1) && aop->type != AOP_LIT)
        return zero;

    /* depending on type */
    switch (aop->type)
    {

    case AOP_IMMD:
        if (bit16)
            sprintf (s, "%s", aop->aopu.aop_immd);
        else if (offset)
            sprintf (s, "(%s >> %d)", aop->aopu.aop_immd, offset * 8);
        else
            sprintf (s, "%s", aop->aopu.aop_immd);
        DEBUGHY08A_emitcode (";", "%d immd %s", __LINE__, s);
        rs = Safe_calloc (1, strlen (s) + 1);
        strcpy (rs, s);

        return rs;
    case AOP_XDATA: // convert from code, so xdata
        if (offset == 1)
        {
            sprintf(s, "#(high %s )", aop->aopu.aop_dir);//, offset);
        }
        else
            sprintf(s, "#%s", aop->aopu.aop_dir);
        rs = Safe_calloc(1, strlen(s) + 1);
        strcpy(rs, s);
        return rs;


    case AOP_DIR:
        if (offset)
        {
            sprintf (s, "(%s + %d)", aop->aopu.aop_dir, offset);
            DEBUGHY08A_emitcode (";", "oops AOP_DIR did this %s\n", s);
        }
        else
            sprintf (s, "%s", aop->aopu.aop_dir);
        rs = Safe_calloc (1, strlen (s) + 1);
        strcpy (rs, s);
        return rs;

    case AOP_REG:
        //if (dname)
        //    return aop->aopu.aop_reg[offset]->dname;
        //else
        return aop->aopu.aop_reg[offset]->name;

    case AOP_CRY:
        //HY08A_emitcode(";","%d",__LINE__);
        return aop->aopu.aop_dir;

	case AOP_STK:
		// depends on if there is var length 
		// the shift will continue with local par, etc
		// linker shall decide
		// 2 situations: variable length and re-entry
		// if var length, para is on stack
		// if re-entry, both para and local is on stack.
		// yes, that is confusing, but it is the best for all kind of 
		// applications!!
		SNPRINTF(s, 20, "@FSR2-0x%02X", (-aop->aopu.aop_stk) -offset-3); // there is pc stack skipped
		rs = Safe_strdup(s);
		return rs;

    case AOP_LIT:
        sprintf (s, "0x%02x", HY08AaopLiteral (aop->aopu.aop_lit, offset));
        rs = Safe_strdup (s);
        return rs;

    case AOP_STR:
        aop->coff = offset;
        //if (strcmp (aop->aopu.aop_str[offset], "a") == 0 && dname)
            //return "acc";
        DEBUGHY08A_emitcode (";", "%d - %s", __LINE__, aop->aopu.aop_str[offset]);

        return aop->aopu.aop_str[offset];

    case AOP_PCODE:
    {
        pCodeOp *pcop = aop->aopu.pcop;
        DEBUGHY08A_emitcode (";", "%d: aopGet AOP_PCODE type %s", __LINE__, pCodeOpType (pcop));
        if (pcop->name)
        {
            if (pcop->type == PO_IMMEDIATE)
            {
                offset += PCOI (pcop)->index;
            }
            if (offset)
            {
                DEBUGHY08A_emitcode (";", "%s offset %d", pcop->name, offset);
                sprintf (s, "(%s+%d)", pcop->name, offset);
            }
            else
            {
                DEBUGHY08A_emitcode (";", "%s", pcop->name);
                sprintf (s, "%s", pcop->name);
            }
        }
        else
            sprintf (s, "0x%02x", PCOI (aop->aopu.pcop)->offset);

    }
    rs = Safe_calloc (1, strlen (s) + 1);
    strcpy (rs, s);
    return rs;

    }

    werror (E_INTERNAL_ERROR, __FILE__, __LINE__, "aopget got unsupported aop->type");
    exit (-__LINE__);
}

/*-----------------------------------------------------------------*/
/* popGetTempReg - create a new temporary pCodeOp                  */
/*-----------------------------------------------------------------*/
/*
static pCodeOp *
popGetTempReg (void)
{

    pCodeOp *pcop;

    pcop = newpCodeOp (NULL, PO_GPR_TEMP);
    if (pcop && pcop->type == PO_GPR_TEMP && PCOR (pcop)->r)
    {
        PCOR (pcop)->r->wasUsed = 1;
        PCOR (pcop)->r->isFree = 0;
    }

    return pcop;
}
*/

/*-----------------------------------------------------------------*/
/* popReleaseTempReg - create a new temporary pCodeOp                  */
/*-----------------------------------------------------------------*/
static void
popReleaseTempReg (pCodeOp * pcop)
{

    if (pcop && pcop->type == PO_GPR_TEMP && PCOR (pcop)->r)
        PCOR (pcop)->r->isFree = 1;

}

/*-----------------------------------------------------------------*/
/* popGetLabel - create a new pCodeOp of type PO_LABEL             */
/*-----------------------------------------------------------------*/
pCodeOp *
popGetLabel (unsigned int key)
{

    DEBUGHY08A_emitcode ("; ***", "%s  key=%d, label offset %d", __FUNCTION__, key, labelOffset);

    if (key > (unsigned int) max_key)
        max_key = key;

    return newpCodeOpLabel (NULL, labelKey2num (key + labelOffset));
}

pCodeOp *
popGetLabelD2(unsigned int key)
{
    pCodeOp *pcl;
    DEBUGHY08A_emitcode("; ***", "%s  key=%d, label offset %d", __FUNCTION__, key, labelOffset);

    if (key > (unsigned int)max_key)
        max_key = key;

    pcl = (pCodeOp*)newpCodeOpLabel(NULL, labelKey2num(key + labelOffset));
    PCOLAB(pcl)->div2 = 1;
    return pcl;
}


/*-------------------------------------------------------------------*/
/* popGetHighLabel - create a new pCodeOp of type PO_LABEL with offset=1 */
/*-------------------------------------------------------------------*/
static pCodeOp *
popGetHighLabel (unsigned int key) // for jump table, it needs div2
{
    pCodeOp *pcop;
    pcop = popGetLabel (key);
    PCOLAB (pcop)->offset = 1;
    PCOLAB(pcop)->div2 = 1;
    return pcop;
}


// static pCodeOp *
// popGetHighLabelByteCount(unsigned int key) // for jump table, it needs div2
// {
// 	pCodeOp *pcop;
// 	pcop = popGetLabel(key);
// 	PCOLAB(pcop)->offset = 1;
// 	PCOLAB(pcop)->div2 = 0;
// 	return pcop;
// }

/*-----------------------------------------------------------------*/
/* popGetLit - asm operator to pcode operator conversion               */
/*-----------------------------------------------------------------*/
pCodeOp *
popGetLit (unsigned int lit)
{
	if (HY08A_getPART()->isEnhancedCore ==4 ) // only for hy08d
		return newpCodeOpLit(lit & 0xffff);
    return newpCodeOpLit ((unsigned char) lit);
}

/*-----------------------------------------------------------------*/
/* popGetImmd - asm operator to pcode immediate conversion         */
/*-----------------------------------------------------------------*/
static pCodeOp *
popGetImmd (char *name, unsigned int offset, int index, int is_func, int size, int in_x, pBlock *pb)
{

    return newpCodeOpImmd (name, offset, index, 0,in_x, is_func, size, pb);
}

/*-----------------------------------------------------------------*/
/* popGetWithString - asm operator to pcode operator conversion            */
/*-----------------------------------------------------------------*/
static pCodeOp *
popGetWithString (char *str, int isExtern)
{
    pCodeOp *pcop;


    if (!str)
    {
        fprintf (stderr, "NULL string %s %d\n", __FILE__, __LINE__);
        exit (1);
    }

    pcop = newpCodeOp (str, PO_STR);
    PCOS (pcop)->isPublic = isExtern ? 1 : 0;

    return pcop;
}

pCodeOp *
popGetExternal (char *str, int isReg)
{
    pCodeOp *pcop;

    if (isReg)
    {
        pcop = newpCodeOpRegFromStr (str);
    }
    else
    {
        pcop = popGetWithString (str, 1);
    }

    if (str)
    {
        symbol *sym;

        for (sym = setFirstItem (externs); sym; sym = setNextItem (externs))
        {
            if (!strcmp (str, sym->rname))
                break;
        }
		//if (strstr(sym->rname, "Temp_Offset_Fun"))
		//	fprintf(stderr, "Temp_Offset_Fun %s\n", sym->rname);

        if (!sym)
        {
            sym = newSymbol (str, 0);
            strncpy (sym->rname, str, SDCC_NAME_MAX);
            addSet (&externs, sym);
        }                       // if
        sym->used++;
    }
    return pcop;
}

/*-----------------------------------------------------------------*/
/* popRegFromString -                                              */
/*-----------------------------------------------------------------*/
static pCodeOp *
popRegFromString (char *str, int size, int offset)
{

    pCodeOp *pcop = Safe_calloc (1, sizeof (pCodeOpReg));
	//reg_info *reg;
	//if ((reg = regFindWithName(str)) != NULL)
	//{
	//	if (reg->type == REG_GPR)
	//		pcop->type = PO_GPR_REGISTER;
	//	else if (reg->type == REG_STK || reg->type == REG_TMP || reg->type == REG_CND)
	//		pcop->type = PO_GPR_TEMP;
	//	else
	//		pcop->type = PO_DIR;
	//	PCOR(pcop)->r = reg;
	//	PCOR(pcop)->rIdx = reg->rIdx;
	//	pcop->name = reg->name;
	//	PCOR(pcop)->instance = offset;
	//	return pcop;
	//}

	pcop->type = PO_DIR;

	if (!strcmp(str, "_STATUS"))
		return PCOP(&pc_status);
	if (!strcmp(str, "_INDF0")) // case sensitive !!
		return PCOP(&pc_indf0);
	if (!strcmp(str, "_INDF1"))
		return PCOP(&pc_indf1);
	if (!strcmp(str, "_INDF2"))
		return PCOP(&pc_indf2);
	if (!strcmp(str, "_PRINC0"))
		return PCOP(&pc_princ0);
//	if (!STRCASECMP(str, "_PRINC1"))
//		return PCOP(&pc_princ1);
	if (!strcmp(str, "_PRINC2"))
		return PCOP(&pc_princ2);
	if (!strcmp(str, "_POINC0"))
		return PCOP(&pc_poinc0);
	if (!strcmp(str, "_POINC1"))
		return PCOP(&pc_poinc1);
	if (!strcmp(str, "_POINC2"))
		return PCOP(&pc_poinc2);
	if (!strcmp(str, "_PLUSW0"))
		return PCOP(&pc_plusw0);
	if (!strcmp(str, "_PLUSW1"))
		return PCOP(&pc_plusw1);
	if (!strcmp(str, "_PLUSW2"))
		return PCOP(&pc_plusw2);
	if (!strcmp(str, "_WREG"))
		return PCOP(&pc_wreg);
	if (!strcmp(str, "_PRODL"))
		return PCOP(&pc_prodl);
	if (!strcmp(str, "_PRODH"))
		return PCOP(&pc_prodh);

    DEBUGHY08A_emitcode (";", "%d", __LINE__);

    if (!str)
        str = "BAD_STRING";

    pcop->name = Safe_calloc (1, strlen (str) + 1);
    strcpy (pcop->name, str);

    //pcop->name = Safe_strdup( ( (str) ? str : "BAD STRING"));

	if (!STRNCASECMP(pcop->name, "@FSR2-", 6))
	{
		PCOR(pcop)->r = NULL;
	}
	else
	{
		PCOR(pcop)->r = dirregWithName(pcop->name);
		if (PCOR(pcop)->r == NULL)
		{
			//fprintf(stderr,"%d - couldn't find %s in allocated registers, size =%d\n",__LINE__,aop->aopu.aop_dir,aop->size);
			PCOR(pcop)->r = allocRegByName(pcop->name, size, 0);
            if(PCOR(pcop)->r->pc_type==PO_SFR_REGISTER)
                pcop->type = PO_SFR_REGISTER;
			DEBUGHY08A_emitcode(";", "%d  %s   offset=%d - had to alloc by reg name", __LINE__, pcop->name, offset);
		}
		else
		{
            if(PCOR(pcop)->r->pc_type==PO_SFR_REGISTER)
                pcop->type = PO_SFR_REGISTER;
			//pcop->type = PCOR(pcop)->r->pc_type; // no more dir?
			DEBUGHY08A_emitcode(";", "%d  %s   offset=%d", __LINE__, pcop->name, offset);
		}
	}
    PCOR (pcop)->instance = offset;
    // volatile to local
    if(PCOR(pcop)->r && PCOR(pcop)->r->isVolatile && !PCOR(pcop)->r->isPublic && pb && PCOR(pcop)->r->isFuncLocal && !PCOR(pcop)->r->isLocalStatic
       && !isinSet(pb->tregisters, PCOR(pcop)->r) )
    {
        addSet(&pb->tregisters, PCOR(pcop)->r);
    }

	
    // if (!strcmp(str, "_STATUS") ||
    //         !strncmp(str, "_INDF", 5) ||
    //         !strncmp(str, "_PRINC", 6) ||
    //         !strncmp(str, "_POINC", 6) ||
    //         !strncmp(str, "_PLUSW", 6) ||
    //         !strcmp(str, "_WREG"))
    //     PCOR(pcop)->r->isVolatile = 1;
	if(pcop->type == PO_STATUS ||
       pcop->type == PO_INDF ||
       pcop->type == PO_PRINC ||
       pcop->type == PO_POINC ||
       pcop->type == PO_W ||
       pcop->type == PO_PLUSW
       )
       PCOR(pcop)->r->isVolatile = 1;
	

    return pcop;
}

/*-----------------------------------------------------------------*/
/*-----------------------------------------------------------------*/
static pCodeOp *
popRegFromIdx (int rIdx)
{
    pCodeOp *pcop;

    DEBUGHY08A_emitcode ("; ***", "%s,%d  , rIdx=0x%x", __FUNCTION__, __LINE__, rIdx);

    pcop = Safe_calloc (1, sizeof (pCodeOpReg));

    PCOR (pcop)->rIdx = rIdx;
    PCOR (pcop)->r = typeRegWithIdx (rIdx, REG_STK, 1);
    if (!(PCOR(pcop)->r))
    {
		//fprintf(stderr, "%s at %s %d\n", "internal error", __FUNCTION__, __LINE__);
        exit(-1100);
    }
    PCOR (pcop)->r->isFree = 0;
    PCOR (pcop)->r->wasUsed = 1;
	if (PCOR(pcop)->r->type == REG_TMP)
		PCOR(pcop)->r->isFuncLocal = 1; // it should be func local

    pcop->type = PCOR (pcop)->r->pc_type;

	// dbg
	//if (strstr(PCOR(pcop)->r->name, "_input"))
	//{
	//	fprintf(stderr, "special input\n");
	//}

    return pcop;
}


static pCodeOp *
popRegFromIdx2(int rIdx, char *funcName, int setExt, int funcStatic) // if linking time local allocation, we use this function
{
    pCodeOp *pcop;
    char nameBuf[64];
    DEBUGHY08A_emitcode("; ***", "%s,%d  , rIdx=0x%x", __FUNCTION__, __LINE__, rIdx);


    SNPRINTF(nameBuf, 63, "%s_STK%02d",funcName,  Gstack_base_addr-rIdx);
    pcop = popRegFromString(nameBuf, 1, 0);


    if (PCOR(pcop)->r->isFuncLocal)// already set
    {
        if (!setExt)
        {
            PCOR(pcop)->r->isExtern = 0; // 0 has priority
            if(!funcStatic)
                PCOR(pcop)->r->isPublic = 1;
            else
                PCOR(pcop)->r->isPublic = 0;

        }
    }
    else
    {
        PCOR(pcop)->r->isFuncLocal = 1;
        PCOR(pcop)->r->isExtern = setExt;
        if(funcStatic)
            PCOR(pcop)->r->isPublic = 0;
        else
            PCOR(pcop)->r->isPublic = 1;
    }
    if (!setExt)
    {   // STK is always .. zero page!! now
        addSet(&pb->tregisters, (void*)PCOR(pcop)->r);
        pcop->type = PO_GPR_TEMP; // IDX2 is temp!!
	}
	else
		pcop->flags.calleePara = 1;

    return pcop;
}
/*-----------------------------------------------------------------*/
/* popGet - asm operator to pcode operator conversion              */
/*-----------------------------------------------------------------*/
pCodeOp *
popGet(asmop * aop, int offset)
{
	return popGet2(aop, offset, 0);
}
pCodeOp *
popGet2 (asmop * aop, int offset, int b16)        //, bool bit16, bool dname)
{
    //char *s = buffer ;
    //char *rs;

    pCodeOp *pcop;
	char sztmp[1024];

    //DEBUGHY08A_emitcode ("; ***","%s  %d",__FUNCTION__,__LINE__);
    /* offset is greater than
       size then zero */

    assert (aop);
	//if ( aop->size > 3)
		//fprintf(stderr, "large aop?\n");


    /* XXX: still needed for BIT operands (AOP_CRY) */
	
	if ((offset >= aop->size) && (aop->type != AOP_LIT) && (aop->type == AOP_DIR || aop->type==AOP_REG)) // very very special case
	{
		return popGetLit(0);
	}
    if ((offset >= aop->size) && (aop->type != AOP_LIT) && (aop->type != AOP_PCODE))
    {
        printf ("%s: (offset[%d] >= AOP_SIZE(op)[%d]) && (AOP_TYPE(op)[%d] != { AOP_LIT, AOP_PCODE })\n",
                __FUNCTION__, offset, aop->size, aop->type);
        return NULL;              //zero;
    }

    /* depending on type */
    switch (aop->type)
    {

    case AOP_IMMD:
        DEBUGHY08A_emitcode (";", "%d", __LINE__);

        pcop =  popGetImmd (aop->aopu.aop_immd, offset, 0, 0,1,0, NULL);
        PCOI(pcop)->r->isVolatile = aop->isvolatile;
        return pcop;

    case AOP_XDATA:
    case AOP_DIR:
        pcop = popRegFromString (aop->aopu.aop_dir, aop->size, offset);
        PCOR(pcop)->r->isVolatile = aop->isvolatile;
        if (pcop->type == PO_NONE)
        {
            if (aop->type == AOP_DIR)
            {
                pcop->type = PO_DIR;
            }
            else
                pcop->type = PO_XDATA;
        }

        return pcop;
	case AOP_STK:
		/*if (aop->aopu.aop_stk + offset == 0)
			SNPRINTF(sztmp, 1023, "@_RAMP1");
		else*/
			SNPRINTF(sztmp, 1023, "@FSR2-0x%02X", (0-aop->aopu.aop_stk) - offset-3);
		//SNPRINTF(sztmp,1023,"@P1,%d",aop->aopu.aop_stk+offset);
		pcop = popRegFromString(sztmp, 1, 0);
		pcop->flags.parapcop = 1;
		return pcop;


    case AOP_REG:
    {
        int rIdx;
        assert (offset < aop->size);
        rIdx = aop->aopu.aop_reg[offset]->rIdx;

        pcop = Safe_calloc (1, sizeof (pCodeOpReg));
        PCOR (pcop)->rIdx = rIdx;
        PCOR (pcop)->r = HY08A_regWithIdx (rIdx);
        PCOR(pcop)->r->isVolatile = aop->isvolatile;
        PCOR (pcop)->r->wasUsed = 1;
        PCOR (pcop)->r->isFree = 0;

        PCOR (pcop)->instance = offset;
        pcop->type = PCOR (pcop)->r->pc_type;
        //rs = aop->aopu.aop_reg[offset]->name;
        DEBUGHY08A_emitcode (";", "%d rIdx = r0x%X ", __LINE__, rIdx);
		if (!isinSet(pb->tregisters, PCOR(pcop)->r))
			addSet(&pb->tregisters, PCOR(pcop)->r);
        return pcop;
    }

    case AOP_CRY:
        pcop = newpCodeOpBit (aop->aopu.aop_dir, 0,1); // this is name only
        PCOR (pcop)->r = dirregWithName (aop->aopu.aop_dir);
        //if(PCOR(pcop)->r == NULL)
        //fprintf(stderr,"%d - couldn't find %s in allocated registers\n",__LINE__,aop->aopu.aop_dir);
        return pcop;

    case AOP_LIT:
		if(b16)
			return newpCodeOpLit(HY08AaopLiteral2(aop->aopu.aop_lit, offset));
        return newpCodeOpLit (HY08AaopLiteral (aop->aopu.aop_lit, offset));

    case AOP_STR:
        DEBUGHY08A_emitcode (";", "%d  %s", __LINE__, aop->aopu.aop_str[offset]);
        return newpCodeOpRegFromStr (aop->aopu.aop_str[offset]);

    case AOP_PCODE:
        pcop = NULL;
        DEBUGHY08A_emitcode (";", "popGet AOP_PCODE (%s + %i) %d %s", pCodeOpType (aop->aopu.pcop), offset,
                             __LINE__, ((aop->aopu.pcop->name) ? (aop->aopu.pcop->name) : "no name"));
        //emitpComment ("popGet; name %s, offset: %i, pcop-type: %s\n", aop->aopu.pcop->name, offset, pCodeOpType (aop->aopu.pcop));
        switch (aop->aopu.pcop->type)
        {
        case PO_IMMEDIATE:
            pcop = pCodeOpCopy (aop->aopu.pcop);
            /* usually we want to access the memory at "<symbol> + offset" (using ->index),
             * but sometimes we want to access the high byte of the symbol's address (using ->offset) */
            PCOI (pcop)->index += offset;
            //PCOI(pcop)->offset = 0;
            break;
        case PO_DIR:
            pcop = pCodeOpCopy (aop->aopu.pcop);
            PCOR (pcop)->instance = offset;
            break;
        default:
            assert (!"unhandled pCode type");
            break;
        }                       // switch
        return pcop;
    }

    werror (E_INTERNAL_ERROR, __FILE__, __LINE__, "popGet got unsupported aop->type");
    exit (-1);
}




/*-----------------------------------------------------------------*/
/* popGetAddr - access the low/high word of a symbol (immediate)   */
/*              (for non-PO_IMMEDIATEs this is the same as popGet) */
/*-----------------------------------------------------------------*/
pCodeOp *
popGetAddr (asmop * aop, int offset, int index)
{
    if (!aop->imm2dir && aop->type == AOP_PCODE && aop->aopu.pcop->type == PO_IMMEDIATE)
    {
        pCodeOp *pcop = aop->aopu.pcop;
		if (offset > GPTRSIZE)
		{
			fprintf(stderr, "internal error at %s:%d\n", __FILE__, __LINE__);
			exit(-__LINE__);
		}
        assert (offset <= GPTRSIZE);

        /* special case: index >= 2 should return GPOINTER-style values */
        if (offset == 2)
        {
            pcop = popGetLit (aop->code ? GPTRTAG_CODE : GPTRTAG_DATA);
            return pcop;
        }

        pcop = pCodeOpCopy (pcop);
        /* usually we want to access the memory at "<symbol> + offset" (using ->index),
         * but sometimes we want to access the high byte of the symbol's address (using ->offset) */
        PCOI (pcop)->offset += offset;
        PCOI (pcop)->index += index;
		if (PCOI(pcop)->r && PCOI(pcop)->r->isFuncLocal)
			PCOI(pcop)->r->addrReferenced = 1;
        //fprintf (stderr, "is PO_IMMEDIATE: %s+o%d+i%d (new o%d,i%d)\n", pcop->name,PCOI(pcop)->offset,PCOI(pcop)->index, offset, index);
        return pcop;
    }
    else
    {
        return popGet (aop, offset + index);
    }
}

/*-----------------------------------------------------------------*/
/* aopPut - puts a string for a aop                                */
/*-----------------------------------------------------------------*/
void
aopPut (asmop * aop, char *s, int offset)
{
    char *d = buffer;
    //symbol *lbl;

    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);

    if (aop->size && offset > (aop->size - 1))
    {
        werror (E_INTERNAL_ERROR, __FILE__, __LINE__, "aopPut got offset > aop->size");
        exit (-__LINE__);
    }

    /* will assign value to value */
    /* depending on where it is ofcourse */
    switch (aop->type)
    {
	case AOP_XDATA:
		
		fprintf(stderr, "Parameter in XDATA is not supported (yet).\n" );
		exit (-__LINE__);
    case AOP_DIR:
        if (offset)
        {
            sprintf (d, "(%s + %d)", aop->aopu.aop_dir, offset);
            //fprintf (stderr, "oops aopPut:AOP_DIR did this %s\n", s);

        }
        else
            sprintf (d, "%s", aop->aopu.aop_dir);

        if (strcmp (d, s))
        {
            DEBUGHY08A_emitcode (";", "%d", __LINE__);
            if (strcmp (s, "W"))
                HY08A_emitcode ("movf", "%s,w", s);
            HY08A_emitcode ("movwf", "%s", d);

            if (strcmp (s, "W"))
            {
                HY08A_emitcode (";BUG!? should have this:movf", "%s,w   %d", s, __LINE__);
                if (offset >= aop->size)
                {
                    emitpcode (POC_CLRF, popGet (aop, offset));
                    break;
                }
                else
                {
                    emitpcode (POC_MVFW, popGetImmd (s, 0, offset, 0,1,0,NULL));
                }
            }
            emitpcode (POC_MVWF, popGet (aop, offset));

        }
        break;

    case AOP_REG:
        if (strcmp (aop->aopu.aop_reg[offset]->name, s) != 0)
        {
            if (strcmp (s, "W") == 0)
                HY08A_emitcode ("movf", "%s,w  ; %d", s, __LINE__);

            HY08A_emitcode ("movwf", "%s", aop->aopu.aop_reg[offset]->name);

            if (strcmp (s, zero) == 0)
            {
                emitpcode (POC_CLRF, popGet (aop, offset));

            }
            else if (strcmp (s, "W") == 0)
            {
                pCodeOp *pcop = Safe_calloc (1, sizeof (pCodeOpReg));
                pcop->type = PO_GPR_REGISTER;

                PCOR (pcop)->rIdx = -1;
                PCOR (pcop)->r = NULL;

                DEBUGHY08A_emitcode (";", "%d", __LINE__);
                pcop->name = Safe_strdup (s);
                emitpcode (POC_MVFW, pcop);
                emitpcode (POC_MVWF, popGet (aop, offset));
            }
            else if (strcmp (s, one) == 0)
            {
                emitpcode (POC_CLRF, popGet (aop, offset));
                emitpcode (POC_INF, popGet (aop, offset));
            }
            else
            {
                emitpcode (POC_MVWF, popGet (aop, offset));
            }
        }
        break;

    case AOP_STK:
 /*       if (strcmp (s, "a") == 0)
            HY08A_emitcode ("push", "acc");
        else
            HY08A_emitcode ("push", "%s", s);*/
		fprintf(stderr, "internel err, AOPSTK put @ %s:%d\n",
			__FILE__, __LINE__);
		exit(-1005);

        break;

    case AOP_CRY:
        /* if bit variable */


		//fprintf(stderr, "internel err, CRY @ %s:%d\n",
			//__FILE__, __LINE__);
       
        break;

    case AOP_STR:
        aop->coff = offset;
        if (strcmp (aop->aopu.aop_str[offset], s))
            HY08A_emitcode ("mov", "%s,%s ; %d", aop->aopu.aop_str[offset], s, __LINE__);
        break;

	

    default:
        werror (E_INTERNAL_ERROR, __FILE__, __LINE__, "aopPut got unsupported aop->type");
        exit (-__LINE__);
    }

}

/*-----------------------------------------------------------------*/
/* mov2w_op - generate either a MOVLW or MOVFW based operand type  */
/*-----------------------------------------------------------------*/
static void
mov2w_op (operand * op, int offset)
{
    assert (op);
    FENTRY;

    /* for PO_IMMEDIATEs: use address or value? */
    if (op_isLitLike (op) )// special case? bug-2385.c?
    {
        /* access address of op */
        if (AOP_TYPE (op) != AOP_LIT)
        {
            assert (offset < 3);
        }
        if (IS_SYMOP (op) && IS_GENPTR (OP_SYM_TYPE (op)) && AOP_SIZE (op) < offset)
        {
            if (offset == GPTRSIZE - 1)
                emitpcode (POC_MVL, popGetLit (GPTRTAG_DATA));
            else
                emitpcode (POC_MVL, popGetLit (0));
        }
        else
            emitpcode (POC_MVL, popGetAddr (AOP (op), offset, 0));
    }
    else
    {
        /* access value stored in op */
        if (IS_TRUE_SYMOP(op) && IN_XSPACE(SPEC_OCLS(OP_SYMBOL(op)->etype)))
        {
			emitpcode21(POC_MVFF, popGet2(AOP(op), offset,1), PCOP_W); 
        }
        else
        {
            mov2w(AOP(op), offset);
        }
    }
}


static void
mov2w_op_cont(operand * op, int offset, int *wvalue)
{
	assert(op);
	FENTRY;

	/* for PO_IMMEDIATEs: use address or value? */
	if (op_isLitLike(op))
	{
		/* access address of op */
		if (AOP_TYPE(op) != AOP_LIT)
		{
			assert(offset < 3);
		}
		if (IS_SYMOP(op) && IS_GENPTR(OP_SYM_TYPE(op)) && AOP_SIZE(op) < offset)
		{
			if (offset == GPTRSIZE - 1)
				emitpcode(POC_MVL, popGetLit(GPTRTAG_DATA));
			else
				emitpcode(POC_MVL, popGetLit(0));
			*wvalue = -1;
		}
		else
		{
			pCodeOp *pcop = popGetAddr(AOP(op), offset, 0);
			if (pcop->type == PO_LITERAL)
			{
				if (*wvalue == PCOL(pcop)->lit)
					return; // no change
				emitpcode(POC_MVL, pcop);
				*wvalue = PCOL(pcop)->lit;
				if (*wvalue == 0xff || *wvalue == 0) // other opt
					*wvalue = -1;
			}
			else
			{

				emitpcode(POC_MVL, pcop);
				*wvalue = -1;
			}
		}
	}
	else
	{
		/* access value stored in op */
		if (IS_TRUE_SYMOP(op) && IN_XSPACE(SPEC_OCLS(OP_SYMBOL(op)->etype)))
		{
			emitpcode21(POC_MVFF, popGet2(AOP(op), offset,1), PCOP_W);
			*wvalue = -1;
		}
		else
		{
			mov2w(AOP(op), offset);
			*wvalue = -1;
		}
	}
}


/*-----------------------------------------------------------------*/
/* mov2w - generate either a MOVLW or MOVFW based operand type     */
/*-----------------------------------------------------------------*/
void
mov2w (asmop * aop, int offset)
{

    if (!aop)
        return;

    DEBUGHY08A_emitcode ("; ***", "%s  %d  offset=%d", __FUNCTION__, __LINE__, offset);


	
    if (aop_isLitLike (aop))
        emitpcode (POC_MVL, popGetAddr (aop, offset, 0));
    else
    {
        pCodeOp *pcop = popGet(aop, offset);

		
        if(pcop->name==NULL || strcmp(pcop->name,"_WREG"))
            emitpcode(POC_MVFW, popGet(aop, offset));
    }
}

static void
movwf (asmop * op, int offset)
{
    emitpcode (POC_MVWF, popGet (op, offset));
}

 pCodeOp *
get_argument_pcop (int idx)
{
    assert (idx > 0 && "the 0th (first) argument is passed via WREG");
    return popRegFromIdx (Gstack_base_addr - (idx - 1));
}

static pCodeOp *
get_return_val_pcop (int offset, operand *thecall)
{
    assert (offset > 0 && "the most significant byte is returned via WREG");
    if (thecall)
    {
        return popRegFromIdx2(Gstack_base_addr - (offset - 1), OP_SYMBOL(thecall)->rname,0, IS_STATIC(OP_SYMBOL(thecall)->etype));
    }
    return popRegFromIdx (Gstack_base_addr - (offset - 1));
}

static void
pass_argument (operand * op, int offset, int idx)
{

    if (idx!=0 && AOP_TYPE(op) == AOP_LIT && HY08AaopLiteral(AOP(op)->aopu.aop_lit, offset)==0)
    {
        emitpcode(POC_CLRF, get_argument_pcop(idx));
		PCI(pb->pcTail)->retOffP1 = idx + 1;
    }
    else if (idx!=0 && AOP_TYPE(op) == AOP_LIT && HY08AaopLiteral(AOP(op)->aopu.aop_lit, offset) == 0xff)
    {
        emitpcode(POC_SETF, get_argument_pcop(idx));
		PCI(pb->pcTail)->retOffP1 = idx + 1;
    }
    else
    {
        mov2w_op(op, offset);
		PCI(pb->pcTail)->retOffP1 = idx + 1;
		if (idx != 0)
		{
			emitpcode(POC_MVWF, get_argument_pcop(idx));
			PCI(pb->pcTail)->retOffP1 = idx + 1;
		}
    }
}


static void
movwf2(asmop * op, int offset)
{
	if(op->type!=AOP_CRY)
		emitpcode(POC_MVWF2, popGet(op, offset));
	else
	{ // add for bug1788177.c!!
		emitpcode(POC_BCF, popGet(op, 0));
		emitpcode(POC_IORL, popGetLit(0));
		emitSKPZ;
		emitpcode(POC_BSF, popGet(op, 0));
	}
}
static void
get_returnvalue (operand * op, int offset, int idx, operand *thecall)
{
    if(IS_SYMOP(op) && !OP_SYMBOL(op)->isitmp && IN_XSPACE(SPEC_OCLS(OP_SYMBOL(op)->etype)))
    {
        if(idx==0)
        {
            emitpcode2(POC_MVFF, PCOP(&pc_wreg), popGet(AOP(op), offset));
        }
        else
        {
            emitpcode2(POC_MVFF, get_return_val_pcop (idx,thecall), 
            popGet(AOP(op), offset));
        }
        return ;
    }

    if (idx != 0)
        emitpcode (POC_MVFW, get_return_val_pcop (idx,thecall));
	if (thecall)
	{        
		movwf2(AOP(op), offset);
	}
    else
        movwf (AOP (op), offset);
}

static void
call_libraryfunc (char *name)
{
    symbol *sym;

    /* library code might reside in different page... */
    //emitpcode (POC_PAGESEL, popGetWithString (name, 1));
    /* call the library function */
    emitpcode (POC_CALL, popGetExternal (name, 0));
    /* might return from different page... */
    //emitpcode (POC_PAGESEL, popGetWithString ("$", 0));

    /* create symbol, mark it as `extern' */
    sym = findSym (SymbolTab, NULL, name);
    if (!sym)
    {
        sym = newSymbol (name, 0);
        strncpy (sym->rname, name, SDCC_NAME_MAX);
        addSym (SymbolTab, sym, sym->rname, 0, 0, 0);
        addSet (&externs, sym);
    }                           // if
    sym->used++;
}

/*-----------------------------------------------------------------*/
/* HY08A_getDataSize - get the operand data size                   */
/*-----------------------------------------------------------------*/
int
HY08A_getDataSize (operand * op)
{
    int size;

    size = AOP_SIZE (op);
    return size;
}

/*-----------------------------------------------------------------*/
/* HY08A_outAcc - output Acc                                       */
/*-----------------------------------------------------------------*/
void
HY08A_outAcc (operand * result)
{
    int size, offset;
    DEBUGHY08A_emitcode ("; ***", "%s  %d - ", __FUNCTION__, __LINE__);
    DEBUGHY08A_AopType (__LINE__, NULL, NULL, result);


    size = HY08A_getDataSize (result);
    if (size)
    {
        emitpcode (POC_MVWF, popGet (AOP (result), 0));
        size--;
        offset = 1;
        /* unsigned or positive */
        while (size--)
            emitpcode (POC_CLRF, popGet (AOP (result), offset++));
    }

}

/*-----------------------------------------------------------------*/
/* HY08A_outBitC - output a bit C                                  */
/*-----------------------------------------------------------------*/
static void
HY08A_outBitC (operand * result)
{

    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);
    /* if the result is bit */
    if (AOP_TYPE (result) == AOP_CRY)
        aopPut (AOP (result), "c", 0);
    else
    {
        HY08A_emitcode ("clr", "a  ; %d", __LINE__);
        HY08A_emitcode ("rlc", "a");
        HY08A_outAcc (result);
    }
}

/*-----------------------------------------------------------------*/
/* HY08A_toBoolean - emit code for orl a,operator(sizeop)          */
/*-----------------------------------------------------------------*/
static void
HY08A_toBoolean (operand * oper) // a bug from torture test
{
	assert(oper);
    int size = AOP_SIZE (oper);
    int offset = 0;
	int inX = IS_TRUE_SYMOP(oper) && IN_XSPACE(SPEC_OCLS(OP_SYMBOL(oper)->etype));

    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);


    
    assert (size > 0);

	// constant is on ROM!!!!
	// usually this is optimzed?
	if (IS_TRUE_SYMOP(oper) && IS_CONSTANT(OP_SYMBOL(oper)->type))
	{
		char fname[100];
		if(GPTRSIZE<=2)
			snprintf(fname, 99, "__g2ptrget%d", size);
		else
			snprintf(fname, 99, "__gptrget%d", size);
		mov2w(AOP(oper), 0);
		
		emitpcode(POC_MVWF, popRegFromIdx2(Gstack_base_addr, fname, 1, 0));
		mov2w(AOP(oper), 1);
		emitpcode(POC_IORL, popGetLit(0x80)); // msb set 1 !!
		call_libraryfunc(fname);
		if (size == 1)
		{
			emitpcode(POC_IORL, popGetLit(0));
		}
		else
		{
			int id;
			for (id = 0; id < size - 1; id++)
			{
				emitpcode(POC_IORFW, get_return_val_pcop(id + 1, NULL));
			}
		}
		return;
	}
	

	if (inX)
		emitldpr(popGet(AOP(oper), 0));





    if (size == 1)
    {
		emitpcode(POC_MVL, popGetLit(0)); // left to peep ?
        /* MOVFW does not load the flags... */
		if (!inX)
		{
			emitpcode(POC_IORFW, popGet(AOP(oper), 0));
			return;
		}
		//else
			//emitpcode (POC_IORFW, PCOP(&pc_poinc0));
        offset = 0;
    }
    else
    {
		if(inX)
			emitpcode(POC_MVFW, PCOP_POINC0);
		else
		{
			if (op_isLitLike(oper))
			{
				
				{
					emitpcode(POC_MVL, popGet(AOP(oper), 0)); // it is XXX
				}
			}
			else
				emitpcode(POC_MVFW, popGet(AOP(oper), 0));
		}
        offset = 1;
    }

    while (offset < size)
    {
		if (inX)
		{
			emitpcode(POC_IORFW, PCOP_POINC0);
			
		}
		else if (op_isLitLike(oper))
			emitpcode(POC_IORL, popGet(AOP(oper), offset)); // it is XXX
		else
			emitpcode (POC_IORFW, popGet (AOP (oper), offset));
		offset++;
    }
    /* Z is set iff (oper == 0) */
}


/*-----------------------------------------------------------------*/
/* genNot - generate code for ! operation                          */
/*-----------------------------------------------------------------*/
static void
genNot (iCode * ic)
{
    //symbol *tlbl;
    int size;
	int resultX = 0;
	int resultbit = 0;
    FENTRY;

    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);
    /* assign asmOps to operand & result */
    aopOp (IC_LEFT (ic), ic, FALSE);
    aopOp (IC_RESULT (ic), ic, TRUE);

	
    DEBUGHY08A_AopType (__LINE__, IC_LEFT (ic), NULL, IC_RESULT (ic));
    /* if in bit space then a special case */
	resultX = IS_TRUE_SYMOP(IC_RESULT(ic)) && IN_XSPACE(SPEC_OCLS(OP_SYMBOL(IC_RESULT(ic))->etype));
	if (resultX)
		emitldpr(popGet(AOP(IC_RESULT(ic)), 0));


	if (AOP_TYPE(IC_RESULT(ic)) == AOP_CRY)
		resultbit = 1;
    if (AOP_TYPE (IC_LEFT (ic)) == AOP_CRY)
    {
        if (AOP_TYPE (IC_RESULT (ic)) == AOP_CRY)
        {
			// special if same
			if (HY08A_sameRegs(AOP(IC_LEFT(ic)), AOP(IC_RESULT(ic))))
			{
				emitpcode(POC_BTGF, popGet(AOP(IC_LEFT(ic)), 0));
			}
			else
			{
				// both left and result are bit
				emitpcode(POC_BSF, popGet(AOP(IC_RESULT(ic)), 0));
				emitpcode(POC_BTSZ, popGet(AOP(IC_LEFT(ic)), 0));
				emitpcode(POC_BCF, popGet(AOP(IC_RESULT(ic)), 0));
			}

        }
        else
        {
			if (resultX)
				emitpcode(POC_CLRF, PCOP_INDF0);
			else
				emitpcode (POC_CLRF, popGet (AOP (IC_RESULT (ic)), 0));
            emitpcode (POC_BTSS, popGet (AOP (IC_LEFT (ic)), 0));
			if (resultX)
				emitpcode(POC_INF, PCOP_INDF0);
			else
				emitpcode (POC_INF, popGet (AOP (IC_RESULT (ic)), 0));
        }
        goto release;
    }

    size = AOP_SIZE (IC_LEFT (ic));
	if(size!=1 || op_isLitLike(IC_LEFT(ic)))
		mov2w (AOP (IC_LEFT (ic)), 0);
    if (size == 1)
    {
        if (op_isLitLike(IC_LEFT(ic)))
            emitpcode(POC_IORL, popGetAddr(AOP(IC_LEFT(ic)), 0, 0));
        else
            emitpcode(POC_RRFW, popGet(AOP(IC_LEFT(ic)), 0));
    }
    else
        while (--size > 0) // for 2 bytes, byte 0 or byte 1 is ok, for 1 byte... it is an issue
        {
            if (op_isLitLike (IC_LEFT (ic)))
                emitpcode (POC_IORL, popGetAddr (AOP (IC_LEFT (ic)), size, 0));
            else
                emitpcode (POC_IORFW, popGet (AOP (IC_LEFT (ic)), size));
        }
	if (resultbit)
	{
		emitpcode(POC_BCF, popGet(AOP(IC_RESULT(ic)), 0));
		emitSKPNZ;
		emitpcode(POC_BSF, popGet(AOP(IC_RESULT(ic)), 0));
		goto release;
	}
    emitpcode (POC_MVL, popGetLit (0));
    emitSKPNZ;
    emitpcode (POC_MVL, popGetLit (1));
	//emitpcode(POC_MVFW, PCOP(&pc_status));// z is bit 0 .. what a faver?.. very incompatible
	//emitpcode(POC_ANDLW, popGetLit(0x01));
	//emitpcode(POC_XORLW, popGetLit(1));
	
	if(resultX)
		emitpcode(POC_MVWF, PCOP_POINC0);
	else
		movwf (AOP (IC_RESULT (ic)), 0);

    for (size = 1; size < AOP_SIZE (IC_RESULT (ic)); size++)
    {
		if (resultX)
			emitpcode(POC_CLRF, PCOP_POINC0);
		else
			emitpcode (POC_CLRF, popGet (AOP (IC_RESULT (ic)), size));
    }
    goto release;

release:
    /* release the aops */
    freeAsmop (IC_LEFT (ic), NULL, ic, (RESULTONSTACK (ic) ? 0 : 1));
    freeAsmop (IC_RESULT (ic), NULL, ic, TRUE);
}


/*-----------------------------------------------------------------*/
/* genCpl - generate code for complement                           */
/*-----------------------------------------------------------------*/
static void
genCpl (iCode * ic)
{
    operand *left, *result;
    int size, offset = 0;
	int resultX = 0;

    FENTRY;

    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);
    aopOp ((left = IC_LEFT (ic)), ic, FALSE);
    aopOp ((result = IC_RESULT (ic)), ic, TRUE);
	resultX = IS_TRUE_SYMOP(IC_RESULT(ic)) && IN_XSPACE(SPEC_OCLS(OP_SYMBOL(IC_RESULT(ic))->etype));
	if (resultX)
		emitldpr(popGet(AOP(IC_RESULT(ic)), 0));
    /* if both are in bit space then
       a special case */
    //if (AOP_TYPE (result) == AOP_CRY && AOP_TYPE (left) == AOP_CRY)
    //{

    //    HY08A_emitcode ("mov", "c,%s", left->aop->aopu.aop_dir);
    //    HY08A_emitcode ("cpl", "c");
    //    HY08A_emitcode ("mov", "%s,c", result->aop->aopu.aop_dir);
    //    goto release;
    //}

	

    size = AOP_SIZE (result);
    if (AOP_SIZE (left) < size)
        size = AOP_SIZE (left);
    while (size--)
    {
        if(op_isLitLike(left))
        {
        emitpcode (POC_MVL, popGet (AOP (left), offset));
        emitpcode (POC_XORLW, popGetLit(0xff));
        }
        else
        {
        emitpcode (POC_COMFW, popGet (AOP (left), offset));
        }
		if(resultX)
			emitpcode(POC_MVWF, PCOP_POINC0);
		else
			emitpcode (POC_MVWF, popGet (AOP (result), offset));
        offset++;
    }
    addSign (result, AOP_SIZE (left), !SPEC_USIGN (operandType (result)));


//release:
    /* release the aops */
    freeAsmop (left, NULL, ic, (RESULTONSTACK (ic) ? 0 : 1));
    freeAsmop (result, NULL, ic, TRUE);
}

/*-----------------------------------------------------------------*/
/* genUminusFloat - unary minus for floating points                */
/*-----------------------------------------------------------------*/
static void
genUminusFloat (operand * op, operand * result)
{
    int size;

    FENTRY;

    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);
    /* for this we just need to flip the
       first it then copy the rest in place */
    size = AOP_SIZE (op) - 1;

    if (HY08A_sameRegs(AOP(op), AOP(result)))
    {
        //emitpcode(POC_MVL, popGetLit(0x80));
        pCodeOp *pcop1 = popGet(AOP(result), size);
        pCodeOp *pcop2 = newpCodeOpBit(PCOR(pcop1)->r->name, 7, 0); // this is name, too
		if (PCOR(pcop1)->r->size > 2)
			PCORB(pcop2)->pcor.instance = PCOR(pcop1)->instance;
        emitpcode(POC_BTGF, pcop2);
    }
    else
    {
        mov2w_op(op, size);
        emitpcode(POC_XORLW, popGetLit(0x80));
        movwf(AOP(result), size);

        while (size--)
        {
            mov2w_op(op, size);
            movwf(AOP(result), size);
        }                           // while
    }
}

/*-----------------------------------------------------------------*/
/* genUminus - unary minus code generation                         */
/*-----------------------------------------------------------------*/
static void
genUminus (iCode * ic)
{
    int size, i;
    sym_link *optype;
	int resultX;
	int resultRET = 0;

    FENTRY;

    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);
    /* assign asmops */
    aopOp (IC_LEFT (ic), ic, FALSE);
    aopOp (IC_RESULT (ic), ic, TRUE);

	// special case, because uminus need inv, +1,
	// if following operation is RET 
	// we shall do the result to STKXX directly
	if (ic->next && ic->next->op == RETURN && IC_RESULT(ic) == IC_LEFT(ic->next) &&
		!IS_TRUE_SYMOP(IC_RESULT(ic)))
	{
		resultRET = 1;
	}

	resultX = IS_TRUE_SYMOP(IC_RESULT(ic)) && IN_XSPACE(SPEC_OCLS(OP_SYMBOL(IC_RESULT(ic))->etype));
	
    /* if both in bit space then special
       case */
    if (AOP_TYPE (IC_RESULT (ic)) == AOP_CRY && AOP_TYPE (IC_LEFT (ic)) == AOP_CRY)
    {
		if (resultX)
		{
			//if (resultX)
				emitldpr(popGet(AOP(IC_RESULT(ic)), 0));
			pCodeOp *oldop = popGet(AOP(IC_RESULT(ic)), 0);
			pCodeOp *indfbit = newpCodeOpBit("_INDF0", PCORB(oldop)->bit, 0);
			emitpcode(POC_BCF, indfbit);
			emitpcode(POC_BTSS, popGet(AOP(IC_LEFT(ic)), 0));
			emitpcode(POC_BSF, indfbit);
		}
		else
		{
			emitpcode(POC_BCF, popGet(AOP(IC_RESULT(ic)), 0));
			emitpcode(POC_BTSS, popGet(AOP(IC_LEFT(ic)), 0));
			emitpcode(POC_BSF, popGet(AOP(IC_RESULT(ic)), 0));
		}
        goto release;
    }

    optype = operandType (IC_LEFT (ic));

    /* if float then do float stuff */
    if (IS_FLOAT (optype))
    {
        genUminusFloat (IC_LEFT (ic), IC_RESULT (ic));
        goto release;
    }

    /* otherwise subtract from zero by taking the 2's complement */
    size = AOP_SIZE (IC_LEFT (ic));
	if (resultX)
		emitldpr(popGet(AOP(IC_RESULT(ic)), 0)); 


    // if size = 1 .. use SUBL 0 .. seems not easy, too, COMF INF   ... MVFW SUBL MVWF ? not good
    
    for (i = 0; i < size; i++)
    {
		if (HY08A_sameRegs(AOP(IC_LEFT(ic)), AOP(IC_RESULT(ic))) && !resultRET)
		{
			if (resultX)
				emitpcode(POC_COMF, PCOP_POINC0);
			else
			{

				emitpcode(POC_COMF, popGet(AOP(IC_RESULT(ic)), i));
				
			}
		}
        else
        {
			// special case
			
            emitpcode (POC_COMFW, popGet (AOP (IC_LEFT (ic)), i));
			
			if (resultX)
				emitpcode(POC_MVWF, PCOP_POINC0);
			else if(!(size == 2 && i == 1 && resultRET)) // this is special case
			{
				if (resultRET && (i!=size-1))
				{
					emitpcode(POC_MVWF, get_argument_pcop(size - 1 - i));
				}
				else
				{
					emitpcode(POC_MVWF, popGet(AOP(IC_RESULT(ic)), i));
				}
			}
        }
    }
	// ADD 1 to them

	

	if (size == 2)
	{
		if (resultX)
			emitpcode(POC_INSUZ, PCOP_POINC0);
		else
		{
			if (resultRET)
			{
				emitpcode(POC_INSUZ, get_argument_pcop(1));
			}else
				emitpcode(POC_INSUZ, popGet(AOP(IC_RESULT(ic)), 0));
		}
		//emitSKPNC;
		if (resultX)
			emitpcode(POC_INF, PCOP_POINC0);
		else
		{
			if (resultRET)
			{
				//emitpcode(POC_INFW, popGet(AOP(IC_RESULT(ic)), 1));// to W directly!!
				emitpcode( POC_ADDLW, popGetLit(1));
				IC_LEFT(ic->next) = NULL; // no return var
			}else
			emitpcode(POC_INF, popGet(AOP(IC_RESULT(ic)), 1));
		}
		goto release;
	}
	if (resultX)
		emitpcode(POC_INF, PCOP_POINC0);
	else
	{
		if (resultRET && size > 1)
		{
			emitpcode(POC_INF, get_argument_pcop(size - 1));
		}else
			emitpcode(POC_INF, popGet(AOP(IC_RESULT(ic)), 0));
	}
	emitpcode(POC_MVL, popGetLit(0));
    for (i = 1; i < size; i++)
    {
		if (resultX)
		{
			emitpcode(POC_ADCWF, PCOP_POINC0);
		}
		else
		{
			if (resultRET)
			{
				if (i != size - 1)
				{
					emitpcode(POC_ADCWF, get_argument_pcop(size - 1 - i));
				}
				else
				{
					emitpcode(POC_ADCFW, popGet(AOP(IC_RESULT(ic)), i));
					IC_LEFT(ic->next) = NULL; // no return var
				}
			}
			else
			{
				emitpcode(POC_ADCWF, popGet(AOP(IC_RESULT(ic)), i));
			}
		}
    }
release:
    /* release the aops */
    freeAsmop (IC_LEFT (ic), NULL, ic, (RESULTONSTACK (ic) ? 0 : 1));
    freeAsmop (IC_RESULT (ic), NULL, ic, TRUE);
}

/*-----------------------------------------------------------------*/
/* saverbank - saves an entire register bank on the stack          */
/*-----------------------------------------------------------------*/
static void
saverbank (int bank, iCode * ic, bool pushPsw)
{
    FENTRY;

    DEBUGHY08A_emitcode ("; ***", "%s  %d - WARNING no code generated", __FUNCTION__, __LINE__);
#if 0
    int i;
    asmop *aop;
    regs *r = NULL;

    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);
    if (options.useXstack)
    {

        aop = newAsmop (0);
        r = getFreePtr (ic, &aop, FALSE);
        HY08A_emitcode ("mov", "%s,_spx", r->name);

    }

    for (i = 0; i < HY08A_nRegs; i++)
    {
        if (options.useXstack)
        {
            HY08A_emitcode ("inc", "%s", r->name);
            //HY08A_emitcode("mov","a,(%s+%d)",
            //       regsHY08A[i].base,8*bank+regsHY08A[i].offset);
            HY08A_emitcode ("movx", "@%s,a", r->name);
        }
        else
            HY08A_emitcode ("push", "");    // "(%s+%d)",
        //regsHY08A[i].base,8*bank+regsHY08A[i].offset);
    }

    if (pushPsw)
    {
        if (options.useXstack)
        {
            HY08A_emitcode ("mov", "a,psw");
            HY08A_emitcode ("movx", "@%s,a", r->name);
            HY08A_emitcode ("inc", "%s", r->name);
            HY08A_emitcode ("mov", "_spx,%s", r->name);
            freeAsmop (NULL, aop, ic, TRUE);

        }
        else
            HY08A_emitcode ("push", "psw");

        HY08A_emitcode ("mov", "psw,#0x%02x", (bank << 3) & 0x00ff);
    }
    ic->bankSaved = 1;
#endif
}

/*-----------------------------------------------------------------*/
/* saveRegisters - will look for a call and save the registers     */
/*-----------------------------------------------------------------*/
static void
saveRegisters (iCode * lic)
{
    iCode *ic;
    sym_link *dtype;

    FENTRY;

    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);

    /* look for call */
    for (ic = lic; ic; ic = ic->next)
        if (ic->op == CALL || ic->op == PCALL)
            break;

    if (!ic)
    {
        fprintf (stderr, "found parameter push with no function call\n");
        return;
    }

    /* if the registers have been saved already then do nothing */
	// special case
	if (IS_SYMOP(IC_LEFT(ic)))
	{
		if (ic->regsSaved || IFFUNC_CALLEESAVES(OP_SYMBOL(IC_LEFT(ic))->type))
			return;
	}
    /* find the registers in use at this time
       and push them away to safety */
    bitVectCplAnd (bitVectCopy (ic->rMask), ic->rUsed);

    ic->regsSaved = 1;

    //fprintf(stderr, "ERROR: saveRegisters did not do anything to save registers, please report this as a bug.\n");

    dtype = operandType (IC_LEFT (ic));
    if (currFunc && dtype &&
            (FUNC_REGBANK (currFunc->type) != FUNC_REGBANK (dtype)) && IFFUNC_ISISR (currFunc->type) && !ic->bankSaved)
    {
        saverbank (FUNC_REGBANK (dtype), ic, TRUE);
    }
}

/*-----------------------------------------------------------------*/
/* unsaveRegisters - pop the pushed registers                      */
/*-----------------------------------------------------------------*/
static void
unsaveRegisters (iCode * ic)
{
    int i;
    bitVect *rsave;

    FENTRY;

    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);
    /* find the registers in use at this time
       and push them away to safety */
    rsave = bitVectCplAnd (bitVectCopy (ic->rMask), ic->rUsed);

    if (options.useXstack)
    {
        HY08A_emitcode ("mov", "r0,%s", spname);
        for (i = HY08A_nRegs; i >= 0; i--)
        {
            if (bitVectBitValue (rsave, i))
            {
                HY08A_emitcode ("dec", "r0");
                HY08A_emitcode ("movx", "a,@r0");
                HY08A_emitcode ("mov", "%s,a", HY08A_regWithIdx (i)->name);
            }

        }
        HY08A_emitcode ("mov", "%s,r0", spname);
    }                           //else
    //for (i =  HY08A_nRegs ; i >= 0 ; i--) {
    //  if (bitVectBitValue(rsave,i))
    //  HY08A_emitcode("pop","%s",HY08A_regWithIdx(i)->dname);
    //}

}


#if 0
/*-----------------------------------------------------------------*/
/* pushSide -                */
/*-----------------------------------------------------------------*/
static void
pushSide (operand * oper, int size)
{
    int offset = 0;
    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);
    while (size--)
    {
        char *l = aopGet (AOP (oper), offset++, FALSE, TRUE);
        if (AOP_TYPE (oper) != AOP_REG && AOP_TYPE (oper) != AOP_DIR && strcmp (l, "a"))
        {
            HY08A_emitcode ("mov", "a,%s", l);
            HY08A_emitcode ("push", "acc");
        }
        else
            HY08A_emitcode ("push", "%s", l);
    }
}
#endif

/*-----------------------------------------------------------------*/
/* assignResultValue -               */
/*-----------------------------------------------------------------*/
static void
assignResultValue (operand * oper, operand *thecall)
{
    int size = AOP_SIZE (oper);
    int offset = 0;

    FENTRY;

    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);

    DEBUGHY08A_AopType (__LINE__, oper, NULL, NULL);

    /* assign MSB first (passed via WREG) */

    while (size--)
    {
        get_returnvalue(oper, size, offset + GpsuedoStkPtr, thecall);
        GpsuedoStkPtr++;
    }

}

static void 
genPointerPush(iCode *ic)
{
	// no need 3 byte pointer yet
	//char *func[] = { NULL, "__gptrget1", "__gptrget2", "__gptrget3", "__gptrget4",NULL,NULL,NULL,"__gptrget8" };
	char *func2[] = { NULL, "__g2ptrget1", "__g2ptrget2", "__g2ptrget3", "__g2ptrget4",NULL,NULL,NULL,"__g2ptrget8" };

	sym_link *type, *etype;
	int i;
	int p_type = -1;
	FENTRY;
	//int litnow = -1;
	if (HY08A_getPART()->isEnhancedCore < 3 || HY08A_getPART()->isEnhancedCore == 5)
	{
		fprintf(stderr, "%s:%d, Sorry, Variable Length parameter is not support on this target.\n",
			ic->filename, ic->lineno
		);
		exit(-1003);
	}

	//DEBUGHY08A_emitcode ("; ***", "%s  %d - WARNING no code generated", __FUNCTION__, __LINE__);

	int size, offset = 0;
	/* if this is not a parm push : ie. it is spill push
	   and spill push is always done on the local stack */
	if (!ic->parmPush)
	{

		/* and the item is spilt then do nothing */
		if (OP_SYMBOL(IC_LEFT(ic))->isspilt)
			return;

		aopOp(IC_LEFT(ic), ic, FALSE);
		size = AOP_SIZE(IC_LEFT(ic));
		if (IS_SYMOP(IC_LEFT(ic)) && IS_FUNCPTR(OP_SYMBOL(IC_LEFT(ic))->type))
			size = 2;
		/* push it on the stack */
		while (size--)
		{
			mov2w(AOP(IC_LEFT(ic)), offset++);
			//HY08A_emitcode ("push", "%s", l);
			PUSHW;
		}
		freeAsmop(IC_LEFT(ic), NULL, ic, TRUE);
		return;
	}
	aopOp(IC_LEFT(ic), ic, FALSE);
	//size = AOP_SIZE(IC_LEFT(ic));
	type = operandType(IC_LEFT(ic));
	size = getSize(type->next);
	var_param_len += size;
	etype = getSpec(type);
	if (IS_PTR(type) && !IS_FUNC(type->next))
		p_type = DCL_TYPE(type);
	else
	{
		/* we have to go by the storage class */
		p_type = PTR_TYPE(SPEC_OCLS(etype));

		DEBUGHY08A_emitcode("; ***", "%d - resolve pointer by storage class", __LINE__);

		if (SPEC_OCLS(etype)->codesp)
		{
			DEBUGHY08A_emitcode("; ***", "%d - cpointer", __LINE__);
			//p_type = CPOINTER ;
		}
		else if (SPEC_OCLS(etype)->fmap && !SPEC_OCLS(etype)->paged)
			DEBUGHY08A_emitcode("; ***", "%d - fpointer", __LINE__);
		/*p_type = FPOINTER ; */
		else if (SPEC_OCLS(etype)->fmap && SPEC_OCLS(etype)->paged)
			DEBUGHY08A_emitcode("; ***", "%d - ppointer", __LINE__);
		/*        p_type = PPOINTER; */
		else if (SPEC_OCLS(etype) == idata)
			DEBUGHY08A_emitcode("; ***", "%d - ipointer", __LINE__);
		/*      p_type = IPOINTER; */
		else
			DEBUGHY08A_emitcode("; ***", "%d - pointer", __LINE__);
		/*      p_type = POINTER ; */
	}

	switch (p_type)
	{
	case POINTER:
	case FPOINTER:
		setup_fsr(IC_LEFT(ic));
		for (i = 0; i < size; i++)
		{
			if (size == 1)
				emitpcode(POC_MVFW, popCopyReg(&pc_indf0));
			else
				emitpcode(POC_MVFW, popCopyReg(&pc_poinc0));
			emitpcode(POC_MVWF, popCopyReg(&pc_princ2));
		}
		break;
	case CPOINTER:
		mov2w_op(IC_LEFT(ic), 0);
		emitpcode(POC_MVWF, popRegFromIdx2(Gstack_base_addr, func2[size], 1, 0));
		mov2w_op(IC_LEFT(ic), 1);
		if (PCI(pb->pcTail)->pcop->type == PO_IMMEDIATE) // it must exist!!
			PCOI(PCI(pb->pcTail)->pcop)->_cp2gp = 1;
		else
			emitpcode(POC_IORL, popGetLit(GPTRTAG_CODE));    /* GPOINTER tag for __code space */
		call_libraryfunc(func2[size]);
		// re-ordering
		
		for (i = size-2; i >=0; i--)
		{
			emitpcode2(POC_MVFF, popRegFromIdx(Gstack_base_addr  - i), popCopyReg(&pc_princ2));			
		}
		emitpcode(POC_MVWF, popCopyReg(&pc_princ2));
		break;

	case GPOINTER:
		if (size <=8 && func2[size] != NULL)
		{
			mov2w(AOP(IC_LEFT(ic)), 0);
			emitpcode(POC_MVWF, popRegFromIdx2(Gstack_base_addr, func2[size], 1, 0));
			mov2w(AOP(IC_LEFT(ic)), 1);
			call_libraryfunc(func2[size]);
			for (i = size - 2; i >= 0; i--)
			{
				emitpcode2(POC_MVFF, popRegFromIdx(Gstack_base_addr - i), popCopyReg(&pc_princ2));
			}
			emitpcode(POC_MVWF, popCopyReg(&pc_princ2));
		}
		else
		{
			// we limit size to 8 bytes
			int size1,off;
			if (size > 8)
			{
				fprintf(stderr, "Error!! %s:%d push struct greater than 8 bytes not support!!\n",ic->filename, ic->lineno);
				exit(-1);
			}
			for (size1 = size; size1 < 8; size1++)
				if (func2[size1])
					break;
			off = size1 - size;
			mov2w(AOP(IC_LEFT(ic)), 0);
			emitpcode(POC_MVWF, popRegFromIdx2(Gstack_base_addr, func2[size1], 1, 0));
			mov2w(AOP(IC_LEFT(ic)), 1);
			call_libraryfunc(func2[size1]);
			for (i = size - 2; i >= 0; i--)
			{
				emitpcode2(POC_MVFF, popRegFromIdx(Gstack_base_addr - i -off), popCopyReg(&pc_princ2));
			}
			emitpcode(POC_MVFW, popRegFromIdx(Gstack_base_addr +1 - off));
			emitpcode(POC_MVWF, popCopyReg(&pc_princ2));


		}
		break;
	}
	
	/* push it on the stack */
	freeAsmop(IC_LEFT(ic), NULL, ic, TRUE);
	return;



}

/*-----------------------------------------------------------------*/
/* genIpush - genrate code for pushing this gets a little complex  */
/*-----------------------------------------------------------------*/
static void
genIpush (iCode * ic)
{
    FENTRY;
	int litnow = -1;
	if (HY08A_getPART()->isEnhancedCore <3 || HY08A_getPART()->isEnhancedCore==5)
	{
		fprintf(stderr, "%s:%d, Sorry, Variable Length parameter is not support on this target.\n",
			ic->filename, ic->lineno
			);
		exit(-1003);
	}

    //DEBUGHY08A_emitcode ("; ***", "%s  %d - WARNING no code generated", __FUNCTION__, __LINE__);

    int size, offset = 0;
    /* if this is not a parm push : ie. it is spill push
       and spill push is always done on the local stack */
    if (!ic->parmPush)
    {
		
        /* and the item is spilt then do nothing */
        if (OP_SYMBOL (IC_LEFT (ic))->isspilt)
            return;

        aopOp (IC_LEFT (ic), ic, FALSE);
        size = AOP_SIZE (IC_LEFT (ic));
		if (IS_SYMOP(IC_LEFT(ic)) && IS_FUNCPTR(OP_SYMBOL(IC_LEFT(ic))->type))
			size = 2;
        /* push it on the stack */
        while (size--)
        {
			mov2w(AOP(IC_LEFT(ic)), offset++);
				//HY08A_emitcode ("push", "%s", l);
			PUSHW;
        }
		freeAsmop(IC_LEFT(ic), NULL, ic, TRUE);
        return;
    }
	aopOp(IC_LEFT(ic), ic, FALSE);
	size = AOP_SIZE(IC_LEFT(ic));
	var_param_len += size;
	/* push it on the stack */
	while (size--)
	{
		asmop *aop0 = AOP(IC_LEFT(ic));
		if (aop0->type == AOP_LIT)
		{
			pCodeOp *litop = popGetAddr(aop0, offset++, 0);
			if (PCOL(litop)->lit == 0)
			{
				emitpcode(POC_CLRF,PCOP(&pc_princ2));
			}
			else if (PCOL(litop)->lit == 0xff)
			{
				emitpcode(POC_SETF, PCOP(&pc_princ2));
			}
			else
			{
				if (litnow != PCOL(litop)->lit)
				{
					emitpcode(POC_MVL, litop);
					litnow = PCOL(litop)->lit;
				}
				
				PUSHW;
			}
		}
		else
		{
			mov2w(AOP(IC_LEFT(ic)), offset++);
			//HY08A_emitcode ("push", "%s", l);
			PUSHW;
		}
	}
	freeAsmop(IC_LEFT(ic), NULL, ic, TRUE);
	return;


    freeAsmop (IC_LEFT (ic), NULL, ic, TRUE);
}

/*-----------------------------------------------------------------*/
/* genIpop - recover the registers: can happen only for spilling   */
/*-----------------------------------------------------------------*/
static void
genIpop (iCode * ic)
{
    FENTRY;

    DEBUGHY08A_emitcode ("; ***", "%s  %d - WARNING no code generated", __FUNCTION__, __LINE__);
    assert (!"genIpop -- unimplemented");
#if 0
    int size, offset;


    /* if the temp was not pushed then */
    if (OP_SYMBOL (IC_LEFT (ic))->isspilt)
        return;

    aopOp (IC_LEFT (ic), ic, FALSE);
    size = AOP_SIZE (IC_LEFT (ic));
    offset = (size - 1);
    while (size--)
        HY08A_emitcode ("pop", "%s", aopGet (AOP (IC_LEFT (ic)), offset--, FALSE, TRUE));

    freeAsmop (IC_LEFT (ic), NULL, ic, TRUE);
#endif
}

/*-----------------------------------------------------------------*/
/* unsaverbank - restores the resgister bank from stack            */
/*-----------------------------------------------------------------*/
static void
unsaverbank (int bank, iCode * ic, bool popPsw)
{
    FENTRY;

    DEBUGHY08A_emitcode ("; ***", "%s  %d - WARNING no code generated", __FUNCTION__, __LINE__);
#if 0
    int i;
    asmop *aop;
    regs *r = NULL;

    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);
    if (popPsw)
    {
        if (options.useXstack)
        {
            aop = newAsmop (0);
            r = getFreePtr (ic, &aop, FALSE);


            HY08A_emitcode ("mov", "%s,_spx", r->name);
            HY08A_emitcode ("movx", "a,@%s", r->name);
            HY08A_emitcode ("mov", "psw,a");
            HY08A_emitcode ("dec", "%s", r->name);

        }
        else
            HY08A_emitcode ("pop", "psw");
    }

    for (i = (HY08A_nRegs - 1); i >= 0; i--)
    {
        if (options.useXstack)
        {
            HY08A_emitcode ("movx", "a,@%s", r->name);
            //HY08A_emitcode("mov","(%s+%d),a",
            //     regsHY08A[i].base,8*bank+regsHY08A[i].offset);
            HY08A_emitcode ("dec", "%s", r->name);

        }
        else
            HY08A_emitcode ("pop", "");     //"(%s+%d)",
        //regsHY08A[i].base,8*bank); //+regsHY08A[i].offset);
    }

    if (options.useXstack)
    {

        HY08A_emitcode ("mov", "_spx,%s", r->name);
        freeAsmop (NULL, aop, ic, TRUE);

    }
#endif
}

/*-----------------------------------------------------------------*/
/* genCall - generates a call statement                            */
/*-----------------------------------------------------------------*/
static void
genCall (iCode * ic)
{
    sym_link *dtype;
    symbol *sym=NULL; // possible lit call
    char *name=NULL;
    int isExtern;
	pCode *nowFunc;
	int litcall = 0;
	int callAddr = 0;
	int paraByteNum = 0; // 2019 MAY tell linker the function should have XX parameter byte
	// or the system will hang
    FENTRY;

    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);

    /* if caller saves & we have not saved then */
    if (!ic->regsSaved)
        saveRegisters (ic);

    /* if we are calling a function that is not using
       the same register bank then we need to save the
       destination registers on the stack */
    dtype = operandType (IC_LEFT (ic));
    if (currFunc && dtype &&
            (FUNC_REGBANK (currFunc->type) != FUNC_REGBANK (dtype)) && IFFUNC_ISISR (currFunc->type) && !ic->bankSaved)

        saverbank (FUNC_REGBANK (dtype), ic, TRUE);

    /* if send set is not empty the assign */
	// special ase
	if (IS_VALOP(IC_LEFT(ic)))
	{
		litcall = 1;
		callAddr = (int)ulFromVal(OP_VALUE(IC_LEFT(ic)));
	}
	else
	{
		sym = OP_SYMBOL(IC_LEFT(ic));
		name = sym->rname[0] ? sym->rname : sym->name;
	}

	if (sym && IFFUNC_ISCRITICAL(sym->type))
	{
		genCritical(ic);
	}


    if (_G.sendSet)
    {
        iCode *sic;
        /* For the port, there is no data stack.
         * So parameters passed to functions are stored
         * in registers. (The pCode optimizer will get
         * rid of most of these :).
         */

        int psuedoStkPtr = -1;
        int firstTimeThruLoop = 1;
		int selfCall = 0;
		int warned = 0;
		struct value *pvalue;
		struct value *finalpara=NULL;
        sym_link **paraTypes=NULL;
        int sendItemNum = elementsInSet(_G.sendSet);
        paraTypes = (sym_link**) calloc(sendItemNum,sizeof(sym_link*));

		for (nowFunc = pb->pcHead; nowFunc; nowFunc = nowFunc->next)
			if (nowFunc->type == PC_FUNCTION)
				break;

		if (!sym ||( nowFunc && !strcmp(PCF(nowFunc)->fname, sym->rname)))
			selfCall = 1;
        _G.sendSet = reverseSet (_G.sendSet);

        /* First figure how many parameters are getting passed */
		if (!sym || sym->type->funcAttrs.hasVargs)
		{
			for (sic = setFirstItem(_G.sendSet); sic; sic = setNextItem(_G.sendSet))
			{

				aopOp(IC_LEFT(sic), sic, FALSE);
				psuedoStkPtr += AOP_SIZE(IC_LEFT(sic));
				freeAsmop(IC_LEFT(sic), NULL, sic, FALSE);
            
			}
			paraByteNum = -1; // this means it is variable length
		}
		else // determine the size by declaration!!
		{
            int i=0;
			for (pvalue = sym->type->funcAttrs.args; pvalue; pvalue = pvalue->next)
			{
				psuedoStkPtr += getSize(pvalue->type);
				paraByteNum += getSize(pvalue->type);
                paraTypes[i++]=pvalue->type;
				if (pvalue->next == NULL)
					finalpara = pvalue;
			}
		}
        int j;
        for (j=0,sic = setFirstItem (_G.sendSet); sic; sic = setNextItem (_G.sendSet),j++)
        {
            int size, offset = 0;
			int wvalue = -1;
			int sizeneed = -1;

			if (finalpara)
				sizeneed = getSize(finalpara->type);
            // only when parameter is struct, the call should push struct!!
            aopOpSend (IC_LEFT (sic), sic, FALSE, IS_STRUCT(paraTypes[sendItemNum-1-j]));
            size = abs(AOP_SIZE (IC_LEFT (sic)));

			// we need to match each parameter to see if size is correct!!
			// special case for bug-2274.c
			if (size == 0) offset = 1;
            while (size--)
            {
                DEBUGHY08A_emitcode ("; ", "%d left %s", __LINE__, AopType (AOP_TYPE (IC_LEFT (sic))));
				if (selfCall && (HY08A_getPART()->isEnhancedCore < 3 || HY08A_getPART()->isEnhancedCore==5) && !warned)
				{
					fprintf(stderr, "Warning, this target does not support recursive function, please correct it, at  %s: %d\n"
					,ic->filename, ic->lineno);
					warned = 1;
					//exit(-2100);
				}

                if (!firstTimeThruLoop)
                {
                    /* If this is not the first time we've been through the loop
                     * then we need to save the parameter in a temporary
                     * register. The last byte of the last parameter is
                     * passed in W. */
                    if (PCI(pb->pcTail)->op == POC_MVL &&
                            PCI(pb->pcTail)->pcop->type == PO_LITERAL && PCOL(PCI(pb->pcTail)->pcop)->lit == 0)
                    {
                        // change the pcode
                         pb->pcTail->prev->next = (pCode *)newpCode(POC_CLRF, (0) ? popRegFromIdx(Gstack_base_addr - --psuedoStkPtr) :
                                                              selfCall?PCOP(&pc_princ2): popRegFromIdx2(Gstack_base_addr - --psuedoStkPtr, name, 1, !sym || IS_STATIC(sym->etype)));
						 pb->pcTail->prev->next->prev = pb->pcTail->prev;
						 pb->pcTail = pb->pcTail->prev->next;
						
                    } else if (PCI(pb->pcTail)->op == POC_MVL &&
                               PCI(pb->pcTail)->pcop->type == PO_LITERAL && PCOL(PCI(pb->pcTail)->pcop)->lit == 255)
                    {
                        pb->pcTail->prev->next = (pCode *)newpCode(POC_SETF, (0) ? popRegFromIdx(Gstack_base_addr - --psuedoStkPtr) :
                                                              selfCall?PCOP(&pc_princ2):popRegFromIdx2(Gstack_base_addr - --psuedoStkPtr, name, 1, !sym || IS_STATIC(sym->etype)));
						pb->pcTail->prev->next->prev = pb->pcTail->prev;
						pb->pcTail = pb->pcTail->prev->next;
					}
                    else
                    {
						if (!(0))
						{
							emitpcode(POC_MVWF, selfCall ? PCOP(&pc_princ2) : popRegFromIdx2(Gstack_base_addr - --psuedoStkPtr, name, 1, !sym || IS_STATIC(sym->etype)));

						}
						else
                            emitpcode(POC_MVWF, popRegFromIdx(Gstack_base_addr - --psuedoStkPtr));
						
                    }
					//PCI(pb->pcTail)->pcop->calleePara = 1; // this is the para of callee!!

                }
                firstTimeThruLoop = 0;

                mov2w_op_cont (IC_LEFT (sic), offset, &wvalue);
                offset++;
            }
            freeAsmop (IC_LEFT (sic), NULL, sic, TRUE);

			while (offset < sizeneed)
			{
				emitpcode(POC_MVWF, selfCall ? PCOP(&pc_princ2) : popRegFromIdx2(Gstack_base_addr - --psuedoStkPtr, name, 1, !sym || IS_STATIC(sym->etype)));
				//mov2w_op_cont(IC_LEFT(sic), offset, &wvalue);
				emitpcode(POC_MVL, popGetLit(0)); // append zero
				offset++;
			}
			if (finalpara)
			{
				for (pvalue = sym->type->funcAttrs.args; pvalue; pvalue = pvalue->next)
				{
					if (pvalue->next == finalpara)// find previous one
					{
						finalpara = pvalue;
						break;
					}
				}
			}
        }
        free (paraTypes);
        _G.sendSet = NULL;
    }
	else if (!litcall && sym && sym->type->funcAttrs.hasVargs && HY08A_getPART()->isEnhancedCore==3)
	{
		// push the var length in stack!!
		emitpcode(POC_MVL, popGetLit(0x100 - var_param_len-1)); // include self
		var_param_len = 0; // reset it
		//PUSHW; // push in function itself!!
	}
	
    /* make the call */

    /*
     * As SDCC emits code as soon as it reaches the end of each
     * function's definition, prototyped functions that are implemented
     * after the current one are always considered EXTERN, which
     * introduces many unneccessary PAGESEL instructions.
     * XXX: Use a post pass to iterate over all `CALL _name' statements
     * and insert `PAGESEL _name' and `PAGESEL $' around the CALL
     * only iff there is no definition of the function in the whole
     * file (might include this in the PAGESEL pass).
     */
    /*
    isExtern = IS_EXTERN (sym->etype) || HY08A_inISR;
    if (isExtern)
      {
        // Extern functions and ISRs maybe on a different page;
        // * must call pagesel
        emitpcode (POC_PAGESEL, popGetWithString (name, 1));
      }
    emitpcode (POC_CALL, popGetWithString (name, isExtern));
    if (isExtern)
      {
        // * May have returned from a different page;
        // * must use pagesel to restore PCLATH before next
        // * goto or call instruction
        emitpcode (POC_PAGESEL, popGetWithString ("$", 0));
      }
    */
	if (litcall)
	{
		char buf[100];
		snprintf(buf, 99, "0x%04X", callAddr); // asm will >>1, we don't >>1 here!!
		emitpcode(POC_CALL, popGetWithString(buf, 0));
	}
	else
	{
		isExtern = IS_EXTERN(sym->etype) || HY08A_inISR;
		emitpcode(POC_CALL, popGetWithString(name, isExtern));
		//if (isExtern)
		//{
		// all give exp-para num
			// we dump the parameter byte number for linker to check
			PCI(pb->pcTail)->exp_para_num = 65536 + paraByteNum;
		//}
	}

	if (sym && !sym->type->funcAttrs.paraNumChecked)
	{
		sym->type->funcAttrs.paraNumChecked = 1;
		PCI(pb->pcTail)->pcop->flags.needCheckPara = 1;
	}


	if (sym && sym->type->funcAttrs.hasVargs && HY08A_getPART()->isEnhancedCore >= 3 && HY08A_getPART()->isEnhancedCore!=5)
	{
		// push the var length in stack!!
		//emitpcode(POC_MVL, popGetLit(0x100 - var_param_len - 1)); // include self
		PCI(pb->pcTail)->exp_para_num = 65536+1000; // 1000 means variable para length, change previous setting
		while (var_param_len)
		{
			if (var_param_len > 32)
			{
				emitpcode2(POC_ADDFSR, popGetLit(2), newpCodeOpLitSign(-32));
				var_param_len -= 32;
			}

			else
			{
				emitpcode2(POC_ADDFSR, popGetLit(2), newpCodeOpLitSign(-var_param_len));
				var_param_len = 0;
			}
		}

	}


    GpsuedoStkPtr = 0;
    /* if we need assign a result value */
    if ((IS_ITEMP (IC_RESULT (ic)) &&
            (OP_SYMBOL (IC_RESULT (ic))->nRegs || OP_SYMBOL (IC_RESULT (ic))->spildir)) || IS_TRUE_SYMOP (IC_RESULT (ic)))
    {

        _G.accInUse++;
        aopOp (IC_RESULT (ic), ic, FALSE);
        _G.accInUse--;
        if(IS_SYMOP(IC_LEFT(ic)) && IS_SPEC(OP_SYMBOL(IC_LEFT(ic))->type->next) && getSizeRet(OP_SYMBOL(IC_LEFT(ic))->type->next)==-1)
        {
            // return C, change to W, optimize later
            emitpcode(POC_MVL, popGetLit(0));
            emitpcode(POC_RLCFW, PCOP(&pc_wreg));
        }

        assignResultValue (IC_RESULT (ic),NULL);

        DEBUGHY08A_emitcode ("; ", "%d left %s", __LINE__, AopType (AOP_TYPE (IC_RESULT (ic))));

        freeAsmop (IC_RESULT (ic), NULL, ic, TRUE);
    }

    /* if register bank was saved then pop them */
    if (ic->bankSaved)
        unsaverbank (FUNC_REGBANK (dtype), ic, TRUE);

    /* if we hade saved some registers then unsave them */
    if (ic->regsSaved && !IFFUNC_CALLEESAVES (dtype))
        unsaveRegisters (ic);


	if (sym && IFFUNC_ISCRITICAL(sym->type))
	{
		genEndCritical(ic);
	}

}

/*-----------------------------------------------------------------*/
/* genPcall - generates a call by pointer statement                */
/*-----------------------------------------------------------------*/
static void
genPcall (iCode * ic)
{
    sym_link *dtype;
    symbol *albl = newiTempLabel (NULL);
    symbol *blbl = newiTempLabel (NULL);
    HYA_OPCODE poc;
    pCodeOp *pcop;
    operand *left;
    pCodeOp *temp1, *temp2;
	//pCodeOp *temp3 = NULL; // special special case, bug-2663.c, which push before pcall
	int LinX = 0;
    int prevPushNoVarPara=0;

	symbol *sym=NULL;


    FENTRY;

    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);


    if( IS_VALOP(IC_LEFT(ic)))
    {
        // if lit, genCall directly

        genCall(ic);
        return;
    }
    
    sym = OP_SYMBOL(IC_LEFT(ic));
    prevPushNoVarPara = ((ic->prev)->op==IPUSH) && (!sym->type->next->funcAttrs.hasVargs);

	 

	

    /* if caller saves & we have not saved then */
    if (!ic->regsSaved)
        saveRegisters (ic);

    /* if we are calling a function that is not using
       the same register bank then we need to save the
       destination registers on the stack */
    dtype = operandType (IC_LEFT (ic));
    if (currFunc && dtype && IFFUNC_ISISR (currFunc->type) && (FUNC_REGBANK (currFunc->type) != FUNC_REGBANK (dtype)))
        saverbank (FUNC_REGBANK (dtype), ic, TRUE);

    left = IC_LEFT (ic);
    aopOp (left, ic, FALSE);
	if (left->type != VALUE)
	{
		LinX = (IS_TRUE_SYMOP(left) && IN_XSPACE(SPEC_OCLS(OP_SYMBOL(left)->etype)))
			|| OP_SYMBOL(left)->rematX;
	}
    DEBUGHY08A_AopType (__LINE__, left, NULL, NULL);

    if(op_isLitLike (IC_LEFT (ic)))
    {
        // reverse again?
        //if (_G.sendSet ) // for bug-2993.c 
          //  _G.sendSet = reverseSet(_G.sendSet);

        genCall(ic);
        return;
    }

    //poc = (op_isLitLike (IC_LEFT (ic)) ? POC_MVL : POC_MVFW);
    poc =  POC_MVFW;

//    pushSide (IC_LEFT (ic), FPTRSIZE);

    /* if send set is not empty, assign parameters */
    //if (_G.sendSet)
    //{

    //    DEBUGHY08A_emitcode ("; ***", "%s  %d - WARNING arg-passing to indirect call not supported", __FUNCTION__, __LINE__);
    //    /* no way to pass args - W always gets used to make the call */
    //}
    /* first idea - factor out a common helper function and call it.
       But don't know how to get it generated only once in its own block

       if(AOP_TYPE(IC_LEFT(ic)) == AOP_DIR) {
       char *rname;
       char *buffer;
       rname = IC_LEFT(ic)->aop->aopu.aop_dir;
       DEBUGHY08A_emitcode ("; ***","%s  %d AOP_DIR %s",__FUNCTION__,__LINE__,rname);
       buffer = Safe_calloc(1,strlen(rname)+16);
       sprintf(buffer, "%s_goto_helper", rname);
       addpCode2pBlock(pb,newpCode(POC_CALL,newpCodeOp(buffer,PO_STR)));
       free(buffer);
       }
     */
    emitpcode (POC_CALL, popGetLabel (albl->key));
    pcop = popGetLabel (blbl->key);
    //emitpcode (POC_PAGESEL, pcop);        /* Must restore PCLATH before goto, without destroying W */
    emitpcode (POC_JMP, pcop);
    emitpLabel (albl->key);
    emitCLRC;
	temp1 = get_argument_pcop(1);
	temp2 = get_argument_pcop(2);

	if (poc == POC_MVFW)
    {
		if (LinX  )
		{
			emitpcode(POC_LDPR, popGetAddr(AOP(left), 1,0)); // MSB FIRST!!
			emitpcode(POC_RRCFW, PCOP_PODEC0);
			emitpcode(POC_MVWF, temp1);
			emitpcode(POC_RRCFW, PCOP_INDF0);
			emitpcode(POC_MVWF, temp2);
		}
		else
		{
			emitpcode(POC_RRCFW, popGetAddr(AOP(left), 1, 0));
			emitpcode(POC_MVWF, temp1);
			emitpcode(POC_RRCFW, popGetAddr(AOP(left), 0, 0));
			emitpcode(POC_MVWF, temp2);
		}
		
    }
    // else
    // {
    //     emitpcode(poc, popGetAddr(AOP(left), 1, 0));
    //     emitpcode(POC_MVWF, temp1);
    //     emitpcode(POC_RRCF, temp1);
    //     emitpcode(poc, popGetAddr(AOP(left), 0, 0));
    //     emitpcode(POC_MVWF, temp2);
    //     emitpcode(POC_RRCF, temp2);
    // }
	// it is a pointer, so type->next!!

	// !! special issue, in pCall we also know it is var length

	if (sym->type->next->funcAttrs.hasVargs && HY08A_getPART()->isEnhancedCore ==3 ) // only for H08C
	{
		// push the var length in stack!!
		emitpcode(POC_MVL, popGetLit(0x100 - var_param_len-1));// include self
		var_param_len = 0; // reset it
						   //PUSHW; // push in function itself!!
	}
    // pass parameter here
    if (_G.sendSet )
    {
        iCode *sic;
        /* For the port, there is no data stack.
        * So parameters passed to functions are stored
        * in registers. (The pCode optimizer will get
        * rid of most of these :).
        */
        int psuedoStkPtr = -1;
        int firstTimeThruLoop = 1;

        _G.sendSet = reverseSet(_G.sendSet);

        /* First figure how many parameters are getting passed */
        for (sic = setFirstItem(_G.sendSet); sic; sic = setNextItem(_G.sendSet))
        {

            aopOp(IC_LEFT(sic), sic, FALSE);
            psuedoStkPtr += AOP_SIZE(IC_LEFT(sic));
            freeAsmop(IC_LEFT(sic), NULL, sic, FALSE);
        }

		
		
        for (sic = setFirstItem(_G.sendSet); sic; sic = setNextItem(_G.sendSet))
        {
            int size, offset = 0;

            aopOp(IC_LEFT(sic), sic, FALSE);
            size = AOP_SIZE(IC_LEFT(sic));

            while (size--)
            {
                DEBUGHY08A_emitcode("; ", "%d left %s", __LINE__, AopType(AOP_TYPE(IC_LEFT(sic))));

                if (!firstTimeThruLoop)
                {
                    /* If this is not the first time we've been through the loop
                    * then we need to save the parameter in a temporary
                    * register. The last byte of the last parameter is
                    * passed in W. */
                    if ( (HY08A_getPART()->isEnhancedCore < 3) || HY08A_getPART()->isEnhancedCore==5)
                    {
                        fprintf(stderr, "Error!! %s:%d, function Pointer not support 2 (more) byte parameter , please fix it.\n",
							ic->filename,ic->lineno);
                        exit(-3001);
                    }
                    //emitpcode(POC_MVWF, popRegFromIdx(Gstack_base_addr - --psuedoStkPtr));
					PUSHW;

                }
                firstTimeThruLoop = 0;

                mov2w_op(IC_LEFT(sic), offset);
                offset++;
            }
            freeAsmop(IC_LEFT(sic), NULL, sic, TRUE);
        }
        _G.sendSet = NULL;
	}
	else if (prevPushNoVarPara)
	{
		emitpcode(POC_MVFW, PCOP(&pc_podec2));
	}
    emitpcode2 (POC_MVFF,temp1, popCopyReg (&pc_pclath));

    emitpcode2 (POC_MVFF,temp2,  popCopyReg (&pc_pclatl));

	// 2020 MAY, mark a PCALL in assembly
	PCI(pb->pcTail)->callFPTR = 1; // only MVFF do PCALL, it will not be optimized (I think)

    popReleaseTempReg(temp1);
    popReleaseTempReg(temp2);
    emitpLabel (blbl->key);
	if (sym->type->next->funcAttrs.hasVargs && HY08A_getPART()->isEnhancedCore ==4)
	{
		while (var_param_len)
		{
			if (var_param_len > 32)
			{
				emitpcode2(POC_ADDFSR, popGetLit(2), newpCodeOpLitSign(-32));
				var_param_len -= 32;
			}

			else
			{
				emitpcode2(POC_ADDFSR, popGetLit(2), newpCodeOpLitSign(-var_param_len));
				var_param_len = 0;
			}
		}
	}


    freeAsmop (IC_LEFT (ic), NULL, ic, TRUE);

    /* if we need to assign a result value */
    if ((IS_ITEMP (IC_RESULT (ic)) &&
            (OP_SYMBOL (IC_RESULT (ic))->nRegs || OP_SYMBOL (IC_RESULT (ic))->spildir)) || IS_TRUE_SYMOP (IC_RESULT (ic)))
    {

        _G.accInUse++;
        aopOp (IC_RESULT (ic), ic, FALSE);
        _G.accInUse--;

        GpsuedoStkPtr = 0;

        assignResultValue (IC_RESULT (ic),NULL);

        freeAsmop (IC_RESULT (ic), NULL, ic, TRUE);
    }

    /* if register bank was saved then unsave them */
	
    if (currFunc && currFunc->type && dtype && (FUNC_REGBANK (currFunc->type) != FUNC_REGBANK (dtype)))
        unsaverbank (FUNC_REGBANK (dtype), ic, TRUE);

    /* if we hade saved some registers then
       unsave them */
    if (ic->regsSaved)
        unsaveRegisters (ic);

}

/*-----------------------------------------------------------------*/
/* resultRemat - result  is rematerializable                       */
/*-----------------------------------------------------------------*/
static int
resultRemat (iCode * ic)
{
    //  DEBUGHY08A_emitcode ("; ***","%s  %d",__FUNCTION__,__LINE__);
    FENTRY;

    if (SKIP_IC (ic) || ic->op == IFX)
        return 0;

    if (IC_RESULT (ic) && IS_ITEMP (IC_RESULT (ic)))
    {
        symbol *sym = OP_SYMBOL (IC_RESULT (ic));
        if ((sym->remat || sym->rematX) && !POINTER_SET (ic))
            return 1;
    }

    return 0;
}

/*-----------------------------------------------------------------*/
/* genFunction - generated code for function entry                 */
/*-----------------------------------------------------------------*/
static void
genFunction (iCode * ic)
{
    symbol *sym;
    //sym_link *ftype;
	pCodeFunction *pf;

    FENTRY;

    DEBUGHY08A_emitcode ("; ***", "%s  %d curr label offset=%dprevious max_key=%d ", __FUNCTION__, __LINE__, labelOffset,
                         max_key);

    labelOffset += (max_key + 4);
    max_key = 0;
    GpsuedoStkPtr = 0;
    _G.nRegsSaved = 0;
	sym = OP_SYMBOL(IC_LEFT(ic));
    /* create the function header */
    HY08A_emitcode (";", "-----------------------------------------");
    HY08A_emitcode (";", " function %s", sym->name);
    HY08A_emitcode (";", "-----------------------------------------");

    /* prevent this symbol from being emitted as 'extern' */
    HY08A_stringInSet (sym->rname, &HY08A_localFunctions, 1);

    HY08A_emitcode ("", "%s:", sym->rname);

	pf = (pCodeFunction*) newpCodeFunction(NULL, sym->rname, !IS_STATIC(sym->etype),sym);
    addpCode2pBlock (pb, (pCode*)pf);


	// If variable length para, the parameter is pushed here

	if (sym->type->funcAttrs.hasVargs)
	{
		if (HY08A_getPART()->isEnhancedCore == 3)// only H08C need push
			PUSHW; // push in callee, not caller!! the number of para!!
		pf->isParaOnStack = 1; // set flag 
		// need copy struct?


		//value *paraVal;
		//symbol *paramSym;
		//char buf[1024];
		//int size, j, idx = 0;
		//int skipFirstParaByte = 1;
		//int isStruct;
		//reg_info *structReg;
		//for (paraVal = sym->type->funcAttrs.args; paraVal; paraVal = paraVal->next)
		//{
		//	paramSym = paraVal->sym;
		//	isStruct = IS_STRUCT(paraVal->sym->type);
		//	size = getSize(paramSym->type);
		//	structReg = NULL;
		//	if (isStruct)
		//	{
		//		if (strlen(paramSym->rname) == 0)
		//		{
		//			snprintf(paramSym->rname, sizeof(paramSym->rname) - 1, "_%s_%d_", paramSym->name, paramSym->lineDef);// it must be local
		//		}
		//		structReg = regFindWithName(paramSym->rname);
		//		if (!structReg)
		//		{
		//			structReg = allocNewDirReg(paramSym->type, paramSym->rname, NULL);

		//		}
		//		structReg->isFuncLocal = 1;
		//		structReg->wasUsed = true;
		//	}
		//	for (j = 0; j < size; j++)
		//	{
		//		if (isStruct)
		//		{
		//			if (!skipFirstParaByte)
		//			{
		//				pCodeOp *paraop;
		//				SNPRINTF(buf, 1023, "%s_STK%02d", sym->rname, idx++);

		//				paraop = newpCodeOpRegFromStr(buf);
		//				if (HY08A_getPART()->isEnhancedCore == 4)
		//				{
		//					if (strlen(paramSym->rname) == 0)
		//					{
		//						snprintf(paramSym->rname, sizeof(paramSym->rname) - 1, "_%s_%d_", paramSym->name, paramSym->lineDef);// it must be local
		//						
		//					}
		//					emitpcode2(POC_MVSS, paraop, newpCodeOpImmd(paramSym->rname, 0, size - 1 - j, 0, 0, 0, size, NULL));
		//				}
		//				PCOR(paraop)->r->isFuncLocal = 1;
		//				if (!isinSet(pb->tregisters, PCOR(paraop)->r))
		//					addSet(&pb->tregisters, PCOR(paraop)->r);
		//				PCOR(paraop)->r->wasUsed = 1;
		//				if (!IS_STATIC(sym->etype)) // if this call is not static
		//					PCOR(paraop)->r->isPublic = 1;

		//			}
		//			else
		//			{
		//				emitpcode(POC_MVWF, newpCodeOpImmd(paramSym->rname, 0, size - 1 - j, 0, 0, 0, size, NULL));
		//			}

		//			//paramSym->regs[j] = regFindWithName(buf);

		//		}
		//		else
		//		{
		//			if (!skipFirstParaByte)
		//				idx++;
		//		}


		//		if (skipFirstParaByte)
		//		{
		//			skipFirstParaByte = 0; // first is by W!!
		//			//continue;
		//		}
		//	}


		//} // 2022 struct passed need copy 
	}
	else //if (!(0))
	{
		// change the para's register name
		value *paraVal;
		symbol *paramSym;
		char buf[1024];
		int size, j, idx = 0;
		int skipFirstParaByte = 1;
        int isStruct;
        reg_info *structReg;
		for (paraVal = sym->type->funcAttrs.args; paraVal; paraVal = paraVal->next)
		{
			paramSym = paraVal->sym;
            isStruct = IS_STRUCT(paraVal->sym->type);
			size = getSize(paramSym->type);
            structReg=NULL;
            if(isStruct )
            {
                structReg = regFindWithName(paramSym->rname);
                if(!structReg)
                {
                    structReg=allocNewDirReg(paramSym->type, paramSym->rname, NULL);
                    
                }
                structReg->isFuncLocal=1;            
                structReg->wasUsed=true;
            }
			for (j = 0; j < size; j++)
			{
                if(isStruct)
                {
                    if(!skipFirstParaByte)
                    {
                        pCodeOp *paraop ;
				        SNPRINTF(buf, 1023, "%s_STK%02d", sym->rname, idx++);

                        paraop = newpCodeOpRegFromStr(buf);
                        if(HY08A_getPART()->isEnhancedCore ==4)
                            emitpcode2(POC_MVSS, paraop, newpCodeOpImmd(paramSym->rname, 0, size-1-j, 0, 0, 0, size, NULL));
                        else
                            emitpcode2(POC_MVFF, paraop, newpCodeOpImmd(paramSym->rname, 0, size-1-j, 0, 0, 0, size, NULL));
                        PCOR(paraop)->r->isFuncLocal = 1;
                        if (!isinSet(pb->tregisters, PCOR(paraop)->r))
			                addSet(&pb->tregisters, PCOR(paraop)->r);
                         PCOR(paraop)->r->wasUsed=1; 
                         if(!IS_STATIC(sym->etype)) // if this call is not static
                           PCOR(paraop)->r->isPublic=1;
                        
                    }
                    else 
                    {
                        emitpcode(POC_MVWF, newpCodeOpImmd(paramSym->rname, 0, size-1-j, 0, 0, 0, size, NULL));
                    }

				    //paramSym->regs[j] = regFindWithName(buf);

                }
                else 
                {
                    if(!skipFirstParaByte)
                        idx++;
                }


				if (skipFirstParaByte)
				{            
					skipFirstParaByte = 0; // first is by W!!
					//continue;
				}
			}
            
            

		}
	}
    /* mark symbol as NOT extern (even if it was declared so previously) */
    assert (IS_SPEC (sym->etype));
    SPEC_EXTR (sym->etype) = 0;
    sym->cdef = 0;
    if (!SPEC_OCLS (sym->etype))
        SPEC_OCLS (sym->etype) = code;
    addSetIfnotP (&SPEC_OCLS (sym->etype)->syms, sym);

    //ftype = operandType (IC_LEFT (ic));

    /* here we need to generate the equates for the
       register bank if required */
#if 0
    if (FUNC_REGBANK (ftype) != rbank)
    {
        int i;

        rbank = FUNC_REGBANK (ftype);
        for (i = 0; i < HY08A_nRegs; i++)
        {
            if (strcmp (regsHY08A[i].base, "0") == 0)
                HY08A_emitcode ("", "%s = 0x%02x", regsHY08A[i].dname, 8 * rbank + regsHY08A[i].offset);
            else
                HY08A_emitcode ("", "%s = %s + 0x%02x", regsHY08A[i].dname, regsHY08A[i].base, 8 * rbank + regsHY08A[i].offset);
        }
    }
#endif

    /* if this is an interrupt service routine */
    HY08A_inISR = 0;
    if (IFFUNC_ISISR (sym->type))
    {
        HY08A_inISR = 1;

        

        pBlockConvert2ISR (pb);
        HY08A_hasInterrupt = 1;
    }
    else
    {
        /* if callee-save to be used for this function
           then save the registers being used in this function */
        if (IFFUNC_CALLEESAVES (sym->type))
        {
            int i;

            /* if any registers used */
            if (sym->regsUsed)
            {
                /* save the registers used */
                for (i = 0; i < sym->regsUsed->size; i++)
                {
                    if (bitVectBitValue (sym->regsUsed, i))
                    {
                        //HY08A_emitcode("push","%s",HY08A_regWithIdx(i)->dname);
                        _G.nRegsSaved++;
                    }
                }
            }
        }
    }

    /* if critical function then turn interrupts off */
    //if (IFFUNC_ISCRITICAL (ftype))
    //{
    //    genCritical (NULL);
    //    if (IFFUNC_ARGS (sym->type))
    //    {
    //        fprintf (stderr, "Warning, functions with __critical (%s) has effect only after parameters passed.\n", sym->name);
    //        //exit (1);
    //    }                       // if
    //}                           // if
	// change to caller side

    /* set the register bank to the desired value */
    if (FUNC_REGBANK (sym->type) || FUNC_ISISR (sym->type))
    {
        HY08A_emitcode ("push", "psw");
        HY08A_emitcode ("mov", "psw,#0x%02x", (FUNC_REGBANK (sym->type) << 3) & 0x00ff);
    }

    if (IFFUNC_ISREENT (sym->type) || options.stackAuto)
    {

        if (options.useXstack)
        {
            HY08A_emitcode ("mov", "r0,%s", spname);
            HY08A_emitcode ("mov", "a,_bp");
            HY08A_emitcode ("movx", "@r0,a");
            HY08A_emitcode ("inc", "%s", spname);
        }
        else
        {
            /* set up the stack */
            HY08A_emitcode ("push", "_bp");       /* save the callers stack  */
        }
        HY08A_emitcode ("mov", "_bp,%s", spname);
    }

    /* adjust the stack for the function */
    if (sym->stack)
    {

        int i = sym->stack;
        if (i > 127)// we have only 127 bytes of stack!!
            werror (W_STACK_OVERFLOW, sym->name);

        //if (i > 3 && sym->recvSize < 4)
        //{

        //    HY08A_emitcode ("mov", "a,sp");
        //    HY08A_emitcode ("add", "a,#0x%02x", ((char) sym->stack & 0xff));
        //    HY08A_emitcode ("mov", "sp,a");

        //}
        //else
        //    while (i--)
        //        HY08A_emitcode ("inc", "sp");
    }

    /*if (sym->xstack)
    {

        HY08A_emitcode ("mov", "a,_spx");
        HY08A_emitcode ("add", "a,#0x%02x", ((char) sym->xstack & 0xff));
        HY08A_emitcode ("mov", "_spx,a");
    }*/

}

/*-----------------------------------------------------------------*/
/* genEndFunction - generates epilogue for functions               */
/*-----------------------------------------------------------------*/
static void
genEndFunction (iCode * ic)
{
    symbol *sym = OP_SYMBOL (IC_LEFT (ic));

    FENTRY;

    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);

    if (IFFUNC_ISREENT (sym->type) || options.stackAuto)
    {
        HY08A_emitcode ("mov", "%s,_bp", spname);
    }

    /* if use external stack but some variables were
       added to the local stack then decrement the
       local stack */
    if (options.useXstack && sym->stack)
    {
        HY08A_emitcode ("mov", "a,sp");
        HY08A_emitcode ("add", "a,#0x%02x", ((char) - sym->stack) & 0xff);
        HY08A_emitcode ("mov", "sp,a");
    }


    if ((IFFUNC_ISREENT (sym->type) || options.stackAuto))
    {
        if (options.useXstack)
        {
            HY08A_emitcode ("mov", "r0,%s", spname);
            HY08A_emitcode ("movx", "a,@r0");
            HY08A_emitcode ("mov", "_bp,a");
            HY08A_emitcode ("dec", "%s", spname);
        }
        else
        {
            HY08A_emitcode ("pop", "_bp");
        }
    }

    /* restore the register bank    */
    if (FUNC_REGBANK (sym->type) || FUNC_ISISR (sym->type))
        HY08A_emitcode ("pop", "psw");

    {

		

        if (IFFUNC_CALLEESAVES (sym->type))
        {
            int i;

            /* if any registers used */
            if (sym->regsUsed)
            {
                /* save the registers used */
                for (i = sym->regsUsed->size; i >= 0; i--)
                {
                    if (bitVectBitValue (sym->regsUsed, i))
                    {
                        HY08A_emitcode ("pop", "junk");   //"%s",HY08A_regWithIdx(i)->dname);
                    }
                }
            }
        }
		if (sym->type->funcAttrs.hasVargs && HY08A_getPART()->isEnhancedCore==3) // like printf .... only in P41!!
		{
			emitpcode(POC_MVWF, get_argument_pcop(4)); // this will be saved by 
			emitpcode(POC_MVFW, popGetWithString("_INDF2", 0));
			emitpcode(POC_ADDWF, PCOP(&pc_fsr2l));
			emitpcode(POC_MVFW, get_argument_pcop(4));
		}

        /* if debug then send end of function */
        if (options.debug && debugFile && currFunc)
        {
			debugFile->writeEndFunction (currFunc, ic, 1);
        }

        HY08A_emitcode ("return", "");
		if (IFFUNC_ISISR(sym->type))
			emitpcodeNULLop(POC_RETI);
		else
		{
			if( IS_VOID(sym->type->next) && sym->type->next->next==NULL)
				emitpcodeNULLop(POC_RETV);
			else
				emitpcodeNULLop(POC_RET);
		}
        /* Mark the end of a function */
        addpCode2pBlock (pb, newpCodeFunction (NULL, NULL, 0,NULL)); // NULL means END
    }

}

/*-----------------------------------------------------------------*/
/* genRet - generate code for return statement                     */
/*-----------------------------------------------------------------*/
static void
genRet (iCode * ic, iCode *funcICode)
{
    int size, offset = 0;

    FENTRY;

    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);
    /* if we have no return value then
       just generate the "ret" */
    if (!IC_LEFT (ic))
        goto jumpret;

    /* we have something to return then
       move the return value into place */
    aopOp (IC_LEFT (ic), ic, FALSE);
    size = AOP_SIZE (IC_LEFT (ic));
    
    // special case , return value is _Bool
    if(funcICode &&  IS_SYMOP(IC_LEFT(funcICode)) &&  IS_BOOL(OP_SYM_TYPE(IC_LEFT(funcICode))->next))
    {
        if(IC_LEFT(ic)->isLiteral)
        {
            unsigned long long lit = ullFromVal (AOP (IC_LEFT(ic))->aopu.aop_lit);
            if(lit)
            {
                emitSETC;
            }else
            {
                emitCLRC;
            }
        }
        else
        {
            pass_argument(IC_LEFT(ic),0, 0);
            // change to C
            emitpcode(POC_ADDLW, popGetLit(255));
        }
    }
    else 
    {
        for (offset = 0; offset < size; offset++)
        {
            pass_argument (IC_LEFT (ic), offset, size - 1 - offset);
        }
    }

    freeAsmop (IC_LEFT (ic), NULL, ic, TRUE);

jumpret:
    /* generate a jump to the return label
       if the next is not the return statement */
    if (!(ic->next && ic->next->op == LABEL && IC_LABEL (ic->next) == returnLabel))
    {

        emitpcode (POC_JMP, popGetLabel (returnLabel->key));
    }

}

static pBlock *criticalPb=NULL;
static pCodeOp *criticalTemp=NULL;
int criticalTempRegID = 0x3000;// from 0x3000, this int is global
static void
genCritical (iCode * ic)
{
//    pCodeOp *saved_intcon;
    symbol *templbl;

// (void) ic;

	if (pb != criticalPb)
	{
		char buf[100];
		criticalTemp = (pCodeOp*)Safe_calloc(1, sizeof(pCodeOpReg));
		PCOR(criticalTemp)->pcop.type = PO_GPR_TEMP;

		PCOR(criticalTemp)->r = (reg_info *)Safe_calloc(1, sizeof(reg_info));
		PCOR(criticalTemp)->r->rIdx = criticalTempRegID++;
		snprintf(buf, 99, "r0x%04x", PCOR(criticalTemp)->r->rIdx);
		PCOR(criticalTemp)->r->name = strdup(buf);
		PCOR(criticalTemp)->pcop.name = strdup(buf);
		PCOR(criticalTemp)->rIdx = PCOR(criticalTemp)->r->rIdx;
		PCOR(criticalTemp)->r->isFuncLocal = 1;
		PCOR(criticalTemp)->r->pc_type = PO_GPR_TEMP;

		PCOR(criticalTemp)->r->wasUsed = true;
		criticalPb = pb;
	}


	if (!isinSet(pb->tregisters, PCOR(criticalTemp)->r))
		addSet(&pb->tregisters, PCOR(criticalTemp)->r);

    //saved_intcon = popGetWithString("WSAVE",1);
    //addSetHead (&critical_temps, saved_intcon);
    templbl = newiTempLabel(NULL);
    /* This order saves one BANKSEL back to INTCON. */

	if (HY08A_getPART()->isEnhancedCore == 4)
	{
		emitpcode(POC_MVFW, popCopyReg(&pc_inte0));
		emitpcode(POC_MVWF, PCOP(criticalTemp));
	}
	else
	{
		emitpcode2(POC_MVFF, popCopyReg(&pc_inte0), PCOP(criticalTemp));
	}
    emitpLabel(templbl->key);
    emitpcode (POC_BCF, popCopyGPR2Bit (popCopyReg (&pc_inte0), 7));
    emitpcode(POC_BTSZ, popCopyGPR2Bit(popCopyReg(&pc_inte0), 7));
    emitpcode(POC_JMP, popGetLabel(templbl->key));


    //emitpcode (POC_MVWF, pCodeOpCopy (saved_intcon));
}

static void
genEndCritical (iCode * ic)
{
    //pCodeOp *saved_intcon = NULL;

    //(void) ic;

    //saved_intcon = popGetWithString("WSAVE",1);
    /*  if (!saved_intcon)
        {
          fprintf (stderr, "Critical section left, but none entered -- ignoring for now.\n");
          return;
        }*/                           // if
	
    emitpcode (POC_BTSZ, popCopyGPR2Bit (popCopyReg((pCodeOpReg*)criticalTemp), 7));
    emitpcode (POC_BSF, popCopyGPR2Bit (popCopyReg (&pc_inte0), 7));
    //popReleaseTempReg (saved_intcon);
}

/*-----------------------------------------------------------------*/
/* genLabel - generates a label                                    */
/*-----------------------------------------------------------------*/
static void
genLabel (iCode * ic)
{
    FENTRY;

    /* special case never generate */
    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);
    if (IC_LABEL (ic) == entryLabel)
        return;

    emitpLabel (IC_LABEL (ic)->key);
    HY08A_emitcode ("", "_%05d_DS_:", labelKey2num (IC_LABEL (ic)->key + labelOffset));
}

/*-----------------------------------------------------------------*/
/* genGoto - generates a goto                                      */
/*-----------------------------------------------------------------*/
//tsd
static void
genGoto (iCode * ic)
{
    FENTRY;

    emitpcode (POC_JMP, popGetLabel (IC_LABEL (ic)->key));
    HY08A_emitcode ("goto", "_%05d_DS_", labelKey2num (IC_LABEL (ic)->key + labelOffset));
}


/*-----------------------------------------------------------------*/
/* genMultbits :- multiplication of bits                           */
/*-----------------------------------------------------------------*/
static void
genMultbits (operand * left, operand * right, operand * result)
{
    FENTRY;
    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);

    if (!HY08A_sameRegs (AOP (result), AOP (right)))
        emitpcode (POC_BSF, popGet (AOP (result), 0));

    emitpcode (POC_BTSZ, popGet (AOP (right), 0)); // both is bit
    emitpcode (POC_BTSS, popGet (AOP (left), 0));
    emitpcode (POC_BCF, popGet (AOP (result), 0));

}


/*-----------------------------------------------------------------*/
/* genMultOneByte : 8 bit multiplication & division                */
/*-----------------------------------------------------------------*/
static void
genMultOneByte (operand * left, operand * right, operand * result)
{
    char *func[] = { NULL, "__mulchar", "__mulint", NULL, "__mullong" };

    // symbol *lbl ;
    int size, offset, i;


    FENTRY;


    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);
    DEBUGHY08A_AopType (__LINE__, left, right, result);
    DEBUGHY08A_AopTypeSign (__LINE__, left, right, result);

    /* (if two literals, the value is computed before) */
    /* if one literal, literal on the right */
    if (AOP_TYPE (left) == AOP_LIT)
    {
        operand *t = right;
        right = left;
        left = t;
    }

    // special case

    assert (AOP_SIZE (left) == AOP_SIZE (right));

    size = min (AOP_SIZE (result), AOP_SIZE (left));
    offset = Gstack_base_addr - (2 * size - 1);
    assert(size > 0 && size <= 4);
    /* pass right operand as argument */
    for (i = 0; i < size; i++)
    {
        mov2w (AOP (right), i);
		emitpcode(POC_MVWF,
			popRegFromIdx2(Gstack_base_addr - 0, func[size], 1, 0)); // popRegFromIdx (++offset));
    }                           // for

    /* pass left operand as argument */
    for (i = 0; i < size; i++)
    {
        mov2w (AOP (left), i);
        if (i != size - 1)
            emitpcode (POC_MVWF, popRegFromIdx (++offset));
    }                           // for
    //assert (offset == Gstack_base_addr);

    /* call library routine */

    call_libraryfunc (func[size]);
    /* assign result */
	if (AOP_SIZE(result) >= 2)
		movwf (AOP (result), 1);
     emitpcode (POC_MVFW, popRegFromIdx (Gstack_base_addr));
     movwf (AOP (result), 0);

	if (AOP_SIZE(result) >= 2) // though ANSI C is 8*8=8, but we ... give it an option
	{
		// depends on trueOP?
		//HYA_device * dev = HY08A_getPART();

		if ((SPEC_USIGN(operandType(left)) && !IS_VALOP(left)) || (SPEC_USIGN(operandType(right)) && !IS_VALOP(right))) // either unsigned shall count!!
		{
		}
		else
			addSign(result, 1, 1);
	}
	if (AOP_SIZE(result) > 2 && SPEC_USIGN(operandType(result)))
	{
		int i;
		//emitpcodeNullop(POC_CLRW, popGet(AOP(result));
		for (i = 2; i < AOP_SIZE(result); i++)
			emitpcode(POC_CLRF, popGet(AOP(result), i));
	}
}
static inline bool OPrangeLimited(operand *op, int lowid, int highid)
{
    if (!IS_SYMOP(op))
		return false;
    if(OP_SYMBOL(op)->liveFrom < lowid || OP_SYMBOL(op)->liveTo > highid)
        return false;
    return true;
}

/*-----------------------------------------------------------------*/
/* genMult - generates code for multiplication                     */
/*-----------------------------------------------------------------*/
static void
genGenericShift(iCode * ic, int shiftRight);
static void
genMult (iCode * ic)
{
	operand *left; 
	operand *right;
	operand *result; 

    FENTRY;

	if (IC_LEFT(ic)->type == VALUE) {
		operand *t = IC_RIGHT(ic);
		IC_RIGHT(ic) = IC_LEFT(ic);
		IC_LEFT(ic) = t;
	}
	 left = IC_LEFT(ic);
	 right = IC_RIGHT(ic);
	 result = IC_RESULT(ic);
    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);
    /* assign the amsops */
    aopOp (left, ic, FALSE);
    aopOp (right, ic, FALSE);
    aopOp (result, ic, TRUE);

    DEBUGHY08A_AopType (__LINE__, left, right, result);

    /* special cases first */
    /* both are bits */
	if (AOP_TYPE(left) == AOP_LIT && AOP_TYPE(right)==AOP_LIT)
    {
        unsigned long long leftv = operandLitValueUll(left);
        unsigned long long rightv = operandLitValueUll(right);
        unsigned long long resultv = leftv*rightv;
        int size = AOP_SIZE(result);
        int i;
        for(i=0;i<size;i++)
        {
            emitpcode(POC_MVL, popGetLit((resultv>>(i*8))&0xff));
            emitpcode(POC_MVWF, popGet(AOP(result),i));
        }
        goto release;
    }

    if (AOP_TYPE (left) == AOP_CRY && AOP_TYPE (right) == AOP_CRY)
    {
        genMultbits (left, right, result);
        goto release;
    }
	
    if (getSize(OP_SYMBOL(left)->type) != 1 && AOP_SIZE(right)<=2)
    {
        if (AOP(right)->type == AOP_LIT && AOP(right)->aopu.aop_lit->type->select.s.const_val.v_int == 2)
        {
            AOP(right)->aopu.aop_lit->type->select.s.const_val.v_int = 1;
            genGenericShift(ic, 0);
            goto release;
        }
        if (AOP(right)->type == AOP_LIT && AOP(right)->aopu.aop_lit->type->select.s.const_val.v_int == 4)
        {
            AOP(right)->aopu.aop_lit->type->select.s.const_val.v_int = 2;
            genGenericShift(ic, 0);
            goto release;
        }
    }

    /* if both are of size == 1 */
	// for literal, we check 0~255
    if ((AOP_SIZE (left) == 1 && AOP_SIZE (right) == 1) || (AOP_SIZE(left) == 1 && AOP(right)->type == AOP_LIT &&
		AOP_SIZE(right)<=2 && 	AOP(right)->aopu.aop_lit->type->select.s.const_val.v_int < 256 &&
		AOP(right)->aopu.aop_lit->type->select.s.const_val.v_int >= 0))

    {
        // try optimize next op if next is '+'

        if (TARGET_IS_HY08A && HY08A_getPART()->isEnhancedCore!=5)
        {
            mov2w(AOP(left), 0);
            if (aop_isLitLike(AOP(right)))
            {
                emitpcode(POC_MULLW, popGet(AOP(right), 0));
            }
            else
            {
                emitpcode(POC_MULWF, popGet(AOP(right), 0));
            }
            bool notSignedLeft = IS_VALOP(left) ? ullFromVal(OP_VALUE(left))>=0 : SPEC_USIGN(operandType(left));
            bool notSignedRight = IS_VALOP(right) ? ullFromVal(OP_VALUE(right))>=0 : SPEC_USIGN(operandType(right));
            if(ic->next->op=='+' && !IS_TRUE_SYMOP(IC_RESULT(ic)) && 
            IS_SYMOP(IC_RIGHT(ic->next)) &&
            notSignedLeft && notSignedRight && ((
            isOperandEqual(IC_RESULT(ic),IC_RIGHT(ic->next)) &&
            OPrangeLimited(IC_RESULT(ic),ic->seq,ic->next->seq) ) ||
				(isOperandEqual(IC_RESULT(ic), IC_LEFT(ic->next)) &&
					OPrangeLimited(IC_RESULT(ic), ic->seq, ic->next->seq)))

            )
            {
                // mov2w(AOP(left), 0);
                // if (aop_isLitLike(AOP(right)))
                // {
                //     emitpcode(POC_MULLW, popGet(AOP(right), 0));
                // }
                // else
                // {
                //     emitpcode(POC_MULWF, popGet(AOP(right), 0));
                // }
                aopOp(IC_RIGHT(ic->next), ic->next, false);
                OP_SYMBOL( IC_RIGHT(ic->next))->regs[0]=pc_prodl.r;
                OP_SYMBOL( IC_RIGHT(ic->next))->regs[1]=pc_prodh.r;
                goto release;

            } 
        

            //mov2w(popCopyReg(&pc_prodl),0);
            emitpcode(POC_MVFW, popCopyReg(&pc_prodl)); // change to MVFF in the future
            movwf(AOP(result), 0);
            if (AOP_SIZE(result) >= 2) // though ANSI C is 8*8=8, but we ... give it an option
            {
				// depends on trueOP?
				//HYA_device * dev = HY08A_getPART();

				if ( (SPEC_USIGN(operandType(left))&&!IS_VALOP(left)) || (SPEC_USIGN(operandType(right))&&!IS_VALOP(right))) // either unsigned shall count!!
				{
						emitpcode(POC_MVFW, PCOP(&pc_prodh));
						emitpcode(POC_MVWF, popGet2(AOP(result), 1, 1));
				}
				else
					addSign(result, 1, 1);
            }
            if (AOP_SIZE(result) > 2 && SPEC_USIGN(operandType(result)))
            {
                int i;
                //emitpcodeNullop(POC_CLRW, popGet(AOP(result));
                for (i = 2; i < AOP_SIZE(result); i++)
                    emitpcode(POC_CLRF, popGet(AOP(result), i));
            }

            goto release;
        }
        else
        {

            genMultOneByte(left, right, result);
            goto release;
        }
    }

    /* should have been converted to function call */
    assert (0);

release:
    freeAsmop (left, NULL, ic, (RESULTONSTACK (ic) ? FALSE : TRUE));
    freeAsmop (right, NULL, ic, (RESULTONSTACK (ic) ? FALSE : TRUE));
    freeAsmop (result, NULL, ic, TRUE);
}

/*-----------------------------------------------------------------*/
/* genDivbits :- division of bits                                  */
/*-----------------------------------------------------------------*/
//static void
//genDivbits (operand * left, operand * right, operand * result)
//{
//
//    char *l;
//
//    FENTRY;
//
//    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);
//    /* the result must be bit */
//    HY08A_emitcode ("mov", "b,%s", aopGet (AOP (right), 0, FALSE, FALSE));
//    l = aopGet (AOP (left), 0, FALSE, FALSE);
//
//    MOVA (l);
//
//    HY08A_emitcode ("div", "ab");
//    HY08A_emitcode ("rrc", "a");
//    aopPut (AOP (result), "c", 0);
//}

/*-----------------------------------------------------------------*/
/* genDivOneByte : 8 bit division                                  */
/*-----------------------------------------------------------------*/
static void
genDivOneByte (operand * left, operand * right, operand * result)
{
    int sign;

    FENTRY;
    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);

    assert (AOP_SIZE (right) == 1);
    assert (AOP_SIZE (left) == 1);

    sign = !(SPEC_USIGN (operandType (left)) && SPEC_USIGN (operandType (right)));

    if (AOP_TYPE (right) == AOP_LIT)
    {
        /* XXX: might add specialized code */
    }

    if (!sign)
    {
        /* unsigned division */
#if 1
        mov2w (AOP (right), 0);
        if((0))
            emitpcode (POC_MVWF, popRegFromIdx (Gstack_base_addr));
        else
            emitpcode(POC_MVWF, popRegFromIdx2(Gstack_base_addr,"__divuchar", 1, 0));
        mov2w (AOP (left), 0);
        call_libraryfunc ("__divuchar");
        movwf (AOP (result), 0);
#else
        pCodeOp *temp;
        symbol *lbl;

        temp = popGetTempReg ();
        lbl = newiTempLabel (NULL);

        /* XXX: improve this naive approach:
           [result] = [a] / [b]
           ::= [result] = 0; while ([a] > [b]) do [a] -= [b]; [result]++ done

           In  assembler:
           movf  left,W
           movwf temp       // temp <-- left
           movf  right,W    // W <-- right
           clrf  result
           label1:
           incf  result
           subwf temp,F     // temp <-- temp - W
           skipNC       // subwf clears CARRY (i.e. sets BORROW) if temp < W
           goto  label1
           decf result      // we just subtract once too often
         */

        /* XXX: This loops endlessly on DIVIDE BY ZERO */
        /* We need 1..128 iterations of the loop body (`4 / 5' .. `255 / 2'). */

        mov2w (AOP (left), 0);
        emitpcode (POC_MVWF, temp);
        mov2w (AOP (right), 0);
        emitpcode (POC_CLRF, popGet (AOP (result), 0));

        emitpLabel (lbl->key);
        emitpcode (POC_INF, popGet (AOP (result), 0));
        emitpcode (POC_SUBF, temp);
        emitSKPNC;
        emitpcode (POC_JMP, popGetLabel (lbl->key));
        emitpcode (POC_DECF, popGet (AOP (result), 0));
        popReleaseTempReg (temp);
#endif
    }
    else
    {
        /* signed division */
        mov2w (AOP (right), 0);
        if ((0))
            emitpcode(POC_MVWF, popRegFromIdx(Gstack_base_addr));
        else
            emitpcode(POC_MVWF, popRegFromIdx2(Gstack_base_addr, "__divschar", 1, 0));
        mov2w (AOP (left), 0);
        call_libraryfunc ("__divschar");
        movwf (AOP (result), 0);
    }

    /* now performed the signed/unsigned division -- extend result */
    addSign (result, 1, sign);
}

/*-----------------------------------------------------------------*/
/* genDiv - generates code for division                */
/*-----------------------------------------------------------------*/
static void
genDiv (iCode * ic)
{
    operand *left = IC_LEFT (ic);
    operand *right = IC_RIGHT (ic);
    operand *result = IC_RESULT (ic);

    FENTRY;
    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);
    /* assign the amsops */
    aopOp (left, ic, FALSE);
    aopOp (right, ic, FALSE);
    aopOp (result, ic, TRUE);

    /* special cases first */
    /* both are bits */
    if (AOP_TYPE (left) == AOP_CRY && AOP_TYPE (right) == AOP_CRY)
    {
		fprintf(stderr, "bit div not support at %s:%d\n", ic->filename, ic->lineno);
		exit(-1000);
        //genDivbits (left, right, result);
        goto release;
    }

    /* if both are of size == 1 */
    if (AOP_SIZE (left) == 1 && AOP_SIZE (right) == 1)
    {
        genDivOneByte (left, right, result);
        goto release;
    }

    /* should have been converted to function call */
    assert (0);
release:
    freeAsmop (left, NULL, ic, (RESULTONSTACK (ic) ? FALSE : TRUE));
    freeAsmop (right, NULL, ic, (RESULTONSTACK (ic) ? FALSE : TRUE));
    freeAsmop (result, NULL, ic, TRUE);
}

/*-----------------------------------------------------------------*/
/* genModOneByte : 8 bit modulus                                   */
/*-----------------------------------------------------------------*/
static void
genModOneByte (operand * left, operand * right, operand * result)
{
    int sign;

    FENTRY;
    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);

    assert (AOP_SIZE (right) == 1);
    assert (AOP_SIZE (left) == 1);

    sign = !(SPEC_USIGN (operandType (left)) && SPEC_USIGN (operandType (right)));

    if (AOP_TYPE (right) == AOP_LIT)
    {
        /* XXX: might add specialized code */
    }

    if (!sign)
    {
        /* unsigned division */
#if 1
        mov2w (AOP (right), 0);
        if ((0))
            emitpcode(POC_MVWF, popRegFromIdx(Gstack_base_addr));
        else
            emitpcode(POC_MVWF, popRegFromIdx2(Gstack_base_addr, "__moduchar", 1, 0));
        mov2w (AOP (left), 0);
        call_libraryfunc ("__moduchar");
        movwf (AOP (result), 0);
#else
        pCodeOp *temp;
        symbol *lbl;

        lbl = newiTempLabel (NULL);

        assert (!HY08A_sameRegs (AOP (right), AOP (result)));

        /* XXX: improve this naive approach:
           [result] = [a] % [b]
           ::= [result] = [a]; while ([result] > [b]) do [result] -= [b]; done

           In  assembler:
           movf  left,W
           movwf result     // result <-- left
           movf  right,W    // W <-- right
           label1:
           subwf result,F   // result <-- result - W
           skipNC       // subwf clears CARRY (i.e. sets BORROW) if result < W
           goto  label1
           addwf result, F  // we just subtract once too often
         */

        /* XXX: This loops endlessly on DIVIDE BY ZERO */
        /* We need 1..128 iterations of the loop body (`4 % 5' .. `255 % 2'). */

        if (!HY08A_sameRegs (AOP (left), AOP (result)))
        {
            mov2w (AOP (left), 0);
            emitpcode (POC_MVWF, popGet (AOP (result), 0));
        }
        mov2w (AOP (right), 0);

        emitpLabel (lbl->key);
        emitpcode (POC_SUBF, popGet (AOP (result), 0));
        emitSKPNC;
        emitpcode (POC_JMP, popGetLabel (lbl->key));
        emitpcode (POC_ADDWF, popGet (AOP (result), 0));
#endif
    }
    else
    {
        /* signed division */
        mov2w (AOP (right), 0);
        if ((0))
            emitpcode(POC_MVWF, popRegFromIdx(Gstack_base_addr));
        else
            emitpcode(POC_MVWF, popRegFromIdx2(Gstack_base_addr, "__modschar", 1, 0));
        mov2w (AOP (left), 0);
        call_libraryfunc ("__modschar");
        movwf (AOP (result), 0);
    }

    /* now we performed the signed/unsigned modulus -- extend result */
    addSign (result, 1, sign);
}

/*-----------------------------------------------------------------*/
/* genMod - generates code for division                            */
/*-----------------------------------------------------------------*/
static void
genMod (iCode * ic)
{
    operand *left = IC_LEFT (ic);
    operand *right = IC_RIGHT (ic);
    operand *result = IC_RESULT (ic);

    FENTRY;
    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);
    /* assign the amsops */
    aopOp (left, ic, FALSE);
    aopOp (right, ic, FALSE);
    aopOp (result, ic, TRUE);

    /* if both are of size == 1 */
    if (AOP_SIZE (left) == 1 && AOP_SIZE (right) == 1)
    {
        genModOneByte (left, right, result);
        goto release;
    }

    /* should have been converted to function call */
    assert (0);

release:
    freeAsmop (left, NULL, ic, (RESULTONSTACK (ic) ? FALSE : TRUE));
    freeAsmop (right, NULL, ic, (RESULTONSTACK (ic) ? FALSE : TRUE));
    freeAsmop (result, NULL, ic, TRUE);
}

/*-----------------------------------------------------------------*/
/* genIfxJump :- will create a jump depending on the ifx           */
/*-----------------------------------------------------------------*/
/*
note: May need to add parameter to indicate when a variable is in bit space.
*/
static void
genIfxJump (iCode * ic, char *jval)
{

    FENTRY;
    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);
    /* if true label then we jump if condition
       supplied is true */
    if (IC_TRUE (ic))
    {

		if (toupper(jval[0]) == 'A')
			emitSKPNZ;
		else if (toupper(jval[0]) == 'C')
			emitSKPNC;
        else
        {
			assert(!"ifxjmp ival fail.");
            DEBUGHY08A_emitcode ("; ***", "%d - assuming %s is in bit space", __LINE__, jval);
            emitpcode (POC_BTSZ, newpCodeOpBit (jval, -1, 0));
        }

        emitpcode (POC_JMP, popGetLabel (IC_TRUE (ic)->key));
        HY08A_emitcode (" goto", "_%05d_DS_", labelKey2num (IC_TRUE (ic)->key + labelOffset));

    }
    else
    {
        /* false label is present */
        //if (STRCASECMP(jval, "a") == 0)
		if(toupper(jval[0])=='A')
            emitSKPZ;
        else if (toupper(jval[0])=='C')
            emitSKPC;
        else
        {
			assert(!"jval error.\n");
            DEBUGHY08A_emitcode ("; ***", "%d - assuming %s is in bit space", __LINE__, jval);
            emitpcode (POC_BTSS, newpCodeOpBit (jval, -1, 0));
        }

        emitpcode (POC_JMP, popGetLabel (IC_FALSE (ic)->key));
        HY08A_emitcode (" goto", "_%05d_DS_", labelKey2num (IC_FALSE (ic)->key + labelOffset));

    }


    /* mark the icode as generated */
    ic->generated = 1;
}

/*-----------------------------------------------------------------*/
/* genSkipc                                                        */
/*-----------------------------------------------------------------*/
static void
genSkipc (resolvedIfx * rifx)
{
    FENTRY;
    if (!rifx)
        return;

    if (rifx->condition)
        emitSKPNC;
    else
        emitSKPC;

    emitpcode (POC_JMP, popGetLabel (rifx->lbl->key));
    emitpComment ("%s:%u: created from rifx:%p", __FUNCTION__, __LINE__, rifx);
    rifx->generated = 1;
}

#define isAOP_REGlike(x)  (AOP_TYPE(x) == AOP_REG || AOP_TYPE(x) == AOP_DIR || AOP_TYPE(x) == AOP_PCODE || AOP_TYPE(x) == AOP_STK)
#define isAOP_LIT(x)      (AOP_TYPE(x) == AOP_LIT)
#define DEBUGpc           emitpComment

/*-----------------------------------------------------------------*/
/* mov2w_regOrLit :- move to WREG either the offset's byte from    */
/*                  aop (if it's NOT a literal) or from lit (if    */
/*                  aop is a literal)                              */
/*-----------------------------------------------------------------*/
static void
HY08A_mov2w_regOrLit (asmop * aop, unsigned long long lit, int offset)
{
    if (aop->type == AOP_LIT)
    {
        emitpcode (POC_MVL, popGetLit ((lit >> (offset * 8)) & 0x00FF));
    }
    else
    {
        emitpcode (POC_MVFW, popGet (aop, offset));
    }
}

/* genCmp performs a left < right comparison, stores
 * the outcome in result (if != NULL) and generates
 * control flow code for the ifx (if != NULL).
 *
 * This version leaves in sequences like
 * "B[CS]F STATUS,0; BTFS[CS] STATUS,0"
 * which should be optmized by the peephole
 * optimizer - RN 2005-01-01 */

static bool litIs2expn(unsigned long long lit, int bytenum)
{
	unsigned long long k;
	unsigned long long mask = (bytenum>=8)?0xffffffffffffffful:((1ULL << (8 * bytenum)) - 1);
	int state = 0; // initial state = 0;
	
	for (k = 1; k; k = (k << 1)&mask)
	{
		
		if (!state)
		{
			if (k&lit)
				state = 1;
		}
		else
		{
			if (k&lit)
				return false;
		}
	}
	return true; // if only 1,2,4,8,0x10,0x20,0x40... 2^n
}

static void
genCmp (operand * left, operand * right, operand * result, iCode * ifx, int sign)
{
    resolvedIfx rIfx;
    int size;
    int offs;
    symbol *templbl;
    operand *dummy;
    unsigned long long lit;
    unsigned long long mask;
    int performedLt;
    int invert_result = 0;

    FENTRY;

    assert (AOP_SIZE (left) == AOP_SIZE (right));
    assert (left && right);

    size = AOP_SIZE (right) - 1;
    mask = (0x100ULL << (size * 8)) - 1;
    // in the end CARRY holds "left < right" (performedLt == 1) or "left >= right" (performedLt == 0)
    performedLt = 1;
    templbl = NULL;
    lit = 0;

    resolveIfx (&rIfx, ifx);

    /**********************************************************************
     * handle bits - bit compares are promoted to int compares seemingly! *
     **********************************************************************/
	// sdcc 3.6.9 add bits comparison here, nolonger promotion at front-end

	if (AOP_TYPE(left) == AOP_CRY && AOP_TYPE(right)==AOP_CRY)
	{
		// if left is bit , we see if right is bit only 0,1 is true
		emitSETC;
		emitpcode(POC_BTSZ, popGet(AOP(left), 0));
		emitCLRC;
		emitpcode(POC_BTSS, popGet(AOP(right), 0));
		emitCLRC;
		goto correct_result_in_carry;

	}
	if (AOP_TYPE(left) == AOP_CRY ) // if only left is bit
	{
		// cases:
		// if right is signed, if left=0, 
		// 
		if (AOP_TYPE(right) == AOP_LIT) // bit compare literal
		{
			lit = ullFromVal(AOP(right)->aopu.aop_lit);
			if (lit <= 0)
			{
				emitCLRC;
				goto correct_result_in_carry;
			}
			else if (lit == 1) // only when 1
			{
				emitSETC;
				emitpcode(POC_BTSZ, popGet(AOP(left), 0));
				emitCLRC;
				goto correct_result_in_carry;
			}
			else
			{
				emitSETC;
				goto correct_result_in_carry;
			}
		} // end of bit comp lit
		if (sign) // if right is signed
		{
			// right should >=1 or >=2
			// which means it will OV if right+0x7fff or 0x7ffe depends on left
			unsigned long long k = (0x80ull << (8 * size)) - 1;
			int ii;
			for (ii = 0; ii <= size; ii++)
			{
				emitpcode(POC_MVL, popGetLit(k & 0xff));
				if (ii == 0)
				{
					emitpcode(POC_BTSZ, popGet(AOP(left), 0));
					emitpcode(POC_ADDLW, popGetLit(255));
					emitpcode(POC_ADDFW, popGet(AOP(right), 0));
				}
				else
					emitpcode(POC_ADCFW, popGet(AOP(right), ii));
			}
			emitCLRC;
			emitpcode(POC_BTSZ, popCopyGPR2Bit(PCOP(&pc_status), HYA_OV_BIT));
			emitSETC;
			goto   result_in_carry;
		}
		else
		{
			// unsigned check C directly
			unsigned long long k = (0x100ull << (8 * size)) - 1;
			int ii;
			for (ii = 0; ii <= size; ii++)
			{
				emitpcode(POC_MVL, popGetLit(k & 0xff));
				if (ii == 0)
				{
					emitpcode(POC_BTSZ, popGet(AOP(left), 0));
					emitpcode(POC_ADDLW, popGetLit(255));
					emitpcode(POC_ADDFW, popGet(AOP(right), 0));
				}
				else
					emitpcode(POC_ADCFW, popGet(AOP(right), ii));
			}
			goto   result_in_carry;
		}
	}
	else if (AOP_TYPE(right) == AOP_CRY)
	{
		// special case, left is not CRY, but right is CRY
		if (AOP_TYPE(left) == AOP_LIT)
		{
			lit = ullFromVal(AOP(left)->aopu.aop_lit);
			if (lit < 0)
			{
				emitSETC;
				goto correct_result_in_carry;
			}
			else if (lit == 0) // only when 0, right 1 is true
			{
				emitCLRC;
				emitpcode(POC_BTSZ, popGet(AOP(right), 0));
				emitSETC;
				goto correct_result_in_carry;
			}
			else
			{
				emitCLRC;
				goto correct_result_in_carry;
			}
		}
		if (sign) // if left is signed
		{
			// right = 0, ==> see msb, left <0 , or left+7fff will not overflow
			// right == 1 ==> left <1, left+7ffe will not overflow
			// which means it will OV if right+0x7fff or 0x7ffe depends on left
			unsigned long long k = (0x80ull << (8 * size)) - 1;
			int ii;
			for (ii = 0; ii <= size; ii++)
			{
				emitpcode(POC_MVL, popGetLit(k & 0xff));
				if (ii == 0)
				{
					emitpcode(POC_BTSZ, popGet(AOP(right), 0));
					emitpcode(POC_ADDLW, popGetLit(255));
					emitpcode(POC_ADDFW, popGet(AOP(left), 0));
				}
				else
					emitpcode(POC_ADCFW, popGet(AOP(left), ii));
			}
			emitSETC;
			emitpcode(POC_BTSZ, popCopyGPR2Bit(PCOP(&pc_status), HYA_OV_BIT));
			emitCLRC;
			goto   result_in_carry;
		}
		else
		{
			// unsigned check C directly
			unsigned long long k = (0x100ull << (8 * size)) - 1;
			int ii;
			for (ii = 0; ii <= size; ii++)
			{
				emitpcode(POC_MVL, popGetLit(k & 0xff));
				if (ii == 0)
				{
					emitpcode(POC_BTSZ, popGet(AOP(right), 0));
					emitpcode(POC_ADDLW, popGetLit(255));
					emitpcode(POC_ADDFW, popGet(AOP(left), 0));
				}
				else
					emitpcode(POC_ADCFW, popGet(AOP(left), ii));
			}
			goto   result_in_carry;
		}
	}


    /*************************************************
     * make sure that left is register (or the like) *
     *************************************************/
    if (!isAOP_REGlike (left))
    {
        DEBUGpc ("swapping arguments (AOP_TYPEs %d/%d)", AOP_TYPE (left), AOP_TYPE (right));
        assert (isAOP_LIT (left));
        assert (isAOP_REGlike (right));
        // swap left and right
        // left < right <==> right > left <==> (right >= left + 1)
        lit = ullFromVal (AOP (left)->aopu.aop_lit);

        if ((!sign && (lit & mask) == mask) || (sign && (lit & mask) == (mask >> 1)))
        {
            // MAXVALUE < right? always false
            if (performedLt)
                emitCLRC;
            else
                emitSETC;
            goto correct_result_in_carry;
        }                       // if

        // This fails for lit = 0xFF (unsigned) AND lit = 0x7F (signed),
        // that's why we handled it above.
        lit++;

        dummy = left;
        left = right;
        right = dummy;

        performedLt ^= 1;         // instead of "left < right" we check for "right >= left+1, i.e. "right < left+1"
    }
    else if (isAOP_LIT (right))
    {
        lit = ullFromVal (AOP (right)->aopu.aop_lit);
    }                           // if

    assert (isAOP_REGlike (left));        // left must be register or the like
    assert (isAOP_REGlike (right) || isAOP_LIT (right));  // right may be register-like or a literal

    /*************************************************
     * special cases go here                         *
     *************************************************/

    if (isAOP_LIT (right))
    {
        if (!sign)
        {
			int size1;
            // unsigned comparison to a literal
			size1 = AOP_SIZE(left);
            DEBUGpc ("unsigned compare: left %s lit(0x%X=%lu), size=%d", performedLt ? "<" : ">=", lit, lit, size + 1);
            if (lit == 0)
            {
                // unsigned left < 0? always false
                if (performedLt)
                    emitCLRC;
                else
                    emitSETC;
                goto correct_result_in_carry;
            }
			// 2018 MAY, !!
			// if we compare unsigned char < 0x1 0x2, 0x4, 0x8, 0x10, 0x20..

			// only for size >1 case, becase CPSL/CPSG will be better than ANDL
			if (size1>1 && litIs2expn(lit,size1) && SPEC_USIGN(getSpec(operandType(left))) &&
				SPEC_USIGN(getSpec(operandType(right)))) // strict unsigned !!
			{
				int i,j,or_ed=0;
                if(size1>=8)
                    lit = (lit^0xffffffffffffffffull)+1; 
				else 
                    lit = (1LL << (8 * size1)) - lit;
				for (i = 0; i < size1; i++)
				{
					j = (lit >> (i * 8)) & 0xff;
					if (j == 0)
						continue;
					if (j != 0xff)
					{
						emitpcode(POC_MVL, popGetLit(j));
						emitpcode(POC_ANDFW, popGet(AOP(left), i));
						or_ed = 1;
					}
					else
					{
						if (!or_ed)
						{
							emitpcode(POC_MVL, popGetLit(0));
							or_ed = 1;
						}
						emitpcode(POC_IORFW, popGet(AOP(left), i));
					}
				}
				// z->c
				emitCLRC;
				if (performedLt)
				{
					emitSKPNZ;
				}
				else
				{
					emitSKPZ;
				}
				emitSETC;
				goto correct_result_in_carry;

			}


        }
        else
        {
            // signed comparison to a literal
            DEBUGpc ("signed compare: left %s lit(0x%X=%ld), size=%d, mask=%x", performedLt ? "<" : ">=", lit, lit, size + 1,
                     mask);
            if ((lit & mask) == ((0x80ULL << (size * 8)) & mask))
            {
                // signed left < 0x80000000? always false
                if (performedLt)
                    emitCLRC;
                else
                    emitSETC;
                goto correct_result_in_carry;
            }
            else if (lit == 0) // this is <0 or >=0, sign bit can get the result
            {
                 //compare left < 0; set CARRY if SIGNBIT(left) is set
                if (performedLt)
                    emitSETC;
                else
                    emitCLRC;
                emitpcode (POC_BTSS, newpCodeOpBit (aopGet (AOP (left), size, FALSE, FALSE), 7, 0)); // this will add +xx
                if (performedLt)
                    emitCLRC;
                else
                    emitSETC;
				/*emitpcode(POC_RLCFW, popGet(AOP(left),size));
				if (!performedLt)
				{
					emitTGLC;
				}*/
                goto correct_result_in_carry;
            }
        }                       // if (!sign)
    }                           // right is literal

    /*************************************************
     * perform a general case comparison             *
     * make sure we get CARRY==1 <==> left >= right  *
     *************************************************/
    // compare most significant bytes
    //DEBUGpc ("comparing bytes at offset %d", size);
	// 2016 Dec, change to cpxx
    if (!sign)
    {
        // unsigned comparison
		// need set z and c, so not so easy
		//if (isAOP_LIT(right) && (lit & 0xff) == 0)
		//{
			//emitSETC; // because no bo
		//}
		//else
		int i;
		if (isAOP_LIT(right))
		{
			// just add a neg one
			// to see if Carry
			// for example  if 4<0x10
			long long slit = lit;
			//int i;
			unsigned char ulitc;
			int canSkipZ = 1;
			int firstAdd = 1;
			slit = -slit;
			for (i = 0; i <= size; i++)
			{
				
				ulitc = (slit >> (i*8)) & 0xff;
				if (ulitc == 0 && canSkipZ) // there must be something nonzero, or 
					continue;
				canSkipZ = 0;
				if (firstAdd)
				{
					switch (ulitc)
					{
					case 1:
						emitpcode(POC_INFW, popGet(AOP(left), i));
						break;
					case 255:
						emitpcode(POC_DECFW, popGet(AOP(left), i));
						break;
						// zero is not necessary because skipped
					default:
						emitpcode(POC_MVL, popGetLit(ulitc));
						emitpcode(POC_ADDFW, popGet(AOP(left), i));
						break;
					}
					firstAdd = 0;
				}
				else
				{
					emitpcode(POC_MVL, popGetLit(ulitc));
					emitpcode(POC_ADCFW, popGet(AOP(left), i));
				}
			}
			goto    result_in_carry;
		}
		else
		{
			for (i = 0; i <= size; i++)
			{
				HY08A_mov2w_regOrLit(AOP(right), lit, i);
				if(i==0)
					emitpcode(POC_SUBFWW, popGet(AOP(left), i));
				else
					emitpcode(POC_SBCFWW, popGet(AOP(left), i));
			}
			goto result_in_carry;
		}
    }
    else
    {
        // signed comparison
        // (add 2^n to both operands then perform an unsigned comparison)
        if (isAOP_LIT (right))
        {
            // left >= LIT <-> LIT-left <= 0 <-> LIT-left == 0 OR !(LIT-left >= 0)

			// 2017 consider OV flag
			// if >=1 means add 7fff will get OV
			// if <1 means add 7fff will not get OV



#ifdef SIGN_CMP_USE_OV
			long long slit = lit;
			long long toAdd;
			int pos = 1;
			unsigned char litbyte;
			int firstAdd = 1;
			int canskipZ = 1;
			int i;
			if (slit > 0) // =0 case is solved
			{
				// if >=1 means add 7fff will get OV
				toAdd = (1LL << (8 * (size+1) - 1)) - slit;
			}
			else
			{
				// if <0, for example, -1, add 8000 will get OV
				// if < -256 we add (0x8000-(-256)) will overflow
				toAdd = (1LL << (8 * (size+1) - 1)) - slit ;
				pos = 0;
			}
			for (i = 0; i <= size; i++)
			{
				litbyte = (toAdd >> (8 * i)) & 0xFF;
				if (litbyte == 0 && canskipZ)
					continue;
				canskipZ = 0;
				if (firstAdd)
				{
					switch (litbyte)
					{
					case 1:
						emitpcode(POC_INFW, popGet(AOP(left), i));
						break;
					case 0xff:
						emitpcode(POC_DECFW, popGet(AOP(left), i));
						break;

					default:
						emitpcode(POC_MVL, popGetLit(litbyte));
						emitpcode(POC_ADDFW, popGet(AOP(left), i));
						break;
					}
					firstAdd = 0;
				}
				else
				{
					emitpcode(POC_MVL, popGetLit(litbyte));
					emitpcode(POC_ADCFW, popGet(AOP(left), i));
				}
			}

			
			if ( pos) // consider < pos .. add pos will NOT ovf, < neg add neg will ovf, pos result
				// is inv of ov, neg result is ov
			{
				// we use nov
				emitCLRC;
				emitpcode(POC_BTSZ, popCopyGPR2Bit(PCOP(&pc_status), HYA_OV_BIT));
				emitSETC;
				goto   result_in_carry;
			}
			emitCLRC;
			emitpcode(POC_BTSS, popCopyGPR2Bit(PCOP(&pc_status), HYA_OV_BIT));
			emitSETC;
			goto    result_in_carry;

#else
            unsigned char litbyte = (lit >> (8 * size)) & 0xFF;

            if (litbyte == 0x80)
            {
                // left >= 0x80 -- always true, but more bytes to come
                mov2w (AOP (left), size);
                emitpcode (POC_XORLW, popGetLit (0x80));  // set ZERO flag
                emitSETC;
            }
            else
            {
                // left >= LIT <-> left + (-LIT) >= 0 <-> left + (0x100-LIT) >= 0x100
                mov2w (AOP (left), size);
                emitpcode (POC_ADDLW, popGetLit (0x80));
                emitpcode (POC_ADDLW, popGetLit ((0x100 - (litbyte + 0x80)) & 0x00FF));
            }
			// if
#endif
        }
		else if (size > 0) // for long signed, we take special
		{
			for (offs = 0; offs < size; offs++)
			{
				mov2w(AOP(right), offs);
				if (offs == 0)
					emitpcode(POC_SUBFWW, popGet(AOP(left), offs));
				else
					emitpcode(POC_SBCFWW, popGet(AOP(left), offs));
				

			}
			// final one need xor
			mov2w(AOP(left), offs);
			emitpcode(POC_XORLW, popGetLit(0x80));
			emitpcode(POC_MVWF, get_argument_pcop(1));
			mov2w(AOP(right), offs);
			emitpcode(POC_XORLW, popGetLit(0x80));
			emitpcode(POC_SBCFWW, get_argument_pcop(1));
			goto result_in_carry;

		}else
        {

            pCodeOp *pctemp = get_argument_pcop(1); // it should be able to be used;
            mov2w (AOP (left), size);
            emitpcode (POC_ADDLW, popGetLit (0x80));
            emitpcode (POC_MVWF, pctemp);
            mov2w (AOP (right), size);
            emitpcode (POC_ADDLW, popGetLit (0x80));
            emitpcode (POC_SUBFWW, pctemp);
            //popReleaseTempReg (pctemp);
        }
    }                           // if (!sign)

    // compare remaining bytes (treat as unsigned case from above)
    templbl = newiTempLabel (NULL);
    offs = size;
    while (offs--)
    {
        //DEBUGpc ("comparing bytes at offset %d", offs);
        emitSKPZ;
        emitpcode (POC_JMP, popGetLabel (templbl->key));
        HY08A_mov2w_regOrLit (AOP (right), lit, offs);
        emitpcode (POC_SUBFWW, popGet (AOP (left), offs));
    }                           // while (offs)
    emitpLabel (templbl->key);
    goto result_in_carry;

result_in_carry:

    /****************************************************
     * now CARRY contains the result of the comparison: *
     * SUBWF sets CARRY iff                             *
     * F-W >= 0 <==> F >= W <==> !(F < W)               *
     * (F=left, W=right)                                *
     ****************************************************/

    if (performedLt)
    {
        invert_result = 1;
        // value will be used in the following genSkipc ()
        rIfx.condition ^= 1;
    }                           // if

correct_result_in_carry:

    // assign result to variable (if neccessary), but keep CARRY intact to be used below
    if (result && AOP_TYPE (result) != AOP_CRY && ifx ==NULL) // result valid only when ifx is null
    {
        //DEBUGpc ("assign result");
        size = AOP_SIZE (result);
        while (size--)
        {
            emitpcode (POC_CLRF, popGet (AOP (result), size));
        }                       // while
        if (invert_result)
        {
            emitSKPC;
            emitpcode (POC_BSF, newpCodeOpBit (aopGet (AOP (result), 0, FALSE, FALSE), 0, 0));// this will add + xx
        }
        else
        {
            emitpcode (POC_RLCF, popGet (AOP (result), 0));
            if (ifx)
            {
                /* Result is expected to be in CARRY by genSkipc () below. */
                emitpcode (POC_RRCFW, popGet (AOP (result), 0));
            }                   // if
        }                       // if
    }                           // if (result)
	else if (ifx==NULL && result && AOP_TYPE(result) == AOP_CRY && AOP(result)->aopu.aop_dir!=NULL) // we need add this!!
	{
		// now correct result in carry
		if (invert_result)
		{
			emitpcode(POC_BCF, popGet(AOP(result), 0));
			emitSKPC;
			emitpcode(POC_BSF, popGet(AOP(result), 0));
		}
		else
		{
			emitpcode(POC_BSF, popGet(AOP(result), 0));
			emitSKPC;
			emitpcode(POC_BCF, popGet(AOP(result), 0));

		}
	}

    // perform conditional jump
    if (ifx)
    {
        //DEBUGpc ("generate control flow");
        genSkipc (&rIfx);
        ifx->generated = 1;
    }                           // if
}

/*-----------------------------------------------------------------*/
/* genCmpGt :- greater than comparison                             */
/*-----------------------------------------------------------------*/
static void
genCmpGt (iCode * ic, iCode * ifx)
{
    operand *left, *right, *result;
    sym_link *letype, *retype;
    int sign;

    FENTRY;
    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);
    left = IC_LEFT (ic);
    right = IC_RIGHT (ic);
    result = IC_RESULT (ic);

    sign = 0;
    if (IS_SPEC (operandType (left)) && IS_SPEC (operandType (right)))
    {
        letype = getSpec (operandType (left));
        retype = getSpec (operandType (right));
        sign = !(SPEC_USIGN (letype) | SPEC_USIGN (retype));
    }

    /* assign the amsops */
    aopOp (left, ic, FALSE);
    aopOp (right, ic, FALSE);
    aopOp (result, ic, TRUE);

    genCmp (right, left, result, ifx, sign);

    freeAsmop (left, NULL, ic, (RESULTONSTACK (ic) ? FALSE : TRUE));
    freeAsmop (right, NULL, ic, (RESULTONSTACK (ic) ? FALSE : TRUE));
    freeAsmop (result, NULL, ic, TRUE);
}

/*-----------------------------------------------------------------*/
/* genCmpLt - less than comparisons                                */
/*-----------------------------------------------------------------*/
static void
genCmpLt (iCode * ic, iCode * ifx)
{
    operand *left, *right, *result;
    sym_link *letype, *retype;
    int sign;

    FENTRY;
    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);
    left = IC_LEFT (ic);
    right = IC_RIGHT (ic);
    result = IC_RESULT (ic);

    sign = 0;
    if (IS_SPEC (operandType (left)) && IS_SPEC (operandType (right)))
    {
        letype = getSpec (operandType (left));
        retype = getSpec (operandType (right));
        sign = !(SPEC_USIGN (letype) | SPEC_USIGN (retype));
    }

    /* assign the amsops */
    aopOp (left, ic, FALSE);
    aopOp (right, ic, FALSE);
    aopOp (result, ic, TRUE);

    genCmp (left, right, result, ifx, sign);

    freeAsmop (left, NULL, ic, (RESULTONSTACK (ic) ? FALSE : TRUE));
    freeAsmop (right, NULL, ic, (RESULTONSTACK (ic) ? FALSE : TRUE));
    freeAsmop (result, NULL, ic, TRUE);
}


// get continuous 0,ff number of lit
int cont00ffnum(unsigned long long lit, int startbyte, unsigned char repv) // down count byte
{
	unsigned char litb;
	int i;
	int count = 0;
	for (i = startbyte; i >= 0; i--)
	{
		litb = (unsigned char)((lit >> (8 * i)) & 0xff);
		if (litb == repv)
			count++;
		else
			break;
	}
	return count;
}

/*-----------------------------------------------------------------*/
/* genCmpEq - generates code for equal to                          */
/*-----------------------------------------------------------------*/
static void
genCmpEq (iCode * ic, iCode * ifx)
{
    operand *left, *right, *result;
    int size;
    symbol *false_label;

    FENTRY;
    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);

    if (ifx)
        DEBUGHY08A_emitcode ("; ifx is non-null", "");
    else
        DEBUGHY08A_emitcode ("; ifx is null", "");

    aopOp ((left = IC_LEFT (ic)), ic, FALSE);
    aopOp ((right = IC_RIGHT (ic)), ic, FALSE);
    aopOp ((result = IC_RESULT (ic)), ic, TRUE);

    DEBUGHY08A_AopType (__LINE__, left, right, result);

    /* if literal, move literal to right */
    if (op_isLitLike (IC_LEFT (ic)))
    {
        operand *tmp = right;
        right = left;
        left = tmp;
    }

    false_label = NULL;
    if (ifx && !IC_TRUE (ifx))
    {
        assert (IC_FALSE (ifx));
        false_label = IC_FALSE (ifx);
    }

    size = min (AOP_SIZE (left), AOP_SIZE (right));
    assert (!HY08A_sameRegs (AOP (result), AOP (left)));
    assert (!HY08A_sameRegs (AOP (result), AOP (right)));

    /* assume left != right */
	if(AOP_TYPE(result)!=AOP_CRY && ifx==NULL) // valid when ifx is null
    {
        int i;
        for (i = 0; i < AOP_SIZE (result); i++)
        {
            emitpcode (POC_CLRF, popGet (AOP (result), i));
        }
    }

    if (AOP_TYPE (right) == AOP_LIT)
    {
        unsigned long long lit = ullFromVal (AOP (right)->aopu.aop_lit);
        int i,j,byteid=-1;
		int repNum;
        //int lastw = -1;
        size = AOP_SIZE (left);
        assert (!op_isLitLike (left));

		if (AOP_TYPE(left) == AOP_CRY) // special case, 
		{
			if (ifx) // if there is ifx, the result can be skipped
			{
				switch (lit)
				{
				case 0:

					emitpcode(IC_TRUE(ifx)?POC_BTSS:POC_BTSZ, popGet(AOP(left), 0)); break;
				default:
					emitpcode(IC_TRUE(ifx)?POC_BTSZ:POC_BTSS, popGet(AOP(left), 0)); break;
				}
				//if (!false_label)
					//false_label = newiTempLabel(NULL);
				emitpcode(POC_JMP, popGetLabel(IC_TRUE(ifx)?IC_TRUE(ifx)->key:IC_FALSE(ifx)->key ));
				goto release;
			}
			else if(AOP_TYPE(result)==AOP_CRY)
			{
				switch (lit)
				{
				case 0:
					emitpcode(POC_BSF, popGet(AOP(result), 0));
					emitpcode(POC_BTSS, popGet(AOP(left), 0)); 
					emitpcode(POC_BCF, popGet(AOP(result), 0));
					break;
				default:
					emitpcode(POC_BCF, popGet(AOP(result), 0));
					emitpcode(POC_BTSS, popGet(AOP(left), 0));
					emitpcode(POC_BSF, popGet(AOP(result), 0));
					break;
				}
				goto release;

			}
			else
			{
				//int size = AOP_SIZE(result);
				//int i;
				//// clear it first
				//for (i = 0; i < size; i++)
				//	emitpcode(POC_CLRF, popGet(AOP(result), i)); // already cleared
				switch (lit)
				{
				case 0:
					emitpcode(POC_BTSS, popGet(AOP(left), 0));
					emitpcode(POC_INF, popGet(AOP(result), 0));
					break;
				default:
					emitpcode(POC_BTSZ, popGet(AOP(left), 0));
					emitpcode(POC_INF, popGet(AOP(result), 0));
					break;
				}
			}
		}

        switch (lit)
        {
        case 0:
			// from high to low , because function return high is W
			assert(size >= 1);
			if (size ==1)
			{
				emitpcode(POC_RRFW, popGet(AOP(left), 0));
			}
			else
			{
				mov2w(AOP(left), size - 1);
				for (i = size - 2; i >= 0; i--)
					emitpcode(POC_IORFW, popGet(AOP(left), i));
			}
            /* now Z is set iff `left == right' */
            emitSKPZ;
            if (!false_label)
                false_label = newiTempLabel (NULL);
            emitpcode (POC_JMP, popGetLabel (false_label->key));
            break;

        default:

			// 2018 Oct, add special case
			// if one byte is non-zero and others zero., we can xor that byte then OR other bytes
			for (i = 0, j = 0; i < size; i++)
			{
				if ((lit >> (i * 8)) & 0xff)
				{
					byteid = i; // this byte is non-zero
					j++;
				}
			}
			if (j == 1 && size>1 && ic->prev->op!=CALL) // there should not be ZERO case, skip CALL
			{
				// get the nonzero byte first
				int lastw = ((lit >> (8 * byteid)) & 0xff);
				if (op_isLitLike(left))
				{
					emitpcode(POC_MVL, popGetLit(lastw));
					emitpcode(POC_XORLW, popGet(AOP(left), byteid));					
				}
				else
				{
					switch (lastw)
					{
						case 0x1:
						emitpcode(POC_DECFW, popGet(AOP(left), byteid)); 
						break;
						case 0xff:
						emitpcode(POC_INFW, popGet(AOP(left), byteid));
						break;

					default:
						emitpcode(POC_MVFW, popGet(AOP(left), byteid));
						emitpcode(POC_XORLW, popGetLit(lastw));
						break;
					}
				}

				for (i = size - 1; i >= 0; i--)
				{
					if (i == byteid)
						continue;
					if (op_isLitLike(left))
						emitpcode(POC_IORL, popGet(AOP(left), i));
					else
						emitpcode(POC_IORFW, popGet(AOP(left), i));
				}
				emitSKPZ;
				if (!false_label)
					false_label = newiTempLabel(NULL);
				assert(false_label);
				emitpcode(POC_JMP, popGetLabel(false_label->key));

			}
			else
			{

				for (i = size - 1; i >= 0; i--) // when func return, msb at acc, were we start to compare
				{
					int lastw;
					if (op_isLitLike(left))// some times address, known when link
					{
						lastw = ((lit >> (8 * i)) & 0xff);
						emitpcode(POC_MVL, popGetLit(lastw));

						emitpcode(POC_XORLW, popGet(AOP(left), i));
						emitSKPZ;
					}
					else
					{
						// for consecutive 00 and FF, we use AND/OR
						switch ((lit >> (8 * i)) & 0xff)
						{
						case 0:
							repNum = cont00ffnum(lit, i, 0);
							if (repNum == 1)
							{
								emitpcode(POC_RRFW, popGet(AOP(left), i));
							}
							else
							{
								int j;
								for (j = 0; j < repNum; j++)
								{
									if (j == 0)
										emitpcode(POC_MVFW, popGet(AOP(left), i));
									else
										emitpcode(POC_IORFW, popGet(AOP(left), i - j));
								}
								i -= (repNum - 1);
							}
							emitSKPZ;
							break;
						case 1:
							emitpcode(POC_DCSZW, popGet(AOP(left), i));
							break;
						case 0xff:
							repNum = cont00ffnum(lit, i, 0xff);
							if (repNum == 1)
							{
								emitpcode(POC_INSZW, popGet(AOP(left), i));
							}
							else
							{
								int j;
								for (j = 0; j < repNum; j++)
								{
									if (j == 0)
										emitpcode(POC_MVFW, popGet(AOP(left), i));
									else
										emitpcode(POC_ANDFW, popGet(AOP(left), i - j));
								}
								emitpcode(POC_INSZW, popCopyReg(&pc_wreg));
								i -= (repNum - 1);
							}
							break;
						case 0x7f:
							// 7f80 uses ov
							emitpcode(POC_INFW, popGet(AOP(left), i));
							emitSKPOV;
							break;
						case 0x80:
							emitpcode(POC_DECFW, popGet(AOP(left), i));
							emitSKPOV;
							break;
						default:

							mov2w(AOP(left), i);
							emitpcode(POC_XORLW, popGetLit((lit >> (8 * i)) & 0xff));
							emitSKPZ;
						}
					}

					/* now Z is cleared if `left != right' */

					if (!false_label)
						false_label = newiTempLabel(NULL);
					assert(false_label);
					emitpcode(POC_JMP, popGetLabel(false_label->key));
				}                   // for i
			}
            break;
        }                       // switch (lit)
    }
	else if (AOP_TYPE(left) == AOP_CRY)// right is not lit
	{
		if (AOP_TYPE(right) == AOP_CRY) // bit compare bit
		{
			// set C to 1 toggle for both
			emitSETC;
			emitpcode(POC_BTSZ, popGet(AOP(left), 0));
			emitpcode(POC_BTGF, popCopyGPR2Bit(PCOP(&pc_status), HYA_C_BIT));
			emitpcode(POC_BTSZ, popGet(AOP(right), 0));
			emitpcode(POC_BTGF, popCopyGPR2Bit(PCOP(&pc_status), HYA_C_BIT));
			if (ifx)
			{
				if (IC_TRUE(ifx))
				{
					emitSKPNC;
					emitpcode(POC_JMP, popGetLabel(IC_TRUE(ifx)->key));
				}
				else
				{
					emitSKPC;
					emitpcode(POC_JMP, popGetLabel(IC_FALSE(ifx)->key));
				}
			}
			else
			{
				if (AOP_TYPE(result) == AOP_CRY)
				{
					emitpcode(POC_BCF, popGet(AOP(result), 0));
					emitSKPNC;
					emitpcode(POC_BSF, popGet(AOP(result), 0));
				}
				else
				{
					//int size = AOP_SIZE(result);
					//int i;
					//// clear it first
					//for (i = 0; i < size; i++)
					//	emitpcode(POC_CLRF, popGet(AOP(result), i));
					emitSKPNC;
					emitpcode(POC_INF, popGet(AOP(result), 0));
				}
			}

		}
		goto release;
	}else
    {
        /* right is no literal */
        int i;

        for (i = size-1; i >=0; i--)
        {
            mov2w (AOP (right), i);
            emitpcode (POC_XORFW, popGet (AOP (left), i));
            /* now Z is cleared if `left != right' */
            emitSKPZ;
            if (!false_label)
                false_label = newiTempLabel (NULL);
			assert(false_label);
            emitpcode (POC_JMP, popGetLabel (false_label->key));
        }                       // for i
    }

    /* if we reach here, left == right */

    if (AOP_SIZE (result) > 0 && ifx==NULL)
    {
        emitpcode (POC_INF, popGet (AOP (result), 0));
    }

    if (ifx && IC_TRUE (ifx))
    {
        emitpcode (POC_JMP, popGetLabel (IC_TRUE (ifx)->key));
    }

    if (false_label && (!ifx || IC_TRUE (ifx)))
        emitpLabel (false_label->key);
release:
    if (ifx)
        ifx->generated = 1;

    freeAsmop (left, NULL, ic, (RESULTONSTACK (ic) ? FALSE : TRUE));
    freeAsmop (right, NULL, ic, (RESULTONSTACK (ic) ? FALSE : TRUE));
    freeAsmop (result, NULL, ic, TRUE);
}

/*-----------------------------------------------------------------*/
/* genAndOp - for && operation                                     */
/*-----------------------------------------------------------------*/
static void
genAndOp (iCode * ic)
{
    operand *left, *right, *result;
    /*     symbol *tlbl; */

    FENTRY;
    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);
    /* note here that && operations that are in an
       if statement are taken away by backPatchLabels
       only those used in arthmetic operations remain */
    aopOp ((left = IC_LEFT (ic)), ic, FALSE);
    aopOp ((right = IC_RIGHT (ic)), ic, FALSE);
    aopOp ((result = IC_RESULT (ic)), ic, FALSE);

    DEBUGHY08A_AopType (__LINE__, left, right, result);

    emitpcode (POC_MVFW, popGet (AOP (left), 0));
    emitpcode (POC_ANDFW, popGet (AOP (right), 0));
    emitpcode (POC_MVWF, popGet (AOP (result), 0));

    /* if both are bit variables */
    /*     if (AOP_TYPE(left) == AOP_CRY && */
    /*         AOP_TYPE(right) == AOP_CRY ) { */
    /*         HY08A_emitcode("mov","c,%s",AOP(left)->aopu.aop_dir); */
    /*         HY08A_emitcode("anl","c,%s",AOP(right)->aopu.aop_dir); */
    /*         HY08A_outBitC(result); */
    /*     } else { */
    /*         tlbl = newiTempLabel(NULL); */
    /*         HY08A_toBoolean(left);     */
    /*         HY08A_emitcode("jz","%05d_DS_",labelKey2num (tlbl->key)); */
    /*         HY08A_toBoolean(right); */
    /*         HY08A_emitcode("","%05d_DS_:",labelKey2num (tlbl->key)); */
    /*         HY08A_outBitAcc(result); */
    /*     } */

    freeAsmop (left, NULL, ic, (RESULTONSTACK (ic) ? FALSE : TRUE));
    freeAsmop (right, NULL, ic, (RESULTONSTACK (ic) ? FALSE : TRUE));
    freeAsmop (result, NULL, ic, TRUE);
}


/*-----------------------------------------------------------------*/
/* genOrOp - for || operation                                      */
/*-----------------------------------------------------------------*/
/*
tsd hycon port -
modified this code, but it doesn't appear to ever get called
*/

static void
genOrOp (iCode * ic)
{
    operand *left, *right, *result;
    symbol *tlbl;
    int i;

    /* note here that || operations that are in an
       if statement are taken away by backPatchLabels
       only those used in arthmetic operations remain */
    FENTRY;
    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);
    aopOp ((left = IC_LEFT (ic)), ic, FALSE);
    aopOp ((right = IC_RIGHT (ic)), ic, FALSE);
    aopOp ((result = IC_RESULT (ic)), ic, FALSE);

    DEBUGHY08A_AopType (__LINE__, left, right, result);

    for (i = 0; i < AOP_SIZE (result); i++)
    {
        emitpcode (POC_CLRF, popGet (AOP (result), i));
    }                           // for i

    tlbl = newiTempLabel (NULL);
    HY08A_toBoolean (left);
    emitSKPZ;
    emitpcode (POC_JMP, popGetLabel (tlbl->key));
    HY08A_toBoolean (right);
    emitpLabel (tlbl->key);
    /* here Z is clear IFF `left || right' */
    emitSKPZ;
    emitpcode (POC_INF, popGet (AOP (result), 0));

    freeAsmop (left, NULL, ic, (RESULTONSTACK (ic) ? FALSE : TRUE));
    freeAsmop (right, NULL, ic, (RESULTONSTACK (ic) ? FALSE : TRUE));
    freeAsmop (result, NULL, ic, TRUE);
}

/*-----------------------------------------------------------------*/
/* isLiteralBit - test if lit == 2^n                               */
/*-----------------------------------------------------------------*/
static int
isLiteralBit (unsigned long long lit)
{
	
 /*   unsigned long pw[64] = { 1LL, 2LL, 4L, 8L, 16L, 32L, 64L, 128L,
                             0x100LL, 0x200LL, 0x400L, 0x800L,
                             0x1000LL, 0x2000LL, 0x4000L, 0x8000L,
                             0x10000LL, 0x20000LL, 0x40000L, 0x80000L,
                             0x100000LL, 0x200000LL, 0x400000L, 0x800000L,
                             0x1000000LL, 0x2000000L, 0x4000000L, 0x8000000L,
                             0x10000000LL, 0x20000000L, 0x40000000L, 0x80000000L,



                           };*/

	unsigned long long chk = 1LL;

    int idx;

    FENTRY;
    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);
    for (idx = 0; idx < 64; idx++, chk<<=1)
        if (lit == chk)
            return idx + 1;
    return 0;
}

/*-----------------------------------------------------------------*/
/* continueIfTrue -                                                */
/*-----------------------------------------------------------------*/
static void
continueIfTrue (iCode * ic)
{
    FENTRY;
    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);
    if (IC_TRUE (ic))
    {
        
		//if (IC_FALSE(ic) == NULL)
			//fprintf(stderr, "no false label?\n");
		emitpcode(POC_JMP, popGetLabel((IC_TRUE(ic)->key))); // need offset?
		//if (strstr(PCOLAB(PCI(pb->pcTail)->pcop)->pcop.name, "397"))
			//fprintf(stderr, "397\n");
        //HY08A_emitcode ("ljmp", "%05d_DS_", labelKey2num (IC_FALSE (ic)->key));
    }
    ic->generated = 1;
}

/*-----------------------------------------------------------------*/
/* jmpIfTrue -                                                     */
/*-----------------------------------------------------------------*/
static void
jumpIfTrue (iCode * ic)
{
    FENTRY;
    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);
    if (!IC_TRUE (ic))
    {
		assert(IC_TRUE(ic));
        emitpcode (POC_JMP, labelKey2num (popGetLabel (IC_TRUE (ic)->key)));
        HY08A_emitcode ("ljmp", "%05d_DS_", labelKey2num (IC_FALSE (ic)->key));
    }
    ic->generated = 1;
}

/*-----------------------------------------------------------------*/
/* jmpTrueOrFalse -                                                */
/*-----------------------------------------------------------------*/
static void
jmpTrueOrFalse (iCode * ic, symbol * tlbl)
{
    FENTRY;
    // ugly but optimized by peephole
    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);
/*
    if (IC_TRUE (ic))
    {
        symbol *nlbl = newiTempLabel (NULL);
        HY08A_emitcode ("sjmp", "%05d_DS_", labelKey2num (nlbl->key));
        HY08A_emitcode ("", "%05d_DS_:", labelKey2num (tlbl->key));
        HY08A_emitcode ("ljmp", "%05d_DS_", labelKey2num (IC_TRUE (ic)->key));
        HY08A_emitcode ("", "%05d_DS_:", labelKey2num (nlbl->key));
    }
    else
    {
        HY08A_emitcode ("ljmp", "%05d_DS_", labelKey2num (IC_FALSE (ic)->key));
        HY08A_emitcode ("", "%05d_DS_:", labelKey2num (tlbl->key));
    }
    ic->generated = 1;
*/
}


static int contfflsb(unsigned long long lit)
{
	int count = 0;
	while ((lit & 0xff) == 0xff)
	{
		count++;
		lit >>= 8;
	}
	return count;
}
/*-----------------------------------------------------------------*/
/* genAnd  - code for and                                          */
/*-----------------------------------------------------------------*/
static void
genAnd (iCode * ic, iCode * ifx)
{
    operand *left, *right, *result;
    int size, offset = 0;
    unsigned long long lit = 0L;
    unsigned long long bytelit = 0;
	unsigned long long sizelmask;
    resolvedIfx rIfx;
	int resultX = 0;
	int i;

    FENTRY;
    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);
    aopOp ((left = IC_LEFT (ic)), ic, FALSE);
    aopOp ((right = IC_RIGHT (ic)), ic, FALSE);
    aopOp ((result = IC_RESULT (ic)), ic, TRUE);
	resultX = IS_TRUE_SYMOP(IC_RESULT(ic)) && IN_XSPACE(SPEC_OCLS(OP_SYMBOL(IC_RESULT(ic))->etype));

    resolveIfx (&rIfx, ifx);
	assert(result);
	assert(left);
	assert(right);
	assert(result->aop);
	assert(left->aop);
	assert(right->aop);

    /* if left is a literal & right is not then exchange them */
    if ((AOP_TYPE (left) == AOP_LIT && AOP_TYPE (right) != AOP_LIT) || AOP_NEEDSACC (left))
    {
        operand *tmp = right;
        right = left;
        left = tmp;
    }

    /* if result = right then exchange them */
    if (HY08A_sameRegs (AOP (result), AOP (right)))
    {
        operand *tmp = right;
        right = left;
        left = tmp;
    }

    /* if right is bit then exchange them */
    if (AOP_TYPE (right) == AOP_CRY && AOP_TYPE (left) != AOP_CRY)
    {
        operand *tmp = right;
        right = left;
        left = tmp;
    }
	if (AOP_TYPE(right) == AOP_LIT)
	{
		lit = ullFromVal(AOP(right)->aopu.aop_lit);
		if (AOP_SIZE(left) != 8)
		{
			lit &= ((1LL<<(AOP_SIZE(left)*8))-1); // we an check if 80000000L is 1 bit!!
		}
	}

	//if (!ifx)
		size = AOP_SIZE(result);
	//else
		//size = 0; // for ifx

    DEBUGHY08A_AopType (__LINE__, left, right, result);

    // if(bit & yy)
    // result = bit & yy;

	// for AND operation , if any operand is CRY,
	// result can be 1 or 0
    if (AOP_TYPE (left) == AOP_CRY)
    {

        // c = bit & literal;
		
        if (AOP_TYPE (right) == AOP_LIT)
        {
			int i;
            if (lit & 1)
            {

                if (size && HY08A_sameRegs (AOP (result), AOP (left)))
                    // no change
                    goto release;
    //            HY08A_emitcode ("mov", "c,%s", AOP (left)->aopu.aop_dir);
				//mov2w(AOP(left), 0);
				//for (i = 1; i < AOP_SIZE(result); i++)
				//	emitpcode(POC_CLRF, popGet(AOP(result), i)); // it will be optimized 
				//emitpcode(POC_MVWF, popGet(AOP(result), 0));
				// if lit &1 , result = 0 ... I think this will be optimized?
				// result = left
				if (AOP_TYPE(result) == AOP_CRY)
				{

					if (ifx)
					{

						emitCLRC;
						emitpcode(POC_BTSZ, popGet(AOP(left), 0));
						emitSETC;
						genIfxJump(ifx, "C");
						goto release;
					}
					emitpcode(POC_BCF, popGet(AOP(result), 0));
					emitpcode(POC_BTSZ, popGet(AOP(left), 0));
					emitpcode(POC_BSF, popGet(AOP(result), 0));
					goto release;
				}
            }
            else
            {
                // bit(result) = 0;
                if (size && (AOP_TYPE (result) == AOP_CRY))
                {
                    //HY08A_emitcode ("clr", "%s", AOP (result)->aopu.aop_dir);
					for (i = 0; i < AOP_SIZE(result); i++)
						emitpcode(POC_CLRF, popGet(AOP(result), i));
                    goto release;
                }
                if ((AOP_TYPE (result) == AOP_CRY) && ifx)
                {
                    jumpIfTrue (ifx);
                    goto release;
                }
                HY08A_emitcode ("clr", "c");
            }
        }
        else 
        { // here left bit, right not lit
            if (AOP_TYPE (right) == AOP_CRY)
            {
                // c = bit & bit;
                //HY08A_emitcode ("mov", "c,%s", AOP (right)->aopu.aop_dir);
                //HY08A_emitcode ("anl", "c,%s", AOP (left)->aopu.aop_dir);
				// we calculate to C
				if (AOP_TYPE(result) == AOP_CRY && !ifx)
				{
					emitpcode(POC_BSF, popGet(AOP(result), 0));
					emitpcode(POC_BTSZ, popGet(AOP(left), 0));
					emitpcode(POC_BTSS, popGet(AOP(right), 0));
					emitpcode(POC_BCF, popGet(AOP(result), 0));
					goto release;

				}

				emitSETC;
				emitpcode(POC_BTSZ, popGet(AOP(left), 0));
				emitpcode(POC_BTSS, popGet(AOP(right), 0));
				emitCLRC;
				// set the result by C
				if (AOP_TYPE(result) == AOP_CRY)
				{
					if (ifx)
						genIfxJump(ifx, "C");
					else
					{
						emitpcode(POC_BCF, popGet(AOP(result), 0));
						emitSKPNC;
						emitpcode(POC_BSF, popGet(AOP(result), 0));

					}
					goto release;
				}
				else
				{
					emitpcode(POC_CLRF, popGet(AOP(result), 0));
					emitSKPNC;
					emitpcode(POC_INF, popGet(AOP(result), 0));
					for (i = 1; i < size; i++)
						emitpcode(POC_CLRF, popGet(AOP(result), i));
					goto release;
				}

				
            }
            else
            {
                // c = bit & val;
                //MOVA (aopGet (AOP (right), 0, FALSE, FALSE));
                // c = lsb
                //HY08A_emitcode ("rrc", "a");
                //HY08A_emitcode ("anl", "c,%s", AOP (left)->aopu.aop_dir);
				emitpcode(POC_RRCFW, popGet(AOP(right), 0)); // now it is C
				emitpcode(POC_BTSS, popGet(AOP(left), 0));
				emitCLRC;
				// now result is C
				if (AOP_TYPE(result) == AOP_CRY)
				{
					if (ifx)
						genIfxJump(ifx, "C");
					else
					{
						emitpcode(POC_BCF, popGet(AOP(result), 0));
						emitSKPC;
						emitpcode(POC_BSF, popGet(AOP(result), 0));
					}
				}
				else
				{
					for (i = 0; i < size; i++)
						emitpcode(POC_CLRF, popGet(AOP(result), i));
					emitSKPNC;
					emitpcode(POC_INF, popGet(AOP(result), 0));
				}
				goto release;
            }
        }
        // bit = c
        // val = c
        //if (size)
        //    HY08A_outBitC (result);
        //// if(bit & ...)
        //else if ((AOP_TYPE (result) == AOP_CRY) && ifx)
        //    genIfxJump (ifx, "c");
        //goto release;
    }

    // if(val & 0xZZ)       - size = 0, ifx != FALSE  -
    // bit = val & 0xZZ     - size = 1, ifx = FALSE -
    if ((AOP_TYPE (right) == AOP_LIT) && (AOP_TYPE (result) == AOP_CRY) && (AOP_TYPE (left) != AOP_CRY))
    {
        int posbit = isLiteralBit (lit);
        /* left &  2^n */
        if (posbit)
        {
            posbit--;
            //MOVA(aopGet(AOP(left),posbit>>3,FALSE,FALSE));
            // bit = left & 2^n
            if (size)
                HY08A_emitcode ("mov", "c,acc.%d", posbit & 0x07);
            // if(left &  2^n)
            else
            {
                if (ifx)
                {
                    int offset = 0;
                    while (posbit > 7)
                    {
                        posbit -= 8;
                        offset++;
                    }
                    emitpcode (((rIfx.condition) ? POC_BTSZ : POC_BTSS),
                    //emitpcode (POC_BTSZ,
                               newpCodeOpBit (aopGet (AOP (left), offset, FALSE, FALSE), posbit, 0));// aopget is fine
                    emitpcode (POC_JMP, popGetLabel (rIfx.lbl->key));

                    ifx->generated = 1;
                }
                goto release;
            }
        }
        else
        {
            symbol *tlbl = newiTempLabel (NULL);
            int sizel = AOP_SIZE (left);
			if (sizel == 8)
				sizelmask = 0xfffffffffffffffful;
			else
				sizelmask = (1ull << (8 * sizel)) - 1;
            //if (size)
                //HY08A_emitcode ("setb", "c");
            while (sizel--)
            {
                if ((bytelit = ((lit >> (offset * 8)) & 0x0FFL)) != 0x0LL)
                {
                    // byte ==  2^n ?
                    if ((posbit = isLiteralBit (bytelit)) != 0)
                    {
						// if last is mvwf to same
						char buf[100];

						if (pb->pcTail && isPCI(pb->pcTail) && PCI(pb->pcTail)->op == POC_MVWF &&
							!strcmp(get_op(PCI(pb->pcTail)->pcop, buf, 99), aopGet(AOP(left), offset, FALSE, FALSE)))
						{
							emitpcode(POC_BTSZ,
								newpCodeOpBit("_WREG", posbit - 1, 0));
						}
						else
						{
							emitpcode(POC_BTSZ,
								newpCodeOpBit(aopGet(AOP(left), offset, FALSE, FALSE), posbit - 1, 0));
						}
                    }
                    else
                    {

						bytelit = (((lit&sizelmask) >> (offset * 8)));
						if ((bytelit & 0x0ff) == 0xff)
						{
							int c = contfflsb(bytelit);
							int ii;
							
							if (c == 1)
							{
								emitpcode(POC_RRFW, popGet(AOP(left), offset));
							}
							else
							{
								emitpcode(POC_MVFW, popGet(AOP(left), offset++));
								//sizel--;
								for (ii = 0; ii < c - 1 && sizel ; ii++)
								{
									emitpcode(POC_IORFW, popGet(AOP(left), offset++));
									sizel--;
								}
								offset--; // later++
							}
						}
						else
						{
							mov2w(AOP(left), offset);
							emitpcode(POC_ANDLW, newpCodeOpLit(bytelit & 0x0ff));
						}
                        emitSKPZ;
                    }
                    emitpcode (POC_JMP, popGetLabel (rIfx.condition ?
                                                     rIfx.lbl->key : tlbl->key));
                }
                offset++;
            }
            if (!rIfx.condition)
            {
                emitpcode (POC_JMP, popGetLabel (rIfx.lbl->key));
            }
            emitpLabel(tlbl->key);
            ifx->generated = 1;
            // bit = left & literal
            if (size)
            {
                HY08A_emitcode ("clr", "c");
                HY08A_emitcode ("", "%05d_DS_:", labelKey2num (tlbl->key));
            }
            // if(left & literal)
            else
            {
                if (ifx)
                    jmpTrueOrFalse (ifx, tlbl);
                goto release;
            }
        }
        HY08A_outBitC (result);
        goto release;
    }

    /* if left is same as result */
	// left=same should not be X?
    if (HY08A_sameRegs (AOP (result), AOP (left)))
    {
        int know_W = -1;
        for (; size--; offset++, lit >>= 8)
        {
            if (AOP_TYPE (right) == AOP_LIT)
            {
                switch (lit & 0xff)
                {
                case 0x00:
                    /*  and'ing with 0 has clears the result */
                    emitpcode (POC_CLRF, popGet (AOP (result), offset));
                    break;
                case 0xff:
                    /* and'ing with 0xff is a nop when the result and left are the same */
                    break;

                default:
                {
                    int p = my_powof2 ((~lit) & 0xff);
                    if (p >= 0)
                    {
                        /* only one bit is set in the literal, so use a bcf instruction */
                        emitpcode (POC_BCF, newpCodeOpBit (aopGet (AOP (left), offset, FALSE, FALSE), p, 0));

                    }
                    else
                    {
                        if (know_W != (int) (lit & 0xff))
                            emitpcode (POC_MVL, popGetLit (lit & 0xff));
                        know_W = lit & 0xff;
                        emitpcode (POC_ANDWF, popGet (AOP (left), offset));
                    }
                }
                }
            }
            else
            {
                emitpcode (POC_MVFW, popGet (AOP (right), offset));
                emitpcode (POC_ANDWF, popGet (AOP (left), offset));
            }
        }

    }
    else
    {
        // left & result in different registers
        if (AOP_TYPE (result) == AOP_CRY)
        {
            // result = bit
            // if(size), result in bit
            // if(!size && ifx), conditional oper: if(left & right)
            symbol *tlbl = newiTempLabel (NULL);
            int sizer = min (AOP_SIZE (left), AOP_SIZE (right));
            if (size)
                HY08A_emitcode ("setb", "c");
            while (sizer--)
            {
                MOVA (aopGet (AOP (right), offset, FALSE, FALSE));
                HY08A_emitcode ("anl", "a,%s", aopGet (AOP (left), offset, FALSE, FALSE));
                HY08A_emitcode ("jnz", "%05d_DS_", labelKey2num (tlbl->key));
                offset++;
            }
            if (size)
            {
                CLRC;
                HY08A_emitcode ("", "%05d_DS_:", labelKey2num (tlbl->key));
                HY08A_outBitC (result);
            }
            else if (ifx)
                jmpTrueOrFalse (ifx, tlbl);
        }
        else
        {
			if (resultX)
				emitldpr(popGet(AOP(result), 0));
            for (; (size--); offset++)
            {
                // normal case
                // result = left & right
                if (AOP_TYPE (right) == AOP_LIT)
                {
                    int t = (lit >> (offset * 8)) & 0x0FFL;
                    switch (t)
                    {
                    case 0x00:
						if (resultX)
							emitpcode(POC_CLRF, PCOP_POINC0);
						else
							emitpcode(POC_CLRF, popGet(AOP(result), offset));
                        break;
                    case 0xff:
                        if(op_isLitLike(left))// special case
                            emitpcode (POC_MVL, popGet (AOP (left), offset));
                        else 
                            emitpcode (POC_MVFW, popGet (AOP (left), offset));
						if (resultX)
							emitpcode(POC_MVWF, PCOP_POINC0);
						else
							emitpcode (POC_MVWF, popGet (AOP (result), offset));
                        break;
                    default:
                        emitpcode (POC_MVL, popGetLit (t));
                        if(op_isLitLike(left))// special case
                            emitpcode (POC_ANDLW, popGet (AOP (left), offset));
                        else 
                            emitpcode (POC_ANDFW, popGet (AOP (left), offset));
						if (resultX)
							emitpcode(POC_MVWF, PCOP_POINC0);
						else
							emitpcode(POC_MVWF, popGet(AOP(result), offset));
                    }
                    continue;
                }

                emitpcode (POC_MVFW, popGet (AOP (right), offset));
                emitpcode (POC_ANDFW, popGet (AOP (left), offset));
				if (resultX)
					emitpcode(POC_MVWF, PCOP_POINC0);
				else
					emitpcode(POC_MVWF, popGet(AOP(result), offset));
            }
        }
    }

release:
    freeAsmop (left, NULL, ic, (RESULTONSTACK (ic) ? FALSE : TRUE));
    freeAsmop (right, NULL, ic, (RESULTONSTACK (ic) ? FALSE : TRUE));
    freeAsmop (result, NULL, ic, TRUE);
}

/*-----------------------------------------------------------------*/
/* genOr  - code for or                                            */
/*-----------------------------------------------------------------*/
static void
genOr (iCode * ic, iCode * ifx)
{
    operand *left, *right, *result;
    int size, offset = 0;
    unsigned long long lit = 0L;
	int resultX = 0;

    FENTRY;
    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);

    aopOp ((left = IC_LEFT (ic)), ic, FALSE);
    aopOp ((right = IC_RIGHT (ic)), ic, FALSE);
    aopOp ((result = IC_RESULT (ic)), ic, TRUE);
	ASSIGNRESULTX;


    DEBUGHY08A_AopType (__LINE__, left, right, result);

    /* if left is a literal & right is not then exchange them */
	assert(left);
	assert(right);
	assert(right->aop);
	assert(left->aop);
    if ((AOP_TYPE (left) == AOP_LIT && AOP_TYPE (right) != AOP_LIT) || AOP_NEEDSACC (left))
    {
        operand *tmp = right;
        right = left;
        left = tmp;
    }

    /* if result = right then exchange them */
    if (HY08A_sameRegs (AOP (result), AOP (right)))
    {
        operand *tmp = right;
        right = left;
        left = tmp;
    }

    /* if right is bit then exchange them */
    if (AOP_TYPE (right) == AOP_CRY && AOP_TYPE (left) != AOP_CRY)
    {
        operand *tmp = right;
        right = left;
        left = tmp;
    }

    DEBUGHY08A_AopType (__LINE__, left, right, result);

    if (AOP_TYPE (right) == AOP_LIT)
        lit = ullFromVal (AOP (right)->aopu.aop_lit);

    size = AOP_SIZE (result);

    // if(bit | yy)
    // xx = bit | yy;
    if (AOP_TYPE (left) == AOP_CRY)
    {
        if (AOP_TYPE (right) == AOP_LIT)
        {
            // c = bit & literal;
            if (lit)
            {
                // lit != 0 => result = 1
                if (AOP_TYPE (result) == AOP_CRY)
                {
					if (size)
					{
						if (resultX)
						{
							pCodeOp *oldop = popGet(AOP(IC_RESULT(ic)), 0);
							pCodeOp *indfbit = newpCodeOpBit("_INDF0", PCORB(oldop)->bit, 0);
							emitldpr(popGet(AOP(IC_RESULT(ic)), 0));
							
							emitpcode(POC_BSF, indfbit);
						}
						else
							emitpcode(POC_BSF, popGet(AOP(result), 0));
					}
                    //HY08A_emitcode("bsf","(%s >> 3), (%s & 7)",
                    //   AOP(result)->aopu.aop_dir,
                    //   AOP(result)->aopu.aop_dir);
                    else if (ifx)
                        continueIfTrue (ifx);
                    goto release;
                }
            }
            else
            {
                // lit == 0 => result = left
                if (size && HY08A_sameRegs (AOP (result), AOP (left)))
                    goto release;
                HY08A_emitcode (";XXX mov", "c,%s  %s,%d", AOP (left)->aopu.aop_dir, __FILE__, __LINE__);
            }
        }
        else
        { // right is not lit
            if (AOP_TYPE (right) == AOP_CRY)
            {
                if (HY08A_sameRegs (AOP (result), AOP (left))) // same will not be X
                {
                    // c = bit | bit;
                    emitpcode (POC_BCF, popGet (AOP (result), 0)); // this is bit
                    emitpcode (POC_BTSZ, popGet (AOP (right), 0));
                    emitpcode (POC_BSF, popGet (AOP (result), 0));

                    HY08A_emitcode ("bcf", "(%s >> 3), (%s & 7)", AOP (result)->aopu.aop_dir, AOP (result)->aopu.aop_dir);
                    HY08A_emitcode ("btfsc", "(%s >> 3), (%s & 7)", AOP (right)->aopu.aop_dir, AOP (right)->aopu.aop_dir);
                    HY08A_emitcode ("bsf", "(%s >> 3), (%s & 7)", AOP (result)->aopu.aop_dir, AOP (result)->aopu.aop_dir);
                }
                else
                {
					if (ifx)
					{
						// result in C shall be fine
						emitCLRC;
						emitpcode(POC_BTSS, popGet(AOP(right), 0));
						emitpcode(POC_BTSZ, popGet(AOP(left), 0));
						emitSETC;
						genIfxJump(ifx, "C");
						goto release;
					}
					if (resultX)
					{
						if (AOP_TYPE(result) == AOP_CRY)
						{
							pCodeOp *oldop = popGet(AOP(IC_RESULT(ic)), 0);
							pCodeOp *indfbit = newpCodeOpBit("_INDF0", PCORB(oldop)->bit, 0);
							emitldpr(popGet(AOP(IC_RESULT(ic)), 0));
							emitpcode(POC_BCF, indfbit);
							emitpcode(POC_BTSS, popGet(AOP(right), 0));
							emitpcode(POC_BTSZ, popGet(AOP(left), 0));
							emitpcode(POC_BSF, indfbit);
						}
						else
						{
							emitldpr(popGet(AOP(IC_RESULT(ic)), 0));
							emitpcode(POC_MVL, popGetLit(0));
							emitpcode(POC_BTSS, popGet(AOP(right), 0));
							emitpcode(POC_BTSZ, popGet(AOP(left), 0));
							emitpcode(POC_MVL, popGetLit(1));
							emitpcode(POC_MVFW, PCOP(&pc_indf0));
						}
					}
					else
					{
						if (AOP_TYPE(result) == AOP_CRY)
						{
							emitpcode(POC_BCF, popGet(AOP(result), 0));
							emitpcode(POC_BTSS, popGet(AOP(right), 0));
							emitpcode(POC_BTSZ, popGet(AOP(left), 0));
							emitpcode(POC_BSF, popGet(AOP(result), 0));
						}
						else
						{
							emitpcode(POC_CLRF, popGet(AOP(result), 0));
							emitpcode(POC_BTSS, popGet(AOP(right), 0));
							emitpcode(POC_BTSZ, popGet(AOP(left), 0));
							emitpcode(POC_INF, popGet(AOP(result), 0));
						}
					}
                }
            }
            else
            {
                // c = bit | val;  .. result can be bit or not bit
				// if result is not bit, bit should be 1 then or the val
				int i;
				if (AOP_TYPE(result) != AOP_CRY)
				{
					// val = bit | val
					// result not cry, unsigned char 1 and the result
					int ii;
					if(resultX)
						emitldpr(popGet(AOP(IC_RESULT(ic)), 0));
					emitpcode(POC_MVL, popGetLit(1));
					emitpcode(POC_BTSS, popGet(AOP(left), 0));
					emitpcode(POC_MVL, popGetLit(0));
					emitpcode(POC_IORFW, popGet(AOP(right), 0));
					if (resultX)
						emitpcode(POC_MVWF, PCOP(&pc_poinc0));
					else
						emitpcode(POC_MVWF, popGet(AOP(result), 0));
					for (ii = 1; ii < size; ii++)
					{
						if (ii >= AOP_SIZE(right))
						{
							if (resultX)
								emitpcode(POC_CLRF, PCOP(&pc_poinc0));
							else
								emitpcode(POC_CLRF, popGet(AOP(result), ii));
						}
						else
						{
							emitpcode(POC_MVFW, popGet(AOP(right), ii));
							if (resultX)
								emitpcode(POC_MVWF, PCOP(&pc_poinc0));
							else
								emitpcode(POC_MVWF, popGet(AOP(result), ii));
						}
					}
					goto release;
				}

                //symbol *tlbl = newiTempLabel (NULL);
                //HY08A_emitcode (";XXX ", " %s,%d", __FILE__, __LINE__);

				
				// CRY cannot be in X!!
				// we can convert val to bit first, or OR he result first
				// we take the later

				emitpcode(POC_MVL, popGetLit(1));
				emitpcode(POC_BTSS, popGet(AOP(left), 0));
				emitpcode(POC_MVL, popGetLit(0));
				for (i = 0; i < AOP_SIZE(right); i++)
				{
						emitpcode(POC_IORFW, popGet(AOP(right), i));
				}
				if (ifx)
				{
					genIfxJump(ifx, "A"); // use Z
					goto release;
				}
				emitpcode(POC_BSF, popGet(AOP(result), 0));
				emitSKPNZ; // if Z, result need clear!!
				emitpcode(POC_BCF, popGet(AOP(result), 0));
				goto release;
            }
        }
        // bit = c
        // val = c
        if (size)
            HY08A_outBitC (result);
        // if(bit | ...)
        else if ((AOP_TYPE (result) == AOP_CRY) && ifx)
            genIfxJump (ifx, "c");
        goto release;
    }

    // if(val | 0xZZ)       - size = 0, ifx != FALSE  -
    // bit = val | 0xZZ     - size = 1, ifx = FALSE -
    if ((AOP_TYPE (right) == AOP_LIT) && (AOP_TYPE (result) == AOP_CRY) && (AOP_TYPE (left) != AOP_CRY))
    {
        if (lit)
        {
            HY08A_emitcode (";XXX ", " %s,%d", __FILE__, __LINE__);
            // result = 1
            if (size)
                HY08A_emitcode (";XXX setb", "%s", AOP (result)->aopu.aop_dir);
            else
                continueIfTrue (ifx);
            goto release;
        }
        else
        {
            HY08A_emitcode (";XXX ", " %s,%d", __FILE__, __LINE__);
            // lit = 0, result = boolean(left)
            if (size)
                HY08A_emitcode (";XXX setb", "c");
            HY08A_toBoolean (right);
            if (size)
            {
                //symbol *tlbl = newiTempLabel (NULL);
                //HY08A_emitcode (";XXX jnz", "%05d_DS_", labelKey2num (tlbl->key));
                //CLRC;
                //HY08A_emitcode ("", "%05d_DS_:", labelKey2num (tlbl->key));
            }
            else
            {
                genIfxJump (ifx, "a");
                goto release;
            }
        }
        HY08A_outBitC (result);
        goto release;
    }

    /* if left is same as result */
    if (HY08A_sameRegs (AOP (result), AOP (left))) // same will not have X
    {
        int know_W = -1;
        for (; size--; offset++, lit >>= 8)
        {
            if (AOP_TYPE (right) == AOP_LIT)
            {
                if ((lit & 0xff) == 0)
                    /*  or'ing with 0 has no effect */
                    continue;
				else if ((lit & 0xff) == 0xff)
				{
					emitpcode(POC_SETF, popGet(AOP(left), offset)); // set ff
				}else
                {
                    int p = my_powof2 (lit & 0xff);
                    if (p >= 0)
                    {
                        /* only one bit is set in the literal, so use a bsf instruction */
                        emitpcode (POC_BSF, newpCodeOpBit (aopGet (AOP (left), offset, FALSE, FALSE), p, 0));
                    }
                    else
                    {
                        if (know_W != (int) (lit & 0xff))
                            emitpcode (POC_MVL, popGetLit (lit & 0xff));
                        know_W = lit & 0xff;
                        emitpcode (POC_IORWF, popGet (AOP (left), offset));
                    }

                }
            }
            else
            {
                emitpcode (POC_MVFW, popGet (AOP (right), offset));

                emitpcode (POC_IORWF, popGet (AOP (left), offset));
            }
        }
    }
    else
    {
        // left & result in different registers
        if (AOP_TYPE (result) == AOP_CRY)
        {
            // result = bit
            // if(size), result in bit
            // if(!size && ifx), conditional oper: if(left | right)
            symbol *tlbl = newiTempLabel (NULL);
            int sizer = max (AOP_SIZE (left), AOP_SIZE (right));
            HY08A_emitcode (";XXX ", " %s,%d", __FILE__, __LINE__);


            if (size)
                HY08A_emitcode (";XXX setb", "c");
            while (sizer--)
            {
                MOVA (aopGet (AOP (right), offset, FALSE, FALSE));
                HY08A_emitcode (";XXX orl", "a,%s", aopGet (AOP (left), offset, FALSE, FALSE));
                HY08A_emitcode (";XXX jnz", "%05d_DS_", labelKey2num (tlbl->key));
                offset++;
            }
            if (size)
            {
                CLRC;
                HY08A_emitcode ("", "%05d_DS_:", labelKey2num (tlbl->key));
                HY08A_outBitC (result);
            }
            else if (ifx)
                jmpTrueOrFalse (ifx, tlbl);
        }
		else
		{
			if (resultX)
				emitldpr(popGet(AOP(result), 0));
			for (; (size--); offset++)
			{
				// normal case
				// result = left | right
				if (AOP_TYPE(right) == AOP_LIT)
				{
					int t = (lit >> (offset * 8)) & 0x0FFL;
					switch (t)
					{
					case 0x00:
						emitpcode(POC_MVFW, popGet(AOP(left), offset));
						if (resultX)
							emitpcode(POC_MVWF, PCOP_POINC0);
						else
							emitpcode(POC_MVWF, popGet(AOP(result), offset));

						break;
					case 0xff: // also ff
						if (resultX)
							emitpcode(POC_SETF, PCOP_POINC0);
						else
							emitpcode(POC_SETF, popGet(AOP(result), offset));
						break;
					default:
						emitpcode(POC_MVL, popGetLit(t));
						emitpcode(POC_IORFW, popGet(AOP(left), offset));
						if (resultX)
							emitpcode(POC_MVWF, PCOP_POINC0);
						else
							emitpcode(POC_MVWF, popGet(AOP(result), offset));
					}
					continue;
				}

				// faster than result <- left, anl result,right
				// and better if result is SFR
				emitpcode(POC_MVFW, popGet(AOP(right), offset));
				emitpcode(POC_IORFW, popGet(AOP(left), offset));
				if (resultX)
					emitpcode(POC_MVWF, PCOP_POINC0);
				else
					emitpcode(POC_MVWF, popGet(AOP(result), offset));
			}
		}
    }

release:
    freeAsmop (left, NULL, ic, (RESULTONSTACK (ic) ? FALSE : TRUE));
    freeAsmop (right, NULL, ic, (RESULTONSTACK (ic) ? FALSE : TRUE));
    freeAsmop (result, NULL, ic, TRUE);
}

/*-----------------------------------------------------------------*/
/* genXor - code for xclusive or                                   */
/*-----------------------------------------------------------------*/
void
genXor (iCode * ic, iCode * ifx)
{
    operand *left, *right, *result;
    int size, offset = 0;
    unsigned long long lit = 0L;
	int resultX = 0;
	resolvedIfx rIfx;
	int i;

    FENTRY;
    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);

    aopOp ((left = IC_LEFT (ic)), ic, FALSE);
    aopOp ((right = IC_RIGHT (ic)), ic, FALSE);
    aopOp ((result = IC_RESULT (ic)), ic, TRUE);

	resolveIfx(&rIfx, ifx);

	resultX = IS_TRUE_SYMOP(IC_RESULT(ic)) && IN_XSPACE(SPEC_OCLS(OP_SYMBOL(IC_RESULT(ic))->etype));

    /* if left is a literal & right is not ||
       if left needs acc & right does not */
	assert(right->aop);
	assert(left->aop);
    if ((AOP_TYPE (left) == AOP_LIT && AOP_TYPE (right) != AOP_LIT) || (AOP_NEEDSACC (left) && !AOP_NEEDSACC (right) && AOP_TYPE(right) != AOP_LIT))
    {
        operand *tmp = right;
        right = left;
        left = tmp;
    }

    /* if result = right then exchange them */
    if (HY08A_sameRegs (AOP (result), AOP (right)))
    {
        operand *tmp = right;
        right = left;
        left = tmp;
    }

    /* if right is bit then exchange them */
    if (AOP_TYPE (right) == AOP_CRY && AOP_TYPE (left) != AOP_CRY)
    {
        operand *tmp = right;
        right = left;
        left = tmp;
    }
    if (AOP_TYPE (right) == AOP_LIT)
        lit = ullFromVal (AOP (right)->aopu.aop_lit);

	//if (ifx)
		//size = 0;
	//else
	size = AOP_SIZE (result);

    // if(bit ^ yy)
    // xx = bit ^ yy;
    if (AOP_TYPE (left) == AOP_CRY)
    {
        if (AOP_TYPE (right) == AOP_LIT)
        {
            // c = bit & literal;
            if (lit >> 1)
            {
				
                // lit>>1  != 0 => result = 1
                if (AOP_TYPE (result) == AOP_CRY)
                {
                    if (size)
                    {
                        emitpcode (POC_BSF, popGet (AOP (result), offset));
                        HY08A_emitcode ("setb", "%s", AOP (result)->aopu.aop_dir);
                    }
                    else if (ifx)
                        continueIfTrue (ifx);
                    goto release;
                }
				// result is not bit .. result = 1
                //HY08A_emitcode ("setb", "c");
				emitpcode(POC_MVL, popGetLit(1));
				emitpcode(POC_MVWF, popGet(AOP(result), 0));
				for (i = 1; i < size; i++)
					emitpcode(POC_CLRF, popGet(AOP(result), i));
				goto release;
            }
            else
            {
                // lit == (0 or 1)
                if (lit == 0)
                {
                    // lit == 0, result = left
                    if (size && HY08A_sameRegs (AOP (result), AOP (left)))
                        goto release;
                    HY08A_emitcode ("mov", "c,%s", AOP (left)->aopu.aop_dir);
					// move result to left
					// this should be optmized by upper compiler?
					if (AOP_TYPE(result) == AOP_CRY)
					{
						emitpcode(POC_BSF, popGet(AOP(result), 0));
						emitpcode(POC_BTSS, popGet(AOP(left), 0));
						emitpcode(POC_BCF, popGet(AOP(result), 0));
						goto release;
					}
					else
					{
						emitpcode(POC_CLRF, popGet(AOP(result), 0));
						emitpcode(POC_BTSZ, popGet(AOP(left), 0));
						emitpcode(POC_INF, popGet(AOP(result), 0));
						for (i = 1; i < size; i++)
							emitpcode(POC_CLRF, popGet(AOP(result), i));
						goto release;
					}
                }
                else
                {
                    // lit == 1, result = not(left)
                    if (size && HY08A_sameRegs (AOP (result), AOP (left)))
                    {
                        //emitpcode (POC_MVL, popGet);
                        //emitpcode (POC_XORF, popGet (AOP (result), offset));
						if (AOP_TYPE(result) == AOP_CRY)
						{

							emitpcode(POC_BTGF, popGet(AOP(result), offset));
							HY08A_emitcode("cpl", "%s", AOP(result)->aopu.aop_dir);
							goto release;
						}
						else // 
						{
							emitpcode(POC_CLRF, popGet(AOP(result), 0));
							emitpcode(POC_BTSS, popGet(AOP(left), 0));
							emitpcode(POC_INF, popGet(AOP(result), 0));
							for (i = 1; i < size; i++)
								emitpcode(POC_CLRF, popGet(AOP(result), i));
							goto release;
						}
                    }
                    else
                    {
                        assert (!"incomplete genXor");
						//emitpcode(POC_BTGF, popGet(AOP(result), offset));
                       /* HY08A_emitcode ("mov", "c,%s", AOP (left)->aopu.aop_dir);
                        HY08A_emitcode ("cpl", "c");
						emitpcode(POC_BCF, popGet(AOP(result), 0));
						emitpcode(POC_BTSS, popGet(AOP(left), 0));
						emitpcode(POC_BSF, popGet(AOP(result), 0));*/
                    }
                }
            }

        }
        else // right is not lit
        {
            // right != literal
            //symbol *tlbl = newiTempLabel (NULL);
			//symbol *tlbl2 = newiTempLabel(NULL);
            if (AOP_TYPE (right) == AOP_CRY)
            {
                // c = bit ^ bit;
                HY08A_emitcode ("mov", "c,%s", AOP (right)->aopu.aop_dir);
				// calculate to C
				emitCLRC;
				emitpcode(POC_BTSZ, popGet(AOP(left), 0));
				emitSETC;
				emitpcode(POC_BTSZ, popGet(AOP(right), 0));
				emitTGLC;
				if (AOP_TYPE(result) == AOP_CRY)
				{
					// C to result
					if (ifx)
					{
						genIfxJump(ifx, "C");
						goto release;
					}
					else
					{
						emitpcode(POC_BSF, popGet(AOP(result), 0));
						emitSKPC;
						emitpcode(POC_BCF, popGet(AOP(result), 0));
						goto release;
					}
				}
				emitpcode(POC_CLRF, popGet(AOP(result), 0));
				emitSKPNC;
				emitpcode(POC_INF, popGet(AOP(result), 0));
				for (i = 1; i < size; i++)
					emitpcode(POC_CLRF, popGet(AOP(result), i));
				goto release;


            }
            else // right is not bit, but left is bit
            {
                int sizer = AOP_SIZE (right);
				int ii;

				// if result is not bit, special operations taken

				if (AOP_TYPE(result) != AOP_CRY)
				{
					emitpcode(POC_MVL, popGetLit(1));
					emitpcode(POC_BTSS, popGet(AOP(left), 0));
					emitpcode(POC_MVL, popGetLit(0));
					for (ii = 0; ii < size; ii++)
					{
						if (ii >= sizer)
							emitpcode(POC_MVL, popGetLit(0xff));
						else
						{
							if (ii > 1)
								emitpcode(POC_MVL, popGetLit(0));
							emitpcode(POC_XORFW, popGet(AOP(right), ii));
						}
						emitpcode(POC_MVWF, popGet(AOP(result), ii));
					}
					goto release;
				}
				// following is result is cry can be ifx
                // this is tricky
				HY08A_toBoolean(right); // now Z is the flag inversed
				if (ifx)
				{
					emitSETC;
					emitSKPNZ;
					emitCLRC; // if zero, C will clear
					emitpcode(POC_BTSZ, popGet(AOP(left), 0)); // toggle if left is 1
					emitTGLC;
					genIfxJump(ifx, "C");
					goto release;
				}
				emitpcode(POC_BSF, popGet(AOP(result), 0));
				emitSKPNZ;
				emitpcode(POC_BCF, popGet(AOP(result), 0));
				emitpcode(POC_BTSZ, popGet(AOP(left), 0)); // toggle if left is 1
				emitpcode(POC_BTGF, popGet(AOP(result), 0));
				goto release;

            }
            //HY08A_emitcode ("jnb", "%s,%05d_DS_", AOP (left)->aopu.aop_dir, (labelKey2num (tlbl->key)));
            //HY08A_emitcode ("cpl", "c");
            //HY08A_emitcode ("", "%05d_DS_:", (labelKey2num (tlbl->key)));
        }
        // bit = c
        // val = c
        //if (size)
            //HY08A_outBitC (result);
        // if(bit | ...)
        //if ((AOP_TYPE (result) == AOP_CRY) && ifx)
            //genIfxJump (ifx, "c");
        goto release;
    }

    if (HY08A_sameRegs (AOP (result), AOP (left)))
    {
		// left==result will not be X
        /* if left is same as result */
        for (; size--; offset++)
        {
            if (AOP_TYPE (right) == AOP_LIT)
            {
                int t = (lit >> (offset * 8)) & 0x0FFL;
                int lastw = -1;
                if (t == 0x00L)
                    continue;
                else
                {
                    pCodeOp *pcop = popGet(AOP(left), offset);
                    pCodeOp *pcopb = newpCodeOpBitByOp(pcop, 0, 0); // change to by OP
                    memcpy(&(PCORB(pcopb)->pcor), PCOR(pcop), sizeof(pCodeOpReg));
                    PCORB(pcopb)->pcor.pcop.type = PO_GPR_BIT;
                    if (pcop->type == PO_GPR_TEMP)
                        PCORB(pcopb)->pcor.instance = 0;
                    PCORB(pcopb)->subtype = pcop->type;



                    switch (t)
                    {
                    case 1:
                        PCORB(pcopb)->bit = 0;
                        emitpcode(POC_BTGF, pcopb);
                        break;
                    case 2:
                        PCORB(pcopb)->bit = 1;
                        emitpcode(POC_BTGF, pcopb);
                        break;
                    case 4:
                        PCORB(pcopb)->bit = 2;
                        emitpcode(POC_BTGF, pcopb);
                        break;
                    case 8:
                        PCORB(pcopb)->bit = 3;
                        emitpcode(POC_BTGF, pcopb);
                        break;
                    case 0x10:
                        PCORB(pcopb)->bit = 4;
                        emitpcode(POC_BTGF, pcopb);
                        break;
                    case 0x20:
                        PCORB(pcopb)->bit = 5;
                        emitpcode(POC_BTGF, pcopb);
                        break;
                    case 0x40:
                        PCORB(pcopb)->bit = 6;
                        emitpcode(POC_BTGF, pcopb);
                        break;
                    case 0x80:
                        PCORB(pcopb)->bit = 7;
                        emitpcode(POC_BTGF, pcopb);
                        break;
                    case 0xff:
                        emitpcode(POC_COMF, popGet(AOP(result), offset));
                        break;
                    default:
                        if (lastw != t)
                        {
                            emitpcode(POC_MVL, popGetLit(t));
                            lastw = t;
                        }
                        emitpcode(POC_XORF, popGet(AOP(left), offset));
                    }

                }
            }
            else
            {
                emitpcode (POC_MVFW, popGet (AOP (right), offset));
                emitpcode (POC_XORF, popGet (AOP (left), offset));
            }
        }
    }
    else
    {
        // left & result in different registers
        if (AOP_TYPE (result) == AOP_CRY) // result is 1 bit ... if (aaa ^ bbb) ??? who use this ... shit!!
        {
            // result = bit
            // if(size), result in bit
            // if(!size && ifx), conditional oper: if(left ^ right)
            symbol *tlbl = newiTempLabel (NULL);
            int sizer = max (AOP_SIZE (left), AOP_SIZE (right));
            //if (size)
                //HY08A_emitcode ("setb", "c");
            while (sizer--)
            {
                if ((AOP_TYPE (right) == AOP_LIT) && (((lit >> (offset * 8)) & 0x0FFL) == 0x00L))
                {
                    //MOVA (aopGet (AOP (left), offset, FALSE, FALSE));
					emitpcode(POC_RRFW, popGet(AOP(left),offset));
					if (rIfx.condition)
					{
						emitSKPNZ;
						emitpcode(POC_JMP, popGetLabel(rIfx.lbl->key));
					}
					else
					{
						emitSKPZ;
						emitpcode(POC_JMP, popGetLabel(tlbl->key));
					}
                }
                else
                {
                    //MOVA (aopGet (AOP (right), offset, FALSE, FALSE));
                    //HY08A_emitcode ("xrl", "a,%s", aopGet (AOP (left), offset, FALSE, FALSE));
					mov2w(AOP(left), offset);
					if (op_isLitLike(right))
						emitpcode(POC_XORLW, popGetAddr(AOP(right), offset, 0));
					else
						emitpcode(POC_XORFW, popGetAddr(AOP(right), offset, 0));
					if (rIfx.condition)
					{
						emitSKPZ;
						emitpcode(POC_JMP, popGetLabel(rIfx.lbl->key));
					}
					else
					{
						emitSKPZ;
						emitpcode(POC_JMP, popGetLabel(tlbl->key));
					}


                }
                //HY08A_emitcode ("jnz", "%05d_DS_", labelKey2num (tlbl->key));
				//emitpcode(POC_TFSZ, PCOP(&pc_wreg));
				//emitpcode(POC_JMP, popGetLabel(tlbl->key));
                offset++;
            }
			if (!rIfx.condition)
			{
				emitpcode(POC_JMP, popGetLabel(rIfx.lbl->key));
			}
			emitpLabel(tlbl->key);
			ifx->generated = 1;
			// bit = left & literal
          /*  if (size)
            {
                CLRC;
                HY08A_emitcode ("", "%05d_DS_:", labelKey2num (tlbl->key));
                HY08A_outBitC (result);
            }

            else if (ifx)
                jmpTrueOrFalse (ifx, tlbl);*/
        }
		else
		{
			if (resultX)
				emitldpr(popGet(AOP(result), 0));
			for (; (size--); offset++)
			{
				// normal case
				// result = left & right
				if (AOP_TYPE(right) == AOP_LIT)
				{
					int t = (lit >> (offset * 8)) & 0x0FFL;
					switch (t)
					{
					case 0x00:
						emitpcode(POC_MVFW, popGet(AOP(left), offset));
						if (resultX)
							emitpcode(POC_MVWF, PCOP_POINC0);
						else
							emitpcode(POC_MVWF, popGet(AOP(result), offset));
						break;
					case 0xff:
						emitpcode(POC_COMFW, popGet(AOP(left), offset));
						if (resultX)
							emitpcode(POC_MVWF, PCOP_POINC0);
						else
							emitpcode(POC_MVWF, popGet(AOP(result), offset));
						break;
					default:
						emitpcode(POC_MVL, popGetLit(t));
						emitpcode(POC_XORFW, popGet(AOP(left), offset));
						if (resultX)
							emitpcode(POC_MVWF, PCOP_POINC0);
						else
							emitpcode(POC_MVWF, popGet(AOP(result), offset));
					}
					continue;
				}

				// faster than result <- left, anl result,right
				// and better if result is SFR
				emitpcode(POC_MVFW, popGet(AOP(right), offset));
				emitpcode(POC_XORFW, popGet(AOP(left), offset));
				if (resultX)
					emitpcode(POC_MVWF, PCOP_POINC0);
				else
					emitpcode(POC_MVWF, popGet(AOP(result), offset));
			}
		}
    }

release:
    freeAsmop (left, NULL, ic, (RESULTONSTACK (ic) ? FALSE : TRUE));
    freeAsmop (right, NULL, ic, (RESULTONSTACK (ic) ? FALSE : TRUE));
    freeAsmop (result, NULL, ic, TRUE);
}

/*-----------------------------------------------------------------*/
/* genInline - write the inline code out                           */
/*-----------------------------------------------------------------*/
// 2016 DEC: add C source information
// for easier debug
// 
static void
HY08A_genInline (iCode * ic)
{
    char *buffer, *bp, *bp1;
    bool inComment = FALSE;
	int lineno = ic->lineno;
	int lastl = -1;
	char *cp;
    FENTRY;
    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);

    genLine.lineElement.isInline += (!options.asmpeep);

    buffer = bp = bp1 = Safe_strdup (IC_INLINE (ic));
	for (cp = bp; *cp; cp++)
		if (*cp == '\n')
			lineno--;
	//lineno++; // skip first one
    while (*bp)
    {
        switch (*bp)
        {
        case ';':
            inComment = TRUE;
            ++bp;
            break;

        case '\x87':
        case '\n':
            inComment = FALSE;
            *bp++ = '\0';
			if (*bp1)
			{
				char buf[256];
				
				if (lastl != lineno)
				{
					SNPRINTF(buf, 255, ";\t;.line\t%d; \"%s\";\t..asm;$SEQ:%d", lineno, ic->filename,ic->seq);
					addpCode2pBlock(pb, newpCodeAsmDir(buf, NULL));
					lastl = lineno;
				}
				addpCode2pBlock(pb, newpCodeAsmDir(bp1, NULL));   // inline directly, no process
			}
            bp1 = bp;
			lineno++;
            break;

        default:
            /* Add \n for labels, not dirs such as c:\mydir */
            if (!inComment && (*bp == ':') && (isspace ((unsigned char) bp[1])))
            {
				char buf[256];
                ++bp;
                *bp = '\0';
                ++bp;
                /* print label, use this special format with NULL directive
                 * to denote that the argument should not be indented with tab */
				if (lastl != lineno)
				{
					SNPRINTF(buf, 255, ";\t;.line\t%d; \"%s\"\t ..asm;$SEQ:%d", lineno, ic->filename, ic->seq);
					addpCode2pBlock(pb, newpCodeAsmDir(buf, NULL));
					lastl = lineno;
				}
                addpCode2pBlock (pb, newpCodeAsmDir (NULL, bp1)); // inline directly, no process
                bp1 = bp;
            }
            else
                ++bp;
            break;
        }
    }
	if ((bp1 != bp) && *bp1)
	{
		char buf[256];
		if (lastl != lineno)
		{
			SNPRINTF(buf, 255, ";\t;.line\t%d; \"%s\"\t ..asm;$SEQ:%d", lineno, ic->filename, ic->seq);
			addpCode2pBlock(pb, newpCodeAsmDir(buf, NULL));
			lastl = lineno;
		}
		addpCode2pBlock(pb, newpCodeAsmDir(bp1, NULL));   // inline directly, no process
		
	}

    Safe_free (buffer);

    /* consumed; we can free it here */
    dbuf_free (IC_INLINE (ic));

    genLine.lineElement.isInline -= (!options.asmpeep);
}
static unsigned long long rleft(unsigned long long val, int size)
{
    return (val<<1)|((val&(1<<(size*8-1)))?1:0);
}
static unsigned long  rright(unsigned long long val, int size)
{
    return (val>>1)|((val&1)?(1<<(size*8-1)):0);
}

/*-----------------------------------------------------------------*/
/* genRRC - rotate right with carry                                */
/*-----------------------------------------------------------------*/
// it is real that SDCC comes out RRC operator!!
// in fact, it is ROL!!
static void
genRRC (iCode * ic)
{
    operand *left, *result;
    int size, offset = 0, same;
	int resultX = 0;
    unsigned long long val=0;

    FENTRY;
    /* rotate right with carry */
    left = IC_LEFT (ic);
    result = IC_RESULT (ic);
    aopOp (left, ic, FALSE);
    aopOp (result, ic, FALSE);

	assert(result);
	resultX = IS_TRUE_SYMOP(IC_RESULT(ic)) && IN_XSPACE(SPEC_OCLS(OP_SYMBOL(IC_RESULT(ic))->etype));

    DEBUGHY08A_AopType (__LINE__, left, NULL, result);

    same = HY08A_sameRegs (AOP (result), AOP (left));

    size = AOP_SIZE (result);

    /* get the lsb and put it into the carry */
    if(IS_OP_LITERAL(left))
    {
        val = rright(operandLitValueUll(left),size);
    }


	if (size == 1) // we have special operator
	{
		if (resultX)
			emitldpr(popGet(AOP(result), 0));
		if(same)
			emitpcode(POC_RRF, popGet(AOP(left), 0));
		else
		{
            if(IS_OP_LITERAL(left))
            {
                emitpcode(POC_MVL, popGetLit(val&0xff));
            }else if(op_isLitLike(left))
            {
                emitpcode(POC_MVL, popGet(AOP(left),0));
                emitpcode(POC_RRFW, (pCodeOp*)&pc_wreg);
            }else
            {
			    emitpcode(POC_RRFW, popGet(AOP(left), 0));
            }
			if (!resultX)
				emitpcode(POC_MVWF, popGet(AOP(result), 0));
			else
				emitpcode(POC_MVWF, PCOP_POINC0);
		}
		goto release;
	}

	if (resultX)
		emitldpr(popGet(AOP(result), size-1)); // max one

	emitCLRC;
    //emitpcode (POC_RRCFW, popGet (AOP (left), size - 1));

	for(offset=size-1;offset>=0;offset--)
    {

        if (same)
        {
            emitpcode (POC_RRCF, popGet (AOP (left), offset));
        }
        else
        {
            if(IS_OP_LITERAL(left))
            {
                emitpcode(POC_MVL, popGetLit((val>>(offset*8))&0xff));
            }else if(op_isLitLike(left))
            {
                emitpcode(POC_MVL, popGet(AOP(left),offset));
                emitpcode(POC_RRCFW, (pCodeOp*)&pc_wreg);
            }else 
            {
                emitpcode (POC_RRCFW, popGet (AOP (left), offset));
            }
			if (resultX)
			{
				if(size==1)
					emitpcode(POC_MVWF, PCOP_INDF0);
				else
					emitpcode(POC_MVWF, PCOP_PODEC0);

			}
			else
				emitpcode(POC_MVWF, popGet(AOP(result), offset));
        }

        
    }
	if (resultX)
		emitldpr(popGet(AOP(result), size - 1));
	emitSKPNC;
	if (resultX)
	{
		
		emitpcode(POC_BSF, newpCodeOpBit("_INDF0", 7, 0));
	}
	else
		emitpcode(POC_BSF, newpCodeOpBit(aopGet(AOP(left), size-1, FALSE, FALSE), 7, 0));
	
release:
    freeAsmop (left, NULL, ic, TRUE);
    freeAsmop (result, NULL, ic, TRUE);
}

/*-----------------------------------------------------------------*/
/* genRLC - generate code for rotate left with carry               */
/*-----------------------------------------------------------------*/
// this is ROL !!
static void
genRLC (iCode * ic)
{
	operand *left, *result;
	int size, offset = 0, same;
	int resultX = 0;
    unsigned long long val=0;

	FENTRY;
	/* rotate left with carry */
	left = IC_LEFT(ic);
	result = IC_RESULT(ic);
	aopOp(left, ic, FALSE);
	aopOp(result, ic, FALSE);

	assert(result);
	resultX = IS_TRUE_SYMOP(IC_RESULT(ic)) && IN_XSPACE(SPEC_OCLS(OP_SYMBOL(IC_RESULT(ic))->etype));

	DEBUGHY08A_AopType(__LINE__, left, NULL, result);

	same = HY08A_sameRegs(AOP(result), AOP(left));

	size = AOP_SIZE(result);

	/* get the lsb and put it into the carry */

	if (size == 1) // we have special operator
	{
		if (resultX)
			emitldpr(popGet(AOP(result), 0));
		if (same)
			emitpcode(POC_RLF, popGet(AOP(left), 0));
		else
		{
            if(IS_OP_LITERAL(left))
            {
                val = rleft(operandLitValueUll (left),1);
                emitpcode(POC_MVL, popGetLit((int)val&0xff));
            }else if(op_isLitLike(left))
            {
                emitpcode(POC_MVL, popGet(AOP(left),0));
                emitpcode(POC_RLFW, (pCodeOp*)&pc_wreg);
            }else 
            {
			    emitpcode(POC_RLFW, popGet(AOP(left), 0));
            }
			if (!resultX)
				emitpcode(POC_MVWF, popGet(AOP(result), 0));
			else
				emitpcode(POC_MVWF, PCOP_POINC0);
		}
		goto release;
	}

	if (resultX)
		emitldpr(popGet(AOP(result), 0)); // max one

	emitCLRC;
	//emitpcode (POC_RRCFW, popGet (AOP (left), size - 1));
    if(IS_OP_LITERAL(left))
        val=operandLitValueUll (left);


	for (offset = 0; offset <size; offset++)
	{

		if (same)
		{
			emitpcode(POC_RLCF, popGet(AOP(left), offset));
		}
		else
		{
            if(IS_OP_LITERAL(left))
            {
                emitpcode(POC_MVL, popGetLit((rleft(val,size)>>(offset*8))&0xff));

            }else if(op_isLitLike(left))
            {
                emitpcode(POC_MVL, popGetLit((val>>(offset*8))&0xff));
                emitpcode(POC_RLCFW, (pCodeOp*)&pc_wreg);    
            }else 
            {
    			emitpcode(POC_RLCFW, popGet(AOP(left), offset));
            }
			if (resultX)
			{
				if (size == 1)
					emitpcode(POC_MVWF, PCOP_INDF0);
				else
					emitpcode(POC_MVWF, PCOP_POINC0);

			}
			else
				emitpcode(POC_MVWF, popGet(AOP(result), offset));
		}


	}
	if (resultX)
		emitldpr(popGet(AOP(result), 0));
	emitSKPNC;
	if (resultX)
	{

		emitpcode(POC_BSF, newpCodeOpBit("_INDF0", 0, 0));
	}
	else
		emitpcode(POC_BSF, newpCodeOpBit(aopGet(AOP(result), 0, FALSE, FALSE), 0, 0));

release:
	freeAsmop(left, NULL, ic, TRUE);
	freeAsmop(result, NULL, ic, TRUE);
}

static void
genGetABit (iCode * ic)
{
    operand *left, *right, *result;
    int shCount;
    int offset;
    int i;

	int resultX = 0;

    left = IC_LEFT (ic);
    right = IC_RIGHT (ic);
    result = IC_RESULT (ic);

	assert(result);
	assert(left);
	assert(right);
	resultX = IS_TRUE_SYMOP(IC_RESULT(ic)) && IN_XSPACE(SPEC_OCLS(OP_SYMBOL(IC_RESULT(ic))->etype));

    aopOp (left, ic, FALSE);
    aopOp (right, ic, FALSE);
    aopOp (result, ic, TRUE);

    shCount = (int) ulFromVal (AOP (right)->aopu.aop_lit);
    offset = shCount / 8;
    shCount %= 8;

    /* load and mask the source byte */
    mov2w (AOP (left), offset);
    emitpcode (POC_ANDLW, popGetLit (1 << shCount));

	LDPRRESULTIFRESULTX;

    /* move selected bit to bit 0 */
    switch (shCount)
    {
    case 0:
        /* nothing more to do */
        break;
    default:
        /* keep W==0, force W=0x01 otherwise */
        emitSKPZ;
        emitpcode (POC_MVL, popGetLit (1));
        break;
    }                           // switch

    /* write result */
	if (resultX)
		emitpcode(POC_MVWF, PCOP_POINC0);
	else
		emitpcode (POC_MVWF, popGet (AOP (result), 0));

    for (i = 1; i < AOP_SIZE (result); ++i)
    {
		if (resultX)
			emitpcode(POC_CLRF, PCOP_POINC0);
		else
			emitpcode(POC_CLRF, popGet(AOP(result), i));
    }                           // for

    freeAsmop (left, NULL, ic, TRUE);
    freeAsmop (right, NULL, ic, TRUE);
    freeAsmop (result, NULL, ic, TRUE);
}

/*-----------------------------------------------------------------*/
/* genGetHbit - generates code get highest order bit               */
/*-----------------------------------------------------------------*/
// static void
// genGetHbit (iCode * ic)
// {
//     operand *left, *result;
// 	//int resultX = 0;
//     left = IC_LEFT (ic);
//     result = IC_RESULT (ic);
//     aopOp (left, ic, FALSE);
//     aopOp (result, ic, FALSE);
// 	//ASSIGNRESULTX;

//     FENTRY;
// 	fprintf(stderr, "internal err, gethbit not implemented. at %s:%d\n", ic->filename, ic->lineno);
// 	exit(1007);

//     //DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);
//     ///* get the highest order byte into a */
//     //MOVA (aopGet (AOP (left), AOP_SIZE (left) - 1, FALSE, FALSE));
//     //if (AOP_TYPE (result) == AOP_CRY)
//     //{
//     //    HY08A_emitcode ("rlc", "a");
//     //    HY08A_outBitC (result);
//     //}
//     //else
//     //{
//     //    HY08A_emitcode ("rl", "a");
//     //    HY08A_emitcode ("anl", "a,#0x01");
//     //    HY08A_outAcc (result);
//     //}


//     //freeAsmop (left, NULL, ic, TRUE);
//     //freeAsmop (result, NULL, ic, TRUE);
// }

/*-----------------------------------------------------------------*/
/* AccLsh - shift left accumulator by known count                  */
/* MARK: HY08A always rotates through CARRY!                       */
/*-----------------------------------------------------------------*/
static void
AccLsh (pCodeOp * pcop, int shCount)
{
	// this operation will not have X
    FENTRY;
    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);
    shCount &= 0x0007;            // shCount : 0..7
    switch (shCount)
    {
    case 0:
        return;
        break;
    case 1:
        //emitCLRC;
        emitpcode (POC_RLF, pcop);
        return;
        break;
    case 2:
        emitpcode (POC_RLF, pcop);
        emitpcode (POC_RLF, pcop);
        break;
    case 3:
        emitpcode (POC_RLF, pcop);
        emitpcode (POC_RLF, pcop);
        emitpcode (POC_RLF, pcop);
        break;
    case 4:
        emitpcode (POC_SWPF, pcop);
        break;
    case 5:
        emitpcode (POC_SWPF, pcop);
        emitpcode (POC_RLF, pcop);
        break;
    case 6:
        emitpcode (POC_SWPF, pcop);
        emitpcode (POC_RLF, pcop);
        emitpcode (POC_RLF, pcop);
        break;
    case 7:
        emitpcode (POC_RRF, pcop);
        emitpcode (POC_RRF, pcop);
        break;
    }
    /* clear invalid bits */
    //emitpcode (POC_MVL, popGetLit ((unsigned char) (~((1UL << shCount) - 1))));
    //emitpcode (POC_ANDWF, pcop);
}

/*-----------------------------------------------------------------*/
/* AccRsh - shift right accumulator by known count                 */
/* MARK: HY08A always rotates through CARRY!                       */
/* maskmode - 0: leave invalid bits undefined (caller should mask) */
/*            1: mask out invalid bits (zero-extend)               */
/*            2: sign-extend result (pretty slow)                  */
/*-----------------------------------------------------------------*/
static void
AccRsh (pCodeOp * pcop, int shCount, int mask_mode)
{
    FENTRY;
    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);
    shCount &= 0x0007;            // shCount : 0..7
    switch (shCount)
    {
    case 0:
        return;
        break;
    case 1:
        /* load sign if needed */
        if (mask_mode == 2)
            emitpcode (POC_RLCFW, pcop);
        //else if (mask_mode == 1)
        //    emitCLRC;
        emitpcode (POC_RRF, pcop);
        return;
        break;
    case 2:
        /* load sign if needed */
		if (mask_mode == 2)
		{
			emitpcode(POC_RLCFW, pcop);
			emitpcode(POC_RRCF, pcop);
		}else
			emitpcode(POC_RRF, pcop);
        /* load sign if needed */
		if (mask_mode == 2)
		{
			emitpcode(POC_RLCFW, pcop);
			emitpcode(POC_RRCF, pcop);
		}
		else
			emitpcode(POC_RRF, pcop);
        if (mask_mode)
            return;
        break;
    case 3:
		if (mask_mode == 2)
		{
			emitpcode(POC_RLCFW, pcop);
			emitpcode(POC_RRCF, pcop);
		}
		else
			emitpcode(POC_RRF, pcop);
		/* load sign if needed */
		if (mask_mode == 2)
		{
			emitpcode(POC_RLCFW, pcop);
			emitpcode(POC_RRCF, pcop);
		}
		else
			emitpcode(POC_RRF, pcop);
		if (mask_mode == 2)
		{
			emitpcode(POC_RLCFW, pcop);
			emitpcode(POC_RRCF, pcop);
		}
		else
			emitpcode(POC_RRF, pcop);
		if (mask_mode)
			return;
        break;
    case 4:
        emitpcode (POC_SWPF, pcop);
        break;
    case 5:
        emitpcode (POC_SWPF, pcop);
        emitpcode (POC_RRF, pcop);
        break;
    case 6:
        emitpcode (POC_SWPF, pcop);
        emitpcode (POC_RRF, pcop);
        emitpcode (POC_RRF, pcop);
        break;
    case 7:
        if (mask_mode == 2)
        {
            /* load sign */
            emitpcode (POC_RLCFW, pcop);
            emitpcode (POC_CLRF, pcop);
            emitSKPNC;
            emitpcode (POC_COMF, pcop);
            return;
        }
        else
        {
            //emitpcode (POC_RLCFW, pcop);
            emitpcode (POC_RLF, pcop);
        }
        break;
    }

    if (mask_mode == 0)
    {
        /* leave invalid bits undefined */
        return;
    }

    ///* clear invalid bits -- zero-extend */
    //emitpcode (POC_MVL, popGetLit (0x00ff >> shCount));
    //emitpcode (POC_ANDWF, pcop);

    if (mask_mode == 2)
    {
        /* sign-extend */
        emitpcode (POC_MVL, popGetLit (0x00ff << (8 - shCount)));
        emitpcode (POC_BTSZ, newpCodeOpBit (get_op (pcop, NULL, 0), 7 - shCount, 0));
        emitpcode (POC_IORWF, pcop);
    }
}

/*-----------------------------------------------------------------*/
/* movLeft2Result - move byte from left to result                  */
/*-----------------------------------------------------------------*/
static void
movLeft2Result (operand * left, int offl, operand * result, int offr)
{
	int resultX = 0;
    FENTRY;
	resultX = IS_TRUE_SYMOP(result) && IN_XSPACE(SPEC_OCLS(OP_SYMBOL(result)->etype));
    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);
	assert(result);
	assert(left);
	assert(left->aop);
	assert(result->aop);
    if (!HY08A_sameRegs (AOP (left), AOP (result)) || (offl != offr))
    {
        aopGet (AOP (left), offl, FALSE, FALSE);

		if (resultX)
		{
			emitpcode22(POC_MVFF, popGet2(AOP(left), offl,1), popGet2(AOP(result), offr,1));
		}
		else
		{
			emitpcode(POC_MVFW, popGet(AOP(left), offl));

			emitpcode(POC_MVWF, popGet(AOP(result), offr));
		}
    }
}

/*-----------------------------------------------------------------*/
/* shiftLeft_Left2ResultLit - shift left by known count            */
/*-----------------------------------------------------------------*/

//static void
//shiftLeft_Left2ResultLit (operand * left, operand * result, int shCount)
//{
//  int sizel, sizer, same, offr, i;
//
//  sizel  = AOP_SIZE (left);
//  sizer = AOP_SIZE(result);
//
//  if (sizel > sizer) sizel = sizer; // size l is the bytes we shift
//  // special case is l < r
//
//  same = HY08A_sameRegs (AOP (left), AOP (result));
//
//  offr = shCount / 8;
//  shCount = shCount & 0x07;
//
//  sizel -= offr;
//
//  switch (shCount)
//    {
//    case 0:                    /* takes 0 or 2N cycles (for offr==0) */
//      if (!same || offr)
//        {
//          for (i = sizel - 1; i >= 0; i--)
//            movLeft2Result (left, i, result, offr + i);
//
//        }                       // if
//      break;
//
//    case 1:                    /* takes 1N+1 or 2N+1 cycles (or offr==0) */
//      if (same && offr)
//        {
//          shiftLeft_Left2ResultLit (left, result, 8 * offr);
//          shiftLeft_Left2ResultLit (result, result, shCount);
//          return;               /* prevent clearing result again */
//        }
//      else
//        {
//          emitCLRC;
//          for (i = 0; i < sizel; i++)
//            {
//              if (same && !offr)
//                {
//                  emitpcode (POC_RLCF, popGet (AOP (left), i));
//                }
//              else
//                {
//                  emitpcode (POC_RLCFW, popGet (AOP (left), i));
//                  emitpcode (POC_MVWF, popGet (AOP (result), i + offr));
//                }               // if
//            }                   // for
//
//        }                       // if (offr)
//      break;
//
//    case 4:                    /* takes 3+5(N-1) = 5N-2 cycles (for offr==0) */
//      /* works in-place/with offr as well */
//      emitpcode (POC_SWPFW, popGet (AOP (left), sizel - 1));
//      emitpcode (POC_ANDLW, popGetLit (0xF0));
//      emitpcode (POC_MVWF, popGet (AOP (result), sizel - 1 + offr));
//
//      for (i = sizel - 2; i >= 0; i--)
//        {
//          emitpcode (POC_SWPFW, popGet (AOP (left), i));
//          emitpcode (POC_MVWF, popGet (AOP (result), i + offr));
//          emitpcode (POC_ANDLW, popGetLit (0x0F));
//          emitpcode (POC_IORWF, popGet (AOP (result), i + offr + 1));
//          emitpcode (POC_XORF, popGet (AOP (result), i + offr));
//        }                       // for i
//
//      break;
//
//    case 7:                    /* takes 2(N-1)+3 = 2N+1 cycles */
//      /* works in-place/with offr as well */
//      emitpcode (POC_RRCFW, popGet (AOP (left), sizel - 1));
//      for (i = sizel - 2; i >= 0; i--)
//        {
//          emitpcode (POC_RRCFW, popGet (AOP (left), i));
//          emitpcode (POC_MVWF, popGet (AOP (result), offr + i + 1));
//        }                       // for i
//      emitpcode (POC_CLRF, popGet (AOP (result), offr));
//      emitpcode (POC_RRCF, popGet (AOP (result), offr));
//
//      break;
//
//    default:
//      shiftLeft_Left2ResultLit (left, result, offr * 8 + shCount - 1);
//      shiftLeft_Left2ResultLit (result, result, 1);
//      return;                   /* prevent clearing result again */
//      break;
//    }                           // switch
//
//  while (0 < offr--)
//    {
//      emitpcode (POC_CLRF, popGet (AOP (result), offr));
//    }
//  for (i = sizel; i < sizer; i++)
//	  emitpcode(POC_CLRF, popGet(AOP(result), i++));
//}
static void
shiftLeft_Left2ResultLit(operand * left, operand * result, int shCount)
{
    int size, same, offr, i;
	int resultX = 0;
	assert(left);
    size = AOP_SIZE(left);

	assert(result);
    if (AOP_SIZE(result) < size)
        size = AOP_SIZE(result);
	resultX = IS_TRUE_SYMOP(result) && IN_XSPACE(SPEC_OCLS(OP_SYMBOL(result)->etype));

    same = HY08A_sameRegs(AOP(left), AOP(result));

    offr = shCount / 8;
    shCount = shCount & 0x07;
    if(IS_OP_LITERAL(left))// special special case
    {
        unsigned long long val = operandLitValueUll(left)<<shCount;
        for(i=0;i<size;i++)
        {
            emitpcode(POC_MVL, popGetLit(((val>>(i*8)))&0xff));
            emitpcode(POC_MVWF, popGet(AOP(result),i));
        }
        return;
    }
    size -= offr;

    switch (shCount)
    {
    case 0:                    /* takes 0 or 2N cycles (for offr==0) */
        if (!same || offr)
        {
            for (i = size - 1; i >= 0; i--)
                movLeft2Result(left, i, result, offr + i);
        }                       // if
        break;

    case 1:                    /* takes 1N+1 or 2N+1 cycles (or offr==0) */
        if (same && offr)
        {
            shiftLeft_Left2ResultLit(left, result, 8 * offr);
            shiftLeft_Left2ResultLit(result, result, shCount);
            return;               /* prevent clearing result again */
        }
        else
        {
            emitCLRC;
            for (i = 0; i < size; i++)
            {
                if (same && !offr)
                {
                    emitpcode(POC_RLCF, popGet(AOP(left), i));
                }
                else
                {
                    emitpcode(POC_RLCFW, popGet(AOP(left), i));
					if (resultX)
					{
						emitpcode22(POC_MVFF, PCOP_W, popGet2(AOP(result), i + offr,1));
					}
					else
						emitpcode(POC_MVWF, popGet(AOP(result), i + offr));
                }               // if
            }                   // for
        }                       // if (offr)
        break;

    case 4:                    /* takes 3+5(N-1) = 5N-2 cycles (for offr==0) */
        /* works in-place/with offr as well */
        emitpcode(POC_SWPFW, popGet(AOP(left), size - 1));
        emitpcode(POC_ANDLW, popGetLit(0xF0));
		// X should be complex
		if (resultX)
		{
			//emitpcode(POC_MVFF, PCOP_W, popGet(AOP(result), size - 1 + offr));
			emitldpr(popGet(AOP(result), size - 1 + offr));
			emitpcode(POC_MVWF, PCOP_PODEC0); // next is -2+offr
		}
		else
			emitpcode(POC_MVWF, popGet(AOP(result), size - 1 + offr)); // size-1+offr

        for (i = size - 2; i >= 0; i--)
        {
            emitpcode(POC_SWPFW, popGet(AOP(left), i));
			if (resultX)
				emitpcode(POC_MVWF, PCOP_POINC0);
			else
				emitpcode(POC_MVWF, popGet(AOP(result), i + offr)); // start from size-2+offr
            emitpcode(POC_ANDLW, popGetLit(0x0F));
			if (resultX)
			{
				emitpcode(POC_IORWF, PCOP_PODEC0);
				emitpcode(POC_XORF, PCOP_PODEC0);
			}
			else
			{
				emitpcode(POC_IORWF, popGet(AOP(result), i + offr + 1));
				emitpcode(POC_XORF, popGet(AOP(result), i + offr));
			}
        }                       // for i
        break;

    case 7:                    /* takes 2(N-1)+3 = 2N+1 cycles */
        /* works in-place/with offr as well */
        emitpcode(POC_RRCFW, popGet(AOP(left), size - 1));
		if (resultX)
			emitldpr(popGet(AOP(result), size- 1));
        for (i = size - 2; i >= 0; i--)
        {
            emitpcode(POC_RRCFW, popGet(AOP(left), i));
			if (resultX)
				emitpcode(POC_MVWF, PCOP_PODEC0);
			else
				emitpcode(POC_MVWF, popGet(AOP(result), offr + i + 1));
        }                       // for i
		if (resultX)
		{
			emitpcode(POC_CLRF, PCOP_INDF0);
			emitpcode(POC_RRCF, PCOP_INDF0);

		}
		else
		{
			emitpcode(POC_CLRF, popGet(AOP(result), offr));
			emitpcode(POC_RRCF, popGet(AOP(result), offr));
		}
        break;

    default:
        shiftLeft_Left2ResultLit(left, result, offr * 8 + shCount - 1);
        shiftLeft_Left2ResultLit(result, result, 1);
        return;                   /* prevent clearing result again */
        break;
    }                           // switch

	if (offr > 0 && resultX)
		emitldpr(popGet(AOP(result), offr));
    while (0 < offr--)
    {
		if (resultX)
			emitpcode(POC_CLRF, PCOP_PODEC0);
		else
			emitpcode(POC_CLRF, popGet(AOP(result), offr));
    }                           // while
}


/*-----------------------------------------------------------------*/
/* shiftRight_Left2ResultLit - shift right by known count          */
/*-----------------------------------------------------------------*/

static void
shiftRight_Left2ResultLit (operand * left, operand * result, int shCount, int sign, int skiphead)
{
    int size, same, offr, i;
	int resultX = 0;
	int size_result=AOP_SIZE(result);
	int size_left = AOP_SIZE(left);

    size = AOP_SIZE (left);
    if (AOP_SIZE (result) < size)
        size = AOP_SIZE (result);

    //if(shCount==18)
      //  fprintf(stderr,"%s\n","18");

    same = HY08A_sameRegs (AOP (left), AOP (result));
	resultX = IS_TRUE_SYMOP(result) && IN_XSPACE(SPEC_OCLS(OP_SYMBOL(result)->etype));
    offr = shCount / 8;
    shCount = shCount & 0x07;


    if(IS_OP_LITERAL(left))// special special case
    {
        unsigned long long val = operandLitValueUll(left)>>shCount;
        for(i=0;i<size;i++)
        {
            emitpcode(POC_MVL, popGetLit(((val>>(i*8)))&0xff));
            emitpcode(POC_MVWF, popGet(AOP(result),i));
        }
        return;
    }

    size -= offr;

	// special case, for SYNC
	//if (offr > 0 && IS_SYMOP(left) && OP_SYMBOL(left)->rname)
    if (offr > 0 && IS_SYMOP(left) && OP_SYMBOL(left))
	{
		if (!strcmp(OP_SYMBOL(left)->rname, "_ADCR")
			|| !strcmp(OP_SYMBOL(left)->rname, "_ADCO1")
			|| !strcmp(OP_SYMBOL(left)->rname, "_ADCO2")
			)
		{
			emitpcode(POC_MVFW, popGet(AOP(left), 0));
		}

	}
    // if(offr ) // shift with offr signed first
    // {
    //     for (i = 0; i < size; i++)
    //         movLeft2Result (left, i + offr, result, i);
    //     addSign (result, size, sign); // add sign first?
    //     left=result;
    //     same=1; offr=0;            
    // }

    if (size)
    {
        switch (shCount)
        {
        case 0:                /* takes 0 or 2N cycles (for offr==0) */
            if (!same || offr)
            {
                for (i = 0; i < size; i++)
                    movLeft2Result (left, i + offr, result, i);
            }                   // if
            break;

        case 1:                /* takes 1N+1(3) or 2N+1(3) cycles (or offr==0) */
            emitpComment ("%s:%d: shCount=%d, size=%d, sign=%d, same=%d, offr=%d", __FUNCTION__, __LINE__, shCount, size, sign,
                          same, offr);
            if (same && offr)
            {
                shiftRight_Left2ResultLit (left, result, 8 * offr, sign,(offr>1)?(offr-1):skiphead);
                shiftRight_Left2ResultLit (result, result, shCount, sign, (offr>1)?(offr-1):skiphead);
                return;           /* prevent sign-extending result again */
            }
            else if(same && skiphead)
            {
                if(!sign)
                    emitCLRC;
                if (resultX)
					emitldpr(popGet(AOP(result), size - skiphead));
                for (i = size - skiphead-1; i >= 0; i--)
                {
                    {
						assert(result);
                        if (i == size - skiphead-1)
                        {
                            if(sign)
                                emitpcode(POC_ARRCFW, popGet(AOP(left), i + offr));
                            else
                                emitpcode(POC_RRCFW, popGet(AOP(left), i + offr));
                        }
                        else
                            emitpcode (POC_RRCFW, popGet (AOP (left), i + offr));

						if (resultX)
							emitpcode(POC_MVWF, PCOP_PODEC0);
						else
							emitpcode(POC_MVWF, popGet(AOP(result), i));
                    }
                }               // for i
                             // if (offr)
            }else 
            {
                if(!sign)
                    emitCLRC;
                /*emitCLRC;
                if (sign)
                  {
                    emitpcode (POC_BTSZ, newpCodeOpBit (aopGet (AOP (left), AOP_SIZE (left) - 1, FALSE, FALSE), 7, 0));
                    emitSETC;
                  }*/
                
				if (resultX)
					emitldpr(popGet(AOP(result), size - 1));
                for (i = size - 1; i >= 0; i--)
                {
                    if (same && !offr)
                    {
                        if (i == size - 1)
                        {
                            if(sign)
                                emitpcode(POC_ARRCF, popGet(AOP(left), i));
                            else
                                emitpcode(POC_RRCF, popGet(AOP(left), i));
                        }
                        else
                            emitpcode (POC_RRCF, popGet (AOP (left), i));

                    }
                    else
                    {
						assert(result);
                        if (i == size - 1)
                        {
                            if(sign)
                                emitpcode(POC_ARRCFW, popGet(AOP(left), i + offr));
                            else
                                emitpcode(POC_RRCFW, popGet(AOP(left), i + offr));
                        }
                        else
                            emitpcode (POC_RRCFW, popGet (AOP (left), i + offr));

						if (resultX)
							emitpcode(POC_MVWF, PCOP_PODEC0);
						else
							emitpcode(POC_MVWF, popGet(AOP(result), i));
                    }
                }               // for i
            }                   // if (offr)
            break;

        case 4:                /* takes 3(6)+5(N-1) = 5N-2(+1) cycles (for offr==0) */
            /* works in-place/with offr as well */
			assert(result);
            emitpcode (POC_SWPFW, popGet (AOP (left), offr));
            emitpcode (POC_ANDLW, popGetLit (0x0F));
			if (resultX)
			{
				emitldpr(popGet(AOP(result), 0));
				emitpcode(POC_MVWF, PCOP_POINC0);// next i=1
			}
			else
				emitpcode(POC_MVWF, popGet(AOP(result), 0));

            for (i = 1; i < size; i++)
            {
                emitpcode (POC_SWPFW, popGet (AOP (left), i + offr));
				if (resultX)
					emitpcode(POC_MVWF, PCOP_PODEC0);
				else
					emitpcode(POC_MVWF, popGet(AOP(result), i));
                emitpcode (POC_ANDLW, popGetLit (0xF0));
				if (resultX)
				{
					emitpcode(POC_IORWF, PCOP_POINC0);
					if(i==size-1)
						emitpcode(POC_XORF, PCOP_INDF0);
					else
						emitpcode(POC_XORF, PCOP_POINC0);
				}
				else
				{
					emitpcode(POC_IORWF, popGet(AOP(result), i - 1));
					emitpcode(POC_XORF, popGet(AOP(result), i));
				}
            }                   // for i

            if (sign)
            {
                emitpcode (POC_MVL, popGetLit (0xF0));
                emitpcode (POC_BTSZ, newpCodeOpBit (aopGet (AOP (result), size - 1, FALSE, FALSE), 3, 0));
				if (resultX)
					emitpcode(POC_IORWF, PCOP_INDF0);
				else
					emitpcode (POC_IORWF, popGet (AOP (result), size - 1));
            }                   // if
            break;

        case 7:                /* takes 2(N-1)+3(4) = 2N+1(2) cycles */
            /* works in-place/with offr as well */
			assert(result);
			if (resultX)
				emitldpr(popGet(AOP(result), 0));
				
				

            emitpcode (POC_RLCFW, popGet (AOP (left), offr));
            for (i = 0; i < size - 1; i++)
            {
                emitpcode (POC_RLCFW, popGet (AOP (left), offr + i + 1));
				if (resultX)
					emitpcode(POC_MVWF, PCOP_POINC0);
				else
					emitpcode(POC_MVWF, popGet(AOP(result), i));
            }                   // for i
			if (resultX)
				emitpcode(POC_CLRF, PCOP_INDF0);
			else
				emitpcode(POC_CLRF, popGet(AOP(result), size - 1));
            if (!sign)
            {
				if (resultX)
					emitpcode(POC_RLCF, PCOP_INDF0);
				else
					emitpcode (POC_RLCF, popGet (AOP (result), size - 1));
            }
            else
            {
                emitSKPNC;
				if (resultX)
					emitpcode(POC_DECF, PCOP_INDF0);
				else
					emitpcode (POC_DECF, popGet (AOP (result), size - 1));
            }
            break;

        default:
            shiftRight_Left2ResultLit (left, result, offr * 8 + shCount - 1, sign,(offr>1)?(offr-1):skiphead);
            shiftRight_Left2ResultLit (result, result, 1, sign,(offr>1)?(offr-1):skiphead);
            return;               /* prevent sign extending result again */
            break;
        }                       // switch
    }                           // if
	if (size <= 0 ) // special case, like -100 >>8
	{
		int i;
		emitpcode(POC_MVL, popGetLit(0x0));
		if (sign)
		{
			emitpcode(POC_BTSZ, newpCodeOpBit(aopGet(AOP(left), size_left-1, FALSE, FALSE), 7, 0));
			emitpcode(POC_MVL, popGetLit(0xff));
		}
		for (i = 0; i < size_result; i++)
		{
			if (resultX)
				emitpcode(POC_MVWF, PCOP_POINC0);
			else
				emitpcode(POC_MVWF, popGet(AOP(result), i));
		}
	} else
		addSign (result, size, sign);
}

/*-----------------------------------------------------------------*
* genMultiAsm - repeat assembly instruction for size of register.
* if endian == 1, then the high byte (i.e base address + size of
* register) is used first else the low byte is used first;
*-----------------------------------------------------------------*/
static void
genMultiAsm (HYA_OPCODE poc, operand * reg, int size, int endian, int ifsign)
{

    int offset = 0;
    int size1 = size;

    FENTRY;
    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);

    if (!reg)
        return;

    if (!endian)
    {
        endian = 1;
    }
    else
    {
        endian = -1;
        offset = size - 1;
    }

    while (size--)
    {
        if(ifsign && size>=(size1-1) && poc == POC_RRCF)
            emitpcode(POC_ARRCF, popGet(AOP(reg), offset));
        else
            emitpcode (poc, popGet (AOP (reg), offset));
        offset += endian;
    }

}



/*-----------------------------------------------------------------*/
/* genRot - generates code for rotation                            */
/*-----------------------------------------------------------------*/
static void
genRot (iCode *ic)
{
  operand *left = IC_LEFT (ic);
  operand *right = IC_RIGHT (ic);
  unsigned int lbits = bitsForType (operandType (left));
  if (IS_OP_LITERAL (right) && operandLitValueUll (right) % lbits == 1)
    genRLC (ic);
  else if (IS_OP_LITERAL (right) && operandLitValueUll (right) % lbits ==  lbits - 1)
    genRRC (ic);
  else
    wassertl (0, "Unsupported rotation.");
}
/*-----------------------------------------------------------------*/
/* loadSignToC - load the operand's sign bit into CARRY            */
// no use ... shift is ok
/*-----------------------------------------------------------------*/

//static void
//loadSignToC (operand * op)
//{
    //FENTRY;
    //assert (op && AOP (op) && AOP_SIZE (op));
//
    //emitCLRC;
    //emitpcode (POC_BTSZ, newpCodeOpBit (aopGet (AOP (op), AOP_SIZE (op) - 1, FALSE, FALSE), 7, 0));
    //emitSETC;
//}
//
/*-----------------------------------------------------------------*/
/* genRightShift - generate code for right shifting                */
/*-----------------------------------------------------------------*/
static void
genGenericShift (iCode * ic, int shiftRight) // 1 is right
{
    operand *right, *left, *result;
    int size;
    symbol *tlbl, *tlbl1, *inverselbl;

    FENTRY;
    /* if signed then we do it the hard way preserve the
       sign bit moving it inwards */
    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);

    /* signed & unsigned types are treated the same : i.e. the
       signed is NOT propagated inwards : quoting from the
       ANSI - standard : "for E1 >> E2, is equivalent to division
       by 2**E2 if unsigned or if it has a non-negative value,
       otherwise the result is implementation defined ", MY definition
       is that the sign does not get propagated */

    right = IC_RIGHT (ic);
    left = IC_LEFT (ic);
    result = IC_RESULT (ic);

    aopOp (right, ic, FALSE);
    aopOp (left, ic, FALSE);
    aopOp (result, ic, FALSE);

    /* if the shift count is known then do it
       as efficiently as possible */
    if (AOP_TYPE (right) == AOP_LIT)
    {
        int lit = (int) ulFromVal (AOP (right)->aopu.aop_lit);
        if (lit < 0)
        {
            lit = -lit;
            shiftRight = !shiftRight;
        }

        if (shiftRight)
            shiftRight_Left2ResultLit (left, result, lit, !SPEC_USIGN (operandType (left)),0);
        else
            shiftLeft_Left2ResultLit (left, result, lit);
        //genRightShiftLiteral (left,right,result,ic, 0);
        return;
    }

    /* shift count is unknown then we have to form
       a loop get the loop count in B : Note: we take
       only the lower order byte since shifting
       more that 32 bits make no sense anyway, ( the
       largest size of an object can be only 32 bits ) */

    /* we must not overwrite the shift counter */
	// c = a>>c .... very very rare case. .. no work, we skip it!!
    assert (!HY08A_sameRegs (AOP (right), AOP (result)));

	// only for LONG we use special function
	if (!HY08A_options.inline_shift_long && AOP_SIZE(left) >= 4 && AOP_SIZE(result) >= 4)
	{
		int i;
		size = AOP_SIZE(left);
		int size1 = AOP_SIZE(result);
		if (size < size1)
			size1 = size;
		// source(left) moved to result above
		// we use stkXX to pass the result
		size = size1;
		for(i=0;i<size;i++)
		{
			mov2w(AOP(left), i);
			if (i == size-1)  // change to FSR1L is LSB!!
				emitpcode(POC_MVWF, get_argument_pcop(size)); // very alternative method
			else
				emitpcode(POC_MVWF, get_argument_pcop(size-i-1));// special rule .. here no change
		}
		mov2w(AOP(right), 0); // shift # is at right

		inverselbl = NULL;
		//if (!SPEC_USIGN(operandType(right))) // ANSI C say t
		//{
		//	inverselbl = newiTempLabel(NULL);
		//	/* signed shift count -- invert shift direction for c<0 */
		//	emitpcode(POC_BTSZ, newpCodeOpBit(aopGet(AOP(right), 0, FALSE, FALSE), 7, 0));
		//	emitpcode(POC_JMP, popGetLabel(inverselbl->key));
		//}
		//if (!inverselbl)
		{
			if (SPEC_USIGN(operandType(left)))
			{
				switch (size)
				{

				case 4:
					if (shiftRight)
					{
						call_libraryfunc("__shr_ulong");
					}
					else
						call_libraryfunc("__shl_long"); // shift l is same
					break;
				case 8:
					if (shiftRight)
						call_libraryfunc("__shr_ulonglong");
					else
						call_libraryfunc("__shl_longlong"); // shift l is same

				}
			}
			else
			{
				switch (AOP_SIZE(result))
				{

				case 4:
					if (shiftRight)
						call_libraryfunc("__shr_slong");
					else
						call_libraryfunc("__shl_long");
					break;
				case 8:
					if (shiftRight)
						call_libraryfunc("__shr_slonglong");
					else
						call_libraryfunc("__shl_longlong");
					break;
				}
			}
		}
		//else // need inverse label
		//{
		//	symbol *finLabel = newiTempLabel(NULL);
		//	if (SPEC_USIGN(operandType(left)))
		//	{
		//		switch (size)
		//		{

		//		case 4:
		//			if (shiftRight)
		//			{

		//				call_libraryfunc("__shr_ulong");
		//				emitpcode(POC_JMP, popGetLabel(finLabel->key));
		//				emitpLabel(inverselbl->key);
		//				emitpcode(POC_SUBL, popGetLit(0));
		//				call_libraryfunc("__shl_long");
		//				emitpLabel(finLabel->key);
		//			}
		//			else
		//			{
		//				call_libraryfunc("__shl_long"); // shift l is same
		//				emitpcode(POC_JMP, popGetLabel(finLabel->key));
		//				emitpLabel(inverselbl->key);
		//				emitpcode(POC_SUBL, popGetLit(0));
		//				call_libraryfunc("__shr_ulong");
		//				emitpLabel(finLabel->key);
		//			}
		//			break;
		//		case 8:
		//			if (shiftRight)
		//			{
		//				call_libraryfunc("__shr_ulonglong");
		//				emitpcode(POC_JMP, popGetLabel(finLabel->key));
		//				emitpLabel(inverselbl->key);
		//				emitpcode(POC_SUBL, popGetLit(0));
		//				call_libraryfunc("__shl_longlong");
		//				emitpLabel(finLabel->key);
		//			}
		//			else
		//			{
		//				call_libraryfunc("__shl_longlong"); // shift l is same
		//				emitpcode(POC_JMP, popGetLabel(finLabel->key));
		//				emitpLabel(inverselbl->key);
		//				emitpcode(POC_SUBL, popGetLit(0));
		//				call_libraryfunc("__shr_ulonglong");
		//				emitpLabel(finLabel->key);
		//			}

		//		}
		//	}
		//	else
		//	{
		//		switch (AOP_SIZE(result))
		//		{

		//		case 4:
		//			if (shiftRight)
		//			{
		//				call_libraryfunc("__shr_slong");
		//				emitpcode(POC_JMP, popGetLabel(finLabel->key));
		//				emitpLabel(inverselbl->key);
		//				emitpcode(POC_SUBL, popGetLit(0));
		//				call_libraryfunc("__shl_long");
		//				emitpLabel(finLabel->key);
		//			}
		//			else
		//			{
		//				call_libraryfunc("__shl_long");
		//				emitpcode(POC_JMP, popGetLabel(finLabel->key));
		//				emitpLabel(inverselbl->key);
		//				emitpcode(POC_SUBL, popGetLit(0));
		//				call_libraryfunc("__shr_slong");
		//				emitpLabel(finLabel->key);
		//			}
		//			break;
		//		case 8:
		//			if (shiftRight)
		//			{
		//				call_libraryfunc("__shr_slonglong");
		//				emitpcode(POC_JMP, popGetLabel(finLabel->key));
		//				emitpLabel(inverselbl->key);
		//				emitpcode(POC_SUBL, popGetLit(0));
		//				call_libraryfunc("__shl_longlong");
		//				emitpLabel(finLabel->key);
		//			}
		//			else
		//			{
		//				call_libraryfunc("__shl_longlong");
		//				emitpcode(POC_JMP, popGetLabel(finLabel->key));
		//				emitpLabel(inverselbl->key);
		//				emitpcode(POC_SUBL, popGetLit(0));
		//				call_libraryfunc("__shr_slonglong");
		//				emitpLabel(finLabel->key);
		//			}
		//			break;
		//		}
		//	}
		//}
		// mov from stk back to result
		// though the process is long, we just want correctness
		size1 = size;
		
		while (--size1 >= 0)
		{
			if (size1 != size - 1)
				emitpcode(POC_MVFW, get_argument_pcop(size-size1-1));
			movwf(AOP(result), size1);
		}
		goto release;
	}

    /* now move the left to the result if they are not the
       same */
    if (!HY08A_sameRegs (AOP (left), AOP (result)))
    {
        size = min (AOP_SIZE (result), AOP_SIZE (left));
        while (size--)
        {
            mov2w (AOP (left), size);
            movwf (AOP (result), size);
        }
        addSign (result, AOP_SIZE (left), !SPEC_USIGN (operandType (left)));
    }

    tlbl = newiTempLabel (NULL);
    tlbl1 = newiTempLabel (NULL);
    inverselbl = NULL;
    size = AOP_SIZE (result);

    mov2w (AOP (right), 0);
    if (!SPEC_USIGN (operandType (right)))
    {
        inverselbl = newiTempLabel (NULL);
        /* signed shift count -- invert shift direction for c<0 */
        emitpcode (POC_BTSZ, newpCodeOpBit (aopGet (AOP (right), 0, FALSE, FALSE), 7, 0));
        emitpcode (POC_JMP, popGetLabel (inverselbl->key));
    }                           // if
    // This SUBL for special shift loop is seldom used, we keep it.
    // SUBL 0 will clear C, if not zero
    emitpcode (POC_SUBL, popGetLit (0)); /* -count in WREG, 0-x > 0 --> BORROW = !CARRY --> CARRY is clear! */ 
    /* check for `a = b >> c' with `-c == 0' */
    emitSKPNZ;
    emitpcode (POC_JMP, popGetLabel (tlbl1->key));
    emitpLabel (tlbl->key);
    /* propagate the sign bit inwards for SIGNED result */

    //if (shiftRight && !SPEC_USIGN (operandType (result)))
    //loadSignToC (result);
    genMultiAsm (shiftRight ? POC_RRCF : POC_RLCF, result, size, shiftRight, !SPEC_USIGN(operandType(result)));
    emitpcode (POC_ADDLW, popGetLit (1)); /* clears CARRY (unless W==0 afterwards) */
    emitSKPC;
    //emitpcode(POC_INSUZW, pCodeOpCopy(PCOP(&pc_wreg)));
    // insuz no change C
    emitpcode (POC_JMP, popGetLabel (tlbl->key));

    if (!SPEC_USIGN (operandType (right)))
    {
        symbol *inv_loop = newiTempLabel (NULL);

        shiftRight = !shiftRight; /* invert shift direction */

        /* we came here from the code above -- we are done */
        emitpcode (POC_JMP, popGetLabel (tlbl1->key));

        /* emit code for shifting N<0 steps, count is already in W */
        emitpLabel (inverselbl->key);
        if (!shiftRight || SPEC_USIGN (operandType (result)))
            emitCLRC;
        emitpLabel (inv_loop->key);
        /* propagate the sign bit inwards for SIGNED result */
        //if (shiftRight && !SPEC_USIGN (operandType (result)))
        //  loadSignToC (result);
        genMultiAsm (shiftRight ? POC_RRCF : POC_RLCF, result, size, shiftRight, !SPEC_USIGN(operandType(result)));
        emitpcode (POC_ADDLW, popGetLit (1));
        emitSKPC;
        //emitpcode(POC_INSUZW, pCodeOpCopy(PCOP(&pc_wreg)));
        emitpcode (POC_JMP, popGetLabel (inv_loop->key));
    }                           // if

    emitpLabel (tlbl1->key);
release:
    freeAsmop (left, NULL, ic, TRUE);
    freeAsmop (right, NULL, ic, TRUE);
    freeAsmop (result, NULL, ic, TRUE);
}

static void
genRightShift (iCode * ic)
{
    genGenericShift (ic, 1);
}

static void
genLeftShift (iCode * ic)
{
    genGenericShift (ic, 0);
}

/*-----------------------------------------------------------------*/
/* SetIrp - Set IRP bit                                            */
/*-----------------------------------------------------------------*/

static void
setup_fsr (operand * ptr)
{
    if (op_isLitLike(ptr))
    {
        //XDATAA2PTR(ptr);
        //if (ptr->aop->type == AOP_LIT)
		// special 08d
		if ((HY08A_getPART()->isEnhancedCore == 4 || HY08A_getPART()->isEnhancedCore == 3) && ptr->type == SYMBOL && ptr->svt.symOperand->liveTo != 0) // limited range, which means on stack
		{
			emitpcode(POC_MVL, popGetAddr(AOP(ptr), 0, 0));
			emitpcode(POC_MVWF, (pCodeOp*)&pc_fsr0l);
			emitpcode(POC_MVL, popGetAddr(AOP(ptr), 1, 0)); // high address?
			emitpcode(POC_MVWF, (pCodeOp*)&pc_fsr0h);

		}
		else
		{
			emitpcode(POC_LDPR, popGetAddr(AOP(ptr), 0, 0));
		}
    }
    else
    {
        mov2w_op(ptr, 0);
        emitpcode(POC_MVWF, popCopyReg(&pc_fsr0l));
        mov2w_op(ptr, 1);
        emitpcode(POC_MVWF, popCopyReg(&pc_fsr0h));
    }
}

//static void
//inc_fsr (int delta)
//{
//  if (0 == delta)
//    {
//      /* Nothing to do. */
//      return;
//    } // if
//
//  if (HY08A_getPART ()->isEnhancedCore)
//    {
//      if (HY08A_options.no_ext_instr)
//        {
//          /*
//           * Not sure if we may modify W here, so implement this without
//           * touching W.
//           *
//           * Efficiency is not too important here, as enhanced cores
//           * will most likely use extended instructions here. This is
//           * only a workaround for gputils 0.13.7, which supports the
//           * 16f1934 enhanced core, but fails to assemble ADDFSR.
//           */
//          while (delta > 0)
//            {
//              emitpcode (POC_INSZ, popCopyReg (&pc_fsr0l));
//              emitpcode (POC_DECF, popCopyReg (&pc_fsr0h));
//              emitpcode (POC_INF, popCopyReg (&pc_fsr0h));
//              --delta;
//            } // while
//          while (delta < 0)
//            {
//              addpCode2pBlock (pb, newpCodeAsmDir("MOVF", "FSR0L, 1"));
//              emitSKPNZ;
//              emitpcode (POC_DECF, popCopyReg (&pc_fsr0h));
//              emitpcode (POC_DECF, popCopyReg (&pc_fsr0l));
//              ++delta;
//            } // while
//        }
//      else
//        {
//          assert (delta >= -32);
//          assert (delta < 32);
//          /* Hack: Turn this into a PCI (not that easy due to the argument structure). */
//          addpCode2pBlock (pb, newpCodeAsmDir ("ADDFSR", "FSR0, %d", delta));
//        } // if
//    }
//  else
//    {
//      while (delta > 0)
//        {
//          emitpcode (POC_INF, popCopyReg (&pc_fsr));
//          --delta;
//        } // while
//      while (delta < 0)
//        {
//          emitpcode (POC_DECF, popCopyReg (&pc_fsr));
//          ++delta;
//        } // while
//    } // if
//}

/*-----------------------------------------------------------------*/
/* emitPtrByteGet - emits code to get a byte into WREG from an     */
/*                  arbitrary pointer (__code, __data, generic)    */
/*-----------------------------------------------------------------*/
static void
emitPtrByteGet (operand * src, int p_type, bool alreadyAddressed)
{
    FENTRY;
    switch (p_type)
    {
    case POINTER:
    case FPOINTER:
        if (!alreadyAddressed)
            setup_fsr (src);
        emitpcode (POC_MVFW, popCopyReg (pc_indf));
        break;

    case CPOINTER:
        assert (AOP_SIZE (src) == 2);

		//if (aop_isLitLike(AOP(src)))
		//{
		//	emitpcode(POC_MVLP, AOP(src)->aopu.pcop);
		//	emitpcode(POC_TBLR, NULL);
		//	// linker shall check if MVLP is odd!!
		//	emitpcode(POC_MVFW, PCOP(&pc_tbldh));
		//	break;
		//}
		if (HY08A_getPART()->isEnhancedCore == 2) // depends on 
		{
			mov2w_op(src, 0);

			emitpcode(POC_MVWF, popRegFromIdx2(Gstack_base_addr - 1, "__gptrget1", 1, 0));
			mov2w_op(src, 1);

			emitpcode(POC_MVWF, popRegFromIdx2(Gstack_base_addr, "__gptrget1", 1, 0));

			emitpcode(POC_MVL, popGetLit(GPTRTAG_CODE));  /* GPOINTER tag for __code space */
			call_libraryfunc("__gptrget1");
			break;
		}
		mov2w_op(src, 0);

		emitpcode(POC_MVWF, popRegFromIdx2(Gstack_base_addr , "__g2ptrget1", 1, 0));
		mov2w_op(src, 1);

		if (PCI(pb->pcTail)->pcop->type == PO_IMMEDIATE) // it must exist!!
			PCOI(PCI(pb->pcTail)->pcop)->_cp2gp = 1;
		else
			emitpcode(POC_IORL, popGetLit(GPTRTAG_CODE));  /* GPOINTER tag for __code space */
		call_libraryfunc("__g2ptrget1");
		break;

    case GPOINTER:
		if (HY08A_getPART()->isEnhancedCore == 2) // only GAUGE has large ROM!!
		{
			assert(AOP_SIZE(src) == 3);
			mov2w_op(src, 0);
			emitpcode(POC_MVWF, popRegFromIdx2(Gstack_base_addr - 1, "__gptrget1", 1, 0));
			mov2w_op(src, 1);
			emitpcode(POC_MVWF, popRegFromIdx2(Gstack_base_addr, "__gptrget1", 1, 0));
			mov2w_op(src, 2);
			call_libraryfunc("__gptrget1");
		}
		else
		{
			assert(AOP_SIZE(src) == 2);
			mov2w_op(src, 0);
			emitpcode(POC_MVWF, popRegFromIdx2(Gstack_base_addr, "__g2ptrget1", 1, 0));
			mov2w_op(src, 1);
			call_libraryfunc("__g2ptrget1");
		}
        break;

    default:
        assert (!"unhandled pointer type");
        break;
    }
}

/*-----------------------------------------------------------------*/
/* emitPtrByteSet - emits code to set a byte from src through a    */
/* pointer register INDF (legacy 8051 uses R0, R1, or DPTR).       */
/*-----------------------------------------------------------------*/
static void
emitPtrByteSet (operand * dst, int p_type, bool alreadyAddressed)
{
    FENTRY;
    switch (p_type)
    {
    case POINTER:
    case FPOINTER:
        if (!alreadyAddressed)
            setup_fsr (dst);
        emitpcode (POC_MVWF, popCopyReg (pc_indf));
        break;

    case CPOINTER:
        assert (!"trying to assign to __code pointer");
        break;

    case GPOINTER:

		if (HY08A_getPART()->isEnhancedCore == 2)
		{
			emitpcode(POC_MVWF, popRegFromIdx2(Gstack_base_addr - 2, "__gptrput1", 1, 0));
			mov2w_op(dst, 0);
			emitpcode(POC_MVWF, popRegFromIdx2(Gstack_base_addr - 1, "__gptrput1", 1, 0));
			mov2w_op(dst, 1);
			emitpcode(POC_MVWF, popRegFromIdx2(Gstack_base_addr - 0, "__gptrput1", 1, 0));
			mov2w_op(dst, 2);
			call_libraryfunc("__gptrput1");
			break;
		}
		emitpcode(POC_MVWF, popRegFromIdx2(Gstack_base_addr - 1, "__g2ptrput1", 1, 0));
		mov2w_op(dst, 0);
		emitpcode(POC_MVWF, popRegFromIdx2(Gstack_base_addr - 0, "__g2ptrput1", 1, 0));
		mov2w_op(dst, 1);
		call_libraryfunc("__g2ptrput1");
		break;


    default:
        assert (!"unhandled pointer type");
        break;
    }
}

/*-----------------------------------------------------------------*/
/* genUnpackBits - generates code for unpacking bits               */
/*-----------------------------------------------------------------*/
static void
genUnpackBits (operand * result, operand * left, int ptype, iCode * ifx)
{
    sym_link *etype;              /* bitfield type information */
    unsigned blen;                /* bitfield length */
    unsigned bstr;                /* bitfield starting bit within byte */
    //int inX;
	int LinX=0; // left inX
	int LinROM = 0;

    FENTRY;
    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);

    etype = getSpec (operandType (result));
    blen = SPEC_BLEN (etype);
    bstr = SPEC_BSTR (etype);
    //inX = IS_TRUE_SYMOP(result) && IN_XSPACE(SPEC_OCLS(etype));
	if (left->type != VALUE)
	{
		LinX = (IS_TRUE_SYMOP(left) && IN_XSPACE(SPEC_OCLS(OP_SYMBOL(left)->etype)))
			|| OP_SYMBOL(left)->rematX;
		LinROM = IN_CODESPACE(SPEC_OCLS(OP_SYMBOL(left)->etype)); // when code, it can be itmp
		if (LinROM && ptype == -1)
			ptype = CPOINTER; // bug-1981238 
	}
    /* single bit field case */
    if (blen == 1)
    {
        if (ifx)
        {
            /* that is for an if statement */
            pCodeOp *pcop=NULL;
            resolvedIfx rIfx;

            resolveIfx (&rIfx, ifx);
            if (ptype == -1 && !LinX && !LinROM)      /* direct */
                pcop = newpCodeOpBit (aopGet (AOP (left), 0, FALSE, FALSE), bstr, 0);
            else
            {
				if (LinROM)
				{
					//assert(AOP_SIZE(src) == 2);
					if (HY08A_getPART()->isEnhancedCore == 2)
					{
						mov2w_op(left, 0);
						emitpcode(POC_MVWF, popRegFromIdx2(Gstack_base_addr - 1, "__gptrget1", 1, 0));
						mov2w_op(left, 1);

						emitpcode(POC_MVWF, popRegFromIdx2(Gstack_base_addr, "__gptrget1", 1, 0));

						emitpcode(POC_MVL, popGetLit(GPTRTAG_CODE));  /* GPOINTER tag for __code space */
						call_libraryfunc("__gptrget1");
					}
					else
					{
						mov2w_op(left, 0);
						emitpcode(POC_MVWF, popRegFromIdx2(Gstack_base_addr, "__g2ptrget1", 1, 0));
						mov2w_op(left, 1);
						if (PCI(pb->pcTail)->pcop->type == PO_IMMEDIATE) // it must exist!!
							PCOI(PCI(pb->pcTail)->pcop)->_cp2gp = 1;
						else
							emitpcode(POC_IORL, popGetLit(GPTRTAG_CODE));  /* GPOINTER tag for __code space */
						call_libraryfunc("__g2ptrget1");
					}
				}
				else
				{
					setup_fsr(left);
					pcop = newpCodeOpBitByOp(PCOP(pc_indf), bstr, 0);
				}
            }
            // special for X memory map

            {
                emitpcode((rIfx.condition) ? POC_BTSZ : POC_BTSS, pcop);
            }
            emitpcode (POC_JMP, popGetLabel (rIfx.lbl->key));
            ifx->generated = 1;
        }
        else
        {
            /*
             * In case of a volatile bitfield read such as
             * (void)PORTCbits.RC3;
             * we end up having no result ...
             */
            int haveResult = !!AOP_SIZE(result);

            if (haveResult)
            {
                assert (!HY08A_sameRegs (AOP (result), AOP (left)));
                //emitpcode (POC_CLRF, popGet (AOP (result), 0));
				emitpcode(POC_MVL, popGetLit(0));
            } // if

            switch (ptype)
            {
            case -1:
                emitpcode (POC_BTSZ, newpCodeOpBit (aopGet (AOP (left), 0, FALSE, FALSE), bstr, 0));
                /* If haveResult, adjust result below, otherwise: */
                if (!haveResult)
                {
                    /* Dummy instruction to allow bit-test above (volatile dummy bitfield read). */
                    emitpcode (POC_MVL, popGetLit (0));
                } // if
                break;

            case POINTER:
            case FPOINTER:
            case GPOINTER:
            case CPOINTER:
                emitPtrByteGet (left, ptype, FALSE);
                if (haveResult)
                {
                    emitpcode (POC_ANDLW, popGetLit (1UL << bstr));
                    emitSKPZ;
                    /* adjust result below */
                } // if
                break;

            default:
                assert (!"unhandled pointer type");
            }                   // switch

            /* move sign-/zero extended bit to result */
            if (haveResult)
            {
                if (SPEC_USIGN (OP_SYM_ETYPE (left)))
                    //emitpcode (POC_INF, popGet(AOP(result), 0));
					emitpcode(POC_MVL, popGetLit(1));
                else
                    //emitpcode (POC_DECF, popGet(AOP(result), 0));
					emitpcode(POC_MVL, popGetLit(0xff));
				emitpcode(POC_MVWF, popGet(AOP(result), 0));
                addSign (result, 1, !SPEC_USIGN (OP_SYM_ETYPE (left)));
            } // if
        }
        return;
    }
    else if (blen <= 8 && ((blen + bstr) <= 8))
    {
        /* blen > 1 */
        int i;

        for (i = 0; i < AOP_SIZE (result); i++)
            emitpcode (POC_CLRF, popGet (AOP (result), i));

        switch (ptype)
        {
        case -1:
			if (LinROM)
			{
				if (HY08A_getPART()->isEnhancedCore == 2)
				{
					mov2w_op(left, 0);
					emitpcode(POC_MVWF, popRegFromIdx2(Gstack_base_addr - 1, "__gptrget1", 1, 0));
					mov2w_op(left, 1);

					emitpcode(POC_MVWF, popRegFromIdx2(Gstack_base_addr, "__gptrget1", 1, 0));

					emitpcode(POC_MVL, popGetLit(GPTRTAG_CODE));  /* GPOINTER tag for __code space */
					call_libraryfunc("__gptrget1");
				}
				else
				{
					mov2w_op(left, 0);
					emitpcode(POC_MVWF, popRegFromIdx2(Gstack_base_addr , "__g2ptrget1", 1, 0));
					mov2w_op(left, 1);
					if (PCI(pb->pcTail)->pcop->type == PO_IMMEDIATE) // it must exist!!
						PCOI(PCI(pb->pcTail)->pcop)->_cp2gp = 1;
					else
						emitpcode(POC_IORL, popGetLit(GPTRTAG_CODE));  /* GPOINTER tag for __code space */
					call_libraryfunc("__g2ptrget1");
				}
			}
			else
			{
				if (aop_isLitLike(AOP(left)))
					emitpcode(POC_MVFW, popGet(AOP(left), 0));
				else
					mov2w(AOP(left), 0);
			}
            break;
        case POINTER:
        case FPOINTER:
        case GPOINTER:
        case CPOINTER:
            emitPtrByteGet (left, ptype, FALSE);
            break;

        default:
            assert (!"unhandled pointer type");
        }                       // switch

        if (blen < 8)
            emitpcode (POC_ANDLW, popGetLit ((((1UL << blen) - 1) << bstr) & 0x00ff));
        movwf (AOP (result), 0);
        AccRsh (popGet (AOP (result), 0), bstr, 1);       /* zero extend the bitfield */

		// bit field we don't process sign
		

		//if (left->type != VALUE)
		//{
		//	if (!SPEC_USIGN(OP_SYM_ETYPE(left)) && (bstr + blen != 8))
		//	{
		//		/* signed bitfield */
		//		assert(bstr + blen > 0);
		//		emitpcode(POC_MVL, popGetLit(0x00ff << (bstr + blen)));
		//		emitpcode(POC_BTSZ, newpCodeOpBit(aopGet(AOP(result), 0, FALSE, FALSE), bstr + blen - 1, 0));
		//		emitpcode(POC_IORWF, popGet(AOP(result), 0));
		//	}

		//	addSign(result, 1, !SPEC_USIGN(OP_SYM_ETYPE(left)));
		//	return;
		//}
		//else
		//{
		//	if (!SPEC_USIGN(OP_VALUE(left)->etype) && (bstr + blen != 8))
		//	{
		//		/* signed bitfield */
		//		assert(bstr + blen > 0);
		//		emitpcode(POC_MVL, popGetLit(0x00ff << (bstr + blen)));
		//		emitpcode(POC_BTSZ, newpCodeOpBit(aopGet(AOP(result), 0, FALSE, FALSE), bstr + blen - 1, 0));
		//		emitpcode(POC_IORWF, popGet(AOP(result), 0));
		//	}

		//	addSign(result, 1, !SPEC_USIGN(OP_VALUE(left)->etype));
		//	return;
		//}
		return;
    }

    assert (!"bitfields larger than 8 bits or crossing byte boundaries are not yet supported");
}



#if 1
/*-----------------------------------------------------------------*/
/* genDataPointerGet - generates code when ptr offset is known     */
/*-----------------------------------------------------------------*/
static void
genDataPointerGet (operand * left, operand * result, iCode * ic)
{
    unsigned int size;
    int offset = 0;

    FENTRY;
    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);


    /* optimization - most of the time, left and result are the same
     * address, but different types. for the hycon code, we could omit
     * the following
     */
    // come here left is already a dir register
#ifndef NEARPOINTER_DIR
#error "Need define NEARPOINTER_DIR"
#endif
#if NEARPOINTER_DIR    
    operand * copiedOperand= malloc(sizeof(operand));
    memcpy(copiedOperand, left, sizeof(operand));
    //if(IS_SYMOP(left) && !IS_DECL(OP_SYMBOL(left)->type->next))
    if(!options.nopeep && ic->next->op!=SEND && IS_SYMOP(left)  && !  (IS_SYMOP(IC_RESULT(ic)) && IS_PTR(OP_SYMBOL(IC_RESULT(ic))->type)))
    //if(1)
    {
    if(ic->next && IS_SYMOP(IC_LEFT(ic->next)) &&  
        isOperandEqual(IC_RESULT(ic),IC_LEFT(ic->next)) &&
        !(IC_RESULT(ic->next) && isOperandEqual(IC_RESULT(ic), IC_RESULT(ic->next))) && // keep for further opt
        !IS_TRUE_SYMOP(IC_RESULT(ic)) &&
        OPrangeLimited(IC_RESULT(ic), ic->seq, ic->next->seq)
    )
    {
        IC_LEFT(ic->next)=copiedOperand; // kill the temp
        ic->generated=1;
    }
    if(ic->next && IS_SYMOP(IC_RIGHT(ic->next)) && 
        isOperandEqual(IC_RESULT(ic),IC_RIGHT(ic->next)) &&
        !(IC_RESULT(ic->next) && isOperandEqual(IC_RESULT(ic), IC_RESULT(ic->next))) && // keep for further opt
        !IS_TRUE_SYMOP(IC_RESULT(ic)) &&
        OPrangeLimited(IC_RESULT(ic), ic->seq, ic->next->seq)
    )
    {
        IC_RIGHT(ic->next)=copiedOperand; // kill the temp
        ic->generated=1;
    }
    if(ic->next && IS_SYMOP(IC_COND(ic->next)) && 
        isOperandEqual(IC_RESULT(ic),IC_COND(ic->next)) &&
        !(IC_RESULT(ic->next) && isOperandEqual(IC_RESULT(ic), IC_RESULT(ic->next))) && // keep for further opt
        !IS_TRUE_SYMOP(IC_RESULT(ic)) &&
        OPrangeLimited(IC_RESULT(ic), ic->seq, ic->next->seq)
    )
    {
        IC_COND(ic->next)=copiedOperand; // kill the temp
        ic->generated=1;
    }
    if(ic->generated)
    {
        // save the near pointer get!!!
        // means convert to DIR
        
        symbol * copiedSym = malloc(sizeof(symbol));
        asmop * copiedAOP = malloc(sizeof(asmop));
        
        memcpy(copiedSym, OP_SYMBOL(copiedOperand), sizeof(symbol));
        memcpy(copiedAOP, AOP(copiedOperand), sizeof(asmop));
        copiedSym->key+=0x1758600; // it will not duplicate
        OP_SYMBOL(copiedOperand)=copiedSym;
        AOP(copiedOperand)=copiedAOP;
        AOP(copiedOperand)->imm2dir=1;
        
        // change the size
        aopOp (result, ic, TRUE);
        // formerly pointer, now I am not pointer
        OP_SYMBOL(copiedOperand)->type = OP_SYMBOL(IC_RESULT(ic))->type;
        AOP(copiedOperand)->size = AOP(result)->size;
        return;
        
    }
    }
    
#endif
    free(copiedOperand);

    aopOp (result, ic, TRUE);

    if (HY08A_sameRegs (AOP (left), AOP (result)))
        return;

    DEBUGHY08A_AopType (__LINE__, left, NULL, result);

    //emitpcode(POC_MVFW, popGet(AOP(left),0));

    size = AOP_SIZE (result);
    //if (size > getSize (OP_SYM_ETYPE (left)))
    //  size = getSize (OP_SYM_ETYPE (left));
    if (OP_SYM_TYPE(left)->next != NULL)
    {
        if (size > getSize(OP_SYM_TYPE(left)->next)) // consider multilevel chain!!
            size = getSize(OP_SYM_TYPE(left)->next);
    }

    offset = 0;
    while (size--)
    {
        emitpcode (POC_MVFW, popGet (AOP (left), offset));
        emitpcode (POC_MVWF, popGet (AOP (result), offset));
        offset++;
    }

    freeAsmop (left, NULL, ic, TRUE);
    freeAsmop (result, NULL, ic, TRUE);
}
#endif

/*-----------------------------------------------------------------*/
/* genNearPointerGet - HY08A_emitcode for near pointer fetch       */
/*-----------------------------------------------------------------*/
static void
genNearPointerGet (operand * left, operand * result, iCode * ic)
{
    asmop *aop = NULL;
    sym_link *ltype = operandType (left);
    sym_link *rtype = operandType (result);
    sym_link *retype = getSpec (rtype);   /* bitfield type information */
    int direct = 0;

    FENTRY;
    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);


    aopOp (left, ic, FALSE);

    /* if left is rematerialisable and
       result is not bit variable type and
       the left is pointer to data space i.e
       lower 128 bytes of space */
    if (AOP_TYPE (left) == AOP_PCODE &&   //AOP_TYPE(left) == AOP_IMMD &&
            !IS_BITVAR (retype) && IS_DATA_PTR (ltype)) // exclude FAR PTR
    {
        genDataPointerGet (left, result, ic);
        return;
    }

    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);
    aopOp (result, ic, FALSE);

    /* Check if can access directly instead of via a pointer */
    if (left->type==VALUE ||((AOP_TYPE (left) == AOP_PCODE) && (AOP (left)->aopu.pcop->type == PO_IMMEDIATE) && (AOP_SIZE (result) <= 1)))
    {
        direct = 1;
    }

    if (IS_BITFIELD (getSpec (operandType (result))))
    {
		// very special case,
		// if bitfield is 1 bit and following operand is EQU, the EQU can be removed, 2018 MAY..
		// it is not a good writing, but some dumb just write it.
		sym_link *etype = getSpec(operandType(result));

		if (SPEC_BLEN(etype) == 1) // we check if following is 1bit 
		{
			if (ic->next->op == EQ_OP && ic->next->next->op==IFX && IC_RIGHT(ic->next)->type==VALUE &&
				IC_COND(ic->next->next)==IC_RESULT(ic->next) &&
				IC_LEFT(ic->next)==IC_RESULT(ic))
			{
				// we check if value is 1 or 0
				if  (IS_OP_LITERAL(IC_RIGHT(ic->next)) &&
					isEqualVal(OP_VALUE(IC_RIGHT(ic->next)), 0))
				{
					// reverse cond
					if (IC_TRUE(ic->next->next))
					{
						IC_FALSE(ic->next->next) = IC_TRUE(ic->next->next);
						IC_TRUE(ic->next->next) = NULL;
					}
					else
					{
						IC_TRUE(ic->next->next) = IC_FALSE(ic->next->next);
						IC_FALSE(ic->next->next) = NULL;
					}
				}
				if (ullFromVal(OP_VALUE(IC_RIGHT(ic->next))) <= 1)
				{
					iCode *p = ic->next;
					ic->next = ic->next->next;
					p->next->prev = ic;
					IC_COND(ic->next) = IC_RESULT(ic);
				}
			}
		}
        genUnpackBits (result, left, direct ? -1 : POINTER, ifxForOpSpecial (IC_RESULT (ic), ic));
        goto release;
    }

    /* If the pointer value is not in a the FSR then need to put it in */
    /* Must set/reset IRP bit for use with FSR. */
    if (direct)
    {
        int size = AOP_SIZE(result);
        int offset = 0;
		unsigned long long lit=0ll;


        DEBUGHY08A_emitcode("; ***", "%s  %d", __FUNCTION__, __LINE__);

		if (AOP(left)->type == AOP_LIT && (lit = ullFromVal(AOP(left)->aopu.aop_lit)) < 128 && 
            !(HY08A_getPART()->isEnhancedCore >=3 && HY08A_getPART()->isEnhancedCore!=5))
		{
			while (size--)
			{
				emitpcode(POC_MVFW, popGetLit((unsigned int)(lit++))); // low to high!!
				emitpcode(POC_MVWF, popGet(AOP(result), offset));

				offset++;
			}
		}

		else
		{
			if (IS_SYMOP(result) && OP_SYMBOL(result)->isitmp && 
                    ((HY08A_getPART()->isEnhancedCore >=3 && HY08A_getPART()->isEnhancedCore!=5)
                    //|| !strcmp(HY08A_getPART()->name,"HY17P56")
                    //|| !strcmp(HY08A_getPART()->name,"HY17P55")
                    ))
			{
                // we make new equ
                char buf[512];
                char bufr[600];
                //static int equsaveid=0;
                while(size--)
                {
                    snprintf(buf,511, "_PTREQU_%X",(unsigned int)(lit&0xfff));
                    snprintf(bufr,599,"_%s",buf);
                    if(!regFindWithName(bufr))
                    {
                        sym_link *the_type = newLink(SPECIFIER);
                        the_type->select.s.b_absadr=1;
                        the_type->select.s._addr=(unsigned int)lit;
                        the_type->select.s.noun = V_CHAR;
                        the_type->select.s.oclass = sfr;
                        the_type->select.s.b_volatile=1;
                        symbol *newsym=newSymbol(buf,0);
                        strncpy(newsym->rname,bufr,sizeof(newsym->rname)-1);
                        newsym->type=the_type;
                        newsym->etype=the_type;
                        allocGlobal(newsym);
                    }
                    

                    emitpcode(POC_MVFW,newpCodeOpRegFromStr(bufr)); // low to high!!
				    emitpcode(POC_MVWF, popGet(AOP(result), offset++));
                    // pCode *newequ=newpCodeAsmDir(buf, "\t.equ\t0x%X",(lit&0xfff));
                    // addpCode2pBlock(pb, newequ);
                    lit++;

                }
                // if(lit+size-1<0x80)
                // {
				//     while (size--)
				//     {
				// 	    emitpcode(POC_MVFW, popGetLit((unsigned int)(lit++))); // low to high!!
				// 	    emitpcode(POC_MVWF, popGet(AOP(result), offset));

				// 	    offset++;
				//     }
                // }else  // we have no mvfs, we add equ inline assembly?
                // {
                //     while (size--)
				//     {
				// 	    emitpcode2(POC_MVFF, popGetLit((unsigned int)(lit++)), PCOP(&pc_wreg)); // low to high!!
				// 	    emitpcode(POC_MVWF, popGet(AOP(result), offset));
				// 	    offset++;
				//     }
                // }
			}
			else
			{
				while (size--)
				{
					emitpcode21(POC_MVFF, popGet2(AOP(left), offset, 1), popGet(AOP(result), offset));
					offset++;

				}
			}
		}
        goto release;
    }
    else
        setup_fsr (left);


//  sym_link *etype;
    /* if bitfield then unpack the bits */
    {
        /* we have can just get the values */
        int size = AOP_SIZE (result);
        int offset = 0;
		int size0 = size;

        DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);

        while (size--)
        {
            if (direct)
                emitpcode (POC_MVWF, popGet (AOP (left), 0));
			else
			{
				if(size0==1)
					emitpcode(POC_MVFW, popCopyReg(&pc_indf0));
				else
					emitpcode(POC_MVFW, popCopyReg(&pc_poinc0));
			}
            if (AOP_TYPE (result) == AOP_LIT) //?
            {
				fprintf(stderr, "pointer get to lit?? seems a bug %s:%d.\n", __FUNCTION__, __LINE__);
                emitpcode (POC_MVL, popGet (AOP (result), offset));
            }
            else
            {
                emitpcode (POC_MVWF, popGet (AOP (result), offset));
            }
            //if (size && !direct)
            //  {
            //    inc_fsr (1);
            //  }
            offset++;
        }
    }

    /* now some housekeeping stuff */
    if (aop)
    {
        /* we had to allocate for this iCode */
        DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);
        freeAsmop (NULL, aop, ic, TRUE);
    }
    else if (!direct)
    {
        /* nothing to do */
    }
    else
    {
        /* we did not allocate which means left
           already in a pointer register, then
           if size > 0 && this could be used again
           we have to point it back to where it
           belongs */
        DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);
        if (AOP_SIZE (result) > 0 && (!OP_SYMBOL (left)->remat && !OP_SYMBOL(left)->rematX) && (OP_SYMBOL (left)->liveTo > ic->seq || ic->depth))
        {
            //int size = AOP_SIZE (result) - 1;
            int size = AOP_SIZE(result) ;
            emitpcode(POC_MVL, newpCodeOpLit(256-size));
            emitpcode(POC_MVFW, PCOP(&pc_plusw0));

            //inc_fsr (-size);
        }
    }

release:
    /* done */
    freeAsmop (left, NULL, ic, TRUE);
    freeAsmop (result, NULL, ic, TRUE);

}

/*-----------------------------------------------------------------*/
/* genGenPointerGet - gget value from generic pointer space        */
/*-----------------------------------------------------------------*/
static void
genGenPointerGet (operand * left, operand * result, iCode * ic)
{
    FENTRY;
    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);
    aopOp (left, ic, FALSE);
    aopOp (result, ic, FALSE);


    DEBUGHY08A_AopType (__LINE__, left, NULL, result);

    if (IS_BITFIELD (getSpec (operandType (result))))
    {
        genUnpackBits (result, left, GPOINTER, ifxForOpSpecial (IC_RESULT (ic), ic));
        return;
    }

	{
		/* emit call to __gptrget */
		char *func[] = { NULL, "__gptrget1", "__gptrget2", "__gptrget3", "__gptrget4",
		NULL,NULL,NULL,"__gptrget8" };
		char *func2[] = { NULL, "__g2ptrget1", "__g2ptrget2", "__g2ptrget3", "__g2ptrget4",
		NULL,NULL,NULL,"__g2ptrget8" };
		int size = AOP_SIZE(result);
		int idx = 0;


		assert(size > 0 && size <= 8);

		if (HY08A_getPART()->isEnhancedCore == 2)
		{
			/* pass arguments */
			assert(AOP_SIZE(left) >= 2);
			mov2w(AOP(left), 0);
			emitpcode(POC_MVWF, popRegFromIdx2(Gstack_base_addr - 1, func[size], 1, 0));
			mov2w(AOP(left), 1);
			emitpcode(POC_MVWF, popRegFromIdx2(Gstack_base_addr, func[size], 1, 0));
			if (AOP_SIZE(left) == 2)
				emitpcode(POC_MVL, popGetLit(0x80)); // it must be const??
			else
				mov2w(AOP(left), 2);
			call_libraryfunc(func[size]);
		}
		else
		{
			assert(AOP_SIZE(left) >= 2);
			mov2w(AOP(left), 0);
			emitpcode(POC_MVWF, popRegFromIdx2(Gstack_base_addr, func2[size], 1, 0));
			mov2w(AOP(left), 1);
			call_libraryfunc(func2[size]);
		}


		/* save result */
		movwf(AOP(result), --size);
		while (size--)
		{
			emitpcode(POC_MVFW, popRegFromIdx(Gstack_base_addr - idx++));
			movwf(AOP(result), size);
		}                         // while
	}

    freeAsmop (left, NULL, ic, TRUE);
    freeAsmop (result, NULL, ic, TRUE);

}

/*-----------------------------------------------------------------*/
/* genConstPointerGet - get value from const generic pointer space */
/*-----------------------------------------------------------------*/
static void
genConstPointerGet (operand * left, operand * result, iCode * ic)
{
    //sym_link *retype = getSpec(operandType(result));
#if 0
    symbol *albl, *blbl;          //, *clbl;
    pCodeOp *pcop;
#endif
    int i, lit;

    FENTRY;
    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);
    aopOp (left, ic, FALSE);
    aopOp (result, ic, FALSE);

    DEBUGHY08A_AopType (__LINE__, left, NULL, result);

    DEBUGHY08A_emitcode ("; ", " %d getting const pointer", __LINE__);

    lit = op_isLitLike (left);

    if (IS_BITFIELD (getSpec (operandType (result))))
    {
        genUnpackBits (result, left, lit ? -1 : CPOINTER, ifxForOpSpecial (IC_RESULT (ic), ic));
        goto release;
    }

	if (aop_isLitLike(AOP(left)))
	{
		int size = AOP_SIZE(result);
		if (size == 1 && HY08A_getPART()->isEnhancedCore!=5) // only1, and not 08e
		{
			pCode *pcRef;
			emitpcode(POC_MVLP, AOP(left)->aopu.pcop);
			pcRef = pb->pcTail;
			PCI(pb->pcTail)->pcop->flags.imm2label = 1;
			emitpcodeNULLop(POC_TBLR);
			// (DW) word must be word aligned,
			// byte HL is add when link
			for (i = 0; i < size; i++)
			{
				if ((i & 1) == 0)
				{
					emitpcode(POC_MVFW, pCodeOpCopy(PCOP(&pc_tbldh)));// it must use copy!!
					//if (i == 0) // single 
					PCI(pb->pcTail)->pcop->TBLR_Reference = pcRef; // special reference!!
				}
				else
				{
					emitpcode(POC_MVFW, pCodeOpCopy(PCOP(&pc_tbldl)));
					PCI(pb->pcTail)->pcop->TBLR_Reference = pcRef;
					PCI(pb->pcTail)->pcop->flags.needSub1 = 1;
				}
				movwf(AOP(result), i);
				if ((i & 1) && i < size - 1)
				{
					emitpcodeNULLop(POC_TBLR);
				}

			}
			goto release;
		}
	}

	{
		char *func[] = { NULL, "__gptrget1", "__gptrget2", "__gptrget3", "__gptrget4",NULL,NULL,NULL,"__gptrget8" };
		char *func2[] = { NULL, "__g2ptrget1", "__g2ptrget2", "__g2ptrget3", "__g2ptrget4",NULL,NULL,NULL,"__g2ptrget8" };

		int size = AOP_SIZE(result);
		assert(size > 0 && size <= 8);


		if (HY08A_getPART()->isEnhancedCore == 2)
		{
			mov2w_op(left, 0);
			emitpcode(POC_MVWF, popRegFromIdx2(Gstack_base_addr - 1, func[size], 1, 0));
			mov2w_op(left, 1);
			emitpcode(POC_MVWF, popRegFromIdx2(Gstack_base_addr, func[size], 1, 0));


			emitpcode(POC_MVL, popGetLit(GPTRTAG_CODE));    /* GPOINTER tag for __code space */
			call_libraryfunc(func[size]);
		}
		else
		{
			mov2w_op(left, 0);
			emitpcode(POC_MVWF, popRegFromIdx2(Gstack_base_addr, func2[size], 1, 0));
			mov2w_op(left, 1);
			if (PCI(pb->pcTail)->pcop->type == PO_IMMEDIATE) // it must exist!!
				PCOI(PCI(pb->pcTail)->pcop)->_cp2gp = 1;
			else
				emitpcode(POC_IORL, popGetLit(GPTRTAG_CODE));    /* GPOINTER tag for __code space */
			call_libraryfunc(func2[size]);
		}


		movwf(AOP(result), size - 1);
		for (i = 1; i < size; i++)
		{
			emitpcode(POC_MVFW, popRegFromIdx(Gstack_base_addr + 1 - i));
			movwf(AOP(result), size - 1 - i);
		}                         // for
	}

release:
    freeAsmop (left, NULL, ic, TRUE);
    freeAsmop (result, NULL, ic, TRUE);

}

/*-----------------------------------------------------------------*/
/* genPointerGet - generate code for pointer get                   */
/*-----------------------------------------------------------------*/
static void
genPointerGet (iCode * ic)
{
    operand *left, *result;
    sym_link *type, *etype;
    int p_type = -1;

    FENTRY;
    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);

    left = IC_LEFT (ic);
    result = IC_RESULT (ic);

    /* depending on the type of pointer we need to
       move it to the correct pointer register */
    type = operandType (left);
    etype = getSpec (type);

    if (IS_PTR_CONST (type))
        DEBUGHY08A_emitcode ("; ***", "%d - const pointer", __LINE__);

    //if( DCL_TYPE(type)==POINTER && SPEC_CONST(etype) && !IS_FUNC(type)) // change to GPTR if s-fixed
//	    p_type=CPOINTER;

    /* if left is of type of pointer then it is simple */
    //else 
	if (IS_PTR (type) && !IS_FUNC (type->next))
        p_type = DCL_TYPE (type);
    else
    {
        /* we have to go by the storage class */
        p_type = PTR_TYPE (SPEC_OCLS (etype));

        DEBUGHY08A_emitcode ("; ***", "%d - resolve pointer by storage class", __LINE__);

        if (SPEC_OCLS (etype)->codesp)
        {
            DEBUGHY08A_emitcode ("; ***", "%d - cpointer", __LINE__);
            //p_type = CPOINTER ;
        }
        else if (SPEC_OCLS (etype)->fmap && !SPEC_OCLS (etype)->paged)
            DEBUGHY08A_emitcode ("; ***", "%d - fpointer", __LINE__);
        /*p_type = FPOINTER ; */
        else if (SPEC_OCLS (etype)->fmap && SPEC_OCLS (etype)->paged)
            DEBUGHY08A_emitcode ("; ***", "%d - ppointer", __LINE__);
        /*        p_type = PPOINTER; */
        else if (SPEC_OCLS (etype) == idata)
            DEBUGHY08A_emitcode ("; ***", "%d - ipointer", __LINE__);
        /*      p_type = IPOINTER; */
        else
            DEBUGHY08A_emitcode ("; ***", "%d - pointer", __LINE__);
        /*      p_type = POINTER ; */
    }

    /* now that we have the pointer type we assign
       the pointer values */
    switch (p_type)
    {

    case POINTER:
    case FPOINTER:
        //case IPOINTER:

        // it is possible that pointer source is address+constant
        // we search back the icode to see if there is the source
        //if (IS_SYMOP(left) && IS_ITEMP(left) && !isPointer2Stack(IC_LEFT(ic)))
        //{
        //	iCode *srcI;
        //	iCode *srcI2;
        //	symbol *operap;
        //	int canBeSimplified = 0;
        //	for (srcI = ic->prev; srcI; srcI = srcI->prev)
        //	{
        //		if (srcI->op == LABEL || srcI->op == FUNCTION)
        //			break;
        //		if (srcI->op == '+' &&  OP_SYMBOL(IC_RESULT(srcI)) == OP_SYMBOL(left)
        //			&& IS_VALOP(IC_RIGHT(srcI)))
        //		{
        //			operap = OP_SYMBOL(IC_LEFT(srcI));
        //			if (IS_SYMOP(IC_LEFT(srcI)) && IS_ITEMP(IC_LEFT(srcI)))
        //			{
        //				for (srcI2 = srcI->prev; srcI2; srcI2 = srcI2->prev)
        //				{
        //					if (srcI2->op == LABEL || srcI2->op == FUNCTION)
        //						break;
        //					if (IC_RESULT(srcI2) == NULL)
        //						continue;
        //					if(OP_SYMBOL(IC_RESULT(srcI2)) == operap)
        //					{
        //						if (srcI2->op == CAST || srcI2->op == '=')
        //						{
        //							if (IS_SYMOP(IC_RIGHT(srcI2)))
        //							{
        //								operap= OP_SYMBOL(IC_RIGHT(srcI2));
        //								continue;
        //							}
        //							else
        //								break;
        //						}
        //						if (srcI2->op == ADDRESS_OF && !IN_CODESPACE(SPEC_OCLS(getSpec(OP_SYM_TYPE(IC_LEFT(srcI2)))))
        //							&& !IN_XSPACE(SPEC_OCLS(getSpec(OP_SYM_TYPE(IC_LEFT(srcI2)))))
        //							)
        //						{
        //							if (!OP_SYMBOL(IC_LEFT(srcI2))->isref)
        //							{
        //								IC_RIGHT(ic) = IC_LEFT(srcI2);
        //								canBeSimplified = 1;
        //							}
        //							break;
        //						}
        //						//if (srcI2->op == CAST && !IN_CODESPACE(SPEC_OCLS(getSpec(OP_SYM_TYPE(IC_RIGHT(srcI2)))))
        //						//	&& !IN_XSPACE(SPEC_OCLS(getSpec(OP_SYM_TYPE(IC_RIGHT(srcI2)))))
        //						//	)
        //						//{
        //						//	IC_RIGHT(ic) = IC_RIGHT(srcI2);
        //						//	canBeSimplified = 1;
        //						//	break;
        //						//}
        //					}
        //				}
        //				break;
        //			}
        //			else
        //				break;

        //		}
        //	}
        //	if (canBeSimplified)
        //	{
        //		int offset2 = IC_RIGHT(srcI)->svt.valOperand->type->select.s.const_val.v_int;
        //
        //		genAssign2(ic, offset2,0);
        //		ic->generated = 1;
        //		break;

        //	}
        //}
        genNearPointerGet (left, result, ic);
        break;
        /*
           case PPOINTER:
           genPagedPointerGet(left,result,ic);
           break;

           case FPOINTER:
           genFarPointerGet (left,result,ic);
           break;
         */
    case CPOINTER:
        genConstPointerGet (left, result, ic);
        break;

    case GPOINTER:
        if(IS_VALOP(left))
        {
            if( ullFromVal (OP_VALUE (left))<0x1000) // pointer can access to 0x1000
            {
                genNearPointerGet(left, result, ic);
            }else 
            {
                 genGenPointerGet (left, result, ic);
            }
        } else 
            genGenPointerGet (left, result, ic);
        break;
    default:
        assert (!"unhandled pointer type");
        break;
    }

}

/*-----------------------------------------------------------------*/
/* genPackBits - generates code for packed bit storage             */
/*-----------------------------------------------------------------*/
static void
genPackBits (sym_link * etype, operand * result, operand * right, int p_type)
{
    unsigned blen;                /* bitfield length */
    unsigned bstr;                /* bitfield starting bit within byte */
    int litval;                   /* source literal value (if AOP_LIT) */
    unsigned char mask;           /* bitmask within current byte */

    memmap *space = SPEC_OCLS(etype);

    FENTRY;
    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);

    blen = SPEC_BLEN (etype);
    bstr = SPEC_BSTR (etype);

    /* If the bitfield length is less than a byte and does not cross byte boundaries */
    if ((blen <= 8) && ((bstr + blen) <= 8))
    {
        mask = ((unsigned char) (0xFF << (blen + bstr)) | (unsigned char) (0xFF >> (8 - bstr)));

        if (AOP_TYPE (right) == AOP_LIT)
        {
            /* Case with a bitfield length <8 and literal source */
            int lit = (int) ulFromVal (AOP (right)->aopu.aop_lit);
            if (blen == 1)
            {
                pCodeOp *pcop;

                switch (p_type)
                {
                case -1:
                    if (AOP (result)->type == AOP_PCODE || result->type==VALUE)
                        pcop = newpCodeOpBit (aopGet (AOP (result), 0, FALSE, FALSE), bstr, 0);
                    else
                        pcop = popGet (AOP (result), 0);

                    if (IN_XSPACE(space) )
                    {
                        // move to ACC then write back ... which is the only way to prevent interrupt error
                        // change bit->reg
                        //pCodeOp *pcop2 = newpCodeOpBit("_WREG", PCORB(pcop)->bit, 0);
                        //pCodeOp *pcop3 = pCodeOpCopy(pcop);
                        //pcop3->type = PCORB(pcop)->subtype;
                        //emitpcode2(POC_MVFF, pcop3, popCopyReg(&pc_wreg));
                        //emitpcode(lit ? POC_BSF : POC_BCF, pcop2);
                        //emitpcode2(POC_MVFF, popCopyReg(&pc_wreg),pcop3);
                        setup_fsr(result);
                        pcop = newpCodeOpBitByOp(PCOP(pc_indf), bstr, 0);

                    }


                    emitpcode(lit ? POC_BSF : POC_BCF, pcop);

                    break;

                case POINTER:
                case FPOINTER:
                    setup_fsr (result);
                    emitpcode (lit ? POC_BSF : POC_BCF, newpCodeOpBitByOp (PCOP (pc_indf), bstr, 0));
                    break;

                case CPOINTER:
                    assert (!"trying to assign to bitfield via pointer to __code space");
                    break;

                case GPOINTER:
                    emitPtrByteGet (result, p_type, FALSE);
                    if (lit)
                    {
                        emitpcode (POC_IORL, newpCodeOpLit (1UL << bstr));
                    }
                    else
                    {
                        emitpcode (POC_ANDLW, newpCodeOpLit ((~(1UL << bstr)) & 0x0ff));
                    }
                    emitPtrByteSet (result, p_type, TRUE);
                    break;

                default:
                    assert (!"unhandled pointer type");
                    break;
                }               // switch (p_type)
            }
            else
            {
                /* blen > 1 */
                litval = lit << bstr;
                litval &= (~mask) & 0x00ff;

                switch (p_type)
                {
                case -1:
					if (aop_isLitLike(AOP(result)))// special case
						emitpcode(POC_MVFW, popGet(AOP(result), 0));
					else
						mov2w (AOP (result), 0);
                    if ((litval | mask) != 0x00ff)
                        emitpcode (POC_ANDLW, popGetLit (mask));
                    if (litval != 0x00)
                        emitpcode (POC_IORL, popGetLit (litval));
                    movwf (AOP (result), 0);
                    break;

                case POINTER:
                case FPOINTER:
                case GPOINTER:
                    emitPtrByteGet (result, p_type, FALSE);
                    if ((litval | mask) != 0x00ff)
                        emitpcode (POC_ANDLW, popGetLit (mask));
                    if (litval != 0x00)
                        emitpcode (POC_IORL, popGetLit (litval));
                    emitPtrByteSet (result, p_type, TRUE);
                    break;

                case CPOINTER:
                    assert (!"trying to assign to bitfield via pointer to __code space");
                    break;

                default:
                    assert (!"unhandled pointer type");
                    break;
                }               // switch
            }                   // if (blen > 1)
        }
        else
        {
            /* right is no literal */
            if (blen == 1)
            {
                switch (p_type)
                {
                case -1:
                    /* Note more efficient code, of pre clearing bit then only setting it if required,
                     * can only be done if it is known that the result is not a SFR */
                    emitpcode (POC_RRCFW, popGet (AOP (right), 0));
                    emitSKPC; // this is not necessary, but prevent glitch
                    emitpcode (POC_BCF, newpCodeOpBit (aopGet (AOP (result), 0, FALSE, FALSE), bstr, 0));
                    emitSKPNC;
                    emitpcode (POC_BSF, newpCodeOpBit (aopGet (AOP (result), 0, FALSE, FALSE), bstr, 0));
                    break;

                case POINTER:
                case FPOINTER:
                case GPOINTER:
                    //emitPtrByteGet (result, p_type, FALSE); // 2024 special no-peep!!
                    setup_fsr (result);
                    emitpcode (POC_BTSS, newpCodeOpBit (aopGet (AOP (right), 0, FALSE, FALSE), 0, 0));
                    emitpcode (POC_BCF, newpCodeOpBit ( "_INDF0",bstr,0));
                    emitpcode (POC_BTSZ, newpCodeOpBit (aopGet (AOP (right), 0, FALSE, FALSE), 0, 0));
                    emitpcode (POC_BSF, newpCodeOpBit ("_INDF0",bstr,0));
                    //emitPtrByteSet (result, p_type, TRUE);
                    break;

                case CPOINTER:
                    assert (!"trying to assign to bitfield via pointer to __code space");
                    break;

                default:
                    assert (!"unhandled pointer type");
                    break;
                }               // switch
                return;
            }
            else
            {
                /* Case with a bitfield 1 < length <= 8 and arbitrary source */
                pCodeOp *temp = newpCodeOpReg(-1);
				PCOR(temp)->r->wasUsed = 1;
				PCOR(temp)->r->isFree = 0;

                mov2w (AOP (right), 0);
                if (blen < 8)
                {
                    emitpcode (POC_ANDLW, popGetLit ((1UL << blen) - 1));
                }
                emitpcode (POC_MVWF, temp);
                if (bstr)
                {
                    AccLsh (temp, bstr);
                }

                switch (p_type)
                {
                case -1:
                    //mov2w (AOP (result), 0);
					emitpcode(POC_MVFW, popGet(AOP(result), 0)); // cannot use MVL!!
                    emitpcode (POC_ANDLW, popGetLit (mask));
                    emitpcode (POC_IORFW, temp);
                    movwf (AOP (result), 0);
                    break;

                case POINTER:
                case FPOINTER:
                case GPOINTER:
                    emitPtrByteGet (result, p_type, FALSE);
                    emitpcode (POC_ANDLW, popGetLit (mask));
                    emitpcode (POC_IORFW, temp);
                    emitPtrByteSet (result, p_type, TRUE);
                    break;

                case CPOINTER:
                    assert (!"trying to assign to bitfield via pointer to __code space");
                    break;

                default:
                    assert (!"unhandled pointer type");
                    break;
                }               // switch

                popReleaseTempReg (temp);
            }                   // if (blen > 1)
        }                       // if (AOP(right)->type != AOP_LIT)
        return;
    }                           // if (blen <= 8 && ((blen + bstr) <= 8))

    assert (!"bitfields larger than 8 bits or crossing byte boundaries are not yet supported");
}

/*-----------------------------------------------------------------*/
/* genDataPointerSet - remat pointer to data space                 */
/*-----------------------------------------------------------------*/
static void
genDataPointerSet (operand * right, operand * result, iCode * ic)
{
    int size = 0;
    int offset = 0;
    int size1 = 0;

    FENTRY;
    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);
    aopOp (right, ic, FALSE);
    aopOp (result, ic, FALSE);

    assert (IS_SYMOP (result));
    assert (IS_PTR (OP_SYM_TYPE (result)));

    /*
     * Determine size from right operand (not result):
     * The result might be a rematerialized pointer to (the first field in) a struct,
     * which then assumes the type (and size) of the struct rather than the first field.
     */
    size = AOP_SIZE(right);
    size1 = size;

    // tsd, was l+1 - the underline `_' prefix was being stripped
    while (size--)
    {
        emitpComment ("%s:%u: size=%d, offset=%d, AOP_TYPE(res)=%d", __FILE__, __LINE__, size, offset,
                      AOP_TYPE (result));

        if (AOP_TYPE (right) == AOP_LIT)
        {
            unsigned int lit = HY08AaopLiteral (AOP (IC_RIGHT (ic))->aopu.aop_lit, offset);
            //fprintf (stderr, "%s:%u: lit %d 0x%x\n", __FUNCTION__,__LINE__, lit, lit);
            if (lit & 0xff)
            {
                emitpcode (POC_MVL, popGetLit (lit & 0xff));
                emitpcode (POC_MVWF, popGet (AOP (result), offset));
            }
            else
            {
                emitpcode (POC_CLRF, popGet (AOP (result), offset));
            }
        }
        else
        {
            //fprintf (stderr, "%s:%u: no lit\n", __FUNCTION__,__LINE__);
            if (AOP_TYPE(right) == AOP_PCODE && size1==2 && ( !AOP(right)->imm2dir)) // seems pointer assignment ?
            {
                if(size==1)
                    emitpcode(POC_MVL, popGetAddr(AOP(right), 0, 0));
                else
                    emitpcode(POC_MVL, popGetAddr(AOP(right), 1, 0));
                emitpcode(POC_MVWF, popGet(AOP(result), offset));
            }
            else
            {
                emitpcode(POC_MVFW, popGet(AOP(right), offset));
                emitpcode(POC_MVWF, popGet(AOP(result), offset));
            }
        }

        offset++;
    }

    freeAsmop (right, NULL, ic, TRUE);
    freeAsmop (result, NULL, ic, TRUE);
}

/*-----------------------------------------------------------------*/
/* genNearPointerSet - HY08A_emitcode for near pointer put         */
/*-----------------------------------------------------------------*/
static void
genNearPointerSet (operand * right, operand * result, iCode * ic, int fptr)
{
    asmop *aop = NULL;
    sym_link *ptype = operandType (result);
    sym_link *retype = getSpec (operandType (right));
    sym_link *letype = getSpec (ptype);
    int direct = 0;
    //int hwptr = 0;
	bool h08d = (HY08A_getPART()->isEnhancedCore >= 4 && HY08A_getPART()->isEnhancedCore != 5) ;

    FENTRY;
    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);
    aopOp (result, ic, FALSE);

#if 1
    /* if the result is rematerializable &
       in data space & not a bit variable */
    //if (AOP_TYPE(result) == AOP_IMMD &&
    // exclude far pointer
    if (AOP_TYPE (result) == AOP_PCODE && IS_DATA_PTR (ptype) && !IS_BITVAR (retype) && !IS_BITVAR (letype))
    {
        genDataPointerSet (right, result, ic);
        freeAsmop (result, NULL, ic, TRUE);
        return;
    }
#endif

    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);
    aopOp (right, ic, FALSE);
    DEBUGHY08A_AopType (__LINE__, NULL, right, result);

    /* Check if can access directly instead of via a pointer */
    if ( result->type==VALUE || ((AOP_TYPE (result) == AOP_PCODE) && (AOP (result)->aopu.pcop->type == PO_IMMEDIATE) && (AOP_SIZE (right) == 1)))
    {
		
		direct = 1;
    }
    // we check if hwptr, 0 means not hwptr, 1 means hwptr no inc, 2 means hwptr with inc


    if (IS_BITFIELD (letype))
    {
		
        genPackBits (letype, result, right, direct ? -1 : POINTER);
        return;
    }

    // try to use direct here

    //if (IS_SYMOP(result) && IS_ITEMP(result) && !isPointer2Stack(IC_LEFT(ic)) && !fptr)
    //{
    // iCode *srcI;
    // iCode *srcI2;
    // symbol *operap;
    // int canBeSimplified = 0;
    // for (srcI = ic->prev; srcI; srcI = srcI->prev)
    // {
    //  if (srcI->op == LABEL || srcI->op == FUNCTION)
    //	  break;
    //  if (srcI->op == '+' &&  OP_SYMBOL(IC_RESULT(srcI)) == OP_SYMBOL(result)
    //	  && IS_VALOP(IC_RIGHT(srcI)))
    //  {
    //	  operap = OP_SYMBOL(IC_LEFT(srcI));
    //	  if (IS_SYMOP(IC_LEFT(srcI)) && IS_ITEMP(IC_LEFT(srcI)))
    //	  {
    //		  for (srcI2 = srcI->prev; srcI2; srcI2 = srcI2->prev)
    //		  {
    //			  if (srcI2->op == LABEL || srcI2->op == FUNCTION)
    //				  break;
    //			  if (IC_RESULT(srcI2) == NULL)
    //				  continue;
    //			  if (OP_SYMBOL(IC_RESULT(srcI2)) == operap)
    //			  {
    //				  if (srcI2->op == CAST || srcI2->op == '=')
    //				  {
    //					  if (IS_SYMOP(IC_RIGHT(srcI2)))
    //					  {
    //						  operap = OP_SYMBOL(IC_RIGHT(srcI2));
    //						  continue;
    //					  }
    //					  else
    //						  break;
    //				  }
    //				  if (srcI2->op == ADDRESS_OF && !IN_CODESPACE(SPEC_OCLS(getSpec(OP_SYM_TYPE(IC_LEFT(srcI2)))))
    //					  && !IN_XSPACE(SPEC_OCLS(getSpec(OP_SYM_TYPE(IC_LEFT(srcI2)))))
    //					  )
    //				  {
    //					  if (!OP_SYMBOL(IC_LEFT(srcI2))->isptr) // cannot be ptr
    //					  {
    //						  IC_RIGHT(ic) = IC_LEFT(srcI2);
    //						  canBeSimplified = 1;
    //					  }
    //					  break;
    //				  }
    //				  //if (srcI2->op == CAST && !IN_CODESPACE(SPEC_OCLS(getSpec(OP_SYM_TYPE(IC_RIGHT(srcI2)))))
    //				  //	&& !IN_XSPACE(SPEC_OCLS(getSpec(OP_SYM_TYPE(IC_RIGHT(srcI2)))))
    //				  //	)
    //				  //{
    //				  //	IC_RIGHT(ic) = IC_RIGHT(srcI2);
    //				  //	canBeSimplified = 1;
    //				  //	break;
    //				  //}
    //			  }
    //		  }
    //		  break;
    //	  }
    //	  else
    //		  break;

    //  }
    // }
    // if (canBeSimplified)
    // {
    //  int offset2 = IC_RIGHT(srcI)->svt.valOperand->type->select.s.const_val.v_int;

    //  genAssign2(ic, 0, offset2);
    //  ic->generated = 1;
    //  goto release;

    // }
    //}

    /* If the pointer value is not in a the FSR then need to put it in */
    /* Must set/reset IRP bit for use with FSR. */
    /* Note only do this once - assuming that never need to cross a bank boundary at address 0x100. */
    if (!direct)
        setup_fsr (result);
    else
    {
        //int size = AOP_SIZE(result); // note that result is pointer
        int sizer = AOP_SIZE(right);
        int offset = 0;
		unsigned long long lit; 
        
        DEBUGHY08A_emitcode("; ***", "%s  %d", __FUNCTION__, __LINE__);

		if (AOP(result)->type == AOP_LIT && ((!h08d && (lit = ullFromVal(AOP(result)->aopu.aop_lit)) < 256) ||
            (h08d && (lit = ullFromVal(AOP(result)->aopu.aop_lit)) < 128)))
		{
			while (sizer--)
			{
				if (aop_isLitLike(AOP(right)))
				{
					if (AOP(right)->type == AOP_LIT)
					{
						int litr = (int) (ullFromVal(AOP(right)->aopu.aop_lit)>>(8*offset));
						if (litr == 0)
							emitpcode(POC_CLRF, popGetLit((unsigned int)(lit++)));
						else if(litr==0xff)
							emitpcode(POC_SETF, popGetLit((unsigned int)(lit++)));
						else
						{
							emitpcode(POC_MVL, popGetLit(litr));
							emitpcode(POC_MVWF, popGetLit((unsigned int)(lit++)));
						}
					}
				}
				else
				{
					emitpcode(POC_MVFW, popGet(AOP(right), offset));
					emitpcode(POC_MVWF, popGetLit((unsigned int)(lit++))); // low to high!!
				}
				offset++;
			}
		}

		else
		{
			lit = 0;
			if(AOP(result)->type == AOP_LIT)
				lit = ullFromVal(AOP(result)->aopu.aop_lit);
			while (sizer--)
			{

				if (aop_isLitLike(AOP(right)))
				{
					emitpcode(POC_MVL, popGet(AOP(right), offset));
                    if((HY08A_getPART()->isEnhancedCore >= 2 && HY08A_getPART()->isEnhancedCore != 5)
                    //|| !strcmp(HY08A_getPART()->name,"HY17P56")
                    //|| !strcmp(HY08A_getPART()->name,"HY17P55")
                        ) // gauge is also ptrequ
                    {
					char buf[512];
                    char bufr[600];
                    //static int equsaveid2=0;
                    snprintf(buf,511, "_PTREQU_%X",(unsigned int)((lit+offset)&0xfff));
                    snprintf(bufr,599,"_%s",buf);
                    if(!regFindWithName(bufr))
                    {
                        sym_link *the_type = newLink(SPECIFIER);
                        the_type->select.s.b_absadr=1;
                        the_type->select.s._addr=(unsigned int)(lit+offset);
                        the_type->select.s.noun = V_CHAR;
                        the_type->select.s.oclass = sfr;
                        the_type->select.s.b_volatile=1;// it is a pointer, later different name will access the same addr
                        symbol *newsym=newSymbol(buf,0);
                        strncpy(newsym->rname,bufr,sizeof(newsym->rname)-1);
                        newsym->type=the_type;
                        newsym->etype=the_type;
                        //  symbol *csym = findSymWithLevel (SymbolTab, newsym);
                        // if(!csym) 
                        allocGlobal(newsym);
                    }
                    emitpcode(POC_MVWF, newpCodeOpRegFromStr(bufr));
                    }else if(!fptr)// near pointer direct mvwf
                    {
                        emitpcode(POC_MVWF,popGet2(AOP(result), offset,1));
                    }else 
                    {
                        emitpcode2(POC_MVFF,PCOP(&pc_wreg), popGet2(AOP(result), offset,1));
                    }
				}
				else
				{
					if (IS_SYMOP(right) && OP_SYMBOL(right)->isitmp && 
                        ((HY08A_getPART()->isEnhancedCore >= 3 && HY08A_getPART()->isEnhancedCore != 5)
                    //     || !strcmp(HY08A_getPART()->name,"HY17P56")
                    //|| !strcmp(HY08A_getPART()->name,"HY17P55")
                    )
                    )

					{
                        if(AOP(result)->type == AOP_LIT && (lit = ullFromVal(AOP(result)->aopu.aop_lit)) < 128)
                        {
                            emitpcode(POC_MVFW, popGet2(AOP(right), offset,1));
						    emitpcode(POC_MVWF, popGet2(AOP(result), offset,1));
                        }
                        else 
                        {

                            char buf[512];
                            char bufr[600];
                            //static int equsaveid2=0;
                            snprintf(buf,511, "_PTREQU_%X",(unsigned int)((lit+offset)&0xfff));
                            snprintf(bufr,599,"_%s",buf);
                            if(!regFindWithName(bufr))
                            {
                                sym_link *the_type = newLink(SPECIFIER);
                                the_type->select.s.b_absadr=1;
                                the_type->select.s._addr=(unsigned int)(lit+offset);
                                the_type->select.s.noun = V_CHAR;
                                the_type->select.s.oclass = sfr;
                                the_type->select.s.b_volatile=1;// it is a pointer, later different name will access the same addr
                                symbol *newsym=newSymbol(buf,0);
                                strncpy(newsym->rname,bufr,sizeof(newsym->rname)-1);
                                newsym->type=the_type;
                                newsym->etype=the_type;
                            //  symbol *csym = findSymWithLevel (SymbolTab, newsym);
                            // if(!csym) 
                                allocGlobal(newsym);
                            }
                            emitpcode(POC_MVFW, popGet2(AOP(right), offset,1));
                            emitpcode(POC_MVWF, newpCodeOpRegFromStr(bufr));
                        }
                        // }else
                        // {

						//     emitpcode(POC_MVFW, popGet2(AOP(right), offset,1));
						//     emitpcode(POC_MVWF, popGet2(AOP(result), offset,1));
                        // }
					}
					else
					{
						emitpcode22(POC_MVFF, popGet2(AOP(right), offset, 1), popGet2(AOP(result), offset, 1));
					}
				}
				offset++;

			}
		}
        goto release;
    }

    {
        /* we have can just get the values */
        int size = AOP_SIZE (right);

        int offset = 0;

        DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);

		if (AOP_TYPE(right) == AOP_LIT)
		{
			int litnow = -1;
			
			for (offset = 0; offset < size; offset++)
			{
				pCodeOp *litop = popGetAddr(AOP(right), offset, 0);
				if (PCOL(litop)->lit == 0)
				{
					emitpcode(POC_CLRF, (size==1)? PCOP(&pc_indf0):PCOP(&pc_poinc0));
				}
				else if (PCOL(litop)->lit == 0xff)
				{
					emitpcode(POC_SETF, (size == 1) ? PCOP(&pc_indf0) : PCOP(&pc_poinc0));
				}
				else
				{
					if (PCOL(litop)->lit != litnow)
					{
						emitpcode(POC_MVL, litop);
						litnow = PCOL(litop)->lit;
					}
					emitpcode(POC_MVWF, (size == 1) ? PCOP(&pc_indf0) : PCOP(&pc_poinc0));
				}
			}
		}
		else
		{

			while (size--)
			{
				// char *l = aopGet (AOP (right), offset, FALSE, TRUE);
				/*       if (*l == '@')
						 {
						   emitpcode (POC_MVFW, popCopyReg (pc_poinc));
						 }
					   else*/
				{
					//if (AOP_TYPE (right) == AOP_LIT)



					if (op_isLitLike(right))
					{
						emitpcode(POC_MVL, popGetAddr(AOP(right), offset, 0));
					}
					else
					{
						emitpcode(POC_MVFW, popGet(AOP(right), offset));
					}
					if (direct)
						emitpcode(POC_MVWF, popGet(AOP(result), 0));
					else
						emitpcode(POC_MVWF, (size == 0) ? PCOP(&pc_indf0) : PCOP(&pc_poinc0));

				}
				/* if (size && !direct)
				   inc_fsr (1);*/
				offset++;
			}
		}
    }

    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);
    /* now some housekeeping stuff */
    if (aop)
    {
        /* we had to allocate for this iCode */
        freeAsmop (NULL, aop, ic, TRUE);
    }
    else if (!direct)
    {
        /* nothing to do */
    }
    else
    {
        /* we did not allocate which means left
           already in a pointer register, then
           if size > 0 && this could be used again
           we have to point it back to where it
           belongs */
        DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);
        if (AOP_SIZE (right) > 0 && !OP_SYMBOL (result)->remat && !OP_SYMBOL(result)->rematX && (OP_SYMBOL (result)->liveTo > ic->seq || ic->depth))
        {
            int size = AOP_SIZE (right) ;
            //inc_fsr (-size);
            emitpcode(POC_MVL, newpCodeOpLit(256 - size));
            emitpcode(POC_MVFW, PCOP(&pc_plusw0));

        }
    }

    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);
    /* done */
release:
    freeAsmop (right, NULL, ic, TRUE);
    freeAsmop (result, NULL, ic, TRUE);
}

/*-----------------------------------------------------------------*/
/* genNearPointerSet - HY08A_emitcode for near pointer put         */
/*-----------------------------------------------------------------*/
//static void
//genNearPointerSetOffset(operand * right, operand * result, operand *offsetv, iCode * ic, operand *inc)
//{
//    asmop *aop = NULL;
//    sym_link *ptype = operandType(result);
//    sym_link *retype = getSpec(operandType(right));
//    sym_link *letype = getSpec(ptype);
//    int direct = 0;
//    int hwptr = 0;
//
//
//    FENTRY;
//    DEBUGHY08A_emitcode("; ***", "%s  %d", __FUNCTION__, __LINE__);
//    aopOp(result, ic, FALSE);
//
//
//    DEBUGHY08A_emitcode("; ***", "%s  %d", __FUNCTION__, __LINE__);
//    aopOp(right, ic, FALSE);
//    DEBUGHY08A_AopType(__LINE__, NULL, right, result);
//    aopOp(inc, ic, FALSE);
//
//    /* Check if can access directly instead of via a pointer */
//    // not possible
//    //if ((AOP_TYPE(result) == AOP_PCODE) && (AOP(result)->aopu.pcop->type == PO_IMMEDIATE) && (AOP_SIZE(right) == 1))
//    //{
//    //	direct = 1;
//    //}
//    // we check if hwptr, 0 means not hwptr, 1 means hwptr no inc, 2 means hwptr with inc
//
//
//    if (IS_BITFIELD(letype))
//    {
//        genPackBits(letype, result, right, direct ? -1 : POINTER);
//        return;
//    }
//
//    /* If the pointer value is not in a the FSR then need to put it in */
//    /* Must set/reset IRP bit for use with FSR. */
//    /* Note only do this once - assuming that never need to cross a bank boundary at address 0x100. */
//    if (!direct)
//        setup_fsr(result);
//
//    {
//        /* we have can just get the values */
//        int size = AOP_SIZE(right);
//        int offset = 0;
//        int ramoffset = (int)ulFromVal(offsetv);
//        int first = 1;
//        emitpcode(POC_MVL, popGetLit(ramoffset & 0xff));
//        emitpcode(POC_MVWF, popCopyReg(&pc_plusw0));
//
//        DEBUGHY08A_emitcode("; ***", "%s  %d", __FUNCTION__, __LINE__);
//        while (size--)
//        {
//            // char *l = aopGet (AOP (right), offset, FALSE, TRUE);
//            /*       if (*l == '@')
//            {
//            emitpcode (POC_MVFW, popCopyReg (pc_poinc));
//            }
//            else*/
//            {
//                if (AOP_TYPE(right) == AOP_LIT)
//                {
//                    emitpcode(POC_MVL, popGet(AOP(right), offset));
//                }
//                else
//                {
//                    emitpcode(POC_MVFW, popGet(AOP(right), offset));
//                }
//                /*if (direct)
//                	emitpcode(POC_MVWF, popGet(AOP(result), 0));
//                else*/
//
//                emitpcode(POC_MVWF, popCopyReg(&pc_poinc0));
//            }
//            /* if (size && !direct)
//            inc_fsr (1);*/
//            offset++;
//        }
//        // after write, result should be set
//        // it will be optimized further if not used
//        emitpcode(POC_MVFW, popCopyReg(&pc_fsr0l));
//        emitpcode(POC_MVWF, popGet(AOP(inc), 0));
//        emitpcode(POC_MVFW, popCopyReg(&pc_fsr0h));
//        emitpcode(POC_MVWF, popGet(AOP(inc), 1));
//        ic->generated = 1;
//
//    }
//
//    DEBUGHY08A_emitcode("; ***", "%s  %d", __FUNCTION__, __LINE__);
//    /* now some housekeeping stuff */
//    if (aop)
//    {
//        /* we had to allocate for this iCode */
//        freeAsmop(NULL, aop, ic, TRUE);
//    }
//    else if (!direct)
//    {
//        /* nothing to do */
//    }
//    else
//    {
//        /* we did not allocate which means left
//        already in a pointer register, then
//        if size > 0 && this could be used again
//        we have to point it back to where it
//        belongs */
//        DEBUGHY08A_emitcode("; ***", "%s  %d", __FUNCTION__, __LINE__);
//        if (AOP_SIZE(right) > 0 && !OP_SYMBOL(result)->remat && !OP_SYMBOL(result)->rematX && (OP_SYMBOL(result)->liveTo > ic->seq || ic->depth))
//        {
//            int size = AOP_SIZE(right);
//            //inc_fsr (-size);
//            emitpcode(POC_MVL, newpCodeOpLit(256 - size));
//            emitpcode(POC_MVFW, popCopyReg(&pc_plusw0));
//
//        }
//    }
//
//    DEBUGHY08A_emitcode("; ***", "%s  %d", __FUNCTION__, __LINE__);
//    /* done */
//
//    freeAsmop(right, NULL, ic, TRUE);
//    freeAsmop(result, NULL, ic, TRUE);
//}




/*-----------------------------------------------------------------*/
/* genGenPointerSet - set value from generic pointer space         */
/*-----------------------------------------------------------------*/
static void
genGenPointerSet (operand * right, operand * result, iCode * ic)
{
    sym_link *retype = getSpec (operandType (result));

    FENTRY;
    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);
    aopOp (right, ic, FALSE);
    aopOp (result, ic, FALSE);


    DEBUGHY08A_AopType (__LINE__, right, NULL, result);

    if (IS_BITFIELD (retype))
    {
        genPackBits (retype, result, right, GPOINTER);
        return;
    }

	{
		/* emit call to __gptrput */
		char *func[] = { NULL, "__gptrput1", "__gptrput2", "__gptrput3", "__gptrput4",
		NULL,NULL,NULL,"__gptrput8" };
		char *func2[] = { NULL, "__g2ptrput1", "__g2ptrput2", "__g2ptrput3", "__g2ptrput4",
		NULL,NULL,NULL,"__g2ptrput8" };
		int size = AOP_SIZE(right);
		int idx = 0;

		/* The following assertion fails for
		 *   struct foo { char a; char b; } bar;
		 *   void demo(struct foo *dst, char c) { dst->b = c; }
		 * as size will be 1 (sizeof(c)), whereas dst->b will be accessed
		 * using (((char *)dst)+1), whose OP_SYM_ETYPE still is struct foo
		 * of size 2.
		 * The frontend seems to guarantee that IC_LEFT has the correct size,
		 * it works fine both for larger and smaller types of `char c'.
		 * */
		 //assert (size == getSize(OP_SYM_ETYPE(result)));
		assert(size > 0 && size <= 8);

		/* pass arguments */
		/* - value (MSB in Gstack_base_addr-2, growing downwards) */
		{
			int off = size;
			idx = 2;

			if (HY08A_getPART()->isEnhancedCore !=2 )
				idx = 1;
			// 2016 Nov, fix the order
			// formerly base-2 , base-3, base-4
			// change to base-4 .. -3 ..-2
			while (off--)
			{
				mov2w_op(right, off);
				//emitpcode (POC_MVWF, popRegFromIdx (Gstack_base_addr - idx++));
				if (HY08A_getPART()->isEnhancedCore != 2)
					emitpcode(POC_MVWF, popRegFromIdx2(Gstack_base_addr - (size - 1) - idx--, func2[size], 1, 0));
				else
					emitpcode(POC_MVWF, popRegFromIdx2(Gstack_base_addr - (size - 1) - idx--, func[size], 1, 0));
			}
			idx = 0;
		}
		/* - address */
		assert(AOP_SIZE(result) >= 2);

		if (HY08A_getPART()->isEnhancedCore == 2)
		{
			mov2w(AOP(result), 0);
			emitpcode(POC_MVWF, popRegFromIdx2(Gstack_base_addr - 1, func[size], 1, 0));
			mov2w(AOP(result), 1);

			emitpcode(POC_MVWF, popRegFromIdx2(Gstack_base_addr, func[size], 1, 0));
			if (AOP_SIZE(result) < 3)
				emitpcode(POC_MVL, popGetLit(0));
			else
				mov2w(AOP(result), 2);
			call_libraryfunc(func[size]);
		}
		else
		{
			mov2w(AOP(result), 0);
			emitpcode(POC_MVWF, popRegFromIdx2(Gstack_base_addr , func2[size], 1, 0));
			mov2w(AOP(result), 1);
			call_libraryfunc(func2[size]);
		}

	}

    freeAsmop (right, NULL, ic, TRUE);
    freeAsmop (result, NULL, ic, TRUE);
}

/*-----------------------------------------------------------------*/
/* genPointerSet - stores the value into a pointer location        */
/*-----------------------------------------------------------------*/
static void
genPointerSet (iCode * ic)
{
    operand *right, *result;
    sym_link *type, *etype;
    int p_type;

    FENTRY;
    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);

    right = IC_RIGHT (ic);
    result = IC_RESULT (ic);

    /* depending on the type of pointer we need to
       move it to the correct pointer register */
    type = operandType (result);
    etype = getSpec (type);
    /* if left is of type of pointer then it is simple */
    if (IS_PTR (type) && !IS_FUNC (type->next))
    {
        p_type = DCL_TYPE (type);
    }
    else
    {
        /* we have to go by the storage class */
        p_type = PTR_TYPE (SPEC_OCLS (etype));

        /*  if (SPEC_OCLS(etype)->codesp ) { */
        /*      p_type = CPOINTER ;  */
        /*  } */
        /*  else */
        /*      if (SPEC_OCLS(etype)->fmap && !SPEC_OCLS(etype)->paged) */
        /*    p_type = FPOINTER ; */
        /*      else */
        /*    if (SPEC_OCLS(etype)->fmap && SPEC_OCLS(etype)->paged) */
        /*        p_type = PPOINTER ; */
        /*    else */
        /*        if (SPEC_OCLS(etype) == idata ) */
        /*      p_type = IPOINTER ; */
        /*        else */
        /*      p_type = POINTER ; */
    }

    /* now that we have the pointer type we assign
       the pointer values */
    switch (p_type)
    {


    case POINTER:

        //case IPOINTER:
        // note that FSR012 will be processed, if post inc 1
        genNearPointerSet(right, result, ic, 0);
        break;

    case FPOINTER:
        genNearPointerSet (right, result, ic,1);
        break;
        /*
           case PPOINTER:
           genPagedPointerSet (right,result,ic);
           break;

           case FPOINTER:
           genFarPointerSet (right,result,ic);
           break;
         */
    case GPOINTER:
        if(IS_VALOP(result))
        {
            if( ullFromVal (OP_VALUE (result))<0x1000) // pointer can access to 0x1000
            {
                genNearPointerSet(right, result, ic, 1);
                return;
            }else 
            {
                fprintf(stderr,"Error, write to address >= 0x1000 is not allowed at line %d.\n", ic->lineno);
                exit(-__LINE__);
            }
        } 
        genGenPointerSet (right, result, ic);
        break;

    default:
        werror (E_INTERNAL_ERROR, __FILE__, __LINE__, "genPointerSet: illegal pointer type");
    }
}

/*-----------------------------------------------------------------*/
/* genIfx - generate code for Ifx statement                        */
/*-----------------------------------------------------------------*/
static void
genIfx (iCode * ic, iCode * popIc)
{
    operand *cond = IC_COND (ic);
    int isbit = 0;

    FENTRY;
    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);

	// special case for bug-2627.c
	if (IS_SYMOP(cond) && cond->aop == NULL && OP_SYMBOL(cond)->isitmp && OP_SYMBOL(cond)->regType == REG_CND)
	{
		symbol *sym=OP_SYMBOL(cond);
		cond->aop = sym->aop = newAsmop(AOP_REG);
		cond->aop->size = 1;
		if (sym->nRegs == 0)
		{
			sym->nRegs = 1;
			sym->regs[0] = HY08A_findFreeReg(REG_GPR);
			cond->aop->aopu.aop_reg[0] = sym->regs[0];
		}
		else
		{
			cond->aop->aopu.aop_reg[0] = sym->regs[0];
		}
	}
	else
		aopOp (cond, ic, FALSE);

    /* get the value into acc */
    if (AOP_TYPE (cond) != AOP_CRY)
        HY08A_toBoolean (cond);
    else
        isbit = 1;

    /* if there was something to be popped then do it */
    if (popIc)
        genIpop (popIc);

    if (isbit)
    {
        /* This assumes that CARRY is set iff cond is true */
        if (IC_TRUE (ic))
        {
            assert (!IC_FALSE (ic));
            emitpcode (POC_BTSZ, popGet (AOP (cond), 0));
            //emitSKPNC;
            emitpcode (POC_JMP, popGetLabel (IC_TRUE (ic)->key));
        }
        else
        {
            assert (IC_FALSE (ic));
            emitpcode (POC_BTSS, popGet (AOP (cond), 0));
            //emitSKPC;
            emitpcode (POC_JMP, popGetLabel (IC_FALSE (ic)->key));
        }
        if (0)
        {
            static int hasWarned = 0;
            if (!hasWarned)
            {
                fprintf (stderr, "WARNING: using untested code for %s:%u -- please check the .asm output and report bugs.\n",
                         ic->filename, ic->lineno);
                hasWarned = 1;
            }
        }
    }
    else
    {
        /* now Z is set iff !cond */
        if (IC_TRUE (ic))
        {
            assert (!IC_FALSE (ic));
            emitSKPZ;
            emitpcode (POC_JMP, popGetLabel (IC_TRUE (ic)->key));
        }
        else
        {
            emitSKPNZ;
            emitpcode (POC_JMP, popGetLabel (IC_FALSE (ic)->key));
        }
    }

    ic->generated = 1;

    /* the result is now in the accumulator */
    freeAsmop (cond, NULL, ic, TRUE);
}

/*-----------------------------------------------------------------*/
/* genAddrOf - generates code for address of                       */
/*-----------------------------------------------------------------*/
static void
genAddrOf (iCode * ic)
{
    operand *right, *result, *left;
    int size, offset;
    int size1;

    symbol *opsym;
//  int isArray;

    FENTRY;
    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);


    //aopOp(IC_RESULT(ic),ic,FALSE);

    aopOp ((left = IC_LEFT (ic)), ic, FALSE);
    aopOp ((right = IC_RIGHT (ic)), ic, FALSE);
    aopOp ((result = IC_RESULT (ic)), ic, TRUE);

    DEBUGHY08A_AopType (__LINE__, left, right, result);
    assert (IS_SYMOP (left));

    /* sanity check: generic pointers to code space are not yet supported,
     * pionters to codespace must not be assigned addresses of __data values. */
#if 0
    fprintf (stderr, "result: %s, left: %s\n", OP_SYMBOL (result)->name, OP_SYMBOL (left)->name);
    fprintf (stderr, "result->type : ");
    printTypeChain (OP_SYM_TYPE (result), stderr);
    fprintf (stderr, ", codesp:%d, codeptr:%d, constptr:%d\n", IN_CODESPACE (SPEC_OCLS (getSpec (OP_SYM_TYPE (result)))),
             IS_CODEPTR (OP_SYM_TYPE (result)), IS_PTR_CONST (OP_SYM_TYPE (result)));
    fprintf (stderr, "result->etype: ");
    printTypeChain (OP_SYM_ETYPE (result), stderr);
    fprintf (stderr, ", codesp:%d, codeptr:%d, constptr:%d\n", IN_CODESPACE (SPEC_OCLS (getSpec (OP_SYM_ETYPE (result)))),
             IS_CODEPTR (OP_SYM_ETYPE (result)), IS_PTR_CONST (OP_SYM_ETYPE (result)));
    fprintf (stderr, "left->type   : ");
    printTypeChain (OP_SYM_TYPE (left), stderr);
    fprintf (stderr, ", codesp:%d, codeptr:%d, constptr:%d\n", IN_CODESPACE (SPEC_OCLS (getSpec (OP_SYM_TYPE (left)))),
             IS_CODEPTR (OP_SYM_TYPE (left)), IS_PTR_CONST (OP_SYM_TYPE (left)));
    fprintf (stderr, "left->etype  : ");
    printTypeChain (OP_SYM_ETYPE (left), stderr);
    fprintf (stderr, ", codesp:%d, codeptr:%d, constptr:%d\n", IN_CODESPACE (SPEC_OCLS (getSpec (OP_SYM_ETYPE (left)))),
             IS_CODEPTR (OP_SYM_ETYPE (left)), IS_PTR_CONST (OP_SYM_ETYPE (left)));
#endif

    if (IS_SYMOP (result) && IS_CODEPTR (OP_SYM_TYPE (result)) && !IN_CODESPACE (SPEC_OCLS (getSpec (OP_SYM_TYPE (left)))))
    {
        fprintf (stderr, "trying to assign __code pointer (%s) an address in __data space (&%s) -- expect trouble\n",
                 IS_SYMOP (result) ? OP_SYMBOL (result)->name : "unknown", OP_SYMBOL (left)->name);
    }
    else if (IS_SYMOP (result) && !IS_CODEPTR (OP_SYM_TYPE (result)) && IN_CODESPACE (SPEC_OCLS (getSpec (OP_SYM_TYPE (left)))))
    {
        fprintf (stderr, "trying to assign __data pointer (%s) an address in __code space (&%s) -- expect trouble\n",
                 IS_SYMOP (result) ? OP_SYMBOL (result)->name : "unknown", OP_SYMBOL (left)->name);
    }

    size = AOP_SIZE (IC_RESULT (ic));
    if (IS_SYMOP (result) && IS_GENPTR (OP_SYM_TYPE (result)))
    {
        /* strip tag */
        if (size > GPTRSIZE - 1)
            size = GPTRSIZE - 1;
    }
    offset = 0;

    opsym = OP_SYMBOL(left);

    size1 = getSize(opsym->type);

	// consider PUSH means PRINC
	// that is, final para number is pointed by FSR2,
	// if left [right most para!!] is 1 byte, the address should be FSR2-1
	// 2 byte is FSR2-2
	if (AOP(left)->type == AOP_STK)
	{
		// final push should count 0
		int theShift =	(AOP(left)->aopu.aop_stk+3) ; // now p41 has 1 byte
		//pCodeOpLit *pcoloff = newpCodeOpLit(theShift&0xff);
		//pcoloff->addSPOffSet = 1; // this is important!!
		char sztmp[32];
		 SNPRINTF(sztmp, 31, "@FSR2-0x%02X", (0-theShift)&0xff);
		//SNPRINTF(sztmp,1023,"@P1,%d",aop->aopu.aop_stk+offset);
		pCodeOp *pcop = popRegFromString(sztmp, 1, 0);
		emitpcode(POC_MVL, pcop);  // because singe pointer operation , we need offset by lines
		emitpcode(POC_SUBFWW , popGetWithString("_FSR2L", 0));



		emitpcode(POC_MVWF, popGet(AOP(result), 0));
		// it will be many case
		if (HY08A_getPART()->isEnhancedCore == 3)
		{

			emitpcode(POC_CLRF, popGet(AOP(result), 1)); // for H08D, we limit to 0x80~17F
		}
		else
		{
			emitpcode(POC_MVL, popGetLit(0)); // offset>128 is not allowed!!
			emitpcode(POC_SBCFWW, popGetWithString("_FSR2H", 0));
			emitpcode(POC_MVWF, popGet(AOP(result), 1));
		}
	}
	else
	{

		while (size--)
		{
			/* fixing bug #863624, reported from (errolv) */
			emitpcode(POC_MVL, popGetImmd(OP_SYMBOL(left)->rname, offset, 0, IS_FUNC(OP_SYM_TYPE(left)),
				size1, IN_XSPACE(SPEC_OCLS(opsym->etype)), opsym->onStack ? pb : NULL));
			emitpcode(POC_MVWF, popGet(AOP(result), offset));

#if 0
			emitpcode(POC_MVL, popGet(AOP(left), offset));
			emitpcode(POC_MVWF, popGet(AOP(result), offset));
#endif
			offset++;
		}

		if (IS_SYMOP(result) && IS_GENPTR(OP_SYM_TYPE(result)))
		{
			/* provide correct tag */
			int isCode = IN_CODESPACE(SPEC_OCLS(getSpec(OP_SYM_TYPE(left))));
			emitpcode(POC_MVL, popGetLit(isCode ? GPTRTAG_CODE : GPTRTAG_DATA));
			movwf(AOP(result), 2);
		}
	}

    freeAsmop (left, NULL, ic, FALSE);
    freeAsmop (result, NULL, ic, TRUE);

}

static int canAssignSelf(char *regname)
{
    if (!strncmp(regname, "_SSPBUF", 8))
        return 1;
    if (!strncmp(regname, "_POINC", 6))
        return 1;
    if (!strncmp(regname, "_PODEC", 6))
        return 1;
    return 0;
}

/*-----------------------------------------------------------------*/
/* genAssign - generate code for assignment                        */
/*-----------------------------------------------------------------*/


static void
genAssign (iCode * ic)
{
    operand *result, *right;
    int size, offset, know_W;
    unsigned long lit = 0L;

    result = IC_RESULT (ic);
    right = IC_RIGHT (ic);

    FENTRY;

    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);

    /* if they are the same */
    if (operandsEqu (IC_RESULT (ic), IC_RIGHT (ic)) &&
            !IC_RESULT(ic)->isvolatile)
        return;

    aopOp (right, ic, FALSE);
    aopOp (result, ic, TRUE);

    DEBUGHY08A_AopType (__LINE__, NULL, right, result);

    /* if they are the same registers */
    if (!right->isvolatile && HY08A_sameRegs (AOP (right), AOP (result)) )
        //&& (AOP(right)->type != AOP_DIR || AOP(right)->aopu.aop_dir==NULL ||
        //!canAssignSelf(AOP(right)->aopu.aop_dir)))
        goto release;

    /* special case: assign from __code */
    if (!IS_ITEMP (right)         /* --> iTemps never reside in __code */
            && IS_SYMOP (right)       /* --> must be an immediate (otherwise we would be in genConstPointerGet) */
            && !IS_FUNC (OP_SYM_TYPE (right)) /* --> we would want its address instead of the first instruction */
            && !IS_CODEPTR (OP_SYM_TYPE (right))      /* --> get symbols address instread */
            && IN_CODESPACE (SPEC_OCLS (getSpec (OP_SYM_TYPE (right)))))
    {
        emitpComment ("genAssign from CODESPACE");
        genConstPointerGet (right, result, ic);
        goto release;
    }

    /* just for symmetry reasons... */
    if (!IS_ITEMP (result) && IS_SYMOP (result) && IN_CODESPACE (SPEC_OCLS (getSpec (OP_SYM_TYPE (result)))))
    {
        assert (!"cannot write to CODESPACE");
    }

    /* if the result is a bit */
    if (AOP_TYPE (result) == AOP_CRY)
    {

        /* if the right size is a literal then
           we know what the value is */
        if (AOP_TYPE (right) == AOP_LIT)
        {

            emitpcode ((((int) operandLitValue (right)) ? POC_BSF : POC_BCF), popGet (AOP (result), 0));

            if (((int) operandLitValue (right)))
                HY08A_emitcode ("bsf", "(%s >> 3),(%s & 7)", AOP (result)->aopu.aop_dir, AOP (result)->aopu.aop_dir);
            else
                HY08A_emitcode ("bcf", "(%s >> 3),(%s & 7)", AOP (result)->aopu.aop_dir, AOP (result)->aopu.aop_dir);
            goto release;
        }

        /* the right is also a bit variable */
        if (AOP_TYPE (right) == AOP_CRY)
        {
            emitpcode (POC_BCF, popGet (AOP (result), 0));
            emitpcode (POC_BTSZ, popGet (AOP (right), 0));
            emitpcode (POC_BSF, popGet (AOP (result), 0));

            HY08A_emitcode ("bcf", "(%s >> 3),(%s & 7)", AOP (result)->aopu.aop_dir, AOP (result)->aopu.aop_dir);
            HY08A_emitcode ("btfsc", "(%s >> 3),(%s & 7)", AOP (right)->aopu.aop_dir, AOP (right)->aopu.aop_dir);
            HY08A_emitcode ("bsf", "(%s >> 3),(%s & 7)", AOP (result)->aopu.aop_dir, AOP (result)->aopu.aop_dir);
            goto release;
        }

        /* we need to or */
        emitpcode (POC_BCF, popGet (AOP (result), 0));
        HY08A_toBoolean (right);
        emitSKPZ;
        emitpcode (POC_BSF, popGet (AOP (result), 0));
        //aopPut(AOP(result),"a",0);
        goto release;
    }

    /* bit variables done */
    /* general case */
    size = AOP_SIZE (result);
    offset = 0;
    // xdata special case
    if (AOP_TYPE(result) == AOP_XDATA && ( HY08A_getPART()->isEnhancedCore < 3 || HY08A_getPART()->isEnhancedCore == 5))
    {
        // XDATA copy uses MVFF
        if (!aop_isLitLike(AOP(right)))
        {
            int i;
            for (i = 0; i < size; i++)
            {
				emitpcode22(POC_MVFF, popGet(AOP(right), i), popGet2(AOP(result), i,1)); 
            }
        }
        else
        {
			int w_now = -1;
            XDATAA2PTR(result);//LDPR only 0
			
            for (offset = 0; offset < size; offset++)
            {

                if (AOP_TYPE(right) == AOP_LIT)
                {
                    lit = (unsigned long)HY08AaopLiteral(AOP(right)->aopu.aop_lit, offset) & 0x0ff;
					if(lit == 0)
						emitpcode(POC_CLRF, PCOP_POINC0);
					else if(lit==0xff)
						emitpcode(POC_SETF, PCOP_POINC0);
					else
                    {
						if (w_now != lit)
						{
							emitpcode(POC_MVL, popGetLit(lit & 0xff));
							w_now = lit;
						}
                        emitpcode(POC_MVWF, PCOP_POINC0);
                    }
                }
                else if(AOP_TYPE(right)==AOP_CRY)
                {

                    if (offset == 0)
                    {
                        emitpcode(POC_CLRF, PCOP_INDF0);
                        emitpcode(POC_BTSS, popGet(AOP(right), 0));
                        emitpcode(POC_INF, PCOP_INDF0);

                    }
                    else
                    {
                        emitpcode(POC_CLRF, popGetWithString("_PRINC0", 1));
                    }
                }
                else
                {
                    mov2w_op(right, offset);
                    emitpcode(POC_MVWF, PCOP_POINC0);
                }

            }

        }
        goto release;

    }
    if (AOP_TYPE (right) == AOP_DIR && (AOP_TYPE (result) == AOP_REG) && size == 1)
    {
        DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);
        if (aopIdx (AOP (result), 0) == 4)
        {
            DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);
            emitpcode (POC_MVFW, popGet (AOP (right), offset));
            emitpcode (POC_MVWF, popGet (AOP (result), offset));
            goto release;
        }
        else
            DEBUGHY08A_emitcode ("; WARNING", "%s  %d ignoring register storage", __FUNCTION__, __LINE__);
    }

    know_W = -1;
    while (size--)
    {

        DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);
        if (AOP_TYPE (right) == AOP_LIT)
        {
            lit = (unsigned long) HY08AaopLiteral (AOP (right)->aopu.aop_lit, offset) & 0x0ff;
            if (lit & 0xff)
            {

                if (lit == 0xff)
                {
                    emitpcode(POC_SETF, popGet(AOP(result), offset));
                }
                else
                {
                    if (know_W != (int)(lit & 0xff))
                        emitpcode(POC_MVL, popGetLit(lit & 0xff));
                    know_W = lit & 0xff;
                    emitpcode(POC_MVWF, popGet(AOP(result), offset));
                }
            }
            else
                emitpcode (POC_CLRF, popGet (AOP (result), offset));

        }
        else if (AOP_TYPE (right) == AOP_CRY)
        {
            emitpcode (POC_CLRF, popGet (AOP (result), offset));
            if (offset == 0)
            {
                emitpcode (POC_BTSZ, popGet (AOP (right), 0)); // if not 1, it is 0... 2024 find a bug?
                emitpcode (POC_INF, popGet (AOP (result), 0));
            }
        }
        else
        {
            if (IS_TRUE_SYMOP(right) && IN_XSPACE(SPEC_OCLS(OP_SYMBOL(right)->etype)))
            {
				emitpcode21(POC_MVFF, popGet2(AOP(right), offset,1), popGet(AOP(result), offset));
            }
            else
            {
                pCodeOp *pcop = popGet(AOP(result), offset);

				if (offset >= AOP_SIZE(right)) // for size issue, we should clear it directly!!
				{
					if (pcop->type != PO_W )
					{
						emitpcode(POC_CLRF, pcop);
					}else
						emitpcode(POC_MVL, popGetLit(0)); // better optimzation?
				}
				else
				{
					mov2w_op(right, offset);

					if ((!pcop->name) || pcop->type!=PO_W)
						emitpcode(POC_MVWF, pcop);
				}
            }
        }

        offset++;
    }


release:
    freeAsmop (right, NULL, ic, FALSE);
    freeAsmop (result, NULL, ic, TRUE);
}

//static void
//genAssign2(iCode * ic, int offset2, int offset3)
//{
    //operand *result, *right;
    //int size, offset, know_W;
    //unsigned long lit = 0L;
//
    //result = IC_RESULT(ic);
    //right = IC_RIGHT(ic);
//
    //FENTRY;
    //DEBUGHY08A_emitcode("; ***", "%s  %d", __FUNCTION__, __LINE__);
//
    /* if they are the same */
    //if (operandsEqu(IC_RESULT(ic), IC_RIGHT(ic)))
        //return;
//
    //aopOp(right, ic, FALSE);
    //aopOp(result, ic, TRUE);
//
//
//
    ///* bit variables done */
    ///* general case */
    //size = AOP_SIZE(result);
    //offset = 0;
    //// xdata special case
    //if (AOP_TYPE(result) == AOP_XDATA)
    //{
        //// XDATA copy uses MVFF
        //if (!aop_isLitLike(AOP(right)))
        //{
            //int i;
            //for (i = 0; i < size; i++)
            //{
				//emitpcode22(POC_MVFF, popGet(AOP(right), i + offset2), popGet(AOP(result), i + offset3));
            //}
        //}
        //else
        //{
//
//
            //fprintf(stderr, "assign2 internal error .. call support \n");
            //exit(-1003);
//
        //}
        //goto release;
//
    //}
    //if (AOP_TYPE(right) == AOP_DIR && (AOP_TYPE(result) == AOP_REG) && size == 1)
    //{
        //DEBUGHY08A_emitcode("; ***", "%s  %d", __FUNCTION__, __LINE__);
        //if (aopIdx(AOP(result), 0) == 4)
        //{
            //DEBUGHY08A_emitcode("; ***", "%s  %d", __FUNCTION__, __LINE__);
            //emitpcode(POC_MVFW, popGet(AOP(right), offset+offset2));
            //emitpcode(POC_MVWF, popGet(AOP(result), offset+offset3));
            //goto release;
        //}
        //else
            //DEBUGHY08A_emitcode("; WARNING", "%s  %d ignoring register storage", __FUNCTION__, __LINE__);
    //}
//
    //know_W = -1;
    //while (size--)
    //{
//
        //DEBUGHY08A_emitcode("; ***", "%s  %d", __FUNCTION__, __LINE__);
        //if (AOP_TYPE(right) == AOP_LIT) // re-mat
        //{
            //fprintf(stderr, "assign2 internal error .. call support \n");
            //exit(-1004);
//
        //}
        //else if (AOP_TYPE(right) == AOP_CRY)
        //{
            //fprintf(stderr, "assign2 internal error .. call support \n");
            //exit(-1005);
//
        //}
        //else
        //{
            //mov2w_op(right, offset+offset2);
            //emitpcode(POC_MVWF, popGet(AOP(result), offset+offset3));
        //}
//
        //offset++;
    //}
//
//
//release:
    //freeAsmop(right, NULL, ic, FALSE);
    //freeAsmop(result, NULL, ic, TRUE);
//}
//
/*-----------------------------------------------------------------*/
/* genJumpTab - genrates code for jump table                       */
/*-----------------------------------------------------------------*/
static void
genJumpTab (iCode * ic)
{
    symbol *jtab;
//    char *l;

    FENTRY;
    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);

    aopOp (IC_JTCOND (ic), ic, FALSE);
    /* get the condition into accumulator */
    //l = aopGet (AOP (IC_JTCOND (ic)), 0, FALSE, FALSE);
    //MOVA (l);
    /* multiply by three */
    //HY08A_emitcode ("add", "a,acc");
    //HY08A_emitcode ("add", "a,%s", aopGet (AOP (IC_JTCOND (ic)), 0, FALSE, FALSE));

    jtab = newiTempLabel (NULL);

	// 2020 Nov, use lib to read table directly
	// word count to byte count

    //HY08A_emitcode ("mov", "dptr,#%05d_DS_", labelKey2num (jtab->key));// these are no use??
    //HY08A_emitcode ("jmp", "@a+dptr");
    //HY08A_emitcode ("", "%05d_DS_:", labelKey2num (jtab->key));
	if(max_rom_addr>=0x10000)
		emitpcode(POC_CLRF, popCopyReg(&pc_pclatu));
    emitpcode (POC_MVL, popGetHighLabel (jtab->key));
    emitpcode (POC_MVWF, popCopyReg (&pc_pclath));
//    emitpcode (POC_MVL, popGetLabelD2 (jtab->key));
//    emitpcode (POC_ADDFW, popGet (AOP (IC_JTCOND (ic)), 0));
	emitpcode (POC_MVFW, popGet (AOP (IC_JTCOND (ic)), 0));
	emitpcode(POC_ADDLW, popGetLabelD2(jtab->key));
    emitSKPNC;
    emitpcode (POC_INF, popCopyReg (&pc_pclath));
    emitpcode (POC_MVWF, popCopyReg (&pc_pclatl));
    emitpLabel (jtab->key);

    freeAsmop (IC_JTCOND (ic), NULL, ic, TRUE);

    /* now generate the jump labels */
    for (jtab = setFirstItem (IC_JTLABELS (ic)); jtab; jtab = setNextItem (IC_JTLABELS (ic)))
    {
        HY08A_emitcode ("ljmp", "%05d_DS_", labelKey2num (jtab->key));
        emitpcode (POC_RJ, popGetLabel (jtab->key)); // it must be small !!

    }

}

/*-----------------------------------------------------------------*/
/* genCast - gen code for casting                                  */
/*-----------------------------------------------------------------*/
static void
genCast (iCode * ic)
{
    operand *result = IC_RESULT (ic);
    sym_link *restype = operandType (result);
    sym_link *rtype = operandType (IC_RIGHT (ic));
    operand *right = IC_RIGHT (ic);
    int size, offset;

    FENTRY;
    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);
    /* if they are equivalent then do nothing */

    aopOp (right, ic, FALSE);
    aopOp (result, ic, FALSE);

    if ((IC_RESULT(ic)->aop->imm2dir == 
        IC_RIGHT(ic)->aop->imm2dir ) && 
        operandsEqu (IC_RESULT (ic), IC_RIGHT (ic)))
        return;


    DEBUGHY08A_AopType (__LINE__, NULL, right, result);

    /* if the result is a bit */
    if (AOP_TYPE (result) == AOP_CRY)
    {
        //assert (!"assigning to bit variables is not supported");
		// we try assign by ...
		genAssign(ic);
		goto release;
    }

    if ((AOP_TYPE (right) == AOP_CRY) && (AOP_TYPE (result) == AOP_REG))
    {
        int offset = 1;
        size = AOP_SIZE (result);

        DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);


		//emitpcode(POC_MVL, popGetLit(0));
        emitpcode(POC_CLRF, popGet(AOP(result), 0));
		emitpcode (POC_BTSZ, popGet (AOP (right), 0));
		//emitpcode(POC_MVL, popGetLit(1));
		emitpcode(POC_INF, popGet(AOP(result), 0));

        while (--size)
            emitpcode (POC_CLRF, popGet (AOP (result), offset++));

        goto release;
    }

    if (IS_BOOL (operandType (result)))
    {
        HY08A_toBoolean (right);
        emitSKPZ;
        emitpcode (POC_MVL, popGetLit (1));
        emitpcode (POC_MVWF, popGet (AOP (result), 0));
        goto release;
    }

    if (IS_PTR (restype))
    {
        operand *result = IC_RESULT (ic);
        //operand *left = IC_LEFT(ic);
        operand *right = IC_RIGHT (ic);
        int tag = 0xff;
		int offset = 0;

        /* copy common part */
        int max, size = AOP_SIZE (result);
        if (size > AOP_SIZE (right))
            size = AOP_SIZE (right);
        DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);

        /* warn if we discard generic opinter tag */
        if (!IS_GENPTR (restype) && IS_GENPTR (rtype) && (AOP_SIZE (result) < AOP_SIZE (right)))
        {
            //fprintf (stderr, "%s:%u: discarding generic pointer type tag\n", __FUNCTION__, __LINE__);
        }                       // if

		if (right->type == SYMBOL && AOP(right)->type == AOP_PCODE && AOP(right)->aopu.pcop->type == PO_IMMEDIATE)
		{
			if (PCOI(AOP(right)->aopu.pcop)->r)
				PCOI(AOP(right)->aopu.pcop)->r->addrReferenced = 1; // pointer set with this pointer!!
            // 2024 4.4.4 
            if(IS_PTR_CONST(rtype))                
            {
                genConstPointerGet(right,result, ic);
                goto release;
            }
            
		}
        max = size;
        while (size--)
        {
            mov2w_op (right, offset); // change from low to high
			if (size == 0 && HY08A_getPART()->isEnhancedCore!=2 && IS_GENPTR(restype) && IS_CODEPTR(rtype))
			{
				if (PCI(pb->pcTail)->pcop->type == PO_IMMEDIATE) // it must exist!!
					PCOI(PCI(pb->pcTail)->pcop)->_cp2gp = 1;
				else
					emitpcode(POC_IORL, popGetLit(GPTRTAG_CODE));
			}
            movwf (AOP (result), offset++);
        }                       // while

        /* upcast into generic pointer type? */
		if (IS_GENPTR(restype) && (max < AOP_SIZE(result)) && (!IS_GENPTR(rtype) || AOP_SIZE(right) < GPTRSIZE))
		{
			//fprintf (stderr, "%s:%u: must determine pointer type\n", __FUNCTION__, __LINE__);
			if (IS_PTR(rtype))
			{
				switch (DCL_TYPE(rtype))
				{
				case POINTER:  /* __data */
				case FPOINTER: /* __data */
					assert(AOP_SIZE(right) == 2);
					tag = GPTRTAG_DATA;
					break;

				case CPOINTER: /* __code */
					assert(AOP_SIZE(right) == 2);
					tag = GPTRTAG_CODE;
					break;

				case GPOINTER: /* unknown destination, __data or __code */
					/* assume __data space (address of immediate) */
					//assert (AOP_TYPE (right) == AOP_PCODE && AOP (right)->aopu.pcop->type == PO_IMMEDIATE);
					if (AOP(right)->code)
						tag = GPTRTAG_CODE;
					else
						tag = GPTRTAG_DATA;
					break;

				default:
					assert(!"unhandled pointer type");
				}               // switch
			}
			else
			{
				/* convert other values into pointers to __data space */
				// special case.. function
				if (IS_FUNC(rtype))
					tag = GPTRTAG_CODE;
				else
					tag = GPTRTAG_DATA;
			}

			//assert (AOP_SIZE (result) == 3);
			if (AOP_SIZE(result) == 3) // for bug3565757.c
			{
				if (tag == 0)
				{
					emitpcode(POC_CLRF, popGet(AOP(result), 2));
				}
				else
				{
					emitpcode(POC_MVL, popGetLit(tag));
					movwf(AOP(result), 2);
				}
			}
        }
        else
        {
            addSign (result, max, 0);
        }                       // if
        goto release;
    }

    /* if they are the same size : or less */

	// 2017, sdcc-3.6.9 cast the result to XDATA!!

    if (AOP_SIZE (result) <= AOP_SIZE (right))
    {

        /* if they are in the same place */
        if (HY08A_sameRegs (AOP (right), AOP (result)))
            goto release;

        DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);
        if (IS_PTR_CONST (rtype))
            DEBUGHY08A_emitcode ("; ***", "%d - right is const pointer", __LINE__);
        if (IS_PTR_CONST (operandType (IC_RESULT (ic))))
            DEBUGHY08A_emitcode ("; ***", "%d - result is const pointer", __LINE__);


		if ((AOP_TYPE(result) == AOP_XDATA))
		{

			if ((AOP_TYPE(right) == AOP_PCODE) && AOP(right)->aopu.pcop->type == PO_IMMEDIATE)
			{
				emitpcode(POC_MVL, popGetAddr(AOP(right), 0, 0));
				emitpcode2(POC_MVFF, PCOP(&pc_wreg),  popGet(AOP(result), 0));
				if (AOP_SIZE(result) < 2)
				{
					fprintf(stderr, "Warning, %s:%d  -- result is not big enough to hold a ptr\n", ic->filename, ic->lineno);
				}
				else
				{
					emitpcode(POC_MVL, popGetAddr(AOP(right), 1, 0));
					emitpcode2(POC_MVWF, PCOP(&pc_wreg),popGet(AOP(result), 1));

				}
			}
			else
			{

				/* if they in different places then copy */
				size = AOP_SIZE(result);
				offset = 0;
				while (size--)
				{
					emitpcode2(POC_MVFF, popGet(AOP(right), offset)
					, popGet(AOP(result), offset));

					//aopPut(AOP(result),
					// aopGet(AOP(right),offset,FALSE,FALSE),
					// offset);

					offset++;
				}
			}
			goto release;
		}
        else if ((AOP_TYPE (right) == AOP_PCODE) && AOP (right)->aopu.pcop->type == PO_IMMEDIATE && !AOP(right)->imm2dir)
        {
            emitpcode (POC_MVL, popGetAddr (AOP (right), 0, 0));
            emitpcode (POC_MVWF, popGet (AOP (result), 0));
			if (AOP_SIZE(result) < 2)
			{
				fprintf(stderr, "Warning, %s:%d  -- result is not big enough to hold a ptr\n", ic->filename,ic->lineno);
			}
			else
			{
				emitpcode(POC_MVL, popGetAddr(AOP(right), 1, 0));
				emitpcode(POC_MVWF, popGet(AOP(result), 1));

			}
        }
        else
        {

            /* if they in different places then copy */
            size = AOP_SIZE (result);
            offset = 0;
            while (size--)
            {
                emitpcode (POC_MVFW, popGet (AOP (right), offset));
                emitpcode (POC_MVWF, popGet (AOP (result), offset));

                //aopPut(AOP(result),
                // aopGet(AOP(right),offset,FALSE,FALSE),
                // offset);

                offset++;
            }
        }
        goto release;
    }

    /* so we now know that the size of destination is greater
       than the size of the source. */

    /* we move to result for the size of source */
    size = AOP_SIZE (right);
    offset = 0;
    while (size--)
    {

		mov2w_op(right, offset);

		if (AOP_TYPE(result) == AOP_XDATA)
			emitpcode2(POC_MVFF, PCOP(&pc_wreg), popGet(AOP(result), offset));
		else
			emitpcode(POC_MVWF, popGet(AOP(result), offset));
        offset++;
    }

	if(!IS_BOOL(operandType(right)) && !IS_BIT(operandType(right)) && 
        !IS_PTR(operandType(right)) && !IS_FUNC(operandType(right))) // add sign only not bool/bit
		addSign (result, AOP_SIZE (right), !SPEC_USIGN (rtype));
	else
	{
		size = AOP_SIZE(right);
		emitpcode(POC_MVL, popGetLit(0));
		while (size < AOP_SIZE(result))
		{
			if (AOP_TYPE(result) == AOP_XDATA)
				emitpcode2(POC_MVFF, PCOP(&pc_wreg), popGet(AOP(result), size++));
			else 
				emitpcode(POC_MVWF, popGet(AOP(result), size++));
		}

	}

release:
    freeAsmop (right, NULL, ic, TRUE);
    freeAsmop (result, NULL, ic, TRUE);

}

/*-----------------------------------------------------------------*/
/* genDjnz - generate decrement & jump if not zero instrucion      */
/*-----------------------------------------------------------------*/
static int
genDjnz (iCode * ic, iCode * ifx)
{
    FENTRY;
    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);

    if (!ifx)
        return 0;

    /* if the if condition has a false label
       then we cannot save */
    if (IC_FALSE (ifx))
        return 0;

    /* if the minus is not of the form
       a = a - 1 */
    if (!isOperandEqual (IC_RESULT (ic), IC_LEFT (ic)) || !IS_OP_LITERAL (IC_RIGHT (ic)))
        return 0;

    if (operandLitValue (IC_RIGHT (ic)) != 1)
        return 0;

    /* if the size of this greater than one then no
       saving */
    if (getSize (operandType (IC_RESULT (ic))) > 1)
        return 0;

    /* otherwise we can save BIG */
    aopOp (IC_RESULT (ic), ic, FALSE);

    emitpcode (POC_DCSZ, popGet (AOP (IC_RESULT (ic)), 0));
    emitpcode (POC_JMP, popGetLabel (IC_TRUE (ifx)->key));

    freeAsmop (IC_RESULT (ic), NULL, ic, TRUE);
    ifx->generated = 1;
    return 1;
}

/*-----------------------------------------------------------------*/
/* genReceive - generate code for a receive iCode                  */
/*-----------------------------------------------------------------*/
static void
genReceive (iCode * ic, operand *thecall)
{
    FENTRY;
    DEBUGHY08A_emitcode ("; ***", "%s  %d", __FUNCTION__, __LINE__);

    if (isOperandInFarSpace (IC_RESULT (ic)) && (OP_SYMBOL (IC_RESULT (ic))->isspilt || IS_TRUE_SYMOP (IC_RESULT (ic))))
    {

        int size = getSize (operandType (IC_RESULT (ic)));
        int offset = fReturnSizeHYA - size;
        while (size--)
        {
            HY08A_emitcode ("push", "%s", (strcmp (fReturn[fReturnSizeHYA - offset - 1], "a") ?
                                           fReturn[fReturnSizeHYA - offset - 1] : "acc"));
            offset++;
        }
        aopOp (IC_RESULT (ic), ic, FALSE);
        size = AOP_SIZE (IC_RESULT (ic));
        offset = 0;
        while (size--)
        {
            HY08A_emitcode ("pop", "acc");
            aopPut (AOP (IC_RESULT (ic)), "a", offset++);
        }

    }
    else
    {
        _G.accInUse++;
        aopOp (IC_RESULT (ic), ic, FALSE);
        _G.accInUse--;
        GpsuedoStkPtr = ic->parmBytes;    // address used arg on stack
        assignResultValue (IC_RESULT (ic),thecall);
    }

    freeAsmop (IC_RESULT (ic), NULL, ic, TRUE);
}

/*-----------------------------------------------------------------*/
/* genDummyRead - generate code for dummy read of volatiles        */
/*-----------------------------------------------------------------*/
static void
genDummyRead (iCode * ic)
{
//  FENTRY;
    //HY08A_emitcode ("; genDummyRead", "");
    //HY08A_emitcode ("; not implemented", "");
    // read to ACC
    operand  *right;
    int size, offset; //know_W;
    //unsigned long lit = 0L;
    right = IC_RIGHT(ic);
	if (right == NULL)
		right = IC_LEFT(ic); // special case

    FENTRY;

    DEBUGHY08A_emitcode("; ***", "%s  %d", __FUNCTION__, __LINE__);

    /* if they are the same */
    if (operandsEqu(IC_RESULT(ic), IC_RIGHT(ic)))
        return;

    aopOp(right, ic, FALSE);
    //aopOp(result, ic, TRUE);

    //DEBUGHY08A_AopType(__LINE__, NULL, right, result);

    /* if they are the same registers */
    //if (HY08A_sameRegs(AOP(right), AOP(result)) && (AOP(right)->type != AOP_DIR || AOP(right)->aopu.aop_dir == NULL ||
    //	!canAssignSelf(AOP(right)->aopu.aop_dir)))
    //	goto release;

    /* special case: assign from __code ==> it is no sense to read ROM as dummy??*/
    if (!IS_ITEMP(right)         /* --> iTemps never reside in __code */
            && IS_SYMOP(right)       /* --> must be an immediate (otherwise we would be in genConstPointerGet) */
            && !IS_FUNC(OP_SYM_TYPE(right)) /* --> we would want its address instead of the first instruction */
            && !IS_CODEPTR(OP_SYM_TYPE(right))      /* --> get symbols address instread */
            && IN_CODESPACE(SPEC_OCLS(getSpec(OP_SYM_TYPE(right)))))
    {
        //emitpComment("genAssign from CODESPACE");
        //genConstPointerGet(right, result, ic);
        goto release;
    }

    /* just for symmetry reasons... */
    //if (!IS_ITEMP(result) && IS_SYMOP(result) && IN_CODESPACE(SPEC_OCLS(getSpec(OP_SYM_TYPE(result)))))
    //{
    //	assert(!"cannot write to CODESPACE");
    //}

    /* if the result is a bit */
    //if (AOP_TYPE(result) == AOP_CRY)
    //{

    //	/* if the right size is a literal then
    //	we know what the value is */
    //	if (AOP_TYPE(right) == AOP_LIT)
    //	{

    //		emitpcode((((int)operandLitValue(right)) ? POC_BSF : POC_BCF), popGet(AOP(result), 0));

    //		if (((int)operandLitValue(right)))
    //			HY08A_emitcode("bsf", "(%s >> 3),(%s & 7)", AOP(result)->aopu.aop_dir, AOP(result)->aopu.aop_dir);
    //		else
    //			HY08A_emitcode("bcf", "(%s >> 3),(%s & 7)", AOP(result)->aopu.aop_dir, AOP(result)->aopu.aop_dir);
    //		goto release;
    //	}

    //	/* the right is also a bit variable */
    //	if (AOP_TYPE(right) == AOP_CRY)
    //	{
    //		emitpcode(POC_BCF, popGet(AOP(result), 0));
    //		emitpcode(POC_BTSZ, popGet(AOP(right), 0));
    //		emitpcode(POC_BSF, popGet(AOP(result), 0));

    //		HY08A_emitcode("bcf", "(%s >> 3),(%s & 7)", AOP(result)->aopu.aop_dir, AOP(result)->aopu.aop_dir);
    //		HY08A_emitcode("btfsc", "(%s >> 3),(%s & 7)", AOP(right)->aopu.aop_dir, AOP(right)->aopu.aop_dir);
    //		HY08A_emitcode("bsf", "(%s >> 3),(%s & 7)", AOP(result)->aopu.aop_dir, AOP(result)->aopu.aop_dir);
    //		goto release;
    //	}

    //	/* we need to or */
    //	emitpcode(POC_BCF, popGet(AOP(result), 0));
    //	HY08A_toBoolean(right);
    //	emitSKPZ;
    //	emitpcode(POC_BSF, popGet(AOP(result), 0));
    //	//aopPut(AOP(result),"a",0);
    //	goto release;
    //}

    /* bit variables done */
    /* general case */
	size = 0;
	if(right)
		size = AOP_SIZE(right);
    offset = 0;
    // xdata special case
    //if (AOP_TYPE(result) == AOP_XDATA)
    //{
    //	// XDATA copy uses MVFF
    //	if (!aop_isLitLike(AOP(right)))
    //	{
    //		int i;
    //		for (i = 0; i < size; i++)
    //		{
    //			emitpcode2(POC_MVFF, popGet(AOP(right), i), popGet(AOP(result), i));
    //		}
    //	}
    //	else
    //	{
    //		XDATAA2PTR(result);//LDPR only 0

    //		for (offset = 0; offset < size; offset++)
    //		{

    //			if (AOP_TYPE(right) == AOP_LIT)
    //			{
    //				lit = (unsigned long)HY08AaopLiteral(AOP(right)->aopu.aop_lit, offset) & 0x0ff;
    //				if (lit != 0)
    //				{
    //					emitpcode(POC_MVL, popGetLit(lit & 0xff));
    //					emitpcode(POC_MVWF, PCOP_POINC0);
    //				}
    //				else
    //				{
    //					emitpcode(POC_CLRF, PCOP_POINC0);
    //				}
    //			}
    //			else if (AOP_TYPE(right) == AOP_CRY)
    //			{

    //				if (offset == 0)
    //				{
    //					emitpcode(POC_CLRF, PCOP_INDF0);
    //					emitpcode(POC_BTSS, popGet(AOP(right), 0));
    //					emitpcode(POC_INF, PCOP_INDF0);

    //				}
    //				else
    //				{
    //					emitpcode(POC_CLRF, popGetWithString("_PRINC0", 1));
    //				}
    //			}
    //			else
    //			{
    //				mov2w_op(right, offset);
    //				emitpcode(POC_MVWF, PCOP_POINC0);
    //			}

    //		}

    //	}
    //	goto release;

    //}
    //if (AOP_TYPE(right) == AOP_DIR && (AOP_TYPE(result) == AOP_REG) && size == 1)
    //{
    //	DEBUGHY08A_emitcode("; ***", "%s  %d", __FUNCTION__, __LINE__);
    //	if (aopIdx(AOP(result), 0) == 4)
    //	{
    //		DEBUGHY08A_emitcode("; ***", "%s  %d", __FUNCTION__, __LINE__);
    //		emitpcode(POC_MVFW, popGet(AOP(right), offset));
    //		emitpcode(POC_MVWF, popGet(AOP(result), offset));
    //		goto release;
    //	}
    //	else
    //		DEBUGHY08A_emitcode("; WARNING", "%s  %d ignoring register storage", __FUNCTION__, __LINE__);
    //}

    //know_W = -1;
    while (size--)
    {

        DEBUGHY08A_emitcode("; ***", "%s  %d", __FUNCTION__, __LINE__);
        if (AOP_TYPE(right) == AOP_LIT)
        {
            /*lit = (unsigned long)HY08AaopLiteral(AOP(right)->aopu.aop_lit, offset) & 0x0ff;
            if (lit & 0xff)
            {

            	if (lit == 0xff)
            	{
            		emitpcode(POC_SETF, popGet(AOP(result), offset));
            	}
            	else
            	{
            		if (know_W != (int)(lit & 0xff))
            			emitpcode(POC_MVL, popGetLit(lit & 0xff));
            		know_W = lit & 0xff;
            		emitpcode(POC_MVWF, popGet(AOP(result), offset));
            	}
            }
            else
            	emitpcode(POC_CLRF, popGet(AOP(result), offset));*/
            goto release;

        }
        else if (AOP_TYPE(right) == AOP_CRY) // no support bit read
        {
            //emitpcode(POC_CLRF, popGet(AOP(result), offset));
            //if (offset == 0)
            //{
            //	emitpcode(POC_BTSS, popGet(AOP(right), 0));
            //	emitpcode(POC_INF, popGet(AOP(result), 0));
            //}
            goto release;
        }
        else
        {
            //pCodeOp *pcop = popGet(AOP(result), offset);
            mov2w_op(right, offset);

            /*if (!pcop->name || (pcop->name != NULL && strcmp(pcop->name, "_WREG")))
            	emitpcode(POC_MVWF, pcop);*/
        }

        offset++;
    }


release:
    freeAsmop(right, NULL, ic, FALSE);
    //freeAsmop(result, NULL, ic, TRUE);

    //ic = ic;
}

/*-----------------------------------------------------------------*/
/* genHY08ACode - generate code for HY08A based controllers        */
/*-----------------------------------------------------------------*/
/*
* At this point, ralloc.c has gone through the iCode and attempted
* to optimize in a way suitable for this chip. Now we've got to generate
* instructions that correspond to the iCode.
*
* Once the instructions are generated, we'll pass through both the
* peep hole optimizer and the pCode optimizer.
*-----------------------------------------------------------------*/


static int canMult2Shift(int mulint)
{
    switch (mulint)
    {
    case 2:
        return 1;
    case 4:
        return 2;
    case 8:
        return 3;
    }
    return 0;
}

static iCode *findPrevFuncICODE(iCode *ic)
{
    do {


        if (ic->op == FUNCTION)
            return ic;
        ic = ic->prev;
    } while (ic && (ic->op == RECEIVE || ic->op==FUNCTION) );
    return NULL;

}

//static int checkSpecialSymbol(char *name)
//{
//	if (!strcmp(name, "FSR0"))
//		return 1;
//	if (!strcmp(name, "FSR1"))
//		return 2;
//	if (!strcmp(name, "FSR2"))
//		return 3;
//	if (!strcmp(name, "TBLPTR"))
//		return 4;
//	return 0;
//}

// check operand use again,
// 2021 oct change to check the live range


static iCode * operand_used_again(operand *op, iCode *ic)
{
    
	if (!IS_SYMOP(op))
		return NULL;
	while (ic)
	{
		if (
			(IS_SYMOP(IC_LEFT(ic)) && OP_SYMBOL(IC_LEFT(ic)) == OP_SYMBOL(op) )

			|| ( 				IS_SYMOP(IC_RIGHT(ic)) &&
				OP_SYMBOL(IC_RIGHT(ic)) == OP_SYMBOL(op)) || 
				(
					IS_SYMOP(IC_COND(ic)) &&
					
			OP_SYMBOL(IC_COND(ic))==OP_SYMBOL(op))
				 || 
			
			(IS_SYMOP(IC_RESULT(ic)) && OP_SYMBOL(IC_RESULT(ic)) == OP_SYMBOL(op)) // it is possible result is used!!
			)
			return ic;
		ic = ic->next;
	}
	return NULL;
}

static int isPointer2Stack(operand *operp)// when pointer to stack LDPR is not allowed
{
	if (HY08A_options.dis_fsr_opt)
		return 1;

	if (HY08A_getPART()->isEnhancedCore < 3 || HY08A_getPART()->isEnhancedCore == 5)
		return 0;

	//2020 remat icode may be many levels

	if (operp->type == SYMBOL && (OP_SYMBOL(operp)->remat || OP_SYMBOL(operp)->rematX) &&
		OP_SYMBOL(operp)->rematiCode)
	{
		//2020 check left only, no check right
		iCode *ic = OP_SYMBOL(operp)->rematiCode;

		if (IS_TRUE_SYMOP(IC_LEFT(ic))) // depends on the true symbol
		{
			if ((OP_SYMBOL(IC_LEFT(ic))->islocal ||
				OP_SYMBOL(IC_LEFT(ic))->onStack))
				return 1;
			return 0;
		}
		// true symbol may at next level
next_lev:
		ic = OP_SYMBOL(IC_LEFT(ic))->rematiCode;
		if (!ic)
			return 1; // possible on stack
		if (IS_TRUE_SYMOP(IC_LEFT(ic))) // depends on the true symbol
		{
			if ((OP_SYMBOL(IC_LEFT(ic))->islocal ||
				OP_SYMBOL(IC_LEFT(ic))->onStack))
				return 1;
			return 0;
		}
		goto next_lev;

	}
	return 1; // safer to use 1?
}

void genHY08ACode(iCode *lic)
{
    iCode *ic;
    iCode *ic2;
    int cln = 0;
    iCode *currFuncICode = NULL;
    const char *cline;

    int noStack = (HY08A_getPART()->isEnhancedCore < 3 || HY08A_getPART()->isEnhancedCore == 5);
    //int noStack = 0;
    //FILE *debugFsave;

    FENTRY;

    pb = newpCodeChain(GcurMemmap, 0, newpCodeCharP("--------------------------------------------------------"));
    addpBlock(pb);

    /* if debug information required */
    if (options.debug && debugFile && currFunc)
    {
        debugFile->writeFunction(currFunc, lic);
    }

    for (ic = lic; ic; ic = ic->next)
    {
        int mul2shift = 0;
        initGenLineElement();

        //DEBUGHY08A_emitcode(";ic","");
        //fprintf (stderr, "in ic loop\n");
        //HY08A_emitcode ("", ";\t%s:%d: %s", ic->filename,
        //ic->lineno, printCLine(ic->filename, ic->lineno));

        // 2023 I don't know why the line-no shifted. fxck!!
        if((ic->op==CALL || ic->op==PCALL || ic->op==SEND) && 
            ic->prev->op == SEND && ic->lineno != ic->prev->lineno)
        {
            ic->lineno = ic->prev->lineno;
        }

        if (!options.noCcodeInAsm && (cln != ic->lineno))
        {
            cln = ic->lineno;
            //fprintf (stderr, "%s\n", printCLine (ic->filename, ic->lineno));
            cline = printCLine(ic->filename, ic->lineno);
            if (!cline || strlen(cline) == 0)
                cline = printCLine(ic->filename, ic->lineno);
            addpCode2pBlock(pb, newpCodeCSource(ic->lineno, ic->filename, cline, ic->seq));
            //emitpComment ("[C-SRC] %s:%d: %s", ic->filename, cln, cline);
        }

        if (options.iCodeInAsm)
        {
            const char *iLine = printILine(ic);
            emitpComment("[ICODE] %s:%d: %s", ic->filename, ic->lineno, printILine(ic));
            dbuf_free(iLine);
        }
        /* if the result is marked as
           spilt and rematerializable or code for
           this has already been generated then
           do nothing */
#ifdef DEBUGLINENO
        if (ic->lineno == DEBUGLINENO)
            fprintf(stderr, "line %d\n", DEBUGLINENO);

#endif           
        if (resultRemat(ic) 
            || ic->generated)
            continue;

            /* depending on the operation */

            // 2021 oct, special case pointer_set to a near data pointer
#if NEARPOINTER_DIR
            // 2024 oct cast ptrget/set changed
        // if (!options.nopeep && ic->op==CAST && ic->next && 
        //     POINTER_SET(ic->next) &&
        //     isOperandEqual(IC_RESULT(ic), IC_RESULT(ic->next)) 

        // )
        // {
        //     //sym_link *ptype = operandType(IC_RESULT(ic->next));
        //     //sym_link *letype = getSpec(ptype);
        //     //if(IS_BITFIELD(letype))
        //     {
        //         IC_RESULT(ic->next) = IC_RIGHT(ic); // modify it?
        //     }
        //     // try to pointer set 
            
        // }
        // tricky?
        if (!options.nopeep && ic->next && //!(ic->prev &&ic->prev->lineno == ic->lineno && resultRemat(ic->prev)) &&
            POINTER_SET(ic->next)  &&
            IS_SYMOP(IC_RESULT(ic)) && IS_SYMOP(IC_RIGHT(ic->next)) &&
            IS_SYMOP(IC_RESULT(ic->next)) &&        
            isOperandEqual(IC_RESULT(ic), IC_RIGHT(ic->next)) &&
            !IS_TRUE_SYMOP(IC_RESULT(ic)) &&
            OPrangeLimited(IC_RESULT(ic), ic->seq, ic->next->seq) &&
            // make sure near data pointer
            IS_DATA_PTR(OP_SYMBOL(IC_RESULT(ic->next))->type))
        {
            sym_link *ptype = operandType(IC_RESULT(ic->next));
            sym_link *letype = getSpec(ptype);
            if (!IS_BITFIELD(letype))
            {
                aopOp(IC_RESULT(ic), ic, TRUE);
                aopOp(IC_RIGHT(ic->next), ic->next, false);
                aopOp(IC_RESULT(ic->next), ic->next, true);
                if (aop_isLitLike(AOP(IC_RESULT(ic->next))))
                {
                    operand *copiedOperand = malloc(sizeof(operand));

                    symbol *copiedSym = malloc(sizeof(symbol));
                    asmop *copiedAOP = malloc(sizeof(asmop));
                    // copied is a pointer
                    memcpy(copiedOperand, IC_RESULT(ic->next), sizeof(operand));
                    memcpy(copiedSym, OP_SYMBOL(copiedOperand), sizeof(symbol));
                    memcpy(copiedAOP, AOP(copiedOperand), sizeof(asmop));
                    copiedSym->key += 0x1758600; // it will not duplicate
                    OP_SYMBOL(copiedOperand) = copiedSym;
                    AOP(copiedOperand) = copiedAOP;
                    AOP(copiedOperand)->imm2dir = 1;

                    // change the size

                    // formerly pointer, now I am not pointer
                    OP_SYMBOL(copiedOperand)->type = OP_SYMBOL(IC_RESULT(ic))->type;
                    AOP(copiedOperand)->size = AOP(IC_RESULT(ic))->size;
                    IC_RESULT(ic) = copiedOperand;
                    ic->next->generated = true;
                }
            }
        }
        

#endif
		// temp-back optimization, our operand support direct back!!
        // first equal is to prevent pointer set
		if (!options.nopeep) // op[timization only when peep enabled
		{
			if (ic->op != '=' && ic->next && ic->next->op == '=' && !POINTER_SET(ic->next) &&
				(((IC_RIGHT(ic) == NULL && isOperandEqual(IC_RESULT(ic->next), IC_LEFT(ic))) ||
				(IC_LEFT(ic) == NULL && isOperandEqual(IC_RESULT(ic->next), IC_RIGHT(ic)))) && IS_ITEMP(IC_RESULT(ic))) &&
				IS_SYMOP(IC_RESULT(ic)) && IS_SYMOP(IC_RESULT(ic->next)) &&
				compareType(OP_SYMBOL(IC_RESULT(ic))->type, OP_SYMBOL(IC_RESULT(ic->next))->type,false) &&

				!operand_used_again(IC_RESULT(ic), ic->next->next))
			{
				IC_RESULT(ic) = IC_RESULT(ic->next);
				ic->next->generated = 1;
			}

			if (ic->op != '=' && ic->op != IFX && ic->next && ic->next->op == '=' && !POINTER_SET(ic->next) &&
				(((IS_OP_LITERAL(IC_RIGHT(ic)) && isOperandEqual(IC_RESULT(ic->next), IC_LEFT(ic)))) ||
				(IS_OP_LITERAL(IC_LEFT(ic)) && isOperandEqual(IC_RESULT(ic->next), IC_RIGHT(ic))) && IS_ITEMP(IC_RESULT(ic))) &&
				compareType(OP_SYMBOL(IC_RESULT(ic))->type, OP_SYMBOL(IC_RESULT(ic->next))->type,false) && // same 
				!operand_used_again(IC_RESULT(ic), ic->next->next))
			{
				IC_RESULT(ic) = IC_RESULT(ic->next);
				ic->next->generated = 1;
			}
		}

        switch (ic->op)
        {
        case '!':
			// 2021
            genNot(ic);
            break;

        case '~':
            genCpl(ic);
            break;

        case UNARYMINUS:
            genUminus(ic);
            break;

        case IPUSH:
            genIpush(ic);
            break;

        case IPOP:
            /* IPOP happens only when trying to restore a
               spilt live range, if there is an ifx statement
               following this pop then the if statement might
               be using some of the registers being popped which
               would destory the contents of the register so
               we need to check for this condition and handle it */
            if (ic->next && ic->next->op == IFX && regsInCommon(IC_LEFT(ic), IC_COND(ic->next)))
                genIfx(ic->next, ic);
            else
                genIpop(ic);
            break;

        case CALL: // 2021 oct, temp optimizations skip at register-packing, use direct assign
            if (ic->next && ic->next->op == '=' && !POINTER_SET(ic->next) && isOperandEqual(IC_RESULT(ic), IC_RIGHT(ic->next)) &&
                !IS_TRUE_SYMOP(IC_RESULT(ic)) &&
                !operand_used_again(IC_RESULT(ic), ic->next->next))
            {
                IC_RESULT(ic) = IC_RESULT(ic->next);
                ic->next->generated = 1;
            }
            genCall(ic);
            break;

        case PCALL:
            genPcall(ic);
            break;

        case FUNCTION:
            genFunction(ic);
            currFuncICode = ic;
            break;

        case ENDFUNCTION:
            genEndFunction(ic);

            break;

        case RETURN:
            genRet(ic, currFuncICode);
            break;

        case LABEL:
            genLabel(ic);
            break;

        case GOTO:
            genGoto(ic);
            break;

        case '+':
            // try .. again the special case
            // for global lcdnum[], it will be optimized directly
            // we need to check lcdnum[k]
            // first is pointer set, second is pointer get
            // consider function pointer set
            //
            // ok, this is a little far away .. we try to re-arrange
            // then the code can be saved
            //isPointer2Stack(IC_LEFT(ic))
            if (!options.nopeep && !isPointer2Stack(IC_LEFT(ic)) && POINTER_SET(ic->next) && //!operand_used_again(IC_RESULT(ic), ic->next->next)
                OPrangeLimited(IC_RESULT(ic), ic->seq, ic->next->seq))
            {
                operand *left0, *right0, *result0;
                operand *right1, *result1; // left1 is null
                left0 = IC_LEFT(ic);
                right0 = IC_RIGHT(ic);
                result0 = IC_RESULT(ic);
                //left1 = IC_LEFT(ic->next);
                right1 = IC_RIGHT(ic->next);
                result1 = IC_RESULT(ic->next);
                aopOp(left0, ic, true);
                aopOp(right0, ic, false);
                aopOp(result0, ic, true);

                //aopOp(left1, ic->next, false);
                aopOp(right1, ic->next, false);
                aopOp(result1, ic->next, true);
                // for a[b]=c, we can use plusw only if a is array, size < 128
                // b .. is OK, because if a size < 128, we can always use plusw0
                // 2 cases, one is literal known
                // another is target size <128, 128 is not ok, because PLUSW cannot + 128
                if (IS_SYMOP(result1) && isOperandEqual(result1, result0) &&
                    //OP_SYMBOL(result1)->regs[0] == OP_SYMBOL(result0)->regs[0] &&
                    //OP_SYMBOL(result1)->regs[1] == OP_SYMBOL(result0)->regs[1] &&

                    (
                        AOP(left0)->type == AOP_PCODE && AOP(left0)->aopu.pcop->type == PO_IMMEDIATE &&
                        //AOP_SIZE(OP_SYMBOL(left0)) < 128 &&
                        PCOI(AOP(left0)->aopu.pcop)->r->size < 128 && (PCOI(AOP(left0)->aopu.pcop)->r->size != 0 || AOP_SIZE(right0) == 1)))
                {
                    // try to write the code directly
                    // W is occupied, we use MVFF
                    int i;
                    int size = AOP_SIZE(right1); // this is the size!!
                    emitpcode(POC_LDPR, AOP(left0)->aopu.pcop);

                    if (op_isLitLike(right1)) // annoying
                    {
                        for (i = 0; i < size; i++)
                        {

                            // use STK00

                            if (IS_OP_LITERAL(right1))
                            {
                                int lit = 0xff & (ullFromVal(AOP(right1)->aopu.aop_lit) >> (8 * i));
                                // if (lit == 0)
                                // 	emitpcode(POC_CLRF, get_argument_pcop(1+i));
                                // else if (lit == 255)
                                // 	emitpcode(POC_SETF, get_argument_pcop(1+i));
                                // else
                                if (lit != 0 && lit != 0xff)
                                {
                                    emitpcode(POC_MVL, popGetLit(lit));
                                    emitpcode(POC_MVWF, get_argument_pcop(1 + i));
                                }
                            }
                            else
                            {
                                mov2w(AOP(right1), i);
                                emitpcode(POC_MVWF, get_argument_pcop(1 + i));
                            }
                        }
                        mov2w(AOP(right0), 0);
                        for (i = 0; i < size; i++)
                        {
                            if (IS_OP_LITERAL(right1))
                            {
                                int lit = 0xff & (ullFromVal(AOP(right1)->aopu.aop_lit) >> (8 * i));
                                if (lit == 0)
                                    emitpcode(POC_CLRF, PCOP(&pc_plusw0));
                                else if (lit == 0xff)
                                    emitpcode(POC_SETF, PCOP(&pc_plusw0));
                                else
                                {
                                    emitpcode2(POC_MVFF, get_argument_pcop(1 + i), PCOP(&pc_plusw0));
                                }
                            }
                            else
                                emitpcode2(POC_MVFF, get_argument_pcop(1 + i), PCOP(&pc_plusw0));
                            if (i < size - 1)
                                emitpcode(POC_INFW, PCOP(&pc_wreg));
                        }
                    }
                    else
                    {
                        if (noStack)
                        {
                            mov2w(AOP(right0), 0);
                            for (i = 0; i < size; i++)
                            {
                                emitpcode2(POC_MVFF, popGet(AOP(right1), i), PCOP(&pc_plusw0));
                                if (i < size - 1)
                                    emitpcode(POC_INFW, PCOP(&pc_wreg));
                            }
                        }
                        else
                        {

                            for (i = 0; i < size; i++)
                            {
                                emitpcode(POC_MVFW, popGet(AOP(right1), i));
                                emitpcode(POC_MVWF, get_argument_pcop(i + 1));
                            }
                            mov2w(AOP(right0), 0);
                            for (i = 0; i < size; i++)
                            {
                                emitpcode2(POC_MVFF, get_argument_pcop(i + 1), PCOP(&pc_plusw0));
                                if (i < size - 1)
                                    emitpcode(POC_INFW, PCOP(&pc_wreg));
                            }
                        }
                    }

                    ic->generated = 1;
                    ic->next->generated = 1;
                    break;
                }
                else if (
                    IS_SYMOP(result1) && isOperandEqual(result1, result0) &&
                    //OP_SYMBOL(result1)->regs[0] == OP_SYMBOL(result0)->regs[0] &&
                    //OP_SYMBOL(result1)->regs[1] == OP_SYMBOL(result0)->regs[1] &&
                    IS_PTR(OP_SYMBOL(left0)->type) && IS_OP_LITERAL(right0) &&
                    ullFromVal(AOP(right0)->aopu.aop_lit) < 128 // only RAM can write!!
                )
                {
                    int i;
                    int size = AOP_SIZE(right1); // this is the size!!
                    //emitpcode(POC_LDPR, AOP(left0)->aopu.pcop);
                    mov2w(AOP(left0), 0);
                    emitpcode(POC_MVWF, PCOP(&pc_fsr0l));
                    mov2w(AOP(left0), 1);
                    emitpcode(POC_MVWF, PCOP(&pc_fsr0h));

                    if (op_isLitLike(right1)) // annoying
                    {
                        for (i = 0; i < size; i++)
                        {

                            // use STK00
                            if (IS_OP_LITERAL(right1))
                            {
                                int lit = 0xff & (ullFromVal(AOP(right1)->aopu.aop_lit) >> (8 * i));
                                if (lit == 0)
                                    emitpcode(POC_CLRF, get_argument_pcop(1 + i));
                                else if (lit == 255)
                                    emitpcode(POC_SETF, get_argument_pcop(1 + i));
                                else
                                {
                                    emitpcode(POC_MVL, popGetLit(lit));
                                    emitpcode(POC_MVWF, get_argument_pcop(1 + i));
                                }
                            }
                            else
                            {
                                mov2w(AOP(right1), i);
                                emitpcode(POC_MVWF, get_argument_pcop(1 + i));
                            }
                        }
                        mov2w(AOP(right0), 0);
                        for (i = 0; i < size; i++)
                        {
                            emitpcode2(POC_MVFF, get_argument_pcop(1 + i), PCOP(&pc_plusw0));
                            if (i < size - 1)
                                emitpcode(POC_INFW, PCOP(&pc_wreg));
                        }
                    }
                    else
                    {
                        if (noStack)
                        {
                            mov2w(AOP(right0), 0);
                            for (i = 0; i < size; i++)
                            {
                                emitpcode2(POC_MVFF, popGet(AOP(right1), i), PCOP(&pc_plusw0));
                                if (i < size - 1)
                                    emitpcode(POC_INFW, PCOP(&pc_wreg));
                            }
                        }
                        else
                        {
                            for (i = 0; i < size; i++)
                            {
                                emitpcode(POC_MVFW, popGet(AOP(right1), i));
                                emitpcode(POC_MVWF, get_argument_pcop(i + 1));
                            }
                            mov2w(AOP(right0), 0);
                            for (i = 0; i < size; i++)
                            {
                                emitpcode2(POC_MVFF, get_argument_pcop(i + 1), PCOP(&pc_plusw0));
                                if (i < size - 1)
                                    emitpcode(POC_INFW, PCOP(&pc_wreg));
                            }
                        }
                    }

                    ic->generated = 1;
                    ic->next->generated = 1;
                    break;
                }
                // else
                // {
                // 	freeAsmop(left0, NULL, ic, false);
                // 	freeAsmop(right0, NULL, ic, false);
                // 	freeAsmop(result0, NULL, ic, true);

                // 	freeAsmop(right1, NULL, ic->next, false);
                // 	freeAsmop(result1, NULL, ic->next, true);
                // }
            }
            else if (!options.nopeep && !isPointer2Stack(IC_LEFT(ic)) && POINTER_GET(ic->next))
            {
                // it is possible read and write
                iCode *againIC = operand_used_again(IC_RESULT(ic), ic->next->next);
                if (againIC)
                {
                    if ( //!operand_used_again(IC_RESULT(ic), againIC->next)
                        OPrangeLimited(IC_RESULT(ic), ic->seq, againIC->seq) && POINTER_SET(againIC) && ic->next->next && ic->next->next->next &&
                        againIC == ic->next->next->next &&
                        (ic->next->next->op == '+' || ic->next->next->op == '-' || ic->next->next->op == '|' || ic->next->next->op == BITWISEAND) && IS_OP_LITERAL(IC_RIGHT(ic->next->next))) // 2020, try optimize k[i]++, k[i]|=2, it cannot apply +=2
                    {

                        // it should be used only twice!!

                        operand *left0, *right0, *result0; // first is add
                        operand *left1, *result1;          //pointer get, right1 is null
                        operand *left2, *right2, *result2; // nn is +1/-1/xor
                        operand *right3, *result3;         // nnn is result and right for pointer set
                        unsigned long long lit2;
                        int bitidOr;
                        int bitidAnd;
                        left0 = IC_LEFT(ic);
                        right0 = IC_RIGHT(ic);
                        result0 = IC_RESULT(ic);

                        left1 = IC_LEFT(ic->next);
                        result1 = IC_RESULT(ic->next);

                        left2 = IC_LEFT(ic->next->next);
                        right2 = IC_RIGHT(ic->next->next);
                        result2 = IC_RESULT(ic->next->next);

                        right3 = IC_RIGHT(ic->next->next->next);
                        result3 = IC_RESULT(ic->next->next->next);

                        aopOp(left0, ic, true);
                        aopOp(right0, ic, false);
                        aopOp(result0, ic, true);

                        aopOp(left1, ic->next, false);
                        aopOp(result1, ic->next, true);

                        aopOp(left2, ic->next->next, true);
                        aopOp(right2, ic->next->next, false);
                        aopOp(result2, ic->next->next, true);

                        aopOp(right3, ic->next->next->next, false);
                        aopOp(result3, ic->next->next->next, true);

                        // now we have to make sure the PLUSW is applicable
                        lit2 = ullFromVal(AOP(right2)->aopu.aop_lit);
                        bitidOr = my_powof2(lit2 & 0xff);
                        bitidAnd = my_powof2((lit2 ^ 0xff) & 0xff);
                        if (AOP_SIZE(result1) == 1)                                                  // it should be a temp
                            if (isOperandEqual(result1, left2) && isOperandEqual(result2, right3) && //!operand_used_again(result2, againIC->next)
                                OPrangeLimited(result2, ic->seq, againIC->seq))
                                if (((ic->next->next->op == '+') && (lit2 == 1)) ||
                                    ((ic->next->next->op == '-') && (lit2 == 1)) ||
                                    ((ic->next->next->op == '|') && bitidOr >= 0) ||
                                    ((ic->next->next->op == BITWISEAND) && bitidAnd >= 0)

                                )
                                {
                                    if (
                                        AOP(left0)->type == AOP_PCODE && AOP(left0)->aopu.pcop->type == PO_IMMEDIATE &&
                                        //AOP_SIZE(OP_SYMBOL(left0)) < 128 &&
                                        PCOI(AOP(left0)->aopu.pcop)->r->size < 128 &&
                                        (PCOI(AOP(left0)->aopu.pcop)->r->size != 0 || AOP_SIZE(right0) == 1) // [size < 128, size is no matter]
                                    )
                                    {
                                        emitpcode(POC_LDPR, AOP(left0)->aopu.pcop);
                                        mov2w(AOP(right0), 0); // if size = 1, use mvfw+mvwf

                                        switch (ic->next->next->op)
                                        {
                                        case '+':
                                            emitpcode(POC_INF, PCOP(&pc_plusw0));
                                            break;
                                        case '-':
                                            emitpcode(POC_DECF, PCOP(&pc_plusw0));
                                            break;
                                        case '|':
                                            emitpcode(POC_BSF, newpCodeOpBit("_PLUSW0", bitidOr, 0));
                                            break;
                                        case BITWISEAND:
                                            emitpcode(POC_BCF, newpCodeOpBit("_PLUSW0", bitidAnd, 0));
                                            break;
                                        default:
                                            fprintf(stderr, "internal unknown error at %s:%d\n", __FILE__, __LINE__);
                                            exit(-__LINE__);
                                        }
                                        ic->generated = 1;
                                        ic->next->generated = 1;
                                        ic->next->next->generated = 1;
                                        ic->next->next->next->generated = 1;
                                        break;
                                    }
                                }

                        freeAsmop(left0, NULL, ic, false);
                        freeAsmop(right0, NULL, ic, false);
                        freeAsmop(result0, NULL, ic, true);

                        freeAsmop(left1, NULL, ic->next, false);
                        freeAsmop(result1, NULL, ic->next, true);

                        freeAsmop(left2, NULL, ic->next->next, false);
                        freeAsmop(right2, NULL, ic->next->next, false);
                        freeAsmop(result2, NULL, ic->next->next, true);

                        freeAsmop(right3, NULL, ic->next->next->next, false);
                        freeAsmop(result3, NULL, ic->next->next->next, true);
                    }
                }

                else

                {
                    operand *left0, *right0, *result0;
                    operand *left1, *result1; // right1 is null
                    left0 = IC_LEFT(ic);
                    right0 = IC_RIGHT(ic);
                    result0 = IC_RESULT(ic);
                    left1 = IC_LEFT(ic->next);
                    //right1 = IC_RIGHT(ic->next);
                    result1 = IC_RESULT(ic->next);
                    aopOp(left0, ic, true);
                    aopOp(right0, ic, false);
                    aopOp(result0, ic, true);

                    aopOp(left1, ic->next, false);
                    //aopOp(right1, ic->next, false);
                    aopOp(result1, ic->next, true);

                    // we have a=b[c] , one case is b is literal, c is var
                    // another case is b is pointer, c is literal
                    // we are afraid b[c] is optimized to cse middle var
                    // but seems we have not the case yet
                    if (
                        IS_SYMOP(left0) && IS_SYMOP(left1) &&
                        (IS_DATA_PTR(OP_SYMBOL(left0)->type) || IS_FARPTR(OP_SYMBOL(left0)->type)) &&
                        isOperandEqual(left1, result0) &&
                        //OP_SYMBOL(left1)->regs[0] == OP_SYMBOL(result0)->regs[0] &&
                        //OP_SYMBOL(left1)->regs[1] == OP_SYMBOL(result0)->regs[1] &&
                        // 2 cases, one is literal known
                        // another is target size <128, 128 is not ok, because PLUSW cannot + 128
                        (
                            AOP(left0)->type == AOP_PCODE && AOP(left0)->aopu.pcop->type == PO_IMMEDIATE &&
                            //AOP_SIZE(OP_SYMBOL(left0)) < 128 &&
                            PCOI(AOP(left0)->aopu.pcop)->r->size < 128 &&
                            (PCOI(AOP(left0)->aopu.pcop)->r->size != 0 || AOP_SIZE(right0) == 1) // [size < 128, size is no matter]
                            ))
                    {
                        // try to write the code directly
                        // W is occupied, we use MVFF
                        int i;
                        int size = AOP_SIZE(result1); // this is the size!!
                        emitpcode(POC_LDPR, AOP(left0)->aopu.pcop);

                        // result must be a var!!

                        if (!noStack)
                        {
                            mov2w(AOP(right0), 0); // if size = 1, use mvfw+mvwf
                            if (size == 1)
                            {
                                emitpcode(POC_MVFW, PCOP(&pc_plusw0));
                                emitpcode(POC_MVWF, popGet2(AOP(result1), 0, 1));
                            }
                            else
                            {
                                for (i = 0; i < size; i++)
                                {
                                    // keep W!!
                                    // MVFF may be wrong for stack space,

                                    emitpcode22(POC_MVFF, PCOP(&pc_plusw0), get_argument_pcop(i + 1));

                                    if (i < size - 1)
                                        emitpcode(POC_INFW, PCOP(&pc_wreg));
                                }
                                for (i = 0; i < size; i++)
                                {
                                    emitpcode(POC_MVFW, get_argument_pcop(i + 1));
                                    emitpcode(POC_MVWF, popGet2(AOP(result1), i, 1));
                                }
                            }
                        }
                        else
                        {
                            mov2w(AOP(right0), 0);
                            if (size == 1)
                            {
                                emitpcode(POC_MVFW, PCOP(&pc_plusw0));
                                emitpcode(POC_MVWF, popGet2(AOP(result1), 0, 1));
                            }
                            else
                            {
                                for (i = 0; i < size; i++)
                                {
                                    // keep W!!
                                    // MVFF may be wrong for stack space,

                                    emitpcode22(POC_MVFF, PCOP(&pc_plusw0), popGet2(AOP(result1), i, 1));

                                    if (i < size - 1)
                                        emitpcode(POC_INFW, PCOP(&pc_wreg));
                                }
                            }
                        }

                        ic->generated = 1;
                        ic->next->generated = 1;
                        break;
                    }
                    else if (
                        IS_SYMOP(left0) &&
                        IS_SYMOP(left1) && IS_SYMOP(result0) &&
                        isOperandEqual(left1, result0) &&
                        //OP_SYMBOL(left1)->regs[0] == OP_SYMBOL(result0)->regs[0] &&
                        //OP_SYMBOL(left1)->regs[1] == OP_SYMBOL(result0)->regs[1] &&
                        (IS_DATA_PTR(OP_SYMBOL(left0)->type) || IS_FARPTR(OP_SYMBOL(left0)->type)) && IS_OP_LITERAL(right0) &&
                        ullFromVal(AOP(right0)->aopu.aop_lit) < 128)
                    {
                        int i;
                        int size = AOP_SIZE(result1); // this is the size!!
                                                      //emitpcode(POC_LDPR, AOP(left0)->aopu.pcop);
                        mov2w(AOP(left0), 0);
                        emitpcode(POC_MVWF, PCOP(&pc_fsr0l));
                        mov2w(AOP(left0), 1);
                        emitpcode(POC_MVWF, PCOP(&pc_fsr0h));

                        if (noStack)
                        {
                            mov2w(AOP(right0), 0);
                            for (i = 0; i < size; i++)
                            {
                                //emitpcode22(POC_MVFF, PCOP(&pc_plusw0), popGet2(AOP(result1), i,1));
                                emitpcode22(POC_MVFF, PCOP(&pc_plusw0), popGet2(AOP(result1), i, 1));
                                if (i < size - 1)
                                    emitpcode(POC_INFW, PCOP(&pc_wreg));
                            }
                        }
                        else
                        {
                            mov2w(AOP(right0), 0);
                            for (i = 0; i < size; i++)
                            {
                                //emitpcode22(POC_MVFF, PCOP(&pc_plusw0), popGet2(AOP(result1), i,1));
                                emitpcode22(POC_MVFF, PCOP(&pc_plusw0), get_argument_pcop(i + 1));
                                if (i < size - 1)
                                    emitpcode(POC_INFW, PCOP(&pc_wreg));
                            }
                            for (i = 0; i < size; i++)
                            {
                                emitpcode(POC_MVFW, get_argument_pcop(i + 1));
                                emitpcode(POC_MVWF, popGet2(AOP(result1), i, 1));
                            }
                        }

                        ic->generated = 1;
                        ic->next->generated = 1;
                        break;
                    }
                    else // 2018 NOV, we try to optimize ROM LOOKUP
                    {
                        if (
                            HY08A_getPART()->isEnhancedCore != 5 &&
                            IS_SYMOP(left0) && IS_SYMOP(left1) &&
                            (IS_CODEPTR(OP_SYMBOL(left0)->type)) && isOperandEqual(left1, result0)
                            // 2 cases, one is literal known
                            // unsigned not exceed 256
                            &&
                            (AOP(left0)->type == AOP_PCODE && AOP(left0)->aopu.pcop->type == PO_IMMEDIATE &&
                             //PCOI(AOP(left0)->aopu.pcop)->r->size < 256 &&
                             AOP_SIZE(right0) == 1 &&
                             AOP_SIZE(result1) == 1 // single byte only
                             ))
                        {
                            emitpcode(POC_MVLP, AOP(left0)->aopu.pcop);
                            PCI(pb->pcTail)->pcop->flags.imm2label = 1;
                            emitpcode(POC_MVFW, popGet(AOP(right0), 0));
                            // linking time selection !!
                            call_libraryfunc("__cptrget1"); // linking time selection for table lookup, by MVLP instruction
                                                            // final _ means linking time selection
                            emitpcode(POC_MVWF, popGet(AOP(result1), 0));
                            ic->generated = 1;
                            ic->next->generated = 1;
                            break;
                        }

                        // else
                        // {

                        // 	freeAsmop(left0, NULL, ic, false);
                        // 	freeAsmop(right0, NULL, ic, false);
                        // 	freeAsmop(result0, NULL, ic, true);

                        // 	freeAsmop(left1, NULL, ic->next, false);
                        // 	freeAsmop(result1, NULL, ic->next, true);
                        // }
                    }
                }
            }

            if (ic->next && ic->next->op == '=' && !POINTER_SET(ic->next) && isOperandEqual(IC_RESULT(ic), IC_RIGHT(ic->next)) &&
                !IS_TRUE_SYMOP(IC_RESULT(ic)) &&
                OPrangeLimited(IC_RESULT(ic), ic->seq, ic->next->seq))
            {
                IC_RESULT(ic) = IC_RESULT(ic->next);
                ic->next->generated = 1;
            }
            genPlus(ic);
            break;

        case '-':
            // try optimize --

            //else
            if (!genDjnz(ic, ifxForOpSpecial(IC_RESULT(ic), ic)))
            {

                // special case is v - lit == 0  ... left to user, change

                //if ((ic->next->op == IFX) && isOperandEqual(IC_RESULT(ic),IC_LEFT(ic->next) ) &&
                //(IC_LEFT(ic)->isLiteral || IC_RIGHT(ic)->isLiteral) && // at least one literal
                //IS_ITEMP(IC_RESULT(ic)) && !isOperandEqual(IC_RESULT(ic), IC_LEFT(ic)) &&
                //!isOperandEqual(IC_RESULT(ic), IC_RIGHT(ic)) &&
                //!operand_used_again(IC_RESULT(ic), ic->next->next))
                //{
                //if (IC_LEFT(ic)->isLiteral)
                //{
                //// la -v == lb ==> v == LA-LB
                //fprintf(stderr, "minus eq\n");
                //}
                //}

                // this is --
                if (IC_RIGHT(ic)->isLiteral && IS_ITEMP(IC_RESULT(ic)) &&
                    ic->next->op == '=' && !POINTER_SET(ic->next) && IS_SYMOP(IC_LEFT(ic)) && IS_SYMOP(IC_RESULT(ic->next)) &&
                    isOperandEqual(IC_RESULT(ic->next), IC_LEFT(ic)) &&
                    isOperandEqual(IC_RESULT(ic), IC_RIGHT(ic->next)) &&
                    !IS_TRUE_SYMOP(IC_RESULT(ic)) &&
                    //!operand_used_again(IC_RESULT(ic), ic->next->next->next)
                    OPrangeLimited(IC_RESULT(ic), ic->seq, ic->next->next->seq))
                {
                    // consider single use only
                    if (isOperandEqual(IC_RESULT(ic), IC_LEFT(ic->next->next)))
                    {
                        IC_LEFT(ic->next->next) = IC_LEFT(ic);
                    }
                    if (isOperandEqual(IC_RESULT(ic), IC_RIGHT(ic->next->next)))
                    {
                        IC_RIGHT(ic->next->next) = IC_LEFT(ic);
                    }
                    if (isOperandEqual(IC_RESULT(ic), IC_RESULT(ic->next->next)))
                    {
                        IC_RESULT(ic->next->next) = IC_LEFT(ic);
                    }
                    IC_RESULT(ic) = IC_LEFT(ic);
                    ic->next->generated = 1;
                }
                genMinus(ic);
            }

            break;

        case '*':

            genMult(ic);
            break;

        case '/':
            genDiv(ic);
            break;

        case '%':
            genMod(ic);
            break;

        case '>':
            genCmpGt(ic, ifxForOpSpecial(IC_RESULT(ic), ic));
            break;

        case '<':
            genCmpLt(ic, ifxForOpSpecial(IC_RESULT(ic), ic));
            break;

        case LE_OP:
        case GE_OP:
        case NE_OP:

            /* note these two are xlated by algebraic equivalence
               during parsing SDCC.y */
            werror(E_INTERNAL_ERROR, __FILE__, __LINE__, "got '>=' or '<=' shouldn't have come here");
            break;

        case EQ_OP:
            genCmpEq(ic, ifxForOpSpecial(IC_RESULT(ic), ic));
            break;

        case AND_OP:
            genAndOp(ic);
            break;

        case OR_OP:
            genOrOp(ic);
            break;

        case '^':
            genXor(ic, ifxForOpSpecial(IC_RESULT(ic), ic));
            break;

        case '|':
            genOr(ic, ifxForOpSpecial(IC_RESULT(ic), ic));
            break;

        case BITWISEAND:
            genAnd(ic, ifxForOpSpecial(IC_RESULT(ic), ic));
            break;

        case INLINEASM:
            HY08A_genInline(ic);
            break;

        // case RRC:
        //     genRRC(ic);
        //     break;

        // case RLC:
        //     genRLC(ic);
        //     break;

        case GETABIT:
            genGetABit(ic);
            break;

        case ROT:
          genRot (ic);
          break;
            //case GETHBIT:
            //genGetHbit(ic);
            //break;

        case LEFT_OP:
            genLeftShift(ic);
            break;

        case RIGHT_OP:
            genRightShift(ic);
            break;

        case GET_VALUE_AT_ADDRESS:
            // special case of aaa.b++, where b is a bit
            //	if (ic->next && ic->next->next &&
            //		ic->next->op == '+' && ic->next->next->op == '=' &&
            //		POINTER_SET(ic->next->next) &&
            //		IC_LEFT(ic)==IC_RESULT(ic->next->next) &&
            //		IC_RESULT(ic)==IC_LEFT(ic->next) &&
            //IS_BITFIELD(OP_SYMBOL(IC_LEFT(ic))->etype) &&
            //	OP_SYMBOL(IC_LEFT(ic))->etype->select.s._bitLength==1
            //
            //		)
            //	{
            //		ic->generated = 1;
            //		ic->next->generated = 1;
            //		ic->next->next->op = '^';
            //		IC_LEFT(ic->next->next) = IC_RESULT(ic->next->next);
            //		IC_RIGHT(ic->next->next) = operandFromLit(1.0);
            //		OP_SYMBOL(IC_RESULT(ic->next->next))->remat = 0;
            //		OP_SYMBOL(IC_RESULT(ic->next->next))->rematX = 0;
            //		OP_SYMBOL(IC_RESULT(ic->next->next))->regType = REG_CND; // special
            //		break;
            //	}
            genPointerGet(ic);
            break;

        case '=':
            if (POINTER_SET(ic))
                genPointerSet(ic);
            else
            {
                // for volatile AND/OR, it will read volatile first
                // we try to combine it
                if (IC_RIGHT(ic)->isvolatile && ic->next->op == BITWISEAND || ic->next->op == '^' || ic->next->op == '|')
                {
                    operand *right0, *result0;
                    operand *result1, *left1, *right1;
                    operand *cond2 = NULL;
                    if (ic->next->next && ic->next->next->op == IFX)
                    {
                        cond2 = IC_COND(ic->next->next);
                        aopOp(cond2, ic->next->next, false);
                    }
                    right0 = IC_RIGHT(ic);
                    result0 = IC_RESULT(ic);
                    right1 = IC_RIGHT(ic->next);
                    result1 = IC_RESULT(ic->next);
                    left1 = IC_LEFT(ic->next);
                    aopOp(right0, ic, false);
                    aopOp(result0, ic, true);
                    aopOp(right1, ic->next, false);
                    aopOp(result1, ic->next, true);
                    aopOp(left1, ic->next, false);

                    if (cond2)
                    {
                        if (!IS_TRUE_SYMOP(result1) && IS_SYMOP(result0) && IS_SYMOP(left1) && AOP_SIZE(result0) == 1 && AOP_SIZE(left1) == 1 &&
                            IS_SYMOP(cond2) && result1 == cond2 &&
                            //OP_SYMBOL(left1)->regs[0] == OP_SYMBOL(result0)->regs[0]
                            isOperandEqual(left1, result0) && !IS_TRUE_SYMOP(result1) &&
                            // !operand_used_again(result1, ic->next->next->next)
                            OPrangeLimited(result1, ic->seq, ic->next->next->seq))
                        {
                            ic->generated = true;
                            IC_LEFT(ic->next) = IC_RIGHT(ic);
                            //IC_COND(ic->next->next) = IC_RIGHT(ic);
                            break;
                        }
                    }
                    else if (!IS_TRUE_SYMOP(result1) && IS_SYMOP(result0) && IS_SYMOP(left1) && AOP_SIZE(result0) == 1 && AOP_SIZE(left1) == 1 &&
                             //OP_SYMBOL(left1)->regs[0] == OP_SYMBOL(result0)->regs[0]
                             !IS_TRUE_SYMOP(result0) &&
                             isOperandEqual(left1, result0) &&
                             //!operand_used_again(result1, ic->next->next)
                             OPrangeLimited(result1, ic->seq, ic->next->seq))
                    {
                        ic->generated = true;
                        IC_LEFT(ic->next) = IC_RIGHT(ic);
                        break;
                    }
                }

                if (POINTER_GET(ic->next))
                {
                    operand *right0, *result0;
                    operand *left1, *result1; // right1 is null
                    unsigned long long lit;
                    //left0 = IC_LEFT(ic);
                    right0 = IC_RIGHT(ic);
                    result0 = IC_RESULT(ic);
                    left1 = IC_LEFT(ic->next);
                    //right1 = IC_RIGHT(ic->next);
                    result1 = IC_RESULT(ic->next);
                    //aopOp(left0, ic, true);
                    aopOp(right0, ic, false);
                    aopOp(result0, ic, true);

                    aopOp(left1, ic->next, false);
                    //aopOp(right1, ic->next, false);
                    aopOp(result1, ic->next, true);
                    if (IC_RIGHT(ic)->type == VALUE && IC_LEFT(ic->next)->type == SYMBOL && OP_SYMBOL(IC_RESULT(ic)) == OP_SYMBOL(IC_LEFT(ic->next)) && (lit = ullFromVal(AOP(right0)->aopu.aop_lit)) < 256)
                    {
                        // remat assig
                        //IC_RESULT(ic->next) = IC_RIGHT(ic);
                        // AOP type
                        //OP_SYMBOL(right0)->type = OP_SYMBOL(result1)->type; // change?

                        // gen code here!!!
                        //sym_link *ptype = operandType(result1);
                        //sym_link *retype = getSpec(operandType(left1));
                        //sym_link *letype = getSpec(ptype);

                        genNearPointerGet(right0, result1, ic->next);

                        ic->generated = 1;
                        ic->next->generated = 1;

                        break;
                    }
                }
                else if (POINTER_SET(ic->next))
                {

                    operand *right0, *result0;
                    operand *right1, *result1; // right1 is null
                    unsigned long long lit;
                    //left0 = IC_LEFT(ic);
                    right0 = IC_RIGHT(ic);
                    result0 = IC_RESULT(ic);
                    right1 = IC_RIGHT(ic->next);
                    //right1 = IC_RIGHT(ic->next);
                    result1 = IC_RESULT(ic->next);
                    //aopOp(left0, ic, true);
                    aopOp(right0, ic, false);
                    aopOp(result0, ic, true);

                    aopOp(right1, ic->next, false);
                    //aopOp(right1, ic->next, false);
                    aopOp(result1, ic->next, true);
                    if (IC_RIGHT(ic)->type == VALUE && IC_RESULT(ic->next)->type == SYMBOL && OP_SYMBOL(IC_RESULT(ic)) == OP_SYMBOL(IC_RESULT(ic->next)) && (lit = ullFromVal(AOP(right0)->aopu.aop_lit)) < 256)
                    {
                        // remat assig
                        //IC_RESULT(ic->next) = IC_RIGHT(ic);
                        // AOP type
                        //OP_SYMBOL(right0)->type = OP_SYMBOL(result1)->type; // change?

                        // gen code here!!!
                        sym_link *ptype = operandType(result1);
                        //sym_link *retype = getSpec(operandType(right1));
                        sym_link *letype = getSpec(ptype);

                        if (IS_BITFIELD(letype))
                        {
                            //AOP_TYPE(right0) = AOP_PCODE; // force
                            genPackBits(letype, right0, right1, -1);
                        }
                        else
                        {
                            int size = AOP_SIZE(result1);
                            int i;
                            for (i = 0; i < size; i++)
                            {
                                mov2w(AOP(right1), i);
                                emitpcode(POC_MVWF, popGetLit((unsigned int)(lit + i)));
                            }
                        }

                        ic->generated = 1;
                        ic->next->generated = 1;

                        break;
                    }
                }

				// special temp ADCR
				if (IS_SYMOP(IC_RIGHT(ic)) && (OP_SYMBOL(IC_RIGHT(ic))->name &&
					!strcmp(OP_SYMBOL(IC_RIGHT(ic))->name, "ADCR")))
				{
					if (!IS_TRUE_SYMOP(IC_RESULT(ic)) && ic->next->op == '=' &&
						IS_SYMOP(IC_RIGHT(ic->next)) && OP_SYMBOL(IC_RIGHT(ic->next)) == OP_SYMBOL(IC_RESULT(ic)) &&
						!operand_used_again(IC_RESULT(ic), ic->next->next))
					{
						IC_RESULT(ic) = IC_RESULT(ic->next);
						ic->next->generated = true;
					}
				}

				// 2021 Dec, simplify single temp self operation
				/*if (IS_ITEMP(IC_RESULT(ic)) && ic->next && ic->next->next &&
					!ic->next->generated && !ic->next->next->generated && 
					ic->next->next->op == '=' &&
					isOperandEqual(IC_RESULT(ic), IC_RESULT(ic->next)) &&
					isOperandEqual(IC_RIGHT(ic), IC_RESULT(ic->next->next)) &&
					(isOperandEqual(IC_RIGHT(ic), IC_RIGHT(ic->next)) || isOperandEqual(IC_RESULT(ic), IC_LEFT(ic->next)))
					&& !operand_used_again(IC_RESULT(ic), ic->next->next->next)
					)
				{
					if (isOperandEqual(IC_RIGHT(ic), IC_RIGHT(ic->next)))
					{
						IC_RIGHT(ic->next) = IC_RIGHT(ic);
					}
					if (isOperandEqual(IC_RIGHT(ic), IC_LEFT(ic->next)))
					{
						IC_LEFT(ic->next) = IC_RIGHT(ic);
					}
					IC_RESULT(ic->next) = IC_RIGHT(ic);
					ic->generated = true;
					ic->next->next->generated = true;
					break;					
				}*/
				
                genAssign(ic);
            }
            break;

        case IFX:
            genIfx(ic, NULL);
            break;

        case ADDRESS_OF:
            if (ic->next && ic->next->op == '=' && !POINTER_SET(ic->next) && isOperandEqual(IC_RESULT(ic), IC_RIGHT(ic->next)) &&
                !IS_TRUE_SYMOP(IC_RESULT(ic)) &&
                //!operand_used_again(IC_RESULT(ic), ic->next->next)
                OPrangeLimited(IC_RESULT(ic), ic->seq, ic->next->seq))
            {
                IC_RESULT(ic) = IC_RESULT(ic->next);
                ic->next->generated = 1;
            }
            genAddrOf(ic);
            break;

        case JUMPTABLE:
            genJumpTab(ic);
            break;

        case CAST:
            
            // 2018 add a special case for |=, for byte operation

            if (ic->next->op == '|' && ic->next->next->op == CAST && IC_RIGHT(ic->next)->type == VALUE)
            {
                operand *right0, *result0;
                operand *left1, *right1, *result1;
                operand *right2, *result2;
                right0 = IC_RIGHT(ic); // left is type!!
                result0 = IC_RESULT(ic);
                right1 = IC_RIGHT(ic->next);
                left1 = IC_LEFT(ic->next);
                result1 = IC_RESULT(ic->next);
                right2 = IC_RIGHT(ic->next->next); // left is type!!
                result2 = IC_RESULT(ic->next->next);
                aopOp(right0, ic, false);
                aopOp(result0, ic, true);
                aopOp(left1, ic->next, false);
                aopOp(right1, ic->next, false);
                aopOp(result1, ic->next, true);
                aopOp(right2, ic->next->next, false);
                aopOp(result2, ic->next->next, true);
                if (!IS_TRUE_SYMOP(result1) &&
                    IS_SYMOP(right0) && AOP_SIZE(right0) == 1 && IS_SYMOP(left1) && IS_SYMOP(result1) && IS_SYMOP(result0) &&
                    isOperandEqual(left1, result0) &&
                    isOperandEqual(result1, result0) &&
                    //OP_SYMBOL(left1)->regs[0] == OP_SYMBOL(result0)->regs[0] &&
                    //OP_SYMBOL(result1)->regs[0] == OP_SYMBOL(result0)->regs[0] &&
                    IS_SYMOP(result2) &&
                    OP_SYMBOL(result2) == OP_SYMBOL(right0) &&
                    !IS_TRUE_SYMOP(result1) &&
                    OPrangeLimited(result1, ic->seq, ic->next->next->seq)
                    //!operand_used_again(result1, ic->next->next->next)
                )
                {
                    ic->generated = true;
                    ic->next->next->generated = true;
                    IC_RESULT(ic->next) = IC_RIGHT(ic);
                    IC_LEFT(ic->next) = IC_RIGHT(ic);
                    break;
                }
            }

            // most simple casle

            //if (!options.nopeep && !isPointer2Stack(IC_LEFT(ic)) && ic->next &&
            if (!options.nopeep && ic->next && // this optimzation proceed if point to stack!!
                POINTER_GET(ic->next) &&
                //!operand_used_again(IC_RESULT(ic), ic->next->next)
                OPrangeLimited(IC_RESULT(ic), ic->seq, ic->next->seq))
            {
                // sometimes there will have this case
                operand *right0, *result0;
                operand *left1, *right1, *result1;
                right0 = IC_RIGHT(ic); // left is type!!
                result0 = IC_RESULT(ic);
                right1 = IC_RIGHT(ic->next);
                left1 = IC_LEFT(ic->next);
                result1 = IC_RESULT(ic->next);
                aopOp(right0, ic, false);
                aopOp(result0, ic, true);
                aopOp(left1, ic->next, false);
                aopOp(right1, ic->next, false);
                aopOp(result1, ic->next, true);
                if (IS_SYMOP(result1) && IS_SYMOP(result0) && IS_VALOP(right1) && (OP_SYMBOL(right0)->remat || OP_SYMBOL(right0)->rematX) &&
                    ullFromVal(AOP(right1)->aopu.aop_lit) == 0 && // I don't know why pointerget need right0?
                    !SPEC_CONST(OP_SYMBOL(right0)->etype) &&
                    //OP_SYMBOL(left1)->regs[0] == OP_SYMBOL(result0)->regs[0] &&
                    //OP_SYMBOL(left1)->regs[1] == OP_SYMBOL(result0)->regs[1]
                    isOperandEqual(left1, result0) &&
                    !IS_BITFIELD(getSpec (operandType (left1)->next)) &&
                     ( AOP(right0)->aopu.pcop->type !=PO_IMMEDIATE ||
                        (PCOI(AOP(right0)->aopu.pcop)->offset == 0 && 
                        PCOI(AOP(right0)->aopu.pcop)->index == 0 &&
                        PCOI(AOP(right0)->aopu.pcop)->_inX == 0 // don't be in X!!
                        )
                    ))
                {
                    IC_RIGHT(ic->next) = right0;
                    if (AOP_TYPE(right0) == AOP_IMMD) // IMM change to DIR
                        AOP_TYPE(right0) = AOP_DIR;
                    if (AOP(right0)->type == AOP_PCODE && AOP(right0)->aopu.pcop->type == PO_IMMEDIATE)
                    {
                        reg_info *r = PCOI(AOP(right0)->aopu.pcop)->r;
                        AOP(right0)->aopu.pcop = newpCodeOp(r->name, PO_GPR_REGISTER);
                        AOP(right0)->aopu.pcop->name = r->name;
                        PCOR(AOP(right0)->aopu.pcop)->r = r;
                        AOP(right0)->size = AOP(result1)->size; // ptr get the full size 2024 oct
                    }

                    genAssign(ic->next);
                    ic->generated = 1;
                    ic->next->generated = 1;
                    continue;
                }
            }
            else if (!options.nopeep && ic->next->next && ic->next->op == CALL &&
                     POINTER_SET(ic->next->next) &&
                     //!operand_used_again(IC_RESULT(ic), ic->next->next->next)
                     OPrangeLimited(IC_RESULT(ic), ic->seq, ic->next->next->seq))
            {
                // call an cast .. we reverse it directly
                // consider literal first

                //re-arrange
                // cast call ptrset
                iCode *iprev, *ics[5];
                ics[0] = ic;
                ics[1] = ic->next;
                ics[2] = ic->next->next;

                //ics[4] = ic->next->next->next->next;

                iprev = ic->prev;

                iprev->next = ics[1]; // cast call ptrset
                ics[1]->prev = iprev;

                ics[1]->next = ics[0]; // cast
                ics[0]->prev = ics[1];

                ics[0]->next = ics[2]; // plus
                ics[2]->prev = ics[0];

                ic = iprev;
                continue;
            }
            else

                if (!options.nopeep && ic->next->next && ic->next->next->next && ic->next->op == '+' && ic->next->next->op == CALL &&
                    POINTER_SET(ic->next->next->next) &&
                    //!operand_used_again(IC_RESULT(ic), ic->next->next->next->next)
                    OPrangeLimited(IC_RESULT(ic), ic->seq, ic->next->next->next->seq))
            {
                operand *left0, *right0, *result0;
                //operand *para; no para call
                operand *right1, *result1; // left1 is null

                //int resultX = IS_TRUE_SYMOP(IC_LEFT(ic->next)) && IN_XSPACE(SPEC_OCLS(OP_SYMBOL(IC_LEFT(ic->next))->etype));
                left0 = IC_LEFT(ic->next);
                right0 = IC_RIGHT(ic->next);
                result0 = IC_RESULT(ic->next);
                //para = IC_LEFT(ic->next->next); // consider only 1 parameter
                //left1 = IC_LEFT(ic->next);
                right1 = IC_RIGHT(ic->next->next->next);
                result1 = IC_RESULT(ic->next->next->next);
                aopOp(left0, ic->next, true);
                aopOp(right0, ic->next, false);
                aopOp(result0, ic->next, true);

                //aopOp(para, ic->next->next, false);

                //aopOp(left1, ic->next, false);
                aopOp(right1, ic->next->next->next, false);
                aopOp(result1, ic->next->next->next, true);

                // consider literal first
                if (IS_SYMOP(result1) &&
                    //OP_SYMBOL(result1)->regs[0] == OP_SYMBOL(result0)->regs[0] &&
                    isOperandEqual(result1, result0) &&
                    IS_PTR(OP_SYMBOL(left0)->type))
                {
                    //re-arrange
                    // cast plus send call ptrset
                    iCode *iprev, *ics[5];
                    ics[0] = ic;
                    ics[1] = ic->next;
                    ics[2] = ic->next->next;
                    ics[3] = ic->next->next->next;
                    //ics[4] = ic->next->next->next->next;

                    iprev = ic->prev;

                    iprev->next = ics[2]; // 2 is call
                    ics[2]->prev = iprev;

                    ics[2]->next = ics[0]; // cast
                    ics[0]->prev = ics[2];

                    ics[0]->next = ics[1]; // plus
                    ics[1]->prev = ics[0];

                    ics[1]->next = ics[3];
                    ics[3]->prev = ics[1]; // ptrset

                    ic = iprev;
                    continue;
                }
            }
            else

                if (!options.nopeep && ic->next->next && ic->next->next->next && ic->next->next->next->next && ic->next->op == '+' && ic->next->next->op == SEND && ic->next->next->next->op == CALL &&
                    POINTER_SET(ic->next->next->next->next) &&
                    //!operand_used_again(IC_RESULT(ic), ic->next->next->next->next->next)
                    OPrangeLimited(IC_RESULT(ic), ic->seq, ic->next->next->next->next->seq))
            {
                operand *left0, *right0, *result0;
                operand *para;
                operand *right1, *result1; // left1 is null

                //int resultX = IS_TRUE_SYMOP(IC_LEFT(ic->next)) && IN_XSPACE(SPEC_OCLS(OP_SYMBOL(IC_LEFT(ic->next))->etype));
                left0 = IC_LEFT(ic->next);
                right0 = IC_RIGHT(ic->next);
                result0 = IC_RESULT(ic->next);
                para = IC_LEFT(ic->next->next); // consider only 1 parameter
                                                //left1 = IC_LEFT(ic->next);
                right1 = IC_RIGHT(ic->next->next->next->next);
                result1 = IC_RESULT(ic->next->next->next->next);
                aopOp(left0, ic->next, true);
                aopOp(right0, ic->next, false);
                aopOp(result0, ic->next, true);

                aopOp(para, ic->next->next, false);

                //aopOp(left1, ic->next, false);
                aopOp(right1, ic->next->next->next->next, false);
                aopOp(result1, ic->next->next->next->next, true);

                // consider literal first
                if (IS_SYMOP(result1) &&
                    OP_SYMBOL(result1)->regs[0] == OP_SYMBOL(result0)->regs[0] &&
                    IS_PTR(OP_SYMBOL(left0)->type) &&
                    (IS_VALOP(para) || (IS_SYMOP(para) && (AOP_SIZE(para) != AOP_SIZE(result0) || OP_SYMBOL(result0)->regs[0] != OP_SYMBOL(para)->regs[0] || OP_SYMBOL(result0)->regs[1] != OP_SYMBOL(para)->regs[1]))))
                {
                    //re-arrange
                    // cast plus send call ptrset
                    iCode *iprev, *ics[5];
                    ics[0] = ic;
                    ics[1] = ic->next;
                    ics[2] = ic->next->next;
                    ics[3] = ic->next->next->next;
                    ics[4] = ic->next->next->next->next;

                    iprev = ic->prev;

                    iprev->next = ics[2];
                    ics[2]->prev = iprev;

                    ics[2]->next = ics[3];
                    ics[3]->prev = ics[2];

                    ics[3]->next = ics[0];
                    ics[0]->prev = ics[3];

                    ics[0]->next = ics[1];
                    ics[1]->prev = ics[0];

                    ics[1]->next = ics[4];
                    ics[4]->prev = ics[1];

                    ic = iprev;
                    continue;
                }
            }
            else if (ic->next->op == '=' && POINTER_SET(ic->next) &&
                     //!operand_used_again(IC_RESULT(ic), ic->next->next)
                     OPrangeLimited(IC_RESULT(ic), ic->seq, ic->next->seq))
            {
                operand *right0, *result0;
                operand *right1, *result1; // left1 is null

                right0 = IC_RIGHT(ic);
                result0 = IC_RESULT(ic);

                right1 = IC_RIGHT(ic->next);
                result1 = IC_RESULT(ic->next);

                aopOp(right0, ic, false);
                aopOp(result0, ic, true);

                aopOp(right1, ic->next, false);
                aopOp(result1, ic->next, true);
                if (
                    result1->type == SYMBOL &&
                    //OP_SYMBOL(result1)->regs[0] == OP_SYMBOL(result0)->regs[0] &&
                    //OP_SYMBOL(result1)->regs[1] == OP_SYMBOL(result0)->regs[1] &&
                    isOperandEqual(result1, result0) &&
                    //!( operandType(result0)->next && IS_BITFIELD(operandType(result0)->next)) &&
                    // 2 cases, one is literal known
                    // another is target size <128, 128 is not ok, because PLUSW cannot + 128
                    (
                        AOP(right0)->type == AOP_PCODE && AOP(right0)->aopu.pcop->type == PO_IMMEDIATE &&
                        //AOP_SIZE(OP_SYMBOL(left0)) < 128 &&
                        !(PCOI(AOP(right0)->aopu.pcop)->_inX) &&
                        PCOI(AOP(right0)->aopu.pcop)->r->size < 128))
                {
                    if(IS_BITFIELD(operandType(result0)->next))
                    {
                       genCast(ic);
                       genPointerSet(ic->next);
                    }else 
                    {
                    IC_RESULT(ic->next) = right0;
                    right0->aop->size = right1->aop->size;
                    if (AOP(right1)->type == AOP_IMMD)
                        AOP(right1)->type = AOP_DIR;

                        genAssign(ic->next);

                    }

                    ic->generated = 1;
                    ic->next->generated = 1;
                }
            } // special case
            else if (
                ic->next && ic->next->next &&
                ic->next->op == '+' && IC_RIGHT(ic->next)->type == VALUE && // only const we optimize it !!
                ic->next->next->op == '=' && POINTER_SET(ic->next->next) && ic->next->next->next &&
                //!operand_used_again(IC_RESULT(ic), ic->next->next->next) &&
                OPrangeLimited(IC_RESULT(ic), ic->seq, ic->next->next->seq) &&
                //!operand_used_again(IC_RESULT(ic->next), ic->next->next->next)
                OPrangeLimited(IC_RESULT(ic->next), ic->next->seq, ic->next->next->seq))
            {
                operand *right0, *result0;
                operand *left1, *right1, *result1;
                operand *right2, *result2; // left1 is null

                right0 = IC_RIGHT(ic);
                result0 = IC_RESULT(ic);

                left1 = IC_LEFT(ic->next);
                right1 = IC_RIGHT(ic->next);
                result1 = IC_RESULT(ic->next);
                right2 = IC_RIGHT(ic->next->next);
                result2 = IC_RESULT(ic->next->next);

                aopOp(right0, ic, false);
                aopOp(result0, ic, true);

                aopOp(left1, ic, true);
                aopOp(right1, ic, false);
                aopOp(result1, ic, true);

                aopOp(right2, ic->next, false);
                aopOp(result2, ic->next, true);
                // for a[b]=c, we can use plusw only if a is array, size < 128
                // b .. is OK, because if a size < 128, we can always use plusw0
                if (
                    //OP_SYMBOL(result2)->regs[0] == OP_SYMBOL(result1)->regs[0] &&
                    //OP_SYMBOL(result2)->regs[1] == OP_SYMBOL(result1)->regs[1] &&
                    isOperandEqual(result2, result1) &&
                    isOperandEqual(left1, result0) &&
                    //OP_SYMBOL(left1)->regs[0] == OP_SYMBOL(result0)->regs[0] &&
                    //OP_SYMBOL(left1)->regs[1] == OP_SYMBOL(result0)->regs[1] &&

                    // 2 cases, one is literal known
                    // another is target size <128, 128 is not ok, because PLUSW cannot + 128
                    (
                        AOP(right0)->type == AOP_PCODE && AOP(right0)->aopu.pcop->type == PO_IMMEDIATE // right0 cast to ...
                        &&
                        //AOP_SIZE(OP_SYMBOL(left0)) < 128 &&
                        PCOI(AOP(right0)->aopu.pcop)->r->size < 128))

                {
                    IC_RESULT(ic->next->next) = right0;
                    right0->aop->size = right2->aop->size;
                    PCOI(right0->aop->aopu.pcop)->index += (int)ullFromVal(AOP(right1)->aopu.aop_lit); // +=?
                    if (AOP(right1)->type == AOP_IMMD)
                        AOP(right1)->type = AOP_DIR; // change it!!
                    if (AOP(right1)->type == AOP_PCODE && AOP(right1)->aopu.pcop->type == PO_IMMEDIATE)
                    {
                        reg_info *r = PCOI(AOP(right1)->aopu.pcop)->r;
                        AOP(right1)->aopu.pcop = newpCodeOp(r->name, PO_GPR_REGISTER);
                        AOP(right1)->aopu.pcop->name = r->name;
                        PCOR(AOP(right1)->aopu.pcop)->r = r;
                    }
                    genAssign(ic->next->next);
                    ic->generated = 1;
                    ic->next->generated = 1;
                    ic->next->next->generated = 1;
                }
            }
            else if (
                ic->next && ic->next->next &&
                ic->next->op == '+' && IC_RIGHT(ic->next)->type == VALUE && // only const we optimize it !!
                POINTER_GET(ic->next->next) && ic->next->next->next &&
                //!operand_used_again(IC_RESULT(ic), ic->next->next->next) &&
                OPrangeLimited(IC_RESULT(ic), ic->seq, ic->next->next->seq) &&
                //!operand_used_again(IC_RESULT(ic->next), ic->next->next->next)
                OPrangeLimited(IC_RESULT(ic->next), ic->next->seq, ic->next->next->seq))
            {
                operand *right0, *result0;
                operand *left1, *right1, *result1;
                operand *left2, *result2; // left1 is null

                right0 = IC_RIGHT(ic);
                result0 = IC_RESULT(ic);

                left1 = IC_LEFT(ic->next);
                right1 = IC_RIGHT(ic->next);
                result1 = IC_RESULT(ic->next);
                left2 = IC_LEFT(ic->next->next);
                result2 = IC_RESULT(ic->next->next);

                aopOp(right0, ic, false);
                aopOp(result0, ic, true);

                aopOp(left1, ic, true);
                aopOp(right1, ic, false);
                aopOp(result1, ic, true);

                aopOp(left2, ic->next, false);
                aopOp(result2, ic->next, true);
                // for a[b]=c, we can use plusw only if a is array, size < 128
                // b .. is OK, because if a size < 128, we can always use plusw0
                if (
                    //OP_SYMBOL(left2)->regs[0] == OP_SYMBOL(result1)->regs[0] &&
                    //OP_SYMBOL(left2)->regs[1] == OP_SYMBOL(result1)->regs[1] &&
                    isOperandEqual(left2, result1) &&

                    //OP_SYMBOL(left1)->regs[0] == OP_SYMBOL(result0)->regs[0] &&
                    //OP_SYMBOL(left1)->regs[1] == OP_SYMBOL(result0)->regs[1] &&
                    isOperandEqual(left1, result0) &&
                    // 2 cases, one is literal known
                    // another is target size <128, 128 is not ok, because PLUSW cannot + 128
                    (
                        AOP(right0)->type == AOP_PCODE && AOP(right0)->aopu.pcop->type == PO_IMMEDIATE // right0 cast to ...
                        &&
                        //AOP_SIZE(OP_SYMBOL(left0)) < 128 &&
                        PCOI(AOP(right0)->aopu.pcop)->r->size < 128))

                {
                    //char buf[1024];
                    IC_LEFT(ic->next->next) = right0;
                    right0->aop->size = right1->aop->size;

                    // there will be array issues!!!! // += or = ???
                    PCOI(right0->aop->aopu.pcop)->index = (int)ullFromVal(AOP(right1)->aopu.aop_lit);

                    //PCOR(right0->aop->aopu.pcop)->instance = ullFromVal(AOP(right1)->aopu.aop_lit);
                    //PCOR(right0->aop->aopu.pcop)->r->size = 256;
                    //AOP(right0)->type = AOP_DIR;
                    //snprintf(buf,1023,"%s", right0->aop->aopu.pcop->name, (int)ullFromVal(AOP(right1)->aopu.aop_lit));
                    //AOP(right0)->aopu.aop_immd = strdup(buf);
                    //AOP(right0)->aopu.
                    //genAssign(ic->next->next);
                    genPointerGet(ic->next->next);
                    ic->generated = 1;
                    ic->next->generated = 1;
                    ic->next->next->generated = 1;
                }
            }

			// 2022 unsigned char -> int ->mul ==> mul directly
			if (!options.nopeep && !IS_TRUE_SYMOP(IC_RESULT(ic)))
			{
				iCode *icp = ic->next;
				bool hasmul = false;
				if (icp )
				{
					if (icp->op == '*')
					{
						hasmul = true;
					}
				}
				if (hasmul)
				{
					if (isOperandEqual(IC_RESULT(ic), IC_LEFT(icp)) ||
						isOperandEqual(IC_RESULT(ic), IC_RIGHT(icp))
						)
					{
						if (!operand_used_again(IC_RESULT(ic), icp->next))
						{
							// if source unsigned, keep result unsigned
							if (IS_SPEC(operandType(IC_RIGHT(ic)))&& SPEC_USIGN(operandType(IC_RIGHT(ic))))
							{
								if (IS_SPEC(operandType(IC_RESULT(ic))))
								{
									SPEC_USIGN(operandType(IC_RESULT(ic))) = true;
								}
							}
							
						}
					}
				}
			}


            if (!ic->generated)
                genCast(ic);

            //{
            //	operand *right=IC_RIGHT(ic), *result=IC_RESULT(ic);
            //	aopOp(right, ic, FALSE);
            //	aopOp(result, ic, FALSE);
            //	if (ic->next->op == '=' &&
            //		IS_SYMOP(IC_RESULT(ic)) &&
            //		!IS_TRUE_SYMOP(IC_RESULT(ic)) &&
            //		IS_PTR(OP_SYMBOL(right)->type) &&
            //		AOP(right)->aopu.pcop->type == PO_IMMEDIATE &&
            //		POINTER_SET(ic->next))
            //	{
            //		//ic->generated = 1;
            //		IC_RESULT(ic->next) = right; // force remat back
            //		AOP(right)->aopu.pcop->type = PO_DIR; // change it!!
            //	}
            //	else
            //	{
            //		genCast(ic);
            //	}
            //}
            break;

        case RECEIVE:
            // there could be many receive

            if (ic->prev && (ic2 = findPrevFuncICODE(ic->prev)) != NULL && !(0))
                genReceive(ic, IC_LEFT(ic2));
            else
                genReceive(ic, NULL);
            break;

        case SEND:

            if (ic->next && ic->next->next && ic->next->next->next && ic->next->op == SEND && ic->next->next->op == CALL &&
                //ic->next->next->ulrrcnd.lrr.left->type == SYMBOL &&
                (!strncmp(IC_LEFT(ic->next->next)->svt.symOperand->name, "_mul", 4)) //include long long
                &&
                IC_LEFT(ic)->type == VALUE && IC_LEFT(ic)->isLiteral &&
                (mul2shift = canMult2Shift(IC_LEFT(ic)->svt.valOperand->type->select.s.const_val.v_int)) > 0)
            {

                iCode *shiftic = newiCode(LEFT_OP, IC_LEFT(ic->next), IC_LEFT(ic));
                IC_LEFT(ic)->svt.valOperand->type->select.s.const_val.v_int = mul2shift; // change!!
                IC_RESULT(shiftic) = IC_RESULT(ic->next->next);                          // call result will be saved

                genLeftShift(shiftic);
                ic->generated = 1;
                ic->next->generated = 1;
                ic->next->next->generated = 1;
                //ic->next->next->next->generated = 1;
            }
            else if (ic->next && ic->next->next && ic->next->next->next && ic->next->op == SEND && ic->next->next->op == CALL &&
                     //ic->next->next->ulrrcnd.lrr.left->type == SYMBOL &&
                     (!strncmp(IC_LEFT(ic->next->next)->svt.symOperand->name, "_divu", 5)) //include long long
                     &&
                     IC_LEFT(ic->next)->type == VALUE && IC_LEFT(ic->next)->isLiteral &&
                     (mul2shift = canMult2Shift(IC_LEFT(ic->next)->svt.valOperand->type->select.s.const_val.v_int)) > 0)
            {
                iCode *shiftic = newiCode(LEFT_OP, IC_LEFT(ic), IC_LEFT(ic->next));
                IC_LEFT(ic->next)->svt.valOperand->type->select.s.const_val.v_int = mul2shift; // change!!
                IC_RESULT(shiftic) = IC_RESULT(ic->next->next);                                // call result will be saved

                genRightShift(shiftic);
                ic->generated = 1;
                ic->next->generated = 1;
                ic->next->next->generated = 1;
            }
            else
                addSet(&_G.sendSet, ic);
            break;

        case DUMMY_READ_VOLATILE:
            genDummyRead(ic);
            break;

        case CRITICAL:
            genCritical(ic);
            break;

        case ENDCRITICAL:
            genEndCritical(ic);
            break;

			// modified 2022: for bug-3368.c
		case IPUSH_VALUE_AT_ADDRESS:
			genPointerPush(ic);
			break;


        default:
            fprintf(stderr, "UNHANDLED iCode: ");
            piCode(ic, stderr);
            ic = ic;
            break;
        }
    }

    /* now we are ready to call the
       peep hole optimizer */
    if (!options.nopeep)
    {
        peepHole(&genLine.lineHead);
    }
    /* now do the actual printing */
    printLine(genLine.lineHead, codeOutBuf);

#ifdef PCODE_DEBUG
    DFPRINTF((stderr, "printing pBlock\n\n"));
    printpBlock(stdout, pb);
#endif

    /* destroy the line list */
    destroy_line_list();
}

/* This is not safe, as a AOP_PCODE/PO_IMMEDIATE might be used both as literal
 * (meaning: representing its own address) or not (referencing its contents).
 * This can only be decided based on the operand's type. */
static int
aop_isLitLike (asmop * aop)
{
    assert (aop);
    if (aop->type == AOP_LIT)
        return 1;
    if (aop->type == AOP_IMMD)
        return 1;
    if (((aop->type == AOP_PCODE)&&!aop->imm2dir) && ((aop->aopu.pcop->type == PO_LITERAL) 
		|| (aop->aopu.pcop->type == PO_IMMEDIATE)))
		
    {
        /* this should be treated like a literal/immediate (use MOVLW/ADDLW/SUBLW
         * instead of MOVFW/ADDFW/SUBFW, use popGetAddr instead of popGet) */
        return 1;
    }
    return 0;
}

int
op_isLitLike (operand * op)
{
    assert (op);
    if (aop_isLitLike (AOP (op)))
        return 1;
    if (IS_SYMOP (op) && IS_FUNC (OP_SYM_TYPE (op)))
        return 1;
    if (IS_SYMOP (op) && IS_PTR (OP_SYM_TYPE (op)) && (AOP_TYPE (op) == AOP_PCODE) && (AOP (op)->aopu.pcop->type == PO_IMMEDIATE)
            && (!op->isaddr) // I don't know what is addr is used .. very very strange
            && !AOP(op)->imm2dir
       )

    {
        return 1;
    }

    return 0;
}
