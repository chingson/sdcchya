/*-------------------------------------------------------------------------
  genarith.c - source file for code generation - arithmetic

  Written By -  Sandeep Dutta . sandeep.dutta@usa.net (1998)
         and -  Jean-Louis VERN.jlvern@writeme.com (1999)
  Bug Fixes  -  Wojciech Stryjewski  wstryj1@tiger.lsu.edu (1999 v2.1.9a)
  PIC port   -  Scott Dattalo scott@dattalo.com (2000)

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

  In other words, you are welcome to use, share and improve this program.
  You are forbidden to forbid anyone else to use, share and improve
  what you give them.   Help stamp out software-hoarding!

  Notes:
  000123 mlh  Moved aopLiteral to SDCCglue.c to help the split
      Made everything static

	  2016/2017: modified by chingsonchen@hycontek.com.tw for HYCON 8-bit CPU
-------------------------------------------------------------------------*/

#if defined(_MSC_VER) && (_MSC_VER < 1300)
#define __FUNCTION__ __FILE__
#endif

#include "common.h"
#include "newalloc.h"
//#include "SDCCglobl.h"
//#include "SDCCpeeph.h"

#include "gen.h"
#include "pcode.h"
#include "ralloc.h"

#define BYTEofLONG(l, b) ((l >> (b << 3)) & 0xff)

const char *AopType(short type)
{
	switch (type)
	{
	case AOP_LIT:
		return "AOP_LIT";
		break;
	case AOP_REG:
		return "AOP_REG";
		break;
	case AOP_DIR:
		return "AOP_DIR";
		break;
	case AOP_STK:
		return "AOP_STK";
		break;
	case AOP_IMMD:
		return "AOP_IMMD";
		break;
	case AOP_STR:
		return "AOP_STR";
		break;
	case AOP_CRY:
		return "AOP_CRY";
		break;
	case AOP_PCODE:
		return "AOP_PCODE";
		break;
	}

	return "BAD TYPE";
}

const char *pCodeOpType(pCodeOp *pcop)
{

	if (pcop)
	{

		switch (pcop->type)
		{

		case PO_NONE:
			return "PO_NONE";
		case PO_W:
			return "PO_W";
		case PO_STATUS:
			return "PO_STATUS";
		case PO_FSR:
			return "PO_FSR";
		case PO_INDF:
			return "PO_INDF";
		case PO_POINC0:
			return "PO_POINC0";
		case PO_PRINC0:
			return "PO_PRINC0";
		case PO_PODEC0:
			return "PO_PODEC0";
		case PO_PLUSW:
			return "PO_PLUSW";
		case PO_PRINC2:
			return "PO_PRINC2";
		case PO_PODEC2:
			return "PO_PODEC2";
		case PO_POINC:
			return "PO_POINC";
		case PO_INTE0:
			return "PO_INTE0";
		case PO_GPR_REGISTER:
			return "PO_GPR_REGISTER";
		case PO_GPR_POINTER:
			return "PO_GPR_POINTER";
		case PO_GPR_BIT:
			return "PO_GPR_BIT";
		case PO_GPR_TEMP:
			return "PO_GPR_TEMP";
		case PO_SFR_REGISTER:
			return "PO_SFR_REGISTER";
		case PO_PCL:
			return "PO_PCL";
		case PO_PCLATH:
			return "PO_PCLATH";
		case PO_PCLATU:
			return "PO_PCLATU";
		case PO_LITERAL:
			return "PO_LITERAL";
		case PO_IMMEDIATE:
			return "PO_IMMEDIATE";
		case PO_DIR:
			return "PO_DIR";
		case PO_CRY:
			return "PO_CRY";
		case PO_BIT:
			return "PO_BIT";
		case PO_STR:
			return "PO_STR";
		case PO_LABEL:
			return "PO_LABEL";
		case PO_WILD:
			return "PO_WILD";
		case PO_FSRPTR0:
			return "FSRPTR0";
		case PO_FSRPTR1:
			return "FSRPTR1";
		case PO_FSRPTR2:
			return "FSRPTR2";
		case PO_ADCR:
			return "ADCR";
		case PO_ADCO1:
			return "ADCO1";
		case PO_ADCO2:
			return "ADCO2";

		case PO_FSR0L:
			return "FSR0L";

		case PO_FSR0H:
			return "FSR0H";

		case PO_SSPBUF:
			return "SSPBUF";
		case PO_PRINC:
			return "PRINC";
		case PO_PRODL:
			return "PRODL"; // HW Multiplier
		case PO_PRODH:
			return "PRODH";
		case PO_TBLPTRL:
			return "TBLPTRL";
		case PO_TBLPTRH:
			return "TBLPTRH";
		case PO_TBLDL:
			return "TBLDL";
		case PO_TBLDH:
			return "TBLDH";
		case PO_XDATA:
			return "XDATA";
		case PO_ADCRB:
			return "ADCRB";
		case PO_ADCO1B:
			return "ADCO1B";
		case PO_ADCO2B:
			return "ADCO2B";
		}
	}

	return "BAD PO_TYPE";
}

/*-----------------------------------------------------------------*/
/* genPlusIncr :- does addition with increment if possible         */
/*-----------------------------------------------------------------*/
static bool genPlusIncr(iCode *ic)
{
	unsigned long long icount;
	unsigned int size = HY08A_getDataSize(IC_RESULT(ic));
	int leftX = IS_TRUE_SYMOP(IC_LEFT(ic)) && IN_XSPACE(SPEC_OCLS(OP_SYMBOL(IC_LEFT(ic))->etype));
	if (size == 2 || leftX)
		return FALSE; // size2 use other methods!!
	FENTRY;

	DEBUGHY08A_emitcode("; ***", "%s  %d", __FUNCTION__, __LINE__);
	DEBUGHY08A_emitcode("; ", "result %s, left %s, right %s",
						AopType(AOP_TYPE(IC_RESULT(ic))),
						AopType(AOP_TYPE(IC_LEFT(ic))),
						AopType(AOP_TYPE(IC_RIGHT(ic))));

	/* will try to generate an increment */
	/* if the right side is not a literal
    we cannot */
	if (AOP_TYPE(IC_LEFT(ic)) == AOP_CRY ||
		AOP_TYPE(IC_RIGHT(ic)) == AOP_CRY ||
		AOP_TYPE(IC_RESULT(ic)) == AOP_CRY)
		return FALSE;

	if (AOP_TYPE(IC_RIGHT(ic)) != AOP_LIT)
		return FALSE;

	DEBUGHY08A_emitcode("; ", "%s  %d", __FUNCTION__, __LINE__);
	/* if the literal value of the right hand side
    is greater than 1 then it is faster to add */
	if ((icount = (unsigned long long)ullFromVal(AOP(IC_RIGHT(ic))->aopu.aop_lit)) > 2ULL)
		return FALSE;

	/* if increment 16 bits in register */
	if (HY08A_sameRegs(AOP(IC_LEFT(ic)), AOP(IC_RESULT(ic))) &&
		(icount == 1))
	{

		int offset = MSB16;

		if (size == 2)
		{
			emitpcode(POC_INSUZ, popGet(AOP(IC_RESULT(ic)), LSB));
			emitpcode(POC_INF, popGet(AOP(IC_RESULT(ic)), MSB16));
		}
		else

		{
			emitpcode(POC_INF, popGet(AOP(IC_RESULT(ic)), LSB));
			//HY08A_emitcode("incf","%s,f",aopGet(AOP(IC_RESULT(ic)),LSB,FALSE,FALSE));

			if (size > 1)
				emitpcode(POC_MVL, popGetLit(0));
			while (--size)
			{
				//emitSKPNZ;
				emitpcode(POC_ADCWF, popGet(AOP(IC_RESULT(ic)), offset++));
				//HY08A_emitcode(" incf","%s,f",aopGet(AOP(IC_RESULT(ic)),offset++,FALSE,FALSE));
			}
		}

		return TRUE;
	}

	//DEBUGHY08A_emitcode ("; ","%s  %d",__FUNCTION__,__LINE__);
	///* if left is in accumulator  - probably a bit operation*/
	//if( strcmp(aopGet(AOP(IC_LEFT(ic)),0,FALSE,FALSE),"a")  &&
	//        (AOP_TYPE(IC_RESULT(ic)) == AOP_CRY) ) {
	//
	//        emitpcode(POC_BCF, popGet(AOP(IC_RESULT(ic)),0));
	//        HY08A_emitcode("bcf","(%s >> 3), (%s & 7)",
	//                AOP(IC_RESULT(ic))->aopu.aop_dir,
	//                AOP(IC_RESULT(ic))->aopu.aop_dir);
	//        if(icount)
	//                emitpcode(POC_XORLW,popGetLit(1));
	//        //HY08A_emitcode("xorlw","1");
	//        else
	//                emitpcode(POC_ANDLW,popGetLit(1));
	//        //HY08A_emitcode("andlw","1");
	//
	//        emitSKPZ;
	//        emitpcode(POC_BSF, popGet(AOP(IC_RESULT(ic)),0));
	//        HY08A_emitcode("bsf","(%s >> 3), (%s & 7)",
	//                AOP(IC_RESULT(ic))->aopu.aop_dir,
	//                AOP(IC_RESULT(ic))->aopu.aop_dir);
	//
	//        return TRUE;
	//}

	///* if the sizes are greater than 1 then we cannot */
	//if (AOP_SIZE(IC_RESULT(ic)) > 1 ||
	//        AOP_SIZE(IC_LEFT(ic)) > 1   )
	//        return FALSE ;
	//
	///* If we are incrementing the same register by two: */
	//
	//if (HY08A_sameRegs(AOP(IC_LEFT(ic)), AOP(IC_RESULT(ic))) ) {
	//
	//        while (icount--)
	//                emitpcode(POC_INF, popGet(AOP(IC_RESULT(ic)),0));
	//        //HY08A_emitcode("incf","%s,f",aopGet(AOP(IC_RESULT(ic)),0,FALSE,FALSE));
	//
	//        return TRUE ;
	//}
	//
	//DEBUGHY08A_emitcode ("; ","couldn't increment ");

	return FALSE;
}
//
///*-----------------------------------------------------------------*/
///* genAddlit - generates code for addition                         */
///*-----------------------------------------------------------------*/
static void genAddLit2byte(operand *result, int offr, int lit)
{
	assert(result);
	int inX = IS_TRUE_SYMOP(result) && IN_XSPACE(SPEC_OCLS(OP_SYMBOL(result)->etype));
	FENTRY;

	switch (lit & 0xff)
	{
	case 0:
		break;
	case 1:
		if (inX)
		{
			emitpcode(POC_LDPR, popGet(AOP(result), offr));
			emitpcode(POC_INF, PCOP_INDF0);
		}
		else
			emitpcode(POC_INF, popGet(AOP(result), offr));
		break;
	case 0xff:
		if (inX)
		{
			emitpcode(POC_LDPR, popGet(AOP(result), offr));
			emitpcode(POC_DECF, PCOP_INDF0);
		}
		else

			emitpcode(POC_DECF, popGet(AOP(result), offr));
		break;
	default:
		if (inX)
		{
			emitpcode(POC_LDPR, popGet(AOP(result), offr));
			emitpcode(POC_MVL, popGetLit(lit & 0xff));
			emitpcode(POC_ADDWF, PCOP_INDF0);
		}
		else
		{
			emitpcode(POC_MVL, popGetLit(lit & 0xff));
			emitpcode(POC_ADDWF, popGet(AOP(result), offr));
		}
	}
}

//emitpcode(POC_MVFW, popGet(AOP(left), 0));
#define emitMOVWF(a, b) emitpcode(POC_MVWF, popGet(AOP(a), b))

//static void emitMOVWF(operand *reg, int offset)
//{
//    FENTRY;
//    if(!reg)
//        return;
//
//    if (IN_XSPACE(SPEC_OCLS(OP_SYMBOL(reg)->etype)))
//    {
//		emitpcode2(POC_MVFF, PCOP_W, popGet(AOP(reg), offset));
//    } else
//        emitpcode(POC_MVWF, popGet(AOP(reg),offset));
//
//}

static void genAddLit(iCode *ic, long long lit)
{
	int size, same;
	int lo;

	operand *result;
	operand *left;

	//int leftX; // left will not, it is from 51's
	int resultX;
	int leftX;

	FENTRY;
	DEBUGHY08A_emitcode("; ***", "%s  %d", __FUNCTION__, __LINE__);

	left = IC_LEFT(ic);
	result = IC_RESULT(ic);

	assert(left);
	assert(result);

	// it is still possible leftX
	// but it will not have both X, since the 8051
	leftX = IS_TRUE_SYMOP(left) && IN_XSPACE(SPEC_OCLS(OP_SYMBOL(left)->etype));
	resultX = IS_TRUE_SYMOP(result) && IN_XSPACE(SPEC_OCLS(OP_SYMBOL(result)->etype));

	same = HY08A_sameRegs(AOP(left), AOP(result));
	size = HY08A_getDataSize(result);
	if (size > HY08A_getDataSize(left))
	{
		size = HY08A_getDataSize(left);
	}

	/*
     * Fix accessing libsdcc/<*>/idata.c:_cinit in __code space.
     */

	// 2020 try to fix 1byte+lit unsigned

	if (AOP_SIZE(left) == 1 && SPEC_USIGN(operandType(left)) && OP_SYMBOL(result)->isitmp && IS_SPEC(OP_SYMBOL(result)->type)) // must be specifier?
		SPEC_USIGN(operandType(result)) = 1;

	if (!same && AOP_PCODE == AOP_TYPE(IC_LEFT(ic)) && !AOP(IC_LEFT(ic))->imm2dir) // pcode is convert from special one
	{
		int u;

		if (debug_verbose)
		{
			printf("%s:%u: CHECK: using address of '%s' instead of contents\n",
				   ic->filename, ic->lineno,
				   popGetAddr(AOP(IC_LEFT(ic)), 0, lit & 0xff)->name);
		} // if

		if (resultX)
			emitpcode(POC_LDPR, popGet(AOP(IC_RESULT(ic)), 0));
		for (u = 0; u < size; ++u)
		{
			emitpcode(POC_MVL, popGetAddr(AOP(IC_LEFT(ic)), u, (int)lit)); // this is very strange!!
			if (resultX)
				emitpcode(POC_MVWF, PCOP_POINC0);
			else
				emitpcode(POC_MVWF, popGet(AOP(IC_RESULT(ic)), u));
		} // for

		assert(result);
		if (size < HY08A_getDataSize(result))
		{
			for (u = size; u < HY08A_getDataSize(result); ++u)
			{
				/* XXX: Might fail for u >= size?!? */
				emitpcode(POC_MVL, popGetAddr(AOP(IC_LEFT(ic)), u, (int)lit));
				if (resultX)
					emitpcode(POC_MVWF, PCOP_POINC0);
				else
					emitpcode(POC_MVWF, popGet(AOP(IC_RESULT(ic)), u));
			} // for
		}	  // if

		goto out;
	} // if

	if (same)
	{
		/* Handle special cases first */
		if (size == 1)
		{
			genAddLit2byte(result, 0, (int)lit);
		}
		else if (size == 2)
		{
			int hi = (lit >> 8) & 0xff;
			lo = lit & 0xff;

			switch (hi)
			{
			case 0:
				/* lit = 0x00LL */
				DEBUGHY08A_emitcode("; hi = 0", "%s  %d", __FUNCTION__, __LINE__);
				switch (lo)
				{
				case 0: // same + 0000
					break;
				case 1: // same + 0001
					if (resultX)
					{
						emitpcode(POC_LDPR, popGet(AOP(IC_RESULT(ic)), 0));
						emitpcode(POC_INSUZ, PCOP_POINC0);
						emitpcode(POC_INF, PCOP_INDF0);
					}
					else
					{
						emitpcode(POC_INSUZ, popGet(AOP(result), 0));
						//emitSKPNZ;
						emitpcode(POC_INF, popGet(AOP(result), MSB16));
					}
					break;
				case 0xff: // same + 00ff
					if (resultX)
					{
						emitpcode(POC_LDPR, popGet(AOP(IC_RESULT(ic)), 0));
						emitpcode(POC_DECF, PCOP_INDF0);
						emitpcode(POC_INSZW, PCOP_POINC0); // if it is FF
						emitpcode(POC_INF, PCOP_INDF0);
					}
					else
					{
						emitpcode(POC_DECF, popGet(AOP(result), 0));
						emitpcode(POC_INSZW, popGet(AOP(result), 0)); // if it is FF
						emitpcode(POC_INF, popGet(AOP(result), MSB16));
					}
					break;
				default: // same + 00xx, xx!=1, xx!=0xff
					if (resultX)
					{
						emitpcode(POC_LDPR, popGet(AOP(IC_RESULT(ic)), 0));
						emitpcode(POC_MVL, popGetLit(lit & 0xff));
						emitpcode(POC_ADDWF, PCOP_POINC0); // if it is FF;
						emitSKPNC;
						emitpcode(POC_INF, PCOP_POINC0); // if it is FF;
					}
					else
					{
						emitpcode(POC_MVL, popGetLit(lit & 0xff));
						emitpcode(POC_ADDWF, popGet(AOP(result), 0));
						emitSKPNC;
						emitpcode(POC_INF, popGet(AOP(result), MSB16));
					}
					break;
				} // switch
				break;

			case 1:
				/* lit = 0x01LL */
				DEBUGHY08A_emitcode("; hi = 1", "%s  %d", __FUNCTION__, __LINE__);
				switch (lo)
				{
				case 0: /* 0x0100 */
					if (resultX)
					{
						emitpcode(POC_LDPR, popGet(AOP(IC_RESULT(ic)), 1));
						emitpcode(POC_INF, PCOP_INDF0);
					}
					else
					{
						emitpcode(POC_INF, popGet(AOP(result), MSB16));
					}
					break;
				case 1: /* 0x0101  */
					if (resultX)
					{
						emitpcode(POC_LDPR, popGet(AOP(IC_RESULT(ic)), 1));
						emitpcode(POC_INF, PCOP_PODEC0);
						emitpcode(POC_INSUZ, PCOP_POINC0);
						//emitSKPNZ;
						emitpcode(POC_INF, PCOP_INDF0);
					}
					else
					{
						emitpcode(POC_INF, popGet(AOP(result), MSB16));
						emitpcode(POC_INSUZ, popGet(AOP(result), 0));
						//emitSKPNZ;
						emitpcode(POC_INF, popGet(AOP(result), MSB16));
					}
					break;
					//              case 0xff: /* 0x01ff  4 intrustions is not an improve*/
					//if (resultX)
					//{

					//}
					//else
					//{
					// emitpcode(POC_DECF, popGet(AOP(result), 0));
					// emitpcode(POC_INSZW, popGet(AOP(result), 0));
					// emitpcode(POC_INF, popGet(AOP(result), MSB16));
					// emitpcode(POC_INF, popGet(AOP(result), MSB16));
					//}
					//                  break;
				default: // 0x01xx, xx!= 1,ff
					if (resultX)
					{
						emitpcode(POC_LDPR, popGet(AOP(IC_RESULT(ic)), 0));
						emitpcode(POC_MVL, popGetLit(lo));
						emitpcode(POC_ADDWF, PCOP_POINC0);
						emitpcode(POC_MVL, popGetLit(1));
						emitpcode(POC_ADCWF, PCOP_INDF0);
					}
					else
					{
						emitpcode(POC_MVL, popGetLit(lo));
						emitpcode(POC_ADDWF, popGet(AOP(result), 0));
						emitpcode(POC_MVL, popGetLit(1));
						emitpcode(POC_ADCWF, popGet(AOP(result), MSB16));
					}
					//emitpcode(POC_INF, popGet(AOP(result),MSB16));
					break;
				} // switch
				break;

			case 0xff:
				DEBUGHY08A_emitcode("; hi = ff", "%s  %d", __FUNCTION__, __LINE__);
				/* lit = 0xffLL */
				switch (lo)
				{
				case 0: /* 0xff00 */
					if (resultX)
					{
						emitpcode(POC_LDPR, popGet(AOP(IC_RESULT(ic)), 1));
						emitpcode(POC_DECF, PCOP_INDF0);
					}
					else
					{
						emitpcode(POC_DECF, popGet(AOP(result), MSB16));
					}
					break;
				case 1: /*0xff01 */
					if (resultX)
					{
						emitpcode(POC_LDPR, popGet(AOP(IC_RESULT(ic)), 0));
						emitpcode(POC_INSZ, PCOP_POINC0);
						emitpcode(POC_DECF, PCOP_INDF0);
					}
					else
					{
						emitpcode(POC_INSZ, popGet(AOP(result), 0));
						emitpcode(POC_DECF, popGet(AOP(result), MSB16));
					}
					break;
				case 0xff: // 0xffff
					if (resultX)
					{
						emitpcode(POC_LDPR, popGet(AOP(IC_RESULT(ic)), 0));
						emitpcode(POC_DECF, PCOP_INDF0);
						emitpcode(POC_INSUZW, PCOP_POINC0);
						emitpcode(POC_DECF, PCOP_INDF0);
					}
					else
					{
						emitpcode(POC_DECF, popGet(AOP(result), 0));
						emitpcode(POC_INSUZW, popGet(AOP(result), 0));	 // if == 0xff
						emitpcode(POC_DECF, popGet(AOP(result), MSB16)); // -- down to 3 byte, seems ok
					}
					break;
				default:
					// 0xffxx, xx is not 00/01/ff
					if (resultX)
					{
						emitpcode(POC_LDPR, popGet(AOP(IC_RESULT(ic)), 0));
						emitpcode(POC_MVL, popGetLit(lo));
						emitpcode(POC_ADDWF, PCOP_POINC0);
						emitSKPC;
						emitpcode(POC_DECF, PCOP_INDF0);
					}
					else
					{
						emitpcode(POC_MVL, popGetLit(lo));
						emitpcode(POC_ADDWF, popGet(AOP(result), 0));
						emitSKPC;
						emitpcode(POC_DECF, popGet(AOP(result), MSB16));
					}
				}
				break;

			default: // high is not 00/01/ff, depends on low

				switch (lo)
				{
				case 0: // XX00, add high byte is enough
					if (resultX)
					{
						emitpcode(POC_LDPR, popGet(AOP(IC_RESULT(ic)), 1));
						emitpcode(POC_MVL, popGetLit(hi));
						emitpcode(POC_ADDWF, PCOP_INDF0);
					}
					else
					{
						emitpcode(POC_MVL, popGetLit(hi));
						emitpcode(POC_ADDWF, popGet(AOP(IC_RESULT(ic)), 1));
					}
					break;
				case 1:
					if (resultX)
					{
						emitpcode(POC_LDPR, popGet(AOP(IC_RESULT(ic)), 0));
						emitpcode(POC_INF, PCOP_POINC0);
						emitpcode(POC_MVL, popGetLit(hi));
						emitpcode(POC_ADCWF, PCOP_INDF0);
					}
					else
					{
						emitpcode(POC_INF, popGet(AOP(IC_RESULT(ic)), 0));
						emitpcode(POC_MVL, popGetLit(hi));
						emitpcode(POC_ADCWF, popGet(AOP(IC_RESULT(ic)), 1));
					}
					break;
				case 0xff:
					if (resultX)
					{
						emitpcode(POC_LDPR, popGet(AOP(IC_RESULT(ic)), 0));
						emitpcode(POC_DECF, PCOP_POINC0);
						emitpcode(POC_MVL, popGetLit(hi));
						emitpcode(POC_ADCWF, PCOP_INDF0);
					}
					else
					{
						emitpcode(POC_DECF, popGet(AOP(IC_RESULT(ic)), 0));
						emitpcode(POC_MVL, popGetLit(hi));
						emitpcode(POC_ADCWF, popGet(AOP(IC_RESULT(ic)), 1));
					}
					break;

				default:
					DEBUGHY08A_emitcode("; hi is generic", "%d   %s  %d", hi, __FUNCTION__, __LINE__);
					if (resultX)
					{
						emitpcode(POC_LDPR, popGet(AOP(IC_RESULT(ic)), 0));
						emitpcode(POC_MVL, popGetLit(lo));
						emitpcode(POC_ADDWF, PCOP_POINC0);
						emitpcode(POC_MVL, popGetLit(hi));
						emitpcode(POC_ADCWF, PCOP_INDF0);
					}
					else
					{
						emitpcode(POC_MVL, popGetLit(lo));
						emitpcode(POC_ADDWF, popGet(AOP(result), 0));
						emitpcode(POC_MVL, popGetLit(hi));
						emitpcode(POC_ADCWF, popGet(AOP(result), MSB16));
					}
				}
				break;
			} // switch
		}
		else // size !=2
		{
			int carry_info = 0;
			int offset = 0;
			int lastlo = -1;
			/* size > 2 */
			DEBUGHY08A_emitcode(";  add lit to ...", "%s  %d", __FUNCTION__, __LINE__);

			while (size--)
			{
				lo = BYTEofLONG(lit, 0);
				if (resultX && lo != 0 && carry_info == 0)
					emitpcode(POC_LDPR, popGet(AOP(IC_RESULT(ic)), offset));

				if (carry_info)
				{

					if (lo != lastlo)
					{
						emitpcode(POC_MVL, popGetLit(lo));
						lastlo = lo;
					}
					if (resultX)
						emitpcode(POC_ADCWF, PCOP_POINC0);
					else
						emitpcode(POC_ADCWF, popGet(AOP(result), offset));

					carry_info = 2;
				}
				else
				{

					if ((lo & 0xff) == 0xff)
					{
						if (resultX)
							emitpcode(POC_DECF, PCOP_POINC0);
						else
							emitpcode(POC_DECF, popGet(AOP(result), offset));
					}
					else if ((lo & 0xff) == 1)
					{
						if (resultX)
							emitpcode(POC_INF, PCOP_POINC0);
						else
							emitpcode(POC_INF, popGet(AOP(result), offset));
					}
					else if ((lo & 0xff) != 0)
					{
						emitpcode(POC_MVL, popGetLit(lo));
						lastlo = lo;
						if (resultX)
							emitpcode(POC_ADDWF, PCOP_POINC0);
						else
							emitpcode(POC_ADDWF, popGet(AOP(result), offset));
					}

					if ((lo & 0xff) != 0) // is this a bug?
						carry_info = 1;

					//      if (lit <0x100)
					//        carry_info = 3;  /* Were adding only one byte and propogating the carry */
					//      else
					//        carry_info = 2;
					//      break;
					//} // switch
				} // if
				offset++;
				lit >>= 8;
			} // while
		}	  // if
	}
	else // not same
	{
		int offset = 1;
		DEBUGHY08A_emitcode(";  left and result aren't same", "%s  %d", __FUNCTION__, __LINE__);
		int resultbool = (AOP(result)->type == AOP_CRY); // if left bool it will be cast, result will not..

		if (size == 1)
		{
			/* left addend is in a register */
			switch (lit & 0xff)
			{
			case 0:

				if (leftX)
				{
					emitpcode21(POC_MVFF, popGet2(AOP(left), 0, 1), popGet(AOP(result), 0)); //PCOP1SETX;
				}
				else
				{
					if (resultbool)
						emitpcode(POC_RRFW, popGet(AOP(left), 0));
					else
						emitpcode(POC_MVFW, popGet(AOP(left), 0));
					if (resultX)
						emitpcode22(POC_MVFF, PCOP_W, popGet2(AOP(result), 0, 1));
					else
					{
						if (resultbool)
						{
							emitpcode(POC_BCF, popGet(AOP(result), 0));
							emitSKPZ;
							emitpcode(POC_BSF, popGet(AOP(result), 0));
						}
						else
							emitMOVWF(result, 0);
					}
				}
				break;
			case 1:
				if (leftX)
				{
					emitldpr(popGet(AOP(left), 0));
					emitpcode(POC_INFW, PCOP_INDF0);
				}
				else
				{
					emitpcode(POC_INFW, popGet(AOP(left), 0));

					if (resultX)
						emitpcode22(POC_MVFF, PCOP_W, popGet2(AOP(result), 0, 1));
					else if (resultbool)
					{
						emitpcode(POC_BCF, popGet(AOP(result), 0));
						emitSKPZ;
						emitpcode(POC_BSF, popGet(AOP(result), 0));
					}
					else
						emitMOVWF(result, 0);
				}
				break;
			case 0xff:
				if (leftX)
				{
					emitldpr(popGet(AOP(left), 0));
					emitpcode(POC_DECFW, PCOP_INDF0);
					emitMOVWF(result, 0);
				}
				else
				{
					emitpcode(POC_DECFW, popGet(AOP(left), 0));
					if (resultX)
						emitpcode22(POC_MVFF, PCOP_W, popGet2(AOP(result), 0, 1));
					else if (resultbool)
					{
						emitpcode(POC_BCF, popGet(AOP(result), 0));
						emitSKPZ;
						emitpcode(POC_BSF, popGet(AOP(result), 0));
					}
					else
						emitMOVWF(result, 0);
				}
				break;
			default:
				if (leftX)
				{
					emitldpr(popGet(AOP(left), 0));
					emitpcode(POC_MVL, popGetLit(lit & 0xff));
					emitpcode(POC_ADDFW, PCOP_INDF0);
				}
				else
				{
					emitpcode(POC_MVL, popGetLit(lit & 0xff));
					emitpcode(POC_ADDFW, popGet(AOP(left), 0));

					if (resultX)
						emitpcode22(POC_MVFF, PCOP_W, popGet2(AOP(result), 0, 1));
					else if (resultbool)
					{
						emitpcode(POC_BCF, popGet(AOP(result), 0));
						emitSKPZ;
						emitpcode(POC_BSF, popGet(AOP(result), 0));
					}
					else
						emitMOVWF(result, 0);
				}
			} // switch
		}
		else // size !=0 ==> X use ldpr
		{
			int clear_carry = 0; // carry is unknown or known
			// left use ind , result use mvff.. though another ind may save some..
			// but .. incase not both are in X
			if (leftX)
			{
				emitldpr(popGet(AOP(left), 0));
			}

			/* left is not the accumulator */
			if (resultX)
				emitldpr(popGet(AOP(result), 0));
			if (lit & 0xff)
			{
				if ((lit & 0xff) == 1)
				{
					if (leftX)
						emitpcode(POC_INFW, PCOP_POINC0);
					else
						emitpcode(POC_INFW, popGet(AOP(left), 0));
				}
				else if ((lit & 0xff) == 0xff)
				{
					if (leftX)
						emitpcode(POC_DECFW, PCOP_POINC0);
					else
						emitpcode(POC_DECFW, popGet(AOP(left), 0));
				}
				else
				{
					emitpcode(POC_MVL, popGetLit(lit & 0xff));
					if (leftX)
					{
						emitpcode(POC_ADDFW, PCOP_POINC0);
					}
					else
						emitpcode(POC_ADDFW, popGet(AOP(left), 0));
				}
				// here carry is useful, "clear_carry"=0;
			}
			else
			{
				if (leftX)
					emitpcode(POC_MVFW, PCOP_POINC0);
				else
					emitpcode(POC_MVFW, popGet(AOP(left), 0));
				/* We don't know the state of the carry bit at this point */
				// need clear carry before next add ==>clear_carry=1
				clear_carry = 1;
			} // if
			if (resultX)
				emitpcode(POC_MVWF, PCOP_POINC0);
			else
				emitMOVWF(result, 0);

			while (--size)
			{
				lit >>= 8;
				if (!clear_carry || ((lit & 0xff) != 1 && (lit & 0xff) != 0xff && (lit & 0xff) != 0x0))
					emitpcode(POC_MVL, popGetLit(lit & 0xff));
				if (leftX)
				{
					if (clear_carry)
					{
						if ((lit & 0xff) == 1)
						{
							emitpcode(POC_INFW, PCOP_POINC0);
							clear_carry = 0;
						}
						else if ((lit & 0xff) == 0xff)
						{
							emitpcode(POC_DECFW, PCOP_POINC0);
							clear_carry = 0;
						}
						else if ((lit & 0xff) == 0)
						{
							emitpcode(POC_MVFW, PCOP_POINC0);
							clear_carry = 1;
						}
						else
						{
							emitpcode(POC_ADDFW, PCOP_POINC0);
							clear_carry = 0;
						}
					}
					else
						emitpcode(POC_ADCFW, PCOP_POINC0);

					emitpcode(POC_MVWF, popGet(AOP(result), offset)); // last version bug?
				}
				else
				{
					if (clear_carry)
					{
						if ((lit & 0xff) == 1)
						{
							emitpcode(POC_INFW, popGet(AOP(left), offset));
							clear_carry = 0;
						}
						else if ((lit & 0xff) == 0xff)
						{
							emitpcode(POC_DECFW, popGet(AOP(left), offset));
							clear_carry = 0;
						}
						else if ((lit & 0xff) == 0x00)
						{
							emitpcode(POC_MVFW, popGet(AOP(left), offset));
							clear_carry = 1; //keep
						}
						else
						{
							emitpcode(POC_ADDFW, popGet(AOP(left), offset));
							clear_carry = 0;
						}
					}
					else
						emitpcode(POC_ADCFW, popGet(AOP(left), offset));
					if (resultX)
						emitpcode(POC_MVWF, PCOP_POINC0);
					else
						emitpcode(POC_MVWF, popGet(AOP(result), offset)); // last version bug?
				}
				offset++;
			} // while
		}	  // if
	}		  // if

out:
	size = HY08A_getDataSize(result);
	if (size > HY08A_getDataSize(left))
	{
		size = HY08A_getDataSize(left);
	}															// if
	addSign(result, size, !SPEC_USIGN(OP_SYMBOL(left)->etype)); // if left is signed, the result is signed!!
}

/*-----------------------------------------------------------------*/
/* genPlus - generates code for addition                           */
/*-----------------------------------------------------------------*/
void genPlus(iCode *ic)
{
	int size, offset = 0,sizelr=0;
	int resultX = 0;
	int leftbit = 0;
	int rightbit = 0;
	int resultbit = 0;

	/* special cases :- */
	DEBUGHY08A_emitcode("; ***", "%s  %d", __FUNCTION__, __LINE__);
	FENTRY;

	aopOp(IC_LEFT(ic), ic, FALSE);
	aopOp(IC_RIGHT(ic), ic, FALSE);
	aopOp(IC_RESULT(ic), ic, TRUE);

	DEBUGHY08A_AopType(__LINE__, IC_LEFT(ic), IC_RIGHT(ic), IC_RESULT(ic));

	/* if literal, literal on the right or
    if left requires ACC or right is already
    in ACC */
	//if(ic->lineno==155)
	//{
	//fprintf(stderr,"line 155\n");
	//}
	resultX = IS_TRUE_SYMOP(IC_RESULT(ic)) && IN_XSPACE(SPEC_OCLS(OP_SYMBOL(IC_RESULT(ic))->etype));
	// damm, we have no ADDC with literal
	if ( AOP_TYPE(IC_LEFT(ic)) == AOP_LIT && !IC_LEFT(ic)->isaddr)
	//if(op_isLitLike(AOP_TYPE(IC_RIGHT(ic)))) // move right lit to left, 
	{ // literal addr left is also ok?
		operand *t = IC_RIGHT(ic);
		IC_RIGHT(ic) = IC_LEFT(ic);
		IC_LEFT(ic) = t;
	}
	// keep left same to result
	else if (HY08A_sameRegs(AOP(IC_RIGHT(ic)), AOP(IC_RESULT(ic))))
	{
		operand *t = IC_RIGHT(ic);
		IC_RIGHT(ic) = IC_LEFT(ic);
		IC_LEFT(ic) = t; // keep left same to result
	}

	/* if left in bit space & right literal */
	if (AOP_TYPE(IC_LEFT(ic)) == AOP_CRY &&
		AOP_TYPE(IC_RIGHT(ic)) == AOP_LIT)
	{
		/* if result in bit space */
		if (AOP_TYPE(IC_RESULT(ic)) == AOP_CRY)
		{
			if (ullFromVal(AOP(IC_RIGHT(ic))->aopu.aop_lit) != 0LL)
			{	// true++=true, false++=true
				//emitpcode(POC_BTGF, popGet(AOP(IC_RESULT(ic)),0));
				//if (!HY08A_sameRegs(AOP(IC_LEFT(ic)), AOP(IC_RESULT(ic))) )
				//    emitpcode(POC_BTSZ, popGet(AOP(IC_LEFT(ic)),0));
				//emitpcode(POC_XORF, popGet(AOP(IC_RESULT(ic)),0));

				//emitpcode(POC_BTSZ, popGet(AOP(IC_LEFT(ic)), 0)); // if lit!=0, result always = true
				emitpcode(POC_BSF, popGet(AOP(IC_RESULT(ic)), 0));
			}
			else
			{
				if (!HY08A_sameRegs(AOP(IC_LEFT(ic)), AOP(IC_RESULT(ic)))) // else result = left
				{
					emitpcode(POC_BCF, popGet(AOP(IC_RESULT(ic)), 0));
					emitpcode(POC_BTSZ, popGet(AOP(IC_LEFT(ic)), 0));
					emitpcode(POC_BSF, popGet(AOP(IC_RESULT(ic)), 0));
				}
			}
		}
		else
		{
			size = HY08A_getDataSize(IC_RESULT(ic));
			while (size--)
			{
				MOVA(aopGet(AOP(IC_RIGHT(ic)), offset, FALSE, FALSE));
				HY08A_emitcode("addc", "a,#00  ;%d", __LINE__);
				aopPut(AOP(IC_RESULT(ic)), "a", offset++);
			}
		}
		goto release;
	}

	/* if I can do an increment instead
    of add then GOOD for ME */
	if (genPlusIncr(ic) == TRUE)
		goto release;

	size = HY08A_getDataSize(IC_RESULT(ic));
	leftbit = (AOP(IC_LEFT(ic))->type == AOP_CRY);
	resultbit = (AOP(IC_RESULT(ic))->type == AOP_CRY);
	rightbit = (AOP(IC_RIGHT(ic))->type == AOP_CRY);

	if (AOP(IC_RIGHT(ic))->type == AOP_LIT)
	{
		/* Add a literal to something else */
		unsigned long long lit = (unsigned long long)ullFromVal(AOP(IC_RIGHT(ic))->aopu.aop_lit);
		DEBUGHY08A_emitcode(";", "adding lit to something. size %d", size);

		genAddLit(ic, lit);
		goto release;
	}
	else if (rightbit)
	{ // right side is bit

		HY08A_emitcode(";bitadd", "right is bit: %s", aopGet(AOP(IC_RIGHT(ic)), 0, FALSE, FALSE));
		HY08A_emitcode(";bitadd", "left is bit: %s", aopGet(AOP(IC_LEFT(ic)), 0, FALSE, FALSE));
		HY08A_emitcode(";bitadd", "result is bit: %s", aopGet(AOP(IC_RESULT(ic)), 0, FALSE, FALSE));

		/* here we are adding a bit to a char or int */
		if (leftbit) // left is bit!! bit + bit ... I really don't like it
		{
			if (resultbit) // if result is bit, we use OR operation
			{
				emitpcode(POC_BCF, popGet(AOP(IC_RESULT(ic)), 0)); // clear then set if any is true
				emitpcode(POC_BTSZ, popGet(AOP(IC_LEFT(ic)), 0));
				emitpcode(POC_BSF, popGet(AOP(IC_RESULT(ic)), 0)); // clear then set if any is true
				emitpcode(POC_BTSZ, popGet(AOP(IC_RIGHT(ic)), 0));
				emitpcode(POC_BSF, popGet(AOP(IC_RESULT(ic)), 0)); // clear then set if any is true
				goto release;
			}
			else
			{
				emitpcode(POC_CLRF, popGet(AOP(IC_RESULT(ic)), 0)); // clear then set if any is true
				emitpcode(POC_BTSZ, popGet(AOP(IC_LEFT(ic)), 0));
				emitpcode(POC_INF, popGet(AOP(IC_RESULT(ic)), 0)); // clear then set if any is true
				emitpcode(POC_BTSZ, popGet(AOP(IC_RIGHT(ic)), 0));
				emitpcode(POC_INF, popGet(AOP(IC_RESULT(ic)), 0)); // clear then set if any is true
				goto release;
			}
		}
		if (size == 1)
		{
			if (HY08A_sameRegs(AOP(IC_LEFT(ic)), AOP(IC_RESULT(ic))))
			{
				emitpcode(POC_BTSZ, popGet(AOP(IC_RIGHT(ic)), 0));
				emitpcode(POC_INF, popGet(AOP(IC_RESULT(ic)), 0));
			}
			else
			{

				emitpcode(POC_MVFW, popGet(AOP(IC_LEFT(ic)), 0));
				emitpcode(POC_BTSZ, popGet(AOP(IC_RIGHT(ic)), 0));
				emitpcode(POC_INFW, popGet(AOP(IC_LEFT(ic)), 0));

				if (AOP_TYPE(IC_RESULT(ic)) == AOP_CRY)
				{
					emitpcode(POC_ANDLW, popGetLit(1));
					emitpcode(POC_BCF, popGet(AOP(IC_RESULT(ic)), 0));
					emitSKPZ;
					emitpcode(POC_BSF, popGet(AOP(IC_RESULT(ic)), 0));
				}
				else
				{
					emitpcode(POC_MVWF, popGet(AOP(IC_RESULT(ic)), 0));
				}
			}
		}
		else
		{
			int offset = 1;
			DEBUGHY08A_emitcode("; ***", "%s  %d", __FUNCTION__, __LINE__);
			if (HY08A_sameRegs(AOP(IC_LEFT(ic)), AOP(IC_RESULT(ic))))
			{
				emitCLRZ;
				emitpcode(POC_BTSZ, popGet(AOP(IC_RIGHT(ic)), 0));
				emitpcode(POC_INF, popGet(AOP(IC_RESULT(ic)), 0));
			}
			else
			{

				emitpcode(POC_MVFW, popGet(AOP(IC_LEFT(ic)), 0));
				emitpcode(POC_BTSZ, popGet(AOP(IC_RIGHT(ic)), 0));
				emitpcode(POC_INFW, popGet(AOP(IC_LEFT(ic)), 0));
				emitMOVWF(IC_RIGHT(ic), 0);
			}

			while (--size)
			{
				emitSKPZ;
				emitpcode(POC_INF, popGet(AOP(IC_RESULT(ic)), offset++));
			}
		}
	}
	else
	{
		DEBUGHY08A_emitcode("; ***", "%s  %d", __FUNCTION__, __LINE__);

		/* Add the first bytes */
		if (resultX)
			emitldpr(popGet(AOP(IC_RESULT(ic)), 0));

		// we don't support ACC
		/*if(strcmp(aopGet(AOP(IC_LEFT(ic)),0,FALSE,FALSE),"a") == 0 ) {
            emitpcode(POC_ADDFW, popGet(AOP(IC_RIGHT(ic)),0));
            if (resultX)
                emitpCode(POC_MVWF, PCOP_POINC0);
            else
                emitpcode(POC_MVWF, popGet(AOP(IC_RESULT(ic)), 0));
        } else*/

		//{
		if (op_isLitLike(IC_RIGHT(ic)))
			emitpcode(POC_MVL, popGet(AOP(IC_RIGHT(ic)), 0));
		else
			emitpcode(POC_MVFW, popGet(AOP(IC_RIGHT(ic)), 0));

		if (HY08A_sameRegs(AOP(IC_LEFT(ic)), AOP(IC_RESULT(ic))))
			emitpcode(POC_ADDWF, popGet(AOP(IC_LEFT(ic)), 0));
		else
		{
			HYA_OPCODE poc = POC_ADDFW;

			if (op_isLitLike(IC_LEFT(ic)))
				poc = POC_ADDLW;
			emitpcode(poc, popGetAddr(AOP(IC_LEFT(ic)), 0, 0));
			if (resultX)
				emitpcode(POC_MVWF, PCOP_POINC0);
			else
				emitpcode(POC_MVWF, popGet(AOP(IC_RESULT(ic)), 0));
		}
		//}
		// 2024 I don't know why LR size comes different ... fxxk!!
		sizelr = max(AOP_SIZE(IC_RIGHT(ic)),AOP_SIZE(IC_LEFT(ic)));
		size = min(AOP_SIZE(IC_RESULT(ic)), sizelr) - 1;
		offset = 1;

		if (size)
		{ //always keep left same to result
			if (HY08A_sameRegs(AOP(IC_LEFT(ic)), AOP(IC_RESULT(ic))))
			{
				if (op_isLitLike(IC_RIGHT(ic)))
				{
					while (size--)
					{
						emitpcode(POC_MVL, popGetAddr(AOP(IC_RIGHT(ic)), offset, 0));
						//emitSKPNC; // there is no ADDC LW !!
						//emitpcode(POC_INFW, popGet(AOP(IC_RIGHT(ic)),offset));

						if (resultX)
						{
							if (!offset)
								emitpcode(POC_ADDWF, PCOP_POINC0);
							else
								emitpcode(POC_ADCWF, PCOP_POINC0);
						}
						else
						{
							if (!offset)
								emitpcode(POC_ADDWF, popGetAddr(AOP(IC_LEFT(ic)), offset, 0));
							else
								emitpcode(POC_ADCWF, popGetAddr(AOP(IC_LEFT(ic)), offset, 0));
						}
						offset++;
					}
				}
				else
				{
					while (size--)
					{
						if(offset>=AOP_SIZE(IC_RIGHT(ic)))
							emitpcode(POC_MVL, popGetLit(0));
						else 
							emitpcode(POC_MVFW, popGet(AOP(IC_RIGHT(ic)), offset));
						//emitSKPNC;
						//emitpcode(POC_INFW, popGet(AOP(IC_LEFT(ic)),offset));
						if (resultX)
							emitpcode(POC_ADCWF, PCOP_POINC0);
						else
							emitpcode(POC_ADCWF, popGet(AOP(IC_RESULT(ic)), offset));
						offset++;
					}
				}
			}
			else
			{
				HYA_OPCODE poc = POC_MVFW;
				if (op_isLitLike(IC_LEFT(ic)))
					poc = POC_MVL;
				while (size--)
				{
					//              if (!HY08A_sameRegs(AOP(IC_LEFT(ic)), AOP(IC_RESULT(ic))) ) {
					//                  emitpcode(poc, popGetAddr(AOP(IC_LEFT(ic)),offset,0));
					//if(resultX)
					//	emitpcode(POC_MVWF, PCOP_POINC0);
					//else
					//	emitpcode(POC_MVWF, popGet(AOP(IC_RESULT(ic)),offset));
					//              }

					emitpcode(poc, popGetAddr(AOP(IC_LEFT(ic)), offset, 0));
					if(offset>=AOP_SIZE(IC_RIGHT(ic)))
					{
						emitSKPNC;
						emitpcode(POC_ADDLW, popGetLit(1));
					}else 
					{
						emitpcode(POC_ADCFW, popGet(AOP(IC_RIGHT(ic)), offset));
					}
					//emitSKPNC;
					//emitpcode(POC_INFW, popGet(AOP(IC_RIGHT(ic)),offset));
					if (resultX)
						emitpcode(POC_MVWF, PCOP_POINC0);
					else
						emitpcode(POC_MVWF, popGet(AOP(IC_RESULT(ic)), offset));
					offset++;
				}
			}
		}
	}
	// following is complex
	// it is possible that bytes different ? without CAST to do it?
	// 4 = 2+2
	// 4 = 2+1
	// 4 = 1+2
	// 2 = 1+4
	if (AOP_SIZE(IC_RESULT(ic)) > sizelr)
	{
		int sign = !(SPEC_USIGN(getSpec(operandType(IC_LEFT(ic)))) |
					 SPEC_USIGN(getSpec(operandType(IC_RIGHT(ic)))));

		/* Need to extend result to higher bytes */
		size = AOP_SIZE(IC_RESULT(ic)) - AOP_SIZE(IC_RIGHT(ic)) - 1;

		/* First grab the carry from the lower bytes */
		if (!sign)
		{
			if (AOP_SIZE(IC_LEFT(ic)) > AOP_SIZE(IC_RIGHT(ic)))
			{
				int leftsize = AOP_SIZE(IC_LEFT(ic)) - AOP_SIZE(IC_RIGHT(ic));
				HYA_OPCODE poc = POC_MVFW;
				if (op_isLitLike(IC_LEFT(ic)))
					poc = POC_MVL;
				while (leftsize-- > 0)
				{ // leftsize is lefts-rightsize
					emitpcode(poc, popGetAddr(AOP(IC_LEFT(ic)), offset, 0));
					emitSKPNC;
					emitpcode(POC_ADDLW, popGetLit(0x01));
					if (resultX)
						emitpcode(POC_MVWF, PCOP_POINC0); // result has enough bytes
					else
						emitpcode(POC_MVWF, popGet(AOP(IC_RESULT(ic)), offset));
					//emitSKPNC;
					//emitpcode(POC_INF, popGet(AOP(IC_RESULT(ic)),offset)); /* INCF does not update Carry! */
					offset++;
					if (size)
						size--;
					else
						break;
				}
			}
			else
			{ // left <= right
				if (resultX)
				{
					emitpcode(POC_CLRF, PCOP_INDF0);
					emitpcode(POC_RLCF, PCOP_INDF0);
				}
				else
				{
					emitpcode(POC_CLRF, popGet(AOP(IC_RESULT(ic)), offset));
					emitpcode(POC_RLCF, popGet(AOP(IC_RESULT(ic)), offset));
				}

			}
		}
		if (sign && offset > 0 && offset < AOP_SIZE(IC_RESULT(ic)))
		{
			/* Now this is really horrid. Gotta check the sign of the addends and propogate
                    * to the result */
			if (resultX)
				emitpcode(POC_CLRF, PCOP_INDF0);
			else
				emitpcode(POC_CLRF, popGet(AOP(IC_RESULT(ic)), offset));
			emitpcode(POC_BTSZ, newpCodeOpBit(aopGet(AOP(IC_RESULT(ic)), offset - 1, FALSE, FALSE), 7, 0));
			if (resultX)
				emitpcode(POC_DECF, PCOP_INDF0);
			else
				emitpcode(POC_DECF, popGet(AOP(IC_RESULT(ic)), offset));
			/*emitpcode(POC_BTSZ, newpCodeOpBit(aopGet(AOP(IC_RESULT(ic)), offset - 1, FALSE, FALSE), 7, 0));
			if (resultX)
				emitpcode(POC_DECF, PCOP_INDF0);
			else
				emitpcode(POC_DECF, popGet(AOP(IC_RESULT(ic)), offset));*/

			/* if chars or ints or being signed extended to longs: */
			if (size)
			{
				emitpcode(POC_MVL, popGetLit(0));
				emitpcode(POC_BTSZ, newpCodeOpBit(aopGet(AOP(IC_RESULT(ic)), offset, FALSE, FALSE), 7, 0));
				emitpcode(POC_MVL, popGetLit(0xff));
			}
		}

		offset++;
		while (size--)
		{
			if (resultX)
			{
				if (sign)
					emitpcode(POC_MVWF, PCOP_PRINC0);
				else
					emitpcode(POC_CLRF, PCOP_PRINC0);
			}
			else
			{
				if (sign)
					emitpcode(POC_MVWF, popGet(AOP(IC_RESULT(ic)), offset));
				else
					emitpcode(POC_CLRF, popGet(AOP(IC_RESULT(ic)), offset));
			}
			offset++;
		}
	}

	//adjustArithmeticResult(ic);

release:
	freeAsmop(IC_LEFT(ic), NULL, ic, (RESULTONSTACK(ic) ? FALSE : TRUE));
	freeAsmop(IC_RIGHT(ic), NULL, ic, (RESULTONSTACK(ic) ? FALSE : TRUE));
	freeAsmop(IC_RESULT(ic), NULL, ic, TRUE);
}

/*-----------------------------------------------------------------*/
/* addSign - propogate sign bit to higher bytes                    */
/*-----------------------------------------------------------------*/
void addSign(operand *result, int offset, int sign)
{
	assert(result);
	int size = (HY08A_getDataSize(result) - offset);
	int resultX = IS_TRUE_SYMOP(result) && IN_XSPACE(SPEC_OCLS(OP_SYMBOL(result)->etype));
	DEBUGHY08A_emitcode("; ***", "%s  %d", __FUNCTION__, __LINE__);
	FENTRY;

	if (size > 0)
	{
		if (sign && offset)
		{

			if (size == 1)
			{
				emitpcode(POC_CLRF, popGet(AOP(result), offset));
				emitpcode(POC_BTSZ, newpCodeOpBit(aopGet(AOP(result), offset - 1, FALSE, FALSE), 7, 0));
				emitpcode(POC_SETF, popGet(AOP(result), offset));
				//emitpcode(POC_RLFW, popGet(AOP(result),offset - 1));
				//emitpcode(POC_ANDLW, newpCodeOpLit(1));
				//emitpcode(POC_SUBL, newpCodeOpLit(0));
				//emitpcode(POC_MVWF,  popGet(AOP(result), offset));
			}
			else
			{

				emitpcode(POC_MVL, popGetLit(0));
				emitpcode(POC_BTSZ, newpCodeOpBit(aopGet(AOP(result), offset - 1, FALSE, FALSE), 7, 0));
				emitpcode(POC_MVL, popGetLit(0xff));
				//emitpcode(POC_RLFW,popGet(AOP(result),offset - 1));
				//emitpcode(POC_ANDLW, newpCodeOpLit(1));
				//emitpcode(POC_SUBL, newpCodeOpLit(0));
				while (size--)
				{
					if (resultX)
					{
						emitpcode22(POC_MVFF, PCOP_W, popGet2(AOP(result), offset + size, 1));
					}
					else
						emitpcode(POC_MVWF, popGet(AOP(result), offset + size));
				}
			}
		}
		else if (resultX)
		{
			emitldpr(popGet(AOP(result), offset));
			while (size--)
			{
				emitpcode(POC_CLRF, PCOP_POINC0);
			}
		}
		else
		{
			while (size--)
			{
				emitpcode(POC_CLRF, popGet(AOP(result), offset++));
			}
		}
	}
}

/*-----------------------------------------------------------------*/
/* genMinus - generates code for subtraction                       */
/*-----------------------------------------------------------------*/
extern void genXor(iCode *ic, iCode *ifx);
void genMinus(iCode *ic)
{
	int size, offset = 0, same = 0;
	int sizel, sizer, resultSize; // operand and result size may differ!!,
	unsigned long long lit = 0ll;
	int isLit;
	symbol *lbl_comm, *lbl_next;
	asmop *left, *right, *result;
	int resultX = 0;

	FENTRY;
	DEBUGHY08A_emitcode("; ***", "%s  %d", __FUNCTION__, __LINE__);
	aopOp(IC_LEFT(ic), ic, FALSE);
	aopOp(IC_RIGHT(ic), ic, FALSE);
	aopOp(IC_RESULT(ic), ic, TRUE);

	if (AOP_TYPE(IC_RESULT(ic)) == AOP_CRY &&
		AOP_TYPE(IC_RIGHT(ic)) == AOP_LIT)
	{
		operand *t = IC_RIGHT(ic);
		IC_RIGHT(ic) = IC_LEFT(ic);
		IC_LEFT(ic) = t;
	}
	resultX = IS_TRUE_SYMOP(IC_RESULT(ic)) && IN_XSPACE(SPEC_OCLS(OP_SYMBOL(IC_RESULT(ic))->etype));

	DEBUGHY08A_emitcode("; ", "result %s, left %s, right %s",
						AopType(AOP_TYPE(IC_RESULT(ic))),
						AopType(AOP_TYPE(IC_LEFT(ic))),
						AopType(AOP_TYPE(IC_RIGHT(ic))));

	left = AOP(IC_LEFT(ic));
	right = AOP(IC_RIGHT(ic));
	result = AOP(IC_RESULT(ic));

	sizel = HY08A_getDataSize(IC_LEFT(ic));
	//sizer = HY08A_getDataSize(IC_RIGHT(ic));
	sizer=sizel;

	resultSize = HY08A_getDataSize(IC_RESULT(ic));

	// if (sizel != sizer)
	// {
	// 	fprintf(stderr, "internal Error at genMinus(), %s:%d\n", __FILE__, __LINE__);
	// 	exit(-__LINE__);
	// }
	size = sizer;
	if (resultSize < size)
		size = resultSize;

	same = HY08A_sameRegs(right, result);

	//if((AOP_TYPE(IC_LEFT(ic)) != AOP_LIT)
	//    && (HY08A_getDataSize(IC_LEFT(ic)) < size))
	//{
	//        fprintf(stderr, "%s:%d(%s):WARNING: left operand too short for result\n",
	//                ic->filename, ic->lineno, __FUNCTION__);
	//} // if
	//if((AOP_TYPE(IC_RIGHT(ic)) != AOP_LIT)
	//    && (HY08A_getDataSize(IC_RIGHT(ic)) < size))
	//{
	//        fprintf(stderr, "%s:%d(%s):WARNING: right operand too short for result\n",
	//                ic->filename, ic->lineno, __FUNCTION__ );
	//} // if

	if (AOP_TYPE(IC_RIGHT(ic)) == AOP_LIT)
	{
		/* Add a literal to something else */

		lit = ullFromVal(right->aopu.aop_lit);
		lit = -(long long)lit;

		genAddLit(ic, lit);
	}
	else if (AOP_TYPE(IC_RIGHT(ic)) == AOP_CRY)
	{
		// bit subtraction

		HY08A_emitcode(";bitsub", "right is bit: %s", aopGet(right, 0, FALSE, FALSE));
		HY08A_emitcode(";bitsub", "left is bit: %s", aopGet(left, 0, FALSE, FALSE));
		HY08A_emitcode(";bitsub", "result is bit: %s", aopGet(result, 0, FALSE, FALSE));

		/* here we are subtracting a bit from a char or int */
		if (size == 1)
		{
			if (HY08A_sameRegs(left, result))
			{ // left result will not have X at the same time.
				// left and result is same register, right is bit
				// it is possible left and result is also bit
				if (AOP_TYPE(IC_LEFT(ic)) == AOP_CRY)
				{
					// here b0 = b0-b1
					emitpcode(POC_BTSZ, popGet(right, 0));
					emitpcode(POC_BTGF, popGet(result, 0));
				}
				else
				{

					emitpcode(POC_BTSZ, popGet(right, 0));
					emitpcode(POC_DECF, popGet(result, 0));
				}
			}
			else
			{ // here size = 1 and left is not result

				if ((AOP_TYPE(IC_LEFT(ic)) == AOP_IMMD) ||
					(AOP_TYPE(IC_LEFT(ic)) == AOP_LIT))
				{
					/*
					 * result = literal - bit
					 *
					 * XXX: probably fails for AOP_IMMDs!
					 */

					lit = ullFromVal(left->aopu.aop_lit);

					if (AOP_TYPE(IC_RESULT(ic)) == AOP_CRY)
					{
						if (HY08A_sameRegs(right, result))
						{
							if (lit != 0)
							{
								// true - 1 = 0
								// false - 1 = 1
								emitpcode(POC_BTGF, popGet(result, 0));
							}
						}
						else
						{
							emitpcode(POC_BCF, popGet(result, 0));
							if (lit & 1)
								emitpcode(POC_BTSS, popGet(right, 0));
							else
								emitpcode(POC_BTSZ, popGet(right, 0));
							emitpcode(POC_BSF, popGet(result, 0));
						}
						goto release;
					}
					else
					{
						emitpcode(POC_MVL, popGetLit(lit & 0xff));
						emitpcode(POC_BTSZ, popGet(right, 0));
						emitpcode(POC_MVL, popGetLit((lit - 1) & 0xff));
						emitpcode(POC_MVWF, popGet(result, 0));
					}
				}
				else
				{ // left is not lit
					// result = register - bit
					// XXX: Fails for lit-like left operands
					// several cases, result is bit/not bit, left is bit / not bit, but right is bit

					// a little complex

					if (result->type == AOP_CRY) // result is bit
					{
						// depends on left
						if (left->type == AOP_CRY)
						{
							// all are bits
							genXor(ic, NULL); // because size=1, there is no ifx!!
							goto release;
						}
						
						//							int ii;
						// I am not sure this case will be casted
						//assert(!"Internal err: Number-bool not implemented!!\n");
						emitpcode(POC_MVFW, popGet(left, 0));
						emitpcode(POC_BTSZ, popGet(right, 0));
						emitpcode(POC_DECFW, popGet(left, 0)); // for size = 1
						emitpcode(POC_BCF, popGet(result, 0));
						emitSKPZ;
						emitpcode(POC_BSF, popGet(result, 0));
						goto release;
						
					}
					// come here result is not bit, right is bit, it is possible left is bit

					if(left->type == AOP_CRY) // 
					{
						genXor(ic, NULL); // because size=1, there is no ifx!!
						goto release;
					}
					emitpcode(POC_MVFW, popGet(left, 0));
					emitpcode(POC_BTSZ, popGet(right, 0));
					emitpcode(POC_DECFW, popGet(left, 0));
					emitpcode(POC_MVWF, popGet(result, 0));
				}
			}
		}
		else
		{
			//fprintf(stderr, "WARNING: subtracting bit from multi-byte operands is incomplete.\n");
			// here right is bit, result is not bit , left .. may be bit
			int i;
			if (AOP_TYPE(IC_LEFT(ic)) == AOP_CRY)
			{
				genXor(ic, NULL);
				goto release;
			}
			if (resultX)
			{
				char buf[1024];
				snprintf(buf, 1023, "%s:%d xdata=bit-val not support, please modify it!!\n", ic->filename, ic->lineno);
				assert(0);
			}
			// here left is not bit, result is not bit, right is bit
			emitpcode(POC_MVFW, popGet(left, 0));
			emitpcode(POC_BTSZ, popGet(right, 0));
			emitpcode(POC_DECFW, popGet(left, 0)); // for size = 1
			emitpcode(POC_MVWF, popGet(result, 0));
			for (i = 1; i < result->size; i++)
			{
				if (i >= left->size)
					emitpcode(POC_CLRF, popGet(result, i));
				else
					emitpcode22(POC_MVFF, popGet(left, i), popGet2(result, i, 1));
				emitpcode(POC_INSUZW, popGet(result, i - 1));
				emitpcode(POC_DECF, popGet(result, i));
			}
			//exit(EXIT_FAILURE);
			goto release;
		} // if
	}
	else if (AOP_TYPE(IC_LEFT(ic)) == AOP_CRY)
	{
		int ii;
		// left is bit right is not bit
		// result = bit - right = -right + bit ==> it is 2' comp +1 or +2
		left = AOP(IC_LEFT(ic));
		right = AOP(IC_RIGHT(ic));
		result = AOP(IC_RESULT(ic));
		if (result->type == AOP_CRY) // if result is bit
		{

			// only if left==right, result will be 0
			symbol *lblnz = newiTempLabel(NULL);
			symbol *lblfin = newiTempLabel(NULL);
			emitpcode(POC_MVL, popGetLit(1));
			emitpcode(POC_BTSS, popGet(left, 0)); // left bit
			emitpcode(POC_MVL, popGetLit(0));
			emitpcode(POC_XORFW, popGet(right, 0));
			emitSKPZ;
			emitpcode(POC_JMP, popGetLabel(lblnz->key));
			for (ii = 1; ii < right->size; ii++)
			{
				emitpcode(POC_RRFW, popGet(right, ii));
				emitSKPZ;
				emitpcode(POC_JMP, popGetLabel(lblnz->key));
			}
			emitpcode(POC_BCF, popGet(result, 0)); // it is 0 now;
			emitpcode(POC_JMP, popGetLabel(lblfin->key));
			emitpLabel(lblnz->key);
			emitpcode(POC_BSF, popGet(result, 0));
			emitpLabel(lblfin->key);
			goto release;
		}
		// here result is not bit, only left is bit
		// we inverse then add 1 or 2
		for (ii = 0; ii < right->size; ii++)
		{
			if (ii >= result->size)
				break;
			emitpcode(POC_COMFW, popGet(right, ii));
			emitpcode(POC_MVWF, popGet(result, ii));
		}
		// sign ext at last is ok
		emitpcode(POC_MVL, popGetLit(1));
		emitpcode(POC_BTSZ, popGet(left, 0));
		emitpcode(POC_MVL, popGetLit(2));
		for (ii = 0; ii < right->size; ii++)
		{
			if (ii >= result->size)
				break;
			if (ii == 0)
				emitpcode(POC_ADDWF, popGet(result, ii));
			else
			{
				emitpcode(POC_MVL, popGetLit(0));
				emitpcode(POC_ADCWF, popGet(result, ii));
			}
		}
		if (right->size < result->size)
		{
			emitpcode(POC_MVL, popGetLit(0));
			emitpcode(POC_BTSS, newpCodeOpBit(aopGet(result, left->size - 1, FALSE, FALSE), 7, 0));
			emitpcode(POC_MVL, popGetLit(0xff));
			while (ii < result->size)
			{
				emitpcode(POC_MVWF, popGet(result, ii));
				ii++;
			}
		}
		goto release;
	}
	else
	{ // ( here AOP RIGHT != CRY), which is general case
		/*
		 * RIGHT is not a literal and not a bit operand,
		 * LEFT is unknown (register, literal, bit, ...)
		 */
		lit = 0;
		isLit = 0;

		if (AOP_TYPE(IC_LEFT(ic)) == AOP_LIT)
		{
			lit = ullFromVal(left->aopu.aop_lit);
			isLit = 1;

			DEBUGHY08A_emitcode("; left is lit", "line %d result %s, left %s, right %s", __LINE__,
								AopType(AOP_TYPE(IC_RESULT(ic))),
								AopType(AOP_TYPE(IC_LEFT(ic))),
								AopType(AOP_TYPE(IC_RIGHT(ic))));
		} // if left == lit over

		if (isLit && size > 1 && !resultX) // only this worth ... if not same, copy is not a good idea
		{
			unsigned long long lits1 = lit + 1;
			int ii;
			int litnow = -1;
			int prevLitZero = 0;
			// multi-byte consideration:
			// pic18 has SUBL/ADDL but not SBCL/ADCL, .. it is annoying
			// however, since COMF will not effect C, we can still save the first operation

			for (ii = 0; ii < size; ii = ii + 1)
			{
				if (!same)
				{
					emitpcode(POC_COMFW, popGet(AOP(IC_RIGHT(ic)), ii));
					if (ii == 0)
					{
						if ((lits1 & 0xff) == 0)
						{
							prevLitZero = 1;
						}
						else
						{
							emitpcode(POC_ADDLW, popGetLit(lits1 & 0xff));
						}
					}
					emitpcode(POC_MVWF, popGet(AOP(IC_RESULT(ic)), ii));
				}
				else
				{
					emitpcode(POC_COMF, popGet(AOP(IC_RIGHT(ic)), ii));
				}
			}
			for (ii = 0; ii < size; ii++)
			{

				// ok we seperate
				if (same)
				{
					if (ii == 0)
					{
						if ((lits1 & 0xff) == 0)
							prevLitZero = 1;
						else
						{
							litnow = lits1 & 0xff;
							emitpcode(POC_MVL, popGetLit(lits1 & 0xff));
							emitpcode(POC_ADDWF, popGet(AOP(IC_RESULT(ic)), ii));
						}
					}
					else
					{
						if (prevLitZero)
						{
							if ((lits1 & 0xff) == 0)
								prevLitZero = 1;
							else
							{
								if (litnow != (lits1 & 0xff))
								{
									litnow = lits1 & 0xff;
									emitpcode(POC_MVL, popGetLit(lits1 & 0xff));
								}
								emitpcode(POC_ADDWF, popGet(AOP(IC_RESULT(ic)), ii));
								prevLitZero = 0;
							}
						}
						else
						{
							if (litnow != (lits1 & 0xff))
							{
								litnow = lits1 & 0xff;
								emitpcode(POC_MVL, popGetLit(lits1 & 0xff));
							}
							emitpcode(POC_ADCWF, popGet(AOP(IC_RESULT(ic)), ii));
							prevLitZero = 0;
						}
					}
				}
				else
				{
					if (ii != 0)
					{
						if (prevLitZero)
						{
							if ((lits1 & 0xff) == 0)
								prevLitZero = 1;
							else
							{
								if (litnow != (lits1 & 0xff))
								{
									litnow = lits1 & 0xff;
									emitpcode(POC_MVL, popGetLit(lits1 & 0xff));
								}
								emitpcode(POC_ADDWF, popGet(AOP(IC_RESULT(ic)), ii));
								prevLitZero = 0;
							}
						}
						else
						{
							if (litnow != (lits1 & 0xff))
							{
								litnow = lits1 & 0xff;
								emitpcode(POC_MVL, popGetLit(lits1 & 0xff));
							}
							emitpcode(POC_ADCWF, popGet(AOP(IC_RESULT(ic)), ii));
							prevLitZero = 0;
						}
					}
				}
				// if (((ii != 0) || same)) // for same operation, save to next [COM not effect C]
				// {

				// 	if (litnow != (lits1 & 0xff))
				// 	{

				// 		litnow = lits1 & 0xff;
				// 		emitpcode(POC_MVL, popGetLit(lits1 & 0xff));
				// 	}
				// 	if (ii == 0)
				// 		emitpcode(POC_ADDWF, popGet(AOP(IC_RESULT(ic)), ii));
				// 	else
				// 		emitpcode(POC_ADCWF, popGet(AOP(IC_RESULT(ic)), ii));
				// }
				lits1 >>= 8;
			}
			// 2020, add exten
			if (size < resultSize)
			{
				// depends if literal is signed or unsigned
				// left
				addSign(IC_RESULT(ic), size, !SPEC_USIGN(OP_VALUE(IC_LEFT(ic))->etype));
			}
		}
		else
		{

			/*
			 * First byte, no carry-in.
			 * Any optimizations that are longer than 2 instructions are
			 * useless.
			 */

			if (same && isLit && ((lit & 0xff) == 0xff))
			{
				// right === res = 0xFF - right = ~right
				emitpcode(POC_COMF, popGet(right, 0));
				if (size > 1)
				{
					// setup CARRY/#BORROW
					emitSETC;
				} // if
			}
			else if ((size == 1) && isLit && ((lit & 0xff) == 0xff))
			{
				// res = 0xFF - right = ~right
				emitpcode(POC_COMFW, popGet(right, 0));
				if (resultX)
				{ // size =1 uses MVFF
					emitpcode22(POC_MVFF, PCOP_W, popGet2(result, 0, 1));
				}
				else
					emitpcode(POC_MVWF, popGet(result, 0));
				// CARRY/#BORROW is not setup correctly
			}
			else if ((size == 1) && same && isLit && ((lit & 0xff) == 0))
			{
				// right === res = 0 - right = ~right + 1
				emitpcode(POC_COMF, popGet(right, 0));
				emitpcode(POC_INF, popGet(right, 0));
				// CARRY/#BORROW is not setup correctly
			}
			else
			{
				// there is a special case that result is bit and left is bit
				// use Xor

				// general case, should always work

				mov2w(right, 0);
				if (HY08A_sameRegs(left, result))
				{
					// result === left = left - right (in place)
					emitpcode(POC_SUBFWF, popGet(result, 0));
				}
				else
				{
					// works always: result = left - right
					emitpcode(op_isLitLike(IC_LEFT(ic))
								  ? POC_SUBL
								  : POC_SUBFWW,
							  popGetAddr(left, 0, 0));
					if (resultX)
					{
						if (size > 1)
						{
							emitldpr(popGet(result, 0));
							emitpcode(POC_MVWF, PCOP_POINC0);
						}
						else
						{
							emitpcode22(POC_MVFF, PCOP_W, popGet2(result, 0, 1));
						}
					}
					else
						emitpcode(POC_MVWF, popGet(result, 0));
				} // if
			}	  // if

			/*
			 * Now for the remaining bytes with carry-in (and carry-out).
			 */
			offset = 0;
			while (--size)
			{
				lit >>= 8;
				offset++; // here offset from 1

				/*
				 * The following code generation templates are ordered
				 * according to increasing length; the first template
				 * that matches will thus be the shortest and produce
				 * the best code possible with thees templates.
				 */

				if (HY08A_sameRegs(left, right))
				{
					/*
					 * This case should not occur; however, the
					 * template works if LEFT, RIGHT, and RESULT are
					 * register operands and LEFT and RIGHT are the
					 * same register(s) and at least as long as the
					 * result.
					 *
					 *   CLRF   result
					 *
					 * 1 cycle
					 */

					if (resultX)
						emitpcode(POC_CLRF, PCOP_POINC0);
					else
						emitpcode(POC_CLRF, popGet(result, offset));
				}
				else if (HY08A_sameRegs(left, result))
				{
					/*
					 * This template works if LEFT, RIGHT, and
					 * RESULT are register operands and LEFT and
					 * RESULT are the same register(s).
					 *
					 *   MOVF   right, W    ; W := right
					 *   BTFSS  STATUS, C
					 *   INCFSZ right, W    ; W := right + BORROW
					 *   SUBWF  result, F   ; res === left := left - (right + BORROW)
					 *
					 * The SUBWF *must* be skipped if we have a
					 * BORROW bit and right == 0xFF in order to
					 * keep the BORROW bit intact!
					 *
					 * 4 cycles
					 */
					if (offset < HY08A_getDataSize(IC_RIGHT(ic)))
					{
						mov2w(right, offset);
						//emitSKPC;
						//emitpcode(POC_INFW, popGet(right, offset));
						emitpcode(POC_SBCFWF, popGet(result, offset));
					}
				}
				else if ((size == 1) && isLit && ((lit & 0xff) == 0xff))
				{
					/*
					 * This template works for the last byte (MSB) of
					 * the subtraction and ignores correct propagation
					 * of the outgoing BORROW bit. RIGHT and RESULT
					 * must be register operands, LEFT must be the
					 * literal 0xFF.
					 *
					 * (The case LEFT === RESULT is optimized above.)
					 *
					 * 0xFF - right - BORROW = ~right - BORROW
					 *
					 *   COMF   right, W    ; W := 0xff - right
					 *   BTFSS  STATUS, C
					 *   ADDLW  0xFF        ; W := 0xff - right - BORROW
					 *   MOVWF  result
					 *
					 * 4 cycles
					 */
					emitpcode(POC_COMFW, popGet(right, offset));
					emitSKPC;
					emitpcode(POC_ADDLW, popGetLit(0xff));
					if (resultX)
						emitpcode(POC_MVWF, PCOP_POINC0);
					else
						emitpcode(POC_MVWF, popGet(result, offset));
				}
				else if (size == 1)
				{
					/*
					 * This template works for the last byte (MSB) of
					 * the subtraction and ignores correct propagation
					 * of the outgoing BORROW bit. RIGHT and RESULT
					 * must be register operands, LEFT can be a
					 * register or a literal operand.
					 *
					 * (The case LEFT === RESULT is optimized above.)
					 *
					 *   MOVF   right, W    ; W := right
					 *   BTFSS  STATUS, C
					 *   INCF   right, W    ; W := right + BORROW
					 *   SUBxW  left, W     ; W := left - right - BORROW
					 *   MOVWF  result
					 *
					 * 5 cycles
					 */
					if (op_isLitLike(IC_LEFT(ic)))
					{
						mov2w(right, offset);
						emitSKPC;
						emitpcode(POC_INFW, popGet(right, offset));
						emitpcode(POC_SUBL,
								  popGetAddr(left, offset, 0));
						if (resultX)
							emitpcode(POC_MVWF, PCOP_INDF0);
						else
							emitpcode(POC_MVWF, popGet(result, offset));
					}
					else
					{
						mov2w(right, offset);
						emitpcode(POC_SBCFWW, popGet(left, offset));
						if (resultX)
							emitpcode(POC_MVWF, PCOP_INDF0);
						else
							emitpcode(POC_MVWF, popGet(result, offset));
					}
				}
				else if (IS_ITEMP(IC_RESULT(ic)) && !HY08A_sameRegByte(right, result,offset)) // 2023, different sym may use same reg?
				{
					/*
					 * This code template works if RIGHT and RESULT
					 * are different register operands and RESULT
					 * is not volatile/an SFR (written twice).
					 *
					 * #########################################
					 * Since we cannot determine that for sure,
					 * we approximate via IS_ITEMP() for now.
					 * #########################################
					 *
					 *   MOVxW  left, W     ; copy left to result
					 *   MOVWF  result
					 *   MOVF   right, W    ; W := right
					 *   BTFSS  STATUS, C
					 *   INCFSZ right, W    ; W := right + BORROW
					 *   SUBWF  result, F   ; res === left := left - (right + BORROW)
					 *
					 * 6 cycles, but fails for SFR RESULT operands
					 */
					// left to peep to optimize
					mov2w(left, offset);
					emitpcode(POC_MVWF, popGet(result, offset));
					mov2w(right, offset);
					//emitSKPC;
					//emitpcode(POC_INSZW, popGet(right, offset));
					// same reg will not have X
					emitpcode(POC_SBCFWF, popGet(result, offset));
				}
				else
				{

					// SUBF is F - W -> F or W, that is move right to W, SUF left, save to result
					// we have no SUBC literal that is a problem

					if (op_isLitLike(IC_LEFT(ic)))
					{
						lbl_comm = newiTempLabel(NULL);
						lbl_next = newiTempLabel(NULL);
						mov2w(right, offset);
						emitSKPC;
						emitpcode(POC_INSZW, popGet(right, offset));
						emitpcode(POC_JMP, popGetLabel(lbl_comm->key));
						mov2w(left, offset);
						emitpcode(POC_JMP, popGetLabel(lbl_next->key));
						emitpLabel(lbl_comm->key);
						emitpcode(POC_SUBL,
								  popGetAddr(left, offset, 0));
						emitpLabel(lbl_next->key);
						if (resultX)
							emitpcode(POC_MVWF, PCOP_POINC0);
						else
							emitpcode(POC_MVWF, popGet(result, offset));
					}
					else
					{
						mov2w(right, offset);
						emitpcode(POC_SBCFWW,
								  popGetAddr(left, offset, 0));
						if (resultX)
							emitpcode(POC_MVWF, PCOP_POINC0);
						else
							emitpcode(POC_MVWF, popGet(result, offset));
					}
				} // if
			}	  // while

			// after sub, we need extend sign if
			if (sizel < resultSize)
			{
				int sign = !(SPEC_USIGN(getSpec(operandType(IC_LEFT(ic)))) |
							 SPEC_USIGN(getSpec(operandType(IC_RIGHT(ic)))));

				emitpcode(POC_MVL, popGetLit(0));
				if (sign)
				{
					emitpcode(POC_BTSZ, newpCodeOpBit(aopGet(AOP(IC_RESULT(ic)), offset, FALSE, FALSE), 7, 0));
					emitpcode(POC_MVL, popGetLit(0xff));
				}
				offset++;
				while (offset < resultSize)
				{
					if (resultX)
						emitpcode(POC_MVWF, PCOP_POINC0);
					else
						emitpcode(POC_MVWF, popGet(result, offset));
					offset++;
				}
			}

		} // if
	}

	if (AOP_TYPE(IC_RESULT(ic)) == AOP_CRY)
	{
		fprintf(stderr, "WARNING: AOP_CRY (bit-) results are probably broken. Please report this with source code as a bug.\n");
		mov2w(result, 0); // load Z flag
		emitpcode(POC_BCF, popGet(result, 0));
		emitSKPZ;
		emitpcode(POC_BSF, popGet(result, 0));
	} // if

	//    adjustArithmeticResult(ic);

release:
	freeAsmop(IC_LEFT(ic), NULL, ic, (RESULTONSTACK(ic) ? FALSE : TRUE));
	freeAsmop(IC_RIGHT(ic), NULL, ic, (RESULTONSTACK(ic) ? FALSE : TRUE));
	freeAsmop(IC_RESULT(ic), NULL, ic, TRUE);
}
