#ifndef MAIN_INCLUDE
#define MAIN_INCLUDE

typedef struct {
    unsigned int isLibrarySource:1;
//    int lklocN ;
    //int disable_df;
    //int no_ext_instr;
    //int no_warn_non_free;
    int ob_count;
    int dis_fsr_opt;
	int inline_shift_long;
	int force_h08a;
	int dumpPeepInfo;
    int keepLoopInduction;
    int keepGCSE;
    int extraStkSize;
	int keepLocal;
} HY08A_options_t;

extern HY08A_options_t HY08A_options;
extern int debug_verbose;
extern int max_rom_addr;

#endif

