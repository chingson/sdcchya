/*-------------------------------------------------------------------------

   pcode.h - post code generation
   Written By -  Scott Dattalo scott@dattalo.com

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 2, or (at your option) any
   later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

-------------------------------------------------------------------------*/

#ifndef __PCODE_H__
#define __PCODE_H__

#include "common.h"
#include<stdint.h>

/* When changing these, you must also update the assembler template
 * in device/lib/libsdcc/macros.inc */
#define GPTRTAG_DATA	0x00
#define GPTRTAG_CODE	0x80

/* Cyclic dependency with ralloc.h: */
struct reg_info;

/*
   Post code generation

   The post code generation is an assembler optimizer. The assembly code
   produced by all of the previous steps is fully functional. This step
   will attempt to analyze the flow of the assembly code and agressively
   optimize it. The peep hole optimizer attempts to do the same thing.
   As you may recall, the peep hole optimizer replaces blocks of assembly
   with more optimal blocks (e.g. removing redundant register loads).
   However, the peep hole optimizer has to be somewhat conservative since
   an assembly program has implicit state information that's unavailable
   when only a few instructions are examined.
     Consider this example:

   example1:
     movwf  t1
     movf   t1,w

   The movf seems redundant since we know that the W register already
   contains the same value of t1. So a peep hole optimizer is tempted to
   remove the "movf". However, this is dangerous since the movf affects
   the flags in the status register (specifically the Z flag) and subsequent
   code may depend upon this. Look at these two examples:

   example2:
     movwf  t1
     movf   t1,w     ; Can't remove this movf
     skpz
     return

   example3:
     movwf  t1
     movf   t1,w     ; This  movf can be removed
     xorwf  t2,w     ; since xorwf will over write Z
     skpz
     return

*/


/***********************************************************************
 * debug stuff
 *
 * The DFPRINTF macro will call fprintf if PCODE_DEBUG is defined.
 * The macro is used like:
 *
 * DPRINTF(("%s #%d\n","test", 1));
 *
 * The double parenthesis (()) are necessary
 *
 ***********************************************************************/
//#define PCODE_DEBUG

#ifdef PCODE_DEBUG
#define DFPRINTF(args) (fprintf args)
#else
#define DFPRINTF(args) ((void)0)
#endif


/***********************************************************************
 *  HYA status bits - this will move into device dependent headers
 ***********************************************************************/
#define HYA_C_BIT    4
#define HYA_DC_BIT   3
#define HYA_Z_BIT    0
#define HYA_OV_BIT	1
#define HYA_N_BIT	2

#define HYA_RP0_BIT  (-1)   /* Register Bank select bits RP1:0 : */
#define HYA_RP1_BIT  (-2)   /* 00 - bank 0, 01 - bank 1, 10 - bank 2, 11 - bank 3 */
#define HYA_IRP_BIT  (-3)   /* Indirect register page select */

/***********************************************************************
 *  HYA INTCON bits - this will move into device dependent headers
 ***********************************************************************/
#define HYA_RBIF_BIT 0   /* Port B level has changed flag */
#define HYA_INTF_BIT 1   /* Port B bit 0 interrupt on edge flag */
#define HYA_T0IF_BIT 2   /* TMR0 has overflowed flag */
#define HYA_RBIE_BIT 3   /* Port B level has changed - Interrupt Enable */
#define HYA_INTE_BIT 4   /* Port B bit 0 interrupt on edge - Int Enable */
#define HYA_T0IE_BIT 5   /* TMR0 overflow Interrupt Enable */
#define HYA_PIE_BIT  6   /* Peripheral Interrupt Enable */
#define HYA_GIE_BIT  7   /* Global Interrupt Enable */


/***********************************************************************
 *
 *  HYA_OPTYPE - Operand types that are specific to the architecture
 *
 *  If a assembly instruction has an operand then here is where we
 *  associate a type to it. For example,
 *
 *     movf    reg,W
 *
 *  The movf has two operands: 'reg' and the W register. 'reg' is some
 *  arbitrary general purpose register, hence it has the type PO_GPR_REGISTER.
 *  The W register, which is the  accumulator, has the type PO_W.
 *
 ***********************************************************************/



typedef enum
{
    PO_NONE=0,         // No operand e.g. NOP
    PO_W,              // The 'W' register
    PO_STATUS,         // The 'STATUS' register
    PO_FSR,            // The "file select register" (in 18c it's one of three)
    PO_FSRPTR0,		// virtual FSRPTR, add for pointer operation easier
    PO_FSRPTR1,		// virtual FSRPTR, add for pointer operation easier
    PO_FSRPTR2,		// virtual FSRPTR, add for pointer operation easier
	PO_ADCR,
	PO_ADCO1,
	PO_ADCO2,
	PO_ADCO1B, // BYTE INDEX
	PO_ADCO2B,
	PO_ADCRB, 

    PO_FSR0L,
    PO_FSR0H,
    PO_INDF,           // The Indirect register
    PO_POINC0,
	PO_PRINC0,
	
    PO_PODEC0,
    PO_POINC,

	PO_PRINC2,
	PO_PODEC2,

    PO_SSPBUF,
    PO_PRINC,
    PO_PLUSW,
    PO_INTE0,         // Interrupt Control register
    PO_GPR_REGISTER,   // A general purpose register
    PO_GPR_BIT,        // A bit of a general purpose register
    PO_GPR_TEMP,       // A general purpose temporary register
    PO_GPR_POINTER,    // A general purpose pointer
    PO_SFR_REGISTER,   // A special function register (e.g. PORTA)
    PO_PCL,            // Program counter Low register
    PO_PCLATH,         // Program counter Latch high register
	PO_PCLATU,
    PO_PRODL,			// HW Multiplier
    PO_PRODH,
    PO_TBLPTRL,
    PO_TBLPTRH,
    PO_TBLDL,
    PO_TBLDH,
    PO_LITERAL,        // A constant
    PO_IMMEDIATE,      //  (8051 legacy)
    PO_DIR,            // Direct memory (8051 legacy)
    PO_XDATA,			// for memory >= 0x200, assume XDATA
    PO_CRY,            // bit memory (8051 legacy)
    PO_BIT,            // bit operand.
    PO_STR,            //  (8051 legacy)
    PO_LABEL,
    PO_WILD            // Wild card operand in peep optimizer
} HYA_OPTYPE;


/***********************************************************************
 *
 *  HYA_OPCODE
 *
 *  This is not a list of the  opcodes per se, but instead
 *  an enumeration of all of the different types of opcodes.
 *
 ***********************************************************************/

typedef enum
{
    //POC_WILD = -1,   /* Wild card - used in the pCode peep hole optimizer
    // * to represent ANY  opcode */
    POC_ADDLW = 0,
    POC_ADDWF,
    POC_ADDFW,
    POC_ANDLW,
    POC_ANDWF,
    POC_ANDFW,
    POC_BCF,
    POC_BSF,
    POC_BTSZ,
    POC_BTSS,
    POC_BTGF, // this is a special one,
    POC_CALL,
    POC_COMF,
    POC_COMFW,
    POC_CLRF,
    POC_SETF,
    //POC_CLRW,
    POC_CLRWDT,
    POC_DECF,
    POC_DECFW,
    POC_DCSZ,
    POC_DCSUZ,
    POC_DCSZW,
    POC_DCSUZW,
    POC_JMP,
    // RJ,RCALL for reference
    POC_RJ,
    POC_RCALL,

    POC_INF,
    POC_INFW,
    POC_INSZ,
    POC_INSZW,
    POC_INSUZ,
    POC_INSUZW,
    POC_IORL,
    POC_IORWF,
    POC_IORFW,
    POC_MVWF,
    POC_MVFW,
    POC_MVL,
    POC_MVFF, // special MVFF, 2 word instruction
    POC_LDPR, // LDPR is special
    POC_NOP,
    POC_RETL,
    POC_RET,
    POC_RETI,
	POC_RETV, // return void, no W need for optimization
    POC_RLF,
    POC_RLFW,

    POC_RLCF,
    POC_RLCFW,

    POC_RRF,
    POC_RRFW,

    POC_RRCF,
    POC_RRCFW,

    POC_ARRCF,
    POC_ARRCFW,
    POC_ARLCF,
    POC_ARLCFW,

    POC_SUBL,
    //POC_SUBF, SUBF is SUBFW
    POC_SUBFWF,
    POC_SUBFWW,
    POC_SWPF,
    POC_SWPFW,
    //POC_TRIS,
    POC_XORLW,
    POC_XORF,
    POC_XORFW,
    POC_LBSR,

    POC_MULWF,
    POC_MULLW,

    // carry instructions
    POC_ADCFW,
    POC_ADCWF,
    POC_SBCFWW,
    POC_SBCFWF,

    POC_JC,
    POC_JNC,
    POC_JN,
    POC_JNN,
    POC_JO,
    POC_JNO,
    POC_JZ,
    POC_JNZ,

    POC_CPSE,
    POC_CPSG,
    POC_CPSL,
    POC_TFSZ,
    POC_MVWF2,
	POC_ADDFSR,
		POC_PUSHL,
		POC_MVSS,
		POC_MVSF,
    //POC_PAGESEL,
	POC_TBLR, // 2018 NOV, add TBLR for const lookup faster
	POC_MVLP,

    MAX_HY08AMNEMONICS
} HYA_OPCODE;


/***********************************************************************
 *  PC_TYPE  - pCode Types
 ***********************************************************************/

typedef enum
{
    PC_COMMENT=0,   /* pCode is a comment     */
    PC_INLINE,      /* user's inline code     */
    PC_OPCODE,      /* PORT dependent opcode  */
    PC_LABEL,       /* assembly label         */
    //PC_FLOW,        /* flow analysis          */
    PC_FUNCTION,    /* Function start or end  */
    PC_WILD,        /* wildcard - an opcode place holder used
	                 * in the pCode peep hole optimizer */
    PC_CSOURCE,     /* C-Source Line  */
    PC_ASMDIR,      /* Assembler directive */
    PC_BAD          /* Mark the pCode object as being bad */
} PC_TYPE;

/************************************************/
/***************  Structures ********************/
/************************************************/
/* These are here as forward references - the
 * full definition of these are below           */
struct pCode;
struct pCodeWildBlock;
struct pCodeRegLives;

/*************************************************
  pBranch

  The first step in optimizing pCode is determining
 the program flow. This information is stored in
 single-linked lists in the for of 'from' and 'to'
 objects with in a pcode. For example, most instructions
 don't involve any branching. So their from branch
 points to the pCode immediately preceding them and
 their 'to' branch points to the pcode immediately
 following them. A skip instruction is an example of
 a pcode that has multiple (in this case two) elements
 in the 'to' branch. A 'label' pcode is an where there
 may be multiple 'from' branches.
 *************************************************/

typedef struct pBranch
{
    struct pCode   *pc;    // Next pCode in a branch
    struct pBranch *next;  /* If more than one branch
	                        * the next one is here */
    //unsigned int head_compare : 1; // in peephole compare, this means this is head instr if it is in label

} pBranch;

/*************************************************
  pCodeOp

  pCode Operand structure.
  For those assembly instructions that have arguments,
  the pCode will have a pCodeOp in which the argument
  can be stored. For example

    movf   some_register,w

  'some_register' will be stored/referenced in a pCodeOp

 *************************************************/

typedef struct pCodeOp
{
    HYA_OPTYPE type;
    char *name;
	char *str;
	uint64_t name_hash;
	uint64_t strHash;
	struct pCode *TBLR_Reference; // special reference for TBLR

    struct {
   	unsigned int isX : 1; // set 1 means it is XDATA region, use for MVFF operation
	unsigned int parapcop : 1; // convert from para of this function
	unsigned int calleePara : 1; // this is para of next level
	unsigned int imm2label : 1; // immed to label set 1, for MVLP
	unsigned int needSub1 : 1; // for TBLDLH +1/-1
    unsigned int isC: 1; // special oper
    unsigned int isDC: 1;
    unsigned int isZ: 1;
    unsigned int isOV: 1;
    unsigned int isN: 1;
	unsigned int needCheckPara : 1;
    unsigned int imm2dir:1; // if imm, not mvl means imm2dir
    
    } flags;

} pCodeOp;

typedef struct pCodeOpLit
{
    pCodeOp pcop;
    int lit;
	//unsigned int addSPOffSet : 1;
	
} pCodeOpLit;

typedef struct pCodeOpImmd
{
    pCodeOp pcop;
    int offset;           /* low,med, or high byte of immediate value */
    int index;            /* add this to the immediate value */
    unsigned _const:1;    /* is in code space    */
    unsigned _function:1; /* is a (pointer to a) function */
    unsigned _inX : 1;
	unsigned _cp2gp : 1; // code ptr to general ptr, need or 0x80

    int rIdx;             /* If this immd points to a register */
    struct reg_info *r;       /* then this is the reg. */

} pCodeOpImmd;

typedef struct pCodeOpLabel
{
    pCodeOp pcop;
    int key;
    int offset;           /* low or high byte of label */
    int div2 : 1;
} pCodeOpLabel;

typedef struct pCodeOpReg
{
    pCodeOp pcop;    // Can be either GPR or SFR
    int rIdx;        // Index into the register table
    struct reg_info *r;
    int instance;    // byte # of Multi-byte registers
    struct pBlock *pb;
} pCodeOpReg;

typedef struct pCodeOpRegBit
{
    pCodeOpReg  pcor;       // The Register containing this bit
    int bit;                // 0-7 bit number.
    HYA_OPTYPE subtype;     // The type of this register.
    unsigned int inBitSpace: 1; /* True if in bit space, else
	                            just a bit of a register */
} pCodeOpRegBit;

typedef struct pCodeOpStr /* Only used here for the name of fn being called or jumped to */
{
    pCodeOp  pcop;
    unsigned isPublic: 1; /* True if not static ie extern */
} pCodeOpStr;

typedef struct pCodeOpWild
{
    pCodeOp pcop;

    struct pCodeWildBlock *pcwb;

    int id;                 /* index into an array of char *'s that will match
	                         * the wild card. The array is in *pcp. */
    pCodeOp *subtype;       /* Pointer to the Operand type into which this wild
	                         * card will be expanded */
    pCodeOp *matched;       /* When a wild matches, we'll store a pointer to the
	                         * opcode we matched */

} pCodeOpWild;


/*************************************************
    pCode

    Here is the basic build block of a instruction.
    Each instruction will get allocated a pCode.
    A linked list of pCodes makes a program.

**************************************************/

typedef struct pCode
{
    PC_TYPE    type;
    int auxFlag;
    unsigned id;         // unique ID number for all pCodes to assist in debugging
    int seq;             // sequence number

    struct pCode *prev;  // The pCode objects are linked together
    struct pCode *next;  // in doubly linked lists.

    

    struct pBlock *pb;   // The pBlock that contains this pCode.

    /* "virtual functions"
     *  The pCode structure is like a base class
     * in C++. The subsequent structures that "inherit"
     * the pCode structure will initialize these function
     * pointers to something useful */
    
    void (*destruct)(struct pCode *_this);
    void (*print)  (FILE *of,struct pCode *_this);

} pCode;


/*************************************************
    pCodeComment
**************************************************/

typedef struct pCodeComment
{

    pCode  pc;

    char *comment;

} pCodeComment;


/*************************************************
    pCodeComment
**************************************************/

typedef struct pCodeCSource
{

    pCode  pc;

    int  line_number;
	int  seq_number;
    char *line;
    char *file_name;
	unsigned int hasRAMWR : 1;

} pCodeCSource;


// /*************************************************
//     pCodeFlow

//   The Flow object is used as marker to separate
//  the assembly code into contiguous chunks. In other
//  words, everytime an instruction cause or potentially
//  causes a branch, a Flow object will be inserted into
//  the pCode chain to mark the beginning of the next
//  contiguous chunk.

// **************************************************/

// typedef struct pCodeFlow
// {

//     pCode  pc;

//     pCode *end;   /* Last pCode in this flow. Note that
// 	                 the first pCode is pc.next */

//     set *from;    /* flow blocks that can send control to this flow block */
//     set *to;      /* flow blocks to which this one can send control */
//     struct pCodeFlow *ancestor; /* The most immediate "single" pCodeFlow object that
// 	                             * executes prior to this one. In many cases, this
// 	                             * will be just the previous */

//     int inCond;   /* Input conditions - stuff assumed defined at entry */
//     int outCond;  /* Output conditions - stuff modified by flow block */

//     int firstBank; /* The first and last bank flags are the first and last */
//     int lastBank;  /* register banks used within one flow object */

//     int FromConflicts;
//     int ToConflicts;

//     set *registers;/* Registers used in this flow */

// } pCodeFlow;


/*************************************************
  pCodeFlowLink

  The Flow Link object is used to record information
 about how consecutive excutive Flow objects are related.
 The pCodeFlow objects demarcate the pCodeInstructions
 into contiguous chunks. The FlowLink records conflicts
 in the discontinuities. For example, if one Flow object
 references a register in bank 0 and the next Flow object
 references a register in bank 1, then there is a discontinuity
 in the banking registers.

*/
// typedef struct pCodeFlowLink
// {
//     pCodeFlow  *pcflow;   /* pointer to linked pCodeFlow object */

//     int bank_conflict;    /* records bank conflicts */

// } pCodeFlowLink;


/*************************************************
    pCodeInstruction

    Here we describe all the facets of a instruction
    (expansion for the 18cxxx is also provided).

**************************************************/

typedef enum asmaddrm {
    asm_NONE=0,
    asm_FDA,
    asm_FBA,
    asm_LIT,
    asm_FA,
    asm_KF,
    asm_F,
    asm_FSFD,
    asm_LDPR,
    asm_LABEL,
    asm_ONE, // for RETI
    asm_TBLR  //* or *+
} asmAddMode;

typedef struct pCodeInstruction
{

    pCode  pc;

    HYA_OPCODE op;        // The opcode of the instruction.

    char const * const mnemonic;       // Pointer to mnemonic string
    char const * const mne2;

    pBranch *from;       // pCodes that execute before this one
    pBranch *to;         // pCodes that execute after
    pBranch *label;      // pCode instructions that have labels

    pCodeOp *pcop;               /* Operand, if this instruction has one */
    char *pcflow;           /* flow block to which this instruction belongs -- removed*/
    pCodeCSource *cline;         /* C Source from which this instruction was derived */

    unsigned int num_ops;        /* Number of operands (0,1,2 for mid range ) */
    unsigned int isModReg:  1;   /* If destination is W or F, then 1==F */
    unsigned int isBitInst: 1;   /* e.g. BCF */
    unsigned int isBranch:  1;   /* True if this is a branching instruction */
    unsigned int isSkip:    1;   /* True if this is a skip instruction */
	unsigned int hasSkipInv : 1; /* true if has inverse skip inv , like dcsz->dcsuz*/
    unsigned int isLit:     1;   /* True if this instruction has an literal operand */
    unsigned int locked : 1;
    unsigned int srcislit : 1;
	unsigned int callFPTR : 1;  // 2020 MAY, callFPTR needs special LBSR operation

    HYA_OPCODE inverted_op;      /* Opcode of instruction that's the opposite of this one */
    unsigned int inCond;   // Input conditions for this instruction
    unsigned int outCond;  // Output conditions for this instruction
    asmAddMode addrMode;

    pCodeOp *pcop2;			// MVFF takes 2 operands
    unsigned int source_cnt; // for peep, -1 means is head, we compare specified head opcode
	int  exp_para_num;     // a number for linking time check if match (only for CALL)
	int  retOffP1;			// if this is return value, add 1 means it may be optimized by linker value 1: LSB
	int  noUseWCheckedID;
	int  noUseWCheckedResult;
} pCodeInstruction;


/*************************************************
    pCodeAsmDir
**************************************************/

typedef struct pCodeAsmDir
{
    pCodeInstruction pci;

    char *directive;
    char *arg;
} pCodeAsmDir;


/*************************************************
    pCodeLabel
**************************************************/

typedef struct pCodeLabel
{

    pCode  pc;

    char *label;
    int key;
    int ishead;

} pCodeLabel;


/*************************************************
    pCodeFunction
**************************************************/

typedef struct pCodeFunction
{

    pCode  pc;

    char *modname;
    char *fname;     /* If NULL, then this is the end of
	                    a function. Otherwise, it's the
	                    start and the name is contained
	                    here */
	symbol *symp;
    pBranch *from;       // pCodes that execute before this one
    pBranch *to;         // pCodes that execute after
    pBranch *label;      // pCode instructions that have labels

    int  ncalled;        /* Number of times function is called */
    unsigned isPublic:1; /* True if the fn is not static and can be called from another module (ie a another c or asm file) */
	unsigned isParaOnStack : 1; // true for var length call, pcall will be handled in linker!!
	unsigned addrReferenced : 1; // if addr referenced, it cannot be inline!!

} pCodeFunction;


/*************************************************
    pCodeWild
**************************************************/

typedef struct pCodeWild
{

    pCodeInstruction  pci;

    int    id;     /* Index into the wild card array of a peepBlock
	                * - this wild card will get expanded into that pCode
	                *   that is stored at this index */

    /* Conditions on wild pcode instruction */
    int    mustBeBitSkipInst:1;
	int	   labelOK : 1; // may or may not have label
    int    mustNotBeBitSkipInst:1;
    int    invertBitSkipInst:1;
    int	   isHead : 1;

    pCodeOp *operand;  // Optional operand
    pCodeOp *label;    // Optional label

} pCodeWild;

/*************************************************
    pBlock

    Here are  program snippets. There's a strong
    correlation between the eBBlocks and pBlocks.
    SDCC subdivides a C program into managable chunks.
    Each chunk becomes a eBBlock and ultimately in the
    port a pBlock.

**************************************************/

// 2016 chingson add optimize record "list"
// single direction

typedef struct svaropt varopt;
#define MAX_LAB_COUNT 4096
typedef struct pBlock
{
    memmap *cmemmap;   /* The snippet is from this memmap */
    char   dbName;     /* if cmemmap is NULL, then dbName will identify the block */
    pCode *pcHead;     /* A pointer to the first pCode in a link list of pCodes */
    pCode *pcTail;     /* A pointer to the last pCode in a link list of pCodes */

    struct pBlock *next;      /* The pBlocks will form a doubly linked list */
    struct pBlock *prev;

    set *function_entries;    /* dll of functions in this pblock */
    set *function_exits;
    set *function_calls;
    set *tregisters;
    set *txregisters;
	set *opt_list;

    set *FlowTree;
    //unsigned visited:1;       /* set true if traversed in call tree */
	//unsigned stkxxPromoted : 1; // only once promote
	//unsigned wsave_used : 1;
	unsigned stkxxNowTemp : 1;
	
    unsigned seq;             /* sequence number of this pBlock */
	int lastOptCount; // last optimize count, if 0, nomore to optimize
	// 2017 add map for label search 
	int labelIndexTab[MAX_LAB_COUNT]; // maximal 16384
	pCode * labelPtrTab[MAX_LAB_COUNT];
	int labelTabSize;
	int cSrcState; // 2019 we start to improve the c-src mapping correctness. The first step is check if mem write is in that line,
					// and if following c code is not mem write, we make sure next c code mapping is correct by its outdep
} pBlock;

/*************************************************
    pFile

    The collection of pBlock program snippets are
    placed into a linked list that is implemented
    in the pFile structure.

    The pcode optimizer will parse the pFile.

**************************************************/

typedef struct pFile
{
    pBlock *pbHead;     /* A pointer to the first pBlock */
    pBlock *pbTail;     /* A pointer to the last pBlock */

    pBranch *functions; /* A SLL of functions in this pFile */

} pFile;



/*************************************************
  pCodeWildBlock

  The pCodeWildBlock object keeps track of the wild
  variables, operands, and opcodes that exist in
  a pBlock.
**************************************************/
typedef struct pCodeWildBlock {
    pBlock    *pb;
    struct pCodePeep *pcp;    // pointer back to ... I don't like this...

    int       nvars;          // Number of wildcard registers in target.
    char    **vars;           // array of pointers to them
    int		*vari;

    int       nops;           // Number of wildcard operands in target.
    pCodeOp **wildpCodeOps;   // array of pointers to the pCodeOp's.

    int       nwildpCodes;    // Number of wildcard pCodes in target/replace
    pCode   **wildpCodes;     // array of pointers to the pCode's.

} pCodeWildBlock;

/*************************************************
  pCodePeep

  The pCodePeep object mimics the peep hole optimizer
  in the main SDCC src (e.g. SDCCpeeph.c). Essentially
  there is a target pCode chain and a replacement
  pCode chain. The target chain is compared to the
  pCode that is generated by gen.c. If a match is
  found then the pCode is replaced by the replacement
  pCode chain.
**************************************************/
typedef struct pCodePeep {
    pCodeWildBlock target;     // code we'd like to optimize
    pCodeWildBlock replace;    // and this is what we'll optimize it with.
	unsigned int needClearV : 1;
	unsigned int needClearO : 1;
	unsigned int needClearC : 1;
	int matchedCNT; // matched count for analysis, 2018 april

    /* (Note: a wildcard register is a place holder. Any register
     * can be replaced by the wildcard when the pcode is being
     * compared to the target. */

    /* Post Conditions. A post condition is a condition that
     * must be either true or false before the peep rule is
     * accepted. For example, a certain rule may be accepted
     * if and only if the Z-bit is not used as an input to
     * the subsequent instructions in a pCode chain.
	 ... Not used in our port, we check bitskip is ok
     */
    //unsigned int postFalseCond;
    //unsigned int postTrueCond;

} pCodePeep;

/*************************************************

  pCode peep command definitions

 Here are some special commands that control the
way the peep hole optimizer behaves

**************************************************/

enum peepCommandTypes {
    NOTBITSKIP = 0,
    BITSKIP,
    INVERTBITSKIP,
    _LAST_PEEP_COMMAND_
};

/*************************************************
    peepCommand structure stores the peep commands.

**************************************************/

typedef struct peepCommand {
    int id;
    char *cmd;
} peepCommand;

/*************************************************
    pCode Macros

**************************************************/
#define PCODE(x)  ((pCode *)(x))

#define PCI(x)    ((pCodeInstruction *)(x))
#define PCL(x)    ((pCodeLabel *)(x))
#define PCF(x)    ((pCodeFunction *)(x))
#define PCFL(x)   ((pCodeFlow *)(x))
#define PCFLINK(x)((pCodeFlowLink *)(x))
#define PCW(x)    ((pCodeWild *)(x))
#define PCCS(x)   ((pCodeCSource *)(x))
#define PCAD(x)	  ((pCodeAsmDir *)(x))

#define PCOP(x)   ((pCodeOp *)(x))
#define PCOL(x)   ((pCodeOpLit *)(x))
#define PCOI(x)   ((pCodeOpImmd *)(x))
#define PCOLAB(x) ((pCodeOpLabel *)(x))
#define PCOR(x)   ((pCodeOpReg *)(x))
#define PCORB(x)  ((pCodeOpRegBit *)(x))
#define PCOS(x)   ((pCodeOpStr *)(x))
#define PCOW(x)   ((pCodeOpWild *)(x))

#define PBR(x)    ((pBranch *)(x))

#define PCWB(x)   ((pCodeWildBlock *)(x))

#define isPCOLAB(x)     ((PCOP(x)->type) == PO_LABEL)
#define isPCOS(x)       ((PCOP(x)->type) == PO_STR)


/*
  macros for checking pCode types
*/
#define isPCI(x)        ((PCODE(x)->type == PC_OPCODE))
#define isPCFL(x)       ((PCODE(x)->type == PC_FLOW))
#define isPCF(x)        ((PCODE(x)->type == PC_FUNCTION))
#define isPCL(x)        ((PCODE(x)->type == PC_LABEL))
#define isPCW(x)        ((PCODE(x)->type == PC_WILD))
#define isPCCS(x)       ((PCODE(x)->type == PC_CSOURCE))
#define isPCASMDIR(x)	((PCODE(x)->type == PC_ASMDIR))

/*
  macros for checking pCodeInstruction types
*/
#define isCALL(x)       (isPCI(x) && ((PCI(x)->op == POC_CALL)||((PCI(x)->op==POC_JMP) && (PCI(x)->pcop->type==PO_STR))))
#define isPCI_BRANCH(x) (isPCI(x) &&  PCI(x)->isBranch)
#define isPCI_SKIP(x)   (isPCI(x) &&  PCI(x)->isSkip)
#define isPCI_LIT(x)    (isPCI(x) &&  PCI(x)->isLit)
#define isPCI_BITSKIP(x)(isPCI_SKIP(x) && PCI(x)->isBitInst)


#define isSTATUS_REG(r) ((r)->pc_type == PO_STATUS)

/*-----------------------------------------------------------------*
 * pCode functions.
 *-----------------------------------------------------------------*/

pCode *newpCode (HYA_OPCODE op, pCodeOp *pcop); // Create a new pCode given an operand
pCode *newpCode2(HYA_OPCODE op, pCodeOp *pcop, pCodeOp *pcop2);
pCode *newpCodeCharP(char *cP);              // Create a new pCode given a char *
pCode *newpCodeFunction(char *g, char *f,int,symbol *); // Create a new function
pCode *newpCodeLabel(char *name,int key);    // Create a new label given a key
pCode *newpCodeCSource(int ln, char *f, const char *l, int seq); // Create a new symbol line
pCode *newpCodeWild(int pCodeID, pCodeOp *optional_operand, pCodeOp *optional_label);
pCode * findNextInstruction(pCode *pci);

pCode *findPrevInstruction(pCode *pci);
pCode *findNextpCode(pCode *pc, PC_TYPE pct);
pCode *pCodeInstructionCopy(pCodeInstruction *pci,int invert);

pBlock *newpCodeChain(memmap *cm,char c, pCode *pc); // Create a new pBlock
void printpBlock(FILE *of, pBlock *pb);      // Write a pBlock to a file
void printpCode(FILE *of, pCode *pc);        // Write a pCode to a file
void addpCode2pBlock(pBlock *pb, pCode *pc); // Add a pCode to a pBlock
void addpBlock(pBlock *pb);                  // Add a pBlock to a pFile
void unlinkpCode(pCode *pc);
void copypCode(FILE *of, char dbName);       // Write all pBlocks with dbName to *of
void movepBlock2Head(char dbName);           // move pBlocks around
//void AnalyzeBanking(void);
//void ReuseReg(void);
void AnalyzepCode(char dbName);
void InlinepCode(void);
int  pCodeInitRegisters(void);
void HY08AinitpCodePeepCommands(void);
void pBlockConvert2ISR(pBlock *pb);
void pBlockMergeLabels(pBlock *pb);
void pCodeInsertAfter(pCode *pc1, pCode *pc2);
void pCodeInsertBefore(pCode *pc1, pCode *pc2);
void pCodeDeleteChain(pCode *f,pCode *t);

pCode *newpCodeAsmDir(char *asdir, char *argfmt, ...);

pCodeOp *newpCodeOpLabel(char *name, int key);
pCodeOp *newpCodeOpImmd(char *name, int offset, int index, int code_space,int x_space, int is_func, int size, pBlock *pb);
pCodeOp *newpCodeOpLit(int lit);
pCodeOp *newpCodeOpLitSign(int lit);
pCodeOp *newpCodeOpBit(char *name, int bit,int inBitSpace);
pCodeOp *newpCodeOpBitByOp(pCodeOp *pcop, int bit, int inBitSpace);

pCodeOp *newpCodeOpWild(int id, pCodeWildBlock *pcwb, pCodeOp *subtype);
pCodeOp *newpCodeOpRegFromStr(char *name);
pCodeOp *newpCodeOp(char *name, HYA_OPTYPE p);
pCodeOp *pCodeOpCopy(pCodeOp *pcop);
pCodeOp *popCopyGPR2Bit(pCodeOp *pc, int bitval);
pCodeOp *popCopyReg(pCodeOpReg *pc);

pBranch *pBranchAppend(pBranch *h, pBranch *n);

struct reg_info * getRegFromInstruction(pCode *pc);

char *get_op(pCodeOp *pcop, char *buff, size_t buf_size);
//uint64_t get_op_str_hash(pCodeOp* pcop);
char *pCode2str(char *str, size_t size, pCode *pc);

int pCodePeepMatchRule(pCode *pc);

void pcode_test(void);
void resetpCodeStatistics (void);
void dumppCodeStatistics (FILE *of);
void reCheckLocalVar(pFile *pf);

/*-----------------------------------------------------------------*
 * pCode objects.
 *-----------------------------------------------------------------*/

extern pCodeOpReg pc_status;
extern pCodeOpReg pc_inte0;

extern pCodeOpReg *pc_indf; /* pointer to either pc_indf_ or pc_indf0 */


extern pCodeOpReg pc_fsr0l;
extern pCodeOpReg pc_fsr0h;
extern pCodeOpReg pc_poinc0;
extern pCodeOpReg pc_princ0;
extern pCodeOpReg pc_princ2;

extern pCodeOpReg pc_podec0;
extern pCodeOpReg pc_podec2;

extern pCodeOpReg pc_plusw0;
extern pCodeOpReg pc_wreg;
extern pCodeOpReg pc_indf0;

extern pCodeOpReg pc_fsr1l;
extern pCodeOpReg pc_fsr1h;
extern pCodeOpReg pc_poinc1;
extern pCodeOpReg pc_plusw1;
extern pCodeOpReg pc_indf1;

extern pCodeOpReg pc_fsr2l;
extern pCodeOpReg pc_fsr2h;
extern pCodeOpReg pc_poinc2;
extern pCodeOpReg pc_plusw2;
extern pCodeOpReg pc_indf2;


extern pCodeOpReg pc_adcr ;
extern pCodeOpReg pc_adc1o ;
extern pCodeOpReg pc_adc2o ;
extern pCodeOpReg pc_adcrh;
extern pCodeOpReg pc_adcrm ;
extern pCodeOpReg pc_adcrl ;
extern pCodeOpReg pc_adco1h;
extern pCodeOpReg pc_adco1m ;
extern pCodeOpReg pc_adco1l;
extern pCodeOpReg pc_adco2h ;
extern pCodeOpReg pc_adco2m ;
extern pCodeOpReg pc_adco2l;


extern pCodeOpReg pc_pclatl;
extern pCodeOpReg pc_pclath;
extern pCodeOpReg pc_pclatu;
extern pCodeOpReg pc_wsave;     /* wsave, ssave and psave are used to save W, the Status and PCLATH*/
//extern pCodeOpReg pc_ssave;     /* registers during an interrupt */
//extern pCodeOpReg pc_psave;     /* registers during an interrupt */
extern pCodeOpReg pc_prodl;
extern pCodeOpReg pc_prodh;
extern pCodeOpReg pc_tbldl;
extern pCodeOpReg pc_tbldh;
extern pCodeOpReg pc_tblptrl;

extern pCodeOpReg pc_sspbuf;

extern pFile *the_pFile;
extern pCodeInstruction *HY08AMnemonics[MAX_HY08AMNEMONICS];

/*
 * From pcodepeep.h:
 */
int getpCode(char *mnem, unsigned dest, int *specialop);
int getpCodePeepCommand(char *cmd);
int pCodeSearchCondition(pCode *pc, unsigned int cond, int contIfSkip);
int getLabelKeyIndexInPb(pBlock *pb, int key);

#endif // __PCODE_H__

