BEGIN {
print "/*"
print " * version.h"
print " * control long build version number"
print " *"
print " * Created automatically with version.awk script"
print " *"
print " */"
print ""
print "#ifndef __VERSION_H__"
print "#define __VERSION_H__"
print ""

FS="[ \t.]"
}

/Revision/ {
cmd = "git rev-parse --short HEAD"
cmd | getline gitrev
	printf "#define SDCC_BUILD_NUMBER   \"%s\"\n", gitrev }
/Revision/ { printf "#define SDCC_BUILD_NR       0x%s\n", gitrev }

END {
print ""
print "#ifndef SDCC_BUILD_NUMBER"
print "#define SDCC_BUILD_NUMBER    \"10001\""
print "#define SDCC_BUILD_NR        10001"
print "#endif"
print ""
print "#endif"
}
