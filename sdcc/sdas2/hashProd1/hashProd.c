// hashProd.cpp : Defines the entry point for the console application.
//
#include<string.h>
#include<stdio.h>
//#include "stdafx.h"
#include "..\cityhash-c-master\city.h"

int main(int argc, char **argv)
{
	if (argc <2)
	{
		fprintf(stderr, "ussage: %s <part name>\n", argv[0]);
		return 1;
	}
	printf("0x%08X\n", CityHash64(argv[1], strlen(argv[1])) & 0xffffffff);
    return 0;
}

