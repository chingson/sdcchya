%{
/****************************
sdld2lex.l
****************************/
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <stdint.h>

#include "ldtype.h" // use different datatype than assembler, fine

#include "sdld2par.tab.h"
//#define DEBUG
#define printf2 printf
#define STRBUFSIZEM 256
#define STRBUFSIZEN 1024
	int strindex;
	char strbuffer[STRBUFSIZEM][STRBUFSIZEN];
	int opcode_lineno=0;
	void before_lex(void);
	void yyerror(char *);
	void newline_state_change(void);
%}

WS	[ \t\r]+
DIGIT ([0-9])
HEXDIGIT [0-9a-fA-F]
%s SMMARK


%%



{WS}		{
#ifdef DEBUG
	printf( "white space skipped\n");
#endif
		}
"\\\n"  {
#ifdef DEBUG
	printf( "\\ \\ \\ n skipped\n");
#endif

}

"\"" {

	int c;
	char *p=strbuffer[strindex];
	while((c=input())!=EOF)
	{
		if(c=='\\')
		{
			c=input();
			if(c==EOF)
				break;
			if(c=='\r')
			{
			    c=input();
				if(c==EOF)
				break;
			}
			if(c=='"')
				break;
			if(c!='\n')
			{
				*p++='\\';
				*p++=c;
			}
			continue;
		}
		if(c=='"')
			break;
		*p++=c;
	}
	*p++=0;
	yylval.string = strbuffer[strindex++];
	if( strindex>=STRBUFSIZEM) strindex = 0;
	
#ifdef DEBUG
			printf("QUOSTR found\n");
#endif
	
	return QUOSTR;	
	}
"$MACDEF".*\n|"$MACRPT".*\n {
		
		macrot *m = macroByLine(yytext);		
		newline_state_change();
		yylval.mac=m;
		return MACDEF;

}
"$MACINS".*\n {
// insert macro
	int i,j,len;
	char buf[4096];
	insertMacRec *mins = (insertMacRec*) calloc(1,sizeof(insertMacRec));
	//memset(mins,0,sizeof(insertMacRec));
	strncpy(buf,yytext,4095);
	len = strlen(buf);
	buf[len-1]='\0';
	for(i=len-1,j=0;i>=0;i--)
	{
		if(buf[i]=='%')
		{
			buf[i]='\0';
			if(j!=0)
			{
				mins->macroName = strdup(buf+i+1);
				break;
			}
			else
			{
				mins->locTree=strdup(buf+i+1);
				//break;
			}
			j++;
		}
	}
	yylval.mins = mins;
newline_state_change(); // no return ?
return MACINS;
}

"//".*\n {  
		
		newline_state_change();
		return '\n';
	}
";".*\n		{
#ifdef DEBUG
	printf( "comments skipped\n");
#endif
		
		newline_state_change();
		return '\n';
		}
	
">>" {
#ifdef DEBUG
		printf("SHIFTRIGHT\n");
#endif
		return SHIFTRIGHT;

}
"<>" {
#ifdef DEBUG
		printf("SHIFTRIGHT\n");
#endif
		return CHECKA;

}


"<<" {
#ifdef DEBUG
		printf("SHIFTLEFT\n");
#endif
		return SHIFTLEFT;

}
">" {
#ifdef DEBUG
		printf(">\n");
#endif
		return '>';

}
"!" {
#ifdef DEBUG
		printf("!\n");
#endif
		return '!';
}
"<" {
#ifdef DEBUG
		printf("<\n");
#endif
		return '<';

}



"\n"	{
#ifdef DEBUG
		printf("new line\n");
#endif
		newline_state_change();
		return'\n';
	}
"/*"    	{
                   int c;
#ifdef DEBUG
	printf( "remark type 2..\n");
#endif
		   while((c = input()) != EOF)
                       {
                       if(c == '*')
                           {
                           if((c = input()) == '/')
                               break;
                           else
                               unput(c);
                           }
                       }
			if(c == EOF )
				yyerror("comments not end at EOF");
                   }
{DIGIT}{HEXDIGIT}+H|{DIGIT}{HEXDIGIT}+h	{
		  sscanf(yytext, "%XH", &yylval.val);
		  return NUM;
		}
0x{HEXDIGIT}+	{
		  size_t i;
		  uint64 val;
		  char *end;
		  for( i = 0; i < yyleng; i++)
			yytext[i]= toupper(yytext[i]);
		  //sscanf(yytext, "0X%X", &yylval.val);
		  val = strtoull(yytext+2, &end, 16);
		  if(val&0xffffffff00000000ll)
			{
				yylval.val64=val;
				return NUM64;
			}
			yylval.val=val&0xffffffff;
		  return NUM;
#ifdef DEBUG
	printf( "0xnum..\n");
#endif
		}
{DIGIT}+|\-{DIGIT}+	{
#ifdef DEBUG
			printf("int NUM found\n");
#endif
			yylval.val=atoi(yytext);
			return NUM;
		}
"@FSR2-" {
	yylval.val=0;
#ifdef DEBUG
			printf("FSR2H found\n");
#endif
	return FSR2H;
}
"#" {
#ifdef DEBUG
			printf("# found\n");
#endif
	return '#';	
	}
"==" {
#ifdef DEBUG
			printf("== found\n");
#endif
	opcode_lineno = yylineno;
	return CEQ;	
	}
"!=" {
#ifdef DEBUG
			printf("!= found\n");
#endif
	opcode_lineno = yylineno;
	return CNE;	
	}
">=" {
#ifdef DEBUG
			printf(">= found\n");
#endif
	opcode_lineno = yylineno;
	return CGE;	
	}
"<=" {
#ifdef DEBUG
			printf("<= found\n");
#endif
	opcode_lineno = yylineno;
	return CLE;	
	}


"=" {
#ifdef DEBUG
			printf("= found\n");
#endif
	opcode_lineno = yylineno;
	return '=';	
	}



"^" {
#ifdef DEBUG
			printf("^ found\n");
#endif
	opcode_lineno = yylineno;
	return '^';	
	}


"&" {
#ifdef DEBUG
			printf("& found\n");
#endif
	opcode_lineno = yylineno;
	return '&';	
	}

"|" {
#ifdef DEBUG
			printf("= found\n");
#endif
	opcode_lineno = yylineno;
	return '|';	
	}


":" {
#ifdef DEBUG
			printf(": found\n");
#endif
	return ':';

}	

"+" {
#ifdef DEBUG
	printf("+ found ");
#endif
	return '+';
}

"-" {
#ifdef DEBUG
	printf("- found ");
#endif
	return '-';
}

"(" {
#ifdef DEBUG
	printf("( found ");
#endif
	return '(';
}

")" {
#ifdef DEBUG
	printf(") found ");
#endif
	return ')';
}

"[" {
#ifdef DEBUG
	printf("[ found ");
#endif
	return '[';
}

"]" {
#ifdef DEBUG
	printf("] found ");
#endif
	return ']';
}

"{" {
#ifdef DEBUG
	printf("{ found ");
#endif
	return '{';
}

"}" {
#ifdef DEBUG
	printf("} found ");
#endif
	return '}';
}


				
"," {
#ifdef DEBUG
	printf(", found ");
#endif
	return ',';
}
"*" {
#ifdef DEBUG
	printf("STAR found ");
#endif
	return '*';
}

"__NULL" {
	return NULLMARK;
}
$ASMINC {
	return INCHEAD;
}
$MACROM {
	BEGIN SMMARK;
	return MACROHEADMARK;
}
$MACRORPT {
	BEGIN SMMARK;
	return MACRORPTMARK;
}
$MACROENDM {
	BEGIN INITIAL;
	return MACROENDMARK;
}


$VER {
	return VERHEAD;
}

$MOD {
	return MHEAD;
}

$EXPORT {
	return EXPORTHEAD;
}

$REF {
	return REFHEAD;
}
$EQU {
	return EQUHEAD;
}
$BLANK {
	return BLANKHEAD;
}
$LAB {
	return LABELHEAD;
}
$WDATA {
	yylval.val=0;
	return WDATAHEAD;
}
$WDATAH {
	yylval.val=1;
	return WDATAHEAD;
}
$WDATAS {
	yylval.val=2;
	return WDATAHEAD;
}
$AREA {
	return AREAHEAD;
}
$ORG {
	return ORGHEAD;
}
$ASFROM {
	return ASFROMHEAD;
}
$SRC {
	return SRCHEAD;
}
$ENDMOD {
	return MFIN;
}
$FUNC { yylval.val = 0;
#ifdef DEBUG
printf("FUNCHEAD found at %d\n",yylineno);
#endif
 return FUNCHEAD;}
$FUNC_PSTK { yylval.val = 1; 
#ifdef DEBUG
printf("FUNCHEAD found at %d\n",yylineno);
#endif
return FUNCHEAD;}

$FUNC_CFPTR { yylval.val = 2;
#ifdef DEBUG
printf("FUNCHEAD found at %d\n",yylineno);
#endif
 return FUNCHEAD;}
$FUNC_PSTK_CFPTR { yylval.val = 3; 
#ifdef DEBUG
printf("FUNCHEAD found at %d\n",yylineno);
#endif
return FUNCHEAD;}


$ENDFUNC { return ENDFUNCHEAD;}
$CDB { return CDBHEAD;}
$CDBF {return CDBFHEAD;}
$C	{ return CALLMARK;}
$L  { return LOCALMARK;}
$R	{ return RETURNSIZEMARK;}
$XL { return XLOCALMARK;}
$LIB {
	return ISLIB;
}
<SMMARK>[A-Z_a-z][A-Z_a-z0-9=+-]*	 {
	
		strncpy(strbuffer[strindex], yytext,STRBUFSIZEN);
		yylval.string=strbuffer[strindex++];
		if( strindex>=STRBUFSIZEM) strindex = 0;
#ifdef DEBUG
	printf2("string found --%s\n", yylval.string );
#endif
	//	label_lineno=yylineno;
		return LABEL;
	
}

[A-Z_a-z][A-Z_a-z0-9]*	 {
	
		strncpy(strbuffer[strindex], yytext, STRBUFSIZEN);
		yylval.string=strbuffer[strindex++];
		if( strindex>=STRBUFSIZEM) strindex = 0;
#ifdef DEBUG
	printf2("string found --%s\n", yylval.string );
#endif
	//	label_lineno=yylineno;
		return LABEL;
	
}


"$$" {
	// only symbol
	return PCNOW; 
}
"$MLINE$" {
	int i=0,c;
	while( (c=input())!='\n')
	{
		strbuffer[strindex][i++]=c;
		if(i>=STRBUFSIZEN)
		{
			fprintf(stderr,"Linker Error: line too long at (obj) line %d.\n", yylineno);
			exit(2);
		}
	}
	unput(c); // push back '\n'
	strbuffer[strindex][i++]='\0';
	yylval.string=strbuffer[strindex++];
	if(strindex>=STRBUFSIZEM) strindex = 0;
	return LABEL;
}


%%

void before_lex(void)
{
		strindex=0;
		fflush(stdout);
}

void newline_state_change(void)
{
	BEGIN INITIAL;
}

