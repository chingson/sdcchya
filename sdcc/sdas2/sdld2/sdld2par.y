%{
    /****************************
    sdld2par.y
    // we still use C code to do it
    // for the maximal compatibility
    ****************************/

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <stdint.h>

#define DEBUG_BSR 0
#include "device.h"
#include "ldtype.h"
#include "symtab.h"
#ifndef _WIN32
#include <unistd.h>
#endif

void yyerror(char const *s);


extern void before_lex(void);
extern int yylex(void);
extern int find_tgtid(void);
int foundOutOfRange=0;
wData * OutOfRangeWdp[1024]; // maximal 1024
int updateList = 0; // -u
int genMapFile = 0;
int genIntelHex = 0;
int genMotHex = 0;
int genCDBFile = 0;
int needOptimize=1;
int needRearrangeFunc=1;
int needOptimizeJxx=1;
int needOptComm=0;
int commCallLevLimit=5; // if call level>=5, common code opt is not allowed
int dataMaxAddr=0x80;
int localMaxAddr=0x80;
int xdataMaxAddr=-1;
int xlocalMaxAddr=-1;
int xStartAddr=0x200;
int xTailAddr=-1; // 2019 try to assign from tail
int xStartAssigned=0;
int xTailAssigned=0;
int labelLock=0;
int mvxxDis=0;
int mvxxForce=0;
int f08a=0;
int hy17p56p55=0;
extern int need_remap;
extern int yylineno;
extern asmIncInfo *asmIncHead;
extern asmIncInfo *asmIncTail;
    
    int error_count=0;
    char *processingFileName;

moduleLD *nowMLD=NULL;
area *nowArea;

char outputFileName[PATH_MAX];
char objFileName[PATH_MAX];
//char libFileName[PATH_MAX];
char TempFile[PATH_MAX];


char * objBasename[128]; // max 128 obj files
char * objSrcName[128]; // obj's src name
char * libBasename[128]; // max 128 obj files
char * libFullPath[128]; 
char * outputBasename; // max 128 obj files
char * nowLibFullPath=NULL;
char nowRelFullPath[PATH_MAX];
  

// area start names, if not ABS, it can assgn start address
char * startAssignedAreaName [MAX_AREAADDR_NUM];
int startAssignedAreaAddr[MAX_AREAADDR_NUM];
int startAssignedAreaNum=0;
uint64 startAssignedAreaNameHash[MAX_AREAADDR_NUM];
unsigned char assignedAreaUsed[MAX_AREAADDR_NUM];



// this is link list
srcLine *libLines[128];
srcLine *libLineTails[128];

srcLine *searchPath=NULL;
unsigned char rom_data[MAX_ROM_SIZE];
unsigned char ram_data[MAX_RAM_SIZE];
unsigned char rom_assigned[MAX_ROM_SIZE];
unsigned char ram_assigned[MAX_RAM_SIZE];

macrot * defined_macros[4096];
moduleLD * macros_mod[4096];
int defined_macro_num=0;

insertMacRec * macInsArray[4096];
moduleLD *macInsMod[4096];
int macInsNum = 0;

int objFileCount = 0;
int libFileCount = 0;
int input_assigned = 0;
int output_assigned = 0;
//int swTgtId=-1; // assume not include!!
int verbose=0;
int dataOrgByte=1;
int keep_unused_function=0;

funcRecord *nowFp=NULL;

struct sTopMap topMap;

// hash values
uint64_t hashMain;
uint64_t hashInt;
uint64_t hashPRINC2;
uint64_t hashsetjmpid;
uint64_t hashHEADFUNC;
uint64_t hashCCODE;


int ramModel=RAM_MODEL_NORMAL;
// constructor
    void parser_init(void)
    {
        // place any extra initialisation code here
		char buffer[PATH_MAX];
		char *env;
		memset(&topMap,0,sizeof(topMap));
		memset(rom_data,0,sizeof(rom_data));
		memset(ram_data,0, sizeof(ram_data));
		memset(rom_assigned,0,sizeof(rom_assigned));
		memset(ram_assigned,0, sizeof(ram_assigned));
		searchPath = appendSrcLine(searchPath,newSrcLine(".",0));

		


		memset(defined_macros,0,sizeof(defined_macros));
		memset(macros_mod,0,sizeof(macros_mod));
		memset(macInsArray,0,sizeof(macInsArray));
		memset(macInsMod,0,sizeof(macInsMod));
        env=getenv("SDCC_HOME");
		if(env!=NULL)
		{
			strncpy(buffer,env, PATH_MAX);
			appendFinalSlash(buffer);
#ifdef _MSC_VER
			strcat(buffer,"lib\\HY08A");
#else
			strcat(buffer,"lib/HY08A");
#endif
			searchPath = appendSrcLine(searchPath,newSrcLine(buffer,0));
		}
    }
	

// destructor
    void parser_exit(void)
    {
        // place any extra cleanup code here
    }

%}


%union  { /* 2 kinds of values */
    int  val;
	uint64 val64;
    char * string;
    expression *exp;
	dataItem *items;
	wData *wdp;
	macrot *mac;
	insertMacRec *mins;
}

%token <mac> MACDEF;
%token <mins> MACINS;
%token <val> NUM;
%token <val64> NUM64;
%token <val> VER;
%token <val> MHEAD;
%token <val> MFIN;
%token <val> EXPORTHEAD;
%token <val> REFHEAD;
%token <val> EQUHEAD;
%token <val> AREAHEAD;
%token <val> ORGHEAD;
%token <val> WDATAHEAD;
%token <val> LABELHEAD;
%token <val> VERHEAD;
%token <val> BLANKHEAD;
%token <val> ASFROMHEAD;
%token <val> CDBHEAD;
%token <val> CDBFHEAD;

%token <val> PCNOW;
%token <val> SRCHEAD;
%token <val> ISLIB;
%token <val> FUNCHEAD;
%token <val> ENDFUNCHEAD;
%token <val> CALLMARK;
%token <val> LOCALMARK;
%token <val> RETURNSIZEMARK;

%token <val> XLOCALMARK;
%token <val> NULLMARK;
%token <val> MACROHEADMARK;
%token <val> MACRORPTMARK;
%token <val> MACROENDMARK;
%token <val> FSR2H


%token <string> LABEL;
%token <string> QUOSTR;

%token <val> CEQ;
%token <val> CNE;
%token <val> CGE;
%token <val> CLE;

%token <val> INCHEAD;


%type <val> numi; // + or -

%type <exp> expr;
%type <val> module_rec; // module record
%type <val> func_rec;
%type <val> endfunc_rec;
%type <val> export_rec;
%type <val> cdb_rec;
%type <val> reference_rec;
%type <val> area_rec;
%type <val> label_rec;
%type <val> equ_rec;
%type <val> wdata_rec;
%type <val> org_rec;
%type <val> asfrom_rec;
%type <val> blank_rec;
%type <val> version_rec;
%type <val> src_rec;
%type <val> lines;
%type <val> line;
%token <val> SHIFTRIGHT;
%token <val> SHIFTLEFT;
%token <val> CHECKA; 

%type <items> callList;
%type <items> localList;
%type <wdp> macroMark_rec;
%type <wdp> macroEndMark_rec;
%type <wdp> macroMark_rec1;
%type <wdp> macroMark_rec2;
%type <wdp> macroMark_rpt;
%type <val> asmIncRec;




%left '+' '-' '|' '&'
%%

/////////////////////////////////////////////////////////////////////////////
// rules section

// place your YACC rules here (there must be at least one)

//Grammar

lines:
  line  
| lines line {
	if(nowArea && nowArea->wDataTail && nowArea->wDataTail->locTree==NULL)
		fprintf(stderr,"srange loc\n");
}
;

// note that labelLine need not '\n' for following MNE

line :
 module_rec '\n' { $$=$1;}
| export_rec  '\n' {$$=$1;}
| cdb_rec '\n' {$$=$1;}
| reference_rec '\n'  {$$=$1;}
| area_rec '\n'  {$$=$1;}
| label_rec '\n' {$$=$1;}
| equ_rec '\n'  {$$=$1;}
| wdata_rec '\n'  {$$=$1;}
| org_rec '\n'  {$$=$1;}
|  version_rec '\n'  {$$=$1;}
| blank_rec '\n'  {$$=$1;}
| src_rec '\n' {$$=$1;}
| func_rec '\n' {$$=$1;}
| endfunc_rec '\n' {$$=$1;}
| macroMark_rec '\n' {$$=0;}
| macroEndMark_rec '\n' {$$=0;}
| asfrom_rec '\n' {$$=0;}
| asmIncRec '\n' {$$=0;}
| MACDEF {$$=0; defined_macros[defined_macro_num]=$1;
		macros_mod[defined_macro_num++]=nowMLD;
}| MACINS {$$=0; macInsArray[macInsNum]=$1;
		macInsMod[macInsNum++]=nowMLD;
}
| '\n' {$$=0;}
;

asmIncRec:
	INCHEAD ':' QUOSTR ':' NUM ':'QUOSTR {
	asmIncInfo *aifp = newAsmIncInfo($3,$5,$7);
	appendAsmIncInfo(&asmIncHead, &asmIncTail,aifp);
	$$=0;
	}
;

macroEndMark_rec:
	MACROENDMARK ':' NUM ':' QUOSTR {
	wdataMacroJob( -1, -1, $3, NULL, NULL);
	$$=nowArea->wDataTail;
	$$->wdIsMacroMarkEnd=1;
	$$->locTree=strdup($5);
	}
;

macroMark_rec:
	macroMark_rec1 {$$=$1;}
| macroMark_rec2 {$$=$1;}
| macroMark_rpt {$$=$1;}
;

macroMark_rpt:
	MACRORPTMARK ':' NUM ':' NUM ':' QUOSTR {
	// id, level lineno name
	wdataMacroJob( -1, $5, $3, "RPT", NULL);
	$$=nowArea->wDataTail;
	$$->locTree=strdup($7);
}
;
macroMark_rec1:
MACROHEADMARK ':' NUM ':' NUM ':' NUM ':' LABEL ':' NULLMARK ':' QUOSTR {
	// id, level lineno name
	wdataMacroJob( $3, $5, $7, $9, NULL);
	$$=nowArea->wDataTail;
	$$->locTree = strdup($13);
}
;

macroMark_rec2:
MACROHEADMARK ':' NUM ':' NUM ':' NUM ':' LABEL ':' LABEL{
	dataItem *dp=newEmptyDataItem();
	expression *exp = newSymbolExp($11,$7);
	dp->exp = exp;
	wdataMacroJob( $3, $5, $7, $9, dp);
	$$=nowArea->wDataTail;
	
	
}
| macroMark_rec2 ',' LABEL {
	dataItem *dp = $1->macroPara;
	while(dp->next)
		dp=dp->next;
	dp->next = newEmptyDataItem();
	dp->next->exp = newSymbolExp($3,-1);
	$$=$1;
}
| macroMark_rec2 ':' QUOSTR {
	$$=$1;
	$1->locTree=strdup($3);
}
;
endfunc_rec:
 ENDFUNCHEAD {$$=0;  nowFp=NULL;}
 ;

func_rec:
	FUNCHEAD ':' LABEL ':' QUOSTR {
	funcRecord *frec = newEmptyFuncRecord();
	strncpy(frec->name,$3,MAXSTRSIZE0-1);
	frec->namehash = hash(frec->name);
	frec->areap=nowArea;
	frec->paraOnStack = ($1&1);
	frec->doCallFPTR =($1&2);
	frec->m = nowMLD;
	nowMLD->funcs[nowMLD->funcNum]=frec;
	nowMLD->funcNum++;
	$$=0;
	nowFp = frec;
	}
| FUNCHEAD ':' LABEL ':' QUOSTR   callList{
	funcRecord *frec = newEmptyFuncRecord();
	strncpy(frec->name,$3,MAXSTRSIZE0-1);
	frec->namehash = hash(frec->name);
	frec->areap=nowArea;
	frec->paraOnStack = $1;
	frec->callList = $6;
	frec->m = nowMLD;
	frec->paraOnStack = $1;
	nowMLD->funcs[nowMLD->funcNum]=frec;
	nowMLD->funcNum++;
	nowFp = frec;
	$$=0;
}
| FUNCHEAD ':' LABEL ':' QUOSTR   localList{
	funcRecord *frec = newEmptyFuncRecord();
	strncpy(frec->name,$3,MAXSTRSIZE0-1);
	frec->namehash = hash(frec->name);
	frec->areap=nowArea;
	frec->paraOnStack = $1;
	frec->localList = $6;
	frec->m = nowMLD;
	nowMLD->funcs[nowMLD->funcNum]=frec;
	nowMLD->funcNum++;
	frec->paraOnStack = $1;
	nowFp = frec;
	$$=0;
}
| FUNCHEAD ':' LABEL ':' QUOSTR  callList  localList{
	funcRecord *frec = newEmptyFuncRecord();
	strncpy(frec->name,$3,MAXSTRSIZE0-1);
	frec->namehash = hash(frec->name);
	frec->areap=nowArea;
	frec->paraOnStack = $1;
	frec->callList = $6;
	frec->localList = $7;
	frec->m = nowMLD;
	frec->paraOnStack = $1;
	nowMLD->funcs[nowMLD->funcNum]=frec;
	nowMLD->funcNum++;
	nowFp = frec;
	$$=0;
} 
|	FUNCHEAD ':' numi ':' LABEL ':' QUOSTR {
	funcRecord *frec = newEmptyFuncRecord();
	strncpy(frec->name,$5,MAXSTRSIZE0-1);
	frec->namehash = hash(frec->name);
	frec->areap=nowArea;
	frec->paraOnStack = $1;
	frec->m = nowMLD;
	frec->declaredParamSize=$3;
	nowMLD->funcs[nowMLD->funcNum]=frec;
	nowMLD->funcNum++;
	$$=0;
	nowFp = frec;
	}
| FUNCHEAD ':' numi ':' LABEL ':' QUOSTR   callList{
	funcRecord *frec = newEmptyFuncRecord();
	frec->declaredParamSize=$3;
	strncpy(frec->name,$5,MAXSTRSIZE0-1);
	frec->namehash = hash(frec->name);
	frec->areap=nowArea;
	frec->paraOnStack = $1;
	frec->callList = $8;
	frec->m = nowMLD;
	frec->paraOnStack = $1;
	nowMLD->funcs[nowMLD->funcNum]=frec;
	nowMLD->funcNum++;
	nowFp = frec;
	$$=0;
}
| FUNCHEAD ':' numi ':' LABEL ':' QUOSTR   localList{
	funcRecord *frec = newEmptyFuncRecord();
	strncpy(frec->name,$5,MAXSTRSIZE0-1);
	frec->declaredParamSize=$3;
	frec->namehash = hash(frec->name);
	frec->areap=nowArea;
	frec->paraOnStack = $1;
	frec->localList = $8;
	frec->m = nowMLD;
	nowMLD->funcs[nowMLD->funcNum]=frec;
	nowMLD->funcNum++;
	nowFp = frec;
	$$=0;
}
| FUNCHEAD ':' numi ':' LABEL ':' QUOSTR  callList  localList{
	funcRecord *frec = newEmptyFuncRecord();
	strncpy(frec->name,$5,MAXSTRSIZE0-1);
	frec->declaredParamSize=$3;
	frec->namehash = hash(frec->name);
	frec->areap=nowArea;
	frec->paraOnStack = $1;
	frec->callList = $8;
	frec->localList = $9;
	frec->m = nowMLD;
	nowMLD->funcs[nowMLD->funcNum]=frec;
	nowMLD->funcNum++;
	nowFp = frec;
	$$=0;
}

|	FUNCHEAD ':' RETURNSIZEMARK ':' numi ':' LABEL ':' QUOSTR {
	funcRecord *frec = newEmptyFuncRecord();
	strncpy(frec->name,$7,MAXSTRSIZE0-1);
	frec->namehash = hash(frec->name);
	frec->areap=nowArea;
	frec->returnSize = $5;
	frec->paraOnStack = ($1&1);
	frec->doCallFPTR =($1&2);
	frec->m = nowMLD;
	nowMLD->funcs[nowMLD->funcNum]=frec;
	nowMLD->funcNum++;
	$$=0;
	nowFp = frec;
	}
| FUNCHEAD ':' RETURNSIZEMARK ':' numi ':' LABEL ':' QUOSTR   callList{
	funcRecord *frec = newEmptyFuncRecord();
	strncpy(frec->name,$7,MAXSTRSIZE0-1);
	frec->namehash = hash(frec->name);
	frec->areap=nowArea;
	frec->paraOnStack = $1;
	frec->returnSize = $5;
	frec->callList = $10;
	frec->m = nowMLD;
	frec->paraOnStack = $1;
	nowMLD->funcs[nowMLD->funcNum]=frec;
	nowMLD->funcNum++;
	nowFp = frec;
	$$=0;
}
| FUNCHEAD ':' RETURNSIZEMARK ':' numi ':' LABEL ':' QUOSTR   localList{
	funcRecord *frec = newEmptyFuncRecord();
	strncpy(frec->name,$7,MAXSTRSIZE0-1);
	frec->namehash = hash(frec->name);
	frec->areap=nowArea;
	frec->paraOnStack = $1;
	frec->localList = $10;
	frec->returnSize = $5;
	frec->m = nowMLD;
	nowMLD->funcs[nowMLD->funcNum]=frec;
	nowMLD->funcNum++;
	frec->paraOnStack = $1;
	nowFp = frec;
	$$=0;
}
| FUNCHEAD ':' RETURNSIZEMARK ':' numi ':' LABEL ':' QUOSTR  callList  localList{
	funcRecord *frec = newEmptyFuncRecord();
	strncpy(frec->name,$7,MAXSTRSIZE0-1);
	frec->namehash = hash(frec->name);
	frec->areap=nowArea;
	frec->paraOnStack = $1;
	frec->callList = $10;
	frec->returnSize = $5;
	frec->localList = $11;
	frec->m = nowMLD;
	nowMLD->funcs[nowMLD->funcNum]=frec;
	nowMLD->funcNum++;
	nowFp = frec;
	$$=0;
} 
|	FUNCHEAD ':' RETURNSIZEMARK ':' numi ':' numi ':' LABEL ':' QUOSTR {
	funcRecord *frec = newEmptyFuncRecord();
	strncpy(frec->name,$9,MAXSTRSIZE0-1);
	frec->namehash = hash(frec->name);
	frec->areap=nowArea;
	frec->paraOnStack = $1;
	frec->returnSize = $5;
	frec->m = nowMLD;
	frec->declaredParamSize=$7;
	nowMLD->funcs[nowMLD->funcNum]=frec;
	nowMLD->funcNum++;
	$$=0;
	nowFp = frec;
	}
| FUNCHEAD ':' RETURNSIZEMARK ':' numi ':' numi ':' LABEL ':' QUOSTR   callList{
	funcRecord *frec = newEmptyFuncRecord();
	frec->declaredParamSize=$7;
	strncpy(frec->name,$9,MAXSTRSIZE0-1);
	frec->namehash = hash(frec->name);
	frec->areap=nowArea;
	frec->paraOnStack = $1;
	frec->returnSize = $5;
	frec->callList = $12;
	frec->m = nowMLD;
	frec->paraOnStack = $1;
	nowMLD->funcs[nowMLD->funcNum]=frec;
	nowMLD->funcNum++;
	nowFp = frec;
	$$=0;
}
| FUNCHEAD ':' RETURNSIZEMARK ':' numi ':' numi ':' LABEL ':' QUOSTR   localList{
	funcRecord *frec = newEmptyFuncRecord();
	strncpy(frec->name,$9,MAXSTRSIZE0-1);
	frec->declaredParamSize=$7;
	frec->namehash = hash(frec->name);
	frec->areap=nowArea;
	frec->paraOnStack = $1;
	frec->returnSize = $5;
	frec->localList = $12;
	frec->m = nowMLD;
	nowMLD->funcs[nowMLD->funcNum]=frec;
	nowMLD->funcNum++;
	nowFp = frec;
	$$=0;
}
| FUNCHEAD ':' RETURNSIZEMARK ':' numi ':' numi ':' LABEL ':' QUOSTR  callList  localList{
	funcRecord *frec = newEmptyFuncRecord();
	strncpy(frec->name,$9,MAXSTRSIZE0-1);
	frec->declaredParamSize=$7;
	frec->namehash = hash(frec->name);
	frec->areap=nowArea;
	frec->paraOnStack = $1;
	frec->callList = $12;
	frec->localList = $13;
	frec->returnSize = $5;
	frec->m = nowMLD;
	nowMLD->funcs[nowMLD->funcNum]=frec;
	nowMLD->funcNum++;
	nowFp = frec;
	$$=0;
}

	;

numi:
	NUM { $$=$1;
		//fprintf(stderr,"num %d to numi\n", $1);
		}
	| '-' NUM {$$=0-$2;
		//fprintf(stderr,"num -%d to numi\n", $2);
		}
	;

callList:
	':' CALLMARK ':' LABEL { 
		$$=newEmptyDataItem();
		$$->exp = newSymbolExp($4, 0);
}
|  callList ':' CALLMARK ':' LABEL {
		dataItem *p=$1;
	while(p->next) p=p->next;
		p->next = newEmptyDataItem();
		p->next->exp = newSymbolExp($5, 0);
		$$=$1;
		
}
|':' CALLMARK ':' NUM { 
		$$=newEmptyDataItem();
		$$->exp = newConstExp($4, 0);
}
|  callList ':' CALLMARK ':' NUM {
		dataItem *p=$1;
	while(p->next) p=p->next;
		p->next = newEmptyDataItem();
		p->next->exp = newConstExp($5, 0);
		$$=$1;
		
}
;
localList:
	':' LOCALMARK ':' LABEL { 
		$$=newEmptyDataItem();
		$$->exp = newSymbolExp($4, 0);
}
|  localList ':' LOCALMARK ':' LABEL {
		dataItem *p=$1;
	while(p->next) p=p->next;
		p->next = newEmptyDataItem();
		p->next->exp = newSymbolExp($5, 0);
		$$=$1;
}
|':' XLOCALMARK ':' LABEL { 
		$$=newEmptyDataItem();
		$$->exp = newSymbolExp($4, 0);
		$$->exp-> ExpSymIsX = 1;
}
|  localList ':' XLOCALMARK ':' LABEL {
		dataItem *p=$1;
	while(p->next) p=p->next;
		p->next = newEmptyDataItem();
		p->next->exp = newSymbolExp($5, 0);
		p->next->exp->ExpSymIsX=1;
		$$=$1;
}
;

src_rec:
	SRCHEAD ':' QUOSTR {
		EXITIFNULLMLD
		strncpy(nowMLD->srcName,$3,MAXSTRSIZE0-1);
		$$=0;
	}
	;
version_rec:
	VERHEAD ':' LABEL {
	if(strncmp($3,ASLNKVER,11))
	{
		fprintf(stderr,"Linker error, %s uses different version of tool, please re-compile/assemble/rebuild ..abort.\n", processingFileName);
		exit(3);
	}
		$$=0;
	}
;

asfrom_rec:
	ASFROMHEAD ':' QUOSTR {

	if(nowMLD)
	{
		strncpy(nowMLD->asFromDir, $3, PATH_MAX-1);
	}
	$$=0;
}
;

module_rec:
	MHEAD ':' LABEL {
		nowMLD=newModuleLD($3);
		topMap.linkedModules=appendModuleLD(topMap.linkedModules,nowMLD);
		nowFp = NULL; // incase no endf
		$$=0;
	}
|	MFIN ':' LABEL {
		$$=0;
	}
|   MHEAD ':' LABEL ':' QUOSTR {
		nowMLD=newModuleLD($3);
		topMap.linkedModules=appendModuleLD(topMap.linkedModules,nowMLD);
		$$=0;
}

|  MHEAD ':' LABEL ':' QUOSTR ':' ISLIB {
		nowMLD=newModuleLD($3);
		nowMLD->isFromLib=1;
		if(nowLibFullPath!=NULL)
			strncpy(nowMLD->libPath,nowLibFullPath, PATH_MAX);
		topMap.linkedModules=appendModuleLD(topMap.linkedModules,nowMLD);
		$$=0;

}
|	MFIN ':' LABEL ':' QUOSTR {
		createDummyParam(nowMLD);//  give functions dummy!!
		$$=0;
	}
;



export_rec:
	EXPORTHEAD ':' LABEL {
		EXITIFNULLMLD
		symbol *symp=newSymbol($3);
		symp -> ldm = nowMLD;
		symp -> symIsGlobl=1;
		nowMLD->exportSym[nowMLD->exportSymNum++]=symp;
		$$=0;
	}
;
cdb_rec:
	CDBHEAD ':' QUOSTR {

	appendCDB2SYM($3);
	$$=0;
}|
	CDBFHEAD ':' NUM ':' QUOSTR {
	appendCDB2SYMn($5,$3);
	$$=0;
}

;
reference_rec:
	REFHEAD ':' LABEL {
		EXITIFNULLMLD
		symbol *symp=newSymbol($3);
		symp->ldm = nowMLD;
		symp->symIsExt = 1;
		nowMLD->refSym[nowMLD->refSymNum++]=symp;
		
		$$=0;
}
;


area_rec:
	AREAHEAD ':' LABEL ':' NUM ':' NUM ':' NUM ':' NUM  {
// if area name not found in defined modules
// we append a new one
	$$ = areaJob($3,$5,$7,$11);

}
;

label_rec: // lineno label
	LABELHEAD ':' NUM ':' LABEL ':' QUOSTR {
	$$=labelJob($3,$5,-1);
	nowArea->wDataTail->locTree=strdup($7);
	}
| LABELHEAD ':' NUM ':' LABEL ':' NUM ':' QUOSTR	{
	$$=labelJob($3,$5,$7);
	nowArea->wDataTail->locTree=strdup($9);
}
;

equ_rec: // label:lineno:expr
	EQUHEAD ':' LABEL ':' NUM ':' expr {
	$$ = equJob($3, $5,$7);
	}
;

wdata_rec: // lineno, isinstr, size, expression
WDATAHEAD ':' NUM ':' NUM ':' NUM ':' expr '^' {
		
		//if($3==307)
			//fprintf(stderr,"line 652\n");
		$$=wdataJob($3, $5,$7,$9,1,$1);
}
|
	WDATAHEAD ':' NUM ':' NUM ':' NUM ':' expr  {
		
		//if($3==307)
			//fprintf(stderr,"line 652\n");
		$$=wdataJob($3, $5,$7,$9,0,$1);
	
	}
| wdata_rec ':' NUM ':' NUM ':'  QUOSTR ':' LABEL {
	appendMacroInfo($3,$5,$7);
	nowArea->wDataTail->locTree=strdup($9);
	$$=$1;
}
| wdata_rec ':' QUOSTR {
nowArea->wDataTail->locTree=strdup($3);
}
;

org_rec: // lineno, offset
	ORGHEAD ':' NUM ':' NUM ':' QUOSTR {
	$$=orgJob($3,$5);
	nowArea->wDataTail->locTree=strdup($7);
	}
;

blank_rec: // lineno, size
	BLANKHEAD ':' NUM ':' NUM ':' QUOSTR {
	$$=blankJob($3,$5);
	nowArea->wDataTail->locTree=strdup($7);
	}
;

expr: 
	numi { 
		$$ = newEmptyExp(yylineno);
		$$ -> type = ExpIsNumber;
		$$ -> exp.number = $1;
	}
|   FSR2H numi {
	$$ = newEmptyExp(yylineno);
	$$->type=ExpIsStackOffset;
	$$->exp.number = $2;
}
|	LABEL {
		$$ = newEmptyExp(yylineno);
		$$ -> type = ExpIsSymbol;
		strncpy($$ -> exp.symbol,$1,MAXSTRSIZE0-1);
		$$ -> symHash = hash($1);
	}
|	PCNOW {
		$$ = newEmptyExp(yylineno);
		$$ -> type = ExpIsPCNow;
	}
|   '[' expr ']' {$$ = $2; $$->exp.tree.OverflowChk=1;}
|   '{' expr '}' {$$ = $2; $$->exp.tree.OverflowChk=1;$$->exp.tree.OverflowChkUnsign=1;}
|	'(' expr ')' {$$ = $2; }
|	'#' expr  {$$ = $2; }
|	'-' expr {
	$$ = newEmptyExp(yylineno);
	$$ ->type = ExpIsTree;
	$$ ->exp.tree.left = newEmptyExp(yylineno);
	$$ ->exp.tree.left -> type = ExpIsNumber;
	$$ ->exp.tree.left -> exp.number = 0;
	$$ ->exp.tree.op = '-';
	$$ ->exp.tree.right = $2;
}
|	expr '+' expr {
	$$ = newEmptyExp(yylineno);
	$$ -> type = ExpIsTree;
	$$ -> exp.tree.left = $1;
	$$ -> exp.tree.right = $3;
	$$ -> exp.tree.op = '+';
}
|	expr '-' expr {
	$$ = newEmptyExp(yylineno);
	$$ -> type = ExpIsTree;
	$$ -> exp.tree.left = $1;
	$$ -> exp.tree.right = $3;
	$$ -> exp.tree.op = '-';

}
| expr CHECKA expr {
	$$ = newEmptyExp(yylineno);
	$$ -> type = ExpIsTree;
	$$ -> exp.tree.left = $1;
	$$ -> exp.tree.right = $3;
	$$ -> exp.tree.op = 'C';
}
| expr SHIFTRIGHT expr {
	$$ = newEmptyExp(yylineno);
	$$ -> type = ExpIsTree;
	$$ -> exp.tree.left = $1;
	$$ -> exp.tree.right = $3;
	$$ -> exp.tree.op = 'A';
}
| expr SHIFTLEFT expr {
	$$ = newEmptyExp(yylineno);
	$$ -> type = ExpIsTree;
	$$ -> exp.tree.left = $1;
	$$ -> exp.tree.right = $3;
	$$ -> exp.tree.op = 'B';
}
| expr '&' expr {
	$$ = newEmptyExp(yylineno);
	$$ -> type = ExpIsTree;
	$$ -> exp.tree.left = $1;
	$$ -> exp.tree.right = $3;
	$$ -> exp.tree.op = '&';
}
| expr '|' expr {
	$$ = newEmptyExp(yylineno);
	$$ -> type = ExpIsTree;
	$$ -> exp.tree.left = $1;
	$$ -> exp.tree.right = $3;
	$$ -> exp.tree.op = '|';
}
| expr '>' expr {
	$$ = newEmptyExp(yylineno);
	$$ -> type = ExpIsTree;
	$$ -> exp.tree.left = $1;
	$$ -> exp.tree.right = $3;
	$$ -> exp.tree.op = '>';
}
| expr '<' expr {
	$$ = newEmptyExp(yylineno);
	$$ -> type = ExpIsTree;
	$$ -> exp.tree.left = $1;
	$$ -> exp.tree.right = $3;
	$$ -> exp.tree.op = '<';
}
| expr CEQ expr {
	$$ = newEmptyExp(yylineno);
	$$ -> type = ExpIsTree;
	$$ -> exp.tree.left = $1;
	$$ -> exp.tree.right = $3;
	$$ -> exp.tree.op = '=';
}
| expr CNE expr {
	$$ = newEmptyExp(yylineno);
	$$ -> type = ExpIsTree;
	$$ -> exp.tree.left = $1;
	$$ -> exp.tree.right = $3;
	$$ -> exp.tree.op = 'N';
}
| expr CGE expr {
	$$ = newEmptyExp(yylineno);
	$$ -> type = ExpIsTree;
	$$ -> exp.tree.left = $1;
	$$ -> exp.tree.right = $3;
	$$ -> exp.tree.op = 'G';
}
| expr CLE expr {
	$$ = newEmptyExp(yylineno);
	$$ -> type = ExpIsTree;
	$$ -> exp.tree.left = $1;
	$$ -> exp.tree.right = $3;
	$$ -> exp.tree.op = 'L';
}

;
	

%%

/////////////////////////////////////////////////////////////////////////////
// programs section
extern FILE *yyin;
extern FILE *yyout;
extern int yyparse(void);

extern unsigned int sw_tgt_id;
//extern int find_tgt_id(void);
extern int usePUSHL;
extern int useADDULNK;
int optRetV = 0;
int replaceOtp = 0;
char *oldOtpBinFile = NULL;
int newOtpCodeWordOffset = 0;
int addrJmpMain = -1;

void reset_counts(void)
{
    error_count = 0;
    yylineno = 1;
}
int yywrap(void)
{
    return 1;
}
void yyerror(char const *s)
{
    fprintf(stderr, "%s", s);
}

static int setAreaStartAddr(char *arg) // input parameter is "-b<seg>=addr(hex)"
{
    char *p = arg + 2;
    char *peq = strchr(arg, '=');
    int i = -1;
    *peq = '\0';
    sscanf(peq + 1, "%X", &i);
    startAssignedAreaName[startAssignedAreaNum] = strdup(p);
    startAssignedAreaAddr[startAssignedAreaNum] = i;
    startAssignedAreaNameHash[startAssignedAreaNum] = hash(p);
    startAssignedAreaNum++;
    return i;
}

void initHash(void)
{
	hashMain=hash("_main");
	hashInt=hash("c_interrupt"); // for areap!!
	hashPRINC2 = hash("_PRINC2");
	hashsetjmpid  = hash("___setjmp");
	hashHEADFUNC = hash("HEADFUNC");
	hashCCODE=hash("CCODE");
}

int main(int argc, char *argv[])
{
    int final;
    int i;
    int result;
    int optCount = 0;
    int needCheckOutOfRange = 0;
    int libLinesRead = -1;
    int again = 0;
    int mainfFound = 0;
    int lbsrCount = 0;
    int firstLoop = 1;
    int xStartAddrSave;

    memset(startAssignedAreaName, 0, sizeof(startAssignedAreaName));
    memset(startAssignedAreaAddr, 0, sizeof(startAssignedAreaAddr));
    memset(assignedAreaUsed, 0, sizeof(assignedAreaUsed));

    if (argc < 2)
    {
        fprintf(stderr,
                "usage: %s [options] asmfile [-l xxx.lib ]\n", argv[0]);
        fprintf(stderr, "%s", "\n\
\t-u update list file (gen .rst) \n\
\t-m create .map file\n\
\t-i create .intel Hex file (.ihx)\n\
\t-o create motorolla hex file (.mot)\n\
\t-y generate debug info (.cdb)\n\
\t-l xxx.lib link library file xxx.lib \n\
\t-v verbose output\n\
\t-k add search path\n\
\t-p use PUSHL instruction (default no-use) \n\
\t-no no call/jmp to rcall/rj optimize\n\
\t-nx no JXX optimize, and no MVL+RET=REL, JMP+RET=RET, CALL+RET=JMP optimize\n\
\t-nr keep un-used functions\n\
\t-nk disable addulnk for H08D instructions [FOR HY17P56]\n\
\t-nm disable MVFF/MVSF optimzations in H08D \n\
\t-em Force   MVFF/MVSF optimization in H08D \n\
\t-er enable function ret-value optimization \n\
\t\treturn value of a function related op code will be \n\
\t\tdeleted if the return value not referenced.\n\
\t-h08b  H08B series special relative branch addressing mode.\n\
\t-dorgw ..data area ORG use word count(default is byte count)\n\
\t-cm n ... common code optimization for seg len>=n (Experimental)\n\
\t-cl n ... common code optimization call (l)evel limit (default 5)\n\
\t-nf .... disable Function code rearragement (reduce far call) \n\
\t-rfa .... RAM Model FLAT (auto-insert LBSR) for H08A\n\
\t-xs ... xdata start addr for small model (hex) \n\
\t-ta ... tail address (hex) of heap data in H08D mode \n\
\t-f08a ... force H08A mode even target supports H08D \n\
\t-ramo ... allow ram over limit\n\
\t-b[AREA]=<address(hex)> ... assign start address of area (CODE only) \n\
\t-V: show version\n\
\t-replaceOtp <old otp bin> <new Code [word] Offset (hex)> ...\n\
\t\t replace old OTP (bin) with new, and genrate new ihx file\n\
");
        return 1;
    }

    for (i = 1; i < argc; i++)
    {
        if (i == (argc - 1))
        {
            final = 1;
            if (argv[i][0] == '-' && argv[i][1] == 'V') // capital!!
            {
                printf("SDLD  %s--%s\n", ASLNKVER, __DATE__);
                return 0;
            }
            if (argv[i][0] == '-' && argv[i][1] == 'b') // no capital
            {
                if (!strstr(argv[i], "="))
                {
                    fprintf(stderr, "Error, -b<seg>=<startaddress(hex)>, not valid at %s.\n", argv[i]);
                    return -__LINE__;
                }
                setAreaStartAddr(argv[i]);
                break;
            }
        }
        else
            final = 0;
        if (argv[i][0] == '-' && !final)
        {
            switch (argv[i][1])
            {
            case 'p':
                usePUSHL = 1;
                break;
            case 't':
                if (argv[i][2] == 'a')
                {
                    if (i < argc - 1)
                    {
                        xTailAddr = (int)strtol(argv[++i], NULL, 16);
                        xTailAssigned = 1;
                        if (xTailAddr < 0x80 || (xTailAddr < 0x200 && xTailAddr >= 0x180))
                        {
                            fprintf(stderr, "Error, H08D (heap) tail address of %X assigned by option -ta is not valid.\n", xTailAddr);
                            return -4;
                        }
                    }
                }
                else
                    fprintf(stderr, "Warning, unknown option %s\n", argv[i]);
                break;
            case 'b': // area begin address
                if (!strstr(argv[i], "="))
                {
                    fprintf(stderr, "Error, -b<seg>=<startaddress(hex)>, not valid at %s.\n", argv[i]);
                    return -__LINE__;
                }
                setAreaStartAddr(argv[i]);
                break;

            case 'r':
                if (strlen(argv[i]) >= 4)
                {
                    if (argv[i][2] == 'f' && argv[i][3] == 'a')
                    {
                        ramModel = RAM_MODEL_FLATA;
                    }
                    else if (argv[i][2] == 'f' && argv[i][3] == 'd')
                    {
                        ramModel = RAM_MODEL_FLATD;
                    }
                    else if (argv[i][2] == 'a' && argv[i][3] == 'm' && argv[i][4] == 'o')
                    {
                        allowRAMOVERLIMIT = 1;
                    }
                    else if (!strcmp(argv[i] + 2, "eplaceOtp"))
                    {
                        if (i >= argc - 2)
                        {
                            fprintf(stderr, "Error, -replaceOtp need 2 more parameters.\n");
                            return -__LINE__;
                        }
                        oldOtpBinFile = strdup(argv[++i]);
                        if (!sscanf(argv[++i], "0x%X", &newOtpCodeWordOffset))
                        {
                            fprintf(stderr, "Unknown parameter %s\n", argv[i]);
                            return -__LINE__;
                        }
                        replaceOtp = 1;
                        break;
                    }
                    else
                    {
                        fprintf(stderr, "Error, unknown option %s\n", argv[i]);
                        return -__LINE__;
                    }
                }
                else
                {
                    fprintf(stderr, "Error, unknown option %s\n", argv[i]);
                    return -__LINE__;
                }
                break;
            case 'c':
                if (argv[i][2] == 'm' && i < (argc - 1))
                {
                    needOptComm = atoi(argv[++i]); // common code
                }
                else if (argv[i][2] == 'l' && i < (argc - 1))
                {
                    commCallLevLimit = atoi(argv[++i]);
                }
                break;

            case 'k':
                searchPath = appendSrcLine(searchPath, newSrcLine(argv[++i], 0));
                break;
            case 'x':
                if (argv[i][2] == 's')
                {
                    if (i < argc - 1)
                    {
                        xStartAddr = (int)strtol(argv[++i], NULL, 16);
                        xStartAssigned = 1;
                        if (xStartAddr < 0x80 || (xStartAddr < 0x200 && xStartAddr >= 0x180))
                        {
                            fprintf(stderr, "Error, X(heap) start address of %X assigned by option -xs is not valid.\n", xStartAddr);
                            return -4;
                        }
                    }
                    else
                    {
                        fprintf(stderr, "%s need xdata start addr after -xs \n", argv[0]);
                        return -3;
                    }
                }
                else
                {
                    fprintf(stderr, "known option %s\n", argv[i]);
                    return -2;
                }
                break;
            case 'n':
                if (argv[i][2] == 'o')
                    needOptimize = 0;
                else if (argv[i][2] == 'x')
                    needOptimizeJxx = 0;
                else if (argv[i][2] == 'f')
                {
                    needRearrangeFunc = 0;
                }
                else if (argv[i][2] == 'm')
                {
                    mvxxDis = 1;
                }
                else if (argv[i][2] == 'r')
                {
                    keep_unused_function = 1;
                }
                else if (argv[i][2] == 'k' )
				{
					useADDULNK = 0;
				}
				else
                {
                    fprintf(stderr, "unknown option %s\n", argv[i]);
                    return 2;
                }
                break;
            case 'v':
                verbose = 1;
                break;
            case 'u':
                updateList = 1;
                break;
            case 'm':
                genMapFile = 1;
                break;
            case 'i':
                genIntelHex = 1;
                break;
            case 'y':
                genCDBFile = 1;
                break;
            case 'o':
                genMotHex = 1;
                break;
            case 'l':
                if (i < (argc - 1))
                    libBasename[libFileCount++] = argv[++i];
                else
                {
                    fprintf(stderr, "-l must have the lib name follows.\n");
                    return 2;
                }
                break;
            case 'd':
                if (!strcasecmp(argv[i], "-dorgw"))
                {
                    dataOrgByte = 0;
                    break;
                }
                else
                {
                    fprintf(stderr, "uknown option %s\n", argv[i]);
                }
                break;
            case 'f':
                if (!strcasecmp(argv[i], "-f08a"))
                {
                    f08a = 1;
                }
                else
                {
                    fprintf(stderr,
                            "unknown option '%s'\n", argv[i]);
                    return (2);
                }
                break;
            case 'e':
                if (argv[i][2] == 'm')
                {
                    mvxxForce = 1;
                }
                else if (argv[i][2] == 'r')
                {
                    optRetV = 1;
                }
                else
                {
                    fprintf(stderr,
                            "unknown option '%s'\n", argv[i]);
                    return (2);
                }
                break;
            case 'h':
                if (!strcasecmp(argv[i], "-h08b"))
                {
                    needOptimize = 0;
                    needOptimizeJxx = 0;
                    break;
                }
                break;
            default:
                fprintf(stderr,
                        "unknown option '%s'\n", argv[i]);
                return (2);
            }
        }
        else
        {
            if (!output_assigned)
            {
                outputBasename = argv[i];
                output_assigned = 1;
                if (strstr(argv[i], ".rel")) // it is a obj ==> link it only
                {
                    objBasename[objFileCount++] = argv[i];
                    input_assigned = 1;
                }
            }
            else
            {
                objBasename[objFileCount++] = argv[i];
                input_assigned = 1;
            }
        }
    }
    if (!output_assigned)
    {
        fprintf(stderr, "%s", "output file  not assigned.\n");
        return 4;
    }
    if (!input_assigned)
    {
        fprintf(stderr, "%s", "input obj file not assigned.\n");
        return 3;
    }



    //
    // if common code optimization is enabled, the mvxx optimization will be disabled
    if (needOptComm)
        mvxxDis = 1; // mvxx disabled if common code enabled


	if(replaceOtp)
		addrJmpMain=checkReplaceOtp(oldOtpBinFile,newOtpCodeWordOffset );

    // at first, read the OBJ files
    parser_init();
	// init hash values
	initHash();

    // before we parse, we promote the obj with main
    for (i = 0; i < objFileCount; i++)
    {
        FILE *mainchk;
        char bufmchk[1024];
        char *endp;

        strncpy(TempFile, objBasename[i], PATH_MAX - 5);
        mainchk = fopen(ChangeFileExt(TempFile, ".rel"), "r");
        if (mainchk == NULL)
        {
            fprintf(stderr, "obj file %s open error.\n", TempFile);
            return 3;
        }
        bufmchk[0] = '\0';
        while (fgets(bufmchk, 1023, mainchk))
        {
            if ((endp = strchr(bufmchk, '\n')) != NULL)
                *endp = '\0';
            if ((endp = strchr(bufmchk, '\r')) != NULL)
                *endp = '\0';
            if (!strcmp(bufmchk, "$EXPORT:_main"))
            {
                mainfFound = 1;
                break;
            }
        }
        fclose(mainchk);
        if (mainfFound)
            break;
    }
    if (!mainfFound)
    {
        fprintf(stderr, "Warning, main() not found.\n");
    }
    else
    {
        if (i != 0) // main not at first
        {
            char *objmainname = objBasename[i];
            objBasename[i] = objBasename[0];
            objBasename[0] = objmainname;
        }
    }

    for (i = 0; i < objFileCount; i++)
    {
        strncpy(TempFile, objBasename[i], PATH_MAX - 5);
        yyin = fopen(ChangeFileExt(TempFile, ".rel"), "r");
        processingFileName = TempFile;
        if (yyin == NULL)
        {
            fprintf(stderr, "obj file %s open error.\n", TempFile);
            return 3;
        }
        yyout = stdout;
        before_lex();

        reset_counts();
        do
        {
            result = yyparse();
        } while (!feof(yyin) && result == 0);
        fclose(yyin);
        if (result)
        {
            fprintf(stderr, "..sdld failed in %s around line %d\n", TempFile, yylineno);
            return result;
        }
        if (nowMLD->srcName != NULL)
            objSrcName[i] = nowMLD->srcName;
        else
        {
            char fname[PATH_MAX + 1];
            strncpy(fname, TempFile, PATH_MAX);
            ChangeFileExt(fname, ".asm");
            objSrcName[i] = strdup(fname);
        }
    }
    // after .rel loaded, we check if export symbol doubles
    if ((result = checkSymDouble()) != 0)
    {
        return result;
    }
    //append_area_symbols();
    result = solveRefSymbols(0); // stage 0

    if (result)
    {
        symbol *sp;
        srcLine *srcp;
        srcLine *newList = NULL;
        srcLine *newListTail = NULL;
        int round = 1;
        char namebuf[PATH_MAX];
        //char *tname;
        int libIndex = -1;

#ifndef _WIN32
        int fid = -1;
#endif

        if (libFileCount == 0)
        {
            sp = find_first_unresolved_sym();
            fprintf(stderr, "symbol %s used in %s cannot resolve, and ..", sp->name, sp->ldm->name);
            fprintf(stderr, "no LIB assigned, Process failed.\n");
            return -15;
        }
        if (libLinesRead <= 0) // read once is enough
        {
            if ((libLinesRead = readLibLines()) <= 0)
            {
                fprintf(stderr, "Linker error, read library file failed.\n");
                exit(-15);
            }
        }
        while ((sp = find_first_unresolved_sym()) != NULL)
        {
            if (verbose)
            {
                fprintf(stderr, "Now search %s.\n", sp->name);
                fflush(stderr);
            }

            if ((srcp = findSymInLibs(sp->name, &libIndex)) == NULL)
            {
                // sometimes it just have nothing
                if (strstr(sp->name, "_STK"))
                {
                    // we add EQU

                    symbol *symp = newSymbol(sp->name);
                    symp->symIsByLinker = 1;
                    topMap.linkerGeneratedSym[topMap.generatedSymNum++] =
                        symp;
                    symp->ldm = NULL;
                    symp->symIsEQU = 1;
                    symp->exp = newConstExp(0xaaaa5555, 0);
                    symp->EQULineNo = 0;
                    sp->definedPos = symp;
                }
                else
                {
                    fprintf(stderr, "Linker error: symbol %s used in %s cannot be found, process failed.\n", sp->name, sp->ldm->name);
                    return (-16);
                }
            }
            else
            {

                if (verbose)
                {
                    fprintf(stderr, "%s found.\n", sp->name);
                    fflush(stderr);
                }
                nowLibFullPath = libFullPath[libIndex];
                processingFileName = nowLibFullPath;
                // copy that list, then write to a new temp file,
                // then re-parse it
                copyPart2NewSrcLine(srcp, &newList, &newListTail);
//#ifdef _WIN32
//                tmpnam(namebuf);
//#else
//                strcpy(namebuf, "sdld2XXXXXX");
//                fid = mkstemp(namebuf);
//                close(fid);
//#endif
				yyin = tmpfile();
                writeSrcLine2Filep(yyin, newList);
				fseek(yyin,0,SEEK_SET);
                delSrcList(newList);
                //yyin = fopen(namebuf, "r");
                before_lex();

                reset_counts();
                do
                {
                    result = yyparse();
                } while (!feof(yyin) && result == 0);
                fclose(yyin);
                // remove the file

                if (result)
                {
                    fprintf(stderr, "..sdld failed in %s around line %d\n", namebuf, yylineno);
                    return result;
                }
                if ((result = checkSymDouble()) != 0)
                {
                    return result;
                }
                remove(namebuf);
                solveRefSymbols(round++);
            }
        }
        if (verbose)
            fprintf(stderr, "all reference symbol solved after add %d modules from lib.\n", round - 1);
    }

    // reset address here!!
    if (sw_tgt_id == 0)
        find_tgtid();
        // P56,P55 regression to HY08A
    if(sw_tgt_id == 0xA6A6B2CF || sw_tgt_id==0x0A5007D9)
    {
		
        hy17p56p55=1;
		useADDULNK = 0;
        //ramModel = RAM_MODEL_FLATA; // auto BSR!!!
    }
    xStartAddrSave = xStartAddr;
again:
    xStartAddr = xStartAddrSave;
    if (HY08A_getPART()->dataMemSize < 0x200 && !xStartAssigned)
        xStartAddr = 0x100; // give 0x100
    dataMaxAddr = 0x80;
    localMaxAddr = 0x80;
    xdataMaxAddr = xStartAddr;
    xlocalMaxAddr = xStartAddr;
	// re-arrange at first, not second
    /* if(ramModel == RAM_MODEL_FLATA)
    {
         mvsfmvss2mvff();
    } */
    if (needRearrangeFunc==1 && !needOptComm) // arrange twice? .. once is enough
    {
        arrangeCodeSegments(0); // arrange code segments for fewest farcall, only once
        needRearrangeFunc=0;
    }
    

    //2017 March,
    // change to multi-pass, because
    // ram will be simplified when not used.
    memset(ram_data, 0, sizeof(ram_data));
    memset(ram_assigned, 0, sizeof(ram_assigned));

    // build call tree twice!!
    if (buildCallTree(dataMaxAddr, &localMaxAddr, xdataMaxAddr, &xlocalMaxAddr) < 0)
    {
        fprintf(stderr, "build Call Tree Fail.\n");
        return -2001;
    }
    if (mainLocalStackSize + interruptLocalStackSize < 256 &&
        mainLocalStackSize + interruptLocalStackSize + 0x80 > xStartAddr)
    {
        xStartAddr = mainLocalStackSize + interruptLocalStackSize + 0x80;
        xdataMaxAddr = xStartAddr;
        xlocalMaxAddr = xStartAddr;
        //fprintf(stderr,"Warning, Heap start address move to 0x%X, because mainthread stack size is 0x%X bytes, and interrupt 0x%X bytes\n",
        //xStartAddr, mainLocalStackSize, interruptLocalStackSize);
    }
    if (errMsgSL)
    {
        showMsgSL(&errMsgSL);
    }
    memset(ram_data, 0, sizeof(ram_data));
    memset(ram_assigned, 0, sizeof(ram_assigned));

    do
    {
            // error in bank check?
        optCount = 0;
        // for H08D, jmpret2ret will cause BSR multi-path different
        // so we don't do the optimization for H08D!!
        // bench: testbsr.c when no peep optimization!!
        if (HY08A_getPART() != NULL && HY08A_getPART()->isEnhancedCore != 4)
            optCount = jmpRet2Ret();

        if (!labelLock)
        {
            optCount += remove_nouse_label();
            labelLock = 1; // only first round can remove labe!!
        }
		if(needOptimizeJxx)
			optCount += callret2jmp(0); // stage 0
        optCount += mvlret2retlw(); // 2021, unconditionally do this!!
    } while (optCount);
    if (HY08A_getPART() != NULL && HY08A_getPART()->isEnhancedCore == 4)
    {

        ramModel = RAM_MODEL_FLATD;
    }
    if (HY08A_getPART() != NULL && HY08A_getPART()->isEnhancedCore == 2)
        ramModel = RAM_MODEL_FLATA;

    // build flow before buildcalltree and ram
    addInterruptPushPull(); // it will do only once, interrupt push pull is ROM

    if (ramModel != RAM_MODEL_NORMAL || (HY08A_getPART() != NULL && HY08A_getPART()->isEnhancedCore == 4))
    {
        static int flowbuild = 0;
        if (!flowbuild)
        {
            buildFlow(); // buildflow cannot be omit,
        }
    }
// again ?
//    if (buildCallTree(dataMaxAddr, &localMaxAddr, xdataMaxAddr, &xlocalMaxAddr) < 0)
//    {
//        fprintf(stderr, "build Call Tree Fail.\n");
//        return -2001;
//    }

#if DEBUG_BSR

    if (ramModel != RAM_MODEL_NORMAL)
    {
        if (buildCallTree(0x80, &localMaxAddr, xStartAddr, &xlocalMaxAddr) < 0) // again
        {
            fprintf(stderr, "build Call Tree Fail.\n");
            return -2001;
        }
        calWDATAOffset(2); // 1 means RAM!!, 2 debug means random from localMaxAddr and xLocalMaxAddr
    }
    else
    {
        calWDATAOffset(1); // 1 means RAM!!

        if (buildCallTree(dataMaxAddr, &localMaxAddr, xdataMaxAddr, &xlocalMaxAddr) < 0) // again
        {
            fprintf(stderr, "build Call Tree Fail.\n");
            return -2001;
        }
    }
#else
    // it is possible large block at 0x200, but not used below 0x180
    calWDATAOffset(1); // 1 means RAM!!
    if( ramModel == RAM_MODEL_FLATA && (mainLocalStackSize+interruptLocalStackSize)<(0x180-middleADDR) )
    {
        if (buildCallTree(middleADDR, &localMaxAddr, xdataMaxAddr, &xlocalMaxAddr) < 0) // again
        {
            fprintf(stderr, "build Call Tree Fail.\n");
            return -2001;
        }

    }
    else 
    {
        if (buildCallTree(dataMaxAddr, &localMaxAddr, xdataMaxAddr, &xlocalMaxAddr) < 0) // again
        {
            fprintf(stderr, "build Call Tree Fail.\n");
            return -2001;
        }
    }

    if ((i = removeNoUseSTK()) != 0) // because it is ram, which must be done first
    {
        if (verbose)
            printf("%d STK optimized.\n", i);
        goto again; // it needs calculate ram again. Seems no other way
    }

#endif

    // ram fixed above,
    // try to insert LBSR here
    if (!needOptimizeJxx)
        insertNOPifMAINHEADCALL();

    if (HY08A_getPART() != NULL && HY08A_getPART()->isEnhancedCore == 4)
    {
        LDPRADDFSRFix(); // for HY08D first version we need to fix LDPR/ADFSR...PUSHL constrain
    }

    if (ramModel != RAM_MODEL_NORMAL)
    {
        lbsrCount = insertLBSR();
        if (verbose)
            printf("%d LBSR instructions inserted.\n", lbsrCount);
    }

    do
    {
        if (errMsgSL)
        {
            delSrcList(errMsgSL);
            errMsgSL = NULL;
            errMsgSLT = NULL;
        }
        memset(rom_data, 0xff, sizeof(rom_data)); // initially assign 0xff!!

        memset(rom_assigned, 0, sizeof(rom_assigned));

        foundOutOfRange = 0; // clear

        // reset address here!! ==> again!!

        need_remap = 0;

        calWDATAOffset(0);
        assignMemData(); // only ROM
        optCount = removeDummyInstructions();
        if (optCount)
            continue;

        if (foundOutOfRange)
        {
            static int oocount = 0;
            needCheckOutOfRange = 1;
            fixSwitchCaseOutOfRange();
            if (++oocount < 3)
                continue;
        }

        if (firstLoop)
        { // ret value optimization
            if (optRetV)
            {
                if (buidFlowStkxx())
                    continue;
            }

            commonCodeAna(needOptComm);
            if (needOptComm)
	        {
	    	    callret2jmp(1); // stage 1
                mvlret2retlw();
	        }
            if (needRearrangeFunc)      // arrange twice?
                arrangeCodeSegments(0); // arrange code segments for fewest farcall

            firstLoop = 0;
            continue; // remap again
        }

        if (needOptimize)
            optCount = jmpCall2RJRCALL();
        if (needOptimizeJxx ) // jxx optimize no skip for H08D, JXX need special care!!
            optCount += jxxOptimize();
        if (optCount == 0 && need_remap == 0)
        {
            again++;
        }

    } while ((need_remap || optCount > 0) || again < 4); // 4 should be enough

	// 2023 OSCCN1 check in HY18E
    //if (HY08A_getPART() != NULL && HY08A_getPART()->isEnhancedCore >= 3 && HY08A_getPART()->isEnhancedCore != 5)
	if (HY08A_getPART() != NULL && HY08A_getPART()->isEnhancedCore >= 3 )//&& HY08A_getPART()->isEnhancedCore != 5)
    {
        int n;
        if(h08cdOSCCN1CHK(HY08A_getPART()->isEnhancedCore, f08a||(HY08A_getPART()->isEnhancedCore == 5))) // see if OSCCN1 is wrong
		{
			fprintf(stderr,"Linker (SDLD.exe) operation failed.\n");
			return -__LINE__;
		}
        n=adjLBSRlineno();
        if(verbose)
        {
            fprintf(stderr,"Adjusted %d LBSR instruction's asm lineno.\n",n);
        }
    }

    rom_assigned[0] = 1; // address 0 must assign

    if (updateList)
        doUpdateList();

    if (genCDBFile)
        doGenCDBFile();

    fflush(stderr);
    fflush(stdout);
    if (needCheckOutOfRange)
    {
        if (foundOutOfRange)
        {
            fprintf(stderr, "out of range  cannot resolve. Process fail.\n");
            showMsgSL(&errMsgSL);
            return -3;
        }
    }
    result = checkSegRange();
    if (genMapFile)
        result |= doGenMapFile(); // possible ram out of range
    if (result == 0)
    {
		if(replaceOtp)
		{
			if(!mergeOldOTP())
			{
				fprintf(stderr,"merge Old OTP ROM fail.\n");
				return -__LINE__;
			}
		}

        if (genIntelHex)
            doGenIntelHex();
        if (genMotHex)
            doGenMotHex();
    }
    if (errMsgSL)
    {
        showMsgSL(&errMsgSL);
    }

    if (verbose && lbsrCount > 0)
        fprintf(stderr, "insert %d words for BSR operations.\n", lbsrCount);

    if (result)
    {
        fprintf(stderr, "Linker operation (%s) failed.\n", argv[0]);
    }
    return result;

    //return 0;
}
//---------------------------------------------------------------------------
