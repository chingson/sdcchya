#ifndef __LDTYPE_H__
#define __LDTYPE_H__
#ifdef _MSC_VER
#include "..\\sdashya2\\mytype.h"
#else
#include "../sdashya2/mytype.h"
#endif

#define MAX_ROM_SIZE (1024*1024*4)
#define MAX_RAM_SIZE 4096
#define MAX_AREAADDR_NUM 64

extern char * startAssignedAreaName[MAX_AREAADDR_NUM];
extern int startAssignedAreaAddr[MAX_AREAADDR_NUM];
extern int startAssignedAreaNum ;
extern uint64 startAssignedAreaNameHash[MAX_AREAADDR_NUM];
extern unsigned char assignedAreaUsed[MAX_AREAADDR_NUM];

#define DUMMY_CONST 0xAAAA5555

typedef struct s_resourceInfo {
	int startA;
	int endA;
	int size;
} resourceInfo;


struct sTopMap {
	area *romArea;
	area *dataArea;
	area *xdataArea;
	area *stkArea;
	symbol *linkerGeneratedSym[3072];
	int generatedSymNum;
	resourceInfo cstackInfo;
	resourceInfo heapInfo;
	resourceInfo xdataInfo;
	resourceInfo romInfo;
	moduleLD *linkedModules;
};
extern struct sTopMap topMap;
extern char *processingFileName;

char *ChangeFileExt(char *, char*);
moduleLD *newModuleLD(char *name);
moduleLD *appendModuleLD(moduleLD *h, moduleLD *n);

#define EXITIFNULLMLD {if(nowMLD==NULL) {fprintf(stderr,"MODULE empty error.\n");exit(5);}}


extern moduleLD *nowMLD;
extern area *nowArea;
extern srcLine *searchPath;

extern char outputFileName[PATH_MAX];
extern char objFileName[PATH_MAX];
//extern char libFileName[PATH_MAX];

extern char * objBasename[128]; // max 128 obj files
extern char * libBasename[128]; // max 128 obj files
extern char * objSrcName[128]; // max 128 obj files
extern char * outputBasename; // max 128 obj files
extern char * libFullPath[128]; // max 128 libraries
extern srcLine *libLines[128];
extern srcLine *libLineTails[128];

extern char * nowLibFullPath;
extern char nowRelFullPath[PATH_MAX];
extern unsigned char ram_data[MAX_RAM_SIZE];
extern unsigned char rom_data[MAX_ROM_SIZE];
extern unsigned char rom_assigned[MAX_ROM_SIZE];
extern unsigned char ram_assigned[MAX_RAM_SIZE];


extern int objFileCount ;
extern int libFileCount ;
extern int input_assigned ;
extern int output_assigned ;
extern int foundOutOfRange;
extern wData *OutOfRangeWdp[1024];
extern int dataMaxAddr;
extern int localMaxAddr;
extern int xdataMaxAddr;
extern int xlocalMaxAddr;
extern int f08a;

extern int xTailAddr;
extern int xTailAssigned;
//extern int swTgtId;


extern int verbose;
extern int keep_unused_function;
extern funcRecord *nowFp;


area *searchAreaLD(char *name, regionType region, int isABS, int isOVL, int skipchk);
funcRecord * newEmptyFuncRecord(void);
int appendAreaToTopMap(area *ap);
area *newLDArea(char *name, int isABS, int isOVL, regionType region);
symbol *findSymInModuleLD(char *name, moduleLD *m, int sinref, int noErr);
int wdataJob(int, int, int, expression *, int, int highbyte);
int wdataMacroJob(int macroid, int macroLevel, int lineno, char *macroname, dataItem *paras);
int replaceExpLineno(expression *exp, int newlineno);
int labelJob(int lineno, char *label, int absaddr);
int equJob(char *label, int lineno, expression *exp);
int orgJob(int lineno, int offset);
int blankJob(int lineno, int size);
int areaJob(char *name, int region, int abs, int ovl);

int commonCodeAna(int minWNum);
int addInterruptPushPull(void);

int fixSwitchCaseOutOfRange(void);


int checkSymDouble(void); // as name, export symbol double
int removeNoUseSTK(void); // if stk is not used, we remove it
int solveRefSymbols(int stage); // return how many symbol not solved
symbol *findExportSym(uint64 hashid, moduleLD *excludeMLD);
int append_area_symbols(void);
symbol *find_first_unresolved_sym(void);
int readLibLines(void);

srcLine *findSymInLibs(char*symName, int *libindex);
int getExpValue(expression *exp, moduleLD *m, int lev, int romByteOffset, int div2RomSym, int dwfsr2offset, funcRecord *, wData *);
int genRstFileO(char* srcFileName, char *listFName, int do_out); // if do_out==0, just recalculate the address and construct the sources

int calWDATAOffset(int ramrom);
int assignMemData(void);
// linking time optimization
int jmpCall2RJRCALL(void);
int remove_nouse_label(void);
//int fsrOpt(void); // mvl fsrxl mvl fsrxh => ldpr
int callret2jmp(int stage);
int jmpRet2Ret(void);
int jxxOptimize(void);
// 2018 insert NOP if main first instr is CALL
int insertNOPifMAINHEADCALL(void);

int doUpdateList(void);
int doGenMapFile(void);
int doGenIntelHex(void);
int doGenMotHex(void);
int hexout(FILE *fhex, int byte, int memory_location, int end);
symbol *findExportSymInMld(uint64 hashid, moduleLD *mdp);
int createDummyParam(moduleLD *mdp);
int removeDummyInstructions(void);
int append_symbols_of_an_area(area *ap);
int doGenCDBFile(void);

int buildCallTree(int startA, int *finalA, int xstartA, int *xfinalA); // return <0 means fail

int checkSegRange(void);
int mvlret2retlw(void);
void arrangeCodeSegments(int includeCommon);
int appendMacroInfo(int macroid, int macroLevel, char *macroline);
int findMaxLocalStack(funcRecord *fp, int level, int prevSp, int *isRecursive);

int sortAddrAsssignedArea(area **alist);

extern unsigned int sw_tgt_id;  // sw target id
extern int needOptComm; // set 1 need try to optimize common code 
extern int commCallLevLimit;

macrot *macroByLine(char*);
int h08cdOSCCN1CHK(int enhanced, int h08amode); // 2018 JUNE give user OSCCN1 warning
// 2022 adj lbsr to avoid HY17M ice bug
int adjLBSRlineno(void);


// insert record
typedef struct insertMacRecs {
	char *macroName;
	char *locTree;
} insertMacRec;

extern macrot * defined_macros[4096];
extern moduleLD * macros_mod[4096];
extern int defined_macro_num ;

extern insertMacRec * macInsArray[4096];
extern moduleLD *macInsMod[4096];
extern int macInsNum ;

extern int ramModel;
#define RAM_MODEL_NORMAL 0
#define RAM_MODEL_FLATA 1
#define RAM_MODEL_FLATD 2
// FOR H08D ... flat model, (no XDATA)
void buildFlow(void); // wDATAh has a next wdps-in-flow , both direction, to check the BSR
// 2021 try to optimize more when RET no use
int buidFlowStkxx(void);

//void calculateBSR(int *bankinglist); // after RAM address is fixed, we can define BSR

int insertLBSR(void); // insert required LBSR
int mvssmvsf2mvff(void);
void appendCDB2SYM(char *cdbrec);
void appendCDB2SYMn(char *cdbrec, int n);
int LDPRADDFSRFix(void);
int checkReplaceOtp(char * oldOtpBinFile, int newOtpCodeWordOffset); // return is the addr to jump main
//void calculateBSR(void);
// ok we have global var
extern int mainLocalStackSize;
extern int interruptLocalStackSize;
extern int middleADDR;
extern int mvxxDis;
extern int mvxxForce;
extern int allowRAMOVERLIMIT;



 extern int replaceOtp ;
 extern char *oldOtpBinFile ;
extern int newOtpCodeWordOffset ;
extern int addrJmpMain ;
int mergeOldOTP(void);

extern unsigned char oldBinFile[16384];  // as large 8K words


// in-out condition for optimization
#define LNK_COND_W 1
#define LNK_COND_F 2
#define LNK_COND_C 4
#define LNK_COND_Z 8
#define LNK_COND_OV 0x10
#define LNK_COND_DC 0x20
#define LNK_COND_FSR 0x40
#define LNK_COND_STK00 0x80
#define LNK_COND_STK01 0x100
#define LNK_COND_STK02 0x200
#define LNK_COND_STK03 0x400
#define LNK_COND_STK04 0x800
#define LNK_COND_STK05 0x1000
#define LNK_COND_STK06 0x2000
#define LNK_COND_TBLR 0x4000
#define LNK_COND_RET 0x8000
#define LNK_COND_N 0x10000
// branch means 2 places to go
#define LNK_COND_BRANCH 0x20000 
// call means W/STKXX will be destroyed, but it must be call func
#define LNK_COND_CALL 0x40000
// common code reuse call is different
#define LNK_COND_CALL_COMMON 0x80000
#define LNK_COND_CDCNOVZ (LNK_COND_C|LNK_COND_DC|LNK_COND_N|LNK_COND_OV|LNK_COND_Z)
#define LNK_COND_NZ (LNK_COND_N|LNK_COND_Z)
#define LNK_COND_CNZ (LNK_COND_C|LNK_COND_N|LNK_COND_Z)
#define LNK_COND_CNOVZ (LNK_COND_C|LNK_COND_N|LNK_COND_OV|LNK_COND_Z)



#endif
