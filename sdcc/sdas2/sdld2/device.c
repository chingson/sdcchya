#include<stdlib.h>
#include<string.h>
#include<ctype.h>
#include<limits.h>
#include "device.h"
#include "ldtype.h"
#include "../cityhash-c-master/city.h"

/*
 * Imports
 */

#define MAX_HYALIST 200
static HYA_device *Hycons[MAX_HYALIST];
static HYA_device *hycon = NULL;
static int num_of_supported_HYA = 0;
static int maxRAMaddress = 0;

#define DEVICE_FILE_NAME "hy08adevices.txt"
#define HY08A_STRING_LEN 256
#define SPLIT_WORDS_MAX 16

/* Keep track of whether we found an assignment to the __config words. */
static int HY08A_hasSetConfigWord = 0;
static unsigned int config_word[MAX_NUM_CONFIGS];
static memRange *rangeRAM = NULL;


/* parse a value from the configuration file */
static int
parse_config_value (char *str)
{
    if (str[strlen (str) - 1] == 'K')
        return atoi (str) * 1024;        /* like "1K" */

    else if (STRNCASECMP (str, "0x", 2) == 0)
        return strtol (str+2, NULL, 16); /* like "0x400" */

    else
        return atoi (str);               /* like "1024" */
}


/* split a line into words */
static int
split_words (char **result_word, char *str)
{
    static const char delim[] = " \f\n\r\t\v,";
    char *token;
    int num_words;

    /* release previously allocated words */
    for (num_words = 0; num_words < SPLIT_WORDS_MAX; num_words++)
    {
        if (result_word[num_words])
        {
            free (result_word[num_words]);
            result_word[num_words] = NULL;
        } // if
    } // for

    /* split line */
    token = strtok (str, delim);
    num_words = 0;
    while (token && (num_words < SPLIT_WORDS_MAX))
    {
        result_word[num_words] = Safe_strdup (token);
        num_words++;
        token = strtok (NULL, delim);
    } // while

    return num_words;
}


/* remove annoying prefixes from the processor name */
static char *
sanitise_processor_name (char *name)
{
    char *proc_pos = name;

    if (name == NULL)
        return NULL;

//  if (STRNCASECMP (proc_pos, "hycon", 3) == 0)
    //  proc_pos += 3;

    else if (tolower (*proc_pos) == 'p')
        proc_pos += 1;

    return proc_pos;
}


/* create a structure for a hycon processor */
static HYA_device *
create_hycona (char *hya_name, int maxram, int bankmsk, int confsiz,
               int config[MAX_NUM_CONFIGS], int program, int data, int eeprom,
               int io, int is_enhanced, int pcstack)
{
    HYA_device *new_hycon;
    char *simple_hya_name = sanitise_processor_name (hya_name);

    new_hycon = Safe_calloc (1, sizeof (HYA_device));
    new_hycon->name = Safe_strdup (simple_hya_name);

    new_hycon->defMaxRAMaddrs = maxram;
    new_hycon->bankMask = bankmsk;
    new_hycon->num_configs = confsiz;
    memcpy(new_hycon->config, config, MAX_NUM_CONFIGS * sizeof(int));

    new_hycon->programMemSize = program;
    new_hycon->dataMemSize = data;
    new_hycon->eepromMemSize = eeprom;
    new_hycon->ioPins = io;
    new_hycon->isEnhancedCore = is_enhanced;

    new_hycon->ram = rangeRAM;
	new_hycon->pcStack = pcstack;

    Hycons[num_of_supported_HYA] = new_hycon;
    num_of_supported_HYA++;

    return new_hycon;
}


/* mark some registers as being duplicated across banks */
static void
register_map (int num_words, char **word)
{
    memRange *r;
    int pcount;

    if (num_words < 3)
    {
        fprintf (stderr, "WARNING: not enough values in %s regmap directive\n",
                 DEVICE_FILE_NAME);
        return;
    } // if

    for (pcount = 2; pcount < num_words; pcount++)
    {
        r = Safe_calloc (1, sizeof (memRange));

        r->start_address = parse_config_value (word[pcount]);
        r->end_address = parse_config_value (word[pcount]);
        r->alias = parse_config_value (word[1]);
        r->bank = (r->start_address >> 7) & 3;
        // add memRange to device entry for future lookup (sharebanks)
        r->next = rangeRAM;
        rangeRAM = r;
    } // for
}


/* define ram areas - may be duplicated across banks */
static void
ram_map (int num_words, char **word)
{
    memRange *r;

    if (num_words < 4)
    {
        fprintf (stderr, "WARNING: not enough values in %s memmap directive\n",
                 DEVICE_FILE_NAME);
        return;
    } // if

    r = Safe_calloc (1, sizeof (memRange));
    //fprintf (stderr, "%s: %s %s %s\n", __FUNCTION__, word[1], word[2], word[3]);

    r->start_address = parse_config_value (word[1]);
    r->end_address = parse_config_value (word[2]);
    r->alias = parse_config_value (word[3]);
    r->bank = (r->start_address >> 7) & 3;

    // add memRange to device entry for future lookup (sharebanks)
    r->next = rangeRAM;
    rangeRAM = r;
}

static void
setMaxRAM (int size)
{
    maxRAMaddress = size;

    if (maxRAMaddress < 0)
    {
        fprintf (stderr, "invalid maxram 0x%x setting in %s\n",
                 maxRAMaddress, DEVICE_FILE_NAME);
        return;
    } // if
}

/* read the file with all the HY08A definitions and pick out the definition
 * for a processor if specified. if hya_name is NULL reads everything */
static HYA_device *
find_device (char *hya_name, unsigned int id)
{
    FILE *hya_file;
    char hya_buf[HY08A_STRING_LEN];
    int found_processor = FALSE;
    int done = FALSE;
    char **processor_name;
    int num_processor_names = 0;
    int hya_maxram = 0;
    int hya_bankmsk = 0;
    int hya_confsiz = 0;
    int hya_config[MAX_NUM_CONFIGS];
    int hya_program = 0;
	int hya_pcstack = 6;
    int hya_data = 0;
    int hya_eeprom = 0;
    int hya_io = 0;
    int hya_is_enhanced = 0;
    char *simple_hya_name;
//    char *dir;
//    char filename[512];
    int len = 512;
    char **hya_word;
    int num_hya_words;
    int wcount;
	char *sdcchome;
#ifdef _WIN32
#ifndef PATH_MAX
#define PATH_MAX _MAX_PATH
#endif
#endif
	char filePath[PATH_MAX];
    int i;

    hya_word = Safe_calloc (sizeof (char *), SPLIT_WORDS_MAX);
    processor_name = Safe_calloc (sizeof (char *), SPLIT_WORDS_MAX);

    for (i = 0; i < MAX_NUM_CONFIGS; i++)
    {
        config_word[i] = -1;
        hya_config[i] = -1;
    } // for

    /* allow abbreviations of the form "f877" - convert to "16f877" */
    simple_hya_name = sanitise_processor_name (hya_name);
    num_of_supported_HYA = 0;

    /* open the list file */
    /* first scan all include directories */
    hya_file = NULL;

	// env is the dir

	sdcchome = getenv("SDCC_HOME");
	if (sdcchome == NULL)
	{
		fprintf(stderr, "Warning, SDCC_HOME not found, memory range of %s will not be checked.", hya_name);
		free(hya_word);
		free(processor_name);
		return NULL;
	}
	strcpy(filePath, sdcchome);
#ifdef _WIN32
	strcat(filePath, "\\include\\HY08A\\hy08adevices.txt");
#else
	strcat(filePath, "/include/HY08A/hy08adevices.txt");
#endif
	

    ////fprintf (stderr, "%s: searching %s\n", __FUNCTION__, DEVICE_FILE_NAME);
    //for (dir = setFirstItem (userIncDirsSet);
    //        !hya_file && dir;
    //        dir = setNextItem (userIncDirsSet))
    //{
    //    //fprintf (stderr, "searching1 %s\n", dir);
    //    SNPRINTF (&filename[0], len, "%s%s", dir,
    //              DIR_SEPARATOR_STRING DEVICE_FILE_NAME);
    //    hya_file = fopen (filename, "rt");
    //    if (hya_file) break;
    //} // for

    //for (dir = setFirstItem (includeDirsSet);
    //        !hya_file && dir;
    //        dir = setNextItem (includeDirsSet))
    //{
    //    //fprintf (stderr, "searching2 %s\n", dir);
    //    SNPRINTF (&filename[0], len, "%s%s", dir,
    //              DIR_SEPARATOR_STRING DEVICE_FILE_NAME);
        hya_file = fopen (filePath, "rt");
        if (!hya_file) return NULL;
    

   

    /* read line by line */
    hya_buf[sizeof (hya_buf)-1] = '\0';
    while (fgets (hya_buf, sizeof (hya_buf)-1, hya_file) != NULL && !done)
    {
        /* strip comments */
        {
            char *comment = strchr (hya_buf, '#');
            if (comment)
                *comment = 0;
        }

        /* split into fields */
        num_hya_words = split_words (hya_word, hya_buf);

        /* ignore comment / empty lines */
        if (num_hya_words > 0)
        {

            if (STRCASECMP (hya_word[0], "processor") == 0)
            {
                if (hya_name == NULL)
                {
                    int dcount;

                    /* this is the mode where we read all the processors in - store the names for now */
                    if (num_processor_names > 0)
                    {
                        /* store away all the previous processor definitions */
                        for (dcount = 1; dcount < num_processor_names; dcount++)
                        {
                            create_hycona (processor_name[dcount], hya_maxram,
                                           hya_bankmsk, hya_confsiz, hya_config,
                                           hya_program, hya_data, hya_eeprom,
                                           hya_io, hya_is_enhanced, hya_pcstack);
                        } // for
                    } // if

                    /* copy processor names */
                    num_processor_names = num_hya_words;
                    for (dcount = 1; dcount < num_processor_names; dcount++)
                    {
                        processor_name[dcount] = hya_word[dcount];
                        hya_word[dcount] = NULL;
                    } // for
                } // if
                else
                {
                    /* if we've just completed reading a processor definition stop now */
                    if (found_processor)
                        done = TRUE;
                    else
                    {
                        /* check if this processor name is a match */
                        for (wcount = 1; wcount < num_hya_words; wcount++)
                        {
                            /* skip uninteresting prefixes */
                            char *found_name = sanitise_processor_name (hya_word[wcount]);

							if (STRCASECMP(found_name, simple_hya_name) == 0)
								found_processor = TRUE;
							else
							{
								
								unsigned long long tid = CityHash64(found_name, strlen(found_name));
								//if (!strcmp(found_name, "HY17M24"))
									//fprintf(stderr, "M24\n");
								if ((tid & 0xffffffffULL) == id)
								{
									found_processor = TRUE;
									strncpy(hya_name, found_name, 16);
								}
							}
                        } // for
                    } // if
                } // if
            } // if
            else
            {
                if (found_processor || hya_name == NULL)
                {
                    /* only parse a processor section if we've found the one we want */
                    if (STRCASECMP (hya_word[0], "maxram") == 0 && num_hya_words > 1)
                    {
                        hya_maxram = parse_config_value (hya_word[1]);
                        setMaxRAM (hya_maxram);
                    } // if

                    else if (STRCASECMP (hya_word[0], "bankmsk") == 0 && num_hya_words > 1)
                        hya_bankmsk = parse_config_value (hya_word[1]);

                    else if (STRCASECMP (hya_word[0], "config") == 0 && num_hya_words > 1)
                    {
                        hya_confsiz = 0;
                        for (i = 1; (i < num_hya_words) && (i < MAX_NUM_CONFIGS + 1); i++)
                        {
                            hya_config[i - 1] = parse_config_value (hya_word[i]);
                            hya_confsiz++;
                        } // for
                    }

                    else if (STRCASECMP (hya_word[0], "program") == 0 && num_hya_words > 1)
                        hya_program = parse_config_value (hya_word[1]);

                    else if (STRCASECMP (hya_word[0], "data") == 0 && num_hya_words > 1)
                        hya_data = parse_config_value (hya_word[1]);

                    else if (STRCASECMP (hya_word[0], "eeprom") == 0 && num_hya_words > 1)
                        hya_eeprom = parse_config_value (hya_word[1]);

                    else if (STRCASECMP (hya_word[0], "enhanced") == 0 && num_hya_words > 1)
                        hya_is_enhanced = parse_config_value (hya_word[1]);

                    else if (STRCASECMP (hya_word[0], "io") == 0 && num_hya_words > 1)
                        hya_io = parse_config_value (hya_word[1]);

					else if (STRCASECMP(hya_word[0], "pc_stack") == 0 && num_hya_words > 1)
					{
						hya_pcstack = parse_config_value(hya_word[1]);
					}

                    else if (STRCASECMP (hya_word[0], "regmap") == 0 && num_hya_words > 2)
                    {
                        if (found_processor)
                            register_map (num_hya_words, hya_word);
                    } // if

                    else if (STRCASECMP (hya_word[0], "memmap") == 0 && num_hya_words > 2)
                    {
                        if (found_processor)
                            ram_map (num_hya_words, hya_word);
                    } // if

                    else
                    {
                        fprintf (stderr, "WARNING: %s: bad syntax `%s'\n",
                                 DEVICE_FILE_NAME, hya_word[0]);
                    } // if
                } // if
            } // if
        } // if
    } // while

    fclose (hya_file);

    split_words (hya_word, NULL);
    free (hya_word);

    /* if we're in read-the-lot mode then create the final processor definition */
    if (hya_name == NULL)
    {
        if (num_processor_names > 0)
        {
            /* store away all the previous processor definitions */
            int dcount;

            for (dcount = 1; dcount < num_processor_names; dcount++)
            {
                create_hycona (processor_name[dcount], hya_maxram, hya_bankmsk,
                               hya_confsiz, hya_config, hya_program, hya_data,
                               hya_eeprom, hya_io, hya_is_enhanced, hya_pcstack);
            } // for
        } // if
    } // if
    else
    {
        /* in search mode */
        if (found_processor)
        {
            split_words (processor_name, NULL);
            free (processor_name);

            /* create a new hycon entry */
            return create_hycona (hya_name, hya_maxram, hya_bankmsk,
                                  hya_confsiz, hya_config, hya_program,
                                  hya_data, hya_eeprom, hya_io, hya_is_enhanced, hya_pcstack);
        } // if
    } // if

    split_words (processor_name, NULL);
    free (processor_name);

    return NULL;
}

/*-----------------------------------------------------------------*
 *  void list_valid_hycon(int ncols, int list_alias)
 *
 * Print out a formatted list of valid devices
 *
 * ncols - number of columns in the list.
 *
 * list_alias - if non-zero, print all of the supported aliases
 *              for a device (e.g. F84, 16F84, etc...)
 *-----------------------------------------------------------------*/
static void
list_valid_hycon(int ncols)
{
    int col=0,longest;
    int i,k,l;

    if (num_of_supported_HYA == 0)
        find_device(NULL,0);          /* load all the definitions */

    /* decrement the column number if it's greater than zero */
    ncols = (ncols > 1) ? ncols-1 : 4;

    /* Find the device with the longest name */
    for(i=0,longest=0; i<num_of_supported_HYA; i++) {
        k = strlen(Hycons[i]->name);
        if(k>longest)
            longest = k;
    }

#if 1
    /* heading */
    fprintf(stderr, "\nHY08A processors and their characteristics:\n\n");
    fprintf(stderr, " processor");
    for(k=0; k<longest-1; k++)
        fputc(' ',stderr);
    fprintf(stderr, "program     RAM      EEPROM    I/O\n");
    fprintf(stderr, "-----------------------------------------------------\n");

    for(i=0;  i < num_of_supported_HYA; i++) {
        fprintf(stderr,"  %s", Hycons[i]->name);
        l = longest + 2 - strlen(Hycons[i]->name);
        for(k=0; k<l; k++)
            fputc(' ',stderr);

        fprintf(stderr, "     ");
        if (Hycons[i]->programMemSize % 1024 == 0)
            fprintf(stderr, "%4dK", Hycons[i]->programMemSize / 1024);
        else
            fprintf(stderr, "%5d", Hycons[i]->programMemSize);

        fprintf(stderr, "     %5d     %5d     %4d\n",
                Hycons[i]->dataMemSize, Hycons[i]->eepromMemSize, Hycons[i]->ioPins);
    }

    col = 0;

    fprintf(stderr, "\nHY08A processors supported:\n");
    for(i=0;  i < num_of_supported_HYA; i++) {

        fprintf(stderr,"%s", Hycons[i]->name);
        if(col<ncols) {
            l = longest + 2 - strlen(Hycons[i]->name);
            for(k=0; k<l; k++)
                fputc(' ',stderr);

            col++;

        } else {
            fputc('\n',stderr);
            col = 0;
        }

    }
#endif
    if(col != 0)
        fputc('\n',stderr);
}

/*-----------------------------------------------------------------*
*
*-----------------------------------------------------------------*/
HYA_device *
init_hya (char *hya_type, int tgtid)
{
    //char long_name[HY08A_STRING_LEN];

    if ((hya_type==NULL || strlen(hya_type)==0)  && tgtid==0)
    {
        fprintf(stderr, "No processor has been specified (use -pPROCESSOR_NAME)\n");
        list_valid_hycon(7);
        return NULL;

    }

    hycon = find_device(hya_type,tgtid); // use both stri
	


    if (!hycon)
    {
        fprintf(stderr, "No processor has been specified (use -pPROCESSOR_NAME)\n");
        list_valid_hycon(7);


    }
	if (f08a) // force H08A!!
		hycon->isEnhancedCore = 0;
    return hycon;
}

/*-----------------------------------------------------------------*
*
*-----------------------------------------------------------------*/
int hyaIsInitialized(void)
{
    if(hycon && maxRAMaddress > 0)
        return 1;

    return 0;

}

/*-----------------------------------------------------------------*
*  char *processor_base_name(void) - Include file is derived from this.
*-----------------------------------------------------------------*/
char *processor_base_name(void)
{

    if(!hycon)
        return NULL;

    return hycon->name;
}

int IS_CONFIG_ADDRESS(int address)
{
    int i;

    for (i = 0; i < hycon->num_configs; i++)
    {
        if ((-1 != address) && (address == hycon->config[i]))
        {
            /* address is used for config words */
            return (1);
        } // if
    } // for

    return (0);
}

/*-----------------------------------------------------------------*
 *  void HY08A_assignConfigWordValue(int address, int value)
 *
 *
 *-----------------------------------------------------------------*/

void
HY08A_assignConfigWordValue(int address, int value)
{
    int i;
    for (i = 0; i < hycon->num_configs; i++)
    {
        if ((-1 != address) && (address == hycon->config[i]))
        {
            config_word[i] = value;
            HY08A_hasSetConfigWord = 1;
        } // if
    } // for
}

/*-----------------------------------------------------------------*
 * int HY08A_emitConfigWord (FILE * vFile)
 *
 * Emit the __config directives iff we found a previous assignment
 * to the config word.
 *-----------------------------------------------------------------*/
//int
//HY08A_emitConfigWord (FILE * vFile)
//{
//    int i;
//
//    if (HY08A_hasSetConfigWord)
//    {
//        fprintf (vFile, "%s", iComments2);
//        fprintf (vFile, "; config word(s)\n");
//        fprintf (vFile, "%s", iComments2);
//        if (hycon->num_configs > 1)
//        {
//            for (i = 0; i < hycon->num_configs; i++)
//            {
//                if (-1 != config_word[i])
//                {
//                    fprintf (vFile, "\t__config _CONFIG%u, 0x%x\n", i + 1, config_word[i]);
//                } //if
//            } // for
//        }
//        else
//        {
//            if (-1 != config_word[0])
//            {
//                fprintf (vFile, "\t__config 0x%x\n", config_word[0]);
//            } // if
//        } // if
//
//        return 1;
//    }
//    return 0;
//}
//
///*-----------------------------------------------------------------*
// * True iff the device has memory aliased in every bank.
// * If true, low and high will be set to the low and high address
// * occupied by the (last) sharebank found.
// *-----------------------------------------------------------------*/
//static int HY08A_hasSharebank(int *low, int *high, int *size)
//{
//    memRange *r;
//
//    assert(hycon);
//    r = hycon->ram;
//
//    while (r) {
//        //fprintf (stderr, "%s: region %x..%x, bank %x, alias %x, hycon->bankmask %x, min_size %d\n",  __FUNCTION__, r->start_address, r->end_address, r->bank, r->alias, hycon->bankMask, size ? *size : 0);
//        // find sufficiently large shared region
//        if ((r->alias == hycon->bankMask)
//                && (r->end_address != r->start_address) // ignore SFRs
//                && (!size || (*size <= (r->end_address - r->start_address + 1))))
//        {
//            if (low) *low = r->start_address;
//            if (high) *high = r->end_address;
//            if (size) *size = r->end_address - r->start_address + 1;
//            return 1;
//        } // if
//        r = r->next;
//    } // while
//
//    if (low) *low = 0x0;
//    if (high) *high = 0x0;
//    if (size) *size = 0x0;
//    //fprintf (stderr, "%s: no shared bank found\n", __FUNCTION__);
//    return 0;
//}
//
///*
// * True iff the memory region [low, high] is aliased in all banks.
// */
//static int HY08A_isShared(int low, int high)
//{
//    memRange *r;
//
//    assert(hycon);
//    r = hycon->ram;
//
//    while (r) {
//        //fprintf (stderr, "%s: region %x..%x, bank %x, alias %x, hycon->bankmask %x\n", __FUNCTION__, r->start_address, r->end_address, r->bank, r->alias, hycon->bankMask);
//        if ((r->alias == hycon->bankMask) && (r->start_address <= low) && (r->end_address >= high)) {
//            return 1;
//        } // if
//        r = r->next;
//    } // while
//
//    return 0;
//}
//
///*
// * True iff all RAM is aliased in all banks (no BANKSELs required except for
// * SFRs).
// */
//int HY08A_allRAMShared(void)
//{
//    memRange *r;
//
//    assert(hycon);
//    r = hycon->ram;
//
//    while (r) {
//        if (r->alias != hycon->bankMask) return 0;
//        r = r->next;
//    } // while
//
//    return 1;
//}
//

/*
 * True iff the pseudo stack is a sharebank --> let linker place it.
 * [low, high] denotes a size byte long block of (shared or banked)
 * memory to be used.
 */
extern int max_rom_addr;
//int HY08A_getSharedStack(int *low, int *high, int *size, int lkloc)
//{
//    int haveShared;
//    int l, h, s;
//
//    s = options.stack_size ? options.stack_size : 0x10;
//    haveShared = HY08A_hasSharebank(&l, &h, &s);
//    if ((options.stack_loc != 0) || !haveShared)
//    {
//        // sharebank not available or not to be used
//        s = options.stack_size ? options.stack_size : 0x10;
//        l = options.stack_loc ? options.stack_loc : 0x20;
//        h = l + s - 1;
//        if (low) *low = l;
//        if (high) *high = h;
//        if (size) *size = s;
//
//		if (lkloc && size)
//		{
//			if (max_rom_addr >= 0x10000 || options.std_c99 || options.std_c11)
//				*size = 10;
//			else
//				*size = 6;
//		}
//
//        // return 1 iff [low, high] is present in all banks
//        //fprintf(stderr, "%s: low %x, high %x, size %x, shared %d\n", __FUNCTION__, l, h, s, HY08A_isShared(l, h));
//        return (HY08A_isShared(l, h));
//    } else {
//        // sharebanks available for use by the stack
//        if (options.stack_size) s = options.stack_size;
//        else if (!s || s > 16) s = 16; // limit stack to 16 bytes in SHAREBANK
//
//        // provide addresses for sharebank
//        if (low) *low = l;
//        if (high) *high = l + s - 1;
//        if (size) *size = s;
//        if (lkloc && size)
//            *size = 6;
//        //fprintf(stderr, "%s: low %x, high %x, size %x, shared 1\n", __FUNCTION__, l, h, s);
//        return 1;
//    }
//}

HYA_device * HY08A_getPART(void)
{
	if (hycon == NULL)
	{
		hycon = calloc(1, sizeof(HYA_device));
		hycon->name = strdup("UNKNOWN");
		hycon->dataMemSize = 4096;
		hycon->defMaxRAMaddrs = 256;
		hycon->maxRAMaddress = 4096;
		hycon->programMemSize = 10240;
		
	}
    return hycon;
}
