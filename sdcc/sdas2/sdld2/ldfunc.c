#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>
#include <stdint.h>
#include "ldtype.h"
#include "device.h"
#include "symtab.h"

//#define NOPUSHL 1

//#define DEBUG_DAOFF
//#define DBGCOMM
//#define DEBUGBSR


// 2017 Before Christmas 
// for common code reuse, we try to spread common use segments
// between functions.
// which should reduce the number of FAR-CALLs.
// before this modification, all common codes are placed at high address.
// which cause most of the common code segments called by FAR-CALLs.


#ifdef _MSC_VER
#include "..\sdashya2\city.h"
#else
#include "../sdashya2/city.h"
#endif



// 2021 more detailed analysis
static int opCodeInCond(int opcode16);
static int opCodeOutCond(int opcode16);

static const int stkxxconds[] = {
	LNK_COND_STK00,
	LNK_COND_STK01,
	LNK_COND_STK02,
	LNK_COND_STK03,
	LNK_COND_STK04,
	LNK_COND_STK05,
	LNK_COND_STK06
};

static int stkxxAddrArray[8] = { -1,-1,-1,-1,-1,-1,-1,-1 }; // all not assigned

// Oct 2016: For P41 series, we support re-entry by compiler (like printf)
// or by linker, detect re-entry automatically.

static int opCodeBSRRelated(int opcode16);
void insertWd(wData* insPoint, wData* newWd);
static expression* dummySymbolUsedInExp(expression *exp, moduleLD *mdp, int lev);
#define insertInstruction1(storagea,inspoint,num) storagea = newWDATA1(f->areap);\
storagea->exp = newConstExp(num, inspoint->lineno);\
insertWd(inspoint, storagea);storagea->wdIsInstruction=1;storagea->instHighByte=1


#define insertInstruction1h(storagea,inspoint,num,sym) storagea = newWDATA1(f->areap);\
storagea->exp = newTreeExp(newConstExp(num, f->firstWD->lineno), \
newTreeExp( newTreeExp( newSymbolExp(sym,f->firstWD->lineno),\
newConstExp(8,f->firstWD->lineno), 'A',f->firstWD->lineno), \
newConstExp(1,f->firstWD->lineno), '&', f->firstWD->lineno),'|',\
f->firstWD->lineno);\
insertWd(inspoint, storagea);storagea->wdIsInstruction=1;storagea->instHighByte=1

#define insertInstruction1d(storagea,inspoint,num) storagea = newWDATA1(f->areap);\
storagea->exp = newConstExp(num, f->firstWD->lineno);\
insertWd(inspoint, storagea);storagea->wdIsInstruction=1
#define insertInstruction1s(storagea,inspoint,sym) storagea = newWDATA1(f->areap);\
storagea->exp = newTreeExp(newSymbolExp(sym, f->firstWD->lineno),newConstExp(0xff,f->firstWD->lineno),'&',\
f->firstWD->lineno);\
insertWd(inspoint, storagea);storagea->wdIsInstruction=1
#define insertInstruction1exp(storagea,inspoint,exp0) storagea = newWDATA1(f->areap);\
storagea->exp = exp0;\
insertWd(inspoint, storagea);storagea->wdIsInstruction=1

#define insertInstruction1exph(storagea,inspoint,exp0) storagea = newWDATA1(f->areap);\
storagea->exp = exp0;\
insertWd(inspoint, storagea);storagea->wdIsInstruction=1;storagea->instHighByte=1

#ifndef _WIN32
static char *dummy;
#endif

//#ifdef _DEBUG
//wData * wdp335a = NULL;
//wData * wdp335b = NULL;
//wData * wdp339 = NULL;
//#endif
int usePUSHL=0;
int useADDULNK = 1;

int find_tgtid(void);
static int body_has_sfr2_stack(void);
static int isWdMVL(wData *, int alsoaddl); // also ADDL!!
static int isWdMVFF0(wData *);
static int isWdMVSF0(wData *);
static int isMdMVSS0(wData *);

static int isWdMVL_NONSTK(wData *); // 
static expression *leftMostExp(expression*exp);
static int expHasShiftRight8(expression *exp);
static int isWdCLRF(wData *wd);
static int isWdSETF(wData *wd);
static int h08dOptimize(funcRecord *f);

static int isWdMVWF(wData *wd);
static int isWdMVFW(wData *wd);
static int isWdPUSHL(wData *wd);
static int isWdADDFSR2(wData *wd);// include ULNK
static int isWdRET(wData *wd, int skipLabel);
static int isWdCALL(wData *wd);
static int isWdJump(wData *wd);
static expression * getSymInExp(expression * exp);


static int isWdPRINC2(wData *wd, funcRecord *fp, int countDummy);
static int isWdPODEC2(wData *wd, funcRecord *fp);


static wData * checkIfSymReferencedByFunction(funcRecord *f, char *sym, uint64 symhash, int level);
static wData * checkIfEQUReferencedByFunction(funcRecord *f, int sfra, char *sym, uint64 symHash, int level);

static int checkIfExpRefLabel(expression *exp, uint64 hashid, int level);
static wData *  checkIfEQUReferencedByFPTR(int sfra, char *sym, uint64 symHash); //FPTR means function pointer !!
static wData *  checkIfSymReferencedByFPTR(char *sym, uint64 symhash);
static wData *  checkIfFSRReferencedByFPTR(int fsrID);
static inline wData *findPrevWdp(wData *wdp);
//#define findPrevWdp(x) (x->prev)

static expression *copyExp(expression *);

char *findSymInExp(expression *exp);

unsigned int sw_tgt_id = 0;
static int interruptPushPullDone = 0;
int partRAMMaxA = 0;
int partROMMaxA = 0;
int partEnhanced = 0;
int need_reentry = 0;
int middleADDR=0x180;

int mainLocalStackSize=0;
int interruptLocalStackSize=0;
int isMainRecursive=0;
int isInterruptRecursive=0;
extern int dataOrgByte;
symbol *stktop_sym=NULL;
int mainThreadCallLevel;
int interruptThreadCallLevel;
funcRecord *mainf = NULL;
funcRecord *interruptf = NULL;
int stktopi = 0;
// return 1 exp has symbol referenced
int need_remap = 0;
int allowRAMOVERLIMIT = 0;

// special hash words
extern uint64_t hashMain;
extern uint64_t hashInt;
extern uint64_t hashPRINC2;
extern uint64_t hashsetjmpid;
extern uint64_t hashHEADFUNC;
extern uint64_t hashCCODE;


asmIncInfo *asmIncHead;
asmIncInfo *asmIncTail;

int fdaop[] = {
	0x14,
	0x10,
	0x28,
	0x58,
	0x5c,
	0x24,
	0x30,
	0x44,
	0x34,
	0x64,
	0x38,
	0x40,
	0x3c,
	0x2c,
	0x48,
	0x54,
	0x4c,
	0x50,
	0x1c,
	0x18,
	0x60,
	0x20,
	-1 };

unsigned char oldBinFile[16384];



//#ifdef _DEBUG
//static void checkLine109(void)
//{
//	wData *wdp;
//	for (wdp = topMap.romArea->next->next->wDatas; wdp; wdp = wdp->next)
//	{
//		if (wdp->lineno == 109)
//		{
//			fprintf(stderr, "109 next lineno is %d size is %d\n", wdp->next->next->lineno,wdp->next->next->sizeOrORG);
//		}
//	}
//}
//#endif


static int isOPFDA(int highbyte) // from assembler myparser.y
{
	int chk = highbyte & 0xfc;
	int *fdap = fdaop;
	while (*fdap != -1)
	{
		if (chk == *fdap)
			return 1;
		fdap++;
	}
	return 0;
}
static int isFAOPW(int highbyte)
{
	int chk = highbyte & 0xfe;
	if (chk == 0x0a || chk == 0x0c) // setf && clrf
		return 1;
	return 0;
}
static int isFBAOPW(int highbyte)
{
	int chk = highbyte & 0xf0;
	if (chk == 0x80 || chk == 0x90)
		return 1;
	return 0;
}
static int isMVFF(int highbyte)
{
	if ((highbyte & 0xf0) == 0xd0)
		return 1;
	return 0;
}

static int exp_has_sym(expression *exp, uint64 hid)
{
	if (exp == NULL)
		return 0;
	if (exp->type == ExpIsSymbol)
	{
		if (exp->symHash == hid)
			return 1;
		return 0;
	}
	if (exp->type == ExpIsTree)
	{
		return exp_has_sym(exp->exp.tree.left, hid) | exp_has_sym(exp->exp.tree.right,hid);
	}
	return 0; // otherwise return 0;
}



int removeNoUseSTK(void) // if stk is not used, we remove it
{
	area *ap;
	wData *wdp;
	symbol *symp;
	int i;
	int j = 0;
	static int varStksaved = 0;
	uint64 hashid;
	static	char  varnames [][16] = { "STK07_SAVE", "STK07", "STK06_SAVE","WSAVE", "STK05", "STK04", "STK03", "STK02", "STK01", "STK00",
		"STK06", "STK05_SAVE", "STK04_SAVE", "STK03_SAVE", "STK02_SAVE", "STK01_SAVE", "STK00_SAVE",
		"_FSR0L_SAVE",
		"_FSR1L_SAVE",
		"_FSR2L_SAVE",
		"_FSR0H_SAVE",
		"_FSR1H_SAVE",
		"_FSR2H_SAVE",
		"_TBLPTRH_SAVE",
		"_TBLPTRL_SAVE",
		"_PCLATH_SAVE"
		"" };
	char *varname;
	if (varStksaved)
		return 0;
	varStksaved = 1; // onceonly
	
	for (i = 0; varnames[i][0] != '\0'; i++)
	{
		int found = 0;
		// check if declared
		varname = varnames[i];
		hashid = hash(varname);
		if ((symp = findExportSym(hashid, NULL)) == NULL)
			continue;
		for (ap = topMap.romArea; ap; ap = ap->next)
		{
			for (wdp = ap->wDatas; wdp; wdp = wdp->next)
			{
				if (exp_has_sym(wdp->exp, hashid))
				{
					found = 1;
					break;
				}
			}
		}
		if (!found)
		{
			wData *wdp2;
			j++;

			if (symp->labelPos == NULL)
				continue;
			wdp2 = symp->labelPos->next;
			wdp2->sizeOrORG = 0;
			
		}
	}
	return j;
}

moduleLD *newModuleLD(char *name)
{
	moduleLD *mdp;
	//char buffer[4096];
	// at first, we check if the name used
	uint64 hashid = hash(name);
	for (mdp = topMap.linkedModules; mdp;mdp=mdp->next)
	{
		if (hashid == mdp->hashcode)
		{
			fprintf(stderr, "Linker Error: file %s Module %s already used, error.\n", processingFileName, name); // this error failed directly
			exit(-10);
		}
	}

	mdp = calloc(1,sizeof(moduleLD));
	//memset((void*)mdp, 0, sizeof(moduleLD));
	strncpy(mdp->name, name, MAXSTRSIZE0-1);
	mdp->hashcode = hash(mdp->name);
	return mdp;

}

moduleLD *appendModuleLD(moduleLD *h, moduleLD *n)
{
	moduleLD *mlp;
	if (!h)
		return n;
	for (mlp = h; mlp->next; mlp = mlp->next) {}
	mlp->next = n;
	return h;
}

int appendAreaToTopMap(area *ap)
{
	switch (ap->region)
	{
	case regionCODE: topMap.romArea = appendArea(topMap.romArea, ap); break;
	case regionDATA: topMap.dataArea= appendArea(topMap.dataArea, ap); break;
	case regionXDATA: topMap.xdataArea = appendArea(topMap.xdataArea, ap); break;
	case regionSTK: topMap.stkArea = appendArea(topMap.stkArea, ap); break;
	}
	return 0;
}

area *searchAreaLD(char *name, regionType region, int isABS, int isOVL, int skipchk)
{
	uint64 nameHash = hash(name);

	area *ap;
	for (ap = topMap.romArea; ap; ap = ap->next)
	{
		if (nameHash != ap->nameHash)
			continue;
		if (skipchk) return ap;
		if  ( ap->region !=region || ap->isABS != isABS || ap->isOVL != isOVL )
		{
			fprintf(stderr, "Linker Error: in %s area %s define different, error.\n", processingFileName, name);
			exit(-1);
		}
		return ap;
	}
	for (ap = topMap.dataArea; ap; ap = ap->next)
	{
		if (nameHash != ap->nameHash)
			continue;
		if (skipchk) return ap;
		if (ap->region != region || ap->isABS != isABS || ap->isOVL != isOVL )
		{
			fprintf(stderr, "Linker Error: in %s area %s define different, error.\n", processingFileName, name);
			exit(-1);
		}
		return ap;
	}
	for (ap = topMap.xdataArea; ap; ap = ap->next)
	{
		if (nameHash != ap->nameHash)
			continue;
		if (skipchk)
			return ap;
		if (ap->region != region || ap->isABS != isABS || ap->isOVL != isOVL )
		{
			fprintf(stderr, "Linker Error: in %s area %s define different, error.\n", processingFileName, name);
			exit(-1);
		}
		return ap;
	}
	for (ap = topMap.stkArea; ap; ap = ap->next)
	{
		if (nameHash != ap->nameHash)
			continue;
		if (skipchk)
			return ap;
		if (ap->region != region || ap->isABS != isABS || ap->isOVL != isOVL)
		{
			fprintf(stderr, "Linker Error: in %s area %s define different, error.\n", processingFileName, name);
			exit(-1);
		}
		return ap;
	}

	return NULL;
}


area *newLDArea(char *name, int isABS, int isOVL, regionType region)
{
//	char buffer[256];
	//gloLocItem *glop;
	area *newa = calloc(1,sizeof(area));
	//memset(newa, 0, sizeof(area));
	strncpy(newa->name, name, MAXSTRSIZE0-1);
	newa->nameHash = hash(newa->name);
	newa->isABS = isABS;
	newa->isOVL = isOVL;
	//newa->isSTK = isSTK;
	newa->region = region;
	//snprintf(buffer, 256, "s_%s", name);
	//appendGloLocToModule(newGloLocItem(buffer, 1));
	//snprintf(buffer, 256, "l_%s", name);
	//appendGloLocToModule(newGloLocItem(buffer, 1));
	//snprintf(buffer, 256, "e_%s", name);
	//appendGloLocToModule(newGloLocItem(buffer, 1));

	return newa;
}

symbol *findSymInModuleLD(char *name, moduleLD *m, int searchRef, int noErr)
{
	uint64 hashid = hash(name);
	int i;
	if (searchRef)
	{
		for (i = 0; i < m->refSymNum; i++)
		{
			if (m->refSym[i]->hashid == hashid)
				return m->refSym[i];
		}
	}
	for (i = 0; i < m->exportSymNum; i++)
		if (m->exportSym[i]->hashid == hashid)
			return m->exportSym[i];
	for (i = 0; i < m->localSymNum; i++)
	{
		if (m->localSym[i]->hashid == hashid)  // if found in local, it is an error
		{
			if (searchRef)
				return m->localSym[i];
			else
			{
				if(!noErr)
					fprintf(stderr, "Warning, local symbol %s duplicate, at %s \n", m->localSym[i]->name, processingFileName);
				//exit(-12);
			return NULL;
			}
		}
	}

	return NULL;
}

int labelJob(int lineno, char *label, int absAddr)
{
	wData *wdp;
	symbol *symp;
	wdp = newWDATA(nowArea);
	wdp->m = nowMLD;
	wdp->wdIsLabel = 1;
	wdp->fp = nowFp; // assign FP!!
	symp = findSymInModuleLD(label, nowMLD, 0,0);
	if (symp == NULL)
	{
		symp = newSymbol(label);
		nowMLD->localSym[nowMLD->localSymNum++] = symp; // add in local
		if (nowMLD->localSymNum >= 32768)
		{
			fprintf(stderr, "Error, module %s too many local symbols.\n", nowMLD->name);
			exit(-__LINE__);
		}
	}
	symp->ldm = nowMLD;
	symp->symIsLabel = 1; // if it is export, we change it, also
	symp->labelPos = wdp;
	symp->ap = nowArea; // this is required!!
	symp->areaOffset = absAddr;


	wdp->sym = symp;
	wdp->lineno = lineno;
	if (absAddr >= 0)
		wdp->labelFixOffset = 1;
	appendTailWDATA(nowArea, wdp,0);
	return 0;
	
}
int equJob(char *label, int lineno, expression *exp)
{
	symbol *symp;
	symp = findSymInModuleLD(label, nowMLD, 0,0);
	if (symp == NULL) // if not public, it is local
	{
		symp = newSymbol(label);
		nowMLD->localSym[nowMLD->localSymNum++] = symp; // add in local
		if (nowMLD->localSymNum >= 32768)
		{
			fprintf(stderr, "Error, module %s too many local symbols.\n", nowMLD->name);
			exit(-__LINE__);
		}

	}
	symp->ldm = nowMLD;
	symp->symIsEQU = 1;
	symp->exp = exp;
	symp->EQULineNo = lineno;
	return 0;
}
int orgJob(int lineno, int offset)
{
	wData *wdp = newWDATA(nowArea);
	wdp->m = nowMLD;
	wdp->lineno = lineno;
	wdp->sizeOrORG = offset;
	wdp->wdIsOrg = 1;
	appendTailWDATA(nowArea, wdp,0);
	return 0;
}

int replaceExpLineno(expression *exp, int newlineno)
{
	if (exp == NULL)
		return -1;
	exp->lineno = newlineno;
	if (exp->type == ExpIsTree)
	{
		replaceExpLineno(exp->exp.tree.left, newlineno);
		replaceExpLineno(exp->exp.tree.right, newlineno);
	}
	return 0;
}

int appendMacroInfo(int macroid, int macroLevel, char *macroline)
{
	if (nowArea->wDataTail == NULL)
	{
		fprintf(stderr, "Linker Internal error at %s:%d .\n",__FILE__,__LINE__);
		exit(-1001);
	}
	nowArea->wDataTail->macro_id = macroid;
	nowArea->wDataTail->macroLevel = macroLevel;
	//nowArea->wDataTail->macro_line_hash = linehash;
	nowArea->wDataTail->macroSrc = strdup(macroline);
	nowArea->wDataTail->wdIsInMacro = 1;

	return 0;
}

// id, level, lineno, name
int wdataMacroJob(int macroid, int macroLevel, int lineno, char *macroname, dataItem *paras)
{
	wData *wdp = newWDATA(nowArea);

	expression *exp = (macroname==NULL)?NULL:(newSymbolExp(macroname, lineno));
	wdp->m = nowMLD;
	wdp->lineno = lineno;
	wdp->wdIsMacroMark = 1;
	wdp->macroPara = paras;
	wdp->macro_id = macroid;
	wdp->macroLevel = macroLevel;
	

	wdp->exp = exp;

	
	appendTailWDATA(nowArea, wdp,0);

	return 0;

}

int wdataJob(int lineno, int isinstr, int size, expression* exp, int needdiv2, int instruh)
{
	wData *wdp = newWDATA(nowArea);
	wdp->m = nowMLD;
	wdp->lineno = lineno;
	wdp->wdIsInstruction = isinstr;
	wdp->sizeOrORG = size;
	wdp->instHighByte = (instruh>0); // add high byte for opt sync!!
	wdp->instSkip = (instruh == 2);
	if (nowFp != NULL && nowFp->firstWD == NULL && isinstr)
		nowFp->firstWD = wdp;
	if (nowFp != NULL && wdp->instHighByte)
		nowFp->finalWD = wdp; // following order

	//if (isinstr)
	if(nowArea->region==regionCODE)
		wdp->fp = nowFp; // see where it belongs.. always give the fp in CODE, even FP is null

	replaceExpLineno(exp, lineno);
	wdp->exp = exp;

	wdp->expCodeLabelDiv2 = needdiv2;
	appendTailWDATA(nowArea, wdp, instruh);
	
	return 0;
}

int blankJob(int lineno, int size)
{
	wData *wdp = newWDATA(nowArea);
	wdp->m = nowMLD;
	wdp->lineno = lineno;
	wdp->wdIsBlank = 1;
	wdp->sizeOrORG = size;
	if (nowArea->wDataTail->wdIsLabel)
	{
		nowArea->wDataTail->sym->symSize = size;
	}
	appendTailWDATA(nowArea, wdp,0);
	
	return 0;
}
int areaJob(char *name, int region, int abs, int ovl)
{
	area *ap;
	if ((ap=searchAreaLD(name, (regionType)region, abs, ovl,0))==NULL)
	{
		ap = newLDArea(name, abs, ovl, (regionType)region);
		appendAreaToTopMap(ap);
		append_symbols_of_an_area(ap);
	}
	nowArea = ap;
	return 0;
}
static int checkSymInOtherModules(char *name, uint64 symhash, moduleLD *exclMod)
{
	moduleLD *mdp;
	int i;
	for (mdp = topMap.linkedModules; mdp; mdp = mdp->next)
	{
		if (mdp == exclMod)
			continue;
		for (i = 0; i < mdp->exportSymNum; i++)
		{
			if (mdp->exportSym[i]->hashid == symhash)
			{
				fprintf(stderr, "symbol %s export doubled in module %s and %s\n", name,
					exclMod->name, mdp->name);
				return 1;
			}
		}
	}
	return 0;
}
int checkSymDouble(void) // check only if the exported symbol doubles
{
	moduleLD *mdp;
	symbol *sp;
	int i;

	for (mdp = topMap.linkedModules; mdp; mdp = mdp->next)
	{
		for (i = 0; i < mdp->exportSymNum; i++)
		{
			sp = mdp->exportSym[i];
			if (checkSymInOtherModules(sp->name, sp->hashid, mdp))
				return 1;
		}
	}
	return 0;

}

symbol *findExportSym(uint64 hashid, moduleLD *excludeMLD)
{
	moduleLD *mdp;
	//symbol *sp;
	int i;
	for (mdp = topMap.linkedModules; mdp; mdp = mdp->next)
	{
		if (mdp == excludeMLD)
			continue;
		for (i = 0; i < mdp->exportSymNum; i++)
		{
			if (mdp->exportSym[i]->hashid == hashid)
				return mdp->exportSym[i];
		}
	}
	// finally we comes to linker generated
	for (i = 0; i < topMap.generatedSymNum; i++)
	{
		if (topMap.linkerGeneratedSym[i]->hashid == hashid)
			return topMap.linkerGeneratedSym[i];
	}
	return NULL;
}
symbol *findExportSymInMld(uint64 hashid, moduleLD *mdp)
{
	int i;
		for (i = 0; i < mdp->exportSymNum; i++)
		{
			if (mdp->exportSym[i]->hashid == hashid)
				return mdp->exportSym[i];
		}
	return NULL;
}


int solveRefSymbols(int stage) // return how many symbol not solved
{
	moduleLD *mdp;
	symbol *sp;
	wData *wdp;
	int i;
	int noFoundCount = 0;
	sdcc_symbol *refSym;
	sdcc_symbol *exportSym;

//	char buffer[4096];

	uint64 id1 = hash("a_STKTOP");
	if (!findExportSym(id1,NULL))
	{
		sp = newSymbol("a_STKTOP");
		sp->symIsByLinker = 1;
		sp->symIsEQU = 1;
		sp->symIsGlobl = 1;
		sp->exp = newConstExp(0, -1); // assign 0 first
		topMap.linkerGeneratedSym[topMap.generatedSymNum++] =
			sp;
		stktop_sym = sp;
	}

	for (mdp = topMap.linkedModules; mdp; mdp = mdp->next)
	{
		for (i = 0; i < mdp->refSymNum; i++)
		{
			//if ((sp = findExportSym(mdp->refSym[i]->hashid, mdp)) == NULL)
			if ((sp = findExportSym(mdp->refSym[i]->hashid, NULL)) == NULL)
			{
				if (verbose)
				{
					if (stage == 0) // verbose uses output directly
						fprintf(stderr, "symbol %s not found in OBJ files. Need solve in libs.\n", mdp->refSym[i]->name);
					else
						fprintf(stderr, "symbol %s not found after %d module from lib linked.\n", mdp->refSym[i]->name, stage);
				}
				//if (mdp->refSym[i]->ap->region != regionSTK) // special case, dummy para, we skip it.
				noFoundCount++;
			}
			mdp->refSym[i]->definedPos = sp; // alink to defined place
			// 2019, may
			// check type match
			refSym = NULL;
			exportSym = NULL;
			if (sp && sp->cdbRecord)
			{
				if (mdp->refSym[i]->cdbRecord)
				{
					//if (strstr(mdp->refSym[i]->cdbRecord, "Temp_Offset_Fun"))
						//fprintf(stderr, "%s\n", mdp->refSym[i]->cdbRecord);
					refSym = parseSymbol(mdp->refSym[i]->cdbRecord + 2); // skip S:
					exportSym = parseSymbol(sp->cdbRecord + 2); // skip S:
					if(!compareSymbolDecl(refSym, exportSym) )
					{
						char *buf1 = type2String(refSym, mdp->name, 0);
						char *buf2 = type2String(exportSym, (sp->ldm) ? sp->ldm->name : NULL, 1);
						fprintf(stderr, "%s\n%s\nCANNOT be linked together, please fix them and try again.\n Linker Process Failed.\n", buf2, buf1);
						exit(__LINE__);
					}
					// if function parameter not same, we drop!!

					// next time we will consider function pointers
					if (IS_FUNC(refSym->type)  && mdp->refSym[i]->funcParaNum>0 && sp->ldm ) // both are functions
					{
						int needChkPara = mdp->refSym[i]->funcParaNum - 1024;
						moduleLD *lmod = sp->ldm;
						int k;
						funcRecord *f;
						for (k = 0; k < lmod->funcNum; k++)
						{
							f = lmod->funcs[k];
							if (f->namehash == sp->hashid)
							{
								if ((needChkPara <1000 && f->declaredParamSize != needChkPara) ||
									(needChkPara == 1000 && !f->paraOnStack))
								{
									char *buf1 = type2String(refSym, mdp->name, 0);
									char *buf2 = type2String(exportSym, (sp->ldm) ? sp->ldm->name : NULL, 1);
									fprintf(stderr, "%s\n%s\nCANNOT be linked together because parameter sizes are different,\n please fix them and try again.\n Linker Process Failed.\n", buf1, buf2);
									exit(__LINE__);
								}
							}
						}
						

					}


				}
			}


			if (sp && sp->symIsLabel)// special case, sp not found, we give 00
			{
				wdp = sp->labelPos->next;
				if (wdp && wdp->wdIsBlank)
				{
					sp->symSize = wdp->sizeOrORG;
					mdp->refSym[i]->symSize = wdp->sizeOrORG;
				}
			}
			
		}
	}
	return noFoundCount;
}

symbol *find_first_unresolved_sym(void)
{
	moduleLD *mdp;
	int i;
	//int noFoundCount = 0;
	for (mdp = topMap.linkedModules; mdp; mdp = mdp->next)
	{
		if (mdp->symResolved)
			continue;
		for (i = 0; i < mdp->refSymNum; i++)
		{
			if (mdp->refSym[i]->definedPos == NULL)
			//if (mdp->refSym[i]->definedPos == NULL && mdp->refSym[i]->ap &&
			//	mdp->refSym[i]->ap->region!=regionSTK) // we skip STK region for solving syms
				return mdp->refSym[i];
		}
		mdp->symResolved = 1;
	}
	return NULL;
}



static funcRecord * findFuncByHash(uint64 hashid, moduleLD *m) // 2017 modify, find local first!!
{
	moduleLD *mdp;
	int i;
	if (m != NULL)
	{
		for (i = 0; i < m->funcNum; i++)
			if (m->funcs[i]->namehash == hashid)
				return m->funcs[i];
	}
	for (mdp = topMap.linkedModules; mdp; mdp = mdp->next)
	{
		for (i = 0; i < mdp->funcNum;i++)
			if (mdp->funcs[i]->namehash == hashid)
			{
				return mdp->funcs[i];
			}
	}
	return NULL;
}

static int simple_spread(funcRecord *fp, moduleLD *mld)
{
	dataItem *dip;
	//	unsigned long hashid;
	funcRecord *fp1;
//	symbol *sym;
	char buffer[MAXSTRSIZE0+1024];
	int i;
//	char namebuf[256];
//	int cmplen;
	fp->spreaded = 1;
	if (fp->localSymNum > 0 || fp->calledFuncNum>0 || fp->localXSymNum>0)
		return 0;
	
	for (dip = fp->callList; dip; dip = dip->next)
	{
		fp1 = findFuncByHash(dip->exp->symHash,fp->m);
		if (dip->exp->type != ExpIsSymbol)
			continue; // literal call!!
		if (fp1 == NULL) // it is inline
		{
			int found = 0;
			//fprintf(stderr, "internal error, cannot find called function %s\n", dip->exp->exp.symbol);
			// some times it just be inlined
			
			for(i=0;i<fp->inlinefunc_num;i++)
				if (dip->exp->symHash == fp->inlinefunc_name_hash[i])
				{
					found = 1;
					break;
				}
			if (!found)
			{
				snprintf(buffer, sizeof(buffer)-1, "Warning, seems inlined  function %s\n", dip->exp->exp.symbol);
				appendErrMsg(buffer);
				fprintf(stderr, "%s", buffer);
				fp->inlinefunc_name[fp->inlinefunc_num] = dip->exp->exp.symbol;
				fp->inlinefunc_name_hash[fp->inlinefunc_num] = dip->exp->symHash;
				fp->inlinefunc_num++;
			}
			//return -1;
		}else 
			fp->calledFunc[fp->calledFuncNum++] = fp1;
	}
	return 0;
	//for (dip = fp->localList; dip; dip = dip->next)
	//{
	//	sym = findSymInModuleLD(dip->exp->exp.symbol, mld, 1, 0);
	//	if (sym == NULL)
	//	{
	//		//fprintf(stderr, "internal error, cannot find local symbol %s\n", dip->exp->exp.symbol);
	//		snprintf(buffer, 4096, "internal error, cannot find local symbol %s\n", dip->exp->exp.symbol);
	//		appendErrMsg(buffer);
	//		fprintf(stderr, buffer);
	//		return -1;
	//	}
	//	sym->fpHasthis = fp; // I have the local, used when assign, important for P41/s41!!
	//	if (dip->exp->ExpSymIsX)
	//		fp->localXSym[fp->localXSymNum++] = sym;
	//	else
	//		fp->localSym[fp->localSymNum++] = sym;
	//	sym->symIsFuncLocal = 1;
	//	// check if para
	//	//if (strncmp(sym->name, namebuf, cmplen))
	//	//{
	//	//	char *numcp = sym->name + strlen(sym->name) - 2;
	//	//	sym->symIsFuncLocalPara = 1;
	//	//	// the format is ... STKXX, we have the offset
	//	//	// stk00 is the left most, -0
	//	//	sym->fsr2origOffset = atoi(numcp); // default is 0,1,2,...
	//	//	//sym->symIsByFSR2 = 1;
	//	//}
	//	

	//	
	//}
}


static int spreadFuncRecord(funcRecord *fp, moduleLD *mld) // ok return 0,
{
	dataItem *dip;
	//	unsigned long hashid;
	funcRecord *fp1;
	symbol *sym;
	char buffer[MAXSTRSIZE1*2+100];
	char namebuf[5120];
	int cmplen;

	if (fp->spread2)
		return 0;
	fp->spread2 = 1;

	snprintf(namebuf, sizeof(namebuf)-1, "%s_STK", fp->name);
	cmplen = strlen(namebuf);
	// second pass is OK
	//if (fp->localSymNum > 0 || fp->calledFuncNum>0 || fp->localXSymNum>0)
	//	return 0;

	if (fp->calledFuncNum == 0)
	{
		for (dip = fp->callList; dip; dip = dip->next)
		{
			if (dip->exp->type != ExpIsSymbol)
				continue; // skip literal call
			fp1 = findFuncByHash(dip->exp->symHash,fp->m); // search local static first, then global
			if (fp1 == NULL)
			{
				//fprintf(stderr, "internal error, cannot find called function %s\n", dip->exp->exp.symbol);
				snprintf(buffer, sizeof(buffer)-1, "seems inlined called function %s\n", dip->exp->exp.symbol);
				appendErrMsg(buffer);
				fprintf(stderr,"%s", buffer);
				//return -1;
			}else 
				fp->calledFunc[fp->calledFuncNum++] = fp1;
		}
	}
	
	
	for (dip = fp->localList; dip; dip = dip->next)
	{
		//if(strstr(dip->exp->exp.symbol,"_baz3_STK00"))
		//	fprintf(stderr,"baz_stk00\n");

		sym = findSymInModuleLD(dip->exp->exp.symbol, mld, 1,0);
		
		// 2020 may, special symbol is for stack address translation
		if (sym == NULL)
		{
			if (strstr(dip->exp->exp.symbol, "__addrtmp__"))
			{
				sym = newSymbol(dip->exp->exp.symbol);
				sym->ldm = fp->m;
				sym->symSize = 1;
				sym->symNoLabel = 1; // it has no label!!
				sym->symIsLabel = 1;
				mld->localSym[mld->localSymNum++] = sym;
				if (mld->localSymNum >= 32768)
				{
					fprintf(stderr, "Error, module %s too many local symbols.\n", mld->name);
					exit(-__LINE__);
				}

			}
			else
			{
				//fprintf(stderr, "internal error, cannot find local symbol %s\n", dip->exp->exp.symbol);
				snprintf(buffer, sizeof(buffer)-1, "internal error, cannot find local symbol %s at %s:%d\n", dip->exp->exp.symbol,__FILE__,__LINE__);
				appendErrMsg(buffer);
				fprintf(stderr, "%s", buffer);
				return -1;
			}
		}
		if (dip->exp->ExpSymIsX)
		{
			fp->localXSym[fp->localXSymNum++] = sym;
			if (fp->localXSymNum >= 4096)
			{
				fprintf(stderr, "Error, function %s too many local X symbol.\n", fp->name);
				exit(-__LINE__);

			}
		}
		else
		{
			fp->localSym[fp->localSymNum++] = sym;
			if (fp->localSymNum >= 256)
			{
				fprintf(stderr, "Error, function %s too many local symbol.\n", fp->name);
				exit(-__LINE__);
			}
		}
		sym->symIsFuncLocal = 1;
		// check if para
		if ((fp->p41ReEntry||fp->h08dReEntry) && !strncmp(sym->name, namebuf, cmplen) && !(sym->symNoLabel || sym->labelPos==NULL) ) // addrtmp is local, not para
		{
			char *numcp = sym->name + strlen(sym->name) - 2;
			sym->symIsFuncLocalPara = 1;
			// the format is ... STKXX, we have the offset
			// stk00 is the left most, -0
			sym->fsr2origOffset = atoi(numcp); // default is 0,1,2,...
			sym->fpHasthis = fp;
			sym->symIsByFSR2 = 1;
			//if (!sym->symNoLabel)
			//{
			if(sym->labelPos)
			{
				sym->labelPos->next->wdBlankIsByFSR2 = 1;
				sym->labelPos->next->fsr2_stack_offset = sym->fsr2origOffset;
			}
			//}
			/*else
			{
				sym->labelPos = newWDATA(fp->areap);
				sym->labelPos->m = fp->m;
				sym->labelPos->wdIsBlank = 1;
				sym->labelPos->sizeOrORG = 1;
				sym->labelPos->next = sym->labelPos;
			}*/
		}
		else if (fp->p41ReEntry || fp->h08dReEntry)
		{
//#ifdef _DEBUG
//			if (strstr(sym->name, "10A4"))
//			{
//				fprintf(stderr, "offset of %s %s defined\n", fp->name, sym->name);
//			}
//#endif

			sym->fsr2origOffset = 0 - fp->localSymSizeExcludePara - 1;
			sym->fpHasthis = fp;
			sym->symIsByFSR2 = 1;
			if (!sym->symNoLabel && (sym->labelPos!=NULL))
			{
				sym->labelPos->next->wdBlankIsByFSR2 = 1;
				sym->labelPos->next->fsr2_stack_offset = 0 - fp->localSymSizeExcludePara - 1;
			}
			else
			{
				sym->labelPos = newWDATA(fp->areap);
				sym->labelPos->m = fp->m;
				sym->labelPos->wdIsBlank = 1;
				sym->labelPos->sizeOrORG = 1;
				//sym->labelPos->next = sym->labelPos; // 2022 oct, no need is this
			}

		}

		if (sym->symIsLabel && sym->labelPos && sym->labelPos->next && sym->labelPos->next->wdIsBlank)
		{
			sym->labelPos->next->wdBlankIsFuncLocal = 1; // mark it future defined!!
		}
		else if (sym->symIsEQU && !strncmp(sym->name, fp->name, strlen(fp->name)))
		{
			
			sym->fpHasthis = fp;

		}else if(!strstr(sym->name,"_STK") && !sym->symNoLabel) // if it is STK, it is no used para, skip automatially
		{
			//fprintf(stderr, "strange sym %s\n", sym->name);
			snprintf(buffer,MAXSTRSIZE1*2+99, "strange sym %s at func %s\n", sym->name, fp->name);
			appendErrMsg(buffer);
			fprintf(stderr, "%s", buffer);
		}
		if (dip->exp->ExpSymIsX)
			fp->localXSymSize += sym->symSize;
		else
		{
			fp->localSymSize += sym->symSize;
			if (!sym->symIsFuncLocalPara)
				fp->localSymSizeExcludePara += sym->symSize;
			else
				fp->realParaSize += sym->symSize;
		}
	}


	return 0;
	
}

// build call tree by modules' func's call list, and check 
// 1. recursive function ... not support, [error return 2]
// 2. Interrupt and main tread both use (warning), if both use, we use main thread.. [warning return 1]
// no problem return 0


//now max call is 6 depth
#define MAX_CALL_DEPTH 128
//#define WARN_CALL_DEPTH 7
static funcRecord *callDepthTable[MAX_CALL_DEPTH];

static wData * needAddLA[65536]; // the wDATA need add Linker record
static unsigned int needAddLANum = 0;
static inline void ADDLAREC(wData *x) { 
		if(needAddLANum>=65536 ) {
			fprintf(stderr, "Error, Memory corrupted at %s:%d\n", __FILE__, __LINE__);
			exit(-__LINE__);
		}
		needAddLA[needAddLANum++] = x; 
}

// insert before inspoint
static wData * insertADDFSR(wData * insPoint, int fsrID, int fsrShift)
{
	wData *wdp, *wdph, *wdp1;
	wData *preLabel = NULL;
	funcRecord *f = insPoint->fp;
	int removeRET = 0;
	if (f)
		wdph = newWDATA1(f->areap);
	else
		wdph = newWDATA1(insPoint->ap);

	// if there is a label before
	// we use that label's lineno,loctree
	wdp = findPrevWdp(insPoint);
	if (wdp && wdp->wdIsLabel && fsrID!=3) // if addulnk, we don't use prev label
	{
		preLabel = wdp;
	}
	else
	{
		preLabel = insPoint;
		
	}
	wdph->locTree = preLabel->locTree;
	wdp = wdph;
	if (f)
		wdp->m = f->firstWD->m;
	else
		wdp->m = insPoint->m;
	wdp->fp = f;
	wdp->lineno = preLabel->lineno;
	wdp->exp = newConstExp(0xC4, insPoint->lineno); // ADDFSR or ADDULNK
	wdp->wdIsInstruction = 1;
	wdp->instHighByte = 1;
	ADDLAREC(wdp);
	// new one
	if (f)
	{
		wdp->next = newWDATA1(f->areap);
		wdp->next->prev = wdp;
	}
	else
	{
		wdp->next = newWDATA1(insPoint->ap);
		wdp->next->prev = wdp;
	}
	wdp = wdp->next;
	wdp->locTree = preLabel->locTree;
	if (f)
		wdp->m = f->firstWD->m;
	else
		wdp->m = insPoint->m;
	wdp->fp = f;
	wdp->lineno = preLabel->lineno;
	if (fsrID==3) // addulnk convert
	{
		//wdp->exp = newConstExp(0xc0 | (fsrShift & 0x3f), insPoint->lineno);
		
		removeRET = 1;
	}
	
	wdp->exp = newConstExp((fsrID << 6) | (fsrShift & 0x3f), insPoint->lineno); // local size or 
		
	wdp->wdIsInstruction = 1;
	wdp->next = insPoint; //
	wdp1 = insPoint->prev;

	insPoint->prev = wdp; // 2022 oct, change to dual dir linklist
	
	ADDLAREC(insPoint);
	// search for f
	if (f && f->firstWD->ap->wDatas == insPoint)
	{
		f->firstWD->ap->wDatas = wdph;
	}
	else 
	{
		// search for prev inspoint
		/*if (f)
		{
			for (wdp1 = f->firstWD->ap->wDatas; wdp1; wdp1 = wdp1->next)
			{
				if (wdp1->next == insPoint)
					break;
			}
		}
		else
		{
			for(wdp1=insPoint->ap->wDatas;wdp1;wdp1=wdp1->next)
				if (wdp1->next == insPoint)
					break;
		}*/

		//

		if (wdp1 == NULL)
		{
			fprintf(stderr, "Linker internal error!! search fp head fail.\n");
			exit(-__LINE__);
		}
		
		wdp1->next = wdph;
		wdph->prev = wdp1;
		if (removeRET)
		{
			wdp1 = insPoint->next;
			wdp1->prev = insPoint;
			wdp->next = insPoint->next->next;
			if (wdp->next != NULL)
				wdp->next->prev = wdp;
			//f->finalWD1 = wdph;
			f->finalWD = wdph; // it should be wdph
			wdp1->next = NULL;
			wdp1->prev = NULL; // isolate it
		}
	}
	return wdph;
}

static wData * insert_plusw2(
	wData *insPoint, int plusw2, int immediate, int skipsavew, int skiprestorew)// insert before!!, not after!!
{
	wData *wdp, *wdph, *wdp1;
	funcRecord *f = insPoint->fp;
	wData *preLabel = findPrevWdp(insPoint);
	if (!preLabel || !(preLabel->wdIsLabel))
		preLabel = insPoint;

	wdph = newWDATA1(f->areap);
	wdph->locTree = preLabel->locTree;
	wdp = wdph;

	if (immediate == 2)// only addlw
	{
		wdp->m = f->firstWD->m;
		wdp->fp = f;
		wdp->lineno = preLabel->lineno;
		wdp->exp = newConstExp(0x04, insPoint->lineno); // addlw
		wdp->wdIsInstruction = 1;
		wdp->instHighByte = 1;
		ADDLAREC(wdp);


		wdp->next = newWDATA1(f->areap);
		wdp->next->prev = wdp;
		wdp = wdp->next;
		wdp->locTree = preLabel->locTree;
		wdp->m = f->firstWD->m;
		wdp->fp = f;
		wdp->lineno = preLabel->lineno;
		wdp->exp = newConstExp(plusw2, insPoint->lineno); // local size or 
		wdp->wdIsInstruction = 1;
	}
	else
	{

		if (!skipsavew)
		{
			wdp->m = f->firstWD->m;
			wdp->fp = f;
			wdp->lineno = preLabel->lineno;
			wdp->locTree = preLabel->locTree;
			wdp->exp = newConstExp(0x66, insPoint->lineno); // mvwf stk03
			wdp->wdIsInstruction = 1;
			wdp->instHighByte = 1;
			ADDLAREC(wdp);



			wdp->next = newWDATA1(f->areap);
			wdp->next->prev = wdp;

			wdp = wdp->next;
			wdp->locTree = preLabel->locTree;
			wdp->m = f->firstWD->m;
			wdp->fp = f;
			wdp->lineno = preLabel->lineno;
			wdp->exp = newSymbolExp("STK07", insPoint->lineno);
			wdp->sizeOrORG = 1;
			wdp->wdIsInstruction = 1;


			wdp->next = newWDATA1(f->areap);
			wdp->next->prev = wdp;

			wdp = wdp->next;
			wdp->locTree = preLabel->locTree;
		}
		wdp->m = f->firstWD->m;
		wdp->fp = f;
		wdp->sizeOrORG = 1;
		wdp->lineno = preLabel->lineno;
		if (immediate)
			wdp->exp = newConstExp(0x06, insPoint->lineno); // mvlw (MVL) the plusw2
		else
			wdp->exp = newConstExp(0x64, insPoint->lineno); // mvfw 
		wdp->wdIsInstruction = 1;
		wdp->instHighByte = 1;
		ADDLAREC(wdp);

		wdp->next = newWDATA1(f->areap);
		wdp->next->prev = wdp;
		wdp = wdp->next;
		wdp->locTree = preLabel->locTree;
		//wdp->sizeOrORG = 1;

		wdp->m = f->firstWD->m;
		wdp->fp = f;
		wdp->lineno = preLabel->lineno;
		wdp->exp = newConstExp(plusw2, preLabel->lineno); // local size or 
		wdp->wdIsInstruction = 1;
		//wdp->instHighByte = 1;
		wdp->next = newWDATA1(f->areap);
		wdp->next->prev = wdp;
		wdp = wdp->next;
		//wdp->sizeOrORG = 1;
		wdp->locTree = preLabel->locTree;
		wdp->m = f->firstWD->m;
		wdp->fp = f;
		wdp->lineno = preLabel->lineno;
		wdp->exp = newConstExp(0x12, preLabel->lineno); // addwf _FSR2L
		wdp->wdIsInstruction = 1;
		wdp->instHighByte = 1;
		ADDLAREC(wdp);


		wdp->next = newWDATA1(f->areap);
		wdp->next->prev = wdp;
		wdp = wdp->next;
		//wdp->sizeOrORG = 1;
		wdp->locTree = preLabel->locTree;
		wdp->m = f->firstWD->m;
		wdp->fp = f;
		wdp->lineno = preLabel->lineno;
		wdp->exp = newConstExp(0x14, preLabel->lineno); // fsr2l 
		wdp->wdIsInstruction = 1;
		//wdp->instHighByte = 1;

		if (!skiprestorew)
		{
			wdp->next = newWDATA1(f->areap);
			wdp->next->prev = wdp;
			wdp = wdp->next;
			wdp->locTree = preLabel->locTree; //
			wdp->m = f->firstWD->m;
			wdp->fp = f;
			wdp->lineno = preLabel->lineno;
			wdp->exp = newConstExp(0x64, preLabel->lineno); // mvfw
			wdp->wdIsInstruction = 1;
			wdp->instHighByte = 1;
			ADDLAREC(wdp);
			wdp->next = newWDATA1(f->areap);
			wdp->next->prev = wdp;
			wdp = wdp->next;
			wdp->locTree = preLabel->locTree;//
			wdp->m = f->firstWD->m;
			wdp->fp = f;
			wdp->lineno = preLabel->lineno;
			wdp->exp = newSymbolExp("STK07", preLabel->lineno);
			wdp->wdIsInstruction = 1;
			//wdp->instHighByte = 1;
		}
	}
	// append it
	wdp->next = insPoint;
	wdp1 = insPoint->prev;
	insPoint->prev = wdp;
	//needAddLA[needAddLANum++] = insPoint;
	ADDLAREC(insPoint);
	// search for f
	if (f->firstWD->ap->wDatas == insPoint)
	{
		f->firstWD->ap->wDatas = wdph;
	}
	else
	{

		//for (wdp1 = f->firstWD->ap->wDatas; wdp1; wdp1 = wdp1->next)
		//{
		//	if (wdp1->next == insPoint)
		//		break;
		//}
		if (wdp1 == NULL)
		{
			fprintf(stderr, "Linker internal error!! search fp head fail.\n");
			exit(-1090);
		}
		wdp1->next = wdph;
		wdph->prev = wdp1;
	}
	return wdph;
}
static wData * confirmWdN(wData *wdp, int n)// confirm there is n wd
{
	int i;
	for (i = 0; i < n; i++)
	{
		if (!wdp)
			return NULL;
		if (wdp->wdIsLabel)
			wdp = wdp->next;
		wdp = wdp->next;
	}
	return wdp;
}
// new optimization will remove LDA for void
static wData *findVarParaFinal66XX(wData *head, wData *tail)
{
	wData *fin;
	do {
		fin = confirmWdN(head, 6);
		if (!fin)
			return NULL;
		if (fin == tail)
		{
			if (head->wdIsLabel)
				head = head->next;
			// incase LDA will be removed!!
			if (getExpMinValue(head->next->next->next->next->next->exp) != 0x64)
				head = head->next->next;

			return head;
		}
		head = head->next;
		if (!head->instHighByte)
			head = head->next;
		

	} while (head);
	
	return NULL;
}


// in case 
// bar( a, b) // maps to bar_STK0 (a)   .. STK12 (b)
// but a is not used (

static int stk_index_remap(funcRecord *f)
{
	int usedSTK[128];
	int i,n,j,paraNum=0;
	dataItem *dip;
	//int remap = 0;
	char *sname;
	char cmpHead[256];
	int stknMax = -1;
	// special case
	if (f->calledByFPTR) // if FPTR call, all should be in order
	{
		for (i = 0; i < sizeof(f->paramSTKReMap)/sizeof(int); i++)
			f->paramSTKReMap[i] = i;

		// re-calculate the local sizes
		if(!f->paraOnStack && f->declaredParamSize>1) // final one is passwd by W
			f->localSymSize = f->localSymSizeExcludePara + f->declaredParamSize-1;
		return 1;
	}


	memset(usedSTK, 0, sizeof(usedSTK));
	strncpy(cmpHead, f->name,252);
	strcat(cmpHead, "_STK");
	n = strlen(cmpHead);
	for (dip = f->localList; dip; dip = dip->next)
	{
		sname = getSymInExp(dip->exp)->exp.symbol;
		if (strncmp(sname, cmpHead, n))
			continue;
		j = atoi(sname + n);
		if (j > stknMax)
			stknMax = j;
		usedSTK[j] = 1;
		paraNum++;
	}

	for (i = 0; i <=stknMax; i++)
	{
		int k = 0;
		for (j = 0; j < i; j++)
			k += usedSTK[j];
		f->paramSTKReMap[i] = k;
	}
	if (paraNum != stknMax + 1)
		return 1;
	return 0;
}

static int add_prologue_epilogue(funcRecord *f)
{

	moduleLD *m = f->m;
	wData* twdp;
	if ((!f->firstWD) || f->proemi_added || (!f->finalWD))
		return -1;
	
//#ifdef _DEBUG
//	if (strstr(f->name, "DelayLIB"))
//		fprintf(stderr,"add prolog to DelayLIB\n");
//#endif
	
	//if (strstr(f->name, "_modsint"))
		//fprintf(stderr, "modisint\n");
	stk_index_remap(f);

	if (f->h08dReEntry)
		h08dOptimize(f);
	
	if ( findSymInModuleLD("STK07", m, 1, 1) == NULL)
	{
		// we need to add STK03 to reference list
		symbol *symp = newSymbol("STK07");
		symbol *symd;
		symp->ldm = m;
		symp->symIsExt = 1;
		m->refSym[m->refSymNum++] = symp;
		symd = findExportSym(hash("STK07"), NULL);
		symp->definedPos = symd;
	}

	// make previous of firstWD is label of function!!

	twdp = findPrevWdp(f->firstWD);
		
	while (twdp && (!(twdp->wdIsLabel) || strcmp(twdp->sym->name, f->name)))
	{
		twdp = findPrevWdp(twdp);
		if (!twdp) // only for special hand-writing function?
		{
			fprintf(stderr, "Internal Error!! function %s head label not found, give up.\n", f->name);
			exit(-__LINE__);
		}
		f->firstWD = twdp->next;
	}
	
	if (f->localSymSizeExcludePara && f->h08dReEntry)
	{
		int shift = f->localSymSizeExcludePara;
		wData *newh;
		while (shift > 31)
		{
			newh = insertADDFSR(f->firstWD, 2, 31);
			shift -= 31;
			f->firstWD = newh;
		}
		 newh = insertADDFSR(f->firstWD, 2, shift); // fsr2
		f->firstWD = newh;
	}

	if (f->localSymSizeExcludePara && (f->p41ReEntry)) // only p41 re-entry need use localon stak
	{
		// we need transfer W to stack3 (interrupt save if used)
		// then move fsr2
		// then readback w
		//if (f->paraOnStack)// we need skip firt push instruction
		//{
			//insert_plusw2(f->firstWD, f->localSymSizeExcludePara, 1, 0, 0); // add before first push!!
		//}
		//else
		//{
			wData *newh = insert_plusw2(f->firstWD, f->localSymSizeExcludePara, 1, 0, 0);
			f->firstWD = newh; // new head found
		//}
	}

	// when return, there are some situations
	// if there is no var length para, the return is 1 step
	if (f->localSymSize || f->paraOnStack)
	{
		if (!f->paraOnStack && f->p41ReEntry)
		{ // one step
		    // special case is when final is 0xFXXX
			if(!((getExpMinValue(f->finalWD->exp)&0xf0)==0xf0))
				insert_plusw2(f->finalWD, 0x100 - f->localSymSize,1,0,0);
		}
		else if ( f->h08dReEntry && !((getExpMinValue(f->finalWD->exp)&0xf0)==0xf0)) // this is req even paraOnStack
		{

#ifdef _DEBUG
			if (strstr(f->name, "Bd_SendUART"))
				fprintf(stderr, "pro/epi %s\n", f->name);
#endif
			int shiftb = 0 - f->localSymSize;
			/*wData *wdpre=NULL;
			for (wdpre = f->firstWD; wdpre && wdpre->next && wdpre->next->next &&
				wdpre->next != f->finalWD; wdpre = wdpre->next)
				;
			if (wdpre == NULL)
			{
				fprintf(stderr, "internal error sdld:%s:%d\n", __FILE__, __LINE__);
				exit(-__LINE__);
			}*/
			while (shiftb < -32)
			{
				insertADDFSR(f->finalWD, 2, -32);
				shiftb += 32;
			}
			// it is possible RETFIE
			if(f->finalWD->next && getExpMinValue(f->finalWD->next->exp)==0x0a && useADDULNK)
				insertADDFSR(f->finalWD, 3, shiftb); // one instruction is fine
			else
				insertADDFSR(f->finalWD, 2, shiftb); // RETFIE cannot be combined!!
		} else // here variable parameter where top stack is the size
		{ // possibly 2 steps
			if (f->localSymSizeExcludePara && ( f->p41ReEntry) && f->paraOnStack) // only for P41, not for P58
			{
				// we need add it before assembly code pop
				// since h08d have no shift ptr from RAM method, many instructions required
				// the assembly will pop the pushed para, linker need to insert after the push
				// in assembly :
				//  66 80  
				//  64 0a // we add addlw is ok
				//  12 14
				//  64 80
				//  00 0a
				// we need find the 
				wData *wdpf=findVarParaFinal66XX(f->firstWD, f->finalWD);
				if (wdpf == NULL)
				{
					fprintf(stderr, "Linker internal err: when insert stack operation.\n");
					exit(-1210);
				}
				

				// this works for both p41 && h08d
				insert_plusw2(wdpf->next->next, 0x100 - f->localSymSizeExcludePara, 2,0,0); //2 means addlw
				

				//insert_plusw2(f->finalWD, 0x0a, 0,1,0);
			}
		}
	}



	f->proemi_added = 1;
	//for(wdp=f->)
	return 0;
}


// check if re-entry
// 
static funcRecord * fparr[100];
static int set_tree_fsr2(funcRecord *fp, int level, int byfptr)
{
	int i;
	if (fp->fsr2set)
		return 0;
	fp->fsr2set = 1;
	fp->calledByFPTR |= byfptr; // any path by fptr, then it should be by fptr
	if(level>=100)
	{
		fprintf(stderr,"Error, call level too deep.\n");
		exit(-__LINE__);
	}
	char buffer[MAXSTRSIZE0+1024];
	for (i = 0; i < level; i++)
		if (fparr[i] == fp)
			return 0;
	fparr[level] = fp;
	//if (strstr(fp->name, "_modsint"))
		//fprintf(stderr, "modsint\n");
	for (i = 0; i < fp->calledFuncNum; i++)
	{
		set_tree_fsr2(fp->calledFunc[i], level + 1,byfptr);
	}
	if (fp->p41ReEntry == 0 && fp->h08dReEntry==0)
	{

		if (fp->baseLocalAddr != 0)
		{
			snprintf(buffer, sizeof(buffer)-1, "func %s change to use FSR2 stack because of (caller) called by pointer.\n",
				fp->name);
			appendErrMsg(buffer);
			fp->needReAssign = 1;
			need_remap = 1;
		}
		if (HY08A_getPART()!=NULL && HY08A_getPART()->isEnhancedCore ==4 )
		{
			fp->h08dReEntry = 1;
			spreadFuncRecord(fp, fp->m);
			add_prologue_epilogue(fp);
		}
		else
		{
			fp->p41ReEntry = 1;
			spreadFuncRecord(fp, fp->m);
			add_prologue_epilogue(fp);
		}
		spreadFuncRecord(fp, fp->m); // spread it again ..whatever
		
		return 1;
	}
	
	return 0;
}
static int maxLevel;

static int traceCallDepth_pre(funcRecord *startf, int level, int thread ) // ok return 0
{
	int i;
	char buffer[4096];
	char buf2[MAXSTRSIZE0+256];
	int deeper=0;
	//if (thread == 1 && startf->calledByMainThread == 1)
	//{
	//	if(!startf->doubleThreadWarned)
	//		fprintf(stderr, "Warning, %s called by both interrupt and main thread!!\n", startf->name);
	//	startf->doubleThreadWarned = 1;
	//	return 0;
	//}
	

	if (startf->highestCallLevel < level)
	{
		startf->highestCallLevel = level;
		deeper=1;
	}

	if (level > maxLevel)
		maxLevel = level; 
	if (body_has_sfr2_stack() < 2)
	{
		for (i = 0; i < level; i++)
		{
			if (callDepthTable[i] == startf)
			{
				//fprintf(stderr, "func %s possible call re-entry .\n", startf->name);
				if (!(startf->reEntryWarnShown))
				{
					int j;
					startf->reEntryWarnShown = 1;
					snprintf(buffer, 4095, "Warning, function %s called re-entry .\n", startf->name);
					if (body_has_sfr2_stack() == 1)
					{
						for (j = i; j < level; j++)
							callDepthTable[i]->p41ReEntry = 1;// if that is re-entry, all should be set to reentry!!
					}
					appendErrMsg(buffer);
				}
				return 0;
			}
		}
	}
	else
	{
		for (i = 0; i < level; i++)
		{
			if (callDepthTable[i] == startf)
			{
				if (!(startf->reEntryWarnShown))
				{
					//int j;
					startf->reEntryWarnShown = 1;
					snprintf(buffer, 4095, "Warning, function %s called re-entry .\n", startf->name);
				}
				return 0;
			}
		}
	}

	callDepthTable[level] = startf;
	if (!(callDepthTable[0]->callTooDeepWarnedLev < level) && level > HY08A_getPART()->pcStack && startf->calledFuncNum==0)
	{
		//fprintf(stderr, "Warning, thread %s possible call too deep:\n", thread==0?"main":"interrupt");
		
		
			snprintf(buffer, 4095, "Warning, thread %s pc stack level %d seems possible overflow(> %d levels):\n", thread == 0 ? "main" : "interrupt", level, HY08A_getPART()->pcStack);
			for (i = 0; i <= level; i++)
			{
				//fprintf(stderr, "%d:%s\n", i + 1, callDepthTable[i]->name);
				snprintf(buf2, sizeof(buf2)-1, "PC Stack level %d:%s\n", i, callDepthTable[i]->name);
				strncat(buffer, buf2, sizeof(buffer)-1);
			}
			appendErrMsg(buffer);
			callDepthTable[0]->callTooDeepWarnedLev = level;
		
		//return -1; // it is possible .. just will not happen
	}
	// check if double

	if (!startf)
		return 0; // sometimes no startf

	if (thread == 0)
		startf->calledByMainThread = 1;
	else
		startf->calledByInterruptThread = 1;
	if (startf->calledByInterruptThread && startf->calledByMainThread && !startf->doubleThreadWarned 
		&& body_has_sfr2_stack()<2) // we warn if only NOT H08D
	{
		fprintf(stderr, "Warning, %s called by both interrupt and main thread!!\n", startf->name);
		//snprintf(buffer,40960, "Warning, %s called by both interrupt and main thread!!\n", startf->name);
		//appendErrMsg(buffer);
		startf->doubleThreadWarned = 1;
		startf->p41ReEntry = body_has_sfr2_stack(); // if both used, we set p41 re-entry
	}
	if (body_has_sfr2_stack() >= 2)
		startf->h08dReEntry = 1; // we give h08d re-entry!!

	
	if (!startf->spreaded)
	{
		simple_spread(startf, startf->m);
		deeper=1; // force spread
	}
	if(deeper ) // if not deeper, the following need not
	{
		for (i = 0; i < startf->calledFuncNum; i++)
		{
			funcRecord *funcp = startf->calledFunc[i];
			traceCallDepth_pre(funcp, level + 1, thread);
		}
	}
	
	// 

	return 0;
}
// h08d optimize include
// MVL+mvwf princ2*n+[w destroy instruction] ==> PUSHL*n + [w destroy instruction]
// ADDFSR2 +RET -> ADDULNK
static int expHasStackOffset(expression *exp)
{
	if (exp->type == ExpIsStackOffset)
		return 1;
	if (exp->type == ExpIsTree)
	{
		return expHasStackOffset(exp->exp.tree.left) | expHasStackOffset(exp->exp.tree.right);
	}
	return 0;
}

static expression * findRightShift8inExp(expression *exp)
{
	if (exp->type == ExpIsTree && exp->exp.tree.op == 'A') // A is right shift
	{
		expression *r = exp->exp.tree.right;
		if (r->type == ExpIsNumber && r->exp.number == 8)
			return exp; // it is correct
	}
	if (exp->type == ExpIsTree)
	{
		expression *r = findRightShift8inExp(exp->exp.tree.right);
		expression *l = findRightShift8inExp(exp->exp.tree.left);
		if (r)
			return r;
		return l;
	}
	return NULL;
}


// 2017 DEC, we try to fix a bug
// that when pushed parameter is not initialized, the 
// push data will not be enough, and 
// though the result will be ok
// but if the data is X
// it is still dangerous that FSR2 will be wrong
extern int needOptimizeJxx;
static int h08dOptimize(funcRecord *f)
{
	wData *wdp;
	wData *wdp2;
	wData *wdp3;
	wData *wdpa;
	int prevSKIP = 0;
	moduleLD *m = f->m;
	int n = 0;
	if ((!f->firstWD) || f->proemi_added || (!f->finalWD))
		return -1;

	
	//if (needOptimizeJxx) // PUSHL optimize need JXX
	if(usePUSHL)
	{
		for (wdp = f->firstWD; wdp && wdp != f->finalWD; wdp = wdp->next)
		{
			if (!wdp->instHighByte)
				continue;
			if (prevSKIP)
			{
				prevSKIP = 0;
				continue;
			}
			if (wdp->instSkip)
				prevSKIP = 1;

			// MVL k
			// MVWF xxx_STK
			// ==> move to PUSHL
			if (isWdMVL_NONSTK(wdp) && wdp->next && wdp->next->next && wdp->next->next->next && isWdMVWF(wdp->next->next)
				&& isWdPRINC2(wdp->next->next->next, f, 0)) // change to pushl cannot count dummy
			{
				//char buf1[1024];
				wdp2 = wdp->next->next->next->next;
				// debug here
				//exprDump(wdp2->exp, buf1, 1023);
				wdpa = NULL;
				// MVL k
				// MVWF xxx_STK
				// ==> move to PUSHL
				if (wdp2 && wdp2->instHighByte && (isWdMVFW(wdp2) || isWdMVL(wdp2, 0)))
				{
					// change to PUSHL
					wdp->exp = newConstExp(0xc5, wdp->exp->lineno);
					wdp->next->next = wdp2;
					wdp2->prev = wdp->next;
					wdpa = wdp->next;
					wdp3 = wdp2;
					n++;
				}
				else // same literal copying,  it is more complex
				{
					// if there is call the final one should be mvl or mvfw,
					wdp3 = wdp2;
					wdpa = wdp->next; // assume this is final
					while (wdp3 && wdp3->wdIsInstruction && !isWdCALL(wdp3) && !isWdJump(wdp3) && !isWdMVFW(wdp3) && !isWdMVL(wdp3, 0))
					{
						if (isWdMVWF(wdp3) && isWdPRINC2(wdp3->next, f, 0))
						{
							wdp3->exp = newConstExp(0xc5, wdp->exp->lineno);
							wdp3->next->exp = wdp->next->exp;// copy the lit
						}
						wdpa = wdp3->next;
						wdp3 = wdp3->next->next;

					}
					wdp->exp = newConstExp(0xc5, wdp->exp->lineno);
					wdp->next->next = wdp2;
					wdp2->prev = wdp->next;
					// special case, if there is no MVL before CALL, we need MVL additionally

					n++;
				}
				if (wdpa && (isWdCALL(wdp3)||isWdJump(wdp3))) // final parameter use W to pass
				{
					// insert MVL before CALL
					wData *wdp4, *wdp5;
					insertInstruction1exph(wdp4, wdpa, newConstExp(0x06, wdp3->lineno));
					insertInstruction1exp(wdp5, wdp4, wdp->next->exp);
					n++;
				}
			}

		}
	}

	// next is mvfw+mvwf -> mvff mvsf
	if (mvxxDis && (mvxxForce==0))
		return n;
	for (wdp = f->firstWD;  wdp && wdp != f->finalWD; wdp = wdp->next)
	{
		if (!wdp->instHighByte)
			continue;
		if (isWdMVFW(wdp) && wdp->next && wdp->next->next && wdp->next->next->next && wdp->next->next->next->next && isWdMVWF(wdp->next->next)
		&& (isWdMVL(wdp->next->next->next->next,0)||isWdMVFW(wdp->next->next->next->next)
			|| isWdPUSHL(wdp->next->next->next->next))) // for mvwf+pushl is formly mvl
		{
			//if(isWdMVWF(wdp->next->next) && if)
			// consider the source and dest, we have FF/SF/SS, but we have no MVFS, because F to S is generally by PUSH
			// for STK3,4,5,6, the return value high words, MVSF should do the job correctly.
			// because TEMP variables are all on stack, 
			// only TEMP to STK3,4,5,6 will be required.
			// we use MVFF, and when evaluate, we change to others
			// we have MVSF, MVFF, MVSS, exception is MVFS
			// make sure it is not F->S first
			char *symn1 = findSymInExp(wdp->exp);
			char *symn2 = findSymInExp(wdp->next->next->exp);
			symbol *symp1 = NULL;
			symbol *symp2 = NULL;
			expression *var0=NULL;
			expression *var1;
			expression *var2;
			expression *var3 = NULL;
			wData *wdp2 = wdp->next->next;

			//char buf0[256];
			//char buf1[256];
			//char buf2[256];
			if (symn1)
				symp1 = findSymInModuleLD(symn1, f->m, 1, 1);
			if (symn2)
				symp2 = findSymInModuleLD(symn2, f->m, 1, 1);
			if (expHasStackOffset(wdp2->next->exp) ||( symp2 && symp2->symIsByFSR2 )) // we don't optimize MVSS, only MVFF, MVSF
				continue;
			// decide which instruction to use
			// use MVSF/MVFF will reduce the need of LBSR

			
			//exprDump(wdp->exp, buf0, 255);
			//exprDump(wdp->next->exp, buf1, 255);
			//exprDump(wdp->next->next->next->exp, buf2, 255);
			if (symp1 && symp1->symIsExt && !symp1->symIsByFSR2 && symp1->definedPos!=NULL)
				symp1 = symp1->definedPos;
			if (symp2 && symp2->symIsExt && !symp2->symIsByFSR2 && symp2->definedPos!=NULL)
				symp2 = symp2->definedPos;


			var0 = findRightShift8inExp(wdp->exp);
			var3 = findRightShift8inExp(wdp->next->next->exp);

			if (wdp->next->exp->type == ExpIsTree && wdp->next->exp->exp.tree.op == '&')
				var1 = wdp->next->exp->exp.tree.left;
			else
				var1 = wdp->next->exp;

			if (wdp2->next->exp->type == ExpIsTree && wdp2->next->exp->exp.tree.op == '&')
				var2 = wdp2->next->exp->exp.tree.left;
			else
				var2 = wdp2->next->exp;

			if (wdp->next->exp->type == ExpIsTree && wdp->next->exp->exp.tree.op == '&')
				var1 = wdp->next->exp->exp.tree.left;
			
			// if VAR2 < 0x80, no need mvsf, mvf+mvf is OK
			if ( (expHasStackOffset(wdp->next->exp) ||( symp1 && symp1->symIsByFSR2))) // first is S, then use MVSF
			{
				if (!((var2->type == ExpIsNumber && var2->exp.number < 128)
					|| (var2->type == ExpIsSymbol && strstr(var2->exp.symbol, "_STK")
						|| (var2->type == ExpIsSymbol && !strcmp(var2->exp.symbol, "STK00"))
						|| (var2->type == ExpIsSymbol && !strcmp(var2->exp.symbol, "STK01"))
						|| (var2->type == ExpIsSymbol && !strcmp(var2->exp.symbol, "STK02"))

						)
					))
				{

					wdp->exp = newConstExp(0xc6, wdp->exp->lineno); // MVSF
					// next exp mark out MSB
					wdp->next->exp = newTreeExp(newConstExp(0x7f, wdp->next->exp->lineno), wdp->next->exp, '&', wdp->next->exp->lineno);

					if (var3)
						wdp2->exp = newTreeExp(newConstExp(0xf0, wdp2->exp->lineno), var3, '|', wdp2->exp->lineno);
					else
						wdp2->exp = newTreeExp(newConstExp(0xf0, wdp2->exp->lineno), newTreeExp(var2, newConstExp(8, wdp2->exp->lineno), 'A', wdp2->exp->lineno), '|', wdp2->exp->lineno);
				}
			}
			else // MVFF
			{
				if(var0)
					wdp->exp = newTreeExp(newConstExp(0xd0, wdp->exp->lineno),var0, '|', wdp2->exp->lineno);
				else
					wdp->exp = newTreeExp(newConstExp(0xd0, wdp->exp->lineno), 
						newTreeExp(var1, newConstExp(8, wdp->exp->lineno), 'A', wdp->exp->lineno), '|', wdp2->exp->lineno);
				
				if(var3)
					wdp2->exp = newTreeExp(newConstExp(0xf0, wdp2->exp->lineno), var3, '|', wdp2->exp->lineno);
				else
					wdp2->exp = newTreeExp(newConstExp(0xf0, wdp2->exp->lineno), newTreeExp(var2, newConstExp(8, wdp2->exp->lineno), 'A', wdp2->exp->lineno), '|', wdp2->exp->lineno);
			}
			n++;
		}
	}

	return n;
}
static void resetFuncBaseAddr(void)
{
	funcRecord* fp;
	int i;
	moduleLD *mdp;
	for (mdp = topMap.linkedModules; mdp; mdp = mdp->next)
	{
		for (i = 0; i < mdp->funcNum; i++)
		{
			fp = mdp->funcs[i];
			fp->baseLocalAddr = 0;
			fp->baseLocalXAddr = 0;
			
		}
	}
}
static int traceCallDepth(funcRecord *startf, int level, int thread, int ramStartA, int *ramFinalA, int xStartA, int *xFinalA) // ok return 0
{
	int i;
	char buffer[4096];
	char buf2[MAXSTRSIZE0+256];


	for (i = 0; i < level; i++)
	{
		if (callDepthTable[i] == startf)
		{
			if (body_has_sfr2_stack() < 2)
			{
				//fprintf(stderr, "func %s possible call re-entry .\n", startf->name);
				if (!(startf->reEntryWarnShown))
				{
					int j;
					startf->reEntryWarnShown = 1;
					snprintf(buffer, sizeof(buffer)-1, "Warning, function %s called re-entry .\n", startf->name);
					if (body_has_sfr2_stack())
					{
						for (j = i; j < level; j++)
							callDepthTable[i]->p41ReEntry = 1;// if that is re-entry, all should be set to reentry!!
					}
					appendErrMsg(buffer);
				}
				
			}
			else
			{
				if (!(startf->reEntryWarnShown))
				{
					//int j;
					startf->reEntryWarnShown = 1;
					snprintf(buffer, sizeof(buffer)-1, "Warning, function %s called re-entry .\n", startf->name);
					appendErrMsg(buffer);
				}
			}
			return 0;
		}
	}
	callDepthTable[level] = startf;
	if ((callDepthTable[0]->callTooDeepWarnedLev< level ) && level > HY08A_getPART()->pcStack && startf->calledFuncNum == 0) // final level
	{
		//fprintf(stderr, "Warning, thread %s possible call too deep:\n", thread==0?"main":"interrupt");
			snprintf(buffer, sizeof(buffer)-1, "Warning, thread %s PC stack level %d seems possible overflow (> %d levels):\n", thread == 0 ? "main" : "interrupt",level, HY08A_getPART()->pcStack);
			for (i = 0; i <= level; i++)
			{
				//fprintf(stderr, "%d:%s\n", i + 1, callDepthTable[i]->name);
				snprintf(buf2, sizeof(buf2)-1, "PC Stack level %d:%s\n", i, callDepthTable[i]->name);
				strncat(buffer, buf2, sizeof(buffer)-1);
			}
			appendErrMsg(buffer);
			callDepthTable[0]->callTooDeepWarnedLev = level;
		//return -1; // it is possible .. just will not happen
	}
	// check if double
	
	
	if (thread == 0)
		startf->calledByMainThread = 1;
	else
		startf->calledByInterruptThread = 1;
	if (startf->calledByInterruptThread && startf->calledByMainThread && !startf->doubleThreadWarned)
	{

		if (body_has_sfr2_stack() < 2)
		{
			fprintf(stderr, "Warning, %s called by both interrupt and main thread!!\n", startf->name);
			//snprintf(buffer,40960, "Warning, %s called by both interrupt and main thread!!\n", startf->name);
			//appendErrMsg(buffer);
			startf->doubleThreadWarned = 1;
			startf->p41ReEntry = body_has_sfr2_stack(); // if both used, we set p41 re-entry
		}
		else
		{
			fprintf(stderr, "Warning, %s called by both interrupt and main thread!!\n", startf->name);
			//snprintf(buffer,40960, "Warning, %s called by both interrupt and main thread!!\n", startf->name);
			//appendErrMsg(buffer);
			startf->doubleThreadWarned = 1;
		}
	}

	if (startf->p41ReEntry || startf->paraOnStack || startf->h08dReEntry)// special address!!, 4k limit of it!!
	{
		//if this is re-entry, we need
		// add prologure and epilog
		// calculate the stack offset for eash instructions 
		// stack offset must not have loops 
		// we shall check that later
		// 
		
		add_prologue_epilogue(startf);
		//startf->baseLocalAddr = 0x1000;
		//startf->baseLocalXAddr = 0x1000;
	}
	// there are some others not add prologue/epilogue

	if(!startf->p41ReEntry && !startf->h08dReEntry)
	{
		if (ramStartA + startf->localSymSize > (*ramFinalA))
		{
			//(*ramFinalA) += startf->localSymSize;
			(*ramFinalA) = ramStartA + startf->localSymSize;
		}
	/*	if (*ramFinalA > 0xf0)
		{
			fprintf(stderr, "alloc large\n");
		}*/
		if (xStartA + startf->localXSymSize > (*xFinalA))
			//(*xFinalA) += startf->localXSymSize;
			(*xFinalA) = xStartA + startf->localXSymSize;
		if (ramStartA > startf->baseLocalAddr)
		{
			if (verbose)
				fprintf(stderr, "func %s base assigned to %X\n", startf->name, ramStartA);
			startf->baseLocalAddr = ramStartA;
		}
		if (xStartA > startf->baseLocalXAddr)
		{
			if (verbose)
				fprintf(stderr, "func %s base assigned to %X\n", startf->name, ramStartA);
			startf->baseLocalXAddr = xStartA;
		}
	}
	if (startf->baseLocalAddr <= 0x180 && startf->baseLocalAddr + startf->localSymSize > 0x180)
	{
		startf->baseLocalAddr = 0x200; // shift them all
	}
	
	if (startf->baseLocalAddr <= 0x780 && startf->baseLocalAddr + startf->localSymSize > 0x780)
	{
		startf->baseLocalAddr = 0x800; // shift them all
	}
	if (startf->baseLocalAddr <= 0x852 && startf->baseLocalAddr + startf->localSymSize > 0x852)
	{
		startf->baseLocalAddr = 0x880; // shift them all
	}
	for (i = 0; i < startf->calledFuncNum; i++)
	{
		funcRecord *funcp = startf->calledFunc[i];
		
		traceCallDepth(funcp, level + 1, thread, startf->baseLocalAddr + startf->localSymSize, ramFinalA,
			startf->baseLocalXAddr+startf->localXSymSize, xFinalA);
	}
	// 

	return 0;
}

static int checkIfExpRefLabel(expression *exp, uint64 hashid, int level)
{
	static expression * oldchk[64];
	int i;
	for (i = 0; i < level; i++)
		if (oldchk[i] == exp)
			return 0;
	oldchk[i] = exp;
	if (exp->type == ExpIsSymbol && exp->symHash == hashid)
		return 1;
	if (exp->type == ExpIsTree)
	{
		return checkIfExpRefLabel(exp->exp.tree.left, hashid, level + 1) |
			checkIfExpRefLabel(exp->exp.tree.right, hashid, level + 1);
	}
	return 0;
}

static int ifLabelReferenced(uint64 hashid)
{
	wData *wdp;
	area * areap;
	for (areap = topMap.romArea; areap; areap = areap->next)
	{
		for (wdp = areap->wDatas; wdp; wdp = wdp->next)
		{
			if (!wdp->wdIsLabel && wdp->exp && checkIfExpRefLabel(wdp->exp, hashid, 0))
			{
				if (wdp->fp && wdp->fp->noAllocWarned)
					continue;
				return 1;
			}
		}
	}
	return 0;
}
// here we check if it uses pointer in more detail
// if it is called by pointer,
// it can be assigned in const (not code)
// or by MVL ... check these first
static int ifLabelReferencedThoughPointer(uint64 hashid)
{
	wData *wdp;
	wData *wdppre = NULL;
	area * areap;
	for (areap = topMap.romArea; areap; areap = areap->next)
	{
		wdppre = NULL;
		for (wdp = areap->wDatas; wdp; wdp = wdp->next)
		{
			if (!wdp->wdIsLabel && !wdp->instHighByte && wdp->exp && checkIfExpRefLabel(wdp->exp, hashid, 0))
			{
				
				if(!wdp->wdIsInstruction)
					return 1; // by DBDW, it should be pointer!!
				
				if (wdppre->next == wdp && isWdMVL(wdppre,1)) // referenced by MVL instruction, it should be pcall
					return 1;
			}
			if(wdp->instHighByte)
				wdppre = wdp;
		}
	}
	return 0;
}

static int isWdMVFF0(wData *wd) // means to 00XX
{
	if (!wd)
		return 0;
	if ((!wd->instHighByte) || wd->wdIsBlank || wd->wdIsLabel || wd->wdIsOrg || wd->wdIsErrMark || wd->exp == NULL)
		return 0;
	if ((getExpMinValue(wd->exp)&0xf0)==0xd0 && (getExpMinValue(wd->next->next->exp)&0x0f)==0)
		return 1;
	return 0;
}
static int isWdMVSF0(wData *wd) // means to 00xx
{
	if (!wd)
		return 0;
	if ((!wd->instHighByte) || wd->wdIsBlank || wd->wdIsLabel || wd->wdIsOrg || wd->wdIsErrMark || wd->exp == NULL)
		return 0;
	if (getExpMinValue(wd->exp)==0xc6 && (getExpMinValue(wd->next->exp)&0x80)==0  && (getExpMinValue(wd->next->next->exp) & 0x0f) == 0)
		return 1;
	return 0;
}
static int isWdMVWF(wData *wd)
{
	if (!wd)
		return 0;
	if ((!wd->instHighByte) || wd->wdIsBlank || wd->wdIsLabel || wd->wdIsOrg || wd->wdIsErrMark || wd->exp == NULL)
		return 0;
	/*if (wd->exp->type == ExpIsNumber && wd->exp->exp.number == 0x66)
		return 1;
	if (wd->exp->type == ExpIsTree && wd->exp->exp.tree.left->type == ExpIsNumber &&
		wd->exp->exp.tree.left->exp.number == 0x66 && wd->exp->exp.tree.op == '|')
		return 1;*/
	if ((getExpMinValue(wd->exp) & 0xfe) == 0x66)
		return 1;
	return 0;
}
static int isWdCLRF(wData *wd)
{
	if (!wd)
		return 0;
	if ((!wd->instHighByte) || wd->wdIsBlank || wd->wdIsLabel || wd->wdIsOrg || wd->wdIsErrMark || wd->exp == NULL)
		return 0;
	if (wd->exp->type == ExpIsNumber && wd->exp->exp.number == 0x0c)
		return 1;
	if (wd->exp->type == ExpIsTree && wd->exp->exp.tree.left->type == ExpIsNumber &&
		wd->exp->exp.tree.left->exp.number == 0x0c && wd->exp->exp.tree.op == '|')
		return 1;
	return 0;
}
static int isWdSETF(wData *wd)
{
	if (!wd)
		return 0;
	if ((!wd->instHighByte) || wd->wdIsBlank || wd->wdIsLabel || wd->wdIsOrg || wd->wdIsErrMark || wd->exp == NULL)
		return 0;
	if (wd->exp->type == ExpIsNumber && wd->exp->exp.number == 0x0A)
		return 1;
	if (wd->exp->type == ExpIsTree && wd->exp->exp.tree.left->type == ExpIsNumber &&
		wd->exp->exp.tree.left->exp.number == 0x0A && wd->exp->exp.tree.op == '|')
		return 1;
	return 0;
}

static int isWdMVFW(wData *wd)
{
	if (!wd)
		return 0;
	if ((!wd->instHighByte) || wd->wdIsBlank || wd->wdIsLabel || wd->wdIsOrg || wd->wdIsErrMark || wd->exp == NULL)
		return 0;
	/*if (wd->exp->type == ExpIsNumber && wd->exp->exp.number == 0x64)
		return 1;
	if (wd->exp->type == ExpIsTree && wd->exp->exp.tree.left->type == ExpIsNumber &&
		wd->exp->exp.tree.left->exp.number == 0x64 && wd->exp->exp.tree.op == '|')
		return 1;*/
	if ((getExpMinValue(wd->exp) & 0xfe) == 0x64)
		return 1;
	return 0;
}
static int isWdPUSHL(wData *wd)
{
	if (!wd)
		return 0;
	if ((!wd->instHighByte) || wd->wdIsBlank || wd->wdIsLabel || wd->wdIsOrg || wd->wdIsErrMark || wd->exp == NULL)
		return 0;
	if (wd->exp->type == ExpIsNumber && wd->exp->exp.number == 0xC5)
		return 1;
	
	return 0;
}
static int isWdADDFSR2(wData *wd)
{
	if (!wd)
		return 0;
	if ((!wd->instHighByte) || wd->wdIsBlank || wd->wdIsLabel || wd->wdIsOrg || wd->wdIsErrMark || wd->exp == NULL)
		return 0;
	if (wd->exp->type == ExpIsNumber && wd->exp->exp.number == 0xC4 && 
		// ADDFSR or ADDUNLK
		wd->next && wd->next->exp && ((getExpMinValue(wd->next->exp)&0x80)==0x80)
		)
		return 1;

	return 0;
}
static int ifwdADDFWF(wData *wd)
{
	if (!wd)
		return 0;
	if (!wd->wdIsInstruction || wd->wdIsBlank || wd->wdIsLabel || wd->wdIsOrg || wd->wdIsErrMark || wd->exp == NULL)
		return 0;
	if (wd->exp->type == ExpIsNumber && wd->exp->exp.number == 0x12)
		return 1;
	if (wd->exp->type == ExpIsTree && wd->exp->exp.tree.left->type == ExpIsNumber &&
		wd->exp->exp.tree.left->exp.number == 0x12 && wd->exp->exp.tree.op == '|')
		return 1;
	return 0;
}
static int isWdMVL_NONSTK(wData *wd)
{
	expression *expt, *syme;
	symbol *symp;
	if (!isWdMVL(wd,0))
		return 0;
	expt = wd->next->exp;
	syme = getSymInExp(expt);
	if (!syme)
		return 1; // it is true literal
	symp = findSymInModuleLD(syme->exp.symbol, wd->m, 1, 1);
	if(symp && symp->symIsByFSR2)
		return 0; // it is not allowed
	return 1;
}
static void SetExpImmed(expression *exp)
{
	exp->ExpSymImmed = 1;
	if (exp->type == ExpIsTree)
	{
		SetExpImmed(exp->exp.tree.left);
		SetExpImmed(exp->exp.tree.right);
	}
}
// important, this will set Immed flag for functions like printf!!
static int isWdMVL(wData *wd, int alsoADDL)
{
	if (!wd)
		return 0;
	if (!wd->wdIsInstruction  || wd->wdIsBlank || wd->wdIsLabel || wd->wdIsOrg || wd->wdIsErrMark || wd->exp == NULL)
		return 0;
	if (wd->exp->type == ExpIsNumber)
	{
		if (wd->exp->exp.number == 0x06 || (alsoADDL && wd->exp->exp.number == 0x04))
		{ //currently we have MVL with stack variable used
			if (wd->next && wd->next->exp)
				SetExpImmed(wd->next->exp);
			return 1;
		}
		//else if (wd->exp->exp.number == 0x04) // ADDL
		//{
		//	if (wd->next && wd->next->exp)
		//		wd->next->exp->ExpSymImmed = 1;
		//}
	}
	return 0;
}
static int ifwdSUBFWW(wData *wd)
{
	if (!wd)
		return 0;
	if ((!wd->instHighByte) || wd->wdIsBlank || wd->wdIsLabel || wd->wdIsOrg || wd->wdIsErrMark || wd->exp == NULL)
		return 0;
	if (wd->exp->type == ExpIsNumber && wd->exp->exp.number == 0x18)
		return 1;

	return 0;
}

static expression * ifwdADDCF(wData *wd)
{
	expression *exp;
	if (!wd)
		return NULL;
	if ((!wd->instHighByte) || wd->wdIsBlank || wd->wdIsLabel || wd->wdIsOrg || wd->wdIsErrMark || wd->exp == NULL)
		return NULL;
	if (getExpMinValue(wd->exp) == 0x16)
	{
		exp = wd->exp;
		while (exp->type == ExpIsTree)
			exp = exp->exp.tree.left;
		return exp;
	}

	return NULL;
}


static int isWdPRINC2(wData *wd, funcRecord *fp, int countDummy)
{
	//char buf[256];
	//if (wd->exp)
		//exprDump(wd->exp, buf, 255);
	//static uint64_t hashPRINC2 = hash("_PRINC2");

	if (!wd)
		return 0;
	if (!wd->wdIsInstruction  || wd->wdIsBlank || wd->wdIsLabel || wd->wdIsOrg || wd->wdIsErrMark || wd->exp == NULL)
		return 0;
	if (wd->exp->type == ExpIsNumber && wd->exp->exp.number == 0x0d)
		return 1;
	if (wd->exp->type == ExpIsTree && wd->exp->exp.tree.left->type == ExpIsNumber &&
		wd->exp->exp.tree.left->exp.number == 0x0d && wd->exp->exp.tree.op == '&')
		return 1;
	
	if (wd->exp->type == ExpIsTree && wd->exp->exp.tree.left->type == ExpIsSymbol)
	{
		symbol *sp = findSymInModuleLD(wd->exp->exp.tree.left->exp.symbol, wd->m, 1, 1);
		//if (strstr(sp->name, "STK00"))
		//{
		//	char buffer[256];
		//	exprDump(wd->exp, buffer, 255);
		//	fprintf(stderr, "%s\n", buffer);
		//}
		if (sp && sp->hashid==hashPRINC2)//
			return 1;
		if (sp && sp->symIsExt)
		{
			sp = sp->definedPos;
		}
		if (sp && sp->symIsByFSR2 && fp != sp->fpHasthis) // not for local operation
		{
			//if (wd->lineno == 32)
				//fprintf(stderr, "line32\n");
			return 1;
		}
		// for dummy, we return 1 
		if (countDummy && sp && sp->symIsEQU && getExpMinValue(sp->exp) == DUMMY_CONST)
			return 1; // special case
	}
	return 0;
}
static int isWdPODEC2(wData *wd, funcRecord *fp)
{
	//char buf[256];
	//if (wd->exp)
	//exprDump(wd->exp, buf, 255);
	if (!wd)
		return 0;
	if (!wd->wdIsInstruction || wd->wdIsBlank || wd->wdIsLabel || wd->wdIsOrg || wd->wdIsErrMark || wd->exp == NULL)
		return 0;
	if (wd->exp->type == ExpIsNumber && wd->exp->exp.number == 0x0c)
		return 1;
	if (wd->exp->type == ExpIsTree && wd->exp->exp.tree.left->type == ExpIsNumber &&
		wd->exp->exp.tree.left->exp.number == 0x0c && wd->exp->exp.tree.op == '&')
		return 1;

	if (wd->exp->type == ExpIsTree && wd->exp->exp.tree.left->type == ExpIsSymbol)
	{
		symbol *sp = findSymInModuleLD(wd->exp->exp.tree.left->exp.symbol, wd->m, 1, 1);
		//if (strstr(sp->name, "STK00"))
		//{
		//	char buffer[256];
		//	exprDump(wd->exp, buffer, 255);
		//	fprintf(stderr, "%s\n", buffer);
		//}
		if (!strcmp(sp->name, "_PODEC2"))
			return 1;
		//if (sp->symIsExt)
		//{
		//	sp = sp->definedPos;
		//}
		//if (sp->symIsByFSR2 && fp != sp->fpHasthis) // not for local operation
		//{
		//	//if (wd->lineno == 32)
		//	//fprintf(stderr, "line32\n");
		//	return 1;
		//}
	}
	return 0;
}

static int ifwdFSR2L(wData *wd)
{
	if (!wd)
		return 0;
	if (!wd->wdIsInstruction  || wd->wdIsBlank || wd->wdIsLabel || wd->wdIsOrg || wd->wdIsErrMark || wd->exp == NULL)
		return 0;
	if (wd->exp->type == ExpIsNumber && wd->exp->exp.number == 0x14)
		return 1;
	if (wd->exp->type == ExpIsTree && wd->exp->exp.tree.left->type == ExpIsNumber &&
		wd->exp->exp.tree.left->exp.number == 0x14 && wd->exp->exp.tree.op == '&')
		return 1;
	return 0;
}

static int isWdCALL(wData *wd)
{
	if (!wd)
		return 0;
	if ((!wd->instHighByte) || wd->wdIsBlank || wd->wdIsLabel || wd->wdIsOrg || wd->wdIsErrMark || wd->exp == NULL)
		return 0;

	// call can be RCALL or Far call
	// far call is 0xc000
	if (wd->exp->type == ExpIsTree && wd->exp->exp.tree.left->type == ExpIsNumber &&
		( wd->exp->exp.tree.left->exp.number == 0xC8) && wd->exp->exp.tree.op == '|')
		return 1;
	if (wd->exp->type == ExpIsNumber && (wd->exp->exp.number == 0xc0 || wd->exp->exp.number == 0xc1)) // c1 is call xxx,1
		return 2; // this is also call!!
	// rcall isc c8
	return 0;

}
static int isWdJump(wData *wd)
{
	if (!wd)
		return 0;
	if ((!wd->instHighByte) || wd->wdIsBlank || wd->wdIsLabel || wd->wdIsOrg || wd->wdIsErrMark || wd->exp == NULL)
		return 0;

	// call can be RCALL or Far call
	// far call is 0xc000
	if (wd->exp->type == ExpIsTree && wd->exp->exp.tree.left->type == ExpIsNumber &&
		(wd->exp->exp.tree.left->exp.number == 0x78) && wd->exp->exp.tree.op == '|')
		return 1;
	if (wd->exp->type == ExpIsNumber && (wd->exp->exp.number == 0xc2) ) // c1 is call xxx,1
		return 2; // this is also call!!
	// rcall isc c8
	return 0;

}

static int isWdRET(wData *wd, int skipLabel)
{
	wData *wdn;
//	char buffer[256];
	if (!wd)
		return 0;
	//if(wd->exp)
		//exprDump(wd->exp, buffer, 255);
	if (skipLabel)
	{
		while (wd->wdIsLabel)
			wd = wd->next;
	}
	//exprDump(wd->exp, buffer, 255);
	wdn = wd->next;
	if (!wdn||!wd->wdIsInstruction ||  wd->wdIsBlank || wd->wdIsLabel || wd->wdIsOrg || wd->wdIsErrMark || wd->exp == NULL)
		return 0;
	if (!wdn->wdIsInstruction ||  wdn->wdIsBlank || wdn->wdIsLabel || wdn->wdIsOrg || wdn->wdIsErrMark || wdn->exp == NULL)
		return 0;
		// 000a and c4c0
	if (wd->exp->type == ExpIsNumber && wd->exp->exp.number == 0x00 && wdn->exp->type==ExpIsNumber && wdn->exp->exp.number==0x0a)
		return 1;
	if (wd->exp->type == ExpIsNumber && wd->exp->exp.number == 0xC4 && wdn->exp->type==ExpIsNumber && wdn->exp->exp.number==0xC0)
		return 1;
	return 0;
}

static expression * getSymInExp(expression * exp)
{
	expression *found;
	switch (exp->type)
	{
	case ExpIsTree:
		found = getSymInExp(exp->exp.tree.left);
		if (found)
			return found;
		return getSymInExp(exp->exp.tree.right);
	case ExpIsSymbol:
		return exp;
	default: 
		return NULL;
	}
	return NULL;
}
static expression * getRAMSymInExp(expression * exp, moduleLD *mdp) // for bsr operation, only for RAM!!
{
	expression *found;
	symbol *symp;
	area *ap;
	if (!mdp)
		return NULL;
	switch (exp->type)
	{
	case ExpIsTree:
		found = getRAMSymInExp(exp->exp.tree.left, mdp);
		if (found)
			return found;
		return getRAMSymInExp(exp->exp.tree.right, mdp);
	case ExpIsSymbol:
		symp=findSymInModuleLD(exp->exp.symbol, mdp, 1, 1);
		if (symp->symIsExt)
		{
			ap = symp->definedPos->ap;
		}
		else
			ap = symp->ap;
		if (ap!=NULL && ap->region == regionCODE)
			return NULL;
		return exp;
	default:
		return NULL;
	}
	return NULL;
}




funcRecord * getFuncByExp(funcRecord *fp0, expression *exp)
{
	expression *symexp = getSymInExp(exp);
	int i;
	if (symexp == NULL)
		return NULL;
	for (i = 0; i < fp0->calledFuncNum; i++)
		if (fp0->calledFunc[i]->namehash == symexp->symHash)
			return fp0->calledFunc[i];
	return NULL;
}


// we add 08d cases!!
// special issue is that <stdarg.h> the FSR2 offset set back at the caller not the callee,
// for H08C the FSR2 is fixed by callee
// but H08D has the ADDFSR instruction, so the SP is modified by caller
// we have to take care of this kind of special issue!!
// can be by PCALL or general CALL

// 2022 oct, try to adjust by parameter size

static int getFSR2RollingOffset(funcRecord *fp)
{
	//when function push/pop data by FSR2, the offset changes
	// this procedure set wDATA's fsr2 rolling offset, from the beginning

	// it is possible there will be branch
	// we want the branch back to be the same
	// if not zero we return -1;
	// also, for MVL fsr2var, we will add subfww _fsr2l later

	
//	char buf1[100];
//	char buf2[100];
	// we trace with call and MVWF
	// at first, we skip branch
	wData *wdp,*wdpn;
	wData *prevW = NULL;
//#ifdef _DEBUG
//	static int ccc = 0;
//	static int ddd = 0;
//
//	//if (strstr(fp->name, "DelayLIB"))
//	//	fprintf(stderr, "DelayLIB rolling offset\n");
//#endif
	int paraPushed = 0;
	int callStyle = 0;
	int pointerDifference = 0;
	int prevMVXF = 0;
	int offsetBeforeFirstPush = 0;
	int delayMVFFRET = 0; // pcall use MVFF, stack reset just follows the MVFF
	int zeroLocal = (fp->localSymSizeExcludePara == 0); // special case need not shift anything, 
	int skipFirstPush = fp->paraOnStack && HY08A_getPART()->isEnhancedCore == 3;

	int fsr2offset = 0;
	int fptrCallVarLenPara = 0;

	char funcTemp[MAXSTRSIZE0+1024];
	int funcTempAvailable = 0;
	funcTemp[0] = '\0';
	//snprintf(funcTemp, sizeof(funcTemp) - 1, "%s__addrtmp__", fp->name);
	//if (fp->m &&  findSymInModuleLD(funcTemp, fp->m, 1, 1))
	//	funcTempAvailable = 1;

	for (wdp = fp->firstWD; wdp && (wdp != fp->finalWD && wdp->m == fp->m) && (wdp->fp==NULL || wdp->fp==fp) && wdp->next; prevW=wdp,wdp = wdp->next)
	{
		wdp->fsr2_stack_offset = fsr2offset;

		if (wdp->exp && (!wdp->wdIsMacroMark)&& dummySymbolUsedInExp(wdp->exp, fp->m, 0))
		{
			wData *wdp1;
			for(wdp1=wdp;wdp1 && wdp1!=fp->finalWD;wdp1=wdp1->next)
				if (isWdCALL(wdp1)||isWdJump(wdp1))
				{
					wdp1->dummyPushFixed = 1;
					break;
				}
		}

		if (!wdp->instHighByte)
			continue;

		wdp->next->fsr2_stack_offset = fsr2offset;
		if (wdp->next && wdp->next->next)
		{
			wdpn = wdp->next->next;
			//if (wdp->exp != NULL)
			//	exprDump(wdp->exp, buf1, 99);
			//if (wdpn->exp != NULL)
			//	exprDump(wdpn->exp, buf2, 99); // consider MVWF+MVLW + MVFW+MVFW
			if (ifwdADDFWF(wdpn) && ifwdFSR2L(wdpn->next))// before plusw2 there should be MVL or MVFW
			{
				if (isWdMVL(wdp,1))
					fsr2offset = (fsr2offset + wdp->next->exp->exp.number)&0xff;

			}
			


		}
		//if (wdp->exp != NULL)
		//	exprDump(wdp->exp, buf1, 99);
		//if (wdp->next->exp != NULL)
		//	exprDump(wdp->exp, buf2, 99); // consider MVWF+MVLW + MVFW+MVFW

		// we need to consider first push is by save, 
		// that is why we skip first push
		//if (prevW != NULL && !(prevW->wdIsLabel && prevW->sym->exp->symHash == wdp->fp->namehash))
		//if (wdp->lineno == 210)
		//	fprintf(stderr, "210\n");
		if ((isWdMVFF0(wdp) || isWdMVSF0(wdp)) && isWdPRINC2(wdp->next->next->next, fp,0))
		{
			prevMVXF = 1;
			continue;
		}
		if ((((isWdMVWF(wdp) || isWdCLRF(wdp) || isWdSETF(wdp)) && isWdPRINC2(wdp->next, fp,1))|| isWdPUSHL(wdp))||
			prevMVXF)
		{
			//char buf[1024];
			//exprDump(wdp->next->exp, buf, 1023);
			prevMVXF = 0;
			if (dummySymbolUsedInExp(wdp->next->exp,wdp->m,0))
			{
				wData *wdCall;
				int callStyle = 0;
				funcRecord *fp1;
				for (wdCall = wdp->next->next; wdCall; wdCall = wdCall->next)
				{
					//char buf[1024];
					//exprDump(wdCall->exp, buf,1023);
					if ((callStyle = (isWdCALL(wdCall)|isWdJump(wdCall))) != 0)
						break;
				}
				fp1 = (callStyle == 1) ? getFuncByExp(fp, wdCall->exp) :
					getFuncByExp(fp, wdCall->next->next->exp);
				if(fp1 && fp1->calledByFPTR)
					fsr2offset++;
			}
			else
			{
				if ((!paraPushed && zeroLocal) || (!paraPushed  && prevW != NULL && !(prevW->wdIsLabel && prevW->sym && prevW->sym->exp
					&& prevW->sym->exp->symHash == wdp->fp->namehash)))
				{
					if (skipFirstPush && isWdPRINC2(wdp->next, fp, 0))
					{
						// first push will comes here!!
						skipFirstPush = 0; // for var para, we skip first push
					}
					else
					{
						offsetBeforeFirstPush = fsr2offset;
						paraPushed = 1;
						//delayMVFFRET = 0;
					}
				}
				fsr2offset++;

			}
		}
		if (((isWdMVFW(wdp) && isWdPODEC2(wdp->next, fp))))
		{

			
			fsr2offset--;
		}
		if (isWdADDFSR2(wdp))
		{
			int shift = getExpMinValue(wdp->next->exp)&0x3f;
			if (shift & 0x20)
				shift = (shift & 0x1f) - 32;
			if (fptrCallVarLenPara)
			{
				fptrCallVarLenPara = 0;
			}else 
				fsr2offset += shift;
			// 2022 follow the shift
			//if (HY08A_getPART()->isEnhancedCore == 3 || shift>0)
			//{
			//	fsr2offset += shift; // for H08C this is direct operation
			//}
			//else // for H08D and <0, we must consider stdarg.h
			//{
			//	wData *wdp0=wdp;
			//	do {
			//		wdp0 = findPrevWdp(wdp0);
			//	} while (wdp0 && !(wdp0->instHighByte));
			//	if(!isWdCALL(wdp0) && getExpMinValue(wdp0->exp)!=0xf0 && !wdp0->callerBackFSR2)
			//	{
			//		fsr2offset += shift;
			//	}else
			//	{
			//		wdp->callerBackFSR2=1;
			//	}

			//}
		}
		
		else if ((callStyle =isWdCALL(wdp))!=0) // we need to make sure the call uses the fsr2 stack
		{

			// since the jump table will not use call, the call can be pointer call, which is also same offset
			// but fptr call uses MVFF
			funcRecord *fp1 = (callStyle==1)? getFuncByExp(fp, wdp->exp) :
				getFuncByExp(fp, wdp->next->next->exp);

			// 2 situations, parameter call and case switch?
			// it is possible PCALL and normal call, if PCALL, we delay to MVFF

			//if (wdp->lineno == 287)
				//fprintf(stderr, "287\n");
			//if (fp1 && (fp1->h08dReEntry || fp1->p41ReEntry || fp1->paraOnStack) && paraPushed)

			// special case that need push more things @ 2017 DEC
			if ((!wdp->dummyPushFixed)&& fp1 && (fp1->localSymNum - fp1->localSymSizeExcludePara) > 0 && !fp1->paraOnStack &&
				((!paraPushed)||(fsr2offset-offsetBeforeFirstPush)<(fp1->localSymNum-fp1->localSymSizeExcludePara ))) // not declared!!
			{
				if (fp->h08dReEntry)
				{
					// use addfsr2 is ok
					// if a call have parameter, it will not have skip 
					//wData *fpp = findPrevWdp(wdp);


					// it is possible that parameter is no used .. at all
					int needpush = paraPushed ? (fp1->localSymNum - fp1->localSymSizeExcludePara -(fsr2offset - offsetBeforeFirstPush)) :
						fp1->localSymNum - fp1->localSymSizeExcludePara;
					insertADDFSR(wdp, 2, needpush);
					wdp->dummyPushFixed = 1;
				}
				else if (! fp->p41ReEntry && !fp->paraOnStack)
				
				{
					// we give up here, 
					fprintf(stderr, "Linker Internal error at %s:%d, seems function %s called by %s \nparameter is not initialized, which is not support yet.\n"
						, __FUNCTION__, __LINE__, fp1->name, fp->name);
					exit(-__LINE__);
				}
			}

			if(paraPushed )
			{
				if (fp1 && (fp1->h08dReEntry || fp1->p41ReEntry || fp1->paraOnStack)) // simple operation
				{
					// 2022 oct, use parameter size to decide the fsr2 movement!!
					if (fp1->declaredParamSize >= 0)
					{
						if (fp1->declaredParamSize > 0) // skip ==0 case
						{
							if (fp1->calledByFPTR) // if FPTR call, no reduce the param size!!
							{
								fsr2offset -= (fp1->declaredParamSize - 1);
								paraPushed -= (fp1->declaredParamSize - 1);
							}
							else
							{
								fsr2offset -= fp1->realParaSize;
								paraPushed -= fp1->realParaSize;
							}
						}
					}
					else if (!fp1->paraOnStack) // need other ADDFSR2 to move it
					{
						fsr2offset = offsetBeforeFirstPush;
						paraPushed = 0;
					}
					delayMVFFRET = 0;//normal call, need no mvff!!
				}
				else
				{
					// call by FPTR, the offset should be reset when MVFF XXX,PCL
					delayMVFFRET = 1;
					
				}
			}
			else if (!fp1)// pcall have no para pushed before call
			{
				delayMVFFRET = 1;
			}
		}
		else if ( delayMVFFRET && isMVFF(getExpMinValue(wdp->exp)) && wdp->next && wdp->next->next && wdp->next->next->next && 
			getExpMinValue(wdp->next->next->next->exp)==0x1b  && paraPushed) // MVFF will not use stack
		{
			fsr2offset = offsetBeforeFirstPush;
			paraPushed = 0;
			delayMVFFRET = 0; 
			if (isWdADDFSR2(wdp->next->next->next->next->next))
			{
				fptrCallVarLenPara = 1;
			}
		}
		
		// add SUBFWW if required
		// some times pointer - pointer is used.., we have to prevent that 
		if(!(wdp->next->next && ifwdSUBFWW(wdp->next->next) && getExpMinValue(wdp->next->next->next->exp)==0x14) // FSR2L is 0x14
		  && isWdMVL(wdp,1) && wdp->next->exp && 
			(leftMostExp(wdp->next->exp)->type==ExpIsSymbol  )
			)

		{
			//char buffer[256];
			symbol *sp;
			//exprDump(wdp->next->exp, buffer, 255);
			sp = findSymInModuleLD(leftMostExp(wdp->next->exp)->exp.symbol, wdp->m, 1, 1);
			if (sp && sp->symIsByFSR2 ) 
			{
				// we have to consider the possible of pointer-address
				// like gcc-torture-execute-20030221-1.c
				// if ((buf[0] != 10) || (p - buf != 1))

				if (expHasShiftRight8(wdp->next->exp)) // in the future, we will change to MVFW FSR2H
				{
					// special case MVL addrh ADDC offseth
					wData *wd2 = wdp->next->next;
					
					// 2021 case ADDC PRODH
					if ((wd2->instHighByte && wd2->exp && wd2->exp->type == ExpIsTree &&
						wd2->exp->exp.tree.left->type==ExpIsNumber &&
						wd2->exp->exp.tree.left->exp.number == 0x14) || (wd2->instHighByte && wd2->exp && wd2->exp->type ==ExpIsNumber
						&& wd2->exp->exp.number==0x14)
						)
					{

						// skip it 
						wdp->next->next = wd2->next->next;
						wd2->next->next->prev = wdp->next; // dual dir
					}
					else if (wd2->instHighByte && wd2->exp && wd2->exp->type==ExpIsNumber
						&& wd2->exp->exp.number==0xa8 && wd2->next && wd2->next->next &&
						wd2->next->next->exp && wd2->next->next->exp->type==ExpIsNumber &&
						wd2->next->next->exp->exp.number==0x04) // btsz+addl 1
					{
						wdp->next->next = wd2->next->next->next->next;
						wd2->next->next->next->next->prev = wdp->next; // dual dir
					}
					if (fp->h08dReEntry) // extend to 0x17F
					{
						// CLRF XXX
						// BTSS _WREG,7
						// INCF XXX

						// change the MVWF to CLRF
						expression *expLeft = leftMostExp(wdp->next->next->exp);
						//wData *wdpre = findPrevWdp(wdp);
						//wData *wdPre = findPrevWdp(wdp);
						wData *wIns0, *wIns1, *wIns2, *wIns3;// *wIns4, *wIns5;
						
						funcRecord *f = wdp->fp;
						// remove this instr
						
						int k = getExpMinValue(expLeft);

						// 2020 MAY, try not use stk07
						snprintf(funcTemp, sizeof(funcTemp) - 1, "%s__addrtmp__", fp->name);
						
						if (fp->m &&  findSymInModuleLD(funcTemp, fp->m, 1, 1))
							funcTempAvailable = 1;


						expression *estk7l = funcTempAvailable? newTreeExp(newSymbolExp(funcTemp, wdp->lineno), newConstExp(0x80, wdp->lineno), '|', wdp->lineno) :
								newTreeExp(newSymbolExp("STK07", wdp->lineno), newConstExp(0xff, wdp->lineno), '&', wdp->lineno);
						expression *estk7hov  =funcTempAvailable ? 
							newTreeExp(
								newTreeExp(newSymbolExp(funcTemp, wdp->lineno), newConstExp(8, wdp->lineno), 'A', wdp->lineno),
								newConstExp(1, wdp->lineno), '&', wdp->lineno):
							newTreeExp(
							newTreeExp(newSymbolExp("STK07", wdp->lineno), newConstExp(8, wdp->lineno), 'A', wdp->lineno),
							newConstExp(1, wdp->lineno), '&', wdp->lineno);
						
						expression *estk7hbe = newTreeExp(newConstExp(0xbe, wdp->lineno),
								estk7hov	, '|', wdp->lineno);
						estk7hov->exp.tree.OverflowChkUnsign = 1;
						estk7hov->exp.tree.OverflowChk = 1;
						
						
						//if (k != 0x66)
						//{
							// it is possible that following instruction is CALL!!
							// fprintf(stderr, "Warning, assembly %s line %d need check\n", wdp->m->srcName, wdp->lineno);

							// we need to set 1 if bit 7 is 0, set 0 if bit7 is 1
							wdp->next->exp->type = ExpIsNumber;
							wdp->next->exp->exp.number = 0;
							// 
							// movlw 0
							// BTSS 0x20,7
							// BSF _WREG,0

							//if (pointerDifference)
							//{
							//	wData *w64, *w20;
							//	insertInstruction1(w64, wdPre, 0x64);
							//	insertInstruction1d(w0c, w64, 0x0c);
							//	pointerDifference = 0; // just zero!!
							//	wdPre = w0c;
			
							//}
							insertInstruction1exph(wIns0, wdp->next, estk7hbe); // BTSS 0x20 
							insertInstruction1exp(wIns1, wIns0, estk7l);
							insertInstruction1(wIns2, wIns1, 0x90); // BSF _WREG,0
							insertInstruction1d(wIns3, wIns2, 0x29); // WREG

							// ***********************************************************
							// 2020 Dec, it is possible that following is ADDCF
							// ********************************************************
							if ((expLeft=ifwdADDCF(wIns3->next))!=NULL)
							{
								// change to ADD (0x16->0x12)
								if(expLeft->type==ExpIsNumber)
									expLeft->exp.number = 0x12; // change to ADD, no C!!
							}
							
						//}
						//else
						//{
						//	wData *wIns0, *wIns1, *wIns2, *wIns3;
						//	wData *wDn = wdp->next->next;
						//	expression *incfhexp, *incflexp;
						//	wdpre->next = wDn;
						//	if (expLeft->exp.number & 1)
						//		expLeft->exp.number = 0x0d;
						//	else
						//		expLeft->exp.number = 0x0c; // change to CLRF
						//	funcRecord *f = wdp->fp;
						//	// clrf xx
						//	// btss _WREG,7 .. BE 29
						//	// incf xx   ... complex is this, we need copy current exp to new one and change the op code
						//	//wd2 = wdp->next->next;
						//	insertInstruction1(wIns0, wDn->next, 0xbe);
						//	insertInstruction1d(wIns1, wIns0, 0x29);
						//	incfhexp = copyExp(wDn->exp);
						//	incflexp = copyExp(wDn->next->exp);
						//	expLeft = leftMostExp(incfhexp);
						//	if (expLeft->exp.number & 1)
						//		expLeft->exp.number = 0x3B;
						//	else
						//		expLeft->exp.number = 0x3A; // INF is 3A
						//	insertInstruction1exph(wIns2, wIns1, incfhexp);
						//	insertInstruction1exp(wIns3, wIns2, incflexp);

						//}
						
					}
					else
					{
						// change to const 0
						wdp->next->exp->type = ExpIsNumber;
						wdp->next->exp->exp.number = 0; // critical, but should work
					}
				}
				else
				{
					wData *wdpn = newWDATA1(wdp->ap);
					wData *wdpnh = wdpn;
					wData *w66, *w20;
					expression *estk7h, *estk7l, *estk7hov;
					funcRecord *f = wdp->fp;
					wdpn->locTree = wdp->locTree;//
					wdpn->m = f->firstWD->m;
					wdpn->fp = f;				// consider we want to read a[i]
												// real address is fsr2-a+i, or fsr2l-(a-i)

												// a must > i, and a, i < 128
												// that is high byte is FSR2H SUBC 0 .. second stage
												// compiler will use i to ADDL a
												// we need to change to SUBL
												// the problem is the high byte
												// (i>>8)+(a>>8)+C
					wdpn->lineno = wdp->lineno;
					wdpn->exp = newConstExp(0x18, wdp->lineno); // subfww _fsr2l

					// if addl is used, we change to subl
					if (wdp->exp->type==ExpIsNumber && wdp->exp->exp.number == 0x04) // ADDL
						wdp->exp->exp.number = 0x08; // change to SUBL
					if ((getExpMinValue(wdp->next->next->exp) & 0xFE) == 0x18) // pointer difference!!
						pointerDifference = 1;
					wdpn->wdIsInstruction = 1;
					wdpn->instHighByte = 1;
					//needAddLA[needAddLANum++] = wdpn; // ADD more
					ADDLAREC(wdpn);


					wdpn->next = newWDATA1(f->areap);
					wdpn->next->prev = wdpn; // dual dir

					wdpn = wdpn->next;
					wdpn->locTree = wdp->locTree;//
					wdpn->m = f->firstWD->m;
					wdpn->fp = f;
					wdpn->lineno = wdp->lineno;
					wdpn->exp = newConstExp(0x14, wdp->lineno); // FSR2L always @ 0x0014!!
					wdpn->sizeOrORG = 1;
					wdpn->wdIsInstruction = 1;
					wdpn->next = wdp->next->next;
					wdp->next->next->prev = wdpn;
					wdp->next->next = wdpnh;
					wdpnh->prev = wdp->next;

					snprintf(funcTemp, sizeof(funcTemp) - 1, "%s__addrtmp__", fp->name);

					if (fp->m &&  findSymInModuleLD(funcTemp, fp->m, 1, 1))
						funcTempAvailable = 1;

					estk7hov=funcTempAvailable ?
						newTreeExp(
							newTreeExp(newSymbolExp(funcTemp, wdp->lineno), newConstExp(8, wdp->lineno), 'A', wdp->lineno),
							newConstExp(1, wdp->lineno), '&', wdp->lineno)
						:
						 newTreeExp(
						newTreeExp(newSymbolExp("STK07", wdp->lineno), newConstExp(8, wdp->lineno), 'A', wdp->lineno),
						newConstExp(1, wdp->lineno), '&', wdp->lineno);
					estk7hov->exp.tree.OverflowChkUnsign = 1;
					estk7hov->exp.tree.OverflowChk = 1;
					estk7h = newTreeExp(newConstExp(0x66, wdp->lineno),
						estk7hov, '|', wdp->lineno);
					// ->right 
					

						
					estk7l = funcTempAvailable?
						newTreeExp(newSymbolExp(funcTemp, wdp->lineno), newConstExp(0x80, wdp->lineno), '|', wdp->lineno):
					newTreeExp(newSymbolExp("STK07", wdp->lineno), newConstExp(0xff, wdp->lineno), '&', wdp->lineno);
						
					insertInstruction1exph(w66, wdpn, estk7h); // always copy to stk07

						insertInstruction1exp(w20, w66, estk7l);
					
				}
			}
		}
		
	}
	return 0;
}

void insertWd(wData* insPoint, wData* newWd)
{
	newWd->next = insPoint->next;
	newWd->prev = insPoint; // 2022 oct, dual direction linklist
	insPoint->next = newWd;
	newWd->ap = insPoint->ap;
	newWd->fp = insPoint->fp;
	newWd->lineno = insPoint->lineno;
	newWd->m = insPoint->m;
	newWd->locTree = insPoint->locTree;


}



// 

static wData * checkIfSymReferencedByFunction(funcRecord *f, char *sym, uint64 symhash, int level)
{
	static funcRecord * oldFunc[100];
	wData *wdp;

	int i;
	if (f->firstWD == NULL || f->finalWD == NULL)
		return NULL; // skipped
	for (i = 0; i < level; i++)
		if (oldFunc[i] == f)
			return NULL;
	// NULL allowed
	for (wdp = f->firstWD;wdp && wdp != f->finalWD && (wdp->fp==NULL || wdp->fp == f); wdp = wdp->next)
	{
		if (wdp->exp)
		{
			if (exp_has_sym(wdp->exp, symhash))
				return wdp;
		}
	}
	oldFunc[level] = f;
	// if not referenced by me, we check if referenced by my called func
	for (i = 0; i < f->calledFuncNum; i++)
	{
		symbol* symp = findSymInModuleLD(sym, f->calledFunc[i]->m, 1, 1);

		if (!symp)
			continue;
		if (symp->symIsEQU)
		{
			if ((wdp=checkIfEQUReferencedByFunction(f->calledFunc[i], getExpMinValue(symp->exp), sym, symhash, level + 1))!=NULL)
				return wdp;

		}
		else
		{
			if ((wdp=checkIfSymReferencedByFunction(f->calledFunc[i], sym, symhash, level + 1))!=NULL)
				return wdp;
		}
	}
	return NULL;
}
static wData * checkIfEQUReferencedByFunction(funcRecord *f, int sfra, char *sym, uint64 symHash, int level)
{
	static funcRecord * oldFunc[100];
	wData *wdp;

	int i;
	if (f->firstWD == NULL || f->finalWD == NULL)
		return NULL; // skipped
	for (i = 0; i < level; i++)
		if (oldFunc[i] == f)
			return NULL;

	for (wdp = f->firstWD; wdp && wdp != f->finalWD && (wdp->fp==NULL||wdp->fp == f); wdp = wdp->next)
	{
		int oph;
		int opl;
		if (!wdp->instHighByte)
			continue;
		oph = getExpMinValue(wdp->exp);
		opl = getExpMinValue(wdp->next->exp);
		if (((oph & 0xf0) == 0xd0) && ((opl == sfra && oph == 0xd0) || (wdp->next->next && wdp->next->next->next &&
			getExpMinValue(wdp->next->next->exp) == 0xf0 &&
			getExpMinValue(wdp->next->next->next->exp) == sfra
			)))
			return wdp;
		if ((oph == 0xc6) && ((opl&0x80)==0) && (wdp->next->next && wdp->next->next->next &&
			getExpMinValue(wdp->next->next->exp) == 0xf0 &&
			getExpMinValue(wdp->next->next->next->exp) == sfra
			))
			return wdp;
		if ((oph == 0x0 && (opl == 0x07 || opl == 0x06)) && sfra <= 0x20 && sfra >= 0x1d) // TBLR will distroy TBLPTRL/H/TBLDL/H, 1d~20
			return wdp;

		if (!isOPFDA(oph))
			continue;
		if (wdp->next->exp && getExpMinValue(wdp->next->exp)==sfra)
		{
			
			return wdp;
		}
	}
	oldFunc[level] = f;
	// if not referenced by me, we check if referenced by my called func
	for (i = 0; i < f->calledFuncNum; i++)
	{
		symbol* symp = findSymInModuleLD(sym, f->calledFunc[i]->m, 1, 1);
		
		if (!symp)
			continue;
		if (symp->symIsEQU)
		{
			if ((wdp=checkIfEQUReferencedByFunction(f->calledFunc[i], sfra, sym, symHash, level + 1))!=NULL)
				return wdp;
		}
		else
		{
			if ((wdp=checkIfSymReferencedByFunction(f->calledFunc[i], sym, symHash, level + 1))!=NULL)
				return wdp;
		}
	}
	return NULL;
}
static int ifInstrModifySFR(wData *wdp, int sfra)
{
	int num1;
	int num2;
	if (!wdp->next || !wdp->instHighByte)
		return 0;
	if (!wdp->exp)
		return 0;
	if (!wdp->next->exp)
		return 0;
	if (wdp->exp->type != ExpIsNumber || wdp->next->exp->type != ExpIsNumber)
		return 0;
	num1 = wdp->exp->exp.number;
	num2 = wdp->next->exp->exp.number;
	if (num2 != sfra)
		return 0;
	if (num1 & 1)
		return 0; // A bit = 1, it should not be SFR we checked
	if (isOPFDA(num1) && (num1 & 0x02)) // fda and d=1
		return 1;
	if (isFAOPW(num1) || isFBAOPW(num1))
		return 1;
	return 0; // seems no others

}



static wData * checkFSRUsedByFunction(funcRecord *f, int fsrID, int level)
{
	static funcRecord * oldFunc[20];
	wData *wdp;
	//uint64 hashph, hashpl;
	int pha, pla; // high addr and low addr
	if (fsrID == 0)
	{
		pha = 0x0f;
		pla = 0x10;
	}
	else if (fsrID == 1)
	{
		pha = 0x11;
		pla = 0x12;
	}
	else if (fsrID == 2)
	{
		pha = 0x13;
		pla = 0x14;
	}

	int i;
	if (f->firstWD == NULL || f->finalWD == NULL)
		return NULL; // skipped
	for (i = 0; i < level; i++)
		if (oldFunc[i] == f)
			return NULL;
	for (wdp = f->firstWD; wdp && wdp != f->finalWD && (wdp->fp==NULL||wdp->fp==f); wdp = wdp->next)
	{
		//if (wdp->lineno == 96 && wdp->m && strstr(wdp->m->name,"interrupt"))
			//fprintf(stderr, "line96\n");
		
		if (wdp->instHighByte && wdp->exp->type == ExpIsNumber && wdp->exp->exp.number == 0 &&
			wdp->next->wdIsInstruction && wdp->next->exp->type == ExpIsNumber && (wdp->next->exp->exp.number ==( 0xc | fsrID))
			)
			return wdp;
		// alternative is FSR0L/FSR0H.. d bit is 1
		if (ifInstrModifySFR(wdp, pha) || ifInstrModifySFR(wdp,pla))
			return wdp;
		/*if (wdp->exp && (exp_has_sym(wdp->exp, hashph) || exp_has_sym(wdp->exp, hashpl)))
			return 1;*/
		
	}
	// if not referenced by me, we check if referenced by my called func
	oldFunc[level] = f;
	for (i = 0; i < f->calledFuncNum; i++)
		if ((wdp=checkFSRUsedByFunction(f->calledFunc[i],fsrID, level + 1))!=NULL)
			return wdp;
	return NULL;
}

static wData*  checkIfEQUReferencedByFPTR(int sfra, char *sym, uint64 symHash)
{
	int i;
	moduleLD *mld;
	wData *wdp;
	for (mld = topMap.linkedModules; mld; mld = mld->next)
	{
		for (i = 0; i < mld->funcNum; i++)
		{
			if (mld->funcs[i]->calledByFPTR)
				if ((wdp=checkIfEQUReferencedByFunction(mld->funcs[i], sfra, sym, symHash, 0))!=NULL)
					return wdp;
		}
	}
	return NULL;
}



static wData *  checkIfSymReferencedByFPTR(char *sym, uint64 symhash)
{
	int i;
	wData *wdp;
	moduleLD *mld;
	for (mld = topMap.linkedModules; mld; mld = mld->next)
	{
		for (i = 0; i < mld->funcNum; i++)
		{
			if (mld->funcs[i]->calledByFPTR)
				if ((wdp=checkIfSymReferencedByFunction(mld->funcs[i], sym, symhash, 0))!=NULL)
					return wdp;
		}
	}
	return NULL;
}
static wData*  checkIfFSRReferencedByFPTR(int fsrID)
{
	int i;
	moduleLD *mld;
	wData *wdp;
	for (mld = topMap.linkedModules; mld; mld = mld->next)
	{
		for (i = 0; i < mld->funcNum; i++)
		{
			if (mld->funcs[i]->calledByFPTR)
				if ((wdp=checkFSRUsedByFunction(mld->funcs[i], fsrID, 0))!=NULL)
					return wdp;
		}
	}
	return NULL;
}
static inline wData *findPrevWdp(wData *wdp)
{
	//wData *wpre;
	//for (wpre = wdp->ap->wDatas; wpre; wpre = wpre->next)
	//	if (wpre->next == wdp)
	//		return wpre;
	//return NULL;

	return wdp->prev;
}

#define SYMHEXP(sym, lineno)  newTreeExp(newSymbolExp(sym, lineno), newConstExp(8, lineno), 'A', lineno)
static int addPushPopSym(funcRecord *f, char *symname, int type)
{
	wData *wd0, *wd1, *wd2, *wd3, *wd4, *wd5,*wd6,*wd7;
	char symSave[40];
	wData *firstPush;
	wData *lastPop;
	//for (firstPush = f->firstWD; firstPush; firstPush = firstPush->next)
		//if (firstPush->exp && firstPush->exp->type == ExpIsNumber && firstPush->exp->exp.number == 0x1c)
			//break;// push is 0x1c
	/*if (strstr(symname, "TBLPTR"))
	{
		fprintf(stderr, "Warning!! H08A body cannot have table look up (const/code access) at both main and interrupt at the same time!!\n");
		return 0;
	}*/
	firstPush = f->firstWD;
	firstPush = findPrevWdp(firstPush);
	lastPop = f->finalWD;
	lastPop = findPrevWdp(lastPop);
	if (firstPush == NULL)
	{
		fprintf(stderr, "internal err: insert pushpop failed.\n");
		exit(-__LINE__);
	}

	
	strncpy(symSave, symname, 31);
	strncat(symSave, "_SAVE", 39);
	//for (lastPop = f->finalWD; lastPop; lastPop = findPrevWdp(lastPop))
	//{
	//	if (lastPop->exp && lastPop->exp->type == ExpIsNumber && lastPop->exp->exp.number == 0xc4)
	//		break;
	//}
	//lastPop = findPrevWdp(lastPop); // before one


	if (type == 0)// RAM, global
	{
		// LSB should be considered in PIC like instructions
		int lineno = firstPush->lineno;
		expression *exp = newTreeExp(newConstExp(0xd0, lineno),
			SYMHEXP(symname,lineno), '|', lineno);
		
		insertInstruction1exph(wd0, firstPush, exp); // use MVFF
		insertInstruction1s(wd1, wd0, symname);

		exp = newTreeExp(newConstExp(0xf0, lineno),
			SYMHEXP(symSave, lineno), '|', lineno);
		insertInstruction1exp(wd2, wd1, exp);
		insertInstruction1s(wd3, wd2, symSave);
		

		exp = newTreeExp(newConstExp(0xd0, lineno),
			SYMHEXP(symSave, lineno), '|', lineno);
		insertInstruction1exph(wd4, lastPop,exp);
		insertInstruction1s(wd5, wd4, symSave);

		exp = newTreeExp(newConstExp(0xf0, lineno),
			SYMHEXP(symname, lineno), '|', lineno);
		insertInstruction1exp(wd6, wd5, exp);
		insertInstruction1s(wd7, wd6, symname);
	}
	//else // sfr
	//{
	//	insertInstruction1(wd0, firstPush, 0x02 + 8); // LDAPU
	//	insertInstruction1s(wd1, wd0, symname);
	//	insertInstruction1(wd2, lastPop, 0xc4); //POP
	//	insertInstruction1(wd3, wd2, 0x12); //STA
	//	insertInstruction1s(wd4, wd3, symname);
	//}
	//needfix3num++;
	return 0;
}

#define CHKADDPUSHPOP(YYY,TT) if ((checkIfSymReferencedByFPTR(YYY, hash(YYY))\
||checkIfSymReferencedByFunction(mainf, YYY, hash(YYY), 0)) &&\
checkIfSymReferencedByFunction(fp, YYY, hash(YYY), 0)) \
	addPushPopSym(fp, YYY,TT)

#define CHKADDPUSHPOP_WARN(YYY,TT,MSG) if (((wdpMain=(wdpMain=checkIfSymReferencedByFPTR(YYY, hash(YYY))))\
||(wdpMain2=checkIfSymReferencedByFunction(mainf, YYY, hash(YYY), 0))) &&\
(wdpInt=checkIfSymReferencedByFunction(fp, YYY, hash(YYY), 0))) \
	{\
	if(wdpMain!=NULL) \
		fprintf(stderr, MSG, wdpMain->fp->name, wdpMain->m->name, wdpInt->fp->name,wdpInt->m->name); \
	else \
		fprintf(stderr, MSG, wdpMain2->fp->name, wdpMain2->m->name, wdpInt->fp->name, wdpInt->m->name); \
}



#define CHKADDPUSHPOPEQU(nn,TT,YYY) if ((checkIfEQUReferencedByFPTR(nn, YYY, hash(YYY))\
||checkIfEQUReferencedByFunction(mainf, nn,YYY, hash(YYY), 0)) &&\
checkIfEQUReferencedByFunction(fp, nn,YYY,hash(YYY), 0)) \
	addPushPopSym(fp, YYY,TT)


#define CHKADDPUSHPOPEQU_WARN(nn,TT,YYY,MSG) if (((wdpMain=checkIfEQUReferencedByFPTR(nn, YYY, hash(YYY)))\
||(wdpMain2=checkIfEQUReferencedByFunction(mainf, nn,YYY, hash(YYY), 0))) &&\
(wdpInt=checkIfEQUReferencedByFunction(fp, nn,YYY,hash(YYY), 0))) {\
	if(wdpMain!=NULL) \
		fprintf(stderr, MSG, wdpMain->fp->name, wdpMain->m->name, wdpInt->fp->name,wdpInt->m->name); \
	else \
		fprintf(stderr, MSG, wdpMain2->fp->name, wdpMain2->m->name, wdpInt->fp->name, wdpInt->m->name); \
}


#define NEEDPUSHPOPFSR(YYY)  ((checkIfFSRReferencedByFPTR(YYY) ||checkFSRUsedByFunction(mainf,YYY, 0)) &&\
checkFSRUsedByFunction(fp,YYY,0))  

#define ADDEXTSYM(XXX) if (!findSymInModuleLD(XXX, fp->m, 1, 1))\
{\
	symbol *symp = newSymbol(XXX);\
symp->ldm = fp->m;\
symp->symIsExt = 1;\
symp->definedPos = findExportSym(symp->hashid, NULL);\
fp->m->refSym[fp->m->refSymNum++] = symp;\
}\

int addInterruptPushPull(void)
{
	//int i;
	funcRecord *fp;
	symbol *symp;
	wData *wdpMain;
	wData *wdpMain2;
	wData *wdpInt;
	//for (i = 0; i < interruptfNum; i++)
	//{ we have only one interrupt
	fp = interruptf;
	if (fp == NULL)
		return 0;
	if (interruptPushPullDone)
		return 0;
	interruptPushPullDone = 1;

	if (topMap.linkedModules == NULL)
		return 0;

	if (fp->interruptPushPullAdded || fp->finalWD == NULL || fp->firstWD == NULL)
		return 0;

	symp = findSymInModuleLD("_PRODL", fp->m, 1, 1);
	if (symp)
	{
		wdpMain = NULL;
		wdpInt = NULL;
		wdpMain2 = NULL;
		if (symp->symIsEQU)
		{
			CHKADDPUSHPOPEQU_WARN(getExpMinValue(symp->exp), 0, "_PRODL",
				"Warning, both main and interrupt threads use PRODL(for Number Product), \n\
function %s module %s, function %s module %s \n\
which interrupt cannot save/restore!!\n");
		}
		else
		{
			CHKADDPUSHPOP_WARN("_PRODL", 0,
				"Warning, both main and interrupt threads use PRODL(for Number Product), \n\
function %s module %s, function %s module %s \n\
which interrupt cannot save/restore!!\n");
		}
	}
	symp = findSymInModuleLD("_PRODH", fp->m, 1, 1);
	if (symp)
	{
		wdpMain = NULL;
		wdpInt = NULL;
		wdpMain2 = NULL;
		if (symp->symIsEQU)
		{

			CHKADDPUSHPOPEQU_WARN(getExpMinValue(symp->exp), 0, "_PRODH",
				"Warning, both main and interrupt threads use PRODH(for Number Product), \n\
function %s module %s, function %s module %s \n\
which interrupt cannot save/restore!!\n");
		}
		else
		{
			CHKADDPUSHPOP_WARN("_PRODH", 0,
				"Warning, both main and interrupt threads use PRODH(for Number Product), \n\
function %s module %s, function %s module %s \n\
which interrupt cannot save/restore!!\n");
		}
	}



	symp = findSymInModuleLD("STK00", fp->m, 1, 1);
	if (symp)
	{
		if (symp->symIsEQU)
		{
			CHKADDPUSHPOPEQU(getExpMinValue(symp->exp), 0, "STK00");
		}
		else
		{
			CHKADDPUSHPOP("STK00", 0);
		}
	}
	symp = findSymInModuleLD("STK01", fp->m, 1, 1);
	if (symp)
	{
		if (symp->symIsEQU)
		{
			CHKADDPUSHPOPEQU(getExpMinValue(symp->exp), 0, "STK01");
		}
		else
		{
			CHKADDPUSHPOP("STK01", 0);
		}
	}
	symp = findSymInModuleLD("STK02", fp->m, 1, 1);
	if (symp)
	{
		if (symp->symIsEQU)
		{
			CHKADDPUSHPOPEQU(getExpMinValue(symp->exp), 0, "STK02");
		}
		else
		{
			CHKADDPUSHPOP("STK02", 0);
		}
	}
	symp = findSymInModuleLD("_TBLPTRH", fp->m, 1, 1);
	if (symp)
	{
		if (symp->symIsEQU)
		{
			CHKADDPUSHPOPEQU(getExpMinValue(symp->exp), 0, "_TBLPTRH");
		}
		else
		{
			CHKADDPUSHPOP("_TBLPTRH", 0);
		}
	}
	if (!body_has_sfr2_stack())
	{
		symp = findSymInModuleLD("_TBLPTRL", fp->m, 1, 1);
		if (symp)
		{
			if (symp->symIsEQU)
			{
				CHKADDPUSHPOPEQU(getExpMinValue(symp->exp), 0, "_TBLPTRL");
			}
			else
			{
				CHKADDPUSHPOP("_TBLPTRL", 0);
			}
		}
	}
	symp = findSymInModuleLD("_PCLATH", fp->m, 1, 1);
	if (symp)
	{
		if (symp->symIsEQU)
		{
			CHKADDPUSHPOPEQU(getExpMinValue(symp->exp), 0, "_PCLATH");
		}
		else
		{
			CHKADDPUSHPOP("_PCLATH", 0);
		}
	}



	//CHKADDPUSHPOP("STK01", 0);
	//CHKADDPUSHPOP("STK02", 0);
	CHKADDPUSHPOP("STK03", 0); // after stk03 it is symbol
	CHKADDPUSHPOP("STK04", 0);
	CHKADDPUSHPOP("STK05", 0);
	CHKADDPUSHPOP("STK06", 0);
	CHKADDPUSHPOP("STK07", 0);
	if (NEEDPUSHPOPFSR(0))
	{
		ADDEXTSYM("_FSR0L");
		ADDEXTSYM("_FSR0H");
		addPushPopSym(fp, "_FSR0L", 0);
		addPushPopSym(fp, "_FSR0H", 0);
	}
	if (NEEDPUSHPOPFSR(1))
	{
		ADDEXTSYM("_FSR1L");
		ADDEXTSYM("_FSR1H");
		addPushPopSym(fp, "_FSR1L", 0);
		addPushPopSym(fp, "_FSR1H", 0);
	}
	if (!body_has_sfr2_stack() && NEEDPUSHPOPFSR(2) )
	{
		ADDEXTSYM("_FSR2L");
		ADDEXTSYM("_FSR2H");
		addPushPopSym(fp, "_FSR2L", 0);
		addPushPopSym(fp, "_FSR2H", 0);
	}
	fp->interruptPushPullAdded = 1;
	return 0;
}

funcRecord *k1;

int buildCallTree(int startA, int *finalA, int startXA, int *finalXA) // return <0 means fail
{
	static int stage0Done = 0;
	moduleLD *mdp;
	funcRecord *funcp;
	symbol *sym;
	int i,j;
	symbol *sp;
	
	char buffer[MAXSTRSIZE1*2+100];
	

	if (!stage0Done)
	{
		mainf = NULL;
		interruptf = NULL;
		for (mdp = topMap.linkedModules; mdp; mdp = mdp->next)
		{
			for (i = 0; i < mdp->funcNum; i++)
			{
				funcp = mdp->funcs[i];
				//if (!strncmp(funcp->name, "_main", 6))
				if(funcp->namehash==hashMain)
				{
					mainf = funcp;
					if (interruptf != NULL)
						break;
					continue;
				}
				if (interruptf==NULL && 
					//!strncmp(funcp->areap->name, "c_interrupt", 12))
					funcp->areap->nameHash == hashInt)
				//if(funcp->areap->namehash==hashInt)
				{
					interruptf = funcp;
					if (mainf != NULL)
						break;
					continue;
				}
				//if (!strcmp(funcp->name, "_k1"))
				//{
				//	k1 = funcp;
				//	continue;
				//}

			}
			if (mainf != NULL && interruptf != NULL)
				break;

		}

		// check if pcall


		maxLevel = 0;
		if (!mainf)
			return 0; // just skip
		traceCallDepth_pre(mainf, 0, 0);
		mainThreadCallLevel = maxLevel;
		if (interruptf)
		{
			maxLevel = 0;
			traceCallDepth_pre(interruptf, 0, 1);
			interruptThreadCallLevel = maxLevel + 1;
		}



		for (mdp = topMap.linkedModules; mdp; mdp = mdp->next)
		{
			for (i = 0; i < mdp->funcNum; i++)
			{
				dataItem *dip;
				funcRecord *fp1;
				funcp = mdp->funcs[i];

				if (funcp->calledFuncNum == 0)
				{
					for (dip = funcp->callList; dip; dip = dip->next)
					{
						if (dip->exp->type != ExpIsSymbol)
							continue;
						fp1 = findFuncByHash(dip->exp->symHash, funcp->m);
						if (fp1 == NULL)
						{
							//fprintf(stderr, "internal error, cannot find called function %s\n", dip->exp->exp.symbol);
							snprintf(buffer, sizeof(buffer) - 1, "Warning, seems inlined function %s\n", dip->exp->exp.symbol);
							appendErrMsg(buffer);
							fprintf(stderr, "%s", buffer);
							//return -1;
						}
						else
							funcp->calledFunc[funcp->calledFuncNum++] = fp1;
					}
				}
			}
		}

		// spread them first



		for (mdp = topMap.linkedModules; mdp; mdp = mdp->next)
		{
			for (i = 0; i < mdp->funcNum; i++)
			{
				funcp = mdp->funcs[i];
//#ifdef _DEBUG
//				if (!strcmp(funcp->name, "_DTMFDecodeCIDAction"))
//					fprintf(stderr, "dtmfdecode action\n");
//#endif

				if (!funcp->calledByFPTR && ifLabelReferencedThoughPointer(funcp->namehash))
					funcp->calledByFPTR = 1;
				if (body_has_sfr2_stack())
				{


					if (!funcp->p41ReEntry && !funcp->h08dReEntry && funcp->calledByFPTR)
					{
						//int k;
						snprintf(buffer, MAXSTRSIZE1 * 2 + 99, "Info: Module %s function %s seems called by function pointer.\n",
							funcp->m->name, funcp->name);
						appendErrMsg(buffer);

						set_tree_fsr2(funcp, 0,1);


					}
					else if (funcp->h08dReEntry)
						set_tree_fsr2(funcp, 0,0);


				} // for h08d, fsr2 is default operation!!
				// 2022 oct, remove following checks
				//if (!strncmp(funcp->name, "_main", 6))
				//	mainf = funcp;
				//if (!strncmp(funcp->areap->name, "c_interrupt", 12))
				//interruptf = funcp;

			}

		}

		// another loop to sread local
		for (mdp = topMap.linkedModules; mdp; mdp = mdp->next)
		{
			for (i = 0; i < mdp->funcNum; i++)
			{
				funcp = mdp->funcs[i];
				if (spreadFuncRecord(funcp, mdp) < 0)
				{
					return -1;
				}
			}
		}
		stage0Done = 1;
	}
	
	*finalA = startA;
	*finalXA = startXA;
	if (!mainf)// old version
		return 0;

	resetFuncBaseAddr(); // before both main/interrupt
	traceCallDepth(mainf, 0, 0, startA, finalA, startXA, finalXA);
	if (interruptf)
	{
		//if ( mainf)
		//	addInterruptPushPull(); // later after switch case fixed
		traceCallDepth(interruptf, 0, 1, *finalA, finalA, startXA, finalXA);
	}
	// append an EQU for final stack address
	
	uint64 hashid = hash("a_STKTOP");
	sp = findExportSym(hashid, NULL);
	
	if (sp == NULL)
	{
		fprintf(stderr, "Linker internal error. a_STKTOP cannot found.\n");
		exit(-__LINE__);
	}
		//sp = newSymbol("a_STKTOP");
		//sp->symIsByLinker = 1;
		//sp->symIsEQU = 1;
		//sp->symIsGlobl = 1;
	sp->exp = newConstExp(*finalA, -1); // there is a finalA
		//topMap.linkerGeneratedSym[topMap.generatedSymNum++] =
			//sp;

	// now check if some are not allocated

	// 2017 07:
	// it is possible we found foo is not used,
	// but foo calls bar
	// we need to check if bar is not used,
	// that is, this needs iterations!!


//#ifdef _DEBUG 
//	checkLine109();
//#endif

	do {
		need_remap = 0;
		for (mdp = topMap.linkedModules; mdp; mdp = mdp->next)
		{
			for (i = 0; i < mdp->funcNum; i++)
			{
				funcp = mdp->funcs[i];

				if (funcp->p41ReEntry && !funcp->proemi_added) // sometimes, it is by pcall, not in tree built
				{
					add_prologue_epilogue(funcp);
					//getFSR2RollingOffset(funcp);
				}
				if (funcp->paraOnStack || funcp->p41ReEntry ||funcp->h08dReEntry)
				{
					// 2020 may, if linker need more local variable, it should do it again
					int resultfsr2;
					do {
						resultfsr2 = getFSR2RollingOffset(funcp); // return 1 means it adds local variable funcname_stktemp
					} while (resultfsr2);
				}


				if ((!funcp->p41ReEntry) && (!funcp->h08dReEntry) && (funcp->baseLocalAddr == 0 || funcp->needReAssign))
				{
					if (funcp == mainf || funcp == interruptf)
						continue;
					if (ifLabelReferenced(funcp->namehash))//not allocated
					{
						// we check if it is in fp
						// if yes, we allocate independently
						// because it is fp
						funcp->baseLocalAddr = *finalA;
						*finalA += funcp->localSymSize;
						if (verbose)
						{
							//fprintf(stderr, "Module %s function %s seems called by function pointer, local allocate at %X\n",
							//	mdp->name, funcp->name, funcp->baseLocalAddr);
							snprintf(buffer, MAXSTRSIZE1*2+99, "Module %s function %s seems called by function pointer, local allocate at %X\n",
								mdp->name, funcp->name, funcp->baseLocalAddr);
							appendErrMsg(buffer);
						}
						funcp->needReAssign = 1;
						//need_remap = 1;

					}
					else
					{
						//fprintf(stderr, "Warning, function %s local not allocated.\n",
						//	funcp->name);
						if (keep_unused_function) // default is remove the function
						{
							if (!funcp->noAllocWarned)
							{
								snprintf(buffer, sizeof(buffer)-1, "Warning, function %s local variable not allocated.\n",
									funcp->name);
								appendErrMsg(buffer);
								funcp->noAllocWarned = 1;
							}
						}
						else
						{
							if (!funcp->noAllocWarned && !strstr(funcp->name,"_cptrget"))
							{
								snprintf(buffer, sizeof(buffer)-1, "Warning, function %s not used will be optimized out.\n",
									funcp->name);
								appendErrMsg(buffer);
								funcp->noAllocWarned = 1;
								need_remap = 1;
							}
						}


					}
				}
			}

		}
	} while (need_remap);

	// now allocate the local variables
	for (mdp = topMap.linkedModules; mdp; mdp = mdp->next)
	{
//#ifdef DEBUG_DAOFF
//		fprintf(stdout, "processing module %s:\n", mdp->name);
//#endif
		for (i = 0; i < mdp->funcNum; i++)
		{
			int addrnow;
			funcp = mdp->funcs[i];
#ifdef DEBUG_DAOFF
			fprintf(stdout, "processing function %s:\n", funcp->name);
#endif
			addrnow = funcp->baseLocalAddr;
			for (j = 0; j < funcp->localSymNum; j++)
			{
				sym = funcp->localSym[j];
				if (!sym->symIsLabel)
				{
					if (sym->symIsEQU && sym->fpHasthis)
					{
						sym->exp = newConstExp(addrnow++,-1);
						continue;
					}
					//if (!strstr(sym->name, "_STK"))
					if(!sym->symNoLabel)
					{
						fprintf(stderr, "Linker internal error, symbol %s local not label.\n", sym->name);
						exit(-3000);
					}
					//else
					//{
					//	sym->exp = newConstExp(0x55, -1);// it should be optimized later
					//	continue;
					//}
				}
				if (addrnow <= 0x180 && addrnow + sym->symSize > 0x180)
					addrnow = 0x200;
				if (sym->labelPos == NULL)
				{
					if(sym->ap==NULL)
						
					sym->labelPos = newWDATA(funcp->areap);
					sym->labelPos->m = sym->ldm;
					sym->symNoLabel = 1;
					sym->symIsLabel = 1;
					sym->labelPos->next = sym->labelPos;
					sym->labelPos->sizeOrORG = 1;
				}
				
					sym->labelPos->areaOffset = addrnow;
					//sym->labelPos->next->areaOffset = addrnow;
				
#ifdef DEBUG_DAOFF
				fprintf(stdout, "%s assigned to %03X\n", sym->name, addrnow);
#endif
				addrnow += sym->symSize;
			}
			addrnow = funcp->baseLocalXAddr;
			for (j = 0; j < funcp->localXSymNum; j++)
			{
				sym = funcp->localXSym[j];
				if (!sym->symIsLabel)
				{
					if (sym->symIsEQU && sym->fpHasthis)
					{
						sym->exp = newConstExp(addrnow++, -1);
						continue;
					}
					fprintf(stderr, "Linker internal error, symbol %s local not label.\n", sym->name);
					exit(-3001);
				}
				if (addrnow <= 0x180 && addrnow + sym->symSize > 0x180)
					addrnow = 0x200;
				sym->labelPos->areaOffset = addrnow;
				sym->labelPos->next->areaOffset = addrnow;
#ifdef DEBUG_DAOFF
				fprintf(stdout, "%s assigned to %03X\n", sym->name, addrnow);
#endif
				addrnow += sym->symSize;

			}
			
		}

	}


	if(mainf)
		mainLocalStackSize = findMaxLocalStack(mainf, 0, 0, &isMainRecursive);
	if(interruptf)
		interruptLocalStackSize = findMaxLocalStack(interruptf, 0, 0, &isInterruptRecursive);
	return 0;

	// now check if d
}

int appendAreaSym(char *name)
{
	symbol *sp;
	uint64 hashid = hash(name);
	if (!findExportSym(hashid, NULL))
	{
		sp = newSymbol(name);
		sp->symIsByLinker = 1;
		topMap.linkerGeneratedSym[topMap.generatedSymNum++] =
			sp;
		return 1;
	}
	return 0;
}

int append_symbols_of_an_area(area *ap)
{
	char buffer[MAXSTRSIZE1];
	int count = 0;
	int len = MAXSTRSIZE1 - strlen(ap->name) - 5;
	strcpy(buffer, "l_");
	strncat(buffer, ap->name,len);
	count += appendAreaSym(buffer);
	strcpy(buffer, "s_");
	strncat(buffer, ap->name, len);
	count += appendAreaSym(buffer);
	strcpy(buffer, "e_");
	strncat(buffer, ap->name, len);
	count += appendAreaSym(buffer);

	return count;
}

int append_area_symbols(void)
{
	area *ap;
	int count=0;
	for (ap = topMap.romArea; ap; ap = ap->next)
		count +=append_symbols_of_an_area(ap);
	for (ap = topMap.dataArea; ap; ap = ap->next)
		count +=append_symbols_of_an_area(ap);
	for (ap = topMap.xdataArea; ap; ap = ap->next)
		count +=append_symbols_of_an_area(ap);
	return count;
}

int readLibLines(void)
{
	int i,j;
	int count = 0;
	char fpath[PATH_MAX];
	for (i = 0; i < libFileCount; i++)
	{
		libLines[i] = NULL;
		
		if ((j=readFile2SrcLine2(libBasename[i], &libLines[i], &libLineTails[i], searchPath, fpath, PATH_MAX,NULL,NULL,"")) < 0)
		{
			fprintf(stderr, "Linker Error: open lib %s failed, abort.\n", libBasename[i]);
			exit(-20);
		}
		libFullPath[i] = strdup(fpath);
		count += j;
	}
	return count;
}


// note that the return is the module
// line of that lib!!
srcLine *findExportInSrcList(char *symName, srcLine *srcList)
{
	srcLine *srcp=NULL;
	uint64 symHash = hash(symName);
	for (; srcList;srcList=srcList->next)
	{
		//if (!strncmp(srcList->line, "$MOD", 4))
		if(srcList->modLine)
		{
			srcp = srcList;
			continue;
		}
		//if (strncmp(srcList->line, "$EXPORT:", 8))
		if(!srcList->exportLine)
			continue;
		//if (!strncmp(symName, srcList->line + 8, MAXSTRSIZE1 - 8))
		if(symHash == srcList->expHash)
		{
			return srcp; // this is the module!!
		}
	}
	return NULL;
}

srcLine *findSymInLibs(char*symName, int *libIndex)
{
	int i;
	srcLine *srcp;
	*libIndex = -1;
	for (i = 0; i < libFileCount; i++)
	{
		srcp = findExportInSrcList(symName, libLines[i]);
		if (srcp != NULL)
		{
			*libIndex = i;
			return srcp;
		}
	}
	return NULL;
}



// generally we can over max a li

int random01(void)
{
	if (rand() >= (RAND_MAX / 2))
		return 1;
	return 0;
}

static int calRamAreaRandom(area *alist, int start, int xstart, int keepOld)
{
	srand((unsigned int) time(NULL));
	area *ap;
	wData *wdp;
	int offset = localMaxAddr; // from global
	int offsetX = xlocalMaxAddr; // from global var xlocalmax
	int offsetNow;
	int abscount;
	int totalcount;
	int isIDATA = 0;
	int isXDATA = 0;
	char buffer[4096];
	
	for (abscount = 0, totalcount = 0, ap = alist; ap; ap = ap->next)
	{
		if (ap->isABS)
			abscount++;
		totalcount++;
	}
	/*
	if (abscount && abscount < totalcount)
	{
	fprintf(stderr,"1 and only 1 abs area is allowed in CODE/DATA/XDATA, abort.\n");
	exit(-30);
	}*/

	for (ap = alist; ap; ap = ap->next)
	{
		//if (!strcmp(ap->name, "UDATA"))
		//	fprintf(stderr, "UDATA\n");
		// for IDATA, IDATAX, only static, we skip local for linker?
		isIDATA = 0;
		if (strstr(ap->name, "IDATA"))
			isIDATA = 1;
		isXDATA = 0;
		if (strstr(ap->name, "XDATA") || strstr(ap->name,"IDATAX"))
			isXDATA = 1;
		// no ROM

		for (wdp = ap->wDatas; wdp; wdp = wdp->next)
		{

			if (wdp->areaOffset > 0 && keepOld)
				continue;
			if (ap->region == regionCODE)
				wdp->areaOffset = offset;
			if (!wdp->wdIsOrg) // no ORG for C codes
			{
				if (wdp->wdIsLabel)
				{
					// skip func local at this phase
					if (!isIDATA && wdp->sym->symIsFuncLocal && wdp->next && wdp->next->wdIsBlank)
					{
						if (wdp->ap->region==regionXDATA)
							wdp->next->wdBlankIsFuncLocalX = 1;
						else
							wdp->next->wdBlankIsFuncLocal = 1;
					}
					else
					{
						if (isXDATA ||(!isIDATA && random01()))
						{
							wdp->areaOffset = offsetX;
							offsetNow = 1;
						}
						else
						{
							wdp->areaOffset = offset; // this is the assignment !!
							offsetNow = 0;
						}
					}
				}
				else
				{

					if (isIDATA || (!wdp->wdBlankIsFuncLocal && !wdp->wdBlankIsFuncLocalX)) // funclocal skip first
					{
						if (offsetNow)
						{
							wdp->areaOffset = offsetX;
							offsetX += wdp->sizeOrORG;
						}
						else
						{
							wdp->areaOffset = offset;
							offset += wdp->sizeOrORG;
						}
						// this is the assignment !!
					}
				}
			}
			else
			{
				if (ap->region == regionCODE)
					offset = wdp->sizeOrORG * 2; // use word
				else if (!dataOrgByte)
					offset = wdp->sizeOrORG * 2;
				else
					offset = wdp->sizeOrORG; // ORG is assign!!
				wdp->areaOffset = offset;
			}


		}
		if (offset > 0x3ff)
		{
			snprintf(buffer, 4096, "Warning, region %s end address is %X, higher than %X",
				(alist->region == regionCODE) ? "CODE" : (alist->region == regionDATA) ? "DATA" : "XDATA",
				offset, 0x3ff
			);
			appendErrMsg(buffer);
		}
		// calculate the start/end address
		ap->startAddr = 655360; // it is not possible
		ap->endAddr = 0;
		for (wdp = ap->wDatas; wdp; wdp = wdp->next)
		{
			if (!wdp->wdIsOrg && !wdp->wdIsLabel && (isIDATA || (!wdp->wdBlankIsFuncLocal && !wdp->wdBlankIsFuncLocalX)))
			{
				if (wdp->areaOffset < ap->startAddr && wdp->sizeOrORG)
					ap->startAddr = wdp->areaOffset;
				if (wdp->areaOffset + wdp->sizeOrORG > ap->endAddr)
				{
					ap->endAddr = wdp->areaOffset + wdp->sizeOrORG;
					ap->occuLength = ap->endAddr - ap->startAddr;
				}
			}
		}
		if (ap->startAddr == 655360)
			ap->startAddr = 0; // no set of address
		if (ap->occuLength != 0 && verbose)
			fprintf(stderr, "get area %s start %X end %X length %X\n", ap->name, ap->startAddr, ap->endAddr, ap->occuLength);

		//if (ap->endAddr>0)
			//*finalA = ap->endAddr;

	}
	return 0;
}

static int getAreaBlankSize(area *ap)
{
	wData *wdp;
	int size = 0;
	for (wdp = ap->wDatas; wdp && wdp != ap->wDataTail; wdp = wdp->next)
	{
		if (wdp->wdIsBlank)
			size += wdp->sizeOrORG;
	}
	// final one
	if(wdp && wdp->wdIsBlank)
		size += wdp->sizeOrORG;
	return size;
}

static int checkAreaStartAssigned(area *a, int min, int max)
{
	int i;
	int found = 0;
	for (i = 0; i < startAssignedAreaNum; i++)
	{
		if (a->nameHash == startAssignedAreaNameHash[i])
		{
			found = 1;
			assignedAreaUsed[i] = 1; // it is used. if not used, show the warning.
			break;
		}
	}
	if (!found)
		return 0;
	if (startAssignedAreaAddr[i] < min)
	{
		fprintf(stderr, "Linker error: area %s assigned address < limit 0x%X, fail.\n", a->name, min);
		exit(-__LINE__);
	}
	if (startAssignedAreaAddr[i] > max)
	{
		fprintf(stderr, "Linker error: area %s assigned address > limit 0x%X, fail.\n", a->name, max);
		exit(-__LINE__);
	}
	return startAssignedAreaAddr[i];
}

static int calAreaListWDATA(area *alist, int offsetstart, int maxoffset, int *finalA, int keepOld, int isROM, int *middleRAMA)
{
	area *ap;
	area *idatap = NULL;
	wData *wdp;
	int offset = offsetstart;
	static uint64 idataromNameHash = 0;
	static uint64 xdataromNameHash = 0;
	int abscount;
	int totalcount;
	int isIDATA=0;
	int idataAllocated = 0;
	int idataStartA = -1;
	int idataEndA = -1;
	char buffer[4096];
	//int isROM = (offsetstart == 0); // word align!!
	if (!idataromNameHash)
	{
		idataromNameHash = hash("IDATAROM");
	}
	if (!xdataromNameHash)
	{
		xdataromNameHash = hash("XDATAROM");
	}
	for (abscount=0,totalcount=0, ap = alist; ap; ap = ap->next)
	{
		if (ap->isABS)
			abscount++;
		totalcount++;
	}
	// for IDATA we calculate the size and allocate it first

	if (!isROM && offsetstart!=0x80 && offsetstart < 0x200)
	{
		int idataSize = 0;
		for(ap=alist;ap;ap=ap->next)
			if (strstr(ap->name, "IDATA"))
			{
				idatap = ap;
				break;
			}
		// we need to calculate its size
		if (idatap)
		{
			int onGrid = 0; // make sure idata data not cross 0xff~0x100
			int lastStartA;
			idatap->occuLength = idataSize = getAreaBlankSize(idatap);
			if (idataSize > (0x180 - offsetstart)) // if idata large, we put it high addr
			{
				offset = 0x200; // start from 0x200
				idatap->startAddr = 0x200;
			}
			else
			{
				offset = offsetstart; // we allocate it first
				idatap->startAddr = offsetstart;
			}
			lastStartA = idatap->startAddr;
			while (!onGrid)
			{
				onGrid = 1;
				for (wdp = idatap->wDatas; wdp; wdp = wdp->next)
				{

					if (wdp->areaOffset > 0 && keepOld)
						continue;
					if (!wdp->wdIsOrg)
					{
						if (wdp->wdIsLabel)
						{
							
							//if (offset < 0x180 && !isROM && wdp->next && wdp->next->wdIsBlank && (offset + wdp->next->sizeOrORG >= 0x180))
							//	offset = 0x200; // idata cannot jump!!
							if (!isROM &&  wdp->next && wdp->next->wdIsBlank && wdp->next->sizeOrORG <= 8 && wdp->next->sizeOrORG > 1 &&
								((offset & 0xf00) != ((offset + wdp->next->sizeOrORG - 1) & 0xf00)))
							{
								onGrid = 0;
								if (offsetstart == lastStartA)
									offsetstart++;
								lastStartA++;

								idatap->startAddr = lastStartA;
								offset = lastStartA;
								break;
							}
							wdp->areaOffset = offset; // this is the assignment !!
						}
						else
						{
							// we need special care if IDATA < 0x200	
							if (isIDATA || (!wdp->wdBlankIsFuncLocal && !wdp->wdBlankIsFuncLocalX)) // funclocal skip first
							{
								// special case, idata cannot cross boundary with size!=1
								if (!isROM && isIDATA && offset < 0x180 && (offset + wdp->sizeOrORG)>0x180)
								{
									if (wdp->wdIsLabel)
									{
										fprintf(stderr, "Error, Initialized data %s cross 0x180 to 0x200 is not allowed, please re-arrange them.\n",
											wdp->sym->name);
									}
									else
									{
										wData *wlab = findPrevWdp(wdp);
										if (wlab->wdIsLabel)
											fprintf(stderr, "Error, Initialized data %s cross 0x180 to 0x200 is not allowed, please re-arrange them.\n",
												wlab->sym->name);
										else
											fprintf(stderr, "Error, Initialized data UNKNWON cross 0x180 to 0x200 is not allowed, please re-arrange them.\n");
									}
									exit(-__LINE__);
								}
								if (!isROM && offset == 0x180)
									offset = 0x200;
								wdp->areaOffset = offset;
								offset += wdp->sizeOrORG;
								// this is the assignment !!

							}
						}
					}
					else
					{
						offset = wdp->sizeOrORG; // ORG is ... idata org is error 
						wdp->areaOffset = offset;
					}


				}
			}
			if (offset > maxoffset)
			{
				snprintf(buffer, 4096, "Warning, region %s end address is %X, higher than %X",
					(alist->region == regionCODE) ? "CODE" : (alist->region == regionDATA) ? "DATA" : "XDATA",
					offset, maxoffset
				);
				appendErrMsg(buffer);
			}
			idatap->endAddr = offset;
			if (idatap->startAddr != offsetstart)
				offset = offsetstart;
			else
			{
				if (offset >= 0x180)
					offset = 0x200;
			}
			

		}
	}

	for (ap = alist; ap; ap = ap->next)
	{
		int isUDATA = 0;
		int isIDATA = 0;
		int UDATAstkSize = 0;
		if (!strcmp(ap->name, "UDATA"))
			isUDATA = 1;
			//fprintf(stderr, "UDATA\n");
		// for IDATA, IDATAX, only static, we skip local for linker?
		if (strstr(ap->name, "IDATA"))
			isIDATA = 1;
		if (isROM && (offset & 1))
			offset++; // word aligh!!


		if (isROM && ap->assignedAddr)
		{
			if (ap->assignedAddr < offset / 2)
			{
				fprintf(stderr, "Linker error!! area %s start address 0x%X overlapped!!, please re-assign.\n",
					ap->name, ap->assignedAddr);
			}
			offset = ap->assignedAddr;
		}

		if (ap == idatap)
			continue; // allocated

		// specialcase UDATA
		if (isUDATA)
		{
			// we need to make sure all STK0x at the same bank, or we must shift the offset
			int i, j;
			char label[100];
			symbol *symp;
			for (i = 3; i < 8; i++) // STK3~STK6 are banked memory for longlong, they must stay at the same address
			{
				snprintf(label, 99, "STK%02d", i);
				symp = findExportSym(hash(label), NULL);
				if (!symp)
					break;
				j = symp->labelPos->next->sizeOrORG;
				UDATAstkSize += j;
			}
			if (UDATAstkSize)
			{
				// we must make sure udata STKxx at same bank
				if (offset<0x100 && offset>(0x100 - UDATAstkSize))
					offset = 0x100; // promote!!
				if (offset<0x180 && offset>(0x180 - UDATAstkSize) && maxoffset>0x200)
					offset = 0x200;
			}
			if(middleRAMA!=NULL && offset<=0x180)
				*middleRAMA = offset;

		}


		for (wdp = ap->wDatas; wdp; wdp = wdp->next)
		{
			
			if (wdp->areaOffset > 0 && keepOld)
				continue;
			if (ap->region == regionCODE)
			{

				wdp->areaOffset = offset;
			}
			if (!wdp->wdIsOrg)
			{
				if (wdp->wdIsLabel)
				{
					// skip func local at this phase
					if (!isIDATA && wdp->sym->symIsFuncLocal && wdp->next && wdp->next->wdIsBlank)
					{
						if(offsetstart>=0x100)
							wdp->next->wdBlankIsFuncLocalX = 1;
						else
							wdp->next->wdBlankIsFuncLocal = 1;
					}
					else
					{
						// 2018 FEB, gauge mcu 4xxx need skip 0x780~0x7ff, 0x852~0x0x87f
						// note that initialized data is still limited in 256 bytes

						// 2017 OCT, add multi-section data location
						if (HY08A_getPART() && HY08A_getPART()->isEnhancedCore == 2)
						{
							if (offset <= 0x780 && !isROM && wdp->next && wdp->next->wdIsBlank && (offset + wdp->next->sizeOrORG > 0x780))
							{

								offset = 0x800; // jump!!
												
							}
							if (offset <= 0x852 && !isROM && wdp->next && wdp->next->wdIsBlank && (offset + wdp->next->sizeOrORG > 0x852))
							{

								offset = 0x880; // jump!!
												
							}
						}

						if (offset <= 0x180 && !isROM && wdp->next && wdp->next->wdIsBlank && (offset + wdp->next->sizeOrORG > 0x180))
						{
							
							offset = 0x200; // jump!!
							// avoid idata
							if (idatap && idatap->endAddr > 0x200)
								offset = idatap->endAddr; // avoid collision!!

						}
						// we also keep var not crossing 00f0~0100 boundary
						if (!isROM &&  wdp->next && wdp->next->wdIsBlank && wdp->next->sizeOrORG <= 8 && wdp->next->sizeOrORG > 1 &&
							((offset & 0xf00) != ((offset + wdp->next->sizeOrORG - 1) & 0xf00)))
						{
							offset = (offset + 0x100) & 0xf00;
							
						}
						// prevent overlap with idata
						
						if (idatap && wdp->next->wdIsBlank && offset + wdp->next->sizeOrORG<idatap->endAddr &&
							offset + wdp->next->sizeOrORG> idatap->startAddr)
							offset = idatap->endAddr;

						if (offset < 0x180 && !isROM && wdp->next && wdp->next->wdIsBlank && (offset + wdp->next->sizeOrORG > 0x180)) // ==0x180 should be ok
							offset = 0x200; // jump!!


						wdp->areaOffset = offset; // this is the assignment !!
					}
				}
				else
				{
					// we need special care if IDATA < 0x200	
					if (isIDATA || (!wdp->wdBlankIsFuncLocal && !wdp->wdBlankIsFuncLocalX)) // funclocal skip first
					{
						// special case, idata cannot cross boundary with size!=1
						if (!isROM && isIDATA && offset < 0x180 && (offset + wdp->sizeOrORG)>0x180)
						{
							if(middleRAMA!=NULL && offset<=0x180)
								*middleRAMA = offset;

							if (wdp->wdIsLabel)
							{
								fprintf(stderr, "Error, Initialized data %s cross 0x180 to 0x200 is not allowed, please re-arrange them.\n",
									wdp->sym->name);
							}
							else
							{
								wData *wlab = findPrevWdp(wdp);
								if (wlab->wdIsLabel)
									fprintf(stderr, "Error, Initialized data %s cross 0x180 to 0x200 is not allowed, please re-arrange them.\n",
										wlab->sym->name);
								else
									fprintf(stderr, "Error, Initialized data UNKNWON cross 0x180 to 0x200 is not allowed, please re-arrange them.\n");
							}
							exit(-__LINE__);
						}
						if (!isROM && offset == 0x180 && (wdp->wdIsLabel  ||  wdp->sizeOrORG ))
						{
							if(middleRAMA!=NULL && offset<=0x180)
								*middleRAMA = offset;

							offset = 0x200;
						}
						wdp->areaOffset = offset;
						offset += wdp->sizeOrORG;
						if(middleRAMA!=NULL && offset<=0x180)
								*middleRAMA = offset;

						 // this is the assignment !!
						
					}
				}
			}
			else
			{
				if (ap->region == regionCODE)
					offset = wdp->sizeOrORG * 2; // use word
				else if (!dataOrgByte)
					offset = wdp->sizeOrORG * 2;
				else
					offset = wdp->sizeOrORG; // ORG is assign!!
				wdp->areaOffset = offset;
			}


		}
		if (offset > maxoffset)
		{
			snprintf(buffer,4096, "Warning, region %s end address is %X, higher than %X",
				(alist->region == regionCODE) ? "CODE" : (alist->region == regionDATA) ? "DATA" : "XDATA",
				offset, maxoffset
				);
			appendErrMsg(buffer);
		}
		// calculate the start/end address
		ap->startAddr = 655360; // it is not possible
		ap->endAddr = 0;
		for (wdp = ap->wDatas; wdp; wdp = wdp->next)
		{
			if (!wdp->wdIsOrg && !wdp->wdIsLabel && (isIDATA || (!wdp->wdBlankIsFuncLocal && !wdp->wdBlankIsFuncLocalX)))
			{
				if (wdp->areaOffset < ap->startAddr)
					ap->startAddr = wdp->areaOffset;
				if (wdp->areaOffset + wdp->sizeOrORG > ap->endAddr)
				{
					ap->endAddr = wdp->areaOffset + wdp->sizeOrORG;
					ap->occuLength = ap->endAddr - ap->startAddr;
				}
			}
		}
		if (ap->startAddr == 655360)
			ap->startAddr = 0; // no set of address
		if(ap->occuLength!=0 && verbose)
			fprintf(stderr, "get area %s start %X end %X length %X\n", ap->name, ap->startAddr, ap->endAddr, ap->occuLength);

		if(ap->endAddr>0)
			*finalA = ap->endAddr;

	}
	return 0;
}

// it is unavoidable that we need to move the area
static int moveWdata(area *sourceArea, area *targetArea, wData *startw, wData *endw) // give end will be good
{
	wData *wdp;
	wData *wdpPre=NULL;
	for (wdp = sourceArea->wDatas; wdp != sourceArea->wDataTail; wdpPre=wdp, wdp = wdp->next)
	{
		if (wdp == startw)
			break;
	}
	if (wdp == NULL)
	{
		fprintf(stderr, "Linker internal error at %s:%d\n", __FUNCTION__, __LINE__);
		exit(-__LINE__);
	}
	if (wdpPre == NULL)
		sourceArea->wDatas = endw->next;
	else
	{
		wdpPre->next = endw->next;
		endw->next->prev = wdpPre;
	}
	if (sourceArea->wDataTail == endw)
		sourceArea->wDataTail = wdpPre;
	if (targetArea->wDatas == NULL)
	{
		targetArea->wDatas = startw;
		targetArea->wDataTail = endw;
	}
	else
	{
		targetArea->wDataTail->next = startw;
		startw->prev = targetArea->wDataTail;
		targetArea->wDataTail = endw;
	}
	endw->next = NULL; // it is on the tail
	return 1;
}

// if multi segments are assigned, we need to sort them
int sortAddrAsssignedArea(area **alist)
{
	area ** areaArray;
	int areaNum;
	area *ap;
	int i, j;
	for (areaNum = 0, ap = *alist; ap; ap = ap->next)
	{
		ap->assignedAddr = checkAreaStartAssigned(ap, 0, 1024 * 1024*2) *2 ; // word to byte
		areaNum++;
	}
	areaArray = (area **)calloc(areaNum,sizeof(area*));
	for (areaNum = 0, ap = *alist; ap; ap = ap->next)
	{
		areaArray[areaNum] = ap;
		areaNum++;
	}
	// sort them
	for (i = 0; i < areaNum-1; i++)
	{
		for (j = 0; j < areaNum - i-1; j++)
		{
			if (areaArray[j]->assignedAddr > areaArray[j + 1]->assignedAddr)
			{
				ap = areaArray[j];
				areaArray[j] = areaArray[j + 1];
				areaArray[j + 1] = ap;
			}
		}
	}
	*alist = areaArray[0];
	ap = *alist;
	for (i = 1; i < areaNum; i = i + 1)
	{
		ap->next = areaArray[i];
		ap = ap->next;
	}
	ap->next = NULL;

	free(areaArray);
	return 1;
}

// for flat model, Skip Instruction and the following 
// code should use variables in the same page
// or the LBSR insertion will fail the skip instruction
// also, there are possibilities that label can follow the skip instruction!!

// second, it is possible continuous skip instructions are used
// LIKE
//     BTSS a
//     BTSC b
//     MVFW c
// I really hate that, but it happens
// in that case, A,B,C must be in the same page!!

// also it is possible that some of them in the zero page, like _STATUS
// it can be skipped because later BSR insertion will insert before the bit skip 
//static int adjustRAMBySkipInstructions(void)
//{
//	// see the skip instructions by ROM code
//	wData * skI[32768]; // ok max is 32768
//	wData * skIN[32768]; 
//	symbol * needZP[128]; // maximum 128 variables
//	int needZPnum = 0;
//	int skNum=0; // number of skip instructions
//	area *areap;
//	wData *wdp;
//	wData *wdp2;
//	int i;
//	symbol *var1;
//	symbol *var2;
//	uint64 h;
//	area *zp=NULL;
//	area *zprom = NULL;
//	area *udata = NULL;
//	area *idata = NULL;
//	area *idatarom = NULL;
//	
//	for (areap = topMap.romArea; areap; areap = areap->next)
//	{
//		for (wdp = areap->wDatas; wdp != areap->wDataTail; wdp = wdp->next) // final byte will not be skip!!
//		{
//			if (wdp->instHighByte && wdp->instSkip)
//			{
//				wdp2 = wdp->next;
//				while (!wdp2->instHighByte)
//					wdp2 = wdp2->next;
//				// we need to check if this instruction is BSR related
//				if (wdp2->bsrRelated)
//				{
//					skI[skNum] = wdp->next; // now byte is easier to process
//					skIN[skNum++] = wdp2->next; // save it
//				}
//			}
//		}
//	}
//	// now we have arrays of skip instructions
//	// we now check if the variable used and next instruction has possibily at different page
//	for (i = 0; i < skNum; i++)
//	{
////#ifdef _DEBUG
//		char buf1[256];
//		char buf2[256];
//		char *e1, *e2;
//		exprDump(skI[i]->exp, buf1, 255);
//		exprDump(skIN[i]->exp, buf2, 255);
////#endif
//		e1 = findSymInExp(skI[i]->exp);
//		e2 = findSymInExp(skIN[i]->exp);
//
//		if (e2 == NULL)
//			continue;
//		if (e1 == NULL)
//			var1 = NULL;
//		else
//			var1 = findSymInModuleLD(e1, skI[i]->m, 1, 1);
//		var2 = findSymInModuleLD(e2, skIN[i]->m, 1, 1);
//		if ( var2 == NULL) // skip for 
//			continue;
//		if (var1 && var1->symIsExt && !(var1->symIsEQU))
//			var1 = findExportSym(hash(var1->name), NULL);
//
//		// we skip if var1 zp equ
//		if (!e1 && getExpMinValue(skI[i]->exp) < 0x100) // it is ok to skip
//			continue;
//
//		if (!(var2->symIsEQU) && var2->symIsExt)
//			var2 = findExportSym(hash(var2->name), NULL);
//
//		
//		// if var2 is by FSR2, it is ok
//		if (var2->symIsByFSR2 || (var2->symIsEQU &&  getExpValue(var2->exp, NULL, 0, 0, 0, 0, NULL,skIN[i]) < 0x100))
//			continue;
//		if (e1 != NULL && e2 != NULL && !strcmp(e1, e2))
//			continue;
//		if ((var2 && var2->needZP) || (var1 && var1->needZP))
//			continue;
//		// in such case we need move var2 to special page
//		// it is possible it has initial value
//		// so we create 2 more segments
//		// one on data, one on ROM
//		// exception is that var2 is EQU 01XX
//		// var1 must be either 01XX or 00XX, or VAR1 need to be moved to special segment
//		if (var1 && var1->symIsEQU)
//			fprintf(stderr, "%s=0x%03X\n", var1->name, getExpValue(var1->exp, NULL, 0, 0, 0, 0, NULL, skI[i]));
//		else if (var1)
//			fprintf(stderr, "skip var1: %s area %s\n", var1->name, var1->ap->name);
//		else
//			fprintf(stderr, " skip var1: operand: %s\n", buf1);
//		if (var2->symIsEQU)
//		{
//			fprintf(stderr, "%s=0x%03X\n", var2->name, getExpValue(var2->exp, NULL, 0, 0, 0, 0, NULL, skIN[i]));
//			if (var1 && !var1->symIsEQU)
//			{
//				var1->needZP = 1;
//				needZP[needZPnum++] = var1;
//			}
//		}
//		else
//		{
//			fprintf(stderr, "skip var2: %s area %s\n", var2->name, var2->ap->name);
//			var2->needZP = 1;
//			needZP[needZPnum++] = var2;
//		}
//	}
//	// ok, we got the zp Num
//	// we move to new area named zp, zprom, which means zeropage and zeropage ROM
//	// ZP/ZPROM is included in assembler
//	// so we have to move them, and in the startup code, we allocate the data
//
//	if (!needZPnum)
//		return 0;
//
//	for (areap = topMap.dataArea; areap; areap = areap->next)
//	{
//		if (areap->nameHash == hash("IDATA"))
//			idata = areap;
//		
//		else if (areap->nameHash == hash("UDATA"))
//			udata = areap;
//	}
//	for (areap = topMap.stkArea; areap; areap = areap->next)
//	{
//		if (areap->nameHash == hash("ZP"))
//			zp = areap;
//	}
//	for (areap = topMap.romArea; areap; areap = areap->next)
//	{
//		if (areap->nameHash == hash("ZPROM"))
//			zprom = areap;
//		else if (areap->nameHash == hash("IDATAROM"))
//			idatarom = areap;
//	}
//	if (zp == NULL || zprom == NULL)
//	{
//		fprintf(stderr, "Linker internal error: ZP/ZPROM area not found.\n");
//		exit(-__LINE__);
//	}
//
//
//	if (needZPnum > 128)
//	{
//		fprintf(stderr, "Too many global/static variales used following SKIP instructions. Please change to LOCAL/STACK!!\n");
//		exit(-__LINE__);
//	}
//	for (i = 0; i < needZPnum; i++)
//	{
//		// move to zp/zpnum
//		symbol *sp = needZP[i];
//		char shadowName[256];
//		wData *wdplabel;
//		wData *wdpFin;
//		uint64 hashn;
//		strncpy(shadowName, sp->name, 255);
//		strncat(shadowName, "_shadow", 255);
//		hashn = hash(shadowName);
//		
//		if (sp->ap && sp->ap->nameHash == hash("IDATA"))// we need to find the corresponding _shadow
//		{
//			
//			wData *wdpirom;
//			wData *wdpfin;
//			
//			if (idata == NULL || idatarom == NULL)
//			{
//				fprintf(stderr, "Linter internal error, cannot find IDATA/IDATAROM.");
//				exit(-__LINE__);
//			}
//			
//			for (wdpirom = idatarom->wDatas; wdpirom; wdpirom = wdpirom->next)
//			{
//				if (wdpirom->wdIsLabel && wdpirom->sym->hashid == hashn)
//					break;
//			}
//			if (!wdpirom)
//			{
//				fprintf(stderr, "Linker Internal error.. %s label not found.\n", shadowName);
//				exit(-__LINE__);
//			}
//			// until next label, the label and dW will be move to ZPROM
//			// we need to find the final one
//			wdpirom->ap = zprom;
//			for (wdpfin = wdpirom->next; wdpfin; wdpfin = wdpfin->next)
//			{
//				wdpfin->ap = zprom;
//				if (wdpfin->next->wdIsLabel || wdpfin->next == NULL)
//					break;
//			}
//			moveWdata(idatarom, zprom, wdpirom, wdpfin);
//		}
//		else // create zero in ZPROM
//		{
//			wData *labelz = newWDATA1(zprom);
//			wData *dbz = newWDATA1(zprom);
//			
//			labelz->wdIsLabel = 1;
//			labelz->sym = newSymbol(shadowName);
//			labelz->sym->ap = zprom;
//			labelz->sym->labelPos = labelz;
//			labelz->next = dbz;
//			dbz->exp = newConstExp(0, -1);
//			dbz->sizeOrORG = sp->labelPos->next->sizeOrORG;
//			if (!zprom->wDatas)
//			{
//				zprom->wDatas = labelz;
//				zprom->wDataTail = dbz;
//			}
//			else
//			{
//				zprom->wDataTail->next = labelz;
//				zprom->wDataTail = dbz;
//			}
//		}
//		// now we move the ram to zp
//		wdplabel = sp->labelPos;
//		for (wdpFin = wdplabel->next; wdpFin; wdpFin = wdpFin->next)
//		{
//			if (wdpFin->next == NULL || wdpFin->next->wdIsLabel)
//				break;
//		}
//		moveWdata(wdplabel->ap, zp, wdplabel, wdpFin);
//		sp->ap = zp;
//		wdplabel->ap = zp;
//
//	}
//	return needZPnum;
//}

static int replaceExprSymbol(expression *exp, char *newSymName, uint64 newHash,  uint64 oldHash)
{
	switch (exp->type)
	{
	case ExpIsSymbol:
		if (exp->symHash == oldHash)
		{
			exp->symHash = newHash;
			strncpy(exp->exp.symbol, newSymName, sizeof(exp->exp.symbol)-1);
			return 1;
		}
		return 0;
	case ExpIsTree:
		return replaceExprSymbol(exp->exp.tree.left, newSymName, newHash, oldHash) +
			replaceExprSymbol(exp->exp.tree.right, newSymName, newHash, oldHash);
	default:
		return 0;
	}
	return 0;
}

// after reference symbols are found
// we calculate the WDATA of area first
// basically RAM can be defined first
static int cptrGetTune(area *romArea)
{
	wData *wdp;
	int i;
	for (;romArea;romArea=romArea->next)
	{
		for (wdp = romArea->wDatas; wdp&& wdp != romArea->wDataTail; wdp = wdp->next)
		{
			if (wdp->next && wdp->next->next && wdp->next->next->next && wdp->next->next->next->next &&
				wdp->next->next->next->next->next && wdp->next->next->next->next->next->next &&
				wdp->next->next->next->next->next->next->next)
			{
				if (!wdp->wdIsInstruction || !wdp->instHighByte)
					continue;
				if (wdp->exp->type == ExpIsNumber && wdp->exp->exp.number == 9 )// MVLP XXXX MVF XX CALL
				{
					//char buffer1[1024], buffer2[1024];
					//exprDump(wdp->next->next->exp, buffer1, 1023);
					//exprDump(wdp->next->next->next->exp, buffer2, 1023);
					//getExpMinValue(wdp->next->next->next->exp->exp.tree.left->exp.tree.left);
					int addrMvlp = getExpValue(wdp->next->next->next->exp->exp.tree.left->exp.tree.left, wdp->m, 0, wdp->areaOffset, 0, 0, NULL, wdp->next->next->next);
					if (isWdCALL(wdp->next->next->next->next->next->next) && (addrMvlp&1))
					{
						wData *wdCall = wdp->next->next->next->next->next->next;
						uint64 oldhash = hash("__cptrget1");
						uint64 newHash = hash("__cptrgeto");
		//				fprintf(stderr, "mvlpcall");
						// we need to change __cptrget1 to __cptrgeto if the MVLP uses ODD address
						i=replaceExprSymbol(wdCall->exp, "__cptrgeto", newHash, oldhash);
						i+=replaceExprSymbol(wdCall->next->exp, "__cptrgeto", newHash, oldhash);
						i+=replaceExprSymbol(wdCall->next->next->exp, "__cptrgeto", newHash, oldhash);
						i+=replaceExprSymbol(wdCall->next->next->next->exp, "__cptrgeto", newHash, oldhash);
						if (i == 0)
						{
							fprintf(stderr, "Warning, cptrget1/o change seems fail at %s:%d\n", wdCall->m->srcName, wdCall->lineno);
						}
						else
						{
							// make geto extern
							if (findSymInModuleLD("__cptrgeto", wdCall->m, 1, 1) == NULL)
							{
								// we need to add STK03 to reference list
								symbol *symp = newSymbol("__cptrgeto");
								symbol *symd;
								symp->ldm = wdCall->m;
								symp->symIsExt = 1;
								wdCall->m->refSym[wdCall->m->refSymNum++] = symp;
								symd = findExportSym(newHash, NULL);
								symp->definedPos = symd;
							}
						}
					}
				}
			}
		}
	}
	return 0;
}
extern int xStartAddr;
extern int xStartAssigned;
int calWDATAOffset(int ramrom) // 1 means RAM, 0 means ROM, 2 is debug mode
{
	// from code
	//int dataFinalA;
	//int xdataFinalA;
	int codeFinalA;
	int result=0;

	if (ramrom == 2)
	{
		result |= calRamAreaRandom(topMap.dataArea, dataMaxAddr, xdataMaxAddr, 0);
		result |= calRamAreaRandom(topMap.xdataArea, dataMaxAddr, xdataMaxAddr, 0);
		result |= calRamAreaRandom(topMap.stkArea, dataMaxAddr, xdataMaxAddr, 1);
	}else
	if (ramrom == 1) // 1 is ram, 2 is ROM
	{
		if (ramModel == RAM_MODEL_FLATD)
		{
			//adjustRAMBySkipInstructions();

			// from tail?
			int defaultStart;
			int needRelocation;
			int xmax;
			int firstNeedExpand = 0;
			if (sw_tgt_id == 0)
				find_tgtid();
			xmax = HY08A_getPART()->defMaxRAMaddrs+1; // it is ok even null
			if(xTailAssigned)
			{
				xmax = xTailAddr;
			}
			else if (xmax == 0x340)
				xmax = 0x300; // try not to use final ram
			// for h08d we try to move forward start address if ram
			// is not enough
			do {
				needRelocation=0;

				defaultStart=xdataMaxAddr;
				// 2020 MAY, try to make UDATA at the same BSR
				result |= calAreaListWDATA(topMap.dataArea, xdataMaxAddr, MAX_RAM_SIZE, &xdataMaxAddr, 0, 0, NULL);
				result |= calAreaListWDATA(topMap.xdataArea, xdataMaxAddr, MAX_RAM_SIZE, &xdataMaxAddr, 0, 0, NULL);
				if(xdataMaxAddr>xmax && !xStartAssigned)
				{
					int need=xdataMaxAddr-xmax;
					firstNeedExpand = 1;
					if(xdataMaxAddr>0x200 && xmax<=0x180)
						need-=0x80;
					if (need > 0x4)
						need = 0x4; // shift 4 bytes per move
					if(defaultStart> 0x90 && defaultStart-need>=0x80) // leave 0x10 for stack
					{
						xdataMaxAddr = defaultStart - need;
					
						if (xdataMaxAddr >= 0x180 && xdataMaxAddr < 0x200)
							xdataMaxAddr -= 0x80; // jump
						xStartAddr = xdataMaxAddr; // shift
						needRelocation=1;
					}
					// try to be high address aligned
				}
				else if (xmax == 0x180 && xdataMaxAddr < 0x180 && !xStartAssigned && !firstNeedExpand)
				{
					xdataMaxAddr = defaultStart + (xmax - xdataMaxAddr); // left one 
					xStartAddr = xdataMaxAddr; // shift
					needRelocation = 1;
					firstNeedExpand = 1; // try once only
				}
			}while(needRelocation);

			

		}
		else if (ramModel == RAM_MODEL_FLATA)
		{
			middleADDR=0x180;
			result |= calAreaListWDATA(topMap.dataArea, 0x80, MAX_RAM_SIZE, &dataMaxAddr, 0, 0,&middleADDR); // this is global
			// some block need large same block?			
			result |= calAreaListWDATA(topMap.xdataArea, 0x200, MAX_RAM_SIZE, &xdataMaxAddr, 0, 0,&middleADDR);
		}
		else
		{
			result |= calAreaListWDATA(topMap.dataArea, 0x80, 0x17f, &dataMaxAddr, 0,0,NULL); // this is global
			result |= calAreaListWDATA(topMap.xdataArea, 0x200, MAX_RAM_SIZE, &xdataMaxAddr, 0,0,NULL);
		}
		// special case, if not assigned localstk, we assign it , 
		// .. but we don't know which ones need to update to new

		if (ramModel == RAM_MODEL_FLATA)
		{
			// if((mainLocalStackSize+interruptLocalStackSize)<
			// 	(0x180-middleADDR))
			// {
			// 	result |= calAreaListWDATA(topMap.stkArea, middleADDR, MAX_RAM_SIZE, &middleADDR, 1,0,NULL);
			// }
			// else
			result |= calAreaListWDATA(topMap.stkArea, dataMaxAddr, MAX_RAM_SIZE, &dataMaxAddr, 1,0,NULL);
		}
		else
			result |= calAreaListWDATA(topMap.stkArea, dataMaxAddr, 0x180, &dataMaxAddr, 1, 0,NULL);

	}
	else if (ramrom == 0)
	{
		int i;
		static int cptrgetChecked = 0; // linking time optimzation
		sortAddrAsssignedArea(&topMap.romArea); // put assigned area to tail 

		result |= calAreaListWDATA(topMap.romArea, replaceOtp * (newOtpCodeWordOffset*2-8), MAX_ROM_SIZE, &codeFinalA, 0, 1,NULL); // use 16K first
		
		if (!cptrgetChecked)
		{
			cptrgetChecked = 1;
			cptrGetTune(topMap.romArea);

		}
		// check if assigned area not used
		for(i=0;i<startAssignedAreaNum;i++)
			if (!assignedAreaUsed[i])
			{
				char buf[1024];
				sprintf(buf, "Linker warning: assigned area %s with start address not found.\n", startAssignedAreaName[i]);
				appendErrMsg(buf);
			}
	}

	return result;
}

funcRecord * newEmptyFuncRecord(void)
{
	funcRecord *frec = calloc(1,sizeof(funcRecord));
	//memset(frec, 0, sizeof(funcRecord));
	frec->declaredParamSize = -1;
	return frec;
}

int getSymValue(symbol *sp, moduleLD *m, int lev, int romByteOffset, int div2RomSym, funcRecord *fpreq, int fsr2wdoffset,wData *wdp)
{
	int needshow = 0;
	if (!m && !sp->symIsEQU) // linker generated??
	{
		// l_ s_ e_

		area *ap = searchAreaLD(sp->name + 2, 0, 0, 0, 1);
		if (!ap  )
		{
			return 0; // don't care

		}
		switch (sp->name[0])
		{
		case 'l':
			return ap->occuLength;
		case 's':
			return ap->startAddr;
		case 'e':
			return ap->endAddr;
			
		}
		fprintf(stderr, "Linker Error: Wrong symbol %s has no module, internal error.\n", sp->name);
		exit(-130);
	}
	//if (!strcmp(sp->name, "r0x1234"))
		//fprintf(stderr, "1234\n");
	if (sp->symIsLabel)
	{
		if (sp->symIsByFSR2)//2 conditions, the function has it or call it pass
		{
			if (fpreq == sp->fpHasthis)
			{
//#ifdef _DEBUG
//				if (strstr(sp->name, "10A4"))
//					fprintf(stderr, "10a4\n");
//#endif
				//wData *wdp = sp->labelPos->next;
				//int size = 1;
				//if (wdp->wdIsBlank)
					//size = wdp->sizeOrORG;
				//return sp->fsr2origOffset + fsr2wdoffset + (size-1) + 0x100; // this is tricky
				// for h08c, it is 1XX
				// for h08d, it is 0XX!!
				int retV;
				int newoffset = sp->fsr2origOffset;
				if (sp->fsr2origOffset >= 0)
					newoffset = sp->fpHasthis->paramSTKReMap[newoffset];
				retV = newoffset + fsr2wdoffset; //80~FF is stack!!
				if (retV >= 0x80)
				{
					fprintf(stderr, "Error, around assembly line %s:%d, need access FSR2-%d,\n which means local stack is too large in function %s,\n module %s, please fix it.\n",
						wdp->m->srcName, wdp->lineno, retV, sp->fpHasthis->name, wdp->m->name);
					exit(-__LINE__);
				}
				else if (retV < 0) // -1 is allowed ??
				{
					fprintf(stderr, "Internal Error, %s:%d. Err at  %s:%d \n", __FILE__, __LINE__,(wdp && wdp->m)?
						wdp->m->srcName:"unknown",wdp->lineno);
					fprintf(stderr, "Info: error at get FSR2 offset of symbol %s, orig-offset %d, fsr2 code offset %d, get final offset %d.\n",
						sp->name,
						sp->fsr2origOffset,
						fsr2wdoffset,
						retV
					);
					
					exit(-__LINE__);
					//retV = 0;
				}
				if (fpreq->h08dReEntry)
				{
					// depends on the addressing mode,
					// for immediate addressing mode, MVL 0x01 SUBWF _FSR2L is correct
					// for direct addressing mode, MVL 0x81 is correct ... that is ... annoying
					// that is why we add a bit ... 
					//if(wdp->exp->ExpSymImmed)

					
					return retV+ ((wdp&& wdp->exp && wdp->exp->ExpSymImmed) ? 0 : 0x80);
					
				}
				//retV=newoffset + fsr2wdoffset  + 0x100; // this is tricky
				//if (retV >= 0x200)
				//{
				//	fprintf(stderr, "Error, around assembly line %s:%d, need access FSR2-%d,\n which means local stack is too large in function %s\n, module %s, please fix it.\n",
				//		wdp->m->srcName, wdp->lineno, retV-0x100, sp->fpHasthis->name, wdp->m->name);
				//	exit(-__LINE__);
				//}
				return retV+0x100;
			}
			else
			{
				return 0x0d; // PRINC2!!
			}
		}
		else
		{
			if (sp->ap && sp->ap->region == regionCODE && wdp)
				wdp->hasRomLabel = 1;
			if (div2RomSym && sp->ap && sp->ap->region == regionCODE)
				return sp->labelPos->areaOffset >> 1; // compatibe mode job is hard?
			if (needshow)
				printf("%X\n", sp->labelPos->areaOffset);
			return sp->labelPos->areaOffset;
		}
	}
	if (sp->symIsExt)
	{
		
		if (!sp->definedPos)
		{
			fprintf(stderr,"linker internal error; %s, %s:%d\n", wdp?wdp->locTree:"xx", __FUNCTION__, __LINE__);
			exit(-__LINE__);
		}
		int val=getSymValue(sp->definedPos, sp->definedPos->ldm, lev, romByteOffset, div2RomSym, fpreq, fsr2wdoffset,wdp);
		if (needshow)
			printf("%X\n", val);
		return val;
	}
	if (sp->symIsEQU)
		return getExpValue(sp->exp, sp->ldm, lev,romByteOffset,div2RomSym,fsr2wdoffset,fpreq,NULL); // lev no inc1, assume no offset here
	fprintf(stderr, "Linker Error: symbol %s cannot get value.\n", sp->name);
	exit(-80);

}
// only in linker we get the final expression value
// it is recursive
// means module local should be searched first
// prevent infinite loop, we need an array to look back

static expression *exptree[64];
char bufferexp[1024];
int getExpValue(expression *exp, moduleLD *m, int lev , int romByteOffset, int div2RomSym, int fsr2offset, funcRecord *fpreq, wData *wdp)
{
	
	symbol *sp;
	int i;
	int left;
	int right;
	char buffer[10240];
	char *locTree = (wdp ? wdp->locTree : " ");
	if (lev >= 64)
	{
		fprintf(stderr, "Linker error: Expression too deep!! %s %s\n", exprDump(exp, bufferexp, 1024),locTree+1);
		exit(-60);
	}
	for (i = 0; i < lev; i++)
	{
		if (exptree[i] == exp)
		{
			fprintf(stderr, "Expression loop found %s %s\n", exprDump(exp, bufferexp, 1024), locTree+1);
			exit(-70);
		}
	}
	exptree[lev] = exp;

	switch (exp->type)
	{
	case ExpIsNumber:
		return exp->exp.number;
	case ExpIsSymbol: // this is complex!! local/exported/referenced
		sp = findSymInModuleLD(exp->exp.symbol, m, 1,0);
		if (sp == NULL)
		{
			fprintf(stderr, "Linker Internal err!! symbol %s not found in module %s %s.\n",
				exp->exp.symbol, m->name, locTree);
			exit(-40);
		}
//#ifdef _DEBUG
//		if (strstr(sp->name, "__addrtmp__"))
//			fprintf(stderr, "%s eval\n", sp->name);
//#endif
		return getSymValue(sp, sp->ldm, lev,romByteOffset,div2RomSym,fpreq,fsr2offset,wdp);
	case ExpIsTree:
		left = getExpValue(exp->exp.tree.left, m, lev + 1, romByteOffset,div2RomSym,fsr2offset,fpreq,wdp);
		right = getExpValue(exp->exp.tree.right, m, lev + 1, romByteOffset, div2RomSym, fsr2offset,fpreq,wdp);
		switch (exp->exp.tree.op)
		{
			// special case, if p41re-entry, the + offset should be change to - 
			case '+':
				if (fpreq != NULL && (fpreq->p41ReEntry || fpreq->h08dReEntry) && exp->exp.tree.right->type == ExpIsNumber &&
					exp->exp.tree.left->type == ExpIsSymbol &&
					exp->exp.tree.left->exp.symbol
					)
				{
					symbol * sp = findSymInModuleLD(exp->exp.tree.left->exp.symbol,fpreq->m, 1,1);
					if (sp && sp->symIsByFSR2)
						return left - right;
					else
						return left + right;
				}
				else
					return left + right;
			case '-': return left - right;
			case '|': return left | right;
			case '&': 
				// this is special
				/*if (exp->exp.tree.left->type == ExpIsTree && exp->exp.tree.left->exp.tree.left->type == ExpIsSymbol)
				{
					fprintf(stderr, "%s\n", exp->exp.tree.left->exp.tree.left->exp.symbol);
					if (strstr(exp->exp.tree.left->exp.tree.left->exp.symbol, "STK07"))
						fprintf(stderr, "STK07\n");
				}*/
				if (exp->exp.tree.OverflowChk) // overflow checks related
				{
					if (exp->exp.tree.OverflowChkUnsign)
					{
						if (left & (right ^ 0xffffff))
						{
							//fprintf(stderr, "Warning, module %s  %s is out of range, assembly %s.asm line %d!!\n",
								//m->name, exprDump(exp, bufferexp, 1024), m->srcName, exp->lineno);
							// we have special case of H08D and bit var ..
							if (right != 1 && ((HY08A_getPART()->isEnhancedCore != 4)))
							{

								snprintf(buffer, sizeof(buffer)-1, "Warning, module %s  %s is out of range, around assembly %s!!\n",
									m->name, exprDump(exp, bufferexp, 1024), wdp->locTree + 1);
								if (verbose)
								{
									fprintf(stderr, "%s", buffer);
								}
								appendErrMsg(buffer);
								if(foundOutOfRange<1024)
								 OutOfRangeWdp[foundOutOfRange ++]=wdp;
							}
							else
								return 1;
						}
					}
					else
					{
						// for example, if right is 7, it means -4~+3
						int max = right >> 1;
						int min = (-max) - 1;
						if (left > max || left < min)
						{
							//fprintf(stderr, "Warning, module %s  %s is out of range!!, assembly %s.asm line %d\n",
							//	m->name, exprDump(exp, bufferexp, 1024), m->srcName, exp->lineno);
							snprintf(buffer,sizeof(buffer)-1, "Warning, module %s  %s is out of range!!, around assembly %s\n",
								m->name, exprDump(exp, bufferexp, 1024),  wdp->locTree+1);
						   if(verbose)
								fprintf(stderr,"%s",buffer);
							appendErrMsg(buffer);
							if (foundOutOfRange < 1024)
								OutOfRangeWdp[foundOutOfRange++] = wdp;

						}
					}

				}
				return left & right;
			case 'A': return left >> right;
			case 'B': return left << right;
			case '>': return (left > right);
			case '<': return (left < right);
			case '=': return (left == right);
			case 'L': return (left <= right);
			case 'G': return (left >= right);
			case 'N': return (left != right);
			case 'C':
				if ( (left == 0 && right >= 0x100))
				{
					snprintf(buffer, sizeof(buffer)-1, "Warning, module %s A-BIT mismatch at %s.\n",m->name,
						 wdp->locTree + 1);
					if (verbose)
						fprintf(stderr, "%s", buffer);
					appendErrMsg(buffer);
					//fprintf(stderr, "Warning, A-BIT range mismatch at %s\n", wdp->locTree+1
					 // warning only
				}
				return left; // return left ony 
			default :
			{
				char buf[1024];
				if(m->srcName!=NULL)
					fprintf(stderr, "Linker Error !! cannot resolve %s expression %s at %s\n",m->srcName, exprDump(exp, buf, 1023), locTree+1);
				else
					fprintf(stderr, "Linker Error !! cannot resolve %s expression %s at %s\n", m->name, exprDump(exp, buf, 1023), locTree+1);
				exit(-1009);
			}
			
		}
	case ExpIsPCNow:
		return (romByteOffset + 2)&0xfffffffe; // +2 is next no change address
	case ExpIsStackOffset:
		//if(sw_tgt_id==15041 || sw_tgt_id==15141 )
		if(HY08A_getPART()->isEnhancedCore==3 )
			return 0x100+(fsr2offset + exp->exp.number);
		return ((exp->ExpSymImmed)?0:0x80)+((fsr2offset + exp->exp.number)&0x7f); // 08d d bit is 0 for stack

	}
	fprintf(stderr, "Linker Error!!cannot solve expression %s, we giveup\n", exprDump(exp, bufferexp, 1024));
	exit(-100);

}

// assign data for ROM only, 2018 nov change to label-word-aligh
int assignAreaListWDATA(area *aList, unsigned char *data2write, unsigned char *assignedMap)
{
	area *ap;
	wData *wdp;
	int value;
	int i;
	char buffer[MAXSTRSIZE0+1024];
	for (ap = aList; ap; ap = ap->next)
	{
		
		for (wdp = ap->wDatas; wdp; wdp = wdp->next)
		{
			if (wdp->wdIsBlank || wdp->wdIsLabel || wdp->wdIsOrg || wdp->wdIsMacroMark)
				continue;

//#ifdef _DEBUG
//			if (wdp->lineno == 271)
//			{
//				exprDump(wdp->exp, buffer, 4095);
//			}
//#endif
			
			value = getExpValue(wdp->exp, wdp->m,0,wdp->areaOffset,wdp->expCodeLabelDiv2,wdp->fsr2_stack_offset,wdp->fp,wdp); // this is the div2
			//if(wdp->lineno==77 )
			//printf("in area %s get exp %s value %x, at offset %x\n",
				//ap->name, exprDump(wdp->exp, buffer, 4095), value, wdp->areaOffset);
			wdp->wValue = value;
			for (i = 0; i < wdp->sizeOrORG; i++)
			{
				if (assignedMap[wdp->areaOffset + i] == 0)
				{
					data2write[wdp->areaOffset + i] = value & 0xff;
					assignedMap[wdp->areaOffset + i] = 1;
				}
				else
				{
					snprintf(buffer,sizeof(buffer)-1, "area %s address overlapped at memory offset 0x%X\n", ap->name, wdp->areaOffset);
					appendErrMsg(buffer);
				}
				value >>= 8;
			}
		}
	}
	return 0;
}





int assignMemData(void) // we solve all the equations here .. only ROM
{
	return assignAreaListWDATA(topMap.romArea, rom_data, rom_assigned);
		/*|
		assignAreaListWDATA(topMap.dataArea, ram_data, ram_assigned) |
		assignAreaListWDATA(topMap.xdataArea, ram_data, ram_assigned);*/
}

static const char linenoFMT[][10] = { "%02d ", "%03d ", "%04d ", "%05d ", "%d " };
static int selectLinenoFMT(int tot)
{
	if (tot < 100) return 0;
	if (tot < 1000) return 1;
	if (tot < 10000) return 2;
	if (tot < 100000) return 3;
	return 4;
}

int doUpdateList(void)
{
	//	char srcName[PATH_MAX];
	char rstName[PATH_MAX];
	int i;
	int result = 0;
	for (i = 0; i < objFileCount; i++)
	{
		//strncpy(srcName, objBasename[i], PATH_MAX);
		strncpy(rstName, objBasename[i], PATH_MAX);
		// we must change to the real source which is different from the basename of obj!!

		//ChangeFileExt(srcName, ".asm");
		ChangeFileExt(rstName, ".rst");
		result |= genRstFileO(objSrcName[i], rstName, 1);
	}
	return result;
}

// generate relocated file output
// as its name
#define FIELDWIDTH 20
int genRstFileO(char* srcFileName, char *listFName, int do_out) // src in that module
{
	int finalLineNo = 0;
	srcLine *h, *t;
	srcLine *entry = NULL;
	int i,j;
	// now dump the file
	char *format1 = (finalLineNo > 10000) ? "%07d " : (finalLineNo > 1000) ? "%05d " : (finalLineNo > 100) ? "%04d " : "%03d ";
	moduleLD *m;
	area *areaArray[1024];
	area *ap;
	wData *wdp;
	int areaNum = 0;
	uint64 macroHash = 0;
	uint64 fNameHash;
	char fullpath[PATH_MAX + 1];
	srcLine *mh = NULL;
	srcLine *mt = NULL;
	int totalLineNo;
	srcLine **entryMap;

#ifdef _WIN32
	_fullpath(fullpath, srcFileName, PATH_MAX);
#else
	dummy=realpath(srcFileName, fullpath);
	//fprintf(stderr,"Warning, path error %s\n", fname);
#endif
	fNameHash = hashi(fullpath);

	for (m = topMap.linkedModules; m; m = m->next)
	{
#ifdef _WIN32
		if (!strcasecmp(m->srcName, srcFileName))
#else
		if (!strcmp(m->srcName, srcFileName))
#endif
			break;
	}
	if (m == NULL)
	{
		fprintf(stderr, "fail to find rst file module of %s\n", srcFileName);
		return (-__LINE__);
	}
	totalLineNo = 0;
	readFile2SrcLine2(srcFileName, &h, &t, NULL, NULL, PATH_MAX, asmIncHead, &totalLineNo,"");


	if (totalLineNo == 0) // still zero
		return -1;
	entryMap = calloc(totalLineNo, sizeof(srcLine *));
	for (entry = h; entry; entry = entry->next)
		if (entry->fnameHash == fNameHash)
			entryMap[entry->lineno - 1] = entry;

	// insert macros
	for (i = 0; i < macInsNum; i++)
	{
		int found = 0;
		for (j = 0; j < defined_macro_num; j++)
		{
			if (macros_mod[j] == m && !strcmp(defined_macros[j]->macroName, macInsArray[i]->macroName))
			{
				found = 1;
				break;
			}
		}
		if (j >= defined_macro_num) // other module, not count
			continue;
		entry = findSrcLineLocTree(h, macInsArray[i]->locTree);
		if (entry == NULL)
			continue; // it is possible not found
		insertMacro2SrcLines(entry, defined_macros[j]);
		// insert macro
	}



	for (ap = topMap.dataArea; ap; ap = ap->next)
		areaArray[areaNum++] = ap;
	for (ap = topMap.xdataArea; ap; ap = ap->next)
		areaArray[areaNum++] = ap;
	for (ap = topMap.romArea; ap; ap = ap->next)
		areaArray[areaNum++] = ap;
	for (ap = topMap.stkArea; ap; ap = ap->next)
		areaArray[areaNum++] = ap;


	for (i = 0; i < areaNum; i++)
	{
		ap = areaArray[i];
		for (wdp = ap->wDatas; wdp; wdp = wdp->next)
		{
			// additional is macrosrc
			if (wdp->m != m)
				continue;
			if (!wdp->wdIsMacroMarkEnd && (wdp->sizeOrORG == 0)) // macroend force write
				continue;
			/*if (wdp->macroSrc != NULL && wdp->macro_id != -2)
			{
				char hashchk[PATH_MAX + 1024];

				strncpy(hashchk, wdp->macroSrc, PATH_MAX + 1023);
				strncat(hashchk, wdp->locTree, PATH_MAX + 1023);
				uint64 newHash = hash(hashchk);
				if (newHash != macroHash)
				{
					if (mh == NULL)
					{
						entry = newSrcLine("---MACRO START---", wdp->lineno);
						appendSrcLine2(&mh, &mt, entry);
					}
					entry = newSrcLine(wdp->macroSrc, wdp->lineno);
					appendSrcLine2(&mh, &mt, entry);
					macroHash = newHash;
				}
				if (entry->wDataStart == NULL)
					entry->wDataStart = entry->wDataEnd = wdp;
				else
					entry->wDataEnd = wdp;
				if (wdp->ap->region == regionCODE)
					entry->onROM = 1;
			}
			else*/
			{
				if (mh != NULL)
				{
					srcLine *bk;
					// rewrite lineno
					char buf[PATH_MAX];
					int i;
					entry = newSrcLine("---MACRO END---", wdp->lineno);
					appendSrcLine2(&mh, &mt, entry);
					strcpy(buf, mh->next->wDataStart->locTree);
					for (i = strlen(buf) - 1; i >= 0; i--)
						if (buf[i] == ':')
							break;
					sprintf(buf + i + 1, "%d", mh->next->wDataStart->lineno);

					entry = findSrcLineLocTree(h, buf);
					if (!entry)
						continue;
					bk = entry->next;
					entry->next = mh;
					mt->next = bk;
					mh = NULL;
					mt = NULL;
					macroHash = 0ll;
				}
				if (wdp->locTree == NULL)
				{
					//fprintf(stderr, "strange wdp\n");
				}
				else
				{
					//if (wdp->locTree && strstr(wdp->locTree, ",ADC.asm:254"))
						//fprintf(stderr, "255");
					//if (wdp->macro_id != -2 && !strchr(wdp->locTree + 1, ',')) // in include
					//	entry = entryMap[wdp->lineno - 1];
					//else
					entry = findSrcLineLocTree(h, wdp->locTree);
					if (entry)
					{
						if (entry->wDataStart == NULL)
							entry->wDataStart = entry->wDataEnd = wdp;
						else
							entry->wDataEnd = wdp;
						if (wdp->ap->region == regionCODE)
							entry->onROM = 1;
					}
				}
			}
		}
	}
	if (h == NULL)
	{
		fprintf(stderr, "gen RST fail to read file %s\n", srcFileName);
		return (-__LINE__);
	}
	if (do_out)
	{
		FILE *fo = fopen(listFName, "w");
		if (fo == NULL)
		{
			fprintf(stderr, "file %s cannot open for write, process failed.\n", listFName);
			return 7;
		}
		for (entry = h; entry; entry = entry->next)
		{
			// first column is lineno
			fprintf(fo, format1, entry->lineno);
			// second column is areaOffset
			if (entry->wDataStart) // 
			{
				int offsetnow = 0; // there is FIELDWIDTH ch spaces for content
				int writeCount = 0;
				wData *wdp;
				if (entry->onROM)
				{

#ifdef _DEBUG
					fprintf(fo, "%08X%08X ", entry->wDataStart->inCond, entry->wDataStart->outCond);
#endif
					fprintf(fo, "%04X ", entry->wDataStart->areaOffset/2);
					/*if (entry->wDataStart->areaOffset / 2 == 0x0082)
						fprintf(stderr, "0x0082\n");*/
					offsetnow = 5;
					
					if (entry->wDataStart && entry->wDataStart->fp && (entry->wDataStart->fp->p41ReEntry ||
						entry->wDataStart->fp->paraOnStack || entry->wDataStart->fp->h08dReEntry)
						)
					{
						fprintf(fo, "[%02d] ", entry->wDataStart->fsr2_stack_offset);
						offsetnow += 5;
						if (entry->wDataStart->wdIsInstruction && entry->wDataStart->instHighByte)
						{
							if (entry->wDataStart->bankFlow < 100)
							{
								switch (entry->wDataStart->bankFlow)
								{
								case 99:
									fprintf(fo, "[X] "); break; // comflict
								case 98:
									fprintf(fo, "[Y] "); break; // searched, no found
								default:
									fprintf(fo, "[Z] "); break; // not checked
								}
									

								//offsetnow += 4;
							}
							else
							{
								fprintf(fo, "[%d] ",entry->wDataStart->bankFlow-100 );
								//offsetnow += 4;
							}

						}
					}
					if (entry->wDataEnd->areaOffset - entry->wDataStart->areaOffset > 30) // max is 10
					{
						entry->wDataEnd = entry->wDataStart->next;
					}
					for (wdp = entry->wDataStart; wdp&& wdp != entry->wDataEnd
							&& (entry->wDataEnd->areaOffset>= wdp->areaOffset); wdp = wdp->next) // need additional loop
					{
						if (wdp->wdIsLabel)
							continue;
						if (wdp->exp == NULL)
						{
							fprintf(fo, (writeCount & 1) ? "   " : "  ");
							offsetnow += ((writeCount & 1) ? 3 : 2);
						}
						else if (wdp->sizeOrORG != 0)
						{
							fprintf(fo, (writeCount & 1) ? "%02X " : "%02X", wdp->wValue);
							offsetnow += ((writeCount & 1) ? 3 : 2);
							writeCount++;
						}

						if (offsetnow >= FIELDWIDTH && wdp->next != NULL) // get a new line
						{
							fprintf(fo, "\n");
							fprintf(fo, format1, entry->lineno);
							fprintf(fo, "%04X ", wdp->next->areaOffset >> 1);
							offsetnow = 5;
						}
					}
					if (wdp && !wdp->wdIsLabel && wdp->sizeOrORG != 0)
					{
						if (wdp->exp != NULL)
							fprintf(fo, (writeCount & 1) ? "%02X " : "%02X", wdp->wValue);
						else
							fprintf(fo, (writeCount & 1) ? "-- " : "--");

						offsetnow += ((writeCount & 1) ? 3 : 2);
						writeCount++;
					}
				}
				else
				{
					if (entry->wDataStart->wdBlankIsByFSR2)
					{
						if (entry->wDataStart->fsr2_stack_offset<0)
							fprintf(fo, "@FSR2-CurrentOffset+%03d ", -(entry->wDataStart->fsr2_stack_offset));
						else if (entry->wDataStart->fsr2_stack_offset == 0)
							fprintf(fo, "@FSR2-%02d ", entry->wDataStart->fsr2_stack_offset);
						else
							fprintf(fo, "@FSR2+%02d ", entry->wDataStart->fsr2_stack_offset);
						offsetnow = 9;
					}
					else
					{
						fprintf(fo, "%04X ", entry->wDataStart->areaOffset);
						offsetnow = 5;
					}
					for (wdp = entry->wDataStart; wdp&&wdp != entry->wDataEnd; wdp = wdp->next) // need additional loop
					{
						if (wdp->wdIsLabel)
							continue;
						if (wdp->exp != NULL && wdp->sizeOrORG != 0)
							fprintf(fo, "%02X ", wdp->wValue);
						else

							fprintf(fo, "-- ");
						writeCount++;
						offsetnow += 3;
						if (offsetnow >= FIELDWIDTH && wdp->next != NULL) // get a new line
						{
							fprintf(fo, "\n");
							fprintf(fo, format1, entry->lineno);
							fprintf(fo, "%04X ", wdp->next->areaOffset);
							offsetnow = 5;
						}
					}
					if (wdp && !wdp->wdIsLabel && wdp->sizeOrORG != 0)
					{
						int j, k;
						if (!wdp->wdIsOrg)
							k = wdp->sizeOrORG;
						else
							k = 1;
						for (j = 0; j < k; j++)
						{
							if (wdp->exp != NULL)
								fprintf(fo, "%02X ", wdp->wValue);
							else
								fprintf(fo, "-- ");
							writeCount++;
							offsetnow += 3;
						}
					}
				}
				while (offsetnow < FIELDWIDTH) {
					fputc(' ', fo); offsetnow++;
				} // aligned

			}
			else // leave the space, 22 
			{
				int offsetnow1 = 0;
				while (offsetnow1 < FIELDWIDTH) {
					fputc(' ', fo); offsetnow1++;
				} // aligned
			}
			// now print the source
			fprintf(fo, "%s\n", entry->line);

		}
		fclose(fo);
	}
	free(entryMap);
	return 0;
}



static void printtab(FILE *fo, int n)
{
	int i;
	for (i = 0; i < n; i++)
		fputc('\t', fo);
}
static int dumpCallTree(funcRecord *fp, int level, FILE *fo, int accumulatedSize)
{
	int i;
	// print-myself
	

	// skip some info .. like common code reuse
	if (!fp->paraOnStack && !fp->p41ReEntry && !fp->h08dReEntry && !fp->localSymSize && !fp->localXSymSize 
		&& !fp->calledFuncNum)
		return level;
	printtab(fo, level);
	fprintf(fo, "--> %s ,", fp->name);
	if (fp->paraOnStack)
		fprintf(fo, "Variable Length Parameter, ");
	if (fp->p41ReEntry || fp->h08dReEntry)
		fprintf(fo, "08C/08D-Re-Entry, local var size 0x%X, accumulated 0x%X.",fp->localSymSize , accumulatedSize+fp->localSymSize);
	else
		fprintf(fo, "local var base at 0x%X, size 0x%X. ", fp->baseLocalAddr, fp->localSymSize);
	if (fp->localXSymSize > 0)
		fprintf(fo, "local bar base-X at 0x%X, size 0x%X. ", fp->baseLocalXAddr, fp->localXSymSize);
	for (i = 0; i < level; i++) // b->a->b->a
	{
		if (callDepthTable[i] == fp || callDepthTable[i]==NULL)
		{
			fprintf(fo, "recursive\n");
			return level;
		}
	}
	fprintf(fo, "\n");
	callDepthTable[i] = fp; // change of this level
	for (i = 0; i < fp->calledFuncNum; i++)
		dumpCallTree(fp->calledFunc[i], level + 1, fo, accumulatedSize+fp->localSymSize);
	return level;
}
// generate the map file
int doGenMapFile(void)
{
//	int i;
	area *ap;
	symbol *sp;
	FILE *fp;
	moduleLD *mdp;
	char mapFileName[PATH_MAX];
	time_t now;;
	char buffer[1024];
	char imgFileName[PATH_MAX];
	funcRecord *funcp;

	int ramOutOfRange = 0;
	int body_ramsize=HY08A_getPART()->dataMemSize;
	int body_romsize=HY08A_getPART()->programMemSize*2;
	strncpy(mapFileName, outputBasename, PATH_MAX);
	strncpy(imgFileName, outputBasename, PATH_MAX);
	ChangeFileExt(mapFileName, ".map");
	ChangeFileExt(imgFileName, ".ihx");
	fp = fopen(mapFileName, "w");
	if (fp == NULL)
	{
		fprintf(stderr, "FILE %s open error.\n", mapFileName);
		return -1;
	}
	if (strlen(HY08A_getPART()->name) > 0)
	{
		fprintf(fp, 
"------------------------------------------------------------------------\n\
 Map File for linked output \"%s\", using target %s.\n\
------------------------------------------------------------------------\n", imgFileName,
			HY08A_getPART()->name);
	}
	else
	{
		fprintf(fp, "--------\nMap File for linked output \"%s\", using target UNKNOWN.\n--------\n", outputBasename);
	}




	fprintf(fp, "\n---Resource Usage Summaries---\n");

	if (HY08A_getPART()->isEnhancedCore > 3 && !f08a)
	{
		fprintf(fp, "Heap RAM: 0x%X~0x%X, size %d bytes.\n", topMap.heapInfo.startA, topMap.heapInfo.endA, topMap.heapInfo.size);
		fprintf(fp, "Estimate C-STACK RAM: 0x80~0x%X, size %d bytes\n", topMap.cstackInfo.size+0x80, topMap.cstackInfo.size);
		fprintf(fp, "RAM usage is around %d%%\n", (topMap.heapInfo.size + topMap.cstackInfo.size) * 100 / body_ramsize);
		if (topMap.heapInfo.size + topMap.cstackInfo.size > body_ramsize)
			ramOutOfRange = 1;

	}
	else if(f08a || ramModel == RAM_MODEL_FLATA)
	{
		fprintf(fp, "Heap&STACK(Zero-Page) RAM: 0x%X~0x%X, size %d bytes.\n", topMap.heapInfo.startA, topMap.heapInfo.endA, topMap.heapInfo.size);
		if (topMap.xdataInfo.size > 0)
			fprintf(fp, "XDATA RAM: 0x%X~0x%X size %d bytes\n", topMap.xdataInfo.startA, topMap.xdataInfo.endA, topMap.xdataInfo.size);
		else
			topMap.xdataInfo.size = 0;
		fprintf(fp, "RAM usage is around %d%%\n", (topMap.heapInfo.size + topMap.cstackInfo.size+topMap.xdataInfo.size) * 100 / body_ramsize);
		if (topMap.heapInfo.size + topMap.cstackInfo.size + topMap.xdataInfo.size > body_ramsize)
			ramOutOfRange = 1;
	}
	else
	{
		int heapSize = 0;
		int usedRAM = dataMaxAddr - 0x80;
		if (localMaxAddr > dataMaxAddr)
			usedRAM = localMaxAddr - 0x80;
		if (body_ramsize > 0x100)
		{
			if ((xdataMaxAddr - xStartAddr) > 0)
			{
				if (xdataMaxAddr >= 0x200 && xStartAddr < 0x180)
				{
					fprintf(fp,"HEAP and STACK shared RAM address from 0x100~0x180,\nTotal RAM usage is around %d%%.\n",
						(usedRAM + xdataMaxAddr - xStartAddr - 0x80) * 100 / body_ramsize);
					heapSize = xdataMaxAddr - xStartAddr - 0x80;
					
				}
				else
				{
					fprintf(fp,"Zero Page RAM usage is around %d%%,\nXDATA RAM usage is %d%%.\nTotal RAM usage is around %d%%.\n",
						usedRAM * 100 / 256, (xdataMaxAddr - xStartAddr) * 100 / (body_ramsize - 256), (usedRAM + xdataMaxAddr - xStartAddr) * 100 / body_ramsize);
					heapSize = xdataMaxAddr - xStartAddr;
					
				}

			}
			else
			{
				fprintf(fp, "Zero Page RAM usage is around %d%%.\nXDATA RAM not used. Total RAM usage is around %d%%.\n",
					usedRAM * 100 / 256, (usedRAM + xdataMaxAddr - xStartAddr) * 100 / body_ramsize);
				
			}
			topMap.heapInfo.startA = xStartAddr;
			topMap.heapInfo.endA = xdataMaxAddr;
			topMap.heapInfo.size = heapSize;
		}
		else
		{
			fprintf(fp,"RAM usage is %d%%.\n", usedRAM * 100 / body_ramsize);
			topMap.heapInfo.startA = 0x80;
			topMap.heapInfo.endA = usedRAM + 0x80;
			topMap.heapInfo.size = usedRAM;
			
		}
	}
	fprintf(fp, "ROM used 0 ~ 0x%X(byte)\nROM usage is around %d%%\n", topMap.romInfo.endA, topMap.romInfo.endA*100/body_romsize);
	fprintf(fp, "---Resource Usage listed above ---\n\n");

	if (topMap.heapInfo.size > 0)
	{
		fprintf(fp, "RAM AREA Summary:\n");
		for (ap = topMap.dataArea; ap; ap = ap->next)
		{
			if (ap->occuLength == 0)
				continue;
			fprintf(fp, "Area %s\t0x%04X~0x%04X, size \t%d\t bytes.\n", ap->name, ap->startAddr, ap->endAddr, ap->occuLength);
		}
		for (ap = topMap.stkArea; ap; ap = ap->next)
		{
			if (ap->occuLength == 0)
				continue;
			fprintf(fp, "Area %s\t0x%04X~0x%04X, size \t%d\t bytes.\n", ap->name, ap->startAddr, ap->endAddr, ap->occuLength);
		}
		for (ap = topMap.xdataArea; ap; ap = ap->next)
		{
			if (ap->occuLength == 0)
				continue;
			fprintf(fp, "Area %s\t0x%04X~0x%04X, size \t%d\t bytes.\n", ap->name, ap->startAddr, ap->endAddr, ap->occuLength);
		}
	}
	if (mainf)
	{
		int isRecursive = 0;
		//		int maxStack;
		fprintf(fp, "\nmain thread max call level is %d\n", mainThreadCallLevel);
		fprintf(fp, "interupt thread max call level is %d\n\nCall-Tree Information:\n", interruptThreadCallLevel);

		dumpCallTree(mainf, 0, fp, 0);
		fprintf(fp, "estimated minimum RAM of call tree [main]: %d (0x%X) bytes.\n", mainLocalStackSize, mainLocalStackSize);


		if (interruptf)
		{
			dumpCallTree(interruptf, 0, fp, mainLocalStackSize);
			fprintf(fp, "estimated minimum RAM of call tree [interrupt]: %d (0x%X) bytes.\n", interruptLocalStackSize, interruptLocalStackSize);
		}
	}
	if (body_has_sfr2_stack())
	{
		if (stktop_sym != NULL)
			fprintf(fp, "\nStack Pointer FSR2 initialized at 0x%X\n\n", getExpValue(stktop_sym->exp, NULL, 0, 0, 0, 0, NULL, NULL));
	}

	fprintf(fp, "\n********************\nLocal stack information:\n********************\n");
	for (mdp = topMap.linkedModules; mdp; mdp = mdp->next)
	{
		int i;

		for (i = 0; i < mdp->funcNum; i++)
		{
			funcp = mdp->funcs[i];
			if (funcp->p41ReEntry || funcp->h08dReEntry)
			{
				if (funcp->localXSymSize)
				{
					fprintf(fp, "Function %-16s local is on FSR2, local size %03X, Xbase address %03X, size %03X.\n",
						funcp->name, funcp->localSymSize,
						funcp->baseLocalXAddr, funcp->localXSymSize);
				}
				else
				{
					fprintf(fp, "Function %-16s local is on FSR2, local size %02X.\n",
						funcp->name, funcp->localSymSize);
				}
			}
			else

			{
				if (funcp->localXSymSize)
				{
					fprintf(fp, "Function %-16s has base address %03X, local size %03X, Xbase address %03X, size %03X.\n",
						funcp->name, funcp->baseLocalAddr, funcp->localSymSize,
						funcp->baseLocalXAddr, funcp->localXSymSize);
				}
				else
				{
					fprintf(fp, "Function %-16s has base address %03X, local size %03X.\n",
						funcp->name, funcp->baseLocalAddr, funcp->localSymSize);
				}
			}
		}
	}





	// for 3 regions, we show each area, corresonding to different module
	// show RAM first
	
	for (ap = topMap.dataArea; ap; ap = ap->next)
	{
		moduleLD *mdp = NULL;
		wData *wdp;
		//fprintf(fp, "\nDATA Area %s, start %04X, end %04X, size %04X.\n", ap->name, ap->startAddr, ap->endAddr, ap->occuLength);
		for (wdp = ap->wDatas; wdp; wdp = wdp->next)
		{
			if (wdp->m != mdp)
			{
				wData *wdp2;
				int mina = 10240;
				int maxa = 0;
				/*fprintf(fp, "in area %s module %s start at 0x%04X (byte#)",
					ap->name, wdp->m->name, wdp->areaOffset );*/
				mdp = wdp->m;
				for (wdp2 = wdp; wdp2 && wdp2->m == mdp; wdp2 = wdp2->next)
				{
					int nowa;
					int nowae;
					if (!wdp2->wdIsLabel || !wdp2->next || !wdp2->next->wdIsBlank)
						continue;
					nowa = wdp2->next->areaOffset;
					nowae = nowa +wdp2->next->sizeOrORG - 1;

					if (nowae > maxa) maxa = nowae;
					if (nowa < mina) mina = nowa;
				}
				fprintf(fp, "in area %s module %s start at 0x%03X end at 0x%03X\n",
					ap->name, mdp->name, mina, maxa);
					
			}
			if (wdp->wdIsLabel)
			{
				if (wdp->sym->symIsGlobl)
					fprintf(fp, "Global ");
				else
					fprintf(fp, "Local  ");
				if(wdp->sym->symIsByFSR2)
					fprintf(fp, "%-16s=FSR2-0x%03X\n", wdp->sym->name, wdp->areaOffset);
				else
				fprintf(fp, "%-16s=0x%03X\n", wdp->sym->name, wdp->areaOffset);
			}
		}
	}
	fprintf(fp, "\n********************\nXDATA Area:\n********************\n");
	for (ap = topMap.xdataArea; ap; ap = ap->next)
	{
		moduleLD *mdp = NULL;
		wData *wdp;
		//fprintf(fp, "\nDATA Area %s, start %04X, end %04X, size %04X.\n", ap->name, ap->startAddr, ap->endAddr, ap->occuLength);
		for (wdp = ap->wDatas; wdp; wdp = wdp->next)
		{
			if (wdp->m != mdp)
			{
				wData *wdp2;
				int mina = 10240;
				int maxa = 0;
				/*fprintf(fp, "in area %s module %s start at 0x%04X (byte#)",
				ap->name, wdp->m->name, wdp->areaOffset );*/
				mdp = wdp->m;
				for (wdp2 = wdp; wdp2 && wdp2->m == mdp; wdp2 = wdp2->next)
				{
					int nowa;
					int nowae;
					if (!wdp2->wdIsLabel || !wdp2->next || !wdp2->next->wdIsBlank)
						continue;
					nowa = wdp2->next->areaOffset;
					nowae = nowa + wdp2->next->sizeOrORG - 1;

					if (nowae > maxa) maxa = nowae;
					if (nowa < mina) mina = nowa;
				}
				fprintf(fp, "\n********************\nin area %s module %s start at 0x%03X end at 0x%03X\n********************\n",
					ap->name, mdp->name, mina, maxa);

			}
			if (wdp->wdIsLabel)
			{
				if (wdp->sym->symIsGlobl)
					fprintf(fp, "Global ");
				else
					fprintf(fp, "Local  ");
				fprintf(fp, "%-16s=0x%03X\n", wdp->sym->name, wdp->areaOffset);
			}
		}
	}
	fprintf(fp, "\n********************\nSTK Area:\n********************\n" );
	for (ap = topMap.stkArea; ap; ap = ap->next)
	{
		moduleLD *mdp = NULL;
		wData *wdp;
		
		for (wdp = ap->wDatas; wdp; wdp = wdp->next)
		{
			if (wdp->m != mdp)
			{
				//wData *wdp2;
				mdp = wdp->m;
				fprintf(fp, "\n********************\nIn area %s module %s:\n*******************\n", ap->name, wdp->m->name);

				//fprintf(fp, "in area %s module %s start at 0x%04X (byte#) ",
				//	ap->name, wdp->m->name, wdp->areaOffset);
				//mdp = wdp->m;
				//for (wdp2 = wdp; wdp2 && wdp2->m == mdp; wdp2 = wdp2->next)
				//	;
				//if (wdp2)

				//	fprintf(fp, "endaddress is 0x%04X(byte#) , size %d bytes\n", wdp2->areaOffset - 1,
				//		wdp2->areaOffset - wdp->areaOffset);

				//else
				//	fprintf(fp, "endaddress is 0x%04X(byte#) , size %d bytes\n", ap->endAddr - 1,
				//		ap->endAddr - wdp->areaOffset);
			}

			if (wdp->wdIsLabel)
			{
				int endAddr=0;
				if (wdp->sym->symIsGlobl)
					fprintf(fp, "Global ");
				else
					fprintf(fp, "Local  ");
				fprintf(fp, "%-16s=0x%03X\n", wdp->sym->name, wdp->areaOffset);
				endAddr = wdp->sizeOrORG + wdp->areaOffset;
				if (endAddr > ap->endAddr)
					ap->endAddr = endAddr;
			}
		}
	}
	for (ap = topMap.romArea; ap; ap = ap->next)
	{
		moduleLD *mdp = NULL;
		wData *wdp;
		if (ap->occuLength == 0)
			continue;
		fprintf(fp, "\n********************\nROM Area %s, start 0x%04X (BYTE), end 0x%04X (BYTE), size 0x%04X (BYTE), or 0x%04X (WORD). \n********************\n", ap->name, ap->startAddr, ap->endAddr, ap->occuLength,
			(ap->occuLength + 1) / 2);
		for (wdp = ap->wDatas; wdp; wdp = wdp->next)
		{
			if (wdp->m != mdp)
			{
				wData *wdp2;
				fprintf(fp, "\n********************\nIn area %s module %s start at 0x%04X (byte#) or 0x%04X (word#),",
					ap->name, wdp->m->name, wdp->areaOffset, wdp->areaOffset / 2);
				mdp = wdp->m;
				for (wdp2 = wdp; wdp2 && wdp2->m == mdp; wdp2 = wdp2->next)
					;
				if (wdp2)

					fprintf(fp, "endaddress is 0x%04X(byte#) or 0x%04X(word#), size %d bytes (%d words)\n********************\n", wdp2->areaOffset - 1, wdp2->areaOffset / 2 - 1,
						wdp2->areaOffset - wdp->areaOffset, (wdp2->areaOffset - wdp->areaOffset) / 2);

				else
					fprintf(fp, "endaddress is 0x%04X(byte#) or 0x%04X(word#), size %d bytes (%d words)\n********************\n", ap->endAddr - 1, ap->endAddr / 2 - 1,
						ap->endAddr - wdp->areaOffset, (ap->endAddr - wdp->areaOffset) / 2);


			}
			if (wdp->wdIsLabel)
			{
				if (wdp->sym->symIsGlobl)
					fprintf(fp, "Global ");
				else
					fprintf(fp, "Local  ");
				fprintf(fp, "%-16s=0x%06X (byte) 0x%06X (word)\n", wdp->sym->name, wdp->areaOffset, wdp->areaOffset / 2);
			}
		}
	}
	fprintf(fp, "\n********************\nEQU defined information:\n********************\n");
	for (mdp = topMap.linkedModules; mdp; mdp = mdp->next)
	{
		int i;

		for (i = 0; i < mdp->exportSymNum; i++)
		{
			sp = mdp->exportSym[i];
			if (sp->symIsEQU && getExpValue(sp->exp, mdp, 0, -10000, 0,0,NULL,NULL)!=DUMMY_CONST) // equ uses NULL is ok
				fprintf(fp, "module %-20s global %-20s EQU %04X\n", mdp->name, sp->name,
					getExpValue(sp->exp, mdp, 0, -10000, 0,0,NULL,NULL));
		}
		for (i = 0; i < mdp->localSymNum; i++)
		{
			sp = mdp->localSym[i];
			if (sp->symIsEQU && getExpValue(sp->exp, mdp, 0, -10000, 0,0, NULL,NULL)!=DUMMY_CONST)
				fprintf(fp, "module %-20s local  %-20s EQU %04X\n", mdp->name, sp->name,
					getExpValue(sp->exp, mdp, 0, -10000, 0,0,NULL,NULL));
		}

	}
	// call tree information

	// generated symbol
	if (topMap.generatedSymNum > 0)
	{
		int i,j;
//		char buf[1024];
		fprintf(fp, "\n********************\nLinker Generated Symbols:\n********************\n");
		for (i = 0; i < topMap.generatedSymNum; i++)
		{
			// it is equ
			if((j= getSymValue(topMap.linkerGeneratedSym[i], NULL, 0, 0, 0, NULL, 0, NULL))!=0)
				fprintf(fp, "%-20s: 0x%X\n", topMap.linkerGeneratedSym[i]->name, j);
		}
	}
	
	now = time(0);
	strftime(buffer, 256, "%Y-%m-%d %H:%M:%S", localtime(&now));
	fprintf(fp, "\nMAP file generated at %s.\n", buffer);

	fclose(fp);
	if (allowRAMOVERLIMIT && ramOutOfRange)
	{
		fprintf(stderr, "\nWarning!!  RAM Ussage over-limit!!\n\n");
		return 0;
	}
	if (ramOutOfRange)
	{
		fprintf(stderr, "\nError!! RAM Ussage over-limit!!\n\n");
	}
		
	return ramOutOfRange;
}
#ifndef min
#define min(a,b) ((a<=b)?a:b)
#endif
#define line_length 120

int motorHexOut(FILE *fp, unsigned long begin_addr, unsigned long end_addr, unsigned char *bufi, int do_headers)
{
	int i;
	unsigned long max_addr, address;
	int byte_count, this_line;
	unsigned char checksum;
	unsigned long c;
	int record_count = 0;
	int addr_offset = 0;
	int addr_bytes = 3;
	int buf_offset = 0;

	//unsigned char buf[32];

	addr_offset = begin_addr;
	max_addr = end_addr;


	//max_addr = addr_offset + (end_addr - begin_addr);

	//fseek(infile, begin_addr, SEEK_SET);

	if ((max_addr > 0xffffl) && (addr_bytes < 3))
		addr_bytes = 3;

	if ((max_addr > 0xffffffl) && (addr_bytes < 4))
		addr_bytes = 4;

	if (verbose)
	{
		fprintf(stderr, "Begin address   = %Xh\n", (unsigned int)begin_addr);
		fprintf(stderr, "End address     = %Xh\n", (unsigned int)end_addr);
		fprintf(stderr, "Address offset  = %Xh\n", (unsigned int)addr_offset);
		fprintf(stderr, "Maximum address = %Xh\n", (unsigned int)max_addr);
		fprintf(stderr, "Address bytes   = %d\n", addr_bytes);
	}

	if (do_headers &1)
		fprintf(fp, "S00600004844521B\n");		/* Header record */

	address = addr_offset;

	for (;;)
	{
		if (verbose)
			fprintf(stderr, "Processing %08Xh\r", (unsigned int)address);

		this_line = min(line_length, (max_addr - address) + 1);
		byte_count = (addr_bytes + this_line + 1);
		fprintf(fp,"S%d%02X", addr_bytes - 1, byte_count);

		checksum = byte_count;

		for (i = addr_bytes - 1; i >= 0; i--)
		{
			c = (address >> (i << 3)) & 0xff;
			fprintf(fp,"%02X", (unsigned int)c);
			checksum += (unsigned char)c;
		}

		//if (fread(buf, 1, this_line, infile)

		for (i = 0; i < this_line; i++)
		{
			fprintf(fp,"%02X", bufi[buf_offset]);
			checksum += bufi[buf_offset];
			buf_offset++;
		}

		fprintf(fp,"%02X\n", 255 - checksum);

		record_count++;

		/* check before adding to allow for finishing at 0xffffffff */
		if ((address - 1 + line_length) >= max_addr)
			break;

		address += line_length;
	}

	if (do_headers & 2)
	{
		if (record_count > 0xffff)
		{
			checksum = 4 + (record_count & 0xff) + ((record_count >> 8) & 0xff) + ((record_count >> 16) & 0xff);
			fprintf(fp,"S604%06X%02X\n", record_count, 255 - checksum);
		}
		else
		{
			checksum = 3 + (record_count & 0xff) + ((record_count >> 8) & 0xff);
			fprintf(fp,"S503%04X%02X\n", record_count, 255 - checksum);
		}

		byte_count = (addr_bytes + 1);
		fprintf(fp,"S%d%02X", 11 - addr_bytes, byte_count);

		checksum = byte_count;

		for (i = addr_bytes - 1; i >= 0; i--)
		{
			c = (addr_offset >> (i << 3)) & 0xff;
			fprintf(fp,"%02X", (unsigned int)c);
			checksum += (unsigned char) c;
		}
		fprintf(fp,"%02X\n", 255 - checksum);
	}

	if (verbose)
		fprintf(stderr, "Processing complete \n");
	return 0;
}
int doGenMotHex(void)
{
	// we check how many segments
	unsigned char prev_assigned=0;
	int first = 0;
	int last = 0;
	int parts = 0;
	int preva = 0;
	int i;
	char motName[PATH_MAX];
	FILE *fp;
	strncpy(motName, outputBasename, PATH_MAX);
	ChangeFileExt(motName, ".mot");
	fp = fopen(motName, "w");
	if (fp == NULL)
	{
		fprintf(stderr, "intel hex file %s open error.\n", motName);
		return -1;
	}

	for (i = 0; i < MAX_ROM_SIZE; i++)
	{
		if (rom_assigned[i] && !prev_assigned)
			parts++;
		prev_assigned = rom_assigned[i];
	}
	last = parts - 1;
	if (last < 0)
	{
		fclose(fp);
		return -1;
	}
	prev_assigned = 0;
	parts = 0;
	for (i = 0; i < MAX_ROM_SIZE; i++)
	{

		if (rom_assigned[i] && !prev_assigned)
			preva = i;
		if (!rom_assigned[i] && prev_assigned)
		{
			int doheaders = 0;
			if (parts == 0)
				doheaders |= 1;
			if (parts == last)
				doheaders |= 2;
			motorHexOut(fp, (unsigned long)preva, (unsigned long)i - 1, rom_data + preva, doheaders);
			parts++;
		}
		prev_assigned = rom_assigned[i];
	}
	fclose(fp);
	return 0;

}
// merge Old OTP data
int mergeOldOTP(void)
{
//	int i;
	memcpy(rom_data, oldBinFile, newOtpCodeWordOffset * 2 - 8);
	memset(rom_assigned, 1, newOtpCodeWordOffset * 2 - 8);
	rom_data[addrJmpMain * 2] = rom_data[newOtpCodeWordOffset * 2 - 6];
	rom_data[addrJmpMain * 2+1] = rom_data[newOtpCodeWordOffset * 2 - 5];
	rom_data[addrJmpMain * 2+2] = rom_data[newOtpCodeWordOffset * 2 - 4];
	rom_data[addrJmpMain * 2+3] = rom_data[newOtpCodeWordOffset * 2 - 3];
	return 1;
}
int doGenIntelHex(void)
{
	char ihxName[PATH_MAX];
	FILE *fp;
	int i;
	int endaddr=0;
	strncpy(ihxName, outputBasename, PATH_MAX);
	ChangeFileExt(ihxName, ".ihx");
	fp = fopen(ihxName, "w");
	if (fp == NULL)
	{
		fprintf(stderr,"intel hex file %s open error.\n", ihxName);
		return -1;
	}
	// we need a final address?
	
	for (i = 0; i < sizeof(rom_data); i++)
	{
		if (!rom_assigned[i])
		{
			continue;
		}
		hexout(fp, rom_data[i], i, 0);
	}
	hexout(fp, -1, i, 1);
	fclose(fp);
	return 0;
}





#define MAXHEXLINE 32	/* the maximum number of bytes to put in one line */

int hexout(FILE *fhex, int byte, int memory_location, int end)
{
	static int byte_buffer[MAXHEXLINE];
	static int last_mem, buffer_pos, buffer_addr;
	static int writing_in_progress = 0;
	register int i, sum;

	if (!writing_in_progress) {
		/* initial condition setup */
		last_mem = memory_location - 1;
		buffer_pos = 0;
		buffer_addr = memory_location;
		writing_in_progress = 1;
	}

	if ((memory_location != (last_mem + 1)) || (buffer_pos >= MAXHEXLINE) \
		|| ((end) && (buffer_pos > 0))) {
		/* it's time to dump the buffer to a line in the file */
		fprintf(fhex, ":%02X%04X00", buffer_pos, buffer_addr);
		sum = buffer_pos + ((buffer_addr >> 8) & 255) + (buffer_addr & 255);
		for (i = 0; i < buffer_pos; i++) {
			fprintf(fhex, "%02X", byte_buffer[i] & 255);
			sum += byte_buffer[i] & 255;
		}
		fprintf(fhex, "%02X\n", (-sum) & 255);
		buffer_addr = memory_location;
		buffer_pos = 0;
	}

	if (end) {
		fprintf(fhex, ":00000001FF\n");  /* end of file marker */
//		fclose(fhex);
		writing_in_progress = 0;
	}

	last_mem = memory_location;
	byte_buffer[buffer_pos] = byte & 255;
	buffer_pos++;
	return buffer_pos;
}
static const char* getFileNameFromPath(const char* path)
{
	int i;
	for (i = strlen(path) - 1; i >= 0; i--)
	{
		if (path[i] == '/' || path[i]=='\\')
		{
			return &path[i + 1];
		}
	}

	return path;
}
// compare L/A record to sort



static int compareLArec(char *rec1, char *rec2)
{
	// we check first ',' with final ':'
	char namebuf1[1024];
	char namebuf2[1024];
	int ln1;
	int ln2;
	int i,j;
	int len1 = strlen(rec1);
	int len2 = strlen(rec2);
	int rec1mark1 = -1;
	int rec1mark2 = -1;
	int rec2mark1 = -1;
	int rec2mark2 = -1;
	for (i = 0; i < len1; i++)
	{
		if (rec1[i] == ',' && rec1mark1 < 0)
			rec1mark1 = i;
		if (rec1[i] == ':')
			rec1mark2 = i;
	}
	for (i = 0; i < len2; i++)
	{
		if (rec2[i] == ',' && rec2mark1 < 0)
			rec2mark1 = i;
		if (rec2[i] == ':')
			rec2mark2 = i;
	}
	for (j = 0, i = rec1mark1; i < rec1mark2; i++, j++)
		namebuf1[j] = rec1[i];
	namebuf1[j] = '\0';

	for (j = 0, i = rec2mark1; i < rec2mark2; i++, j++)
		namebuf2[j] = rec2[i];
	namebuf2[j] = '\0';

	ln1 = atoi(rec1 + rec1mark2 + 1);
	ln2 = atoi(rec2 + rec2mark2 + 1);
	i = strcmp(namebuf1, namebuf2);
	if (i)
		return i;
	return (int)(ln1 - ln2);

}
int  compareSrcLine(srcLine **a, srcLine **b)
{
	return compareLArec((*a)->line, (*b)->line);
}

// lets generate the CDB file
int doGenCDBFile(void)
{
	char cdbFileName[PATH_MAX];
	FILE *fp;
	moduleLD *mdp;
	symbol *sp;
	area *ap;
	wData *wdp;
	uint64 macroHash=0;
	srcLine *entry;
	srcLine *LARECH = NULL;
	srcLine *LARECT = NULL;
	srcLine **LAArr = NULL;
	int LAnum = 0;
	int lastlineno;
	int i;
	asmIncInfo *aifp=NULL;
	strncpy(cdbFileName, outputBasename, PATH_MAX);
	ChangeFileExt(cdbFileName, ".cdb");
	fp = fopen(cdbFileName, "w");
	if (fp == NULL)
	{
		fprintf(stderr, "cdbfile %s open write error.\n", cdbFileName);
		return -1;
	}
	// include information, very first, before L:A record
	for (aifp = asmIncHead; aifp; aifp = aifp->next)
	{
		fprintf(fp, "I:%s$%d$%s\n", aifp->fname1, aifp->lineno, aifp->fname2);
	}
	// assembly macro record, we call it C record, it can be RPT or MAC R/M
	// C:R:<rptn>$line1$line2....
	// C:M$line1$line2.....
	for (i = 0; i < defined_macro_num; i++)
	{
		if (defined_macros[i]->isRPT)
		{
			fprintf(fp, "C:R$%s$%s$%d", macros_mod[i]->name, defined_macros[i]->macroName, defined_macros[i]->paras->exp->exp.number);
			for (entry = defined_macros[i]->lines; entry; entry = entry->next)
				fprintf(fp, "$%s", entry->line);
		}
		else
		{
			fprintf(fp, "C:M$%s$%s", macros_mod[i]->name, defined_macros[i]->macroName);
			for (entry = defined_macros[i]->lines; entry; entry = entry->next)
				fprintf(fp, "$%s", entry->line);
		}
		fprintf(fp, "\n");
	}

	for (i = 0; i < macInsNum; i++)
	{
		fprintf(fp, "C:I$%s$%s$%s\n", macInsMod[i]->name, macInsArray[i]->macroName, macInsArray[i]->locTree);
	}


	// at first, we list modules
	for (mdp = topMap.linkedModules; mdp; mdp = mdp->next)
	{
		
		if (mdp->isFromLib)
		{
			fprintf(fp, "M:%s:1:\"%s\":%s\n", mdp->name, mdp->libPath, getFileNameFromPath(mdp->srcName)); // give assembly name
		}
		else
		{
#ifdef _WIN32
			char buf[PATH_MAX];
			_fullpath(buf, mdp->srcName, PATH_MAX - 1);
			fprintf(fp, "M:%s:\"%s\"$\"%s\"\n", mdp->name, buf, mdp->asFromDir);
#else
			fprintf(fp, "M:%s:\"%s\"$\"%s\"\n", mdp->name, mdp->srcName, mdp->asFromDir);
#endif
		}
	}
	
	// then we list symbols, from linker generated
	for (i = 0; i < topMap.generatedSymNum; i++)
	{
		sp = topMap.linkerGeneratedSym[i];
		fprintf(fp, "L:%s:%X\n", sp->name, getSymValue(sp, NULL, 0, 0,0,NULL,0,NULL));// linker has no fp
	}
	// then for each module, we list the symbols , exported first
	for (mdp = topMap.linkedModules; mdp; mdp = mdp->next)
	{
		for (i = 0; i < mdp->exportSymNum; i++)
		{
			sp = mdp->exportSym[i];
			if (sp->symIsLabel && sp->labelPos && !sp->symIsByFSR2)
			{
				fprintf(fp, "L:%s:%X:%s\n", sp->name, getSymValue(sp, mdp, 0, 0, 0, NULL,0,NULL), // not fsr2 use null
					(sp->labelPos->ap->region==regionCODE)?"ROM":"RAM");
			}
			else if (sp->symIsByFSR2)
			{
				// it must be RAM!!
				// add a module name, consider static function
				fprintf(fp, "R:%s:%s:%s:%X:%s\n", sp->name, sp->fpHasthis->m->name, sp->fpHasthis->name, sp->fsr2origOffset, // R Record!!
					sp->labelPos?((sp->labelPos->ap->region == regionCODE) ? "ROM" : "RAM"):"RAM");
			}else
				fprintf(fp, "L:%s:%X\n", sp->name, getSymValue(sp, mdp, 0, 0, 0,NULL,0,NULL));
		}
		// then local variable, middle is _%_
		for (i = 0; i < mdp->localSymNum; i++)
		{
			sp = mdp->localSym[i];
			if (sp->symIsLabel && sp->labelPos && !sp->symIsByFSR2)
			{
				fprintf(fp, "L:%s_%%_%s:%X:%s\n", mdp->name, sp->name, getSymValue(sp, mdp, 0, 0, 0,NULL,0,NULL),
					(sp->labelPos->ap->region == regionCODE) ? "ROM" : "RAM"
					);
			}else if (sp->symIsByFSR2)
			{
				// it must be RAM!!
				fprintf(fp, "R:%s:%s:%s:%X:%s\n", sp->name, sp->fpHasthis->m->name,sp->fpHasthis->name, sp->fsr2origOffset, // R record
					(sp->labelPos->ap->region == regionCODE) ? "ROM" : "RAM");
			}
			else
				fprintf(fp, "L:%s_%%_%s:%X\n", mdp->name, sp->name, getSymValue(sp, mdp, 0, 0,0,NULL,0,NULL));
		}
	}
	// then there is A format, output form ROM only
	lastlineno = -1;
	// 2018 need sort!!??

	for (ap = topMap.romArea; ap; ap = ap->next)
	{
		for (wdp = ap->wDatas; wdp; wdp = wdp->next)
		{
			// it is possible macros
			//if (wdp->macro_line_hash != macroHash && wdp->macroSrc!=NULL)
			//{
			//	macroHash = wdp->macro_line_hash;
			//	// make ice show this line in assembly
			//	if(wdp->macro_id==-2) // include 
			//		fprintf(fp, "L:A$%s$%X$%s\n", wdp->m->name, wdp->areaOffset,wdp->locTree); // A replace I
			//	else // for every byte of code, we need a "string" to find  the source, that is what LOCTREE for!!
			//		fprintf(fp, "L:M$%s$%X$%s$%s\n", wdp->m->name, wdp->areaOffset, wdp->macroSrc,wdp->locTree);
			//}


			if (wdp->locTree && ((!(wdp->areaOffset&1))&&(wdp->ap->region==regionCODE)) && !wdp->wdIsLabel)
			//if (wdp->locTree && (!(wdp->areaOffset&1)&&(wdp->ap->region==regionCODE)) )
			{
				//if(wdp->macro_id!=-2)
				char linebuf[MAXSTRSIZE1*3+100];
				snprintf(linebuf,MAXSTRSIZE1*3+99, "L:A$%s$%X$%s\n", wdp->m->name, wdp->areaOffset,wdp->locTree);
				
				appendSrcLine2(&LARECH, &LARECT, newSrcLine(linebuf, -1));
				LAnum++;

				//lastlineno = wdp->lineno;
			}
			
		}
		

	}
	LAArr = (srcLine**)malloc(sizeof(srcLine*)*LAnum);
	for (i = 0, entry = LARECH; i < LAnum; i++,entry=entry->next)
		LAArr[i] = entry; // put them to array
	// now sort the array
	
	/*
	for (i = 0; i < LAnum - 1; i++)
	{
		int j;
		for (j = 0; j < LAnum - 1 - i; j++)
		{
			if (compareLArec(LAArr[j]->line, LAArr[j + 1]->line)>0)
			{
				entry = LAArr[j];
				LAArr[j] = LAArr[j + 1];
				LAArr[j + 1] = entry;
			}
		}
	}
	*/
	
	qsort(&LAArr[0], LAnum, sizeof(srcLine*) , ( int (*)(const void*, const void*))compareSrcLine);
	
	for (i = 0; i < LAnum; i++)
		fprintf(fp, "%s", LAArr[i]->line);


	// additional ones
	for (i = 0; i < needAddLANum; i++)
	{
		char linkerInstr[128];
		int code1 = needAddLA[i]->wValue;
		int code2 = 0;
	         if(needAddLA[i]->next) code2	= needAddLA[i]->next->wValue;

		switch (code1)
		{
		case 0x06: snprintf(linkerInstr, 127, "MVL 0x%02X", code2); break;
		case 0x66: snprintf(linkerInstr, 127, "MVF 0x%02X,1,0", code2); break;
		case 0x64: snprintf(linkerInstr, 127, "MVF 0x%02X,0,0", code2); break;
		case 0x12: snprintf(linkerInstr, 127, "ADDF 0x%02X,1,0", code2); break;
		case 0x04: snprintf(linkerInstr, 127, "ADDL 0x%02X,1,0", code2); break;
		case 0x14: snprintf(linkerInstr, 127, "ADDC 0x%02X,1,0", code2); break;
		case 0x67: snprintf(linkerInstr, 127, "MVF @FSR2-%d,1,0", code2); break;
		case 0x65: snprintf(linkerInstr, 127, "MVF @FSR2-%d,0,0", code2); break;
		case 0xc5: snprintf(linkerInstr, 127, "PUSHL 0x%02X", code2); break;
		case 0xc4: 
			if ((code2 & 0xc0) == 0xc0)
			{
				code2 = code2 & 0x3f;
				if (code2 & 0x20)
					code2 -= 64;
				snprintf(linkerInstr, 127, "ADDUNLK %d", code2); 
			}
			else
			{
				int fsri = code2 >> 6;
				code2 = code2 & 0x3f;
				if (code2 & 0x20)
					code2 -= 64;
				snprintf(linkerInstr, 127, "ADDFSR %d,%d", fsri,code2); 
			}
			break;
		default: 
			linkerInstr[0] = '\0'; break;
		}
		fprintf(fp, "L:K$%s$%d:%X:%s\n", needAddLA[i]->m->name, needAddLA[i]->lineno, needAddLA[i]->areaOffset,
			linkerInstr); // K means by linker
	}
	// for HY15P41, we have new kinds of records, R record is for FSR2 memory, we have O record for fsr offset 
	// reference to PC
	for (ap = topMap.romArea; ap; ap = ap->next)
	{
		for (wdp = ap->wDatas; wdp; wdp = wdp->next)
		{
			if (wdp->instHighByte && wdp->fp && (wdp->fp->p41ReEntry || wdp->fp->paraOnStack || wdp->fp->h08dReEntry))
			{
				fprintf(fp, "O:%X:%X\n", wdp->areaOffset, wdp->fsr2_stack_offset);
			}
		}
	}
	

	fclose(fp);
	return 0;
}

// BTSS _STATUS,Z (0xb0,0x2b)  +RJ ==> means no jump when z set, or jump when z not set ==> JNZ, 0x75
// BTSC _status,Z (0xa0,0x2b)  +RJ ==> JZ, 0x71
// BTSS _STATUS,c (0xb8,0x2b)  +RJ ==> JNC, 0x74
// BTSC _status,c (0xa8,0x2b)  +RJ ==> JC, 0x70
// BTSS _STATUS,N (0xb4,0x2b)  +RJ ==> JNN, 0x76
// BTSC _STATUS,N (0xa4,0x2b)  +RJ ==> JN, 0x72
// BTSS _STATUS,O (0xb2,0x2b)  +RJ ==> JNO, 0x77
// BTSC _STATUS,O (0xa2,0x2b)  +RJ ==> JO, 0x73

static int opChange(int bxx)
{
	switch (bxx)
	{
	case 0xb0: return 0x75;
	case 0xa0: return 0x71;
	case 0xb8: return 0x74;
	case 0xa8: return 0x70;
	case 0xb4: return 0x76;
	case 0xa4: return 0x72;
	case 0xb2: return 0x77;
	case 0xa2: return 0x73;
	default:
		fprintf(stderr, "Linker Error. internal jxx optimization error.\n");
		exit(-1000);
	}
	return -1;
}

// this is the key, JMP 2 RJ, CALL 2 RCALL
// it needs loop and loop ... many times ... I think it is OK for today's PC
int checkNextNop(wData *wdp)
{
	wData *nextw;
	
	
	if (wdp == NULL)
		return 0;
	if (wdp->next == NULL)
		return 0;
	if (wdp->next->next == NULL)
		return 0;
	nextw = wdp->next->next;
	if (nextw->wdIsLabel)
		nextw = nextw->next;
	if (nextw == NULL)
		return 0;
	if (nextw->next == NULL)
		return 0;
	if (nextw->wValue == 0 && nextw->next->wValue == 0)
		return 1;
	return 0;
}
#define BIT_C 4
#define BIT_Z 0
#define BIT_OV 1
#define BIT_N 2
int jxxOptimize(void) // now we comes to JXX
{
	int count = 0;
	area *ap;
	wData *wdp;
	wData *prevW;
	wData *prevW2;
	wData *prevW3;
	wData *prevW4;
	int wordDiff = 0xffff;
	int absAddr = 0xffffffff;
	int chkv;
	int chkbit;
	for (ap = topMap.romArea; ap; ap = ap->next)
	{
		prevW4 = prevW3 = prevW2 = prevW = NULL;
		for (wdp = ap->wDatas; wdp; wdp = wdp->next) // be careful the ->next
		{
			// we looking for RJ
			if (wdp->wdIsOrg || wdp->wdIsLabel || wdp->sizeOrORG != 1 || (wdp->areaOffset & 1) || !wdp->wdIsInstruction)
				goto cont1;
			if (prevW == NULL || prevW2 == NULL || prevW3 == NULL || prevW4 == NULL)
				goto cont1;
			chkv = prevW2->wValue & 0xf1; // status bit 
			chkbit = (prevW2->wValue & 0x0e) >> 1; // which bit, we use 
			if (chkbit != BIT_C && chkbit != BIT_Z && chkbit != BIT_N && chkbit != BIT_OV)
				goto cont1;
			if ((wdp->wValue & 0xf8)!= 0x78)
				goto cont1;
			if( prevW->wValue == 0x2B && (chkv == 0xa0 || chkv == 0xb0) )
										// rj  , if this is rj , we need prevw2==BTS? STATUS,?
										// BTSS, BTSC is aXXX , bxxx bit 8 is 0, status is 2B

				
			{
				// get the word offset
				absAddr = ((wdp->wValue & 7)<<8) | (wdp->next->wValue);
				if (absAddr > 1023)
					absAddr -= 2048;
				
				if (absAddr <=127 && absAddr>=-128) // now we can change
				{
					//		fprintf(stderr, "it can optimize ..\n");
					int newop = opChange(prevW2->wValue);
					
					prevW2->exp = newConstExp(newop, prevW2->exp->lineno);
					prevW2->next = wdp->next; // skip 2 instructions
					wdp->next->prev = prevW2;
					wdp->next->exp->exp.tree.OverflowChk = 1; // the expression not changed!!

					count++;
				}


			}
		cont1:
			prevW4 = prevW3;
			prevW3 = prevW2;
			prevW2 = prevW;
			prevW = wdp;
		}
	}


	return count;
}

static expression *leftMostExp(expression*exp)
{
	if (exp->type != ExpIsTree)
		return exp;
	return leftMostExp(exp->exp.tree.left);
}


static int expHasShiftRight8(expression *exp) // as its name
{
	if (exp->type == ExpIsTree && exp->exp.tree.op == 'A' && exp->exp.tree.right->type ==ExpIsNumber &&
		exp->exp.tree.right->exp.number==8)
		return 1;
	if (exp->type == ExpIsTree)
		return expHasShiftRight8(exp->exp.tree.left) | expHasShiftRight8(exp->exp.tree.right);
	return 0;
}

int callret2jmp(int stage)
{
	area *ap;
	wData *wdp;
	wData *prevW;
//	wData *wdpLabel;

	wData *newW;
	wData *prevI;
	wData *prevII;
	wData *prevIII;
	int count = 0;
//	char buffer[256];
//	char buffer1[256];
	//int wordDiff = 0xffff;
	//int absAddr = 0xffffffff;
//	expression *symExp;
	int lastskip;

	for (ap = topMap.romArea; ap; ap = ap->next)
	{
		prevW = NULL;
		prevI = NULL;
		prevII = NULL;
		prevIII = NULL;
		for (wdp = ap->wDatas; wdp && wdp->next && wdp->next->next && wdp->next->next->next && wdp->next->next->next->next ; wdp=wdp->next) // be careful the ->next
		{
			// lets replace!!
			if (!wdp->instHighByte)
				goto cont1;
			if (wdp->wdIsOrg || wdp->wdIsLabel || wdp->sizeOrORG != 1 || !wdp->wdIsInstruction || prevW == NULL)

				goto cont1;
			lastskip = 0;
			if (prevI!=NULL && prevI->instSkip || (prevII!=NULL && prevIII!=NULL && prevI->exp->type==ExpIsTree && prevII->exp->type==ExpIsNumber
				&& prevII->exp->exp.number==0xc2)) // jmp
			{
				//exprDump(prevI->exp, buffer, 255);
//				prevI = wdp;
				//goto cont1;
				lastskip = 1;
				
			
			}
			prevIII = prevII;
			prevII = prevI;
			prevI = wdp;

			
			//  if(wdp->lineno==209) // debug?
			// {
			// 	char buf[1024];
			// 	fprintf(stderr,"209\n");
			// 	if (wdp->exp->type == ExpIsNumber && wdp->exp->exp.number == 0xc0)
			// 	{
			// 		exprDump(wdp->next->next->exp,buf,1023);
			// 		fprintf(stderr,"%s\n",buf);
			// 		exprDump(wdp->next->next->next->next->exp,buf,1023);
			// 		fprintf(stderr,"%s\n",buf);
			// 		exprDump(wdp->next->next->next->next->next->exp,buf,1023);
			// 		fprintf(stderr,"%s\n",buf);
			// 	}
			// }
			if (wdp->exp->type == ExpIsNumber && wdp->exp->exp.number == 0xc0
				
				&& leftMostExp(wdp->next->next->exp)->type == ExpIsSymbol
				&& isWdRET(wdp->next->next->next->next,0) // cannot skip label
				) // lcall or ljmp, high address 0
			{
				// get the word offset
				// we check the label and see if it is a ret
				

				if (!lastskip && !wdp->next->next->wdIsLabel)
				{
					funcRecord *funcp;
					//char buf2[256] ;
					//char buf3[256];
					//if(prevII!=NULL && prevII->exp!=NULL)
					//	exprDump(prevII->exp, buf2, 255);
					//if( prevIII!=NULL && prevIII->exp!=NULL)
					//	exprDump(prevIII->exp, buf3, 255);
					prevW = wdp->next->next->next;
					newW = wdp->next->next->next->next;
					wdp->exp->exp.number = 0xc2; // change to jmp!!
					while (!newW->instHighByte)
					{
						newW = newW->next;
						prevW = prevW->next;
					}
					//prevW->next = newW->next->next;
					newW->sizeOrORG = 0;
					newW = newW->next;
					while (!newW->wdIsInstruction)
						newW = newW->next;
					newW->sizeOrORG = 0;
					prevW->next = newW->next;
					if(newW->next)
						newW->next->prev = prevW; // 2022 oct dual dir linklist


					// add a mark 2021 Feb,
					if ((funcp = findFuncByHash(leftMostExp(wdp->next->next->exp)->symHash, wdp->m)) != NULL)
					{
						funcp->callret2jmpOptimized = 1;
					}
					count++;

				}
			}
			//else
			//	fprintf(stderr, "no opt..\n");



		cont1:
			prevW = wdp;
		}
	}
	return count;
}





int mvlret2retlw(void)
{
	// afer jmpret2ret, there will be new ret, mvl+ret can become retlw
	area *ap;
	wData *wdp;
	wData *prevW;
//	wData *wdpLabel;

//	wData *newW;
	int count = 0;
	int wordDiff = 0xffff;
	int absAddr = 0xffffffff;
//	expression *symExp;
	wData *prevI;
	int lastskip;
	for (ap = topMap.romArea; ap; ap = ap->next)
	{
		prevW = NULL;
		prevI = NULL;
		for (wdp = ap->wDatas; wdp && wdp->next; wdp = wdp->next) // be careful the ->next
		{
			// lets replace!!
			if (!wdp->instHighByte)
				continue;
			lastskip = 0;
			if (prevI && prevI->instSkip)
				lastskip = 1;
			prevI = wdp;
			if (lastskip || wdp->wdIsOrg || wdp->wdIsLabel || wdp->sizeOrORG != 1 || !wdp->wdIsInstruction || prevW == NULL)

				goto cont1;

			if (isWdMVL(wdp,0) && isWdRET(wdp->next->next,0))// no label!!
			{
				wdp->exp->exp.number = 0x05;
				wdp->next->next = wdp->next->next->next->next;
				if(wdp->next->next)
					wdp->next->next->prev = wdp->next; // 2022 oct dual dir linklist
				count++;
			}

			//else
			//	fprintf(stderr, "no opt..\n");



		cont1:
			prevW = wdp->next;
		}
	}
	return count;

}
int jmpRet2Ret(void)// when this func is called we consider far call/far jump only
{
	area *ap;
	wData *wdp;
	wData *prevW;
	wData *wdpLabel;

	wData *newW;
	int count = 0;
	int wordDiff = 0xffff;
	int absAddr = 0xffffffff;
//	expression *symExp;
	wData *prevI;
	int lastskip;
	for (ap = topMap.romArea; ap; ap = ap->next)
	{
		prevW = NULL;
		prevI = NULL;
		for (wdp = ap->wDatas; wdp && wdp->next; wdp = wdp->next) // be careful the ->next
		{
			// lets replace!!
			if (!wdp->instHighByte)
				continue;
			lastskip = 0;
			if (prevI && prevI->instSkip)
				lastskip = 1;
			prevI = wdp;
			if (lastskip || wdp->wdIsOrg || wdp->wdIsLabel || wdp->sizeOrORG != 1 || !wdp->wdIsInstruction || prevW==NULL)

				goto cont1;

			
			
			if (wdp->exp->type==ExpIsNumber && wdp->exp->exp.number==0xc2 
				&& wdp->next->next
				&& wdp->next->next->exp->type == ExpIsTree
				&& leftMostExp(wdp->next->next->exp)->type ==ExpIsSymbol
				) // lcall or ljmp, high address 0
			{
				// get the word offset
				// we check the label and see if it is a ret
				symbol *sym = findSymInModuleLD(leftMostExp(wdp->next->next->exp)->exp.symbol,
					wdp->m,
					1, 1);
				if (!sym || !sym->symIsLabel)
					continue;
				wdpLabel = sym->labelPos;
				if (isWdRET(wdpLabel,1))
				{
					if(verbose)
						fprintf(stdout,"JMPRET2RET--%s:%s\n",wdp->m->name,wdpLabel->sym->name);
					prevW->next = newWDATA1(wdp->ap);
					prevW->next->prev = prevW;
					newW = prevW->next;
					newW->m = wdp->m;
					newW->fp = wdp->fp;
					newW->lineno = wdp->lineno;
					newW->wdIsInstruction = 1;
					newW->instHighByte = 1;
					newW->exp = newConstExp(0x0, wdp->lineno); // mvfw
					newW->locTree = wdp->locTree;
					
					newW->next = newWDATA1(wdp->ap);
					newW->next->prev = newW;
					newW = newW->next;
					newW->m = wdp->m;
					newW->fp = wdp->fp;
					newW->wdIsInstruction = 1;
					newW->lineno = wdp->lineno;
					newW->sizeOrORG = 1;
					newW->exp = newConstExp(0x0a, wdp->lineno); // mvfw
					newW->next = wdp->next->next->next->next;
					if(newW->next)
						newW->next->prev = newW; // dual link, possible NULL
					newW->locTree = wdp->locTree;
					

					count++;
				}
			}
				//else
				//	fprintf(stderr, "no opt..\n");


			
		cont1:
			if (wdp->next->next && wdp->next->next->wdIsLabel)
				prevW = wdp->next->next;
			else
				prevW = wdp->next;
		}
	}
	return count;
}
int check_label_referenced(int64_t labelhash, wData *excludew)
{
	area *ap;
	wData *wdp;
	for (ap = topMap.romArea; ap; ap = ap->next)
	{
		for (wdp = ap->wDatas; wdp&&wdp->next; wdp = wdp->next)
		{
			if (wdp!=excludew && wdp->exp && checkIfExpRefLabel(wdp->exp, labelhash, 0))
				return 1;
		}
	}
	return 0;
}
int remove_nouse_label(void) // sometimes jmp ret2ret..> label ret no use, we remove it
{
	area *ap;
	wData *wdp;
	wData *wprev;
	int count = 0;
	for (ap = topMap.romArea; ap; ap = ap->next)
	{
		wprev = NULL;
		for (wdp = ap->wDatas; wdp&&wdp->next; wprev=wdp,wdp = wdp->next)
		{
			if (!wprev)
				continue;
			if (wdp->wdIsLabel && isWdRET(wdp, 1))
			{
				if (!check_label_referenced(wdp->sym->hashid, wdp))
				{
					wprev->next = wdp->next; // wd 1 entry for a label!!
					wprev->next->prev = wprev;
					count++;
				}
			}
		}
	}
	return count;
}
static int has7wd(wData* wdp)
{
	int i;
	if (!wdp)
		return 0;
	if (!wdp->wdIsInstruction)
		return 0;
	for (i = 0; i < 7; i++)
	{
		wdp = wdp->next;
		if (!wdp)
			return 0;
		if (!wdp->wdIsInstruction)
			return 0;
	}
	return 1;
}


int jmpCall2RJRCALL(void)
{
	area *ap;
	wData *wdp;
	wData *wdpAbs;
	wData *prevW;
	wData *nextW;
	int count = 0;
	int wordDiff = 0xffff;
	int absAddr=0xffffffff;
	expression *symExp;
	for (ap = topMap.romArea; ap; ap = ap->next)
	{
		prevW = NULL;
		for (wdp = ap->wDatas; wdp; wdp = wdp->next) // be careful the ->next
		{
			// lets replace!!
			if (!wdp->instHighByte)
			
				goto cont1;
			
			if (prevW == NULL) // skip first jmp ... now
				goto cont1;
			absAddr = wdp->areaOffset;
			if ((wdp->wValue == 0xc2 || wdp->wValue == 0xc0) // long jump and long call, not shadow of call!! C1 is not allowed
				&& wdp->next->next 
				&& wdp->next->next->exp->type == ExpIsTree 
				) // lcall or ljmp, high address 0
			{
				// get the word offset
				int jmp0=0;
				wData *prevwh=NULL;
				wData *prevwl=NULL;
				wdpAbs = wdp->next->next;
				absAddr = ((wdp->next->wValue & 0x0f) << 12) | ((wdpAbs->wValue & 0x0f) << 8) | (wdpAbs->next->wValue);
				wordDiff = absAddr - ((wdp->areaOffset + 2) >> 1) ;

				// 2022 July special case JMP +0
				if(wdp->wValue == 0xc2 )
				{
					if(wordDiff==1 ) // there is offset
					{
						prevwh=findPrevWdp(wdp);
						while(prevwh && !prevwh->instHighByte)
							prevwh=findPrevWdp(prevwh);
						if(prevwh && !prevwh->instSkip)
							jmp0=1;						
					}
				}
				// we have 11 bit for relative addressing , signed, word address is -1024~1023
				if(jmp0)
				{
					prevwl = findPrevWdp(wdp);
					prevwl->next = wdp->next->next->next->next;
					
					prevwl->next->prev = prevwl; // dual dir link
					count++;
				}
				else if (wordDiff <= 1024 && wordDiff >= -1024 &&  // it will down 1 after down size!!
					!( (wdp->areaOffset < 8) || (replaceOtp && wdp->areaOffset<newOtpCodeWordOffset*2)
						 && checkNextNop(wdpAbs)))
				{
			//		fprintf(stderr, "it can optimize ..\n");

					expression *diff;
					expression *sh9;
					expression *and7;
					expression *base;
					expression *highbyte;
					expression *sh1;
					expression *andff;

					symExp = wdpAbs->exp->exp.tree.left->exp.tree.left->exp.tree.left; // left *3
					nextW = wdpAbs->next->next;


					int newopcode = (wdp->wValue == 0xc2) ? 0x78 : 0xc8; // rj, rcall

					base = newConstExp(newopcode, wdp->lineno);
					diff = newTreeExp(symExp, newPCNOWExp(wdp->lineno), '-', wdp->lineno);
					sh9 = newTreeExp(diff, newConstExp(9, wdp->lineno), 'A', wdp->lineno);// diff >>9
					and7 = newTreeExp(sh9, newConstExp(7, wdp->lineno), '&', wdp->lineno); and7->exp.tree.OverflowChk = 1; // it is signed for n11
					highbyte = newTreeExp(base, and7, '|', wdp->lineno);
					

					

					//diff = newTreeExp(inst->operand, newPCNOWExp(inst->lineno), '-', inst->lineno);
					// expression share part.. should be OK
					sh1 = newTreeExp(diff, newConstExp(1, wdp->lineno), 'A', wdp->lineno);//>>1
					andff = newTreeExp(sh1, newConstExp(0xff, wdp->lineno), '&', wdp->lineno);
					wdp->exp = highbyte;
					//wdp->instSkip = 1; // skip ?? 

					
					wdp->next->exp = andff;
					wdp->next->next = nextW; // leaves others along
					if(nextW)
						nextW->prev = wdp->next; // durl link

					count++;
				}
				//else
				//	fprintf(stderr, "no opt..\n");
			

			} 
cont1:
			prevW = wdp;
		}
	}
	return count;
}

// dummy no need because auto skip 
#define MAX_DUMMY_STK 1 
// special case, for some functions, the parameter is optimized, but others still call it,
// we create the dummy EQU first, and later 
int createDummyParam(moduleLD *mdp)
{
	funcRecord *funcp;
	expression *dummyExp;
	symbol *dummySym;
	int funcIsGlobl;
	char dummyName[MAXSTRSIZE0+1024];
	int i,j;
	uint64 hashcode;
	int createCount = 0;
	
	for (i = 0; i < mdp->funcNum; i++)
	{
		funcp = mdp->funcs[i];
		if (funcp->namehash == hashMain)
			continue;// no main stk
		funcIsGlobl = (findExportSymInMld(funcp->namehash, mdp) == NULL) ? 0 : 1;
		for (j = 0; j < MAX_DUMMY_STK; j++)
		{
			snprintf(dummyName, MAXSTRSIZE0+1023, "%s_STK%02d", funcp->name, j);
			hashcode = hash(dummyName);
			if (findSymInModuleLD(dummyName, mdp, 0,1)) // no err
				continue;
			// if not found, we create dummy parameter
			dummyExp = newConstExp(DUMMY_CONST, 0);
			dummySym = newSymbol(dummyName);
			dummySym->symIsEQU = 1;
			dummySym->exp = dummyExp;
			createCount++;
			if (funcIsGlobl)
			{
				mdp->exportSym[mdp->exportSymNum++] = dummySym;
			}
			else
			{
				mdp->localSym[mdp->localSymNum++] = dummySym;
				if (mdp->localSymNum >= 32768)
				{
					fprintf(stderr, "Error, module %s too many local symbols.\n", mdp->name);
					exit(-__LINE__);
				}
			}

		}
	}
	return createCount;
}

static expression*dummySymbolUsedInExp(expression *exp, moduleLD *mdp, int lev)
{
	symbol *sp;
	expression *expr;
	if (!exp)
		return NULL;
	if (!mdp)
		return NULL;
	switch (exp->type)
	{
	case ExpIsNumber:
	case ExpIsASCII:
	case ExpIsPCNow:
		return NULL;
	case ExpIsSymbol:
		sp = findSymInModuleLD(exp->exp.symbol, mdp, 1,0);
		if (sp == NULL)
		{
			fprintf(stderr, "Linker Internal err!!! (%s:%d) symbol %s not found in module %s.\n",
				__FILE__,__LINE__,
				exp->exp.symbol, mdp->name);
			exit(-40);
		}
		if (getSymValue(sp, sp->ldm, lev, 0, 0,NULL,0,NULL) == DUMMY_CONST) // only EQU have dummy
			return exp;
		return NULL;
	case ExpIsTree:
		expr = dummySymbolUsedInExp(exp->exp.tree.left, mdp, lev); 
		if (expr) return expr;
		return dummySymbolUsedInExp(exp->exp.tree.right, mdp, lev);
	default:
		return NULL;
	}
	return NULL;
}

int removeDummyInstructions(void)
{
	area *ap;
	wData *wdp, *wpre;
	expression *expr;
	int removeCount=0;
	for (ap = topMap.romArea; ap; ap = ap->next)
	{
		wpre = NULL;
		for (wdp = ap->wDatas; wdp; wdp = wdp->next)
		{
			//char show1[256];
			if (wpre == NULL || !wdp->wdIsInstruction)
			{
				wpre = wdp;
				continue;
			}
			

			if ((expr=dummySymbolUsedInExp(wdp->exp, wdp->m, 0))!=NULL)
			{
				// it should have call in the following
				wData *wdCall=NULL;
				int callStyle = -1;
				funcRecord *fp1 = NULL;
//#ifdef _DEBUG
				char show[1024];
				exprDump(wdp->exp, show, 1024);
//#endif
				if (wdp->fp)
				{
					for (wdCall = wdp; wdCall; wdCall = wdCall->next)
					{
						char show2[1024];
						exprDump(wdCall->exp, show2, 1024);
						if ((callStyle = (isWdCALL(wdCall)|isWdJump(wdCall))) != 0)
							break;
					}
					fp1 = (wdCall==NULL)?NULL:((callStyle == 1) ? getFuncByExp(wdp->fp, wdCall->exp) :
						getFuncByExp(wdp->fp, wdCall->next->next->exp));
				}
				if (fp1 && fp1->calledByFPTR)
				{
					//wdp->exp = newConstExp(0x0d, wdp->exp->lineno); // change to PRINC2
					expr->type = ExpIsNumber; // just change it
					expr->exp.number = 0x0d;
				}
				else
				{
					//wData *wdp1;
					
					if (wdp->instHighByte && (getExpMinValue(wdp->exp) & 0xf0) == 0xf0 ) // dual word!!
					{
						// only MVFF
						
						wData *pre2 = findPrevWdp(wpre);
						if (pre2->wdIsInstruction)
						{
							wData *pre3 = findPrevWdp(pre2);

							if ((getExpMinValue(pre2->exp) & 0xf0) == 0xd0 || getExpMinValue(pre2->exp)==0xc6) // mvsf+ss
								wpre = pre3; // it is MVFF??
						}
					}
					fprintf(stderr, "No-use code for %s at %s:%d removed when linking.\n", show,wdp->m?wdp->m->srcName:"unknown module", wdp->lineno);

					if (wdp->instHighByte)
					{
						wpre->next = wdp->next->next; // skip it!!
						wdp->next->prev = wdp; // dual dir
						//if (wdp->next->next)
							//wdp->next->next->prev = wpre;
					}
					else
					{
						if(wpre->instHighByte) // upper should be low
							wpre = findPrevWdp(wpre);
						wpre->next = wdp->next;
						//wpre->next->prev = wpre; // 
						if (wdp->next)
							wdp->next->prev = wpre; // dual link here
					}
					++removeCount;

					// it is possible we use MVXX


					// following call need a mark
					//for (wdp1 = wdp; wdp1; wdp1 = wdp1->next)
					//{
					//	if (isWdCALL(wdp1))
					//	{
					//		wdp1->dummyPushFixed = 1;
					//		break;
					//	}
					//}
				}
				continue;
			}
			if (wdp->fp && !keep_unused_function && wdp->fp->noAllocWarned)
			{
				wpre->next = wdp->next;
				if(wdp->next)
					wdp->next->prev = wpre; // dual link here
				++removeCount;
				continue;
			}
			wpre = wdp;
		}
		
	}
	return removeCount;
}

// check if segments out of range
int find_tgtid(void)
{
	moduleLD *mdp;
	char partName[256];
	
	if (sw_tgt_id)
		return sw_tgt_id; // already found;
	mdp = topMap.linkedModules; // only first module!!
	{
		int i;
		int val;
		symbol *sp;
		int middlechar;

		for (i = 0; i < mdp->localSymNum; i++) // first
		{
			sp = mdp->localSym[i];
			if (sp->symIsEQU && (val = getExpValue(sp->exp, mdp, 0, -10000, 0,0,NULL,NULL)) != DUMMY_CONST)
			{
				if (!strncmp(sp->name, "__SWTGTID", 10))
				{
					sw_tgt_id = val;
					break;
				}
			}
		}
		
		if (sw_tgt_id != 0)
		{
			// add a linker generated sym
			symbol *s = newSymbol("__SWTGTID");


			// special case, 41xx
			if ((sw_tgt_id / 100) == 41 || (sw_tgt_id / 100) == 42)
			{
				sprintf(partName, "HY%d", sw_tgt_id);
			}
			else 
			
			{

				if (sw_tgt_id == 11114)
					sw_tgt_id = 11014;
				if (sw_tgt_id < 100000u)
				{

					middlechar = (sw_tgt_id % 1000) / 100;
					sprintf(partName, "HY%d%s%d", sw_tgt_id / 1000, middlechar ? "S" : "P", sw_tgt_id % 100);
				}
				else // hy11p4104m
					if (sw_tgt_id > 1000000000u)
					{
						partName[0] = 0;
					}
					else if (sw_tgt_id > 100000000u)
					{
						int tailCharId = sw_tgt_id % 100;
						middlechar = (sw_tgt_id % 10000000) / 1000000;
						char tailChar = (tailCharId > 9) ? ('A' + (tailCharId - 10)) : ('0' + tailCharId);
						sprintf(partName, "HY%d%s%d%c",
							sw_tgt_id / 10000000, middlechar ? "S" : "P", (sw_tgt_id % 1000000) / 100, tailChar);
					}
				// 11P414M 11041426
					else if (sw_tgt_id > 10000000u)
					{
						int tailCharId = sw_tgt_id % 100;
						middlechar = (sw_tgt_id % 1000000) / 100000;
						char tailChar = (tailCharId > 9) ? ('A' + (tailCharId - 10)) : ('0' + tailCharId);
						sprintf(partName, "HY%d%s%d%c",
							sw_tgt_id / 1000000, middlechar ? "S" : "P", (sw_tgt_id % 100000) / 100, tailChar);
					}
					else
					{
						fprintf(stderr, "Warning, unknown target id %d\n", sw_tgt_id);
						partName[0] = '\0';
						sw_tgt_id = 0;
					}
			}
			
			if (sw_tgt_id==0 || !init_hya(partName,sw_tgt_id))
			{
				if(sw_tgt_id!=0 )
				fprintf(stderr, "part %s not list in support devices, memory range ignored.\n", partName);

			}


			s->ldm = topMap.linkedModules;
			s->symIsEQU = 1;
			s->exp = newConstExp(sw_tgt_id, -1);
			topMap.linkerGeneratedSym[topMap.generatedSymNum++] = s;
		}

	}
	return sw_tgt_id;
}
extern int xStartAddr;
int checkSegRange(void)
{
//	moduleLD *mdp;
//	symbol *sp;
	area *ap;
	// get ID first
	int maxaddr;
	int maxxaddr;
	int maxiram = 0;
	int maxxram = 0;
	int maxrom = 0;
	int pcstklevel = 6;
	int outOfRange = 0;
	int stackuse = 0;
	int usedRAM=0;
	int bodyRAM=1;
	
	
	find_tgtid();
	
	
	//sw
	if (sw_tgt_id == 0)
	{
		fprintf(stderr, "Warning, target seems not specified, memory/stack range not checked.\n");
		return 0;
	}
	bodyRAM = HY08A_getPART()->dataMemSize;
	maxiram = HY08A_getPART()->defMaxRAMaddrs + 1;
	maxrom = HY08A_getPART()->programMemSize*2; // to byte
	pcstklevel = HY08A_getPART()->pcStack;
	if (pcstklevel < 6)
		pcstklevel = 6;

	if (maxiram > 512)
	{
		maxxram = maxiram;
		maxiram = 512-128; // 180~1ff is for sfr!!
	}

	if (ramModel != RAM_MODEL_NORMAL && maxxram!=0)
	{
		maxiram = maxxram;
	}
	//switch (sw_tgt_id)
	//{
	//case 11012: maxiram = 0x100; maxxram = 0; maxrom = 2048 * 2; break;
	//case 11013: maxiram = 0x180; maxxram = 0; maxrom = 4096 * 2; break;
	//case 11114:
	//case 11014: maxiram = 0x180; maxxram = 0x300 ; maxrom = 8192 * 2; break;

	//case 11023: maxiram = 0x180; maxxram = 0; maxrom = 4096 * 2; break;// 11p23 is 4k 256
	//case 11024: maxiram = 0x180; maxxram = 0x300; maxrom = 8192 * 2; break;
	//case 11032: maxiram = 0x100; maxxram = 0; maxrom = 2048 * 2; break;// 11p32 is 2k 128
	//case 11033: maxiram = 0x180; maxxram = 0; maxrom = 4096 * 2; break;// 11p33 is 4k 256
	//case 11035: maxiram = 0x180; maxxram = 0; maxrom = 6144 * 2; break;// 11p35 is 6k 256
	//case 11036: maxiram = 0x180; maxxram = 0; maxrom = 4096 * 2; break;// 11p36 is 4k 256
	//case 11041: maxiram = 0x100; maxxram = 0; maxrom = 2096 * 2; break;// 11p36 is 4k 256
	//case 15041: maxiram = 0x100; maxxram = 0; maxrom = 2048 * 2; break;// 11p36 is 4k 256
	//case 15141: maxiram = 0x100; maxxram = 0; maxrom = 2048 * 2; break;// 11p36 is 4k 256
	//case 11042: maxiram = 0x100; maxxram = 0; maxrom = 2096 * 2; break;// 11p36 is 4k 256
	//case 11052: maxiram = 0x100; maxxram = 0; maxrom = 2096 * 2; break;// 11p36 is 4k 256
	//case 11054: maxiram = 0x180; maxxram = 0; maxrom = 8192 * 2; break;// 11p36 is 4k 256

	//case 4163: maxiram = 0x180; maxxram = 0x1000; maxrom = 0x7800 * 2; pcstklevel = 10;  break;
	//default:
	//	fprintf(stderr, "Warning, unknow software target id %d, memory range not checked.\n", sw_tgt_id);
	//	maxiram = 0x180; maxxram = 0x1000; maxrom = 0x100000; break;
	//}
	if (mainThreadCallLevel + interruptThreadCallLevel > pcstklevel)
	{
		fprintf(stderr, "Warning, main thread has call level %d, \ninterrupt has call level %d, \n\
total %d is greater than allowed PC stack level %d.\n", mainThreadCallLevel, interruptThreadCallLevel,
mainThreadCallLevel + interruptThreadCallLevel , pcstklevel);
	}
	maxaddr = 0;
	for (ap = topMap.romArea; ap; ap = ap->next)
	{
		moduleLD *mdp = NULL;
//		wData *wdp;
		if (ap->endAddr >= maxrom)
		{
			fprintf(stderr, "area %s ROM max address 0x%X is out of range 0x%X\n",
				ap->name, ap->endAddr, maxrom);
			outOfRange = 1;
		}
		if (ap->endAddr > maxaddr)
			maxaddr = ap->endAddr;
	}
	if (!outOfRange)
	{
		printf("ROM max address is 0x%X (byte), or size is %d (decimal) words. \nROM usage is around %d%%. \n", maxaddr, (maxaddr+1)/2, maxaddr *100/maxrom);
		topMap.romInfo.startA = 0;
		topMap.romInfo.endA = maxaddr;
		topMap.romInfo.size = maxaddr;
	}
	maxaddr = 0;
	for (ap = topMap.dataArea; ap; ap = ap->next)
	{
		if (ap->endAddr > maxiram)
		{
			fprintf(stderr, "area %s RAM max address 0x%X is out of range 0x%X\n",
				ap->name, ap->endAddr, maxiram);
			outOfRange = 1;
		}
		if (ap->endAddr > maxaddr)
			maxaddr = ap->endAddr;
	}

	for (ap = topMap.stkArea; ap; ap = ap->next)
	{
		
		if (ap->endAddr > maxiram && ap->endAddr<0x200)
		{
			fprintf(stderr, "Error !! Area %s RAM max address 0x%X is out of range 0x%X\n",
				ap->name, ap->endAddr, maxiram);
			outOfRange = 1;
		}
		if (ap->endAddr > maxaddr)
			maxaddr = ap->endAddr;
	}
	
	maxxaddr = -1;
	topMap.xdataInfo.startA = 0x200;
	topMap.xdataInfo.endA = -1;
	if (!maxxram && !sw_tgt_id)
		maxxram = 0x1000; // 4K RAM max
	for (ap = topMap.xdataArea; ap; ap = ap->next)
	{
		if (ap->endAddr > maxxram)
		{
			fprintf(stderr, "Error!! Area %s RAM2 (xdata) max address 0x%X is out of range 0x%X\n",
				ap->name, ap->endAddr, maxxram);
			outOfRange = 1;
		}
		if (ap->endAddr > maxxaddr)
			maxxaddr = ap->endAddr;
		if (ap->startAddr!=0 && ap->startAddr < topMap.xdataInfo.startA)
			topMap.xdataInfo.startA = ap->startAddr;
		if (ap->endAddr > topMap.xdataInfo.endA)
			topMap.xdataInfo.endA = ap->endAddr;
	}
	topMap.xdataInfo.size = topMap.xdataInfo.endA - topMap.xdataInfo.startA;
	if (!outOfRange && maxxaddr>0 )
	{
		printf("XDATA allocated max address is 0x%X.\n", maxxaddr);
		
	}
	if (!outOfRange)
	{
		if (stktop_sym)
		{
			stktopi = stktop_sym->exp->exp.number;
			if (maxxram == 0 && stktopi > maxiram || (maxxram != 0 && stktopi > maxxram))
			{
				fprintf(stderr, "Out-of-range error!! data area of stacks comes to 0x%X, greater than 0x%X\n", stktopi, (maxxram == 0) ? maxiram : maxxram);
				return 1;
			}

		}

		//printf("HEAP (DATA) area allocated max address is 0x%X.\n", maxaddr);
		if (HY08A_getPART()->isEnhancedCore == 4 || (ramModel == RAM_MODEL_FLATA))
		{
			stackuse = mainLocalStackSize + interruptLocalStackSize;
			topMap.cstackInfo.size = stackuse; // only stack use is enough
			if (maxaddr >= xStartAddr)
				usedRAM = maxaddr - xStartAddr; // heap
			else
				usedRAM = maxaddr - 0x80;
			if (maxaddr >= 0x200 && xStartAddr < 0x180)
				usedRAM -= 128; // sfr region!!
			topMap.heapInfo.startA = xStartAddr;
			topMap.heapInfo.endA = maxaddr;
			topMap.heapInfo.size = usedRAM;
			printf("Estimated C-STACK/HEAP sizes are  %d(0x%X)/%d(0x%X) bytes.\n", stackuse, stackuse, usedRAM, usedRAM);
			printf("RAM usage is around %d%%.\n", (usedRAM + stackuse) * 100 / bodyRAM);

			if (usedRAM + stackuse > bodyRAM)
				outOfRange = 1;
			else if((mainLocalStackSize + 0x80)>topMap.heapInfo.startA)
			{
				fprintf(stderr,"Servere Warning!! main thread stack overlap %d bytes with heap!!\n please adjust or use -xs to change heap start address!!\n",
				mainLocalStackSize + 0x80 - topMap.heapInfo.startA
				);
				fprintf(stderr,"Get heap start at 0x%X, and (main thread) stack end at 0x%X.\n", topMap.heapInfo.startA, (mainLocalStackSize + 0x80));
				//outOfRange = 1;
			}
			else if((stackuse+0x80) > topMap.heapInfo.startA)//overlap
			{
				fprintf(stderr,"Warning!!, possible main+interrupt stack/heap overlap %d bytes!!,\nplease make sure interrupt disabled at high level call or \nadjust or use -xs to change heap start address!!\n",
				stackuse + 0x80 - topMap.heapInfo.startA
				);
				fprintf(stderr,"Get heap start at 0x%X, and stack end at 0x%X.\n", topMap.heapInfo.startA, (stackuse + 0x80));
			}
			
		}
		else
		{
			int heapSize=0;
			usedRAM = dataMaxAddr - 0x80;
			if (localMaxAddr > dataMaxAddr)
				usedRAM = localMaxAddr - 0x80;
			if (bodyRAM > 0x100)
			{
				if ((xdataMaxAddr - xStartAddr) > 0)
				{
					if (xdataMaxAddr >= 0x200 && xStartAddr < 0x180)
					{
						printf("HEAP and STACK shared RAM address from 0x100~0x180,\nTotal RAM usage is around %d%%.\n",
							(usedRAM + xdataMaxAddr - xStartAddr - 0x80) * 100 / bodyRAM);
						heapSize = xdataMaxAddr - xStartAddr - 0x80;
						if (usedRAM + xdataMaxAddr - xStartAddr - 0x80 > bodyRAM)
							outOfRange = 1;
					}
					else
					{
						printf("Zero Page RAM usage is around %d%%,\nXDATA RAM usage is %d%%.\nTotal RAM usage is around %d%%.\n",
							usedRAM * 100 / 256, (xdataMaxAddr - xStartAddr) * 100 / (bodyRAM - 256), (usedRAM + xdataMaxAddr - xStartAddr) * 100 / bodyRAM);
						heapSize = xdataMaxAddr - xStartAddr;
						if ( usedRAM>256  || (xdataMaxAddr - xStartAddr)> (bodyRAM - 256) || (usedRAM + xdataMaxAddr - xStartAddr) > bodyRAM)
							outOfRange = 1;
					}
					
				}
				else
				{
					printf("Zero Page RAM usage is around %d%%.\nXDATA RAM not used. Total RAM usage is around %d%%.\n",
						usedRAM * 100 / 256, (usedRAM + xdataMaxAddr - xStartAddr) * 100 / bodyRAM);
					if (usedRAM>256 || (usedRAM + xdataMaxAddr - xStartAddr) > bodyRAM)
						outOfRange = 1;
				}
				topMap.heapInfo.startA = xStartAddr;
				topMap.heapInfo.endA = xdataMaxAddr;
				topMap.heapInfo.size = heapSize;
			}
			else
			{
				printf("RAM usage is %d%%.\n", usedRAM * 100 / bodyRAM);
				topMap.heapInfo.startA = 0x80;
				topMap.heapInfo.endA = usedRAM + 0x80;
				topMap.heapInfo.size = usedRAM;
				if (usedRAM > bodyRAM)
					outOfRange = 1;
			}
		}
	}
	if (outOfRange)
	{
		fprintf(stderr, "MEMORY OUT OF RANGE!!\n");
	}
	

	return outOfRange;
}


static inline int body_has_sfr2_stack(void)
{
	if (sw_tgt_id == 0)
		find_tgtid();
	if (HY08A_getPART() != NULL)
	{
		//if (sw_tgt_id == 15041 || sw_tgt_id == 15141) // support 15p41 first
		if (HY08A_getPART()->isEnhancedCore == 4)
			return 2; // we have H08D

		if(HY08A_getPART()->isEnhancedCore==3)
			return 1;
	}
	return 0;
}
// segments should not have label, etc
static wData *segStart[32768];
static wData *segEnd[32768];
static int segLen[32768];
int segnum = 0;

static int codeNeedNext(int opcode)// see if this opcode need next word
{
	int oph = opcode >> 8;
	if ((opcode & 0xfffc) == 0x000c) // LDPR
		return 1;
	if (oph == 0x09 ||  // MVLP
		oph == 0xc0 || oph==0xc6 || // LCALL, mvsf, mvss
		oph == 0xc2 || (oph&0xf0)==0xd0 ) // LJMP && mvff
		return 1;
	if ((oph & 0xf0) == 0xb0 || (oph & 0xf0) == 0xa0 || (oph & 0xf0)==0xd0 ) // mvff bit skip, skip must include following 1 word or 2 word
		return 1; 
	if ((oph & 0xf8) == 0x68) // TFSZ CPSE..
		return 1;
	return 0;
}
static int codeIsBranch(int opcode)
{
	// call/jmp is branch 
	int oph = opcode >> 8;
	if (opcode == 0x0008 || opcode == 0x000a || oph==0x05 || (oph==0xc4 && ((opcode&0xc0)==0xc0)) ) // 05 i RETL
		return 4; // RET && RETI, is branch
	if ((oph & 0xf8) == 0xc8 || (oph & 0xf8) == 0x78) // RJ && RCALL
		return 1;
	if (oph == 0xc0 || // LCALL
		oph == 0xc2) // LJMP)
		return 2;
	if ((oph & 0xf8) == 0x70) // JXX
		return 3;
	return 0;
}
int cmpInROM(int a1, int a2, int len) // check if these 2 segment are equal, 0 means equal
{
	return memcmp(rom_data + (a1 ), rom_data + (a2 ), len << 1);
}
#define CPWDINFO(A,B) A->m=B->m;A->fp=B->fp; A->lineno=B->lineno; A->locTree=B->locTree;A->wdIsInstruction=1
static void insertCall(wData *prev, wData *next, char *label)
{
	int lineno = prev->lineno;
	prev->next = newWDATA1(prev->ap);
	prev->next->prev = prev;
	CPWDINFO(prev->next, prev);
	
	prev->next->instHighByte = 1;
	prev->next->exp = newConstExp(0xC0, prev->lineno);

	prev->next->next = newWDATA1(prev->ap);
	prev->next->next->prev = prev->next;
	CPWDINFO(prev->next->next, prev);
	prev->next->next->exp = newTreeExp(newTreeExp(newSymbolExp(label, lineno), newConstExp(13, lineno), 'A', lineno),
		newConstExp(255, lineno), '&', lineno);
	prev->next->next->exp->exp.tree.OverflowChk = 1;
	prev->next->next->exp->exp.tree.OverflowChkUnsign = 1;

	prev->next->next->next = newWDATA1(prev->ap);
	prev->next->next->next->prev = prev->next->next;
	CPWDINFO(prev->next->next->next, prev);
	prev->next->next->next->instHighByte = 1;
	prev->next->next->next->exp = newTreeExp(newTreeExp(newTreeExp(newSymbolExp(label, lineno), newConstExp(9, lineno), 'A', lineno),
		newConstExp(0x0f, lineno), '&', lineno), newConstExp(0xf0, lineno), '|', lineno);

	prev->next->next->next->next = newWDATA1(prev->ap);
	prev->next->next->next->next->prev = prev->next->next->next;
	CPWDINFO(prev->next->next->next->next, prev);
	prev->next->next->next->next->exp = newTreeExp(newTreeExp(newSymbolExp(label, lineno), newConstExp(1, lineno), 'A', lineno),
		newConstExp(0xff, lineno), '&', lineno);
	prev->next->next->next->next->next = next;
	next->prev = prev->next->next->next->next;
}

static void appendRET(wData *prev, wData *next) // this is easier 
{
	prev->next = newWDATA1(prev->ap);
	prev->next->prev = prev;
	CPWDINFO(prev->next, prev);
	prev->next->exp = newConstExp(0x00, prev->lineno);
	prev->next->instHighByte = 1;
	prev->next->next = newWDATA1(prev->ap);
	prev->next->next->prev = prev->next->next;
	CPWDINFO(prev->next->next, prev);
	prev->next->next->exp = newConstExp(0x0A, prev->lineno);
	prev->next->next->next = next;

}
#ifdef DBGCOMM
void dumpSeg(int id, wData *startW, wData *endW)
{
	char buf[100];
	while (startW && startW != endW)
	{
		if (startW->exp == NULL)
			fprintf(stderr, "err, exp NULL\n");
		printf("seg %d offset %X exp %s\n", id, startW->areaOffset, exprDump(startW->exp, buf, 99));
		startW = startW->next;
	}
}
#endif
int commonCodeAna(int minWNum)
{
		// construct graphs to see what codes can be optimized
	wData *wdp,*wdPre;
	//symbol *trueSym;
	area *ap;
	int opcode;
	int i,j,k,m;
	int is, js; // different shift
	int skipmark = 0;
	int use4 = 0;
	int possibleSave = 0;
	int firstMatch;
	unsigned char matched[32768];
	int matchedOffset[32768];
	int matchedId[32768];
	int matchedWn[32768];
	int segid;
	int br;
	int needNext=0;
	int processid = 0; // no processsed
	funcRecord *funcp=NULL;
	if (!minWNum)
		return 0;
	if (minWNum < 5)
		minWNum = 5;
	memset(matched, 0, sizeof(matched));
	memset(matchedOffset, 0, sizeof(matchedOffset));
	memset(matchedId, 0, sizeof(matchedId));
	memset(matchedWn, 0, sizeof(matchedWn));
	segid = 1;
	for (ap = topMap.romArea; ap; ap = ap->next)
	{
		segStart[segnum] = NULL;
		segEnd[segnum] = NULL;
		// 2019 skip common code of interrupt insersion

		if (strstr(ap->name, "c_interrupt"))
			continue; // no common code for interrupt
		if (strstr(ap->name, "HEADFUNC"))
			continue;

		//wdPre = NULL; // cross border is null
		
		for (wdp = ap->wDatas,wdPre=NULL; wdp; wdPre=wdp,wdp = wdp->next)
		{
			//if (wdp->lineno == 88)
				//fprintf(stderr, "87\n");
			wdp->prev = wdPre; // give wdPre!!
			if (wdp->fp && (wdp->fp->highestCallLevel >= commCallLevLimit )) // re-entry cannot optimize!! because of level!!
				continue;
			
			if (wdp->wdIsLabel || wdp->hasRomLabel || (wdp->next && wdp->next->hasRomLabel))
			{
				if (segStart[segnum])
				{
					segnum++;
					segStart[segnum] = NULL;
					segEnd[segnum] = NULL;
				}
			}
			if (!wdp->instHighByte || !wdp->wdIsInstruction || wdp->wdIsMacroMark || wdp->wdIsMacroMarkEnd)
				continue;
			// depends on the byte to see if it can be a segment of common code
			// some code cannot replace .. we don't replace skip and branch ones
			
			opcode = (wdp->wValue << 8) | (wdp->next->wValue);
			br = codeIsBranch(opcode);
			needNext = codeNeedNext(opcode);
			if (skipmark > 1)
			{
				skipmark--;
				continue;
			}
			if (skipmark==1 && !wdp->instSkip && !needNext)
			{
				skipmark--;
				continue;
			}
			if (codeNeedNext(opcode) || wdp->instSkip || br || wdp->noCommonCode ) // consider simple case, no reu
			{
				if (segStart[segnum])
				{
					segnum++;
					segStart[segnum] = NULL;
					segEnd[segnum] = NULL;
				}
				if (wdp->instSkip || br==2 || needNext)
					skipmark = 1;
				if (opcode == 0xc801)// fptr
					skipmark = 9;
				continue;
			}
			else
			{
				if (segStart[segnum])
				{
					segEnd[segnum] = wdp;
				}
				else if((opcode&0xf000)!=0xf000)
				{
					segStart[segnum] = wdp;
					segEnd[segnum] = wdp;
				}
			}
			
		}
		if (segStart[segnum])
		{
			segnum++;
			segStart[segnum] = NULL;
			segEnd[segnum] = NULL;
		}
	}
	for (i = 0; i < segnum; i++)
	{
		segLen[i] = (segEnd[i]->areaOffset - segStart[i]->areaOffset + 2) / 2;
#ifdef DBGCOMM
		printf("seg %d offset start %04X end %04X length %d (words)\n", i, segStart[i]->areaOffset/2, segEnd[i]->areaOffset/2,
			segLen[i]);
#endif
	}
	// we check how many 4word packet can be re-use
	// see head to head
	for (m = 20; m>=minWNum; m--)
	{
		k = 0;
		//memset(matched, 0, sizeof(matched));
		for (i = 0; i < segnum; i++)
		{
#ifdef DBGCOMM
			
			if (i == 75 && m<=7)
			{
				printf("now m=%d matched75=%d\n", m,matched[75]);
				dumpSeg(i, segStart[i], segEnd[i]);
			}
#endif
			if (segLen[i] < m || matched[i]) // already matched
				continue;

			for (is = 0; is <= segLen[i] - m; is++) // consider shift
			{
				firstMatch = 1;
				for (j = i + 1; j < segnum; j++)
				{
					if (segLen[j] < m || matched[j])
						continue;
					for (js = 0; js <= segLen[j] - m; js++)
					{
						if (!cmpInROM(segStart[i]->areaOffset + is * 2, segStart[j]->areaOffset+js*2, m))
						{
							matched[i] = 1;
							matched[j] = 2;
							matchedOffset[i] = is;
							matchedOffset[j] = js;
							matchedId[i] = segid;
							matchedId[j] = segid;
							matchedWn[i] = m;
							matchedWn[j] = m;
#ifdef DBGCOMM
							printf("%04X+%d-%04X+%d match-%d\n", segStart[i]->areaOffset / 2, is,
																segStart[j]->areaOffset / 2, js, m);
#endif
							k++;
							if (firstMatch)
							{
								possibleSave += (m - 5);//first lcall+returnret
								firstMatch = 0;
							}
							else
								possibleSave += (m - 2); // only lcall
							break;
						}
						
					}

				}
				if(matched[i])
					break;
			}
			if (matched[i])
				segid++;

		}
		
		if(k && verbose)
			printf("matched %d word count is %d\n",m, k);
		if (segid == processid+1)
			continue;
		for (i = 0; i < segnum; i++)
		{
			area *ap;
			wData *wdph, *wdpt, *wLabel; // head and tail
			char buff[100];
#ifdef DBGCOMM
			if (matched[i])
			{
				if(strstr(segStart[i]->fp, "ISR"))
					printf("match seg-%d type is %d, match-id %d, now processid is %d \n", i, matched[i], matchedId[i], processid);
			}
#endif
			if (matched[i] && matchedId[i]>processid)
			{
				// copy a new area

				sprintf(buff, "__COMMON_%d_", matchedId[i]);
#ifdef DBGCOMM
				printf("matched id is %d\n", matchedId[i]);
#endif
				ap = newLDArea(buff, 0, 0, regionCODE);
				if (matched[i] == 1)
					appendAreaToTopMap(ap); // append first only
				wdpt = NULL;
				for (j = 0, ap->wDatas = segStart[i]; j < matchedOffset[i]; j++)
				{
					ap->wDatas = ap->wDatas->next->next;
					//ap->wDataTail = ap->wDatas;
				}
				for (j = 0, ap->wDataTail = ap->wDatas; j < matchedWn[i]; j++)
				{
#ifdef DBGCOMM
					char expbuf[128];
					printf("address %X exp %s\n", ap->wDataTail->areaOffset, exprDump(ap->wDataTail->exp, expbuf, 127));
					printf("address %X exp %s\n", ap->wDataTail->next->areaOffset, exprDump(ap->wDataTail->next->exp, expbuf, 127));
#endif
					if (ap->wDataTail->sizeOrORG == 0 || ap->wDataTail->next->sizeOrORG == 0)
					{
						if(!(ap->wDataTail->wdIsMacroMark || ap->wDataTail->wdIsMacroMarkEnd))
							fprintf(stderr, "debug err seg.\n");
					}
					ap->wDataTail->ap = ap;
					ap->wDataTail->next->ap = ap;// change its area, though function keep.
					//ap->wDataTail->next->next->prev = ap->wDataTail->next;
					wdpt = ap->wDataTail->next;
					ap->wDataTail = ap->wDataTail->next->next;

				}
				// if (ap->wDataTail && ap->wDataTail->prev == NULL)
				// {
				// 	fprintf(stderr, "strange tail ?\n");
				// }

				// now cut
				wdph = ap->wDatas->prev;
				// if (wdph->areaOffset != ap->wDatas->areaOffset - 1 && !wdph->wdIsLabel && wdph->sizeOrORG!=0) // some times macromark
				// {
					
				// 	fprintf(stderr, "strange prev offset\n");
				// }

				


				
				insertCall(wdph, ap->wDataTail, buff);
				if(wdpt)
					appendRET(wdpt, NULL);
				else
				{
					fprintf(stderr, "internal error WDPT at %s:%d\n", __FILE__, __LINE__);
					exit(-__LINE__);
				}
				if (matched[i] == 1) // once is ok
				{
					// 2017 Dec, create function
					

					int ii;
					wLabel = newWDATA(ap);
					wLabel->fp = ap->wDatas->fp;
					wLabel->m = ap->wDatas->m;
					wLabel->locTree = ap->wDatas->locTree;
					wLabel->lineno = ap->wDatas->lineno;
					wLabel->wdIsLabel = 1;
					wLabel->sym = newSymbol(buff);


					wdph->m->localSym[wdph->m->localSymNum++] = wLabel->sym; // add in local
					if (wdph->m->localSymNum >= 32768)
					{
						fprintf(stderr, "Error, module %s too many local symbols.\n", wdph->m->name);
						exit(-__LINE__);
					}

					wLabel->sym->ldm = wdph->m;
					wLabel->sym->symIsLabel = 1; // if it is export, we change it, also
					wLabel->sym->labelPos = wLabel;
					wLabel->sym->ap = ap; // this is required!!
					wLabel->sym->symIsGlobl = 1;
					//wLabel->sym->areaOffset = absAddr;
					//trueSym = wLabel;

					wdph->m->exportSym[wdph->m->exportSymNum++] = wLabel->sym; // export it!!

					wLabel->next = ap->wDatas;
					ap->wDatas->prev = wLabel;
					ap->wDatas = wLabel;

					funcp = newEmptyFuncRecord();
					funcp->areap = ap;
					funcp->firstWD = ap->wDatas;
					funcp->finalWD = wdpt->next->next; // second byte of RET
					strncpy(funcp->name, buff, sizeof(funcp->name)-1);
					funcp->namehash = hash(buff);
					

					// see if included
					for (ii = 0; ii < wdph->m->funcNum; ii++)
						if (wdph->m->funcs[ii] == funcp)
							break;
					if (ii >= wdph->m->funcNum)
					{
						wdph->m->funcs[wdph->m->funcNum++] = funcp; // add in that module
						wdph->fp->calledFunc[wdph->fp->calledFuncNum++] = funcp;
					}
				}
				else
				{
					int ii;
					if (funcp == NULL)
					{
						fprintf(stderr, "internal error funcp of common at %s:%d\n", __FILE__, __LINE__);
						exit(-__LINE__);
					}
					for (ii = 0; ii < wdph->m->funcNum; ii++)
						if (wdph->m->funcs[ii] == funcp)
							break;
					if (ii >= wdph->m->funcNum && wdph->fp)
						wdph->fp->calledFunc[wdph->fp->calledFuncNum++] = funcp; // add a called entry?
					if (!findExportSymInMld(hash(buff), wdph->m))
					{
						wdph->m->refSym[wdph->m->refSymNum] = newSymbol(buff);
						wdph->m->refSym[wdph->m->refSymNum]->ldm = wdph->m; // this is cool?
						wdph->m->refSym[wdph->m->refSymNum]->symIsExt = 1;
						wdph->m->refSym[wdph->m->refSymNum]->definedPos = findExportSym(hash(buff), NULL);
						wdph->m->refSymNum++;
					}
				}

			}
		}
		processid = segid-1;//time by time
	}
	// cut and paste
	if (segid == 1 && verbose)
	{
		printf("no common code match found.\n");
		return 0;
	}
	

	
	//printf("possible save  %d words\n", possibleSave);
	return possibleSave;
}

// here we rearrange the code segments that inter-CALL 
// the call should be function 

// this is get distance
// RJ RCALL can go to distance of 7ff , +3ff -> - 400, preincrease mans +400 -> - 3ff
static int getLongCallNum(int *seq, int *fsize, int *callfromf, int *callfromoff, int *calltof, int fnum, int callnum)
{
	int accuoff[1024]; // accumulated offset of all functions
	//int sizedown[1024];
	memset(accuoff,0,sizeof(accuoff));

	int i,j,dist=0;
	for (i = 0, j = 0; i < fnum; i++)
	{
#ifdef _DEBUG
		if (seq[i] >= 1024 || seq[i]<0)
		{
			fprintf(stderr, "internal error, fail at %s:%d\n", __FILE__, __LINE__);
			exit(-__LINE__);
		}
#endif
		accuoff[seq[i]] = j;
		j += fsize[seq[i]];
	}
	for (i = 0; i < callnum; i++)
	{
		// distance is j
		//memset(sizedown, 0, sizeof(sizedown));
#ifdef _DEBUG
		if (calltof[i] < 0 || callfromf[i]<0 || callfromoff[i]<0)
		{
			fprintf(stderr, "error check\n");
		}
#endif
		
		j = accuoff[calltof[i]] - (accuoff[callfromf[i]] + callfromoff[i]);
		if ((j > 0x400) || (j < -0x3ff) )// 3FF/-400 word offset after +1 ==> 400/3ff words
			dist++;
	}
	return dist;
}
// int mvssmvsf2mvff(void)
// {
// 	moduleLD *mld;
// 	funcRecord *f;
// 	int i;
// 	wData *wp;
// 	for(mld = topMap.linkedModules; mld; mld = mld->next)
// 	{
// 		for(i=0;i<mld->funcNum;i++)
// 		{
// 			for(wp=f->firstWD;wp&&wp!=f->finalWD;wp=wp->next)
// 			{
// 				if(!wp->instHighByte || wp->instSkip)
// 					continue;
// 				if(isWdMVSF0(wp))
// 			}
// 		}
// 	}
// }
void arrangeCodeSegments(int includeCommon)
{
	int totalFunctionNum = 0;
	//int functionBodySize[1024]; // first we calculate the size/function number
	int i,j;
	moduleLD *mld;
	funcRecord * farray[1024];
	int fSize[1024]; // 
	int seq[1024];
	int bestseq[1024];
	int minDist = 102400000;
	int dist;
	int callNum = 0;
	int callFromFuncID[40960];
	int callFromFuncOffset[40960]; 
	int callToFuncID[40960];
	int callOutNum[40960];
	char fLock[1024];
	int calledNum[40960]; // how many time each function is called directly
				// if not called, it can be called by fptr, put to final!!
				// but our algorithm should make a correct sequency by that
//	int callIONum[4096];
	int fnum=0;
	int k,m;
	int interruptFuncNum=0;
	int headFuncNum=0;
	wData *wdp,*wdp1;
	uint64 fnameHash;
//	funcRecord *fp1;
	area *ap;
	area *ap1;
	int lock1index=-1;
	int lock2index[1024];
	memset(fSize, 0, sizeof(fSize));
	memset(calledNum, 0xff, sizeof(calledNum));
	memset(callOutNum, 0xff, sizeof(callOutNum));

	// it should be 0 or ff?
	memset(callFromFuncID, 0xff, sizeof(callFromFuncID));
	memset(callFromFuncOffset, 0xff, sizeof(callFromFuncOffset));
	memset(callToFuncID, 0xff, sizeof(callToFuncID));
	



	memset(fLock, 0, sizeof(fLock));
	for (mld = topMap.linkedModules; mld; mld = mld->next)
	{
		for (i = 0; i < mld->funcNum; i++)
		{
			// we cut the function/start/end 
			// first loop we make sure both start/end exists
			if (mld->funcs[i]->areap && mld->funcs[i]->areap->nameHash==hashInt)//   !strcmp(mld->funcs[i]->areap->name, "c_interrupt"))
			{
				fLock[fnum] = 1; // lock it
				lock1index = fnum;
				interruptFuncNum++;
			}
			// 2022 comm group together
			if (mld->funcs[i]->areap && mld->funcs[i]->areap->nameHash==hashHEADFUNC)//  !strcmp(mld->funcs[i]->areap->name, "HEADFUNC"))
			{
				fLock[fnum] = 2;// 2 means after interrupt
				//lock2index[headFuncNum]=fnum;
				//headFuncNum++;
			}
			
			if (mld->funcs[i]->finalWD && mld->funcs[i]->firstWD)
				farray[fnum++] = mld->funcs[i];
		}
	}
	if(interruptFuncNum>1)
	{
		fprintf(stderr,"Error, interrupt function number > 1, only 1 interrupt allowed.\n");
		for(i=0;i<fnum;i++)
		{
			if(fLock[i]==1)
				fprintf(stderr,"%s:%s, interrupt\n", farray[i]->m->name, farray[i]->name);
		}
		exit(-__LINE__);
	}
	// switch lock functions if needed
	
	if(interruptFuncNum!=0 && lock1index!=0)
	{
		funcRecord *tfp;
		int tmp;
		tfp = farray[lock1index];
		farray[lock1index]=farray[0];
		farray[0]=tfp;
		tmp = fLock[0];
		fLock[0]=1;
		fLock[lock1index]=tmp; // lock1index need check if conflict lock2
	}
	for(i=0;i<fnum;i++)
		if(fLock[i]==2)
		{
			lock2index[headFuncNum++]=i;
		}
	for(i=0;i<headFuncNum;i++)
	{
		j=interruptFuncNum+i; // the offset to check
		if(fLock[j]!=2)
		{
			funcRecord *tfp;
			int tmp;
			tfp = farray[lock2index[i]];
			farray[lock2index[i]]=farray[j];
			farray[j]=tfp;
			tmp = fLock[j];
			fLock[j]=2;
			fLock[lock2index[i]]=tmp;
		}
	}
	
	for (i = 0; i < fnum; i++)
	{
		// count by word
		wData *finchk = (farray[i]->finalWD && farray[i]->finalWD->instHighByte) ? (farray[i]->finalWD->next) : farray[i]->finalWD;
		j = 0;
		for (wdp = farray[i]->firstWD; wdp && (wdp != finchk || (
			wdp->fp && wdp->fp!=farray[i]))
			; wdp = wdp->next)
		{
			if(wdp->instHighByte) // count words
			{
				
				if (wdp->exp->type == ExpIsNumber && wdp->exp->exp.number == 0xc0) // call not optimized yet
				{
					if (!(wdp->next->exp->type == ExpIsTree)  )
						continue;
					fnameHash = wdp->next->exp->exp.tree.left->exp.tree.left->symHash;
					for (k = 0; k < fnum; k++)
						if (farray[k]->namehash == fnameHash)
							break;
					if (k < fnum)// sometime call is 
					{
						callToFuncID[callNum] = k;
						calledNum[k]++;
						callOutNum[i]++;
						callFromFuncID[callNum] = i;
						callFromFuncOffset[callNum++] = j;
					}
				}
				j++;
			}
			
		}
		fSize[i] = j ; // words!!
	}
	// then there is math problem, so many inter-call, how to decide the best allocating seq?
	// we can not try all combinations
	// because 20! = 2.4e18 , which is not possible to be solved in such short time!!
	// and it is not meaningful
	 memset((void*)seq, 0xff, sizeof(seq));
	 memset((void*)bestseq, 0xff, sizeof(bestseq));


	// alternate method is insert each function to different locations
	 // that  f0 f1 f2 f3 ->0123  1023 1203 1230 // see which is best
	 // assume 1023 is ok, then try number 1 => 1023, 0123, 0213, 0231 // see which is best
	 // if distance is already 0, we quit
	 for (i = 0; i < fnum; i++)
		 seq[i] = i;
	 // switch flock
	 // first is 1
	 //if(fLock[0]!=1 )
	 memcpy(bestseq, seq, fnum * sizeof(int));
	 minDist= getLongCallNum(seq, fSize, callFromFuncID, callFromFuncOffset, callToFuncID, fnum, callNum);
	 
	 for (m = 0; m < 2; m++)// usually 2 times are enough
	 {
		 for (i = 0; i < fnum; i++) // i is which func we change its order
		 {
			 if (fLock[i])
				 continue;
			 for (j = 0; j < fnum; j++)// insert func[i] to which one
			 {
				 //....new seq
				 int seqtemp[1024];
				 int *intp = seq;
				 if (fLock[seq[j]])
					 continue;

				 for (k = 0; k < fnum; k++)
				 {
					 if (k == j)
						 seqtemp[k] = i;
					 else
					 {
						 seqtemp[k] = *intp++;
						 if (seqtemp[k] == i)// skip that number
							 seqtemp[k] = *intp++;
					 }
				 }
				 memcpy(seq, seqtemp, fnum * sizeof(int));
				 // now seq is found
				 dist = getLongCallNum(seq, fSize, callFromFuncID, callFromFuncOffset, callToFuncID, fnum, callNum);
				 if (dist < minDist)
				 {
					 minDist = dist;
					 memcpy(bestseq, seq, fnum * sizeof(int));
				 }
				 if (minDist == 0)
					 break;

			 }
			 memcpy(seq, bestseq, fnum * sizeof(int)); // after a loop we keep the best seq
			 if (minDist == 0)
				 break;
		 }
		 if (minDist == 0)
			 break;
	 }
	 if (verbose)
	 {
		 printf("get func order:\n");
		 for (i = 0; i < fnum; i++)
			 printf("..%s\n", farray[bestseq[i]]->name);
		 printf("\nlong call num is %d\n", minDist);
	 }
	 // now cut them 
	 // but first is to split no-func parts
	 // put no func part first
	 if(includeCommon)
		 ap1 = newLDArea("_ALL2", 0, 0, regionCODE);
	 else 
		ap1 = newLDArea("_ALL", 0, 0, regionCODE);

	 //extend start to previous label, and stop to data of the last

	 //for (ap = topMap.romArea; ap; ap = ap->next)
	 //{
		// for (wdp = ap->wDatas; wdp; wdp = wdp->next)
		// {
		//	 if (wdp->wdIsLabel && wdp->fp == NULL && wdp->next->fp != NULL)
		//	 {
		//		 wdp->fp = wdp->next->fp;
		//		 wdp->fp->finalWD1 = wdp;
		//	 }
		// }
	 //}
	 //for (i = 0; i < fnum; i++)
	 //{
		// for (wdp = farray[i]->finalWD,farray[i]->finalWD1=wdp; wdp; wdp = wdp->next)
		// {
		//	 if (wdp->m != farray[i]->m ||( wdp->fp != NULL && wdp->fp != farray[i]))
		//		 break;
		//	 if(wdp->fp==NULL)
		//		 wdp->fp = farray[i]; // extend
		//	 farray[i]->finalWD1 = wdp;
		//	
		// }
	 //}
	 
	 for (ap = topMap.romArea; ap; ap = ap->next)
	 {
		 //if (strcmp(ap->name, "CCODE") && //strcmp(ap->name,"HEADFUNC")) // only CODE but no function!!
		 if(ap->nameHash!=hashCCODE && ap->nameHash!=hashHEADFUNC)
			 continue;
		 for (wdp = ap->wDatas; wdp; wdp = wdp->next)
		 {
			
			 if (!wdp->fp)
			 {
				 wdp1 = calloc(1, sizeof(wData));
				 memcpy(wdp1, wdp, sizeof(wData));
				 wdp1->next = NULL;
				 wdp1->ap = ap1;
				 wdp1->areaOffset = ap1->offsetAddr;
				 appendTailWDATA(ap1, wdp1,(int)wdp1->instHighByte);
				 if (wdp1->wdIsLabel)
				 {
					 
					 wdp1->sym->ap = ap1;
					 wdp1->sym->labelPos = wdp1;
				 }
			 }
			 
		 }
		 //if (ap1->offsetAddr & 1)
		 //{
			// // insert dummy
			// wdp1 = newWDATA1(ap1);
			// wdp1->lineno = ap1->wDataTail->lineno;
			// wdp1->ap = ap1;
			// wdp1->areaOffset = ap1->offsetAddr;
			// wdp1->exp = newConstExp(0xff, ap1->wDataTail->lineno);
			// appendTailWDATA(ap1, wdp1, 0); // make it odd
			// 
		 //}
	 }
	// it should be even!!
		 //fprintf(stderr, "odd err\n");
	 uint64_t hashALL = hash("_ALL");
	 // replace only CODE
	 for (i = 0; i < fnum; i++)
	 {
		 wData *fstart = NULL;
		 wData *ffin = NULL;
		 //if (!strcmp(farray[bestseq[i]]->areap->name, "c_interrupt"))
		 if(farray[bestseq[i]]->areap->nameHash == hashInt)
			 continue;
		 for (ap = topMap.romArea; ap; ap = ap->next)
		 {
			 //if ((strcmp(ap->name, "CCODE")&& strcmp(ap->name,"HEADFUNC")) && !strstr(ap->name,"__COMMON_" )) // dual '_'
			 // second loop check ALL
			 if (includeCommon)
			 {
				 if (ap->nameHash != hashALL && !strstr(ap->name, "__COMMON_")) // second loop copy all common
					 continue;
			 }
			 else
			 {
				 if ((ap->nameHash != hashCCODE && ap->nameHash != hashHEADFUNC && !strstr(ap->name, "__COMMON_")) )
					 continue;
			 }
			 for (wdp = ap->wDatas; wdp; wdp = wdp->next)
			 {
						 
				 if (wdp->fp==farray[bestseq[i]])
				 {
					 wdp1 = calloc(1, sizeof(wData));
					 memcpy(wdp1, wdp, sizeof(wData));
					 wdp1->next = NULL;
					 wdp1->ap = ap1;
					 wdp1->areaOffset = ap1->offsetAddr;
					 if (wdp->instHighByte)
					 {
						 if(fstart==NULL)
							fstart = wdp1;
						 ffin = wdp1;
					 }
					 
					 appendTailWDATA(ap1, wdp1, (int)wdp1->instHighByte);
					 if (wdp1->wdIsLabel)
					 {
						 wdp1->sym->ap = ap1;
						 wdp1->sym->labelPos = wdp1;

					 }
				 }
				 
			 }
			
			 farray[bestseq[i]]->areap = ap1;
		 }
		 // sometimes they were justbe optimized
		 //if (fstart == NULL && !strstr(farray[bestseq[i]]->name,"__COMMON_"))
		//	 fprintf(stderr, "strange, cannot find function %s code seg.\n", farray[bestseq[i]]->name);
		 farray[bestseq[i]]->firstWD = fstart;
		 farray[bestseq[i]]->finalWD = ffin;
		 

	 }
	 
	 // replace CODE && COMMON
	 i = 0;
	 //uint64_t hashALL = hash("_ALL");
	 for (ap = topMap.romArea; ap && ap->next;)
	 {
		 //if (ap->next && (!strcmp(ap->next->name, "HEADFUNC") || !strcmp(ap->next->name, "CCODE") || strstr(ap->next->name,"__COMMON_" )))
		 if (ap->next && (ap->next->nameHash==hashHEADFUNC || ap->next->nameHash==hashCCODE || (includeCommon&&ap->next->nameHash==hashALL) || strstr(ap->next->name, "__COMMON_")))
		 {
			 
			 if (i == 0)
			 {
				 ap1->next = ap->next->next;				 
				 ap->next = ap1;
				 ap = ap1;
				 i = 1;
			 }
			 else
			 {
				 ap->next = ap->next->next;// skip that area!!
			 }
			
		 }
		 else
			 ap = ap->next;
	 }
	 //solveRefSymbols(1);
}
macrot *macroByLine(char* line)
{
	macrot *m = (macrot*)calloc(1,sizeof(macrot));
	char buf[4096];
	char *sub[1024];
	char *para[100];
	int sepnum = 0;
	int i, len, len0,j;
	memset(m, 0, sizeof(macrot));
	strncpy(buf, line, 4095);
	//str2lower(buf); // lowercase!!
	len = strlen(buf);
	for(i=0;i<len;i++)
		if (buf[i] == '%')
		{
			buf[i] = '\0';
			sub[sepnum++] = &buf[i + 1];
		}
	if (!STRCASECMP(buf, "$MACDEF"))
	{
		dataItem *dp = NULL;
		m->isRPT = 0;
		len0 = strlen(sub[0]);
		// 2 pass
		for(i=0,j=0;i<len0;i++)
			if (sub[0][i] == ':')
			{
				sub[0][i] = '\0';
				
				para[j] = sub[0] + i + 1;
				
				j++;
			}
		//j--;
		for (i = 0; i < j; i++)
		{
			if (i == 0)
			{
				m->paras = dp = newEmptyDataItem();
			}
			else
			{
				dp->next = newEmptyDataItem();
				dp = dp->next;
			}
			dp->exp = newSymbolExp(para[i], -1);
			
		}
	}
	else
		m->isRPT = 1;

	
	m->macroName = strdup(sub[0]); // first field is always name!!
	m->nameHash = hash(m->macroName);

	if (m->isRPT)
	{
		m->macroRootLoc = strdup(sub[1]);
		m->paras = newEmptyDataItem();
		m->paras->exp = newConstExp(atoi(sub[2]),-1);
		for (i = 3; i < sepnum - 1; i++)
		{
			srcLine *sp = newSrcLine(sub[i], -1);
			appendSrcLine2(&(m->lines), &(m->linestail), sp);
		}
	}
	else
	{
		for (i = 1; i < sepnum - 1; i++)
		{
			srcLine *sp = newSrcLine(sub[i], -1);
			appendSrcLine2(&(m->lines), &(m->linestail), sp);
		}
	}
	
	return m;
}


static void appendWdp2Node(flowList **h, wData *wdp, wData *revWdp)
{
	flowList *flp;
	if ((*h) == NULL)
	{
		*h = calloc(1, sizeof(flowList));
		(*h)->cpuNext = wdp;
		return;
	}
	flp = (*h);
	while (flp->next)
		flp = flp->next;

	flp->next = calloc(1, sizeof(flowList));
	flp->cpuNext = wdp;

	// now give reverse
	if (wdp->flowPrev)
	{
		for (flp = wdp->flowPrev; flp->next; flp = flp->next)
			;
		flp->next = calloc(1, sizeof(flowList));
		flp->next->cpuNext = revWdp;
	}
	else
	{
		wdp->flowPrev = calloc(1, sizeof(flowList));
		wdp->flowPrev->cpuNext = revWdp;
	}


}
static wData *findTopBK(flowList *flo)
{
	if (!flo)
		return NULL;
	while (flo->next)
		flo = flo->next;
	return flo->cpuNext;
}
static wData *backTrace2Call(wData *wdp)
{
	// take the final one and we can find many call
	while (wdp)
	{
		if (wdp->callNormal)
			return wdp;
		wdp = findTopBK(wdp->flowPrev);
	}
	return NULL;
}

char *findSymInExp(expression *exp)
{
	char *sym;
	switch (exp->type)
	{
	case ExpIsASCII:
	case ExpIsNumber:
	case ExpIsPCNow:
	case ExpIsStackOffset:
		return NULL;
	case ExpIsTree:
		if ((sym = findSymInExp(exp->exp.tree.left))!=NULL)
			return sym;
		return findSymInExp(exp->exp.tree.right);
	case ExpIsSymbol:
		return exp->exp.symbol;

	}
	return NULL;
}

wData *findLabelWdp(expression *exp, moduleLD *mdp)
{
	char *sym;
	symbol *symp;
	if ((sym=findSymInExp(exp))==NULL)
		return NULL;
	symp = findSymInModuleLD(sym, mdp, 1, 1);
	if (symp == NULL)
		return NULL;
	if(symp->labelPos)
		return symp->labelPos;
	if (symp->definedPos)
		return symp->definedPos->labelPos;
	return NULL;
}

// we can construct the graph by 2-pass method
// first pass is no branch method, constructs forward link
// second pass by the forward link, contructs the reverse link
// the problem is call and branch
// for a call entry, we search its end and construct the fw/back link
// it need first construct function-entry-exit 
// though c code usually has single function entry,
// but assembly may have 2 entry
// some marks is required to do the trick
// that is, we need 3 passes to construct all flow links.





#define MAXPOSSIBLEFLOWN 256 

// it cound be array!!
// 
static wData *findNextInstrHigh(wData * wdp)
{
	while (wdp && !wdp->instHighByte)
		wdp = wdp->next;
	// skip 0xf0
	//if (wdp && (getExpMinValue(wdp->exp) & 0xf0) == 0xf0) // 2word instr
	//{
	//	wdp = wdp->next;
	//	while (wdp && !wdp->instHighByte)
	//		wdp = wdp->next;
	//}
	return wdp;
}
static wData *findNextNextInstrHigh(wData * wdp)
{
	while (wdp && !wdp->instHighByte)
		wdp = wdp->next;
	if (wdp)
	{
		wdp = wdp->next;
		while (wdp && !wdp->instHighByte)
			wdp = wdp->next;
	}
	if (wdp && !wdp->exp)
	{
		fprintf(stderr, "internal error, wdp no exp at %s:%d\n", __FILE__, __LINE__);
		exit(__LINE__);
	}
	//if (wdp && (getExpMinValue(wdp->exp) & 0xf0) == 0xf0) // 2word instr
	//{
	//	wdp = wdp->next;
	//	while (wdp && !wdp->instHighByte)
	//		wdp = wdp->next;
	//}

	return wdp;
}

union arru{wData *wdp; int m;} ;

// find next instructions as WDP array
// return 1 means next head instruction no use, for long call's nopf
static int findNextExecutionInstructions(wData *wdp, moduleLD *mdp, wData **wdpArr)
{
	int highbyte, lowbyte, wordMin;
	int br1;
	union arru arru0;
	if (!wdp->instHighByte)
		return 0;
	if (!mdp)
	{
		fprintf(stderr, "null mdp\n");
	}

#ifdef DEBUGBSR
	if (wdp->locTree && strstr(wdp->locTree, "56"))
		printf("%d\n",56);
#endif
	//memset(wdpArr, 0, MAXPOSSIBLEFLOWN * sizeof(wData *));
	arru0.wdp=NULL;
	arru0.m = 0;
	wdpArr[MAXPOSSIBLEFLOWN] = arru0.wdp; // number at final
	highbyte = getExpMinValue(wdp->exp);
	lowbyte = getExpMinValue(wdp->next->exp);
	wordMin = highbyte * 256 + lowbyte;
	// possible wordmin
	br1 = codeIsBranch(wordMin);
	wdp->bsrRelated = opCodeBSRRelated(wordMin);
	
	switch (br1)
	{
	case 1: // rj && rcall, flow is only the new entry!!
		wdpArr[0] = findNextInstrHigh(findLabelWdp(wdp->exp, mdp));
		arru0.m=(wdpArr[0]!=NULL);
		wdpArr[MAXPOSSIBLEFLOWN] =arru0.wdp;
		if ((highbyte & 0xf8) == 0xc8)
		{
			wdp->callNormal = 1;
			if(wdpArr[0]!=NULL)
				wdpArr[0]->called = 1;
			return 1; // if call return 1
		}
		return 0;
	case 2: // lcall and ljump
		wdpArr[0] = findNextInstrHigh(findLabelWdp(wdp->next->next->exp, mdp));
		arru0.m=(wdpArr[0]!=NULL);
		wdpArr[MAXPOSSIBLEFLOWN] = arru0.wdp;
		if (highbyte==0xc0)
		{
			wdp->callNormal = 1;
			if(wdpArr[0]!=NULL)
				wdpArr[0]->called = 1;
			return 1;
		}
		return 2; // C8 is long jump, we need process second word
	case 3: // JXX
		// JXX has 2 possible forward path
		wdpArr[0] = findNextInstrHigh(wdp->next);
		wdpArr[1] = findNextInstrHigh(findLabelWdp(wdp->next->exp, mdp));
		wdpArr[MAXPOSSIBLEFLOWN] = (wData*)2;
		return 0;
	case 4: // RET
		wdp->retHere = 1;
		wdpArr[0] = NULL;
		wdpArr[MAXPOSSIBLEFLOWN] = (wData*)0;
		return 0; // I don't know where to go yet 
	}
	// consider skip first
	if (wdp->instSkip) // instSkip is contructed from compiler
	{
		wdpArr[0] = findNextInstrHigh(wdp->next);
		wdpArr[1] = findNextNextInstrHigh(wdp->next);
		wdpArr[MAXPOSSIBLEFLOWN] = (wData*)2;
		return 0;
	}
	// it is possible PC_LAT distroyed.
	// because SFR is usually EQU, it usually optimized out.

	// 

	if ((highbyte & 0xff) == 0xd0)
	{
		// it cound be symbol or EQU
		char *symn = findSymInExp(wdp->next->next->exp); // high/low should both have that sym!!
		if (symn != NULL)
		{
			if (!STRCASECMP(symn, "_PCLATL"))
				wdp->callfptr = 1;
		}
		else
		{
			if (getExpMinValue(wdp->next->next->exp) == 0xf0 &&
				getExpMinValue(wdp->next->next->next->exp) == 0x1b)
				wdp->callfptr = 1; // 1b is PCLLATL
		}
	}
	if (wdp->callfptr) // 0 no change
		return 0; // special oper

	if ((highbyte & 0xff) == 0x66)
	{
		int isPCLATL = 0;
		char *symn = findSymInExp(wdp->exp); // high/low should both have that sym!!
		if (symn != NULL)
		{
			if (!STRCASECMP(symn, "_PCLATL"))
				isPCLATL = 1;
		}
		else
		{

			if (getExpMinValue(wdp->next->exp) == 0x1b)
				isPCLATL = 1; // 1b is PCLLATL
		}
		if (isPCLATL)
		{
			// we have munti addresses
			// search for RJ
			wData *wdp3 = wdp->next->next->next;
			int i = 0;
			while (wdp3->exp && ((getExpMinValue(wdp3->exp) & 0xf8) == 0x78))
			{
				wdpArr[i++] = wdp3;
				wdp3 = wdp3->next->next; // pack
			}
			arru0.m=i;
			wdpArr[MAXPOSSIBLEFLOWN] = arru0.wdp;
			return 0;

		}
	}

	// finally normal case
	wdpArr[0] = findNextInstrHigh(wdp->next);
	arru0.m=1;
	wdpArr[MAXPOSSIBLEFLOWN] = arru0.wdp;
	return 0;
	
}

flowList *wdpArr2flowList(wData ** wdparr)
{
	int i;
	flowList *flp = NULL;
	flowList *flpRoot = NULL;
	union arru arru1;
	arru1.wdp = wdparr[MAXPOSSIBLEFLOWN];
	for (i = 0; i< arru1.m; i++)
	{
		if(wdparr[i]==NULL)
			break;
		if (!i)
		{
			flp = flpRoot = (flowList*)calloc(1, sizeof(flowList));
			flp->cpuNext = wdparr[i];
		}
		else
		{
			flp->next = (flowList*)calloc(1, sizeof(flowList));
			flp->next->cpuNext = wdparr[i];
			flp = flp->next;
		}
	}
	return flpRoot;
}

static struct sFlowList * clearFlowList(struct sFlowList *flp)
{
	if (flp->next)
		flp->next = clearFlowList(flp->next);
	free((void*)flp);
	return NULL;
}

void buildFlow(void) // wDATAh has a next wdps-in-flow , both direction, to check the BSR
{
	wData *wdp;
	wData *wdpArr[MAXPOSSIBLEFLOWN+1];
	moduleLD *mdp;
	funcRecord *fp;
	union arru arru2;
	area *ap;
	int  isCALLFUNC = 0; // if is CALL, we have to check if it is the head of function. 
	// if yes, we need construct the flow of func's RET
#ifdef DEBUGBSR
	flowList *flp;
	char buf[1024];
	char buf2[1024];
#endif
	//uint64 hashsetjmpid = hash("___setjmp");
	// phase1 build forward flow
	//2021 FEB, second time remove old pointers

	for (ap = topMap.romArea; ap; ap = ap->next)
	{
		for (wdp = ap->wDatas; wdp && wdp != ap->wDataTail; wdp = wdp->next)
		{
			if (!wdp->instHighByte)
				continue;
			if (wdp->flowNext)
				wdp->flowNext = clearFlowList(wdp->flowNext);
			if (wdp->flowPrev)
				wdp->flowPrev = clearFlowList(wdp->flowPrev);
		}
	}


	for (ap = topMap.romArea; ap; ap = ap->next)
	{
		for (wdp = ap->wDatas; wdp && wdp != ap->wDataTail; wdp = wdp->next)
		{
			if (!wdp->instHighByte)
				continue;
			isCALLFUNC = findNextExecutionInstructions(wdp, wdp->m, wdpArr);
			arru2.wdp = wdpArr[MAXPOSSIBLEFLOWN];
			if (arru2.m != 0) // possible literal call
			{
				wdp->flowNext = wdpArr2flowList(wdpArr);
				if (isCALLFUNC==1) // call
				{
					// we need to check if it is a head of a function
					
					if (!wdpArr[0]->fp || !(wdpArr[0]->fp->firstWD) || !(wdpArr[0]->fp->finalWD))
					{
						//fprintf(stderr, "Warning, strange call at %s.\n", wdp->locTree);
					}
					else if (wdpArr[0]->fp && wdpArr[0]->fp->namehash==hashsetjmpid)
					{
						wData *wd1 = wdpArr[0]->fp->firstWD;
						if (wd1->wdIsLabel)
							wd1 = wd1->next;
						if (wd1 == wdpArr[0]) // it is the entry
						{
							wData *wdpn = wdp->next->next;
							if (wdpn->flowPrev == NULL)
							{
								wdpn->flowPrev = (flowList *)calloc(1, sizeof(flowList));
								wdpn->flowPrev->cpuNext = NULL; // add the reverse flow directly
							}
							else
							{
								flowList *f1 = wdpn->flowPrev;
								while (f1->next)
									f1 = f1->next;
								f1->next = (flowList *)calloc(1, sizeof(flowList));
								f1->next->cpuNext = NULL;
							}
						}
					}
					else
					{
						wData *wd1 = wdpArr[0]->fp->firstWD;
						if (wd1->wdIsLabel)
							wd1 = wd1->next;
						if (wd1 == wdpArr[0]) // it is the entry
						{
							wData *wdpn = wdp->next->next;
							if (wdpn->flowPrev == NULL)
							{
								wdpn->flowPrev = (flowList *)calloc(1, sizeof(flowList));
								wdpn->flowPrev->cpuNext = wdpArr[0]->fp->finalWD; // add the reverse flow directly
								wdpn->flowPrev->isBackFromCall = 1;
								wdpn->flowPrev->fp = wdpArr[0]->fp;
							}
							else
							{
								flowList *f1 = wdpn->flowPrev;
								while (f1->next)
									f1 = f1->next;
								f1->next = (flowList *)calloc(1, sizeof(flowList));
								f1->next->cpuNext = wdpArr[0]->fp->finalWD;
								f1->next->isBackFromCall = 1;
								f1->next->fp = wdpArr[0]->fp;
							}
#ifdef DEBUGBSR
							printf("add rev flow from %s to %s\n", wdpn->locTree, wdpArr[0]->fp->finalWD->locTree);
#endif

						}
					}
				}
				else if (isCALLFUNC == 2) // long jump
				{
					// need to skip FXXX if prev prev is not skip
					wData *wdpPre = findPrevWdp(wdp);
					while(wdpPre && ! wdpPre->instHighByte)
						wdpPre = findPrevWdp(wdpPre);
					if (wdpPre && !wdpPre->instSkip)
					{
						if (wdp->next) wdp = wdp->next;
						if (wdp->next) wdp = wdp->next;
					}
				}
#ifdef DEBUGBSR
				for (flp = wdp->flowNext; flp; flp = flp->next)
					printf("construct forward flow %s, %s -> %s, %s\n", wdp->locTree, exprDump(wdp->exp, buf, 1023),
						flp->cpuNext->locTree, exprDump(flp->cpuNext->exp, buf2, 1023));
#endif
			}
		}
	}
	// phase2 build reverse flow
	for (ap = topMap.romArea; ap; ap = ap->next)
	{
		for (wdp = ap->wDatas; wdp && wdp != ap->wDataTail; wdp = wdp->next)
		{
			flowList *fl2;
			if (!wdp->instHighByte)
				continue;
			for (fl2 = wdp->flowNext; fl2; fl2 = fl2->next)
			{
				flowList *fl = fl2->cpuNext->flowPrev;
				if (fl == NULL)
				{
					fl = (flowList *)calloc(1, sizeof(flowList));
					fl->cpuNext = wdp;
					fl2->cpuNext->flowPrev = fl;
#ifdef DEBUGBSR
					printf("construct back flow %s, %s -> %s , %s\n",fl2->cpuNext->locTree, exprDump(fl2->cpuNext->exp, buf, 1023),
						wdp->locTree, exprDump(wdp->exp, buf2,1023));
#endif
				}
				else
				{
					while (fl->next)
						fl = fl->next;
					fl->next = (flowList *)calloc(1, sizeof(flowList));
					fl->next->cpuNext = wdp;
#ifdef DEBUGBSR
					printf("construct back flow2 %s -> %s\n", fl2->cpuNext->locTree,
						wdp->locTree);
#endif

				}
			}

		}
	}
	// phase 3, for fptr called function, we insert a "null" prev branch
	// which means it needs to set BSR at its head
	
	for (mdp = topMap.linkedModules; mdp; mdp = mdp->next)
	{
		int i;
		for (i = 0; i < mdp->funcNum; i++)
		{
			fp = mdp->funcs[i];
			if (fp->calledByFPTR && fp->firstWD!=NULL && fp->firstWD->flowPrev!=NULL)
			{
				// just clear it
				fp->firstWD->flowPrev = NULL;
			}
		}
	}

}

static void clearSTKXXAUX(void)
{
	wData *wdp;
	area *ap;
	for (ap = topMap.romArea; ap; ap = ap->next)
	{
		for (wdp = ap->wDatas; wdp && wdp != ap->wDataTail; wdp = wdp->next)
		{
			wdp->stkxxAux = 0;
		}
	}
}

// stkxx = -1 means W
// return 1 means referenced
// we don't follow branches... which takes a lot of time

static int chkSTKxxReferenced(int stkxx, wData *wdp, int lev)
{
	wData *wdpCommon;
	if (lev > 64) // it must be wrong
	{
		fprintf(stderr, "Error, internal error, %s:%d\n", __FILE__, __LINE__);
		exit(-__LINE__);
	}
	if (!lev)
	{
		clearSTKXXAUX();

	}
	if (!wdp || wdp->stkxxAux)
		return 0;
	if (stkxx < 0) // check W
	{
		
		do {
			if (wdp->inCond & LNK_COND_W)
				return 1;
			if (wdp->outCond & LNK_COND_W)
				return 0; // W destroyed
			if (wdp->outCond & (LNK_COND_RET | LNK_COND_CALL))
				return 1; // W referenced
			// for common code, trace until RET
			if (wdp->outCond & LNK_COND_CALL_COMMON)
			{
				int countCommon=0;
				wdpCommon = wdp->flowNext->cpuNext;
				do {
					if (wdpCommon->inCond & LNK_COND_W)
						return 1;
					if (wdpCommon->outCond & LNK_COND_W)
						return 0; // W destroyed
					if (++countCommon > 8192) // too long common
						return 1;
					wdpCommon = wdpCommon->flowNext->cpuNext;
				} while (!(wdpCommon->outCond&LNK_COND_RET));
			}

			wdp->stkxxAux = 1;
			if (wdp->flowNext == NULL)
				return 0;
			if (wdp->flowNext->next) // more than one branch
			{
				flowList * flp = wdp->flowNext;
				do {
					if (chkSTKxxReferenced(stkxx, flp->cpuNext, lev + 1))
						return 1;
					flp = flp->next;
				} while (flp);
				return 0;
			}
			wdp = wdp->flowNext->cpuNext;

		} while (wdp && !wdp->stkxxAux);
		return 0;
	}
	else
	{

		int stkxxcond;
		// stk00~02 is EQU/alias in hy17 series
		
		if (stkxxAddrArray[stkxx] < 0)
		{
			//if(HY08A_getPART()->isEnhancedCore!=4 || stkxx>=3) // 012 is alias
			return 0;
		}
		stkxxcond = stkxxconds[stkxx];
		do {
			if (wdp->inCond & stkxxcond)
				return 1;
			if (wdp->outCond & stkxxcond)
				return 0; // W destroyed
			if (wdp->outCond & (LNK_COND_RET ))
				return 1; // assume referenced
			if (wdp->outCond & LNK_COND_CALL)
				return 0; // assume no reference
			if (wdp->outCond & LNK_COND_CALL_COMMON)
			{
				int countCommon = 0;
				wdpCommon = wdp->flowNext->cpuNext;
				do {
					if (wdpCommon->inCond & stkxxcond)
						return 1;
					if (wdpCommon->outCond & stkxxcond)
						return 0; // W destroyed
					if (++countCommon > 8192) // too long common
						return 1;
					wdpCommon = wdpCommon->flowNext->cpuNext;
				} while (!(wdpCommon->outCond&LNK_COND_RET));
			}


			wdp->stkxxAux = 1;
			if (wdp->flowNext == NULL)
				return 0;
			if (wdp->flowNext->next) // more than one branch
			{
				flowList * flp = wdp->flowNext;
				do {
					if (chkSTKxxReferenced(stkxx, flp->cpuNext, lev + 1))
						return 1;
					flp = flp->next;
				} while (flp);
				return 0;
			}
			wdp = wdp->flowNext->cpuNext;

		} while (wdp && !wdp->stkxxAux);

	}
	return 1; // later add stkxx
}


// in a function,
// W may be modified in many places
// we shall use the mark generated by compiler
// the special case is W,
// which may be in the middle of a loop,
// and if it is referenced first..
// we skip that path
static wData * stkxxModifyPlaces[256]; // I think 256 is the most places
static int placeCount;

static int findBranchTailModifySTKXX(wData *wdp, int stkxx, int lev)
{
	int branchCount = 0;
	flowList *flp;
	if (wdp->callNormal) // if find a call, we don't process
		return 0;
	if (lev > 1024)
	{
		fprintf(stderr, "internal error, lnk opt search too deep, %s:%d\n", __FILE__, __LINE__);
		exit(-__LINE__);
	}

	if (stkxx < 0)// W
	{
		do {
			if (wdp->outCond& LNK_COND_BRANCH) // if there is a branch we give up
				return 0;
			if (wdp->outCond & LNK_COND_W)
			{
				stkxxModifyPlaces[placeCount++] = wdp;
				if (placeCount >= 256)
				{
					fprintf(stderr, "Internal error, lnk opt fail %s:%d\n", __FILE__, __LINE__);
					exit(-__LINE__);
				}
				return ++branchCount;
			}
			if (wdp->inCond & LNK_COND_W)
				return 0;
			flp = wdp->flowPrev;
			if (flp && flp->next)
			{
				do {
					branchCount += findBranchTailModifySTKXX(flp->cpuNext, stkxx, lev + 1);
					flp = flp->next;
				} while (flp);
				return branchCount;
			}
			if (!flp)
				return 0;// special case
			wdp = flp->cpuNext;
		} while (wdp);
		return branchCount;
	}

	int stkxxcond = stkxxconds[stkxx];

	do {
		if (wdp->outCond& LNK_COND_BRANCH) // if there is a branch we give up
			return 0;
		if (wdp->outCond & stkxxcond)
		{
			stkxxModifyPlaces[placeCount++] = wdp;
			if (placeCount >= 256)
			{
				fprintf(stderr, "Internal error, lnk opt fail %s:%d\n", __FILE__, __LINE__);
				exit(-__LINE__);
			}
			return ++branchCount;
		}
		if (wdp->inCond & stkxxcond)
			return 0;
		flp = wdp->flowPrev;
		if (flp && flp->next)
		{
			do {
				branchCount += findBranchTailModifySTKXX(flp->cpuNext, stkxx, lev + 1);
				flp = flp->next;
			} while (flp);
			return branchCount;
		}
		if (!flp)
			return 0;// special case
		wdp = flp->cpuNext;
	} while (wdp);
	return branchCount;
}
// consider there is JMPRET to ret
// we use tables to get the return address of functions
static int findFuncTailModifySTKXX(funcRecord *fp, int stkxx) // this is a recursive function
{
	wData *wdp;
	int branchCount = 0;
	int i;
	flowList * flp;
	clearSTKXXAUX(); // clear flags
	placeCount = 0;

	// search back from all the return places

	for (i = 0; i < fp->retLocCount; i++)
	{
		wdp = fp->retLoc[i];

		if (stkxx < 0)
		{

			if (wdp->outCond & LNK_COND_W) // RETL, no optimization
				continue;
			// search for following branches
			flp = wdp->flowPrev;
			if (!flp)
				return placeCount;
			do {
				branchCount += findBranchTailModifySTKXX(flp->cpuNext, stkxx, 0);
				flp = flp->next;
			} while (flp);

		}
		else
		{
			
			flp = wdp->flowPrev;
			if (!flp)
				return placeCount;
			do {
				branchCount += findBranchTailModifySTKXX(flp->cpuNext, stkxx, 0);
				flp = flp->next;
			} while (flp);
		}

	}

	return placeCount;
}

static inline int getOpCodeCondStkxx(int op16,int bsrNow)
{
	int i;
	int enhanced = HY08A_getPART()->isEnhancedCore;
	if ((op16 >> 12) == 0x0d || (op16 >> 12) == 0x0f)  // MVFF
	{
		for (i = 0; i < 7; i++)
		{
			if (stkxxAddrArray[i] == (op16 & 0xfff))
				return stkxxconds[i];
		}
		return 0;
	}
	for (i = 0; i < 7; i++)
	{ // stkxx < 0x80 no need check bsr!!
			
		if ((stkxxAddrArray[i] & 0xff) == (op16 & 0xff))
		{
			// depends on bank, etc
			if (enhanced == 4 && ramModel == RAM_MODEL_FLATD) // normal use FLATD?
			{
				if (( bsrNow< 0 && (op16 & 0x100) && stkxxAddrArray[i]>0x100) || (stkxxAddrArray[i] < 0x80 && !(op16 & 0x100)) ||
					(op16 & 0x100 && bsrNow == (stkxxAddrArray[i] >> 8)
						))
					return stkxxconds[i];
				continue;
			}
			else
			{
				if (((!(op16 & 0x100))  && stkxxAddrArray[i] < 0x100) ||
					((op16 & 0x100) &&  (stkxxAddrArray[i]>=0x100)
						))
					return stkxxconds[i];
				continue;
			}
			
		}
	}
	return 0;
}

// for STKXX optimization, build the flow with call/ret
// if possible, build flow with jump tables
int buidFlowStkxx(void) // wDATAh has a next wdps-in-flow , both direction, to check the BSR
{
	wData *wdp;
	wData *wdpArr[MAXPOSSIBLEFLOWN+1]; // final one is the number!!
	moduleLD *mdp;
	funcRecord *fp;
	union arru arru3;
	area *ap;
	int  isCALLFUNC = 0; // if is CALL, we have to check if it is the head of function. 
	int  count = 0;
	int i;
	//static int hashset = 0;
	// if yes, we need construct the flow of func's RET
	//static  uint64 hashsetjmpid;// = hash("___setjmp");
	// phase1 build forward flow
	//if (!hashset)
	//{
	//	hashset = 1;
	//	//hashmain = hash("_main");
	//	hashsetjmpid = hash("___setjmp");
	//}
	//2021 FEB, second time remove old pointers

	for (ap = topMap.romArea; ap; ap = ap->next)
	{
		for (wdp = ap->wDatas; wdp && wdp != ap->wDataTail; wdp = wdp->next)
		{
			if (!wdp->instHighByte)
				continue;
			if (wdp->flowNext)
				wdp->flowNext = clearFlowList(wdp->flowNext);
			if (wdp->flowPrev)
				wdp->flowPrev = clearFlowList(wdp->flowPrev);
		}
	}


	for (ap = topMap.romArea; ap; ap = ap->next)
	{
		for (wdp = ap->wDatas; wdp && wdp != ap->wDataTail; wdp = wdp->next)
		{
			if (!wdp->instHighByte)
				continue;
			
			isCALLFUNC = findNextExecutionInstructions(wdp, wdp->m, wdpArr);
			arru3.wdp = wdpArr[MAXPOSSIBLEFLOWN];
			if (arru3.m != 0) // possible literal call
			{
				wdp->flowNext = wdpArr2flowList(wdpArr);
				if (isCALLFUNC == 1) // call
				{

					// we need to check if it is a head of a function


					if (!wdpArr[0]->fp || !(wdpArr[0]->fp->firstWD) || !(wdpArr[0]->fp->finalWD))
					{
						fprintf(stderr, "Error, Internal error, strange call at %s, [Internal]%s:%d\n", wdp->locTree, __FILE__, __LINE__);
						exit(-__LINE__);
					}
					else if (wdpArr[0]->fp && wdpArr[0]->fp->namehash == hashsetjmpid)
					{
						wData *wd1 = wdpArr[0]->fp->firstWD;
						if (wd1->wdIsLabel)
							wd1 = wd1->next;
						if (wd1 == wdpArr[0]) // it is the entry
						{
							wData *wdpn = wdp->next->next;
							if (wdpn->flowPrev == NULL)
							{
								wdpn->flowPrev = (flowList *)calloc(1, sizeof(flowList));
								wdpn->flowPrev->cpuNext = NULL; // add the reverse flow directly
							}
							else
							{
								flowList *f1 = wdpn->flowPrev;
								while (f1->next)
									f1 = f1->next;
								f1->next = (flowList *)calloc(1, sizeof(flowList));
								f1->next->cpuNext = NULL;
							}
						}
					}
					else
					{
						wData *wd1 = wdpArr[0]->fp->firstWD;
						funcRecord *fp = wdpArr[0]->fp;
						if (wd1->wdIsLabel)
							wd1 = wd1->next;

						fp->callmeWDP[fp->callmeWDPNum++] = findNextInstrHigh(wdp->next);
						if (fp->callmeWDPNum >= 8192)
						{
							fprintf(stderr, "Error, Module %s function %s called too many times.\n",fp->m->name, fp->name);
							exit(-__LINE__);
						}
						if (wd1 == wdpArr[0]) // it is the entry
						{
							wData *wdpn = wdp->next->next;

							// add forward flow to final WD of callee
							wData *wdpt = wdpArr[0]->fp->finalWD;
							if (wdpt->flowNext == NULL)
							{
								wdpt->flowNext = (flowList *)calloc(1, sizeof(flowList));
								wdpt->flowNext->cpuNext = wdpn;
							}
							else
							{
								flowList *flp = wdpt->flowNext;
								while (flp->next)
									flp = flp->next;
								flp->next = (flowList *)calloc(1, sizeof(flowList));
								flp->next->cpuNext = wdpn;
							}

							if (wdpn->flowPrev == NULL)
							{
								wdpn->flowPrev = (flowList *)calloc(1, sizeof(flowList));
								wdpn->flowPrev->cpuNext = wdpArr[0]->fp->finalWD; // add the reverse flow directly
								wdpn->flowPrev->isBackFromCall = 1;
								wdpn->flowPrev->fp = wdpArr[0]->fp;
							}
							else
							{
								flowList *f1 = wdpn->flowPrev;
								while (f1->next)
									f1 = f1->next;
								f1->next = (flowList *)calloc(1, sizeof(flowList));
								f1->next->cpuNext = wdpArr[0]->fp->finalWD;
								f1->next->isBackFromCall = 1;
								f1->next->fp = wdpArr[0]->fp;
							}
#ifdef DEBUGBSR
							printf("add rev flow from %s to %s\n", wdpn->locTree, wdpArr[0]->fp->finalWD->locTree);
#endif

						}
					}
				}
				else if (isCALLFUNC == 2) // long jump
				{
					// need to skip FXXX if prev prev is not skip
					wData *wdpPre = findPrevWdp(wdp);
					while (wdpPre && !wdpPre->instHighByte)
						wdpPre = findPrevWdp(wdpPre);
					if (wdpPre && !wdpPre->instSkip)
					{
						if (wdp->next) wdp = wdp->next;
						if (wdp->next) wdp = wdp->next;
					}
				}
#ifdef DEBUGBSR
				for (flp = wdp->flowNext; flp; flp = flp->next)
					printf("construct forward flow %s, %s -> %s, %s\n", wdp->locTree, exprDump(wdp->exp, buf, 1023),
						flp->cpuNext->locTree, exprDump(flp->cpuNext->exp, buf2, 1023));
#endif
			}
		}
	}
	// phase2 build reverse flow
	for (ap = topMap.romArea; ap; ap = ap->next)
	{
		for (wdp = ap->wDatas; wdp && wdp != ap->wDataTail; wdp = wdp->next)
		{
			flowList *fl2;
			if (!wdp->instHighByte)
				continue;
			for (fl2 = wdp->flowNext; fl2; fl2 = fl2->next)
			{
				flowList *fl = fl2->cpuNext->flowPrev;
				if (fl == NULL)
				{
					fl = (flowList *)calloc(1, sizeof(flowList));
					fl->cpuNext = wdp;
					fl2->cpuNext->flowPrev = fl;
#ifdef DEBUGBSR
					printf("construct back flow %s, %s -> %s , %s\n", fl2->cpuNext->locTree, exprDump(fl2->cpuNext->exp, buf, 1023),
						wdp->locTree, exprDump(wdp->exp, buf2, 1023));
#endif
				}
				else
				{
					while (fl->next)
						fl = fl->next;
					fl->next = (flowList *)calloc(1, sizeof(flowList));
					fl->next->cpuNext = wdp;
#ifdef DEBUGBSR
					printf("construct back flow2 %s -> %s\n", fl2->cpuNext->locTree,
						wdp->locTree);
#endif

				}
			}

		}
	}
	//// phase 3, for fptr called function, we insert a "null" prev branch
	//// which means it needs to set BSR at its head

	//for (mdp = topMap.linkedModules; mdp; mdp = mdp->next)
	//{
	//	int i;
	//	for (i = 0; i < mdp->funcNum; i++)
	//	{
	//		fp = mdp->funcs[i];
	//		if (fp->calledByFPTR && fp->firstWD != NULL && fp->firstWD->flowPrev != NULL)
	//		{
	//			// just clear it
	//			fp->firstWD->flowPrev = NULL;
	//		}
	//	}
	//}

	// gen COND (dep analysis)
	// first is find stkxx address
	for (i = 0; i < 7; i++)
	{
		char stkxxname[32];
		symbol *symp;
		snprintf(stkxxname, 31, "STK%02d", i);
		if (i < 3 && HY08A_getPART()->isEnhancedCore==4) // only hy17p series can use ..
		{
			 // STK00 is 0x20, stk01 is 0x1f, stk02 is 1E
			switch (i)
			{
			case 0: stkxxAddrArray[i] = 0x20; break;
			case 1: stkxxAddrArray[i] = 0x1F; break;
			case 2: stkxxAddrArray[i] = 0x1E; break;
			default:break;
			}
			continue;
		}
		symp = findSymInModuleLD(stkxxname, mainf->firstWD->m, 1, 1);
		if (!symp || !symp->labelPos || symp->labelPos->next->sizeOrORG==0)
			continue;
		stkxxAddrArray[i] = symp->labelPos->areaOffset;
	}
	for (ap = topMap.romArea; ap; ap = ap->next)
	{
		for (wdp = ap->wDatas; wdp && wdp != ap->wDataTail; wdp = wdp->next)
		{
			int op16=0;
			int op16b = 0; // for MVFF/MVSF
			wData *wdpnw;
			if (!wdp->instHighByte)
				continue;
			/*if (strstr(wdp->locTree,"552"))
			{
				fprintf(stderr, "line 552 here.\n");
			}*/
			op16 = ((getExpValue(wdp->exp, wdp->m, 0, wdp->areaOffset, 0, wdp->fsr2_stack_offset, wdp->fp, wdp)&0xff)<<8)|
				(getExpValue(wdp->next->exp, wdp->m, 0,wdp->areaOffset,0, wdp->fsr2_stack_offset, wdp->fp, wdp->next)&0xff);
			wdp->inCond = opCodeInCond(op16);
			
			wdp->outCond = opCodeOutCond(op16);
			if (wdp->outCond & LNK_COND_CALL)
			{
				expression *expsym = getSymInExp(wdp->exp);
				if (expsym &&  strstr(expsym->exp.symbol, "__COMMON_")) // dual _ prevent user code match
				{
					wdp->outCond &= ~LNK_COND_CALL;
					wdp->outCond |= LNK_COND_CALL_COMMON;
				}
			}

			if ((wdp->inCond & LNK_COND_F))
			{
				wdp->inCond |= getOpCodeCondStkxx(op16, wdp->bankVal);
			}
			if ((wdp->outCond & LNK_COND_F))
			{
				if ((op16 & 0xf000) == 0xd000 // MVFF || 
					|| (op16 & 0xff80) == 0xc600)
				{
					int op16b = ((getExpValue(wdp->next->next->exp, wdp->m, 0, wdp->areaOffset, 0, wdp->fsr2_stack_offset, wdp->fp, wdp) & 0xff) << 8) |
						(getExpValue(wdp->next->next->next->exp, wdp->m, 0, wdp->areaOffset, 0, wdp->fsr2_stack_offset, wdp->fp, wdp->next) & 0xff);
					wdp->outCond |= getOpCodeCondStkxx(op16b, wdp->bankVal);
				}
				else
				{
					wdp->outCond |= getOpCodeCondStkxx(op16, wdp->bankVal);
				}
			}
			if (wdp->fp && (wdp->outCond & LNK_COND_RET))
			{
				wdp->fp->retLoc[wdp->fp->retLocCount++] = wdp;
				if (wdp->fp->retLocCount >= 1024)
				{
					fprintf(stderr,"Error, module %s function %s too many return locations.\n", wdp->m->name, wdp->fp->name);
					exit(-__LINE__);
				}
			}
			// condW need check PLUSW
			if (((op16 >> 8) & 0xff) == 0xd0) // MVFF
			{
				wdpnw = wdp->next->next;
				if ((op16 & 0xfff) == 0x29)
				{
					wdp->inCond &= ~LNK_COND_F;
					wdp->inCond |= LNK_COND_W;
				}
				op16b = ((getExpValue(wdpnw->exp, wdpnw->m, 0, wdpnw->areaOffset, 0, wdpnw->fsr2_stack_offset, wdpnw->fp, wdpnw) & 0xff) << 8) |
					(getExpValue(wdpnw->next->exp, wdpnw->m, 0, wdpnw->areaOffset, 0, wdpnw->fsr2_stack_offset, wdpnw->fp, wdpnw->next) & 0xff);
				if ((op16b & 0xfff) == 0x29)
				{
					wdp->outCond &= ~LNK_COND_F;
					wdp->outCond |= LNK_COND_W;
				}
			}
			else if ( (op16&0xff80) == 0xc600) // MVSF
			{
				wdpnw = wdp->next->next;
				op16b = ((getExpValue(wdpnw->exp, wdpnw->m, 0, wdpnw->areaOffset, 0, wdpnw->fsr2_stack_offset, wdpnw->fp, wdpnw) & 0xff) << 8) |
					(getExpValue(wdpnw->next->exp, wdpnw->m, 0, wdpnw->areaOffset, 0, wdpnw->fsr2_stack_offset, wdpnw->fp, wdpnw->next) & 0xff);
				if ((op16b & 0xfff) == 0x29)
				{
					wdp->outCond &= ~LNK_COND_F;
					wdp->outCond |= LNK_COND_W;
				}
			}
			else if ((op16 >> 8) != 0xc6 && (op16>>12)<0x0d)
			{

				if ((wdp->inCond & LNK_COND_F) && !(wdp->inCond & LNK_COND_W))
				{
					if (!(op16 & 0x100) // A bit
						&& ((op16 & 0xff) == 4 ||
						(op16 & 0xff) == 9 ||
							(op16 & 0xff) == 0x0e))
					{
						wdp->inCond |= LNK_COND_W; // PLUSW
					}
					if (!(op16 & 0x100) // A bit
						&& ((op16 & 0xff) == 0x29))
					{
						wdp->inCond |= LNK_COND_W; // wreg directly
						wdp->outCond &= ~LNK_COND_F;

					}

				}
				// outCond need check immediate
				if ((wdp->outCond & LNK_COND_F) && !(wdp->outCond & LNK_COND_W))
				{
					if (!(op16 & 0x100) // A bit==0
						&& ((op16 & 0xff) == 0x29))
					{
						wdp->outCond |= LNK_COND_W; // IMM 2 W
						wdp->outCond &= ~LNK_COND_F;
					}
				}
			}

		}
	}

	// now lets optimize MVFW before RET
	// if W is not used after RET,
	for (mdp = topMap.linkedModules; mdp; mdp = mdp->next)
	{
		int i,j,k,stkxx;
		wData *wdph;
		for (i = 0; i < mdp->funcNum; i++)
		{
			fp = mdp->funcs[i];
			if (fp->retVchecked || fp->calledByFPTR || fp->returnSize <= 0 || fp->callmeWDPNum <= 0 || 
				fp->callret2jmpOptimized || fp->namehash == hashMain) // don't optimize functions called by FPTR
			{
				continue;
			}
			fp->retVchecked = 1;
#ifdef _DEBUG
			//if (!strcmp(fp->name, "_fool"))
			//{
			//	fprintf(stderr, "_fool\n");
			//}
#endif
			// here the func shall return value
			for (stkxx = -1; stkxx < fp->returnSize-1; stkxx++) // 8->7, 4->3, because int returns w,stk00  long returns w, stk00,01,02
			{
				k = 0;

				for (j = 0; j < fp->callmeWDPNum; j++)
				{
					wData *fwdp = fp->callmeWDP[j];
					
					if (chkSTKxxReferenced(stkxx, fwdp, 0))
					{
						k = 1; // fail
						break;
					}
				}
				if (!k)
				{
					if (verbose)
					{
						if(stkxx<0)
							fprintf(stderr, "RET WREG of %s will be optimized.\n", fp->name);
						else
							fprintf(stderr, "RET STK%2d of %s will be optimized.\n",stkxx, fp->name);
					}
					// check for the instruction modifies W only
					k = findFuncTailModifySTKXX(fp, stkxx);// check W first
					// skip RETL
					
					if (k)
					{
						for (j = 0; j < placeCount; j++)
						{
							// SKIP RETL part!!
							if(stkxx<0 && getExpMinValue(stkxxModifyPlaces[j]->exp)==0x05)
								continue; 
							wdp = findPrevWdp(stkxxModifyPlaces[j]); // special case wdp is null
							
							
							if (!wdp)
								continue;
							wdph = findPrevWdp(wdp);
							if(wdph && wdph->instSkip) // this is skip 
							{
								if(verbose)
									fprintf(stderr,"%s", "retv opt skip because skip inst.\n");
								continue;
							}
							if(stkxxModifyPlaces[j]->next->next )
							{
								wdp->next = stkxxModifyPlaces[j]->next->next;
								stkxxModifyPlaces[j]->next->next->prev = wdp;
								count++;
								if (verbose)
								{
									if(stkxx<0)
										fprintf(stderr, "Info: RET WREG of %s optimized.\n", fp->name);
									else
										fprintf(stderr, "Info: RET  STK%02d of %s optimized.\n",stkxx, fp->name);
								}

							}
						}
					}

				}
			}
		}


		
	}
	// 

	return count;

}


/*



FDAOP:
	ADDF { $$=     0x1000        ;}
	|ADDC { $$=    0x1400       ;}
	|SUBF { $$=    0x1800    ;}
	|SUBC { $$=    0x1C00    ;}
	|XORF { $$=    0x2000    ;}
	|COMF { $$=    0x2400     ;}
	|ANDF { $$=    0x2800      ;}
	|IORF { $$=    0x2C00    ;}
	|DCF { $$=     0x3000    ;}
	|DCSZ { $$=   0x13400    ;} // bit16 is skip
	|INF { $$=     0x3800 ;}
	|INSZ { $$=   0x13C00 ;}
	|INSUZ { $$=  0x14000 ;}
	|DCSUZ { $$=  0x14400    ;}
	|RRF { $$=     0x4C00    ;}
	|RLF { $$=     0x4800    ;}
	|RRFC { $$=    0x5000    ;}
	|RLFC { $$=    0x5400    ;}
	|ARLC { $$=   0x25800     ;}// bit 17 means 08B no support
	|ARRC { $$=   0x25c00      ;}
	|SWPF { $$ =   0x6000   ;} // this line change because special FD format
	|MVF { $$=      0x6400    ;}
;


FAOP:
	CPSE{$$=     0x16C00      ;}
	|CPSG{$$=     0x16800      ;}
	|CPSL{$$=     0x16a00      ;}
	|TFSZ{$$=      0x16e00    ;}
	|SETF{$$=      0x0A00    ;}
	|CLRF{$$=     0x0C00      ;}
	|MULF{$$=      0x20e00    ;}
;
FBAOP:
	 BCF{$$=      0x8000 ;}
	|BSF{$$=      0x9000 ;}
	|BTGF{$$=      0xe000 ;}
	|BTSS{$$=      0x1b000 ;}
	|BTSZ{$$=      0x1a000 ;}
;
// 08xx : SUBL
// 09xx : MVLP (ROMA, 24 BIT)
// 0AXX~0FXX is FAOP				BSR
// 10xx~67xx is FDAOP				BSR
// 68XX~6Fxx is CPXX/TFSZ			BSR
// 7xxx      is JXX
// 8xxx/9xxx: bcf,bsf				BSR
// Axxx/Bxxx: btss/btsz				BSR
// C0XX~C1XX: CALL (with save)
// C2XX~C3xx:	  long jmp
// C4xx:        ADDFSR
// C5XX:		PUSHL
// C6XX:		MVSF,MVSS
// C7xx:		reserved (CALL offset?)
// C8XX:		RCALL
// Dxxx: MVFF
// EXXX: BTGF						BSR
// FXXX: NOPF

*/

static int opCodeBSRRelated(int opcode16)
{
	// 8xxx,9xxx,axxx,bxxx for bcf, bsv, btsz, btss
	int bitiChk = (opcode16 & 0xf000);
	// bit instructions ... have BTGF!!
	if ((bitiChk == 0x8000) || (bitiChk == 0x9000) || (bitiChk == 0xa000) || (bitiChk == 0xb000) || (bitiChk==0xe000))
		return 1;
	if ((bitiChk >= 0x1000) && (bitiChk <= 0x6000))
		return 1; // fda or fa instructions
	if ((opcode16 & 0xfe00) == 0x0a00) return 1; //setf
	if ((opcode16 & 0xfe00) == 0x0c00) return 1; //clrf
	if ((opcode16 & 0xfe00) == 0x0e00) return 1; //mulwf
	//if ((opcode16 & 0xfe00) == 0x0e00) return 1; //mulwf

	return 0;
}

static int opCodeInCond(int opcode16)
{
	if ((opcode16 & 0xfff0) == 0x0000) // group 000
	{
		switch (opcode16 & 0x0f)
		{
		case 0x0000: return 0; // NOP
		case 0x0001: return 0; // IDLE
		case 0x0002: return 0; // SLP
		case 0x0003: return 0; // CWDT
		case 0x0004: return LNK_COND_W | LNK_COND_DC | LNK_COND_C ; // DAW
		case 0x0005: return 0; // unknown, brk?
		case 0x0006: return LNK_COND_TBLR; // TBLR xx
		case 0x0007: return LNK_COND_TBLR; // TBLR xx
		case 0x0008:
		case 0x0009: return LNK_COND_RET; // RETI
		case 0x000A:
		case 0x000B: return LNK_COND_RET; // RET
		case 0x000c:  // LDPR, out cond only, FSR0~2
		case 0x000d: 
		case 0x000e: 
		case 0x000f: return 0; // 000F is reserved LDPR2 ?
		default: return 0;
		}
	}
	switch (opcode16 & 0xfff0)
	{
	case 0x0010: return 0; // LBSR
	case 0x0020: return 0; // PUSH,POP, 0030~0x00ff no use
	default: 
		if ((opcode16 & 0xff00) == 0)
			return 0;
		switch (opcode16 & 0xff00)
		{
		case 0x0100: return LNK_COND_W; // ANDL
		case 0x0200: return LNK_COND_W; // IORL
		case 0x0300: return LNK_COND_W; // XORL
		case 0x0400: return LNK_COND_W; // ADDL
		case 0x0500: return LNK_COND_RET; // RETLW, outcond W
		case 0x0600: return 0; // MVL, outcond w
		case 0x0700: return LNK_COND_W; // MULL
		case 0x0800: return LNK_COND_W; // SUBL
		case 0x0900: return LNK_COND_TBLR; // MVLP
		case 0x0a00: 
		case 0x0b00: return LNK_COND_F; // SETF
		case 0x0c00:
		case 0x0d00: return LNK_COND_F; // CLRF
		case 0x0e00: 
		case 0x0f00:return LNK_COND_F | LNK_COND_W; // MULF
		default:
			switch (opcode16 & 0xfc00)
			{
			case 0x1000:
				return LNK_COND_W | LNK_COND_F ; // ADDF
			case 0x1400:
				return LNK_COND_W | LNK_COND_F | LNK_COND_C; // ADDC
			case 0x1800:
				return LNK_COND_W | LNK_COND_F ; // SUBF
			case 0x1C00:
				return LNK_COND_W | LNK_COND_F | LNK_COND_C; // SUBC
			case 0x2000:
				return LNK_COND_W | LNK_COND_F; // XORF
			case 0x2400:
				return LNK_COND_F; // COMF, W no effect result
			case 0x2800:
				return LNK_COND_W | LNK_COND_F; // ANDF
			case 0x2C00:
				return LNK_COND_W | LNK_COND_F; // IORF
			case 0x3000:
				return LNK_COND_F; // DCF
			case 0x3400:
				return LNK_COND_F; // DCSZ
			case 0x3800:
				return LNK_COND_F; // INF
			case 0x3c00:
				return LNK_COND_F; // INSZ
			case 0x4000:
				return LNK_COND_F; // INSUZ
			case 0x4400:
				return LNK_COND_F; // DCSUZ
			case 0x4800:
				return LNK_COND_F; // RLF
			case 0x4C00:
				return LNK_COND_F; // RRF
			case 0x5000:
				return LNK_COND_F | LNK_COND_C; // RRFC
			case 0x5400:
				return LNK_COND_F | LNK_COND_C; // RLFC
			case 0x5800:
				return LNK_COND_F | LNK_COND_C; // ARLC
			case 0x5C00:
				return LNK_COND_F; // ARRC, sign bit extend
			case 0x6000:
				return LNK_COND_F; // SWPF
			case 0x6400: // MVFW/MVWF
				return (opcode16 & 0x200) ? LNK_COND_W : LNK_COND_F;
			case 0x6800:
				return LNK_COND_W | LNK_COND_F; // CPSG, 6axx is CPSL
			case 0x6c00:
				if ((opcode16 & 0xfe00) == 0x6e00) // TFSZ
					return LNK_COND_F; // TFSZ use only F
				return LNK_COND_W | LNK_COND_F; // CPSE
			case 0x7000:
			case 0x7400:
				switch (opcode16 & 0xff00)
				{
				case 0x7000: return LNK_COND_C; // JC
				case 0x7100: return LNK_COND_Z; // JZ
				case 0x7200: return LNK_COND_N; // JN
				case 0x7300: return LNK_COND_OV; // JOV
				case 0x7400: return LNK_COND_C; // JNC
				case 0x7500: return LNK_COND_Z; // JNZ
				case 0x7600: return LNK_COND_N; // JNN
				case 0x7700: return LNK_COND_OV; // JNO
				}
				return 0;
			case 0x7800:
			case 0x7C00:	// RJ
				return 0;
			default:
				switch (opcode16 & 0xf000)
				{
				case 0x8000: return LNK_COND_F; // BCF
				case 0x9000: return LNK_COND_F; // BSF
				case 0xa000: return LNK_COND_F; // BTSZ
				case 0xb000: return LNK_COND_F; // BTSS
					

				case 0xc000:  // CALL  call pushl mvss/mvsf addfsr addulnk
					// C0: long call, 2 words
					// C2: long jump, 2 words// RJ is 78
					// C4: addfsr, addulnk
					// C5: PUSHL
					// C6: MVSF,MVSS
					// C7: reserved
					// C8~CF: near call +/- 1K word range
					// 
					if ((opcode16 & 0xf800) == 0xc800)
						return 0; // near CALL
					if ((opcode16 & 0xfe00) == 0xc000)
						return 0; // long CALL
					if ((opcode16 & 0xfe00) == 0xc200)
						return 0; // long jump
					if ((opcode16 & 0xff00) == 0xc400)// addfsr and addulnk
					{
						if ((opcode16 & 0xffc0) == 0xc4c0) // ulnk
						{
							return LNK_COND_FSR | LNK_COND_RET;
						}
						return LNK_COND_FSR; // addfsr
					}
					if ((opcode16 & 0xff00) == 0xc500) // PUSHL
					{
						return LNK_COND_FSR; // PUSHL no distroy W
					}
					if ((opcode16 & 0xff00) == 0xc600) // MVSF/MVSS
					{
						return LNK_COND_F;
					}
					// C7 is reserved, c8 processed

					return 0;

				case 0xD000: return LNK_COND_F; // MVFF
				case 0xe000: return LNK_COND_F; // BTGF
				case 0xf000: return 0; // NOGF
				}
			}
		}
	}
	return 0;
}
static int opCodeOutCond(int opcode16)
{
	if ((opcode16 & 0xfff0) == 0x0000) // group 000
	{
		switch (opcode16 & 0x0f)
		{
		case 0x0000: return 0; // NOP
		case 0x0001: return 0; // IDLE
		case 0x0002: return 0; // SLP
		case 0x0003: return 0; // CWDT
		case 0x0004: return LNK_COND_W | LNK_COND_DC | LNK_COND_C; // DAW, same
		case 0x0005: return 0; // unknown, brk?
		case 0x0006: return LNK_COND_TBLR; // TBLR xx
		case 0x0007: return LNK_COND_TBLR; // TBLR xx
		case 0x0008:
		case 0x0009: return LNK_COND_RET; // RETI
		case 0x000A:
		case 0x000B: return LNK_COND_RET; // RET, same
		case 0x000c:  // LDPR, out cond only, FSR0~2
		case 0x000d:
		case 0x000e:
		case 0x000f: return LNK_COND_FSR; // 000F is reserved LDPR2 ?
		default: return 0;
		}
	}
	switch (opcode16 & 0xfff0)
	{
	case 0x0010: return 0; // LBSR
	case 0x0020: return 0; // PUSH,POP, 0030~0x00ff no use
	default:
		if ((opcode16 & 0xff00) == 0)
			return 0;
		switch (opcode16 & 0xff00)
		{
		case 0x0100: return LNK_COND_W|LNK_COND_NZ; // ANDL
		case 0x0200: return LNK_COND_W|LNK_COND_NZ; // IORL
		case 0x0300: return LNK_COND_W|LNK_COND_NZ; // XORL
		case 0x0400: return LNK_COND_W|LNK_COND_CDCNOVZ; // ADDL
		case 0x0500: return LNK_COND_RET|LNK_COND_W; // RETLW, outcond W
		case 0x0600: return LNK_COND_W; // MVL, outcond w
		case 0x0700: return LNK_COND_F; // MULL, assume F, process later
		case 0x0800: return LNK_COND_W|LNK_COND_CDCNOVZ; // SUBL
		case 0x0900: return LNK_COND_TBLR; // MVLP
		case 0x0a00:
		case 0x0b00: return LNK_COND_F; // SETF
		case 0x0c00:
		case 0x0d00: return LNK_COND_F; // CLRF
		case 0x0e00:
		case 0x0f00:return LNK_COND_F ; // to PRODLH
		default:
			switch (opcode16 & 0xfc00)
			{
			case 0x1000:
				return ((opcode16&0x200)?LNK_COND_F:LNK_COND_W)|LNK_COND_CDCNOVZ ; // ADDF
			case 0x1400:
				return ((opcode16 & 0x200) ? LNK_COND_F : LNK_COND_W) | LNK_COND_CDCNOVZ; // ADDC
			case 0x1800:
				return ((opcode16 & 0x200) ? LNK_COND_F : LNK_COND_W) | LNK_COND_CDCNOVZ; // SUBF
			case 0x1C00:
				return ((opcode16 & 0x200) ? LNK_COND_F : LNK_COND_W) | LNK_COND_CDCNOVZ; // SUBC
			case 0x2000:
				return ((opcode16 & 0x200) ? LNK_COND_F : LNK_COND_W) | LNK_COND_NZ; // XORF
			case 0x2400:
				return ((opcode16 & 0x200) ? LNK_COND_F : LNK_COND_W) | LNK_COND_NZ; // COMF, W no effect result
			case 0x2800:
				return ((opcode16 & 0x200) ? LNK_COND_F : LNK_COND_W) | LNK_COND_NZ; // ANDF
			case 0x2C00:
				return ((opcode16 & 0x200) ? LNK_COND_F : LNK_COND_W) | LNK_COND_NZ;; // IORF
			case 0x3000:
				return ((opcode16 & 0x200) ? LNK_COND_F : LNK_COND_W) | LNK_COND_CDCNOVZ; // DCF
			case 0x3400:
				return ((opcode16 & 0x200) ? LNK_COND_F|LNK_COND_BRANCH : LNK_COND_W | LNK_COND_BRANCH); // DCSZ
			case 0x3800:
				return ((opcode16 & 0x200) ? LNK_COND_F : LNK_COND_W) | LNK_COND_CDCNOVZ; // INF
			case 0x3c00:
				return ((opcode16 & 0x200) ? LNK_COND_F | LNK_COND_BRANCH : LNK_COND_W | LNK_COND_BRANCH) ; // INSZ
			case 0x4000:
				return ((opcode16 & 0x200) ? LNK_COND_F | LNK_COND_BRANCH : LNK_COND_W | LNK_COND_BRANCH); // INSUZ
			case 0x4400:
				return ((opcode16 & 0x200) ? LNK_COND_F | LNK_COND_BRANCH : LNK_COND_W | LNK_COND_BRANCH); // DCSUZ
			case 0x4800:
				return ((opcode16 & 0x200) ? LNK_COND_F : LNK_COND_W)| LNK_COND_NZ; // RLF
			case 0x4C00:
				return ((opcode16 & 0x200) ? LNK_COND_F : LNK_COND_W)| LNK_COND_NZ; // RRF
			case 0x5000:
				return ((opcode16 & 0x200) ? LNK_COND_F : LNK_COND_W)| LNK_COND_CNZ; // RRFC
			case 0x5400:
				return ((opcode16 & 0x200) ? LNK_COND_F : LNK_COND_W)| LNK_COND_CNZ; // RLFC
			case 0x5800:
				return ((opcode16 & 0x200) ? LNK_COND_F : LNK_COND_W)| LNK_COND_CNOVZ; // ARLC
			case 0x5C00:
				return ((opcode16 & 0x200) ? LNK_COND_F : LNK_COND_W)| LNK_COND_CNZ; // ARRC, sign bit extend
			case 0x6000:
				return ((opcode16 & 0x200) ? LNK_COND_F : LNK_COND_W); // SWPF
			case 0x6400: // MVFW/MVWF 
				return (opcode16 & 0x200) ? LNK_COND_F : LNK_COND_W;
			case 0x6800:
				return  LNK_COND_BRANCH; // CPSG, 6axx is CPSL
			case 0x6c00:
				return  LNK_COND_BRANCH; // TFSZ, CPSE
			case 0x7000:
			case 0x7400:
				/*
				case 0x7000: return LNK_COND_C; // JC
				case 0x7100: return LNK_COND_Z; // JZ
				case 0x7200: return LNK_COND_N; // JN
				case 0x7300: return LNK_COND_OV; // JOV
				case 0x7400: return LNK_COND_C; // JNC
				case 0x7500: return LNK_COND_Z; // JNZ
				case 0x7600: return LNK_COND_N; // JNN
				case 0x7700: return LNK_COND_OV; // JNO
				*/
				return  LNK_COND_BRANCH;
			case 0x7800:
			case 0x7C00:	// RJ
				return 0;
			default:
				switch (opcode16 & 0xf000)
				{
				case 0x8000: return LNK_COND_F; // BCF
				case 0x9000: return LNK_COND_F; // BSF
				case 0xa000: return LNK_COND_BRANCH; // BTSZ
				case 0xb000: return LNK_COND_BRANCH; // BTSS
				case 0xc000: 
					// C0: long call, 2 words
// C2: long jump, 2 words// RJ is 78
// C4: addfsr, addulnk
// C5: PUSHL
// C6: MVSF,MVSS
// C7: reserved
// C8~CF: near call +/- 1K word range
// 
					if ((opcode16 & 0xf800) == 0xc800 || (opcode16 & 0xfe00) == 0xc000) // CALL far or short
					{
						return LNK_COND_CALL; // if CALL, W will be used, but STKXX will be destroyed
					}
					if ((opcode16 & 0xfe00) == 0xc200)
						return 0; // long jump
					if ((opcode16 & 0xff00) == 0xc400)// addfsr and addulnk
					{
						if ((opcode16 & 0xffc0) == 0xc4c0) // ulnk
						{
							return LNK_COND_FSR | LNK_COND_RET;
						}
						return LNK_COND_FSR; // addfsr
					}
					if ((opcode16 & 0xff00) == 0xc500) // PUSHL
					{
						return LNK_COND_FSR; // PUSHL no distroy W
					}
					if ((opcode16 & 0xff00) == 0xc600) // MVSF/MVSS
					{
						return LNK_COND_F;
					}
					// C7 is reserved, c8 processed

					return 0;

				case 0xD000: return LNK_COND_F; // MVFF
				case 0xe000: return LNK_COND_F; // BTGF
				case 0xf000: return 0; // NOGF
				}
			}
		}
	}
	return 0;
}



int ifExpUsesFSR2(expression *exp, moduleLD *mdp)
{
	char *symn = findSymInExp(exp);
	symbol *symp;
	if (symn == NULL)
	{
		if (exp->type == ExpIsStackOffset )
			return 1;
		return 0;
	}
	symp = findSymInModuleLD(symn, mdp, 1, 1);
	// external symbol will not use FSR2!!
	// only local ones
	if (symp->symIsByFSR2)
		return 1;
	// another kind is @FSR2-XX
	

	return 0;
}
// find the branch include the exp in the exp root
static expression *findBranchHasExp(expression *root, expression *sym)
{
	expression *result;
	if (root->type == ExpIsTree)
	{
		if (root->exp.tree.left == sym || root->exp.tree.right == sym)
			return root;
		if ((result = findBranchHasExp(root->exp.tree.left, sym)) != NULL)
			return result;
		return findBranchHasExp(root->exp.tree.right, sym);
	}
	return NULL;
}
// we shall calculate what banks are used
static void calculateBSR(int *bankinglist) // after RAM address is fixed, we can define BSR
{
	wData *wdp;
	expression *exp;
	//wData *wdpArr[MAXPOSSIBLEFLOWN];
	//moduleLD *mdp;
	area *ap;
	int addr;
#ifdef DEBUGBSR
	char buf[1024];
#endif

	// phase1 build forward flow
	for (ap = topMap.romArea; ap; ap = ap->next)
	{
		for (wdp = ap->wDatas; wdp && wdp != ap->wDataTail; wdp = wdp->next)
		{
			if (!wdp->instHighByte)
				continue;
			if (!wdp->bsrRelated)
				continue;
#ifdef DEBUGBSR
			printf("check wdp of %s ", wdp->locTree);
#endif
			//exp = wdp->next->exp;
			//if (exp->type == ExpIsTree && exp->exp.tree.op == '&')
				//exp = exp->exp.tree.left;
			//if (wdp->lineno == 216)
				//fprintf(stderr, "216");
#ifdef _DEBUG
			//if (strstr(wdp->locTree, "adc.asm:38"))
			//{
			//	char buffer[1024];
			//	exprDump(wdp->exp, buffer, 1023);
			//	fprintf(stderr, "get %s \n", buffer);
			//}
#endif
			exp = getRAMSymInExp(wdp->exp, wdp->m);
			if (exp == NULL)// special operation
			{
				int highbyte = getExpMinValue(wdp->exp);
				int lowbyte;
				if (highbyte & 1)
					wdp->bankVal = -1;
				else
				{
					exp = wdp->next->exp;
					lowbyte = getExpMinValue(exp);
					if (lowbyte > 0x7f)
					{
						wdp->bankVal = 0;
						bankinglist[wdp->bankVal]++;
					}
					else
						wdp->bankVal = -1;
				}
				continue;
			}
			if (wdp->m == NULL )
			{
				wdp->bankVal = -1; // don't care
#ifdef DEBUGBSR
				printf("at %s m is null, give bank -1 \n", wdp->locTree);
#endif

			}
			
			else if (ifExpUsesFSR2(exp, wdp->m))
			{
				wdp->bankVal = -1;
#ifdef DEBUGBSR
				printf("exp %s (left)use fsr2, give bank -1 \n", exprDump(wdp->exp, buf, 1023));
#endif

			}
			else
			{
				// consider ADDF (SYM+N)
				// the sym value will need add the shift
				expression *upper = findBranchHasExp(wdp->exp, exp);
				expression *nextexp=NULL;
#ifdef _DEBUG

				//if (strstr(wdp->locTree, "testb1.asm:46"))
				//{
				//	char buff3[256];
				//	char buff1[256], buff2[256];
				//	char buff4[256];
				//	exprDump(wdp->exp, buff1, 255);
				//	exprDump(exp, buff2, 255);
				//	exprDump(upper, buff3, 256);
				//	exprDump(wdp->next->exp, buff4, 256);
				//	fprintf(stderr, "line38\n");
				//}
#endif

				if (upper && (upper->exp.tree.op == '+' || upper->exp.tree.op == '-' ||
					(upper->exp.tree.op=='A' && upper->exp.tree.right->type==ExpIsNumber && 
						upper->exp.tree.right->exp.number==3))) // only if >>3
					exp = upper; // it must be + or -, we use the upper expression
				addr = getExpValue(exp, wdp->m, 0, 0, 0, 0, wdp->fp, wdp);

				// 2021 fix bitfield bug, be careful and it is tricky
				// pure assembly no use H08D mode !! leading bitfield only for C!!
				if (wdp->next)
					nextexp = wdp->next->exp;
				if (nextexp && nextexp->type == ExpIsTree && !(exp->type==ExpIsSymbol && !strncmp(exp->exp.symbol,"bitfield",8))) // A is write shift
				{
					int needshift = 0;
					if (nextexp->exp.tree.op == 'A' && nextexp->exp.tree.right->type == ExpIsNumber &&
						nextexp->exp.tree.right->exp.number > 0)
						needshift = nextexp->exp.tree.right->exp.number;
					else if (nextexp->exp.tree.op == '&' && nextexp->exp.tree.left->type == ExpIsTree)
					{
						nextexp = nextexp->exp.tree.left; // check the left // we check 2 levels
						if (nextexp->exp.tree.op == 'A' && nextexp->exp.tree.right->type == ExpIsNumber &&
							nextexp->exp.tree.right->exp.number > 0)
							needshift = nextexp->exp.tree.right->exp.number;
					}
					addr >>= needshift;
				}


#ifdef DEBUGBSR
				printf("get exp %s left value %X ", exprDump(wdp->exp, buf, 1023), addr);
#endif
				//wdp->bankVal = addr >> 8;
				if (ramModel == RAM_MODEL_FLATA)
				{
					if (addr > 0xff)
					{
						wdp->bankVal = addr >> 8;
						bankinglist[wdp->bankVal]++;
					}
					else
						wdp->bankVal = -1;
#ifdef DEBUGBSR
					printf("set bank %d\n", wdp->bankVal);
#endif
					// for H08A, !=0 is bank
				}
				else if (ramModel == RAM_MODEL_FLATD)
				{
					if (addr >= 0x80) // for modelD, only 0x80 need bank!!
					{
						wdp->bankVal = addr >> 8;
						bankinglist[wdp->bankVal]++;
					}else
						wdp->bankVal = -1;

				}
			}
		}
	}
}

int bsrCheck(int bsrv, flowList *rev, int level)
{
	// we need to see if the bank is different of its flowprev
	wData *wdp;
	if (rev == NULL) // no prev flow
		return 1;
	while (rev)
	{
		wdp = rev->cpuNext;
		if (wdp == NULL) // unknown source, call by fptr
			return 1;
		if (bsrv == wdp->bankFlow - 100)
		{
			rev = rev->next;
			continue;
		}
		return 1;
	}
	return 0;
}


//int bsrCheck(int bsrv, flowList *rev, int level) // return 1 means need LBSR instruction!!
//{
//static flowList * history[65536]; // I think 64k is ok
//	int i;
//	for (i = 0; i < level; i++)
//		if (history[i] == rev)
//			return 0; // again, it is loop
//	if (rev == NULL) // no-previous one, we need to set bsr
//	{
//		// special case is wdp 0xfxxx, need auto backword
//#ifdef DEBUGBSR
//		printf("no prev, give bsr\n ");
//#endif
//
//		return 1;
//	}
//	history[i] = rev;
//	while (rev)
//	{
//		wData *wdp = rev->cpuNext;
//
//	
//
//#ifdef DEBUGBSR
//		printf("now check %s \n", wdp->locTree);
//#endif
//		if (wdp->bsrRelated && wdp->bankVal >= 0)
//		{
//			wdp->bankFlow = 100 + wdp->bankVal;
//			if (wdp->bankVal == bsrv)
//			{
//#ifdef DEBUGBSR
//				printf("same-bsr %d\n", bsrv);
//#endif
//				rev = rev->next;
//				continue;
//			}
//#ifdef DEBUGBSR
//			printf("different-bsr %d..%d\n", bsrv,wdp->bankVal);
//#endif
//			return 1; // different, we need to set new one!!!
//		} // if not BSR related, we trace backward
//		// depth first
//		//if (wdp && wdp->flowPrev == NULL)
//		//{
//		//	// special case, auto backward
//		//	if (wdp->exp && (getExpMinValue(wdp->exp) & 0xf0) == 0xf0)
//		//	{
//		//		wdp = findPrevWdp(wdp);
//		//		wdp = findPrevWdp(wdp);
//
//		//	}
//		//}
//		if (bsrCheck(bsrv, wdp->flowPrev, level + 1))
//		{
//#ifdef DEBUGBSR
//			printf("%s give bsr\n", wdp->locTree);
//#endif
//			return 1;
//		}
//		rev = rev->next;
//	}
//#ifdef DEBUGBSR
//	printf("backword paths checked no bsr needed.\n");
//#endif
//	return 0;
//	
//}

// when build flow, we need check if a function touched BSR. If a function modifed bsr, it should be in the tree.
// if the function not in the BSR, the tree should be skipped
// before this function, calltree must be built, bankVal should be set correctly
int CheckFuncModifyBSR(funcRecord *fp, int level)
{
	wData *wdp;
	//funcRecord *fpCalled;
	int i;
	if (fp == NULL)
		return 1; // assume modified
	if (level > 8)
	{
		fp->BSR_State = 1;
		return 1; // too many levels, assume modified
	}
	if (fp->BSR_State != 0) // it has been checked if not zero
		return fp->BSR_State;

	if (fp->doCallFPTR)
	{
		fp->BSR_State = 1;
		return 1;
	}
	for (i = 0; i < fp->calledFuncNum; i = i + 1)
	{
		if (fp->calledFunc[i]!=fp && CheckFuncModifyBSR(fp->calledFunc[i], level + 1) == 1)
		{
			fp->BSR_State = 1; // it will modify bsr
			return 1;
		}
	}

	// after tree is built, we check if the function called modifies BSR

	for (wdp = fp->firstWD; wdp && wdp != fp->finalWD->next; wdp = wdp->next)
	{
		if (wdp->instHighByte && wdp->bsrRelated && wdp->bankVal >= 0)
		{
			fp->BSR_State = 1; // it will modify bsr
			return 1;
		}
	}
	fp->BSR_State = 2;
	return 2;
}

static void simplifyRETinFlow(void) // if call a function no use BSR, the analysis may pass
{
	// the only thing is to check if function modifies BSR
	moduleLD *mdp;
	funcRecord *fp;
	area *ap;
	wData *wdp;
	int i;
	for (mdp = topMap.linkedModules; mdp; mdp = mdp->next)
	{
		for (i = 0; i < mdp->funcNum; i++)
		{
			fp = mdp->funcs[i];

			CheckFuncModifyBSR(fp, 0);
		}
	}
	for (ap = topMap.romArea; ap; ap = ap->next)
	{
		for (wdp = ap->wDatas; wdp && wdp != ap->wDataTail; wdp = wdp->next)
		{
			flowList *flp;
			if (!wdp->instHighByte)
				continue;
			//if(strstr(wdp->locTree,":98"))
			//	fprintf(stderr,"line98\n");				
			for (flp = wdp->flowPrev; flp; flp = flp->next)
			{
				if (flp->isBackFromCall && flp->fp->BSR_State == 2) // no use BSR
				{
					// change the entry to prev prev
					wData *wdp2 = findPrevWdp(wdp);
					while (wdp2 && !wdp2->instHighByte)
						wdp2 = findPrevWdp(wdp2);
					flp->cpuNext = wdp2;
				}
			}
		}
	}
}

// we need set bankflow of all instructions
// to see if it has fixed or not fixed bank
static int instrBankFlow(wData *wdp, int level)
{
	int bankget = 0; // undetermined
	int newBank;
	flowList *flp;
	static int backFromLoop = 0;
	wData *lastFlowPrevWDP;
//	wData *wdp2;
	

	if (!wdp || level > 8192) // no more than 8192!!!
		return 99; // it is unknown!!
	if (wdp->bankFlow) // already calculated
		return wdp->bankFlow;

	if (wdp->bsrRelated && wdp->bankVal >= 0)
	{
		wdp->bankFlow = wdp->bankVal + 100;
		return wdp->bankVal + 100;
	}

	
	for (flp = wdp->flowPrev, lastFlowPrevWDP=NULL; flp; flp = flp->next)
	{
		if (flp->cpuNext == lastFlowPrevWDP)// special special case
			continue;
		lastFlowPrevWDP = flp->cpuNext;
		backFromLoop = 0;
		if (flp->analyzing)
		{
			flp->fromLoop = 1;
			flp->analyzing = 0;
		}
		
		if (flp->fromLoop)
		{
			if (flp->next)
			{
				continue;
			}
			backFromLoop = 1;
			return bankget;
		}
		flp->analyzing = 1;
#ifdef _DEBUG
		/* if(strstr(wdp->locTree,",_fsadd_08D.asm:79"))
		 {
		 	fprintf(stderr,"now special line\n");
		 }
		 fprintf(stderr,"check from %s to %s, level %d\n", wdp->locTree, flp->cpuNext->locTree, level);*/
#endif
		newBank = instrBankFlow(flp->cpuNext, level+1);
#ifdef _DEBUG
		 //fprintf(stderr,"wdp %s done.\n", wdp->locTree);
#endif	
		flp->analyzing = 0;
		if (bankget == 0)
			bankget = newBank;
		else if (bankget != newBank)
		{
			wdp->bankFlow = 99;
			return 99; // 1 more posibility
		}
		if (flp->fromLoop)
			backFromLoop = 0;
		
	}
	if (bankget == 0 && !backFromLoop) // unknown
	{
		wdp->bankFlow = 98;
		return 98;
	}
	wdp->bankFlow = bankget;
	return bankget;

}
void constructBankFlow(void)
{
	area *ap;
	wData *wdp;
	for (ap = topMap.romArea; ap; ap = ap->next)
	{
		for (wdp = ap->wDatas; wdp && wdp != ap->wDataTail; wdp = wdp->next)
		{
			if (!wdp->instHighByte)
				continue;
#ifdef _DEBUG
			//if (strstr(wdp->locTree, ":109"))
			//	fprintf(stderr, "line109\n");
#endif
			if (wdp->fp && wdp->fp->BSR_State == 2) // no need
			{
				wdp->bankFlow = 98;
				continue;
			}
			instrBankFlow(wdp,0);

		}
	}
}

static void dumpfuncp(funcRecord *funcp, FILE *fp)
{
	wData *wdp;
	char buf[1024];
	if (fp == NULL)
		fp = stdout;
	for (wdp = funcp->firstWD; wdp && wdp != funcp->finalWD->next->next; wdp = wdp->next)
	{
		if (wdp->wdIsLabel)
		{
			fprintf(fp, "%s\t:%s:\n", wdp->locTree,wdp->sym->name);
		}
		else if (wdp->instHighByte)
		{
			exprDump(wdp->exp, buf, 1023);
			fprintf(fp, "%s\thbyte:%s\n",wdp->locTree, buf);
		}
		else if (wdp->wdIsInstruction)
		{
			exprDump(wdp->exp, buf, 1023);
			fprintf(fp, "%s\tlbyte:%s\n",wdp->locTree, buf);
		}
		else
		{
			fprintf(fp, "%s:xxx\n",wdp->locTree);
		}
	}
	fflush(fp);
}

int insertLBSR(void) // return how many LBSR word count inserted
{
	int i;
	area *ap;
	wData *wdp,*wdp2,*wdph,*wdphh;
	funcRecord *f;
	int insertWordCount=0;
	int banklist[16];
	int totalBanks = 0;
	wData *reorder_save=NULL;
	memset(banklist, 0, sizeof(banklist));

	calculateBSR(banklist);
	for (i = 0; i < 16; i++) // dont' count 0
		if (banklist[i])
			totalBanks++;
	// even single bank, we need modify the instructions
	if (totalBanks == 0 || ((totalBanks == 1 && banklist[2] != 0) && ramModel != RAM_MODEL_FLATA)) // if single bank, we giveup
	{
		return 0;
	}

	simplifyRETinFlow(); // simplify the call/ret if no use BSR
	constructBankFlow();

	// for every bsr related instructions,

	for (ap = topMap.romArea; ap; ap = ap->next)
	{
		for (wdp = ap->wDatas; wdp && wdp != ap->wDataTail; wdp = wdp->next)
		{
			reorder_save = NULL; // in case re-ordered
			if (!wdp->fp)
				continue; // only function ones
			if (!wdp->instHighByte)
				continue;
			if (!wdp->bsrRelated)
				continue;
			if (wdp->bankVal == -1)
				continue;
			f = wdp->fp;
			// now check if all prev bsr is different from this one
#ifdef DEBUGBSR
			printf("now check bsr of %s,\n", wdp->locTree);
			if (strstr(wdp->locTree, ":58"))
				printf("%s", wdp->locTree);
#endif
			if ( bsrCheck(wdp->bankVal, wdp->flowPrev, 0))
			{
				// we insert bsr
				wData *wdp3, *wdp4=NULL, *wdp5;
				wdp2 = findPrevWdp(wdp);
				if (wdp2->wdIsLabel) // if previous is label, we insert directly!!
				{
					// regression/call1.c: special special case
					//  if (ui != k)
					//      failure++
					//  it will compile as 
					//   mvfw ui+0,
					//   xorl k0
					//   btss status,z
					//   jmp label
					//   mvfw ui+1
					//    xorl k1
					//    btsz status,z
					// label:
					//    incf failure

					// final skip need to be modified as
					//     btss status,z
					//     rj    +2
					// label:
					//     lbsr fallure2
					//     incf failure2
					// 

					wdp3 = findPrevWdp(wdp2);
					if (wdp3)
						wdp4 = findPrevWdp(wdp3);
					if (wdp4 && wdp4->instSkip)
					{
						wData *wdp5;
						wData *wdp6;
						wData *wdp7;
						wData *wdp8;
#ifdef _DEBUG
						//char buf1[256];
						//char buf2[256];
						//exprDump(wdp4->exp, buf1, 255);
						//exprDump(wdp3->exp, buf2, 255);
#endif
						// this is very annoying

						if (wdp4->exp->type == ExpIsNumber && (((wdp4->exp->exp.number & 0xf0) == 0xB0) ||
							((wdp4->exp->exp.number & 0xf0) == 0xA0))) // BTSZ BTSS change to BTSS/BTSZ + JMP
						{
							fprintf(stderr, "Info: skip instruction following label at %s, modified for LBSR insertion.\n",	wdp->locTree+1);
							wdp4->exp->exp.number ^= 0x10; // a0 <-> b0
							insertInstruction1(wdp5, wdp3, 0x78);
							insertInstruction1d(wdp6, wdp5, 0x02); wdp->noCommonCode = 1;
							insertInstruction1(wdp7, wdp2, 0); wdp7->noCommonCode = 1;wdp7->bankFlow=100+wdp->bankVal;
							insertInstruction1d(wdp8, wdp7, 0x10 | (wdp->bankVal));
							insertWordCount += 2;
						}
						else if ((getExpMinValue(wdp4->exp) & 0xFE) == 0x6c) 
						{
							// for dcsz/dcsuz/insz/insuz, it is not possible to have this kind of instruction
							// however .. it is very headache if 
							// CPSG 68 .. few use in compiler
							// CPSL 6A .. one byte comapre, no branch
							// CPSE 6C  .... only this one 
							// TFSZ 6E ...>  and this one .... 
							fprintf(stderr, "Info: skip instruction following label at %s, modified for LBSR insertion. (CPSE->XOR)\n",	wdp->locTree+1);
							// we need to deal with CPSE ... change to XORFW + JZ+2
							if (wdp4->exp->type == ExpIsNumber)
							{
								wdp4->exp->exp.number = 0x20|(wdp4->exp->exp.number&1); // XORFW!!
							}
							else if (wdp4->exp->type == ExpIsTree && wdp->exp->exp.tree.left->type==ExpIsNumber )
							{
								wdp4->exp->exp.tree.left->exp.number = 0x20;
							}
							else
							{
								fprintf(stderr, "too compress expression at %s, please contact HYCONTEK.\n", wdp4->locTree+1);
								exit(-__LINE__);
							}
							// now we need to add JZ +02 7102
							insertInstruction1(wdp5, wdp3, 0x71);
							insertInstruction1d(wdp6, wdp5, 0x02);
							insertInstruction1(wdp7, wdp2, 0); wdp->noCommonCode = 1; wdp7->noCommonCode = 1;wdp7->bankFlow=100+wdp->bankVal;
							insertInstruction1d(wdp8, wdp7, 0x10 | (wdp->bankVal));
							insertWordCount += 2;
						}else if ((getExpMinValue(wdp4->exp) & 0xFE) == 0x6e) // TFSZ ==> change to RRF(0x4d)+JZ(0x71+02)
						{
							// for dcsz/dcsuz/insz/insuz, it is not possible to have this kind of instruction
							// however .. it is very headache if 
							// CPSG 68 .. few use in compiler
							// CPSL 6A .. one byte comapre, no branch
							// CPSE 6C  .... only this one 
							// TFSZ 6E ...>  and this one ....  0 
							fprintf(stderr, "Info: skip instruction following label at %s, modified for LBSR insertion (TFSZ->RRFW+JZ).\n",wdp->locTree+1);
							// we need to deal with CPSE ... change to XORFW + JZ+2
							if (wdp4->exp->type == ExpIsNumber)
							{
								wdp4->exp->exp.number = 0x4D | (wdp4->exp->exp.number & 1); // XORFW!!
							}
							else if (wdp4->exp->type == ExpIsTree && wdp->exp->exp.tree.left->type == ExpIsNumber)
							{
								wdp4->exp->exp.tree.left->exp.number = 0x4D;
							}
							else
							{
								fprintf(stderr, "Error, too complex expression for LBSR insertion at %s, please contact HYCONTEK.\n", wdp4->locTree+1);
								exit(-__LINE__);
							}
							// now we need to add JZ +02 7102
							insertInstruction1(wdp5, wdp3, 0x71);
							insertInstruction1d(wdp6, wdp5, 0x02);
							insertInstruction1(wdp7, wdp2, 0); wdp->noCommonCode = 1; wdp7->noCommonCode = 1;wdp7->bankFlow=100+wdp->bankVal;
							insertInstruction1d(wdp8, wdp7, 0x10 | (wdp->bankVal));
							insertWordCount += 2;
						}
						else if ((getExpMinValue(wdp4->exp) & 0xFc) == 0x3c) // incfsz -> incfsnz
						{
							fprintf(stderr, "Info: skip instruction following label at %s, modified for LBSR insertion. (INCFSZ->INCFSNZ)\n", wdp->locTree + 1);
							if (wdp4->exp->type == ExpIsNumber)
							{
								wdp4->exp->exp.number = 0x40; // XORFW!!
							}
							else if (wdp4->exp->type == ExpIsTree && wdp->exp->exp.tree.left->type == ExpIsNumber)
							{
								wdp4->exp->exp.tree.left->exp.number = 0x40;
							}
							else
							{
								fprintf(stderr, "too complex expression at %s, please contact HYCONTEK.\n", wdp4->locTree + 1);
								exit(-__LINE__);
							}
							insertInstruction1(wdp5, wdp3, 0x78);
							insertInstruction1d(wdp6, wdp5, 0x02); wdp->noCommonCode = 1;
							insertInstruction1(wdp7, wdp2, 0); wdp7->noCommonCode = 1; wdp7->bankFlow = 100 + wdp->bankVal;
							insertInstruction1d(wdp8, wdp7, 0x10 | (wdp->bankVal));
							insertWordCount += 2;
						}else if ((getExpMinValue(wdp4->exp) & 0xFc) == 0x40) // incfsz -> incfsnz
						{
							fprintf(stderr, "Info: skip instruction following label at %s, modified for LBSR insertion (INCFSNZ->INCFSZ).\n", wdp->locTree + 1);
							if (wdp4->exp->type == ExpIsNumber)
							{
								wdp4->exp->exp.number = 0x3c; // XORFW!!
							}
							else if (wdp4->exp->type == ExpIsTree && wdp->exp->exp.tree.left->type == ExpIsNumber)
							{
								wdp4->exp->exp.tree.left->exp.number = 0x3c;
							}
							else
							{
								fprintf(stderr, "too complex expression at %s, please contact HYCONTEK.\n", wdp4->locTree + 1);
								exit(-__LINE__);
							}
							insertInstruction1(wdp5, wdp3, 0x78);
							insertInstruction1d(wdp6, wdp5, 0x02); wdp->noCommonCode = 1;
							insertInstruction1(wdp7, wdp2, 0); wdp7->noCommonCode = 1; wdp7->bankFlow = 100 + wdp->bankVal;
							insertInstruction1d(wdp8, wdp7, 0x10 | (wdp->bankVal));
							insertWordCount += 2;
						}
						else
						{
						wData *wdpLabel0;
						wData *wdpLabel1;
						symbol *symp0;
						symbol *symp1;
						wData *wdpAdd0; 
						//wData *wdpAdd1;
						//wData *wdpAdd2;
						wData *wdpAdd3;
						wData *wdpAdd4;
						wData *wdpAdd4a;
						wData *wdpAdd4b;
						wData *wdpAdd5;
						wData *wdpAdd6;
						wData *wdpAdd7;
						wData *wdpAddJ0;
						wData *wdpAddJ1;
						wData *wdpAddJ2;
						wData *wdpAddJ3;
						wData *funcEndNext;

						char buf0[1024];
						char buf1[1024]; 
						char buf2[1024];
						int i;
							
							if (!wdp->fp || !wdp->m || !wdp->fp->finalWD )
							{
								fprintf(stderr, "Error, not in a function/module to insert LBSR at %s\n", wdp->locTree);
								exit(-__LINE__);
							}
							// it is possible findlWD is missed
							if (!findPrevWdp(wdp->fp->finalWD))
							{
								wData *wph;
								for (wph = wdp->fp->firstWD; wph && (wph->fp == NULL || wph->fp == wdp->fp); wph = wph->next)
								{
									if (wph->instHighByte)
										wdp->fp->finalWD = wph;
								}
							}
							strncpy(buf0, wdp->locTree, 1023);
							for (i = 0; i < (int)strlen(buf0); i++)
								if (!isalnum(buf0[i]))
									buf0[i] = '_';
							strncpy(buf1, buf0, 1023);
							strncat(buf1, "bsr0", 1023);
							strncpy(buf2, buf0, 1023);
							strncat(buf2, "bsr1", 1023);
							
							wdpAdd0 = wdp->fp->finalWD;
							if (wdpAdd0->instHighByte)
								wdpAdd0 = wdpAdd0->next;
							funcEndNext = wdpAdd0->next;
							symp0 = newSymbol(buf1);
							wdp->m->localSym[wdp->m->localSymNum++] = symp0;
							if (wdp->m->localSymNum >= 32768)
							{
								fprintf(stderr, "Error,module %s too many local symbols.\n", wdp->m->name);
								exit(-__LINE__);
							}
							symp1 = newSymbol(buf2);
							wdp->m->localSym[wdp->m->localSymNum++] = symp1;
							if (wdp->m->localSymNum >= 32768)
							{
								fprintf(stderr, "Error,module %s too many local symbols.\n", wdp->m->name);
								exit(-__LINE__);
							}

							wdpLabel0 = newWDATA(wdp->ap);
							wdpLabel1 = newWDATA(wdp->ap);

							symp0->ldm = wdp->m;
							symp1->ldm = wdp->m;
							symp0->symIsLabel = 1;
							symp1->symIsLabel = 1;
							symp0->ap = wdp->ap;
							symp1->ap = wdp->ap;
							symp0->labelPos = wdpLabel0;
							symp1->labelPos = wdpLabel1;



							wdpLabel0->wdIsLabel = 1;
							wdpLabel0->sym = symp0;
							wdpLabel1->wdIsLabel = 1;
							wdpLabel1->sym = symp1;

							insertWd(wdpAdd0, wdpLabel0); 
							//wdpAdd1 = newWDATA1(wdp->ap); wdpAdd1->instHighByte = 1; wdpAdd1->wdIsInstruction = 1;
							//wdpAdd1->exp = newConstExp(0, wdp->lineno);
							//wdpAdd2 = newWDATA1(wdp->ap); wdpAdd2->wdIsInstruction = 1;
							//wdpAdd2->exp = newConstExp(0x10 | wdp->bankVal, wdp->lineno);
							wdpAdd3 = wdp->next->next; // save

							//insertWd(wdpLabel0, wdpAdd1);
							//insertWd(wdpAdd1, wdpAdd2); 

							

							// wdp2 next is wdp, so we move wdp
							wdpLabel0->next = wdp;
							wdp->prev = wdpLabel0; // 2022 dual link
							wdpAdd4 = newWDATA1(wdp->ap); // jmp long, 0xc2

							wdpAdd4->exp = newConstExp(0xc2, wdp->lineno); wdpAdd4->instHighByte = 1; wdpAdd4->wdIsInstruction = 1;
							wdpAdd5 = newWDATA1(wdp->ap); wdpAdd5->wdIsInstruction = 1;
							wdpAdd5->exp = newTreeExp(newSymbolExp(symp1->name, wdp->lineno),
								newConstExp(13, wdp->lineno), 'A', wdp->lineno); // >> 13
							wdpAdd6 = newWDATA1(wdp->ap);  wdpAdd6->instHighByte = 1; wdpAdd6->wdIsInstruction = 1;
							wdpAdd6->exp =
								newTreeExp(newTreeExp(newTreeExp(newSymbolExp(symp1->name, wdp->lineno),
									newConstExp(9, wdp->lineno), 'A', wdp->lineno),
									newConstExp(0x0f, wdp->lineno), '&', wdp->lineno),
									newConstExp(0xf0, wdp->lineno), '|', wdp->lineno);
							wdpAdd7 = newWDATA1(wdp->ap); wdpAdd7->wdIsInstruction = 1;
							wdpAdd7->exp =
								newTreeExp(newTreeExp(newSymbolExp(symp1->name, wdp->lineno),
									newConstExp(1, wdp->lineno), 'A', wdp->lineno),
									newConstExp(0xff, wdp->lineno), '&', wdp->lineno);

							wdpAdd4a = newWDATA1(wdp->ap); wdpAdd4a->wdIsInstruction = 1; wdpAdd4a->instHighByte = 1;
							wdpAdd4a->exp = newConstExp(0, wdp->lineno);

							wdpAdd4b = newWDATA1(wdp->ap); wdpAdd4a->wdIsInstruction = 1;
							wdpAdd4b->exp = newConstExp(0x10|wdp4->bankVal, wdp->lineno);

							//insertWd(wdp->next, wdpAdd4a); 
							//insertWd(wdpAdd4a, wdpAdd4b);
							//insertWd(wdpAdd4b, wdpAdd4);
							insertWd(wdp->next, wdpAdd4);
							insertWd(wdpAdd4, wdpAdd5);
							insertWd(wdpAdd5, wdpAdd6);
							insertWd(wdpAdd6, wdpAdd7);
							wdpAdd7->next = funcEndNext;
							funcEndNext->prev = wdpAdd7;


							wdpAddJ0 = newWDATA1(wdp->ap); wdpAddJ0->instHighByte = 1; wdpAddJ0->wdIsInstruction = 1;
							wdpAddJ1 = newWDATA1(wdp->ap); wdpAddJ1->wdIsInstruction = 1;
							wdpAddJ2 = newWDATA1(wdp->ap); wdpAddJ2->instHighByte = 1; wdpAddJ2->wdIsInstruction = 1;
							wdpAddJ3 = newWDATA1(wdp->ap); wdpAddJ3->wdIsInstruction = 1;

							wdpAddJ0->exp = newConstExp(0xc2, wdp->lineno);
							wdpAddJ1->exp = newTreeExp(newSymbolExp(symp0->name, wdp->lineno),
								newConstExp(13, wdp->lineno), 'A', wdp->lineno); // >> 13
							wdpAddJ2->exp =
								newTreeExp(newTreeExp(newTreeExp(newSymbolExp(symp0->name, wdp->lineno),
									newConstExp(9, wdp->lineno), 'A', wdp->lineno),
									newConstExp(0x0f, wdp->lineno), '&', wdp->lineno),
									newConstExp(0xf0, wdp->lineno), '|', wdp->lineno);
							wdpAddJ3->exp =
								newTreeExp(newTreeExp(newSymbolExp(symp0->name, wdp->lineno),
									newConstExp(1, wdp->lineno), 'A', wdp->lineno),
									newConstExp(0xff, wdp->lineno), '&', wdp->lineno);
							insertWd(wdp2, wdpAddJ0);
							insertWd(wdpAddJ0, wdpAddJ1);
							insertWd(wdpAddJ1, wdpAddJ2);
							insertWd(wdpAddJ2, wdpAddJ3);
							insertWd(wdpAddJ3, wdpLabel1);
							
							wdpLabel1->next = wdpAdd3; // change the link
							wdpAdd3->prev = wdpLabel1;

							wdp->fp->finalWD = wdpAdd6; // final one changed, insth
							//dumpfuncp(wdp->fp, NULL);

							reorder_save = wdpLabel1;
						
							
							fprintf(stderr, "Info: label following skip instruction insert bsr change to jmp+jmp at %s.\n",wdp->locTree+1);
							//exit(-__LINE__);
						}

					}
					else
					{
						insertInstruction1(wdp3, wdp2, 0);wdp3->bankFlow=100+wdp->bankVal;
						insertInstruction1d(wdp4, wdp3, 0x10 | (wdp->bankVal));
						insertWordCount += 1;
					}
				}
				else
				{
					wdph = wdp2;
					while (wdph && !wdph->instHighByte)
						wdph = findPrevWdp(wdph);
					wdphh = findPrevWdp(wdph);
					while (wdphh && !wdphh->instHighByte)
						wdphh = findPrevWdp(wdphh);
					// we need prevent skip instruction
					// and there are double-skip problem
					// I don't like skip instructions
					// though it is ... somewhat easy for simple cpu
					// but it is not used in RV32 like cpu
					// we have only possible double skip, not triple skip?

					if (wdphh && wdphh->instSkip && wdph->instSkip)
					{
						// special case
						if ((wdph->bsrRelated && wdph->bankVal >= 0 && wdph->bankVal != wdp->bankVal) ||
							(wdphh->bsrRelated && wdphh->bankVal >= 0 && wdphh->bankVal != wdp->bankVal)
							)
						{
#ifdef _DEBUG
							//char buf1[256];
							//char buf2[256];
							//char buf3[256];
							//
							////wData *wdp5;
							//exprDump(wdphh->next->exp, buf1, 255);
							//exprDump(wdph->next->exp, buf2, 255);
							//exprDump(wdp->exp, buf3, 255);
#endif
							wData *wdp6;
							wData *wdp7;
							wData *wdp8;
								wData *wdp9;
							if (wdph->modified2indf0)
							{
								
								
								
								fprintf(stderr, "Info: BITSKIP+BITSKIP+BSRRELATED instructions at %s, modified to bitskip bitskip jmp jmp lbsr bsr-related!!\n", wdp->locTree+1);
								wdp3 = findPrevWdp(wdp);
								insertInstruction1(wdp4, wdp3, 0x78);
								insertInstruction1d(wdp5, wdp4, 0x01);
								insertInstruction1(wdp6, wdp5, 0x78);
								insertInstruction1d(wdp7, wdp6, 0x02);
								insertInstruction1d(wdp8, wdp7, 0x00); wdp8->noCommonCode = 1; wdp8->bankFlow = 100 + wdp->bankVal;
								insertInstruction1d(wdp9, wdp8, 0x10|(wdp->bankVal));

								

							}else 
							{
								fprintf(stderr, "Info: skip instr's bsr is different from next instruction's bsr at %s, linker will insert JMP instructions.\n",wdp->locTree+1);

							//exit(-__LINE__);
							// wdp3 = findPrevWdp(wdphh);
							// // insert LDPR, use fsr0
							// insertInstruction1(wdp4, wdp3, 0); 
							// insertInstruction1d(wdp5, wdp4, 0x0c);
							// // WDP is high byte, the e
							// insertInstruction1d(wdp6, wdp5, 0xf0 | wdp->bankVal);
							// insertInstruction1exp(wdp7, wdp6, wdp->next->exp);
							// insertWordCount += 2;
							// wdp->bankVal = -1;
							// wdp->bsrRelated = 0;
							// wdp->modified2indf0 = 1;
							// wdp->next->exp = newConstExp(0, -1); // INDF0
							// if (wdp->exp->type == ExpIsTree && wdp->exp->exp.tree.op == '|') // A bit must be zero!!
							// {
							// 	wdp->exp = wdp->exp->exp.tree.left; // give up right part
							// }
							wdp3 = findPrevWdp(wdp);
								insertInstruction1(wdp4, wdp3, 0x78);
								insertInstruction1d(wdp5, wdp4, 0x01);
								insertInstruction1(wdp6, wdp5, 0x78);
								insertInstruction1d(wdp7, wdp6, 0x02);
								insertInstruction1d(wdp8, wdp7, 0x00); wdp8->noCommonCode = 1; wdp8->bankFlow = 100 + wdp->bankVal;
								insertInstruction1d(wdp9, wdp8, 0x10|(wdp->bankVal));
								insertWordCount += 3;
							}
								
							

							// also, it is possible that there is a label here
						}
						else
						{
							wdp3 = findPrevWdp(wdphh);
							insertInstruction1(wdp4, wdp3, 0);wdp4->bankFlow=100+wdp->bankVal;
							insertInstruction1d(wdp5, wdp4, 0x10 | (wdp->bankVal));
							insertWordCount += 1;
						}
					}
					else
					{

						if (wdph && wdph->instSkip)
						{

							if (wdph->bsrRelated && wdph->bankVal >= 0 && wdph->bankVal != wdp->bankVal)
							{
								wData *wdp6;
								wData *wdp7;
								wData *wdp8;
								wData *wdp9;
								int banknow = wdph->bankVal;
								//char buf[1024];

								//exprDump(wdp->exp, buf, 1023);


								fprintf(stderr, "Info: skip instr's bsr is different from next instruction's bsr at %s. Linker will insert JMP operation.\n",	wdp->locTree+1);
								// wdp3 = findPrevWdp(wdph);
								// // insert LDPR, use fsr0
								// insertInstruction1(wdp4, wdp3, 0);
								// insertInstruction1d(wdp5, wdp4, 0x0c);
								// // WDP is high byte, the e
								// insertInstruction1d(wdp6, wdp5, 0xf0 | wdp->bankVal);
								// insertInstruction1exp(wdp7, wdp6, wdp->next->exp);
								// insertWordCount += 2;
								// wdp->bankVal = banknow;
								// wdp->modified2indf0 = 1;
								// wdp->bsrRelated = 0;
								// wdp->next->exp = newConstExp(0, -1); // INDF0
								// // 2019 MAY: INDF0 need A bit = 0 !!
								// if (wdp->exp->type == ExpIsTree && wdp->exp->exp.tree.op == '|')
								// {
								// 	wdp->exp = wdp->exp->exp.tree.left; // give up right part
								// }
								// // check special next bsr
								// wData *wdp8 = wdp->next->next;
								// if(wdp8) wdp8=wdp8->next->next;
								// if (!wdp8)
								// 	continue;
								// if (wdp8->wdIsLabel) wdp8 = wdp8->next;
								// if ((getExpMinValue(wdp8->exp) & 0xf0) == 0xf0)
								// 	wdp8 = wdp8->next->next;
								// if (!wdp8)
								// 	continue;
								// if (wdp->instSkip && wdp8->bsrRelated && wdp8->bankVal != banknow) // if different, we switch
								// {
								// 	wData *wdp9 = findPrevWdp(wdp8);
								// 	wData *lbsrh;
								// 	wData *lbsrl;
								// 	insertInstruction1(lbsrh, wdp9, 0); lbsrh->bankFlow = 100 + wdp8->bankVal;
								// 	insertInstruction1d(lbsrl, lbsrh, 0x10 | (wdp8->bankVal));
								// 	insertWordCount += 1;
								// }
								wdp3 = findPrevWdp(wdp);
								insertInstruction1(wdp4, wdp3, 0x78);
								insertInstruction1d(wdp5, wdp4, 0x01);
								insertInstruction1(wdp6, wdp5, 0x78);
								insertInstruction1d(wdp7, wdp6, 0x02);
								insertInstruction1d(wdp8, wdp7, 0x00); wdp8->noCommonCode = 1; wdp8->bankFlow = 100 + wdp->bankVal;
								insertInstruction1d(wdp9, wdp8, 0x10|(wdp->bankVal));
								wdp->bsrRelated=1;
								insertWordCount += 3;


							
							}
							// insert before skip
							else 
							{
							wdp3 = findPrevWdp(wdph);
							
							insertInstruction1(wdp4, wdp3, 0); wdp4->bankFlow=100+wdp->bankVal;
							insertInstruction1d(wdp5, wdp4, 0x10 | (wdp->bankVal));
							insertWordCount += 1;
							}
						}
						else
						{
							insertInstruction1(wdp3, wdp2, 0); wdp3->bankFlow=100+wdp->bankVal;
							insertInstruction1d(wdp4, wdp3, 0x10 | (wdp->bankVal));
							insertWordCount += 1;
						}
					}

				}
			}
			// now we need to modify the expression
			switch (wdp->exp->type)
			{
			case ExpIsNumber: wdp->exp->exp.number |= 1; break;
			case ExpIsTree:
				if (wdp->exp->exp.tree.op == '|')
				{
					wdp->exp->exp.tree.right = newConstExp(1, wdp->exp->lineno);
					break;
				}
			default:
				wdp->exp = newTreeExp(wdp->exp, newConstExp(1, wdp->exp->lineno), '|', wdp->exp->lineno);
				break;

			}
			if (reorder_save)
				wdp = reorder_save;
		}
	}
	return insertWordCount;
}
static expression *copyExp(expression *expold)
{
	expression *nexp = newEmptyExp(expold->lineno);
	memcpy(nexp, expold, sizeof(expression));
	if (expold->type == ExpIsTree)
	{
		nexp->exp.tree.left = copyExp(expold->exp.tree.left);
		nexp->exp.tree.right = copyExp(expold->exp.tree.right);
	}
	return nexp;
}
int findMaxLocalStack(funcRecord *fp, int level, int prevSp, int *isRecursive)
{
	static funcRecord * history[100];
	int i;
	int maxStackSize = 0;
	int stackSizeNow=0,stackSize;
	for(i=0;i<level;i++)
		if (history[i] == fp)
		{
			*isRecursive = 1;
			return 0;
		}
	history[level] = fp;
	stackSizeNow = fp->localSymSize; // include para
	for (i = 0; i < fp->calledFuncNum; i++)
	{
		stackSize = findMaxLocalStack(fp->calledFunc[i], level + 1, 0, isRecursive);
		if (stackSize > maxStackSize)
			maxStackSize = stackSize;
	}
	return maxStackSize + stackSizeNow;
}

// 0 means no related, 1 means possible destroy, -1 means safe(ADDFSR2, etc), 2 means pushl
// RET 000a return 3, RETL 05XX returns 4
static int isPUSHLRelatedCode(wData *wdp) 
{
	int hb, lb;
	if (!wdp->instHighByte)
		return 0;
	hb = getExpMinValue(wdp->exp);
	lb = getExpMinValue(wdp->next->exp);
	if (hb == 0xc5) // PUSHL
		return 2;
	if (hb == 0xc4) // ADDSFR or ADDULNK
	{
		if (lb & 0x80)
			return -1;
		return 1;
	}
	if (hb == 0)
	{
		if ((lb & 0xfc) == 0x0c)
		{
			if (lb == 0x0e)// LDPR2
				return -1;
			return 1; 
		}
		// special case is 000a, we change to ADDUNLK 0

		
	}
	if (hb == 0x5)// RETL
		return 4;
	return 0;
	
}
// insert addfsr2,0 before
static int insertADDFSR20toPUSHL(wData *startp)
{
	//static wData*history[65536]; // history is node
	//static wData *localHistory[65536];
	int i;
	wData *wdpp;
	while (startp)
	{
		if (startp->pushlChecked)
			return 0;

		startp->pushlChecked = 1;
		i = isPUSHLRelatedCode(startp);
		switch(i)
		{
		case -1:
			return 0;
		case 4:
		case 2: // before PUSHL there must not be 
				// RETL
			// we check if prev is skip
			wdpp = findPrevWdp(startp);
			if(wdpp) wdpp = findPrevWdp(wdpp);
			/*if (wdpp == NULL)
			{
				fprintf(stderr,"before c5 null\n");
			}*/
			if (wdpp && wdpp->instSkip)
			{
				if (!wdpp->pushlWarned)
				{
					wdpp->pushlWarned = 1;
					fprintf(stderr, "Warning, assembly %s:%d need to insert ADDFSR2,0 but previous instruction is skip, please add it manually!!\n",
						wdpp->m->srcName, wdpp->lineno);
				}
			}
			else 
				insertADDFSR(startp,2,0);
			return  1;
		}
		if (!(startp->flowNext))
		{
			// special case, RETURN so we have no next!!
			if (isWdRET(startp,0) && useADDULNK)
			{
				insertADDFSR(startp, 3, 0);
				return 1;
			}
			return 0;
		}

		if (startp->flowNext->next)// 2 more
		{
			int count=0;
			flowList *flp = startp->flowNext;
			while (flp)
			{
				count += insertADDFSR20toPUSHL(flp->cpuNext);
				flp = flp->next;
			}
			return count;
		}
		startp = startp->flowNext->cpuNext;
	}
	return 0;
}
int LDPRADDFSRFix(void) // after flow built!!
{
	// we check if LDPR and ADDFSR following ... PUSHL first instructions
	// if so, we insert ADDFSR2,0 to PUSHL

	// we find all "possible destroy code"
	wData *wdp;
	area *ap;
	int count = 0;
	for (ap = topMap.romArea; ap; ap = ap->next)
	{
		for (wdp = ap->wDatas; wdp; wdp = wdp->next)
		{
			if (isPUSHLRelatedCode(wdp) == 1)
			{
				count +=insertADDFSR20toPUSHL(wdp->flowNext->cpuNext);
			}
		}
	}
	return count;
}

int insertNOPifMAINHEADCALL(void)
{
	if (!mainf)
		return 0;
	if (mainf->firstWD && isWdCALL(mainf->firstWD))
	{
		// insert NOP
		wData *firstprev = findPrevWdp(mainf->firstWD);
		wData *ins1, *ins2;
		funcRecord *f = mainf;
		if (!firstprev)
			return 0;
		insertInstruction1(ins1, firstprev, 0); ins1->instHighByte = 1;
		insertInstruction1(ins2, ins1, 0);
		return 1;
	}
	return 0;
}

int adjLBSRlineno(void)
{
	area *ap;
	wData *wdp;
	int n=0;
	int i,j,k;
	char buf[4096];
	for(ap=topMap.romArea; ap; ap=ap->next)
	{ 
		for (wdp = ap->wDatas; wdp && wdp->next && wdp->next->next && wdp->next->next->next && wdp != ap->wDataTail; wdp = wdp->next)
		{
			if(!((wdp->instHighByte || wdp->wdIsInstruction) && wdp->next->wdIsInstruction&& wdp->next->next->instHighByte &&
			wdp->next->next->next->wdIsInstruction))
				continue;
			//if(wdp->next->next->lineno==252)
			//	fprintf(stderr,"252\n");
			if(wdp->wValue==0 && (wdp->next->wValue&0xf0)==0x10 && wdp->lineno != 
				wdp->next->next->lineno)
			{
				//fprintf(stderr,"diff lineno\n");
				

				wdp->lineno=wdp->next->next->lineno; // modify it, also modify the loc-tree
				strncpy(buf,wdp->locTree,sizeof(buf)-1);
				for(i=strlen(buf)-1;i>0;i--)
					if(buf[i]==':')
						break;
				if(!i)
					continue;
				snprintf(buf+i+1, sizeof(buf)-2-i, "%d", wdp->lineno);
				wdp->locTree=strdup(buf); // copy it to heap
				wdp->next->locTree = strdup(buf);
				wdp->next->lineno = wdp->lineno;
				n++;
			}
			
		}
	}
	return n;
}
int h08cdOSCCN1CHK(int enhanced, int h08amode)
{
	moduleLD *mldp;
	int i;
	symbol *osccn1p = NULL;
//	funcRecord *funcp;
	int osccn1a;
	wData *wdp;
	area *ap;
	uint64 osccn1namehash = hash("_OSCCN1");
	int found = 0;
	for (mldp = topMap.linkedModules; mldp; mldp = mldp->next)
	{

		for (i = 0; i < mldp->localSymNum; i++)
			if (mldp->localSym[i]->hashid == osccn1namehash)
			{
				osccn1p = mldp->localSym[i];
				break;
			}
		if (!osccn1p || !osccn1p->symIsEQU)
			continue;
		osccn1a = getSymValue(osccn1p, mldp, 0, 0, 0, NULL, 0, NULL);
		if (osccn1a > 1)
			break;
	}


	//funcp = mldp->funcs[i];
	for(ap=topMap.romArea; ap; ap=ap->next)
	{ 
		for (wdp = ap->wDatas; wdp && wdp->next && wdp->next->next && wdp->next->next->next && wdp != ap->wDataTail; wdp = wdp->next)
		{
			if (!wdp->instHighByte)
				continue;
			// we check literal only
			if ((isWdMVL(wdp, 0) && isWdMVWF(wdp->next->next) &&
				getExpMinValue(wdp->next->next->next->exp) == osccn1a))
			{
				int newv = getExpMinValue(wdp->next->exp);
				if ((h08amode && ((newv & 0x80) != 0)) || (!h08amode && ((newv & 0x80) == 0)))
				{
					fprintf(stderr, "Error!! OSCCN1 set to 0x%X around %s will cause compiled code execution fail!!!\n",
						newv, wdp->locTree + 1);
					found++;
				}
			}
			if (getExpMinValue(wdp->exp) == 0x0c) // 0x0C is CLRF
				if (getExpMinValue(wdp->next->exp) == osccn1a && !h08amode)
				{
					fprintf(stderr, "Error!! OSCCN1 set to 0 around %s will cause compiled code execution fail!!!\n",
						wdp->locTree + 1);
					found++;
				}

			if (getExpMinValue(wdp->exp) == 0x0a
				&& getExpMinValue(wdp->next->exp) == osccn1a && h08amode) // 0x0a is setf
			{
				fprintf(stderr, "Error!! OSCCN1 set to 0xff around %s will cause compiled code execution fail!!!\n",
					wdp->locTree + 1);
				found++;
			}

			if (getExpMinValue(wdp->exp) == 0x9e && h08amode) // 9e is bsf
				if (getExpMinValue(wdp->next->exp) == osccn1a)
				{
					fprintf(stderr, "Error!! CCOPT set to 1 around %s will cause compiled code execution fail!!!\n",
						wdp->locTree + 1);
					found++;
				}

			if (getExpMinValue(wdp->exp) == 0x8e && !h08amode) // 8e is bcf
				if (getExpMinValue(wdp->next->exp) == osccn1a)
				{
					fprintf(stderr, "Error !! CCOPT set to 0 around %s will cause compiled code execution fail!!!\n",
						wdp->locTree + 1);
					found++;
				}
			if (getExpMinValue(wdp->exp) == 0x2a && !h08amode) // 2a is andwf
				if (getExpMinValue(wdp->next->exp) == osccn1a)
				{
					wData *p0, *p1;
					p0 = findPrevWdp(wdp);
					p1 = findPrevWdp(p0);
					if (isWdMVL(p1, 0) && (getExpMinValue(p0->exp) & 0x80) == 0)
					{
						fprintf(stderr, "Error !! CCOPT set to 0 around %s will cause compiled code execution fail!!!\n",
							wdp->locTree + 1);
						found++;

					}
					else
					{
						fprintf(stderr, "Warning!! ANDWF OSCCN1 around %s may cause compiled code execution fail if clear CCOPT bit to 0, please check carefully!!!\n",
							wdp->locTree + 1);
					}
				}

			if (getExpMinValue(wdp->exp) == 0x2e && h08amode) // 2e is IORF
				if (getExpMinValue(wdp->next->exp) == osccn1a)
				{
					wData *p0, *p1;
					p0 = findPrevWdp(wdp);
					p1 = findPrevWdp(p0);
					if (isWdMVL(p1, 0) && (getExpMinValue(p0->exp) & 0x80) == 0x80)
					{
						fprintf(stderr, "Error !! CCOPT set to 1 around %s will cause compiled code execution fail!!!\n",
							wdp->locTree + 1);
						found++;

					}
					else
					{

						fprintf(stderr, "Warning!! IORF OSCCN1 around %s may cause compiled code execution fail if clear CCOPT bit to 1, please check carefully!!!\n",
							wdp->locTree + 1);
					}
					
				}


		}
	}
	return found;
}


void appendCDB2SYM(char *cdbrec)
{
	int i;
	char *dollar;
	// it must be export or reference sym, by nowMLD
	char cdb[1024]; // it should not exceed it
	strncpy(cdb, cdbrec, 1023);
	dollar = strchr(cdb + 4, '$');
	*dollar = '\0';
	for (i = nowMLD->exportSymNum-1; i>=0; i--)
	{
		if (!strcmp(nowMLD->exportSym[i]->name + 1, cdb + 4))
		{
			nowMLD->exportSym[i]->cdbRecord = strdup(cdbrec); // give it, check when needed!!
			return;
		}
	}
	for (i = nowMLD->refSymNum-1; i>=0 ; i--)
	{
		if (!strcmp(nowMLD->refSym[i]->name + 1, cdb + 4))
		{
			nowMLD->refSym[i]->cdbRecord = strdup(cdbrec);
			return;
		}
	}
	
	
	return;

}

void appendCDB2SYMn(char *cdbrec, int needpara)
{
	int i;
	char *dollar;
	// it must be export or reference sym, by nowMLD
	char cdb[1024]; // it should not exceed it
	strncpy(cdb, cdbrec, 1023);
	dollar = strchr(cdb + 4, '$');
	*dollar = '\0';
	// only for ref sym
	for (i = nowMLD->refSymNum - 1; i >= 0; i--)
	{
		if (!strcmp(nowMLD->refSym[i]->name + 1, cdb + 4))
		{
			nowMLD->refSym[i]->cdbRecord = strdup(cdbrec);
			nowMLD->refSym[i]->funcParaNum = needpara + 1024; // we check with 1k shift
			return;
		}
	}

}

// if wdp is a part of switch/case, return the first MVL HIGHD2
// otherwise return NULL
static wData *switchCaseCheck(wData *wdp, wData **lath0, wData **lath1, wData **latl, wData **mvl2lath0)
{
	wData *wdpp;
	wData *wdpRJ0;
	if (!wdp->instHighByte) // we check high byte only
		return NULL; 
	for (wdpp = wdp; wdpp; wdpp = findPrevWdp(wdpp))
	{
		if (!wdpp->instHighByte)
			continue;
		// if RJ, continue
		if (getExpMinValue(wdpp->exp) == 0x78)
			continue;
		else
			break;
	}
	wdpRJ0 = wdpp;
	// see if 661B,  (1B) will not be modified, MVWF PCLATL
	if (getExpMinValue(wdpp->exp) != 0x66 || getExpMinValue(wdpp->next->exp) != 0x1b)
		return NULL;
	*latl = wdpp;
	wdpp = findPrevWdp(findPrevWdp(wdpp));
	// if INF PCLATH
	if (getExpMinValue(wdpp->exp) != 0x3A || getExpMinValue(wdpp->next->exp) != 0x1a)
		return NULL;
	*lath1 = wdpp;
	wdpp = findPrevWdp(findPrevWdp(wdpp));
	// BTSZ status,4
	if (getExpMinValue(wdpp->exp) != 0xa8 || getExpMinValue(wdpp->next->exp) != 0x2b)
		return NULL;
	wdpp = findPrevWdp(findPrevWdp(wdpp));
	// ADDL
	if (getExpMinValue(wdpp->exp) != 0x04)
		return NULL;
	wdpp = findPrevWdp(findPrevWdp(wdpp));
	//mvfw ...
	if (getExpMinValue(wdpp->exp) != 0x64)
		return NULL;
	wdpp = findPrevWdp(findPrevWdp(wdpp));
	if (getExpMinValue(wdpp->exp) != 0x66 || getExpMinValue(wdpp->next->exp) != 0x1a)
		return NULL;
	*lath0 = wdpp;

	wdpp = findPrevWdp(findPrevWdp(wdpp));
	if (getExpMinValue(wdpp->exp) != 0x06) // it must be MVL
		return NULL;
	*mvl2lath0 = wdpp;
	return wdpRJ0;

}


int fixSwitchCaseOutOfRange(void)
{
	int i;
	int count = 0;
	wData *lath0=NULL, *lath1=NULL, *latl=NULL, *mvl2lath0;
	for (i = 0; i < foundOutOfRange; i++)
	{
		wData *wdp = OutOfRangeWdp[i];
		wData *rj0 = switchCaseCheck(wdp, &lath0, &lath1, &latl, &mvl2lath0);
		wData *wpn, *wdpnl;
		wData *labelp;
		funcRecord *f = wdp->fp;
		if (!f)
			continue;
		if (!rj0)
			continue;


		if (HY08A_getPART()->isEnhancedCore == 5)
		{
			// use BIE
			wData *mvl69h;
			wData *mvl69l;
			wData *mvfwkeyh;
			wData *mvfwkeyl;

			wData *mvlc0h;
			wData *mvlc0l;
			wData *mvfwcnh;
			wData *mvfwcnl;

			wData *mvlprevwd = findPrevWdp(mvl2lath0);
			insertInstruction1exph(mvl69h, mvlprevwd, newConstExp(0x06,mvlprevwd->lineno));
			insertInstruction1exp(mvl69l, mvl69h, newConstExp(0x69, mvlprevwd->lineno));
			insertInstruction1exph(mvfwkeyh, mvl69l, newConstExp(0x67, mvlprevwd->lineno)); // BIEKEY at 185
			insertInstruction1exp(mvfwkeyl, mvfwkeyh, newConstExp(0x85, mvlprevwd->lineno));

			insertInstruction1exph(mvlc0h, mvfwkeyl, newConstExp(0x06, mvlprevwd->lineno));
			insertInstruction1exp(mvlc0l, mvlc0h, newConstExp(0xC0, mvlprevwd->lineno)); // BIECN at 180
			insertInstruction1exph(mvfwcnh, mvlc0l, newConstExp(0x67, mvlprevwd->lineno)); 
			insertInstruction1exp(mvfwcnl, mvfwcnh, newConstExp(0x80, mvlprevwd->lineno));

			lath0->next->exp = newConstExp(0x81, lath0->lineno); // change to ARH, 181
			lath1->next->exp = newConstExp(0x81, lath1->lineno); // 
			latl->next->exp = newConstExp(0x82, latl->lineno); // change to ARL, 182

			lath0->exp = newTreeExp(lath0->exp, newConstExp(0x01, lath0->lineno), '|', lath0->lineno);
			lath1->exp = newTreeExp(lath1->exp, newConstExp(0x01, lath1->lineno), '|', lath1->lineno);
			latl->exp  = newTreeExp(latl->exp, newConstExp(0x01, latl->lineno), '|', latl->lineno);



			// add table lookup to h(1a),l(1b)
			// tblr

			//insertInstruction1exph(wpn, latl->next, newConstExp(0x00, latl->lineno));
			//insertInstruction1exp(wdpnl, wpn, newConstExp(0x06, latl->lineno)); // TBLR
			// change to RDPulse BSF BIECN,0,1  9180
			insertInstruction1exph(wpn, latl->next, newConstExp(0x91, latl->lineno));
			insertInstruction1exp(wdpnl, wpn, newConstExp(0x80, latl->lineno)); // BSF BIECN,0,1



			insertInstruction1exph(wpn, wdpnl, newConstExp(0x65, latl->lineno)); // BIEDRH, 183
			insertInstruction1exp(wdpnl, wpn, newConstExp(0x83, latl->lineno));

			insertInstruction1exph(wpn, wdpnl, newConstExp(0x66, latl->lineno)); 
			insertInstruction1exp(wdpnl, wpn, newConstExp(0x1a, latl->lineno));

			insertInstruction1exph(wpn, wdpnl, newConstExp(0x65, latl->lineno));// BIEDRL, 184
			insertInstruction1exp(wdpnl, wpn, newConstExp(0x84, latl->lineno));


			insertInstruction1exph(wpn, wdpnl, newConstExp(0x0D, latl->lineno));// CLRF BIECN
			insertInstruction1exp(wdpnl, wpn, newConstExp(0x80, latl->lineno));

			insertInstruction1exph(wpn, wdpnl, newConstExp(0x0D, latl->lineno));// CLRF BIEKEY
			insertInstruction1exp(wdpnl, wpn, newConstExp(0x85, latl->lineno));

			insertInstruction1exph(wpn, wdpnl, newConstExp(0x66, latl->lineno));
			insertInstruction1exp(wdpnl, wpn, newConstExp(0x1b, latl->lineno)); // this is the final one

		}
		else
		{
			// change lath/l to tabptrhl 0x1d, 0x1e
			lath0->next->exp = newConstExp(0x1d, lath0->lineno);
			lath1->next->exp = newConstExp(0x1d, lath1->lineno);
			latl->next->exp = newConstExp(0x1e, latl->lineno);
			// add table lookup to h(1a),l(1b)
			// tblr

			insertInstruction1exph(wpn, latl->next, newConstExp(0x00, latl->lineno));
			insertInstruction1exp(wdpnl, wpn, newConstExp(0x06, latl->lineno)); // TBLR

			insertInstruction1exph(wpn, wdpnl, newConstExp(0x64, latl->lineno));
			insertInstruction1exp(wdpnl, wpn, newConstExp(0x1f, latl->lineno));

			insertInstruction1exph(wpn, wdpnl, newConstExp(0x66, latl->lineno));
			insertInstruction1exp(wdpnl, wpn, newConstExp(0x1a, latl->lineno));

			insertInstruction1exph(wpn, wdpnl, newConstExp(0x64, latl->lineno));
			insertInstruction1exp(wdpnl, wpn, newConstExp(0x20, latl->lineno));
			insertInstruction1exph(wpn, wdpnl, newConstExp(0x66, latl->lineno));
			insertInstruction1exp(wdpnl, wpn, newConstExp(0x1b, latl->lineno)); // this is the final one
		}
		wdpnl->noCommonCode = 1; // it is not allowed!!
		wpn->noCommonCode = 1;
		labelp = wdpnl->next;
		// later is modify the RJ to table entry
		for (wpn = labelp->next; wpn; wpn = wpn->next)
		{
			expression *expLabel=NULL;
			if (!wpn->instHighByte)
				continue;
			if (getExpMinValue(wpn->exp) != 0x78)// if ! RJ --> finish
				break;
			if (wpn->exp->type == ExpIsTree)
				expLabel = wpn->exp->exp.tree.right;
			else
				break;
			if (wpn->exp->type == ExpIsTree)
				expLabel = expLabel->exp.tree.left;
			else
				break;
			if (wpn->exp->type == ExpIsTree)
				expLabel = expLabel->exp.tree.left;
			else
				break;
			if (wpn->exp->type == ExpIsTree)
				expLabel = expLabel->exp.tree.left; // left 3 times
			else
				break;
			wpn->exp = newTreeExp(expLabel, newConstExp(9, wpn->lineno), 'A', wpn->lineno);
			wpn->instHighByte = 0;
			wpn->wdIsInstruction = 0;
			wpn = wpn->next;
			wpn->exp = newTreeExp(newTreeExp(expLabel, newConstExp(1, wpn->lineno), 'A', wpn->lineno), newConstExp(255, wpn->lineno), '&', wpn->lineno);
			wpn->wdIsInstruction = 0; // no more instruction
			count++;
		}


	}
	return count;
}

int checkReplaceOtp(char *oldOtpFileName, int newCodeWordOffset)
{
	FILE *fin=fopen(oldOtpFileName, "rb") ;
	int count;
	int gsinitAddr;
	int i,j,k;
	int found=0;
	unsigned char pattern[4]; // patterns to match
	if(fin == NULL)
	{
		fprintf(stderr,"FILE %s open read Error!!\n",oldOtpFileName);
		exit(-__LINE__);
	}
	memset(oldBinFile,0xff,sizeof(oldBinFile));
	count = fread((void*)oldBinFile, 1, sizeof(oldBinFile), fin);
	fclose(fin);
	if(count==0)
	{
		fprintf(stderr,"FILE %s reads Empty Error!!\n",oldOtpFileName);
		exit(-__LINE__);
	}
	// now lets analyze the binary code
	// first is reset vector, usually jump to gsinitxxx
	if(oldBinFile[0]|oldBinFile[1])
	{
		fprintf(stderr,"Error, file %s first word nop not found, cannot proceed.\n", oldOtpFileName);
		exit(-__LINE__);
	}
	if(oldBinFile[2]!=0xc2)
	{
		fprintf(stderr,"Error, file %s first Long Jump not found, cannot proceed.\n", oldOtpFileName);
		exit(-__LINE__);
	}
	i = (oldBinFile[3]<<12)|((oldBinFile[4]&0x0f)<<8)|oldBinFile[5];
	gsinitAddr = i;
	fprintf(stderr,"info: gsinit at 0x%X(Word)\n",i);
	for(j=i*2;j<sizeof(oldBinFile)-2;j+=2)
	{
		if((oldBinFile[j]&oldBinFile[j+1])!=0xff)
		{
			oldBinFile[j] = 0x0;
			oldBinFile[j + 1] = 0x0; // NOP
		}else
			break; // keep j !!
	}
	if((j/2) >= newCodeWordOffset )
	{
		fprintf(stderr,"Error!! Old OTP code exceed expect new offset 0x%X, give up.\n",newCodeWordOffset);
		exit(-__LINE__);
	}
	pattern[0]=0xc2;
	pattern[1]=newCodeWordOffset>>12;
	pattern[2]=(newCodeWordOffset&0xf00)>>8;
	pattern[3]= newCodeWordOffset&0xff;
	// now search if interrupt can have that patterns
	for(i=8;i<gsinitAddr;i+=2)
	{
		found=1;
		for(k=0;k<4;k++)
		{
			if((oldBinFile[i+k]&pattern[k])!=pattern[k])
			{
				found=0;
				break;
			}
		}
		if(found)
			break;
	}
	if(!found)
	{
		fprintf(stderr,"Error, Linker cannot find new code interrupt address!\n");
		exit(-__LINE__);
	}
	fprintf(stderr, "info: address 0x%X(Word) %02X %02X %02X %02X can be modified as %02X %02X %02X %02X\n", i / 2,
		oldBinFile[i], oldBinFile[i + 1], oldBinFile[i + 2], oldBinFile[i + 3],
		pattern[0],pattern[1],pattern[2],pattern[3]
		);
	for(k=8;k<i;k++)
		oldBinFile[k] = 0;
	oldBinFile[k++]=pattern[0];
	oldBinFile[k++]=pattern[1];
	oldBinFile[k++]=pattern[2];
	oldBinFile[k++]=pattern[3];
	return j>>1;
}
