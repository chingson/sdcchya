#ifndef __PARSER_H__
#define __PARSER_H__



#include "mytype.h"

// use ARRAY for simple operation
// assume 640k max
#define MAXCDBREC 655360

extern char * cdbRecs[MAXCDBREC];
extern int cdbRecNum;

char *findCDBSym(char *sym);
	extern int phase2(void);
	
	extern void yyerror(const char *);

	extern module topModule;
	extern int compatibleMode;
	extern int symbolCaseSensitivity;
	extern int is08b;
	extern int is08d;
	extern int ramModel;
	// ram MODEL
#define MODELSMALL 0
#define MODELMEDIUM 1
#define MODELCLARGE 2

#define set_string_dec5(a,b) if(a==NULL) a=(char*)malloc(10); sprintf(a,"%05d",b)
#define set_string_hex2(a,b) if(a==NULL) a=(char*)malloc(10); sprintf(a,"%02X",b)
#define set_string_hex3(a,b) if(a==NULL) a=(char*)malloc(10); sprintf(a,"%03X",b)
#define set_string_hex4(a,b) if(a==NULL) a=(char*)malloc(10); sprintf(a,"%04X",b)

	MNE * newEmptyMne(void);
	dataItem * newEmptyDataItem(void);
	
	
	symbol *appendNewSymbol(area *ap, symbol *n);// include topmodule
	wData *newWDATA(area *ap);
	wData *appendTailWDATA(area *ap, wData *n, int isinsth);
	
	gloLocItem *newGloLocItem(char *name, int isGlobl);
	gloLocItem *appendGloLocToModule(gloLocItem *glp);
	int genListFileO(char* srcFileName,char *listFName, int doout, int longmode);
	int genOBJFileO(char *objFileName, char *srcFileName);
	int genSymFileO(char *symfilename);
	int failExpression(expression *, uint64 excl, expression **);
	int resolveExpression(expression *);
	int checkWDATAListExpValid(void);
	int checkEQUexpressionValid(void);
	expression *simplifyExp(expression *exp,char *locTree);
	int getExpMinValue(expression *exp);
	int constructExportList(void);
	//int div2more(expression **exp);
	//int div2Job(void);
	symbol *hashInSymlist(uint64 hashid, uint64 excludeID);
	

    // place any extra class members here
#ifndef ARRAY_SIZE
#define ARRAY_SIZE 32768
#endif

#define MAX_INC_LEVEL 16
	extern char * now_lex_fname;

	
#define MAX_MACRO_NUM 1024
#define MAX_MACROLINE_WD 128
#define MAX_MACRO_LEVEL 32
#define MAX_MACRO_TOTAL_PARA_NUM 128
#define MAX_DEFINE_REP_NUM 4096

	
	extern macrot * defined_macros[MAX_MACRO_NUM];
	extern int defined_macro_num;

	extern char * incSearchPath[128];
	extern int incSearchPathNum;

	extern void push_replaced_macro(char *buf, int addList, int bufsize);
	extern int macro_level;
 	extern int macro_left_line[MAX_MACRO_LEVEL];
	extern int macro_used_line[MAX_MACRO_LEVEL];
	extern int macroid_stack[MAX_MACRO_LEVEL];
	extern int mrep_str_num[MAX_MACRO_TOTAL_PARA_NUM];
	extern int mrep_str_local[MAX_MACRO_TOTAL_PARA_NUM]; // the number to replace in macros, stacked
	extern char mrep_src[MAX_MACRO_TOTAL_PARA_NUM][MAXSTRSIZE0];
	extern uint64 mrep_src_hash[MAX_MACRO_TOTAL_PARA_NUM];
	extern char mrep_dst[MAX_MACRO_TOTAL_PARA_NUM][MAXSTRSIZE1];
	extern int rep_str_num;
	extern char rep_src[MAX_DEFINE_REP_NUM][MAXSTRSIZE0];
	extern uint64 rep_src_hash[MAX_DEFINE_REP_NUM];
	extern char rep_dst[MAX_DEFINE_REP_NUM][MAXSTRSIZE1];
	extern char mrep_local[MAX_MACRO_TOTAL_PARA_NUM];
	extern int macroLocalCount;
	extern int keyword_defined;
	extern int include_lev;
	extern int includeissue_lineno;
	extern int genDep;
	extern char *depFname;
	extern FILE *depfp;
	extern void newline_state_change(int preChange);// some times we need to change before '\n', when label found first
    extern int appendEQU(MNE *mp, area *current_area);
	extern asmIncInfo *asmIncHead;
	extern asmIncInfo *asmIncTail;
	extern char *macroRootLoc;
	extern int rpt_id;

	extern int macro_ins_num; // macro insert record, in rel file, RPT not include
	extern char * macroInsLocTree[1024];
	extern int macroInsMacroID[1024];
	
#endif
