%option yylineno
%{
/****************************
mylexer.l
****************************/
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <stdint.h>

#include "mytype.h"
#include "asparser.h"
#include "myparser.tab.h"
//#define DEBUG
#define ECHO
#define printf2(x,y) fprintf(stderr,x,y)
#define S_INI 0
#define S_INI08B 1
#define S_INDEFINE 2
#define S_DEFINED 3
#define S_NODEFINED 4
#define S_INIFDEF 5
#define S_INIFNDEF 6
#define S_MACDEF 7
#define S_MACDEF1 8
#define S_MACPARA 9
#define S_INIFTRUE 10
#define S_INIFFALSE 11
#define S_NAMEONLY 12
#define MAX_LEX_STR_IND 1024

#define SCASECHK if(symbolCaseSensitivity) str2upper(yytext)
#define MAX_STATE_DEPTH 128
	int macroLocalCount=0;
	

	extern char  *asmPath;
	int rptFlag=0;
	int noMacroNow=0; // some situation no macro
	int strindex;
	static char strbuffer[MAX_LEX_STR_IND][MAXSTRSIZE0];
	int rep_str_num=0;
	char rep_src[MAX_DEFINE_REP_NUM][MAXSTRSIZE0]; // for define, the src is short, dest is long
	uint64 rep_src_hash[MAX_DEFINE_REP_NUM];
	char rep_dst[MAX_DEFINE_REP_NUM][MAXSTRSIZE1];
	int mrep_str_num[MAX_MACRO_TOTAL_PARA_NUM]; // macro many level
	char mrep_src[MAX_MACRO_TOTAL_PARA_NUM][MAXSTRSIZE0];
	uint64 mrep_src_hash[MAX_MACRO_TOTAL_PARA_NUM]; // use hash to compare
	char mrep_dst[MAX_MACRO_TOTAL_PARA_NUM][MAXSTRSIZE1];
	char mrep_local[MAX_MACRO_TOTAL_PARA_NUM]; // local set 1 means it is local label
	int mrep_str_local[MAX_MACRO_TOTAL_PARA_NUM];
	int evaluate(char *text); // for if true and if false
	int  opcode_lineno;
	char *opcode_fname;
	char *state_str(int state);
	char *now_state_str(void);
	int  label_lineno;
	static int  state_stack[MAX_STATE_DEPTH];
	int  now_state=0;
	int  state_lev;
	int  keyword_defined;
	FILE * yyinput_stack[MAX_INC_LEVEL];
	char * yyfile_stack[MAX_INC_LEVEL];
	int yylineno_stack[MAX_INC_LEVEL];
	char * now_lex_fname=NULL;
	int include_lev=0;
	void before_lex(void);
	void push_state(int new_state);
	void switch_state( int state);
	void pop_state(void);

	char * cdbRecs[MAXCDBREC];
	int cdbRecNum=0;

	extern symbol * getSymByName(char *);
	extern char *getLocTree(void);
	extern int macro_used_line[MAX_MACRO_LEVEL];
	extern int opcode_macro_used_line[MAX_MACRO_LEVEL];
	#define OPCODEOPER {if(strcmp(opcode_fname,now_lex_fname)) opcode_fname=strdup(now_lex_fname);\
			memcpy(opcode_macro_used_line,macro_used_line,sizeof(macro_used_line)); };opcode_lineno=yylineno
	
%}

WS	[ \t\r]+
DIGIT ([0-9])
HEXDIGIT [0-9a-fA-F]
%s INI08B
%s INDEFINE
%s DEFINED
%s NODEFINED
%s INIFDEF
%s INIFNDEF
%s INIFTRUE
%s INIFFALSE
%s MACDEF1
%s MACDEF
%s MACPARA
%s NAMEONLY
%%



{WS}		{
#ifdef DEBUG
	fprintf(stderr, "white space skipped\n");
#endif
		}

"//".*\n {  
		
		newline_state_change(0);
		return '\n';
	}

";".*\n		{


#ifdef DEBUG
	fprintf(stderr, "comments skipped: %s\n", yytext);
#endif
	if(strstr(yytext,";--cdb--"))
	{
		char *r;
		int i;
		if(cdbRecNum>=MAXCDBREC)
		{
			fprintf(stderr,"Error, too many CDB record in ASM file, please correct it.\n");
			exit(-4);
		}
		r=strdup(yytext+8);
		for(i=0;i<strlen(r);i++)
			if(r[i]=='\r' || r[i]=='\n')
				r[i]='\0';
		cdbRecs[cdbRecNum++]=r;
		
	}
	
		
		newline_state_change(0);
		return '\n';
		}


"\n"|"\r"	{
#ifdef DEBUG
		fprintf(stderr,"new line in lex a\n");
#endif
		newline_state_change(0);
		return'\n';
	}
"/*"    	{
                   int c;
#ifdef DEBUG
	fprintf( stderr,"remark type 2..\n");
#endif
		   while((c = input()) != EOF)
                       {
                       if(c == '*')
                           {
                           if((c = input()) == '/')
                               break;
                           else
                               unput(c);
                           }
                       }
			if(c == EOF )
				yyerror("comments not end at EOF");
                   }
AT {
	return '@';
	}
<INITIAL,INI08B,DEFINED>"\.define"|"\.DEFINE" {
	char buf[1024];
	int i=0,c;
	OPCODEOPER;

	push_state(S_INDEFINE);

	while(isspace(c=input())&& c!='\n' && c!=EOF);

	if(c==EOF)
	{
		fprintf(stderr,"Assembler error: unexpect EOF at line %d.\n", yylineno);
		exit(2001);
	}
	buf[i++]=c;
	while(!isspace(c=input())&& c!='\n' && c!=EOF)
		buf[i++]=c;
	unput(c);
	buf[i]='\0';
	yylval.string=strdup(buf);
#ifdef DEBUG
	fprintf(stderr, "define keyword %s %d\n",buf,yylineno);
#endif


	return DEFINE;

   }



<INITIAL,INI08B,DEFINED,NODEFINED>"\.IFDEF" {
	// even nodefined we need calculate ifdef
#ifdef DBGDEF
	fprintf(stderr, "ifdef keyword %d\n",yylineno);
#endif
		if(now_state==S_NODEFINED)
			push_state(S_NODEFINED);
		else
			push_state(S_INIFDEF);
	
	}
<INITIAL,INI08B,DEFINED,NODEFINED>"\.IFNDEF" {
#ifdef DBGDEF
	fprintf(stderr, "ifndef keyword %d\n",yylineno);
#endif
		if(now_state==S_NODEFINED)
			push_state(S_NODEFINED);
		else
			push_state(S_INIFNDEF);
	}

<INITIAL,INI08B,DEFINED,NODEFINED>"\.iftrue"|"\.if" {
#ifdef DBGDEF
	printf2( "iftrue keyword %d\n",yylineno);
#endif
		if(now_state==S_NODEFINED)
			push_state(S_NODEFINED);
		else
			push_state(S_INIFTRUE);
			// construct the expression by parser
		return IFTRUE;
	
	}
<INITIAL,INI08B,DEFINED,NODEFINED>"\.iffalse"|"\.IFFALSE" {
#ifdef DBGDEF
	printf2( "iffalse keyword %d\n",yylineno);
#endif
		if(now_state==S_NODEFINED)
			push_state(S_NODEFINED);
		else
			push_state(S_INIFFALSE);
			// construct the expression by parser
		return IFFALSE;
	}
<INITIAL,INI08B,DEFINED>"@FSR2" {
	return FSR2H;
}
<INITIAL,INI08B,DEFINED>\.include|INCLUDE {
	int i=0,c;
	FILE *newfp;
	int inc=0;
	asmIncInfo *aifp;
	char newfname[PATH_MAX+1];
	char newfname2[PATH_MAX*2+1];
	strncpy(newfname2, asmPath, PATH_MAX);
	
	while( (c=input())!=EOF)
	{
		if(c=='\"' || c=='\n')
			break;

	}
	if(c==EOF || c=='\n')
	{
		fprintf(stderr,"Assembler error: Include error 1 at %s:%d\n", now_lex_fname,yylineno);
		exit(-3);
	}
	while (( c=input())!=EOF)
	{
		if(c!='\"' && c!='\n')
			newfname[i++]=c;
		else
		{
			newfname[i++]='\0';
			break;
		}
		if(i>=4096)
		break;
	}
	if(c!='\"')
	{
		fprintf(stderr,"Assembler error: include error 2 at %s:%d\n", now_lex_fname,yylineno);
		exit(-3);
	}
#ifdef _WIN32
	if(strlen(newfname2)>0) // it is possible empty string
		strncat(newfname2,"\\", PATH_MAX*2-1);
#else
	if(strlen(newfname2)>0)
		strncat(newfname2,"/", PATH_MAX*2-1);
#endif
	strncat(newfname2,newfname, PATH_MAX*2-1);
	newfp = fopen(newfname2,"r");
	if(!newfp)
	{
		// try other paths
		for(inc=0;inc<incSearchPathNum;inc++)
		{
			strncpy(newfname2,incSearchPath[inc],PATH_MAX);
			#ifdef _WIN32
	if(strlen(newfname2)>0) // it is possible empty string
		strncat(newfname2,"\\", PATH_MAX-1);
#else
	if(strlen(newfname2)>0)
		strncat(newfname2,"/", PATH_MAX*2-1);
#endif
		strncat(newfname2,newfname, PATH_MAX*2-1);
		newfp = fopen(newfname2,"r");
		if(newfp!=NULL)
			break;
		}
	}
	if(!newfp)
	{
		fprintf(stderr,"Assembler Error: include file %s open error at %s:%d.\n", newfname, now_lex_fname,yylineno);
		exit(-4);
	}
	if(genDep)
	{
		fprintf(depfp, "%s ", newfname2); // add to dependence list
	}
	// append inc info
	aifp = newAsmIncInfo(now_lex_fname,yylineno,newfname2);
	appendAsmIncInfo(&asmIncHead,&asmIncTail,aifp);
	
//	yyinput_stack[include_lev]=yyin;
	yyin = newfp;
	yyfile_stack[include_lev]=now_lex_fname;
	yylineno_stack[include_lev]=yylineno;
	if(strlen(newfname2)>2 && newfname2[0]=='.' && (newfname2[1]=='\\' || newfname2[1]=='/'))
		now_lex_fname = strdup(newfname2+2); // skip ./
	else
		now_lex_fname = strdup(newfname2);

	if(include_lev==0)
		includeissue_lineno=yylineno; // very first include!!

	include_lev++;
	yypush_buffer_state(yy_create_buffer( yyin, YY_BUF_SIZE ));
	if(is08b)
		push_state(S_INI08B);
	else
		push_state(S_INI);
	yylineno=1;
	//fprintf(stderr,"after include yylineno is %d\n", yylineno);
}
	
<<EOF>> {
			if(now_state==S_DEFINED || now_state==S_NODEFINED)
			{
				fprintf(stderr,"Assembler Error: ifdef/endif not matched.\n");
				exit(-11);
			}
     			yypop_buffer_state();
     
                 if ( !YY_CURRENT_BUFFER )
                     {
                     yyterminate();
                     }
    include_lev--;

    now_lex_fname = yyfile_stack[include_lev];
	if(strlen(now_lex_fname)>2 && now_lex_fname[0]=='.' && (now_lex_fname[1]=='\\'||now_lex_fname[1]=='/'))
		now_lex_fname+=2;
	yylineno = yylineno_stack[include_lev];
	pop_state();
                 }


<INITIAL,INI08B,DEFINED>\.MACRO {
	
	OPCODEOPER;
	//state_stack[state_lev++]=now_state;
	//now_state=S_MACDEF1;
	//BEGIN MACDEF1;
	push_state(S_MACDEF1);
#ifdef DEBUG
			fprintf(stderr,"MACROHEAD found\n");
#endif
	return MACROHEAD;
}

<INITIAL,INI08B,DEFINED>\.REPT { // rept following expression
	
	OPCODEOPER;
	// push_state(S_MACDEF1); // no change state
#ifdef DEBUG
			fprintf(stderr,"MACROREPHEAD found\n");
#endif
	rptFlag=1; // rept following MACROSTATE
	//if(macro_level<0)
		//macroRootLoc = getLocTree();
	return MACROREPHEAD;
}

<MACDEF>\.ENDM {
	OPCODEOPER;
	pop_state();
#ifdef DEBUG
			fprintf(stderr,"MACROEND found\n");
#endif

	return MACROTAIL;
}



"\#" {
#ifdef DEBUG
			fprintf(stderr,"# found\n");
#endif
	return '#';	
	}
<INITIAL,INI08B,DEFINED>"=" {
#ifdef DEBUG
			fprintf(stderr,"= found\n");
#endif
	opcode_lineno = yylineno;
	return '=';	
	}

<INITIAL,INI08B,DEFINED>"\"" {

	int c;
	char *p=strbuffer[strindex];
	while((c=input())!=EOF)
	{
		if(c=='\\')
		{
			c=input();
			if(c==EOF)
				break;
			if(c=='\r')
			{
			    c=input();
				if(c==EOF)
				break;
			}
			if(c!='\n')
				*p++=c;
			continue;
		}
		if(c=='"')
			break;
		*p++=c;
	}
	*p++=0;
	yylval.string = strbuffer[strindex++];
	if( strindex>=MAX_LEX_STR_IND) strindex = 0;
	
#ifdef DEBUG
			fprintf(stderr,"QUOSTR found\n");
#endif
	
	return QUOSTR;	
	}
<INITIAL,INI08B,DEFINED>":" {
#ifdef DEBUG
			fprintf(stderr,": found\n");
#endif
	return ':';

}	

"\\" {int c;
	while((c=input())!=EOF && c!='\n');
}

"+" {
#ifdef DEBUG
	fprintf(stderr,"+ found ");
#endif
	return '+';
}

"-" {
#ifdef DEBUG
	fprintf(stderr,"- found ");
#endif
	return '-';
}

"(" {
#ifdef DEBUG
	fprintf(stderr,"( found ");
#endif
	return '(';
}

")" {
#ifdef DEBUG
	fprintf(stderr,") found ");
#endif
	return ')';
}

				
<INITIAL,INI08B,DEFINED,MACDEF1,MACPARA>"," {
#ifdef DEBUG
	fprintf(stderr,"COMMA found ");
#endif
	return ',';
}
"*" {
#ifdef DEBUG
	fprintf(stderr,"STAR found ");
#endif
	return '*';
}


"&" {
#ifdef DEBUG
	fprintf(stderr,"& found ");
#endif
	return '&';
}

"|" {
#ifdef DEBUG
	fprintf(stderr,"| found ");
#endif
	return '|';
}

">>" {
#ifdef DEBUG
	fprintf(stderr,">> found ");
#endif
	return SHRIGHT;

}
"<<" {
#ifdef DEBUG
	fprintf(stderr,"<< found ");
#endif
	return SHLEFT;

}


<INITIAL,INI08B,DEFINED>.module 	{ OPCODEOPER;yylval.val = 0;		
#ifdef DEBUG
	fprintf(stderr,"module found ");
#endif
push_state(S_NAMEONLY);

return MODULE;}


<INITIAL,INI08B,DEFINED>\.MACROPUSHEND {OPCODEOPER;return MACROPUSHEND;}
<INITIAL,INI08B,DEFINED>\.endfunc 		{ OPCODEOPER;yylval.val = 0;		return FUNCENDHEAD;}
<INITIAL,INI08B,DEFINED>\.func 		{ OPCODEOPER;yylval.val = 0;push_state(S_NAMEONLY);		return FUNCHEAD;}
<INITIAL,INI08B,DEFINED>\.func_pstk { OPCODEOPER;yylval.val = 1;push_state(S_NAMEONLY);		return FUNCHEAD;}
<INITIAL,INI08B,DEFINED>$C 		{ OPCODEOPER;yylval.val = 0;		return CALLMARK;}
<INITIAL,INI08B,DEFINED>$L 		{ OPCODEOPER;yylval.val = 0;		return LOCALMARK;}
<INITIAL,INI08B,DEFINED>$R 		{ OPCODEOPER;yylval.val = 0;		return RETURNSIZEMARK;}
<INITIAL,INI08B,DEFINED>$XL 		{ OPCODEOPER;yylval.val = 0;		return XLOCALMARK;}

<INITIAL,INI08B,DEFINED>\.globl 		{ OPCODEOPER;yylval.val = 0;		return GLOBL;}
<INITIAL,INI08B,DEFINED>\.local 		{ OPCODEOPER;yylval.val = 0;		return LOCAL;}
<INITIAL,INI08B,DEFINED>".area"{WS}[A-Z_0-9]+ 		{ OPCODEOPER;yylval.val = 0;		
// this is special, we need return the area name 
		strcpy(strbuffer[strindex], yytext);
		yylval.string=strbuffer[strindex++];
		if( strindex>=MAX_LEX_STR_IND) strindex = 0;
return AREA;}
<INITIAL,INI08B,DEFINED>\.ds 		{ OPCODEOPER;yylval.val = 0;		return DS;}
<INITIAL,INI08B,DEFINED>\.dw 		{ 
	OPCODEOPER;yylval.val = 0;		return DW;
	}
<INITIAL,INI08B,DEFINED>\.byte 		{ OPCODEOPER;yylval.val = 0;		return BYTEDEF;}
<INITIAL,INI08B,DEFINED>\.db|DB 		{ OPCODEOPER;yylval.val = 0;		return BYTEDEF;}
<INITIAL,INI08B,DEFINED>\.ascii 		{ OPCODEOPER;yylval.val = 0;		return ASCII;}
<INITIAL,INI08B,DEFINED>\.equ|EQU		{ OPCODEOPER;yylval.val = 0;		
#ifdef DEBUG
	fprintf(stderr,"EQU found at %d\n", yylineno);
#endif

			return EQU;}
<INITIAL,INI08B,DEFINED>\.org|"ORG"		{ OPCODEOPER;yylval.val = 0;	noMacroNow=1;	return ORG;}

<INITIAL,INI08B,DEFINED>CODE 		{ OPCODEOPER;yylval.val = 0;		return CODE;}
<INITIAL,INI08B,DEFINED>DATA 		{ OPCODEOPER;yylval.val = 0;		return DATA;}
<INITIAL,INI08B,DEFINED>XDATA 		{ OPCODEOPER;yylval.val = 0;		return XDATA;}
<INITIAL,INI08B,DEFINED>STK 		{ OPCODEOPER;yylval.val = 0;		return STK;}
<INITIAL,INI08B,DEFINED>REL 		{ OPCODEOPER;yylval.val = 0;		return REL;}
<INITIAL,INI08B,DEFINED>ABS 		{ OPCODEOPER;yylval.val = 0;		return ABS;}
<INITIAL,INI08B,DEFINED>CON 		{ OPCODEOPER;yylval.val = 0;		return CON;}
<INITIAL,INI08B,DEFINED>OVL 		{ OPCODEOPER;yylval.val = 0;		return OVL;}
<INITIAL,INI08B,DEFINED>END 		{ OPCODEOPER;yylval.val = 0;		
label_lineno=yylineno;
if(symbolCaseSensitivity)
	str2upper(yytext);
strcpy(strbuffer[strindex], yytext);

		yylval.string=strbuffer[strindex++]; // value is string!!
		if( strindex>=MAX_LEX_STR_IND) strindex = 0;

		if(noMacroNow || now_state==S_NAMEONLY)
			{
	if(symbolCaseSensitivity)
			str2upper(yylval.string);
#ifdef DEBUG
		fprintf(stderr, "end as label at line %d\n", yylineno);
#endif
	
		return LABEL;
			}
		
return END;}
<INITIAL,INI08B,DEFINED>LOW 		{ OPCODEOPER;yylval.val = 0;		return LOW;}
<INITIAL,INI08B,DEFINED>HIGHD2 		{ OPCODEOPER;yylval.val = 0;		return HIGHD2;}
<INITIAL,INI08B,DEFINED>D2		{ OPCODEOPER;yylval.val = 0;		return D2;}

<INITIAL,INI08B,DEFINED>HIGH 		{ OPCODEOPER;yylval.val = 0;		return HIGH;}



<INITIAL,INI08B,DEFINED>CLRF { OPCODEOPER;yylval.val = 0;	noMacroNow=1;	return CLRF ;}
<INITIAL,INI08B,DEFINED>ADDF { OPCODEOPER;yylval.val = 0;	noMacroNow=1;	return ADDF ;}
<INITIAL,INI08B,DEFINED>INF { OPCODEOPER;yylval.val = 0;	noMacroNow=1;	return INF ;}
<INITIAL,INI08B,DEFINED>INSZ { OPCODEOPER;yylval.val = 0;	noMacroNow=1;	return INSZ ;}
<INITIAL,INI08B,DEFINED>DCSZ { OPCODEOPER;yylval.val = 0;	noMacroNow=1;	return DCSZ ;}
<INITIAL,INI08B,DEFINED>SUBF { OPCODEOPER;yylval.val = 0;	noMacroNow=1;	return SUBF ;}
<INITIAL,INI08B,DEFINED>COMF { OPCODEOPER;yylval.val = 0;	noMacroNow=1;	return COMF ;}
<INITIAL,INI08B,DEFINED>MVF { OPCODEOPER;yylval.val = 0;		noMacroNow=1;
#ifdef DEBUG
	fprintf(stderr,"MVF found ");
#endif
return MVF ;}
<INITIAL,INI08B,DEFINED>ADDC { OPCODEOPER;yylval.val = 0;		noMacroNow=1;return ADDC ;}
<INITIAL,INI08B,DEFINED>ANDF { OPCODEOPER;yylval.val = 0;		noMacroNow=1;return ANDF ;}
<INITIAL,INI08B,DEFINED>IORF { OPCODEOPER;yylval.val = 0;		noMacroNow=1;return IORF ;}
<INITIAL,INI08B,DEFINED>XORF { OPCODEOPER;yylval.val = 0;		noMacroNow=1;return XORF ;}
<INITIAL,INI08B,DEFINED>SUBC { OPCODEOPER;yylval.val = 0;		noMacroNow=1;return SUBC ;}
<INITIAL,INI08B,DEFINED>RRF { OPCODEOPER;yylval.val = 0;		noMacroNow=1;return RRF ;}
<INITIAL,INI08B,DEFINED>ADDL { OPCODEOPER;yylval.val = 0;		noMacroNow=1;return ADDL ;}
<INITIAL,INI08B,DEFINED>SUBL { OPCODEOPER;yylval.val = 0;		noMacroNow=1;return SUBL ;}
<INITIAL,INI08B,DEFINED>ANDL { OPCODEOPER;yylval.val = 0;		noMacroNow=1;return ANDL ;}
<INITIAL,INI08B,DEFINED>IORL { OPCODEOPER;yylval.val = 0;		noMacroNow=1;return IORL ;}
<INITIAL,INI08B,DEFINED>XORL { OPCODEOPER;yylval.val = 0;		noMacroNow=1;return XORL ;}
<INITIAL,INI08B,DEFINED>MVL { OPCODEOPER;yylval.val = 0;		noMacroNow=1; 
#ifdef DEBUG
	fprintf(stderr,"MVL found ");
#endif

return MVL ;
	
}
<INITIAL,INI08B,DEFINED>RETL { OPCODEOPER;yylval.val = 0;		noMacroNow=1;return RETL ;}
<INITIAL,INI08B,DEFINED>CALL { OPCODEOPER;yylval.val = 0;		push_state(S_NAMEONLY); if(is08b) return RCALL;return CALL ;}
<INITIAL,INI08B,DEFINED>".CHKPARA" { OPCODEOPER;yylval.val = 0;		push_state(S_NAMEONLY); return CHKPARAHEAD ;}
<INITIAL,INI08B,DEFINED>".CALLFPTR" { OPCODEOPER;yylval.val = 0;		push_state(S_NAMEONLY); return CALLFPTR ;}

<INITIAL,INI08B,DEFINED>JMP { OPCODEOPER;yylval.val = 0;	push_state(S_NAMEONLY);	if(is08b) return RJ; return JMP ;}
<INITIAL,INI08B,DEFINED>RETI { OPCODEOPER;yylval.val = 0;		noMacroNow=1;return RETI ;}
<INITIAL,INI08B,DEFINED>RET { OPCODEOPER;yylval.val = 0;		noMacroNow=1;
#ifdef DEBUG
fprintf(stderr,"RET token found\n");
#endif
return RET ;}
<INITIAL,INI08B,DEFINED>IDLE { OPCODEOPER;yylval.val = 0;		noMacroNow=1;return IDLE ;}
<INITIAL,INI08B,DEFINED>DAW { OPCODEOPER;yylval.val = 0;		noMacroNow=1;return DAW ;}
<INITIAL,INI08B,DEFINED>SLP { OPCODEOPER;yylval.val = 0;		noMacroNow=1;return SLP ;}
<INITIAL,INI08B,DEFINED>CWDT { OPCODEOPER;yylval.val = 0;		noMacroNow=1;return CWDT ;}
<INITIAL,INI08B,DEFINED>SETF { OPCODEOPER;yylval.val = 0;		noMacroNow=1;return SETF ;}
<INITIAL,INI08B,DEFINED>BCF { OPCODEOPER;yylval.val = 0;		noMacroNow=1;return BCF ;}
<INITIAL,INI08B,DEFINED>BSF { OPCODEOPER;yylval.val = 0;		noMacroNow=1;return BSF ;}
<INITIAL,INI08B,DEFINED>BTSZ { OPCODEOPER;yylval.val = 0;		noMacroNow=1;return BTSZ ;}
<INITIAL,INI08B,DEFINED>BTSS { OPCODEOPER;yylval.val = 0;		noMacroNow=1;return BTSS ;}
<INITIAL,INI08B,DEFINED>MULF { OPCODEOPER;yylval.val = 0;		noMacroNow=1;return MULF ;}
<INITIAL,INI08B,DEFINED>JZ { OPCODEOPER;yylval.val = 0;		noMacroNow=1;return JZ ;}
<INITIAL,INI08B,DEFINED>RRFC { OPCODEOPER;yylval.val = 0;		noMacroNow=1;return RRFC ;}
<INITIAL,INI08B,DEFINED>RLFC { OPCODEOPER;yylval.val = 0;		noMacroNow=1;return RLFC ;}
<INITIAL,INI08B,DEFINED>RLF { OPCODEOPER;yylval.val = 0;		noMacroNow=1;return RLF ;}
<INITIAL,INI08B,DEFINED>SWPF { OPCODEOPER;yylval.val = 0;		noMacroNow=1;return SWPF ;}
<INITIAL,INI08B,DEFINED>TBLR { OPCODEOPER;yylval.val = 0;		noMacroNow=1;return TBLR ;}
<INITIAL,INI08B,DEFINED>MVLP { OPCODEOPER;yylval.val = 0;		noMacroNow=1;return MVLP ;}
<INITIAL,INI08B,DEFINED>JC { OPCODEOPER;yylval.val = 0;		noMacroNow=1;return JC ;}
<INITIAL,INI08B,DEFINED>INSUZ { OPCODEOPER;yylval.val = 0;		noMacroNow=1;return INSUZ ;}
<INITIAL,INI08B,DEFINED>DCSUZ { OPCODEOPER;yylval.val = 0;		noMacroNow=1;return DCSUZ ;}
<INITIAL,INI08B,DEFINED>MULL { OPCODEOPER;yylval.val = 0;		noMacroNow=1;return MULL ;}
<INITIAL,DEFINED>LDPR { OPCODEOPER;yylval.val = 0;		noMacroNow=1;return LDPR ;}
<INITIAL,DEFINED>LBSR { OPCODEOPER;yylval.val = 0;		noMacroNow=1;return LBSR ;}
<INITIAL,INI08B,DEFINED>NOP { OPCODEOPER;yylval.val = 0;		return NOP ;}
<INITIAL,DEFINED>ARLC { OPCODEOPER;yylval.val = 0;		noMacroNow=1;return ARLC ;}
<INITIAL,DEFINED>ARRC { OPCODEOPER;yylval.val = 0;		noMacroNow=1;return ARRC ;}
<INITIAL,INI08B,DEFINED>CPSG { OPCODEOPER;yylval.val = 0;		noMacroNow=1;return CPSG ;}
<INITIAL,INI08B,DEFINED>CPSL { OPCODEOPER;yylval.val = 0;		noMacroNow=1;return CPSL ;}
<INITIAL,INI08B,DEFINED>CPSE { OPCODEOPER;yylval.val = 0;		noMacroNow=1;return CPSE ;}
<INITIAL,INI08B,DEFINED>TFSZ { OPCODEOPER;yylval.val = 0;		noMacroNow=1;return TFSZ ;}
<INITIAL,INI08B,DEFINED>JN { OPCODEOPER;yylval.val = 0;		push_state(S_NAMEONLY);
#ifdef DEBUG
fprintf(stderr,"JN found at line %d\n", yylineno);
#endif

return JN ;}
<INITIAL,INI08B,DEFINED>JO  { OPCODEOPER;yylval.val = 0;		push_state(S_NAMEONLY);return JO ;}
<INITIAL,INI08B,DEFINED>JNC { OPCODEOPER;yylval.val = 0;		push_state(S_NAMEONLY);return JNC ;}
<INITIAL,INI08B,DEFINED>JNZ { OPCODEOPER;yylval.val = 0;		push_state(S_NAMEONLY);return JNZ ;}
<INITIAL,INI08B,DEFINED>JNN { OPCODEOPER;yylval.val = 0;		push_state(S_NAMEONLY);return JNN ;}
<INITIAL,INI08B,DEFINED>JNO { OPCODEOPER;yylval.val = 0;		push_state(S_NAMEONLY);return JNO ;}
<INITIAL,INI08B,DEFINED>BTGF { OPCODEOPER;yylval.val = 0;		noMacroNow=1;return BTGF ;}
<INITIAL,DEFINED>MVFF { OPCODEOPER;yylval.val = 0;		noMacroNow=1;return MVFF ;}

<INITIAL,DEFINED>MVSF { OPCODEOPER;yylval.val = 0;		noMacroNow=1;return MVSF ;}
<INITIAL,DEFINED>MVSS { OPCODEOPER;yylval.val = 0;		noMacroNow=1;return MVSS ;}
<INITIAL,DEFINED>PUSHL { OPCODEOPER;yylval.val = 0;		noMacroNow=1;return PUSHL ;}
<INITIAL,DEFINED>ADDULNK { OPCODEOPER;yylval.val = 0;		noMacroNow=1;return ADDULNK ;}
<INITIAL,DEFINED>ADDFSR { OPCODEOPER;yylval.val = 0;		noMacroNow=1;return ADDFSR ;}

<INITIAL,INI08B,DEFINED>RJ { OPCODEOPER;yylval.val = 0;		push_state(S_NAMEONLY);return RJ ;}
<INITIAL,INI08B,DEFINED>RCALL { OPCODEOPER;yylval.val = 0;		noMacroNow=1;return RCALL ;}
<INITIAL,INI08B,DEFINED>PUSH { OPCODEOPER;yylval.val = 0;		noMacroNow=1;return PUSH ;}
<INITIAL,INI08B,DEFINED>POP { OPCODEOPER;yylval.val = 0;		noMacroNow=1;return POP ;}
<INITIAL,INI08B,DEFINED>DCF { OPCODEOPER;yylval.val = 0;		noMacroNow=1;return DCF ;}


\>\= { OPCODEOPER; yylval.val=0; return CGE;}
\<\= { OPCODEOPER; yylval.val=0; return CLE;}
\=\= { 
#ifdef DEBUG
fprintf(stderr,"== found.\n");
#endif
OPCODEOPER; yylval.val=0; return CEQ;
}
\!\= { 
#ifdef DEBUG
fprintf(stderr,"== found.\n");
#endif
OPCODEOPER; yylval.val=0; return CNE;
}

\> { OPCODEOPER; yylval.val=0; return CGT;}
\< { OPCODEOPER; yylval.val=0; return CLT;}


<INITIAL,INI08B,DEFINED,INIFTRUE,INIFFALSE,MACDEF1,NAMEONLY>[A-Z_a-z][A-Z_a-z0-9]*	 {
	int i;
	uint64 labelhash=symbolCaseSensitivity?hashi(yytext):hash(yytext);
	if(symbolCaseSensitivity) str2upper(yytext);

	for(i=0;i<rep_str_num;i++)
	{
		
		if(labelhash==rep_src_hash[i])
		{
			char *p=rep_dst[i]+strlen(rep_dst[i]);
			while(p>rep_dst[i])
				unput(*--p);
			break;
		}
	}
	
	if(i==rep_str_num )
	{
	SCASECHK;
		strcpy(strbuffer[strindex], yytext);
		yylval.string=strbuffer[strindex++];
		if( strindex>=MAX_LEX_STR_IND) strindex = 0;
#ifdef DEBUG
	printf2("string found --%s\n", yylval.string );
    //if(!strcmp(yylval.string,"_out"))
		//fprintf(stderr,"_out found.\n");
#endif
		label_lineno=yylineno;

		// check if match macro
		if(now_state!=S_MACDEF1 && noMacroNow==0)
		{
			uint64 hid=symbolCaseSensitivity?hashi(yytext):hash(yytext);
			for(i=0;i<defined_macro_num;i++)
			{
				if(hid!=defined_macros[i]->nameHash )
					continue;



				yylval.val=i;
				OPCODEOPER;
				if(macro_level<0)
					macroRootLoc = getLocTree();

				macroInsLocTree[macro_ins_num]=getLocTree();
				macroInsMacroID[macro_ins_num]=i;
				macro_ins_num++;
				defined_macros[i]->used=1;

				if(defined_macros[i]->paras)
				{
					push_state(S_MACPARA); // following is MACPARA state
					

				}
				return MACROID;
			}
		}
		if(now_state==S_NAMEONLY)
			pop_state();
		else // check equ
		{
			symbol *sp = getSymByName(yytext);
			if(sp && sp->symIsEQU && sp->exp->type==ExpIsNumber)
			{
				yylval.val=sp->exp->exp.number;
				return NUM;
			}
		}
		if(symbolCaseSensitivity)
			str2upper(yylval.string);
#ifdef DEBUG
	fprintf(stderr,"return label %s\n", yylval.string);
#endif
OPCODEOPER;
		return LABEL;
	}
}

<MACDEF>[ \t\.A-Z_a-z0-9\,:\+]* {
	char *p=yytext;
	while(*p && isspace(*p))
		p++;
	if(!strncasecmp(p,".ENDM",5))
	{
	OPCODEOPER;
		pop_state();
#ifdef DEBUG
		fprintf(stderr,"MACROEND found\n");
#endif
	
	return MACROTAIL;
	}
	yylval.string=strdup(yytext);
#ifdef DEBUG
			fprintf(stderr,"MACROLINE %s found\n", yytext);
#endif

	return MACROLINE;
}
	
"?"[A-Z_a-z][A-Z_a-z0-9]* {

	SCASECHK;
		strcpy(strbuffer[strindex], yytext+1);
		yylval.string=strbuffer[strindex++];
		if( strindex>=MAX_LEX_STR_IND) strindex = 0;
#ifdef DEBUG
	printf2("local label string found --%s\n", yylval.string );
#endif
		label_lineno=yylineno;
		return LOCALLABEL;
}
	
<MACPARA>[A-Z_a-z0-9+-]+	 {
	int i;
	uint64 labelhash = symbolCaseSensitivity?hashi(yytext):hash(yytext);
	for(i=0;i<rep_str_num;i++)
	{
		
		if(labelhash==rep_src_hash[i])
		{
			char *p=rep_dst[i]+strlen(rep_dst[i]);
			while(p>rep_dst[i])
				unput(*--p);
			break;
		}
	}
	if(i==rep_str_num)
	{
	SCASECHK;
		strcpy(strbuffer[strindex], yytext);
		yylval.string=strbuffer[strindex++];
		if( strindex>=MAX_LEX_STR_IND) strindex = 0;
		label_lineno=yylineno;
		if(symbolCaseSensitivity) str2upper(yylval.string);
#ifdef DEBUG
	printf2("string (LABEL) found --%s\n", yylval.string );
#endif
		
		
		return LABEL;
	}
}

<DEFINED,NODEFINED>"\.ELSE"|".else" {
#ifdef DBGDEF
	printf2("else found at line  %d",yylineno);
#endif
	if(now_state==S_DEFINED)
	{
		
		switch_state(S_NODEFINED);
		
	}
	else
	{
		switch_state(S_DEFINED);
	}
}


<INIFDEF>[A-Z_a-z0-9][A-Z_a-z0-9]* {
	int i;
	int found=0;
	uint64 hashid=symbolCaseSensitivity?hashi(yytext):hash(yytext);
	for(i=0;i<rep_str_num ;i++)
	{
		
		if(hashid==rep_src_hash[i])
		{
			found=1;
			break;
		}
	}
	if(found)
		keyword_defined=1;
	else
		keyword_defined=0;
#ifdef DBGDEF
	printf2("found is %d",found);
#endif
}
<INIFNDEF>[A-Z_a-z0-9][A-Z_a-z0-9]* {
	int i;
	int found=0;
	uint64 hashid=symbolCaseSensitivity?hashi(yytext):hash(yytext);
	for(i=0;i<rep_str_num ;i++)
	{
		
		if(hashid==rep_src_hash[i])
		{
			found=1;
			break;
		}
	}
	if(found)
		keyword_defined=0;
	else
		keyword_defined=1;
#ifdef DBGDEF
	printf2("found is %d",found);
#endif
}




<NODEFINED,DEFINED>\.ENDIF {
#ifdef DBGDEF
	printf2("get endif a line %d",yylineno);
#endif
	
	
	pop_state();
}

<INDEFINE>[a-z\:\\A-Z0-9 \t\r\.,_\+\-\=\>\<\(\)]+ { 
	//char *p;
	//char *q;
/*	
	p = strstr(yytext,"/\*"); // formerly comment left

	if(p!=NULL)
	{
		q=yytext+strlen(yytext);
		while(q!=p)
			unput(*--q);
		*p='\0';
	}

	p=strstr(yytext, ";");
	if(p!=NULL) *p='\0';
	p = strstr(yytext,"//");
	if(p!=NULL) *p='\0';
	*/
	yylval.string = strdup(yytext);
	if(symbolCaseSensitivity) str2upper(yylval.string);
#ifdef DEBUG
	fprintf(stderr,"def label %s found\n", yylval.string);
#endif
	return LABEL;
}

<NODEFINED>.*\n { 
	char *p=yytext;
	while(*p && isspace(*p))
		p++;
	if(!strncmpi(p, ".if", 3))
	{
		push_state(S_NODEFINED);
	}else
	if(!strncmpi(p,".ELSE",5))
	{
		switch_state(S_DEFINED);
#ifdef DBGDEF
	printf2("else find2 at %d",yylineno);
#endif
	}
	else if(!strncmpi(p,".ENDIF",6))
	{

#ifdef DBGDEF
		printf2("get endif b line %d",yylineno);
		printf2("statelev is %d",state_lev);
		printf2("now_state is %d",now_state);
#endif
	

		pop_state();
	}
#ifdef DBGDEF
	else printf2("skip line %d",yylineno);
#endif

		return '\n';
}

{DIGIT}{HEXDIGIT}+H|{DIGIT}{HEXDIGIT}+h|{DIGIT}+h	{
		//	if(now_state == S_MACPARA)
		//	{
		//		yylval.string=strdup(yytext);
		//		return LABEL;
		//	}

		  sscanf(yytext, "%XH", &yylval.val);
#ifdef DEBUG
		fprintf(stderr, "get num 0x%X at line %d\n", yylval.val, yylineno);
#endif
		  return NUM;
		}
[01]+b {  // binary number
		int i;
		int val=0;
		for( i=0;i<yyleng;i++)
		{
			if(!isdigit(yytext[i]))
				break;
			val<<=1;
			if(yytext[i]=='1')
				val|=1;
		}
		yylval.val = val;
		return NUM;
}	
0x{HEXDIGIT}+|0x{HEXDIGIT}+H	{
		  int i;
			char *p;
		  
		  for( i = 0; i < yyleng; i++)
			yytext[i]= toupper(yytext[i]);
		p = strchr(yytext,'H');
		if(p)
			*p='\0';
			//if(now_state == S_MACPARA)
			//{
				//yylval.string=strdup(yytext);
				//return LABEL;
			//}
			
		  sscanf(yytext, "0X%X", &yylval.val);

#ifdef DEBUG
		fprintf(stderr,"Number %s found at line %d\n", yytext, yylineno);
#endif
		  return NUM;
		}
{DIGIT}+ {
#ifdef DEBUG
			fprintf(stderr,"int NUM %s found at line %d\n", yytext, yylineno);
#endif
			//if(now_state == S_MACPARA)
			//{
				//yylval.string=strdup(yytext);
				//return LABEL;
			//}
			yylval.val=atoi(yytext);
			return NUM;
		}


%%

void before_lex(void)
{
		keyword_defined=0;
		strindex=0;
		rep_str_num=0;
		//{if(strcmp(opcode_fname,now_lex_fname)) opcode_fname=strdup(now_lex_fname);};opcode_lineno=0;
		opcode_fname="";
		state_lev=0;
		if(is08b)
		{
			now_state=S_INI08B;
			BEGIN INI08B;
		}else
		{
			now_state=S_INI;
		}
		memset(state_stack,0,sizeof(state_stack));
		fflush(stdout);
}

void newline_state_change(int preChange)
{
static int changed = 0;
    noMacroNow = 0; // reset noMacroNow
#ifdef DEBUG
	fprintf(stderr,"new line state change .. statenow = %s at %d\n", now_state_str(), yylineno);
#endif

	if(changed)
	{
		changed=0;
		return;
	}
	changed = preChange;
	if(macro_level>=0) // for macro processing
	{
		macro_left_line[macro_level]--;
		macro_used_line[macro_level]++;
		if(macro_left_line[macro_level]<0)
			macro_level--;
	}
	if(rptFlag)
	{
		rptFlag=0;
		push_state(S_MACDEF);
		return;
	}
	switch(now_state)
	{
		case S_MACPARA: // para in the same line!!
		case S_INDEFINE:
		case S_NAMEONLY: // when newline nameonly skip!!
			pop_state();
			break;
		case S_INIFDEF:
		case S_INIFNDEF:
		case S_INIFTRUE:
		case S_INIFFALSE:
			if(keyword_defined)
			{
				switch_state(S_DEFINED);
			}else
			{
				switch_state(S_NODEFINED);
			}
			break;
		case S_MACDEF1:
			switch_state(S_MACDEF);
			break;
	}
}

int yywrap(void)
{
//    if(include_lev==0)
    	return 1;
    //include_lev--;
    //now_lex_fname = yyfile_stack[include_lev];
    //yyin = yyinput_stack[include_lev];
    //yypop_buffer_state();
    //return 0;
}
void switch_state( int state)
{
#ifdef DEBUG
	fprintf(stderr,"switch to  state %s at %d\n", state_str(state), yylineno);
#endif
	now_state=state;
	switch(state)
	{
		case S_INI: BEGIN INITIAL; break;
		case S_INI08B: BEGIN INI08B;break;
		case S_INDEFINE: BEGIN INDEFINE; break;
		case S_DEFINED: BEGIN DEFINED; break;
		case S_NODEFINED: BEGIN NODEFINED; break;
		case S_INIFDEF: BEGIN INIFDEF; break;
		case S_INIFNDEF: BEGIN INIFNDEF; break;
		case S_MACDEF: BEGIN MACDEF; break;
		case S_MACDEF1: BEGIN MACDEF1; break;
		case S_MACPARA: BEGIN MACPARA; break;
		case S_INIFTRUE: 
				BEGIN INIFTRUE; break;
		case S_INIFFALSE: BEGIN INIFFALSE; break;
		case S_NAMEONLY: BEGIN NAMEONLY; break;

		default:
			fprintf(stderr,"Assembler Error: Internal ERROR:switch_state unknown error.\n");
			exit(5);
		
	}
	
}
char *now_state_str(void)
{
	return state_str(now_state);
}
char *state_str(int state)
{
	switch(state)
	{
		case S_INI:return "INI";
		case S_INI08B: return "INI08B";
		case S_INDEFINE: return "INDEFINE"; 
		case S_DEFINED: return "DEFINED"; 
		case S_NODEFINED: return "NODEFINED"; 
		case S_INIFDEF: return "INIFDEF"; 
		case S_INIFNDEF: return "INIFNDEF"; 
		case S_MACDEF: return "MACDEF"; 
		case S_MACDEF1: return "MACDEF1"; 
		case S_MACPARA: return "MACPARA"; 
		case S_INIFTRUE: return "INIFTRUE"; 
		case S_INIFFALSE: return "INIFFALSE"; 
		case S_NAMEONLY: return "NAMEONLY";

	}
	return "unknown";
}
void push_state(int new_state)
{
#ifdef DEBUG
	fprintf(stderr,"push state %s at %d\n", now_state_str(), yylineno);
#endif
	state_stack[state_lev++]=now_state;
	now_state=new_state;
	switch_state(now_state);
}
void pop_state(void)
{
#ifdef DEBUG
	fprintf(stderr,"pop state at %d\n", yylineno);
#endif
	// it is possible a bug for call 000
	if(state_lev>0)
	{
	now_state = state_stack[--state_lev];
	switch_state(now_state);
	}
}

void replace_macro_str(char *buf)
{
	int i;
	uint64 hashid=symbolCaseSensitivity?hashi(buf):hash(buf);
	for(i=mrep_str_num[macro_level]-1; i>=0;i--)
	{
		if(mrep_src_hash[i]==hashid)
		{
			if(mrep_local[i]) // replacing local var
			{
				char sid[100];
				snprintf(sid, 100,"_M_%d_", mrep_str_local[macro_level]);

				strncpy(buf,mrep_src[i], MAXSTRSIZE1);
				strncat(buf,sid, MAXSTRSIZE1);
			}
			else
				strncpy(buf,mrep_dst[i], MAXSTRSIZE1);
			break;
		}
	}
}

#define BUFLEN 65536
void push_replaced_macro(char *rmacro, int addList, int bufsize)
{
const	char finalMark[]=".MACROPUSHEND\n";
char buf[BUFLEN+1]; // maximum 64k
//char *p=buf;
char *pp;
	int len1=strlen(finalMark);
	int len;
	char *macro_Lines[1024];
	char lineBuf[MAXSTRSIZE1+1];
	char strBuf[MAXSTRSIZE1+1];
	int comment_found=0; // 1 is oneline comment, 2 is multiline comment
	int i;
	int jj;

	//int mystate=0; // 0 is not comment, 1 is comment

	int totaln=split2lines(rmacro, macro_Lines);
	while(len1)
		unput(finalMark[--len1]);
	//unput('\n'); // add additional newline
	// we have to replace the arguments
	buf[0]='\0';
	// simple parser
	for(i=0;i<totaln;i++)
	{
		int kk;
		char *qq,*tt;
		jj=0;
		pp=macro_Lines[i];
		if(comment_found==1)
			comment_found=0;
		
		if(comment_found==2) // need find */
		{
			if((pp=strstr(buf,"*/"))==NULL)
				continue; // no push is OK
			pp+=2;
			comment_found=0;
		}
		while(*pp)
		{
			while(*pp && !isalnum(*pp)) 
			{
				if(*pp==';' || (*pp=='/' && *(pp+1)=='/'))
				{
					// copy to end of line
					while(*pp)
						lineBuf[jj++]=*pp++;

					break;
				}
				if(*pp=='/' && *(pp+1)=='*')
				{
					*pp='\0';
					while(*pp)
						lineBuf[jj++]=*pp++;
					comment_found=0;
					break;
				}
				lineBuf[jj++]=*pp; 
				pp++;
			}
			if(!(*pp))
				 break;
			for(qq=pp;*qq;qq++)
				if(!isalnum(*qq))
					break;
			// copy pp ..qq
			for(tt=pp,kk=0;tt!=qq;tt++)
				strBuf[kk++]=*tt;
			strBuf[kk]='\0';
			pp=qq;
			// see if it need to re replaced
			replace_macro_str(strBuf);
			for(tt=&strBuf[0];*tt;tt++)
				lineBuf[jj++]=*tt;
		}
		lineBuf[jj]='\0';
		free(macro_Lines[i]);
		strncat(buf,lineBuf,BUFLEN-1);
		strncat(buf,"\n", BUFLEN-1);
	}
	strncpy(rmacro,buf,bufsize);
	// check any nonspace tokens
	len = strlen(buf);
	while(len)
		unput(rmacro[--len]);
}




