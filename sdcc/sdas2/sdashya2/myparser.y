%{
    /****************************
    myparser.y
    // we still use C code to do it
    // for the maximal compatibility
    ****************************/

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>

#ifdef _WIN32
//#include <windows.h>
#include "Shlwapi.h"
#else
#include "libgen.h"
#endif

#include "mytype.h"
#include "asparser.h"
//#define DEBUG
#ifndef __COUNTER__
int __count=0;
#define __COUNTER__ (__count++)
#endif

int yylex();
void yyerror(const char *s);
extern void pop_state(void);



char *getLocTree(void);
    extern int yylineno;
    extern int opcode_lineno;
	extern char *opcode_fname;
	extern int label_lineno;
    extern void before_lex(void);
    char *ChangeFileExt(char *,char*);
    int defined_macro_num=0;
	macrot * defined_macros[MAX_MACRO_NUM];
	//int macroref_lineno=0;
    
	//static int localReplaceId=0;
    dataItem * appendExp(dataItem *head, expression* taile);
    int dumpMne(void);
    int error_count;
	// macro related
	int macro_level=-1;
 	int macro_left_line[MAX_MACRO_LEVEL];
	int macro_used_line[MAX_MACRO_LEVEL];
	int opcode_macro_used_line[MAX_MACRO_LEVEL];
	int macroid_stack[MAX_MACRO_LEVEL];
	//int macroref_lineno[MAX_MACRO_LEVEL];
	char *macroRootLoc;
	int macroissue_lineno=-1;
	int includeissue_lineno=-1;
	 char * incSearchPath[128];
	 int incSearchPathNum=0;

	//static dataItem *macroParap=NULL;
	static int paraMatchIndex;
	char * oldMacroBufs[32]={NULL}; 
	char *asmPath;
	macrot *newMacro(char *name, int check, int declare_lineno, char * declaresrc); // check means check if doubles

    //unsigned char program_code [65536];
    int code_addr;
// default highlow=0

    //int addr, int lineno);

    void reset_counts(void);
    int lastaddr;
    int row_count;
    int endlineno;
    int equ_count;
    int equ_def[40960];
    int equ_lineno[40960];
    int highest_addr=0;
	int compatibleMode=0;
	int symbolCaseSensitivity=0;
	int is08b=0;
	int is08d=0;
	int genDep=0;
	char *depFname=NULL;
	FILE *depfp=NULL;

    int ds_addr=0x8000;
    MNE *nowMNE=NULL;

	area *parserArea;

	int rpt_id=1; // start from 1
	int macro_ins_num=0; // macro insert record, in rel file, RPT not include
	char * macroInsLocTree[1024];
	int macroInsMacroID[1024];

	int ramModel=0;

	//static int macroIssueLineNo=-1;

	

    static void clear_module(module *m)
    {
		memset(m,0, sizeof(module));
    }
    static char* find_area_name(char *areahead)
    {
	while(!isspace(*areahead))
		areahead++;
	while(isspace(*areahead))
		areahead++;
	return areahead;
    }
    static void range_chk(int range, int val)
    {
	char buff[256];
	if(val <0 || val>range )
	{
		sprintf(buff,"val %x range %d error at line %d\n",
		val, range, opcode_lineno);
		yyerror(buff);

	}
		
    }

    module topModule;
	
// constructor
    void parser_init(void)
    {
        // place any extra initialisation code here
        clear_module(&topModule);
		topModule.areas = newArea("_CODE", 0, 0,  regionCODE); // default here!!
		//topModule.areas->next = newArea("ZP", 0, 0,  regionSTK);
		//topModule.areas->next->next = newArea("ZPROM", 0, 0,  regionCODE);
		parserArea = topModule.areas;
    }

// destructor
    void parser_exit(void)
    {
        // place any extra cleanup code here
    }

%}


%union  { /* 2 kinds of values */
    int  val;
	regionType region;
    char * string;
    MNE * mne;
    expression *exp;
    dataItem *items;
	macrot *macro; // it can be a macro
	macroParaT *para1;
}

%token <string> MACROLINE
%token <string> DEFINE
%token <val> IFTRUE
%token <val> IFFALSE
%token <val> NUM
%token <val> MODULE
%token <string> LABEL
%token <string> LOCALLABEL
%token <val> GLOBL
%token <val> LOCAL
%token <string> AREA
%token <val> ORG
%token <val> MACROID

%token <val> CGE
%token <val> CGT
%token <val> CEQ
%token <val> CNE
%token <val> CLE
%token <val> CLT



// special tokens
%token <val> CODE
%token <val> DATA
%token <val> XDATA
%token <val> STK
%token <val> REL
%token <val> ABS
%token <val> OVL
%token <val> CON

// ram assignment
%token <val> DS
%token <val>  BYTEDEF
%token <val> DW
%token <val> ASCII

// quoted string
%token <string> QUOSTR



// instructions
%token <val> NOP
%token <val> CLRF
%token <val> ADDF
%token <val> INF
%token <val> INSZ
%token <val> DCSZ
%token <val> SUBF
%token <val> COMF
%token <val> MVF
%token <val> ADDC
%token <val> ANDF
%token <val> IORF
%token <val> XORF
%token <val> DCF
%token <val> RLF
%token <val> SUBC
%token <val> RRF
%token <val> ADDL
%token <val> SUBL
%token <val> ANDL
%token <val> IORL
%token <val> XORL
%token <val> MVL
%token <val> RETL
%token <val> CALL
%token <val> JMP
%token <val> RETI
%token <val> RET
%token <val> IDLE
%token <val> DAW
%token <val> SLP
%token <val> CWDT
%token <val> SETF
%token <val> BCF
%token <val> BSF
%token <val> BTSZ
%token <val> BTSS
%token <val> MULF
%token <val> JZ
%token <val> RRFC
%token <val> RLFC
%token <val> SWPF
%token <val> TBLR
%token <val> MVLP
%token <val> JC
%token <val> INSUZ
%token <val> DCSUZ
%token <val> MULL
%token <val> LDPR
%token <val> LBSR
%token <val> ARLC
%token <val> ARRC
%token <val> CPSG
%token <val> CPSL
%token <val> CPSE
%token <val> TFSZ
%token <val> JN
%token <val> JO
%token <val> JNC
%token <val> JNZ
%token <val> JNN
%token <val> JNO
%token <val> BTGF
%token <val> MVFF
%token <val> RJ
%token <val> RCALL
%token <val> PUSH
%token <val> POP		
%token <val> FSR2H; // Stack pointer

%token <val> ADDFSR;
%token <val> ADDULNK;
%token <val> PUSHL;
%token <val> MVSF;
%token <val> MVSS;


%token <string> END
%token <val> HIGH
%token <val> EQU
%token <val> LOW
%token <val> D2
%token <val> HIGHD2
%token <val> SHRIGHT
%token <val> SHLEFT


%token <val> CHKPARAHEAD;
%token <val> CALLFPTR;
%token <val> FUNCHEAD;
%token <val> FUNCENDHEAD;
%token <val> CALLMARK;
%token <val> LOCALMARK;
%token <val> RETURNSIZEMARK;
%token <val> XLOCALMARK;

%token <val> MACROHEAD;
%token <val> MACROREPHEAD;
%token <val> MACROTAIL;

%token <val> MACROPUSHEND;

/* pos or neg num */
%type <val> numpn

%type <mne> line
%type <mne> lines
%type <mne> macroLines
%type <mne> macroLinesPara
%type <mne> macroPushEndLine
%type <val> ends

// per instruction, it has fda, fba, fa, lit, jxx, fsfd, f


// Add for H08D
%type <mne> callFPTRLine;
%type <mne> specialInstr;

%type <mne> instrLine;
%type <mne> dbdsLine;
%type <mne> moduleLine;
%type <mne> globlLine;
%type <mne> areaLine;
%type <mne> areaBase;
%type <mne> labelLine;
%type <mne> equLine;
%type <mne> org_line;
%type <mne> func_line;
%type <mne> parachk_line;
//%type <mne> func_line_pre;
//%type <val> param_seg;
%type <mne> func_end_line;
%type <val> defineLine;
%type <macro> macroDefHeadLineBase;
%type <macro> macroDefHeadLineBaseA;
%type <macro> macroDefHeadLinePara;
%type <macro> macroDefHeadLineParaA;
%type <macro> macroDefHeadLineRept;
%type <macro> macroDefHeadLineReptA;
%type <macro> macroDef;




// types of INSTR
%type <mne> instrFDA;
%type <mne> instrLIT;
%type <mne> instrFA;
%type <mne> instrFBA;
%type <mne> instrLBSR;
%type <mne> instrKF;
%type <mne> instrFSFD;
%type <mne> instrN12;
%type <mne> instrNOPR;
%type <mne> instrN8;
%type <mne> instrN11;
%type <mne> instrADDBIT;
%type <mne> instrTBLR;

%type <val> FDAOP;
%type <val> FBAOP;
%type <val> FAOP;
%type <val> LITOP;
%type <val> LBSROP;
%type <val> KFOP;
%type <val> FSFDOP;
%type <val> N12OP;
%type <val> NOPROP;
%type <val> N8OP;
%type <val> N11OP;
%type <val> ADDBITOP;
%type <val> TBLROP;


%type <val> ABSREL;
%type <val> CONOVL;
%type <val> GLOLOC;
%type <region> CODEDATASTK;

%type <exp> expr;
%type <items> dataItems;
%type <items> callList;
%type <items> localList;
%type <para1> macro_para;



%left '+' '-'
%%

/////////////////////////////////////////////////////////////////////////////
// rules section

// place your YACC rules here (there must be at least one)

//Grammar
assembly:
lines;
// | lines ends ;

ends:
END     {
#ifdef DEBUG
    fprintf(stderr, "END found at %d\n", yylineno);
#endif
    endlineno=yylineno;
}
| ends '\n' 
{
#ifdef DEBUG
    fprintf(stderr, "END found at %d .. as ends\n", yylineno);
#endif

}
;

lines:
  line  {
		if($1!=NULL)
		{
			if(nowMNE==NULL)
			{
				nowMNE=$1;
				topModule.mneList=nowMNE;
				$$=nowMNE;
			}
			else
			{
				nowMNE->next=$1;
				nowMNE=$1;
				$$=nowMNE;
			}
		}
		else
			$$=NULL;
	}
| lines line  { 
		if($2!=NULL)
		{
			if(nowMNE==NULL)
			{
				topModule.mneList=$2;
				nowMNE=$2;
				$$=$2;
			}else
			{
				nowMNE->next =$2;
				nowMNE=$2;
				$$=$2;
				// incase LABEL+....
				if(nowMNE->next!=NULL)
				{
					nowMNE=$2->next;
					$$=$2->next;
				}
			}
			$2->macroLevel = macro_level;
			$2->include_lev = include_lev;
			$2->include_fname = now_lex_fname; // the fname is dup, which will not be destroyed
			if(include_lev>0)
			{
				$2->includeissue_lineno = includeissue_lineno; // change lineno!!
			}
			if(macro_level>=0)
			{
				char *macro_Lines[1024];
				int totalmacrolineno=split2lines(oldMacroBufs[macro_level], macro_Lines);
				int i;
				char hashbuf[20];
				
				// make a hash
				memset(hashbuf,0,sizeof(hashbuf));
		
				$2->macroid=macroid_stack[macro_level];
				//hashbuf[0] = macroid_stack[macro_level] &0xff;
				//hashbuf[1] = (macroid_stack[macro_level]>>8) &0xff;
				//hashbuf[2] = (macro_used_line[macro_level]-1)&0xff;
				//hashbuf[3] = ((macro_used_line[macro_level]-1)>>8)&0xff;
				//hashbuf[4] = macroissue_lineno &0xff;
				//hashbuf[5] = (macroissue_lineno>>8)&0xff;

				//$2->macroHash = CityHash64(hashbuf,6);

				if(macro_used_line[macro_level]>0)
				{
				
					$2->macroLine=strdup(macro_Lines[macro_used_line[macro_level]-1]); // because it is after \n
				}
				else
				{
					$2->macroLine=strdup("macro..");
				}
				for(i=0;i<totalmacrolineno;i++)
					free(macro_Lines[i]);
			}
		}
		else
			$$=$1;
		
	}
| lines macroLines { 
		if($2!=NULL)
		{
			if(nowMNE==NULL)
			{
				topModule.mneList=$2;
				nowMNE=$2;
				$$=$2;
			}else
			{
				nowMNE->next =$2;
				nowMNE=$2;
				$$=$2;
				// incase LABEL+....
				if(nowMNE->next!=NULL)
				{
					nowMNE=$2->next;
					$$=$2->next;
				}
			}
		}
		else $$=$1;
		
	}


;

macroLines: 
MACROID '\n' { 
// no para replace
	char buf[40960];
	macrot *mp = defined_macros[$1];
	int macroLineNo=0;
	srcLine *sp=mp->lines;
	dataItem *dip;
	int iii=0;
	buf[0]='\0';
	//if(macro_level>=0)
	//{ // show single issue line
		//strncat(buf, defined_macros[$1]->macroName, 40960);
		//strncat(buf,"\n",40960);
		//macroLineNo++;
	//}
#ifdef DEBUG
	fprintf(stderr, "get MACROID.\n");
#endif
	while(sp)
	{
		strncat(buf,sp->line,40960-1);
		strncat(buf,"\n",40960-1);
		macroLineNo++;
		sp=sp->next;
	}
	//$$=NULL;

	$$= newEmptyMne();
	$$->type = mneIsMacro;
	$$->m.asm_macro.level = macro_level+1; // +1 before real ?
	strncat($$->m.asm_macro.issueLine, defined_macros[$1]->macroName,4096-1);
	if(macro_level<0)
	{
		$$->m.asm_macro.src_lineno=opcode_lineno;
		macroissue_lineno=opcode_lineno;
	}
	else
	{
		$$->m.asm_macro.src_lineno=macroissue_lineno;
		$$->m.asm_macro.upper_macro_offset = macro_used_line[macro_level];
	}

	macro_level++; // -1 -> 0
	if(macro_level==0)
	{
			dip = defined_macros[$1]->paras;
			defined_macros[$1]->used=1;
			
			iii = 0;
			while(dip)
			{
				strncpy(mrep_src[iii],dip->exp->exp.symbol, MAXSTRSIZE1);
				mrep_src_hash[iii]=symbolCaseSensitivity?hashi(dip->exp->exp.symbol):hash(dip->exp->exp.symbol);
				mrep_local[iii]=dip->flag;
				dip=dip->next;
				if(mrep_local[iii]==0) // it is real para, not local label
				{
					fprintf(stderr,"Assembler error %s at macro %s needs parameter!!\n",
						getLocTree(), defined_macros[$1]->macroName );
					exit(2003);
				}
				else
					mrep_dst[iii][0]='\0';
				iii++;
			}
			mrep_str_num[0]=iii;
			mrep_str_local[0]=macroLocalCount++;
		}
		else
		{
			dip = defined_macros[$1]->paras;
			defined_macros[$1]->used=1;
			iii=mrep_str_num[macro_level-1];
			while(dip)
			{
				strncpy(mrep_src[iii],dip->exp->exp.symbol, MAXSTRSIZE1);
				mrep_src_hash[iii]=symbolCaseSensitivity?hashi(dip->exp->exp.symbol):hash(dip->exp->exp.symbol);
				mrep_local[iii]=dip->flag;
				dip=dip->next;
				if(mrep_local[iii]==0) // it is real para, not local label
				{
					fprintf(stderr,"Assembler error %s macro %s needs parameter!!\n",
						getLocTree(), defined_macros[$1]->macroName );
					exit(2003);
				}
				else
					mrep_dst[iii][0]='\0';
				iii++;
			}
			mrep_str_num[macro_level]=iii;
			mrep_str_local[macro_level]=macroLocalCount++;
		}

	

	if(macro_level>0)
		$$->m.asm_macro.upper_macro_offset = macro_used_line[macro_level-1];
	$$->m.asm_macro.total_lineno=macroLineNo;
	$$->m.asm_macro.macro_id = $1;


	if(macro_level>0)
		{
			macro_used_line[macro_level-1]++; // because we will increase a line
		}
	macro_left_line[macro_level]=macroLineNo;
	macro_used_line[macro_level]=0;
	macroid_stack[macro_level]=$1;
		
	push_replaced_macro(buf,1,40960); // buf will be changed!!
	oldMacroBufs[macro_level]=strdup(buf);
}
| macroLinesPara '\n' 
{
	char buf[65536];
	// if level >=0, we need show the issue line
	strncpy(buf,oldMacroBufs[macro_level],65536-1);
	push_replaced_macro(buf,1, 65536-1);// pushed back here!!
	free(oldMacroBufs[macro_level]);
	oldMacroBufs[macro_level]=strdup(buf);
	pop_state(); // force pop state?
	$$=$1;
}
;
macroLinesPara: 
MACROID LABEL  { 
// no para replace
	char buf[40960];
	macrot *mp = defined_macros[$1];
	if(!mp->paras)
	{
		sprintf(buf,"%s macro %s need NO parameter.", getLocTree(), mp->macroName);
		yyerror(buf);
		$$=newEmptyMne();
		$$->type = mneIsErr;
		$$->m.err.src_lineno=opcode_lineno;
		$$->m.err.msg = strdup(buf);
	}else{
		int macroLineNo=0;
		dataItem *dip;
		int iii=0;
		int labelCopied=0;
		srcLine *sp=mp->lines;
		buf[0]='\0';
		while(sp)
		{
			strncat(buf,sp->line,40960-1);
			strncat(buf,"\n",40960-1);
			macroLineNo++;
			sp=sp->next;
		}

		$$= newEmptyMne();
		$$->type = mneIsMacro;
		$$->m.asm_macro.level = macro_level+1;
		$$->m.asm_macro.src_lineno=opcode_lineno;
		strncat($$->m.asm_macro.issueLine, defined_macros[$1]->macroName,4096-1);
		strncat($$->m.asm_macro.issueLine, " ",4096-1);
		strncat($$->m.asm_macro.issueLine, $2,4096-1); // add para
	if(macro_level<0)
	{
		$$->m.asm_macro.src_lineno=opcode_lineno;
		macroissue_lineno=opcode_lineno;
	}
	else
	{
		$$->m.asm_macro.src_lineno=macroissue_lineno;
		$$->m.asm_macro.upper_macro_offset = macro_used_line[macro_level];
	}



		macro_level++; // -1 -> 0
		if(macro_level==0)
		{
			
			// we push all paras when we see first para
			dip = defined_macros[$1]->paras;
			iii = 0;
			while(dip)
			{
				strncpy(mrep_src[iii],dip->exp->exp.symbol, MAXSTRSIZE1);
				mrep_src_hash[iii]=symbolCaseSensitivity?hashi(dip->exp->exp.symbol):hash(dip->exp->exp.symbol);
				mrep_local[iii]=dip->flag;
				dip=dip->next;
				if(!labelCopied)
				{
					if(mrep_local[iii]==0) // it is real para, not local label
					{
						strncpy(mrep_dst[iii],$2, MAXSTRSIZE1); // copy to dest
						labelCopied=1;
						paraMatchIndex=iii+1;
					}
					else
						mrep_dst[iii][0]='\0';
				}
				iii++;
			}
			mrep_str_num[0]=iii;
			mrep_str_local[0]=macroLocalCount++;
		}
		else
		{
			dip = defined_macros[$1]->paras;
			iii=mrep_str_num[macro_level-1];
			while(dip)
			{
				strncpy(mrep_src[iii],dip->exp->exp.symbol, MAXSTRSIZE1);
				mrep_src_hash[iii]=symbolCaseSensitivity?hashi(dip->exp->exp.symbol):hash(dip->exp->exp.symbol);
				mrep_local[iii]=dip->flag;
				dip=dip->next;
				if(!labelCopied)
				{
					if(mrep_local[iii]==0) // it is real para, not local label
					{
						strncpy(mrep_dst[iii],$2, MAXSTRSIZE1); // copy to dest
						labelCopied=1;
						paraMatchIndex=iii+1;
					}
					else
						mrep_dst[iii][0]='\0';
				}
				iii++;
			}
			mrep_str_num[macro_level]=iii;
			mrep_str_local[macro_level]=macroLocalCount++;
		}
		if(!labelCopied)
		{
			fprintf(stderr,"Assembler Error: macro parameter not matched  %s!!\n", getLocTree());
			exit(2003);
		}
		
			
		$$->m.asm_macro.total_lineno=macroLineNo;
		$$->m.asm_macro.macro_id = $1;
		$$->m.asm_macro.paras = newEmptyDataItem();
		$$->m.asm_macro.paras->exp = newSymbolExp($2,opcode_lineno);

		macro_left_line[macro_level]=macroLineNo+1;
		if(macro_level>0)
		{
			//macro_used_line[macro_level-1]++; // because we will increase a line
			$$->m.asm_macro.upper_macro_offset = macro_used_line[macro_level-1];
			
		}
		macro_used_line[macro_level]=0;
		
		
		macroid_stack[macro_level]=$1;
		oldMacroBufs[macro_level]=strdup(buf);
	}
}
| macroLinesPara ',' LABEL {
	int labelCopied=0;
	
	dataItem *p=$1->m.asm_macro.paras;
	while(p->next) p=p->next;
	p->next = newEmptyDataItem();
	p->next->exp = newSymbolExp($3, opcode_lineno);
	
	// find a real parameter to copy
	while(paraMatchIndex<mrep_str_num[macro_level] && !labelCopied)
	{
		if(mrep_local[paraMatchIndex]==0)
		{
			strncpy(mrep_dst[paraMatchIndex++],$3,MAXSTRSIZE1);
			labelCopied=1;
		}
	}
	if(!labelCopied)
	{
		fprintf(stderr,"Assembler Error: macro parameter not matched %s!!\n", getLocTree());
		exit(2003);
	}
		strncat($1->m.asm_macro.issueLine, " ",4096);
		strncat($1->m.asm_macro.issueLine, $3,4095); // add para


		
		$$=$1;
}
;

macro_para:
	LABEL { 
			$$=(macroParaT*) calloc(1,sizeof(macroParaT)); 
			//memset($$,0,sizeof(macroParaT));
			$$->string = $1;
			$$->isLocal =0;
	}
| LOCALLABEL {
			$$=(macroParaT*) calloc(1,sizeof(macroParaT)); 
			//memset($$,0,sizeof(macroParaT));
			$$->string = $1;
			$$->isLocal =1;
}
;


// note that labelLine need not '\n' for following MNE

line:
 moduleLine '\n' { $$=$1;}
| ends { $$=NULL;}
| areaLine  '\n' {$$=$1; parserArea=newArea($$->m.area.name, $$->m.area.isABS, $$->m.area.isOVL,
		 $$->m.area.region);
	topModule.areas = appendArea(topModule.areas, parserArea);
	

}
| labelLine   {
	$$=$1;
	//newline_state_change(1);
	}
| callFPTRLine {$$=$1;}
| equLine   {$$=$1;
if(appendEQU($$,parserArea)!=0)
{
	fprintf(stderr,"Assembler Error: EQU fail  %s\n", getLocTree());
	exit(5);
}


 }
| instrLine '\n' {$$=$1;
#ifdef DEBUG
	fprintf(stderr,"instr+newline -- found. at line %d\n", yylineno);
#endif

}
| parachk_line '\n' {$$=$1;}
| defineLine '\n'  {$$=NULL;}
| globlLine '\n'  {$$=$1;}
| dbdsLine '\n'  {$$=$1;}
| org_line '\n'  {$$=$1;}
| func_line '\n'  {$$=$1;}
| func_end_line '\n'  {$$=$1;}
| macroPushEndLine '\n' {$$=$1;}
| macroDef {$$=NULL;
#ifdef DEBUG
	fprintf(stderr, "get macroDef at %d\n", yylineno);
#endif
}
| error '\n' { 
		yyclearin;yyerrok;    
		$$=newEmptyMne();
		$$->type = mneIsErr;
		$$->m.err.src_lineno=yylineno-1;
		$$->m.err.msg = strdup("Syntax error.");
		}
| LABEL '\n' {
		char buf[1024];
		yyclearin;yyerrok;    
		$$=newEmptyMne();
		$$->type = mneIsErr;
		$$->m.err.src_lineno=yylineno-1;
		snprintf(buf,1023,"Mnemonic %s unknown", $1);
		yyerror(buf);
		$$->m.err.msg = strdup(buf);
}
| LABEL LABEL '\n' {
		char buf[1024];
		yyclearin;yyerrok;    
		$$=newEmptyMne();
		$$->type = mneIsErr;
		$$->m.err.src_lineno=yylineno-1;
		snprintf(buf,1023,"Mnemonic %s unknown", $1);
		yyerror(buf);
		$$->m.err.msg = strdup(buf);
}
| LABEL NUM '\n' {
		char buf[1024];
		yyclearin;yyerrok;    
		$$=newEmptyMne();
		$$->type = mneIsErr;
		$$->m.err.src_lineno=yylineno-1;
		snprintf(buf,1023,"Mnemonic %s unknown", $1);
		yyerror(buf);
		$$->m.err.msg = strdup(buf);
}
| LABEL LABEL ','  {
		char buf[1024];
		yyclearin;yyerrok;    
		$$=newEmptyMne();
		$$->type = mneIsErr;
		$$->m.err.src_lineno=yylineno-1;
		snprintf(buf,1023,"Mnemonic %s unknown", $1);
		yyerror(buf);
		$$->m.err.msg = strdup(buf);
}
| LABEL LABEL LABEL ','  {
		char buf[1024];
		yyclearin;yyerrok;    
		$$=newEmptyMne();
		$$->type = mneIsErr;
		$$->m.err.src_lineno=yylineno-1;
		snprintf(buf,1023,"Mnemonic %s unknown", $1);
		yyerror(buf);
		$$->m.err.msg = strdup(buf);
}
| LABEL NUM ','  {
		char buf[1024];
		yyclearin;yyerrok;    
		$$=newEmptyMne();
		$$->type = mneIsErr;
		$$->m.err.src_lineno=yylineno-1;
		snprintf(buf,1023,"Mnemonic %s unknown", $1);
		yyerror(buf);
		$$->m.err.msg = strdup(buf);
}
| LABEL LABEL NUM ','  {
		char buf[1024];
		yyclearin;yyerrok;    
		$$=newEmptyMne();
		$$->type = mneIsErr;
		$$->m.err.src_lineno=yylineno-1;
		snprintf(buf,1023,"Mnemonic %s unknown", $1);
		yyerror(buf);
		$$->m.err.msg = strdup(buf);
}
| '\n' {$$=NULL;}
;

macroDef:
macroDefHeadLineBaseA MACROTAIL '\n'  {
#ifdef DEBUG
	fprintf(stderr, "get macroDef 1 at line %d\n", 
	yylineno);
#endif
	defined_macros[defined_macro_num++]=$1;
	$$=$1;
}
| macroDefHeadLineParaA MACROTAIL '\n' {
#ifdef DEBUG
	fprintf(stderr, "get macroDef 2 at line %d\n", 
	yylineno);
#endif
	defined_macros[defined_macro_num++]=$1;
	$$=$1;
}
| macroDefHeadLineReptA MACROTAIL '\n' {
	// what a job .... 
	char buf[65536];
	int i, j;
	int macroLineNo=0;
	MNE * newrpt;
#ifdef DEBUG
	fprintf(stderr, "get macroDef 3 at line %d\n", 
	yylineno);
#endif
	
	if(macro_level < 0 )
		macroRootLoc = getLocTree();
	{if(strcmp(opcode_fname,now_lex_fname)) opcode_fname=strdup(now_lex_fname);};opcode_lineno=yylineno-1; // lineno after ENDM!!
	// repeat directly, no new macro defined!!
	//defined_macros[defined_macro_num++]=$1;
	// push the macro by repeat , with macrohead, push, and tail
	// now we evaluate the expression
	$1->paras->exp=simplifyExp($1->paras->exp, getLocTree());
	if($1->paras->exp->type!=ExpIsNumber)
	{
		fprintf(stderr,"Assembler Error: repeat number cannot solved %s\n", getLocTree());
		exit(-1003);
	}
	j = $1->paras->exp->exp.number;
	// we push a macromark now
	newrpt = newEmptyMne();
	newrpt ->type = mneIsMacro;
	newrpt ->m.asm_macro.level = macro_level+1;
	newrpt ->m.asm_macro.src_lineno = opcode_lineno;
	
	//macroref_lineno = opcode_lineno;
	if(macro_level<0)
	{
		newrpt->m.asm_macro.src_lineno=opcode_lineno;
		macroissue_lineno=opcode_lineno;
	}
	else
	{
		newrpt->m.asm_macro.src_lineno=macroissue_lineno;
		newrpt->m.asm_macro.upper_macro_offset = macro_used_line[macro_level];
	}
	macroid_stack[macro_level+1]=defined_macro_num;
	$1->macroRootLoc=macroRootLoc;
	defined_macros[defined_macro_num++]=$1;
	macroInsLocTree[macro_ins_num]=getLocTree(); // before level +1
	macroInsMacroID[macro_ins_num]=defined_macro_num-1;
	macro_ins_num++;
	macro_level++;
	if(macro_level==0)
	{
		mrep_str_num[0]=0;
		mrep_str_local[0]=macroLocalCount++;
	}
	else
	{
		mrep_str_num[macro_level]=mrep_str_num[macro_level-1];
		mrep_str_local[macro_level]=macroLocalCount++;
	}
	if(macro_level>0)
		newrpt->m.asm_macro.upper_macro_offset = macro_used_line[macro_level-1];
	buf[0]='\0';

	// add a macro use record
	

	for(i=0;i<j;i++)
	{
	srcLine *sp=$1->lines;

		while(sp)
		{
			char linenobuf[100];
			snprintf(linenobuf,100,"; RPT lineno: %d, RPT Count: %d \n", sp->lineno, i);
			strncat(buf,sp->line,40960); 
			strncat(buf,linenobuf,40960);
			macroLineNo++;
			sp=sp->next;
		}
	}
	push_replaced_macro(buf,1,65536);
	newrpt->m.asm_macro.macro_id = defined_macro_num-1;
	newrpt->m.asm_macro.total_lineno=macroLineNo;
	macro_left_line[macro_level]=macroLineNo;
	macro_used_line[macro_level]=0;
	macroid_stack[macro_level]=defined_macro_num-1;
	oldMacroBufs[macro_level]=strdup(buf);
	if(nowMNE==NULL)
	{
		nowMNE=newrpt;
		topModule.mneList=nowMNE;
	}
	else
	{
		nowMNE->next=newrpt;
		nowMNE=newrpt;
	}

	$$=$1;
}
;

macroDefHeadLineBaseA:
 macroDefHeadLineBase '\n' MACROLINE '\n' { 
#ifdef DEBUG
	fprintf(stderr, "get macroDefHeadLineBaseA at line %d\n", 
	yylineno);
#endif
    srcLine *n = newSrcLine($3,-1);
	appendSrcLine2(&($1->lines),&($1->linestail), n);
	$$=$1;}
| macroDefHeadLineBaseA MACROLINE '\n' { 
#ifdef DEBUG
	fprintf(stderr, "get macroDefHeadLineBaseA 2 at line %d\n", 
	yylineno);
#endif
    srcLine *n = newSrcLine($2,-1);
	appendSrcLine2(&($1->lines),&($1->linestail), n);
	$$=$1;}
| macroDefHeadLineBaseA  '\n' {  // empty line
#ifdef DEBUG
	fprintf(stderr, "get macroDefHeadLineBaseA 3 at line %d\n", 
	yylineno);
#endif
	$$=$1;}
;

macroDefHeadLineBase:
	MACROHEAD LABEL  { 
		$$ = newMacro($2,1, opcode_lineno,now_lex_fname ); 

#ifdef DEBUG
	fprintf(stderr, "get macroDefHeadLineBase at line %d\n", 
	yylineno);
#endif

}
|macroDefHeadLineBase  '\n'
{
#ifdef DEBUG
	fprintf(stderr, "get macroDefHeadLineBase2 at line %d\n", 
	yylineno);
#endif
	$$=$1;
}
;

macroDefHeadLineParaA:
 macroDefHeadLinePara '\n' MACROLINE '\n' { 
#ifdef DEBUG
	fprintf(stderr, "get macroDefHeadLineParaA 1 at line %d\n", 
	yylineno);
#endif
    srcLine *n = newSrcLine($3,-1);
	appendSrcLine2(&($1->lines),&($1->linestail), n);
	$$=$1;}
| macroDefHeadLineParaA MACROLINE '\n' { 
#ifdef DEBUG
	fprintf(stderr, "get macroDefHeadLineParaA 2 at line %d\n", 
	yylineno);
#endif
    srcLine *n = newSrcLine($2,-1);
	appendSrcLine2(&($1->lines),&($1->linestail), n);
	$$=$1;}
| macroDefHeadLineParaA '\n' {  // empty line
#ifdef DEBUG
	fprintf(stderr, "get macroDefHeadLineParaA 3 at line %d\n", 
	yylineno);
#endif
	$$=$1;}
;

macroDefHeadLinePara:
	macroDefHeadLineBase macro_para {
#ifdef DEBUG
	fprintf(stderr, "get macroDefHeadLinePara 1 at line %d\n", 
	yylineno);
#endif
	$1->paras = newEmptyDataItem();
	$1->paras->exp = newSymbolExp($2->string,yylineno);
	$1->paras->flag = $2->isLocal;
	$$=$1;
}
| macroDefHeadLinePara ',' macro_para {
#ifdef DEBUG
	fprintf(stderr, "get macroDefHeadLinePara 2 at line %d\n", 
	yylineno);
#endif
	dataItem *p=$1->paras;
		while(p->next) p=p->next;
		p->next = newEmptyDataItem();
		p->next->exp = newSymbolExp($3->string, opcode_lineno);
		p->next->flag = $3->isLocal; // local label defined
		$$=$1;
}
;

macroDefHeadLineReptA:
 macroDefHeadLineRept '\n' MACROLINE '\n' { 
#ifdef DEBUG
	fprintf(stderr, "get macroDefHeadLineReptA1 at line %d\n", 
	yylineno);
#endif
    srcLine *n = newSrcLine($3,yylineno-1);
	appendSrcLine2(&($1->lines),&($1->linestail), n);
	$$=$1;}
| macroDefHeadLineReptA MACROLINE '\n' { 
#ifdef DEBUG
	fprintf(stderr, "get macroDefHeadLineReptA2 at line %d\n", 
	yylineno);
#endif
    srcLine *n = newSrcLine($2,yylineno-1);
	appendSrcLine2(&($1->lines),&($1->linestail), n);
	$$=$1;}
;

macroDefHeadLineRept:
	MACROREPHEAD expr {
	char newName[100];
#ifdef DEBUG
	fprintf(stderr, "get macroDefHeadLineRpt at line %d\n", 
	yylineno);
#endif
	sprintf(newName,"__RPT__%d",rpt_id++);
	$$=newMacro(newName,0, opcode_lineno, now_lex_fname); // declare and issue are the same lineno
	$$->used=1;
	$$->isRPT=1;
	$$->paras = newEmptyDataItem();
	$$->paras->exp = $2;
}
;

numpn:  '-' NUM { $$ = - $2; }
| NUM {$$=$1;}
;

func_line:
	FUNCHEAD LABEL  { $$ = newEmptyMne();
		$$ ->type = mneIsFuncRec;
		$$ ->m.func.paraOnStack = $1;
		$$ ->m.func.lineno = opcode_lineno;
		$$ ->m.func.declaredParamSize=-1;
		$$ ->m.func.returnSize=-1;
		strncpy($$->m.func.name,$2,MAXSTRSIZE0-1);
	}
|   FUNCHEAD LABEL callList { $$ = newEmptyMne();
		$$ ->type = mneIsFuncRec;
		$$ ->m.func.lineno = opcode_lineno;
		$$ ->m.func.paraOnStack = $1;
		$$ ->m.func.declaredParamSize=-1;
		$$ ->m.func.returnSize=-1;
		strncpy($$->m.func.name,$2,MAXSTRSIZE0-1);
		$$ ->m.func.callList = $3;
		}
|   FUNCHEAD LABEL localList { 
		$$ = newEmptyMne();
		$$ ->type = mneIsFuncRec;
		$$ ->m.func.paraOnStack = $1;
		$$ ->m.func.lineno = opcode_lineno;
		$$ ->m.func.declaredParamSize=-1;
		$$ ->m.func.returnSize=-1;
		strncpy($$->m.func.name,$2,MAXSTRSIZE0-1);
		$$ ->m.func.localList = $3;
}
|   FUNCHEAD LABEL callList localList { $$ = newEmptyMne();
		$$ ->type = mneIsFuncRec;
		$$ ->m.func.lineno = opcode_lineno;
		$$ ->m.func.paraOnStack = $1;
		$$ ->m.func.declaredParamSize=-1;
		$$ ->m.func.returnSize=-1;
		strncpy($$->m.func.name,$2,MAXSTRSIZE0-1);
		$$ ->m.func.callList = $3;
		$$ ->m.func.localList = $4;
		}
| FUNCHEAD   LABEL ':' NUM { $$ = newEmptyMne();
		$$ ->type = mneIsFuncRec;
		$$ ->m.func.paraOnStack = $1;
		$$ ->m.func.declaredParamSize=$4;
		$$ ->m.func.returnSize=-1;
		$$ ->m.func.lineno = opcode_lineno;
		strncpy($$->m.func.name,$2,MAXSTRSIZE0-1);
	}
|   FUNCHEAD  LABEL ':' NUM callList { $$ = newEmptyMne();
		$$ ->type = mneIsFuncRec;
		$$ ->m.func.lineno = opcode_lineno;
		$$ ->m.func.declaredParamSize=$4;
		$$ ->m.func.returnSize=-1;
		$$ ->m.func.paraOnStack = $1;
		strncpy($$->m.func.name,$2,MAXSTRSIZE0-1);
		$$ ->m.func.callList = $5;
		}
|   FUNCHEAD LABEL ':' NUM  localList { 
		$$ = newEmptyMne();
		$$ ->type = mneIsFuncRec;
		$$ ->m.func.paraOnStack = $1;
		$$ ->m.func.declaredParamSize=$4;
		$$ ->m.func.returnSize=-1;
		$$ ->m.func.lineno = opcode_lineno;
		strncpy($$->m.func.name,$2,MAXSTRSIZE0-1);
		$$ ->m.func.localList = $5;
}
|   FUNCHEAD LABEL ':' NUM callList localList { $$ = newEmptyMne();
		$$ ->type = mneIsFuncRec;
		$$ ->m.func.lineno = opcode_lineno;
		$$ ->m.func.paraOnStack = $1;
		$$ ->m.func.returnSize=-1;
		$$ ->m.func.declaredParamSize=$4;
		strncpy($$->m.func.name,$2,MAXSTRSIZE0-1);
		$$ ->m.func.callList = $5;
		$$ ->m.func.localList = $6;
		}
| FUNCHEAD   LABEL ':' RETURNSIZEMARK ':' numpn ':' NUM { $$ = newEmptyMne();
		$$ ->type = mneIsFuncRec;
		$$ ->m.func.paraOnStack = $1;
		$$ ->m.func.declaredParamSize=$8;
		$$ ->m.func.returnSize = $6;
		$$ ->m.func.lineno = opcode_lineno;
		strncpy($$->m.func.name,$2,MAXSTRSIZE0-1);
	}
|   FUNCHEAD  LABEL ':' RETURNSIZEMARK ':' numpn ':' NUM callList { $$ = newEmptyMne();
		$$ ->type = mneIsFuncRec;
		$$ ->m.func.lineno = opcode_lineno;
		$$ ->m.func.declaredParamSize=$8;
		$$ ->m.func.paraOnStack = $1;
		strncpy($$->m.func.name,$2,MAXSTRSIZE0-1);
		$$ ->m.func.callList = $9;
		$$ ->m.func.returnSize = $6;
		}
|   FUNCHEAD LABEL ':' RETURNSIZEMARK ':' numpn ':' NUM  localList { 
		$$ = newEmptyMne();
		$$ ->type = mneIsFuncRec;
		$$ ->m.func.paraOnStack = $1;
		$$ ->m.func.declaredParamSize=$8;
		$$ ->m.func.lineno = opcode_lineno;
		strncpy($$->m.func.name,$2,MAXSTRSIZE0-1);
		$$ ->m.func.localList = $9;
		$$ ->m.func.returnSize = $6;
}
|   FUNCHEAD LABEL ':' RETURNSIZEMARK ':' numpn ':' NUM callList localList { $$ = newEmptyMne();
		$$ ->type = mneIsFuncRec;
		$$ ->m.func.lineno = opcode_lineno;
		$$ ->m.func.paraOnStack = $1;
		$$ ->m.func.declaredParamSize=$8;
		strncpy($$->m.func.name,$2,MAXSTRSIZE0-1);
		$$ ->m.func.callList = $9;
		$$ ->m.func.localList = $10;
		$$ ->m.func.returnSize = $6;
		}
;

func_end_line:
	FUNCENDHEAD LABEL  { 
	$$ = newEmptyMne();
		$$ ->type = mneIsFuncEnd;
		$$ ->m.func_end.lineno = opcode_lineno;
		strncpy($$->m.func_end.name,$2,MAXSTRSIZE0-1);
}
;

callList:
	':' CALLMARK ':' LABEL { 
		$$=newEmptyDataItem();
		$$->exp = newSymbolExp($4, opcode_lineno);
}
|  callList ':' CALLMARK ':' LABEL {
	dataItem *p=$1;
	while(p->next) p=p->next;
		p->next = newEmptyDataItem();
		p->next->exp = newSymbolExp($5, opcode_lineno);
		$$=$1;
}
|
':' CALLMARK ':' NUM { 
		$$=newEmptyDataItem();
		$$->exp = newConstExp($4, opcode_lineno);
}
|  callList ':' CALLMARK ':' NUM {
	dataItem *p=$1;
	while(p->next) p=p->next;
		p->next = newEmptyDataItem();
		p->next->exp = newConstExp($5, opcode_lineno);
		$$=$1;
}
;


localList:
	':' LOCALMARK ':' LABEL { 
		$$=newEmptyDataItem();
		$$->exp = newSymbolExp($4, opcode_lineno);
}
|  localList ':' LOCALMARK ':' LABEL {
	dataItem *p=$1;
	while(p->next) p=p->next;
		p->next = newEmptyDataItem();
		p->next->exp = newSymbolExp($5, opcode_lineno);
		$$=$1;
} |
':' XLOCALMARK ':' LABEL { 
		$$=newEmptyDataItem();
		$$->exp = newSymbolExp($4, opcode_lineno);
		$$->exp->ExpSymIsX=1;
}
|  localList ':' XLOCALMARK ':' LABEL {
	dataItem *p=$1;
	while(p->next) p=p->next;
		p->next = newEmptyDataItem();
		p->next->exp = newSymbolExp($5, opcode_lineno);
		p->next->exp->ExpSymIsX=1;
		$$=$1;
}

;




org_line:
	ORG	NUM {
		$$ = newEmptyMne();
		$$ ->type = mneIsORG;
		$$ ->m.org.lineno=opcode_lineno;
		$$ ->m.org.offset = $2;
	}
	;
equLine:
	LABEL EQU expr {
		$$ = newEmptyMne();
		$$ ->type=mneIsEQU;
		$$ ->m.equ.nameHash=symbolCaseSensitivity?hashi($1):hash($1);
		$$ ->m.equ.lineno=opcode_lineno;
		strncpy($$ ->m.equ.name, $1, MAXSTRSIZE0-1);
		$$->m.equ.valueexp = $3;
	}
| LABEL ':' EQU expr {
		$$ = newEmptyMne();
		$$ ->type=mneIsEQU;
		$$ ->m.equ.nameHash=symbolCaseSensitivity?hashi($1):hash($1);
		$$ ->m.equ.lineno=opcode_lineno;
		strncpy($$ ->m.equ.name, $1, MAXSTRSIZE0-1);
		$$->m.equ.valueexp = $4;
	}
| 	LABEL '=' expr {
		$$ = newEmptyMne();
		$$ ->type=mneIsEQU;
		$$ ->m.equ.lineno=opcode_lineno;
		strncpy($$ ->m.equ.name, $1, MAXSTRSIZE0-1);
		$$->m.equ.valueexp = $3;
	};
	
macroPushEndLine:
MACROPUSHEND {
	$$ = newEmptyMne();
	$$->type = mneIsMacro;
	$$->m.asm_macro.level = macro_level; // +1 before real ?
	$$->m.asm_macro.src_lineno=macroissue_lineno;
	$$->m.asm_macro.upper_macro_offset = macro_used_line[macro_level];
	$$->m.asm_macro.macro_end = 1; // this is end!!
}
;	


dbdsLine:
	DS NUM { 
		$$ = newEmptyMne();
		$$ ->type=mneIsDBDS;
		$$ ->m.dbds.lineno=opcode_lineno;
		$$ ->m.dbds.hasSize=1;
		$$ ->m.dbds.size = $2;
	};
|	BYTEDEF dataItems {
		$$ = newEmptyMne();
		$$ ->type=mneIsDBDS;
		$$ ->m.dbds.lineno=opcode_lineno;
		$$ ->m.dbds.hasContent=1;
		$$ ->m.dbds.size = 1;
		$$ ->m.dbds.items=$2;
	};
|	ASCII dataItems {
		$$ = newEmptyMne();
		$$ ->type=mneIsDBDS;
		$$ ->m.dbds.lineno=opcode_lineno;
		$$ ->m.dbds.hasContent=1;
		$$ ->m.dbds.size = 1;
		$$ ->m.dbds.items=$2;
}
|	DW dataItems {
		$$ = newEmptyMne();
		$$ ->type=mneIsDBDS;
		$$ ->m.dbds.lineno=opcode_lineno;
		$$ ->m.dbds.hasContent=1;
		$$ ->m.dbds.size = 2;
		$$ ->m.dbds.items=$2;
}
;

	

dataItems:
	expr{ 
		$$=newEmptyDataItem();
		$$->exp = $1;
	} 
|    dataItems ',' expr{
	$$ = appendExp($1,$3);
}
;
	

globlLine:
	GLOLOC dataItems {
	$$=newEmptyMne();
	$$->type = mneIsGlobalMark;
	$$->m.globl.lineno=opcode_lineno;
	$$->m.globl.uuid=__COUNTER__;
	$$->m.globl.symIsGlobl=$1;
	
	$$->m.globl.items=$2;
};

labelLine:
	LABEL ':' {
	{if(strcmp(opcode_fname,now_lex_fname)) opcode_fname=strdup(now_lex_fname);};opcode_lineno=label_lineno; // though there is no opcode
	$$=newEmptyMne();
	$$->type = mneIsLabelMark;
	$$->m.label.uuid=__COUNTER__;
	$$->m.label.lineno=label_lineno;// it is label
	strncpy($$->m.label.name,$1,MAXSTRSIZE0-1);
#ifdef DEBUG
	fprintf(stderr,"labelLine #1 found. at line %d\n", yylineno);
#endif
	
} 
|  END ':' { // we give END can become label
	$$=newEmptyMne();
	$$->type = mneIsLabelMark;
	$$->m.label.uuid=__COUNTER__;
	$$->m.label.lineno=label_lineno;// it is label
	strncpy($$->m.label.name,$1,MAXSTRSIZE0-1); // end will bring back yytext
#ifdef DEBUG
	fprintf(stderr,"labelLine #2 -- found. at line %d\n", yylineno);
#endif
}
;

callFPTRLine:
	CALLFPTR { $$= newEmptyMne(); 
			$$->type = mneIsCALLFPTR;
			$$->m.callFPTR.lineno=opcode_lineno;
	}
	;

instrLine:
	instrFDA {$$=$1;}
	| instrLIT {$$=$1;}
	| instrFBA {$$=$1;}
	| instrN12 {$$=$1; 
#ifdef DEBUG
	fprintf(stderr,"N12 line -- found. at line %d\n", yylineno);
#endif
	
	}
	| instrNOPR {$$=$1;
#ifdef DEBUG
	fprintf(stderr,"NOPR line -- found. at line %d\n", yylineno);
#endif	
	}
	| instrFA {$$=$1;}
	| instrFSFD {$$=$1;}
	| instrADDBIT {$$=$1;}
	| instrN8 {$$=$1;}
	| instrN11 {$$=$1;}
	| instrKF {$$=$1;}
	| instrLBSR {$$=$1;}
	| instrTBLR {$$=$1;}
	| specialInstr{$$=$1;}
;
specialInstr:
	ADDFSR NUM ',' expr {
	if($2!=0 && $2!=1 && $2!=2)
	{
		char buf[256];
		sprintf(buf,"Line %s fsr %d error.\n",getLocTree(), $2);
		yyerror(buf);
		$$=NULL;
	}else
	{
	$$=newEmptyMne();
	$$->type=mneIsInstruction;
	$$->m.inst.lineno=opcode_lineno;
	$$->m.inst.wordnum = 1;
	$$->m.inst.opcode1=0xc400|($2<<6);
	$$->m.inst.addrMode=addr_addfsr;
	$$->m.inst.operand = $4;
	}
	}
|	ADDULNK expr {
	$$=newEmptyMne();
	$$->type=mneIsInstruction;
	$$->m.inst.lineno=opcode_lineno;
	$$->m.inst.wordnum = 1;
	$$->m.inst.opcode1=0xc4c0;
	$$->m.inst.addrMode=addr_addulnk;
	$$->m.inst.operand = $2;
}
|	MVSF expr ',' expr {
	$$=newEmptyMne();
	$$->type=mneIsInstruction;
	$$->m.inst.lineno=opcode_lineno;
	$$->m.inst.wordnum = 2;
	$$->m.inst.opcode1=0xc600;
	$$->m.inst.addrMode=addr_mvsf;
	$$->m.inst.operand = $2;
	$$->m.inst.operand1 = $4;
}
| MVSS expr ',' expr {
	$$=newEmptyMne();
	$$->type=mneIsInstruction;
	$$->m.inst.lineno=opcode_lineno;
	$$->m.inst.wordnum = 2;
	$$->m.inst.opcode1=0xc680;
	$$->m.inst.addrMode=addr_mvss;
	$$->m.inst.operand = $2;
	$$->m.inst.operand1 = $4;
}
;


instrLBSR: 
	LBSROP expr {
	$$=newEmptyMne();
	$$->type=mneIsInstruction;
	$$->m.inst.lineno=opcode_lineno;
	$$->m.inst.wordnum = 1;
	$$->m.inst.opcode1=$1;
	$$->m.inst.addrMode=addr_lbsr;
	$$->m.inst.operand = $2;
	
};
	
instrTBLR: 
	TBLROP '*' {
	$$=newEmptyMne();
	$$->type=mneIsInstruction;
	$$->m.inst.lineno=opcode_lineno;
	$$->m.inst.wordnum = 1;
	$$->m.inst.opcode1=$1;
	$$->m.inst.addrMode=addr_tblr;
}
|
	TBLROP '*' '+' {
	$$=newEmptyMne();
	$$->type=mneIsInstruction;
	$$->m.inst.lineno=opcode_lineno;
	$$->m.inst.wordnum = 1;
	$$->m.inst.opcode1=$1|1;
	$$->m.inst.addrMode=addr_tblr;
};
;

instrFDA: 
	FDAOP expr ',' expr ',' expr {
	//range_chk(1,$4);
	//range_chk(1,$6);
	$$=newEmptyMne();
	$$->type=mneIsInstruction;
	$$->m.inst.lineno=opcode_lineno;
	$$->m.inst.wordnum = 1;
	$$->m.inst.opcode1=$1;
	$$->m.inst.bord=$4;
	$$->m.inst.aexp=$6;
	$$->m.inst.addrMode=addr_fda;
	$$->m.inst.operand = $2;
	
}|FDAOP expr ','  expr { // left f,d, a is 0
	//range_chk(1,$4);
	//range_chk(1,$6);
	$$=newEmptyMne();
	$$->type=mneIsInstruction;
	$$->m.inst.lineno=opcode_lineno;
	$$->m.inst.wordnum = 1;
	$$->m.inst.opcode1=$1;
	$$->m.inst.bord=$4;
	$$->m.inst.aexp=newConstExp(0,opcode_lineno);
	$$->m.inst.addrMode=addr_fda;
	$$->m.inst.operand = $2;
	
}
| SWPF expr ',' expr ',' expr { // swpf means d is 0
	//range_chk(1,$6);
	$$=newEmptyMne();
	$$->type=mneIsInstruction;
	$$->m.inst.lineno=opcode_lineno;
	$$->m.inst.wordnum = 1;
	$$->m.inst.opcode1=0x6000;
	$$->m.inst.bord=$4;
	$$->m.inst.aexp=$6;
	$$->m.inst.addrMode=addr_fda;
	$$->m.inst.operand = $2;
}| SWPF expr ',' expr {
	$$=newEmptyMne();
	$$->type=mneIsInstruction;
	$$->m.inst.lineno=opcode_lineno;
	$$->m.inst.wordnum = 1;
	$$->m.inst.opcode1=0x6000;
	$$->m.inst.bord=newConstExp(0, opcode_lineno);
	$$->m.inst.aexp=$4;
	$$->m.inst.addrMode=addr_fda;
	$$->m.inst.operand = $2;

} 
;
instrKF: 
	KFOP expr ',' expr  {
	//range_chk(3,$4);
	$$=newEmptyMne();
	$$->type=mneIsInstruction;
	$$->m.inst.lineno=opcode_lineno;
	$$->m.inst.wordnum = 2;
	$$->m.inst.opcode1=$1;
	$$->m.inst.opcode2=0xf000;
	$$->m.inst.aexp = $4;
	$$->m.inst.addrMode=addr_kf;
	$$->m.inst.operand = $2;
	
};

instrN8: 
	N8OP expr  {
	//range_chk(1,$4);
	//range_chk(1,$6);
	$$=newEmptyMne();
	$$->type=mneIsInstruction;
	$$->m.inst.lineno=opcode_lineno;
	$$->m.inst.wordnum = 1;
	$$->m.inst.opcode1=$1;
	$$->m.inst.addrMode=addr_n8;
	$$->m.inst.operand = $2;
	
};
instrN11: 
	N11OP expr  {
	//range_chk(1,$4);
	//range_chk(1,$6);
	$$=newEmptyMne();
	$$->type=mneIsInstruction;
	$$->m.inst.lineno=opcode_lineno;
	$$->m.inst.wordnum = 1;
	$$->m.inst.opcode1=$1;
	$$->m.inst.addrMode=addr_n11;
	$$->m.inst.operand = $2;
	
};


instrADDBIT: 
	ADDBITOP  NUM  {
	range_chk(1,$2);
	$$=newEmptyMne();
	$$->type=mneIsInstruction;
	$$->m.inst.lineno=opcode_lineno;
	$$->m.inst.wordnum = 1;
	$$->m.inst.opcode1=$1|$2; // OR directly
	$$->m.inst.addrMode=addr_addbit;
}
| ADDBITOP {
$$=newEmptyMne();
	$$->type=mneIsInstruction;
	$$->m.inst.lineno=opcode_lineno;
	$$->m.inst.wordnum = 1;
	// if RETI, default is RETI 1,if RET, defautl is RET 0, RETI is 8
	$$->m.inst.opcode1=($1==8)?9:$1;
	$$->m.inst.addrMode=addr_addbit;
}
;

instrFSFD: 
	FSFDOP expr ',' expr {
	$$=newEmptyMne();
	$$->type=mneIsInstruction;
	$$->m.inst.lineno=opcode_lineno;
	$$->m.inst.wordnum = 2;
	$$->m.inst.opcode1=$1;
	$$->m.inst.opcode2=0xf000;
	$$->m.inst.addrMode=addr_fsfd;
	
	$$->m.inst.operand = $2;
	$$->m.inst.operand1 = $4;
};



instrFA: 
	FAOP expr ',' expr {
	//range_chk(1,$4);
	$$=newEmptyMne();
	$$->type=mneIsInstruction;
	$$->m.inst.lineno=opcode_lineno;
	$$->m.inst.wordnum = 1;
	$$->m.inst.opcode1=$1;
	$$->m.inst.aexp=$4;
	$$->m.inst.addrMode=addr_fa;
	
	$$->m.inst.operand = $2;
	$$->m.inst.operand1 = $4;
}
| FAOP expr { // a=0
	$$=newEmptyMne();
	$$->type=mneIsInstruction;
	$$->m.inst.lineno=opcode_lineno;
	$$->m.inst.wordnum = 1;
	$$->m.inst.opcode1=$1;
	$$->m.inst.bord=newConstExp(0,opcode_lineno);
	$$->m.inst.addrMode=addr_fa;
	$$->m.inst.aexp=newConstExp(0,opcode_lineno); 
	$$->m.inst.operand = $2;
	$$->m.inst.operand1 = newConstExp(0,opcode_lineno);
}
;

instrNOPR: 
	NOPROP  {
	$$=newEmptyMne();
	$$->type=mneIsInstruction;
	$$->m.inst.lineno=opcode_lineno;
	$$->m.inst.wordnum = 1;
	$$->m.inst.opcode1=$1;
	$$->m.inst.addrMode=addr_nopr;
};

instrN12:
N12OP NUM {
	$$=newEmptyMne();
	$$->type=mneIsInstruction;
	$$->m.inst.lineno=opcode_lineno;
	$$->m.inst.wordnum = 2;
	$$->m.inst.opcode1=$1;
	$$->m.inst.opcode2=0xf000;
	$$->m.inst.addrMode=addr_n12;
	
	$$->m.inst.operand = newConstExp($2, opcode_lineno);
	// we need pop state here
	pop_state();
}
|	N12OP expr {
	$$=newEmptyMne();
	$$->type=mneIsInstruction;
	$$->m.inst.lineno=opcode_lineno;
	$$->m.inst.wordnum = 2;
	$$->m.inst.opcode1=$1;
	$$->m.inst.opcode2=0xf000;
	$$->m.inst.addrMode=addr_n12;
	
	$$->m.inst.operand = $2;
}
| instrN12 ',' NUM { // 0 or 1 only
	$$=$1;
	if($$->m.inst.opcode1!=0x2c000)		
	{
		yyerror("wrong mnenomic");
	}else
	{
		if($3==1)
			$$->m.inst.opcode1|=0x100;
		else if($3!=0)
		{
			yyerror("Unknown Number");
		}
	}
}
;

instrLIT:
	LITOP expr {
	//range_chk(255,$1);
	$$=newEmptyMne();
	$$->type=mneIsInstruction;
	$$->m.inst.lineno=opcode_lineno;
	$$->m.inst.wordnum = 1;
	$$->m.inst.opcode1=$1;
	$$->m.inst.addrMode=addr_lit;
	$$->m.inst.operand = $2;
};

instrFBA:
	FBAOP expr ',' expr ',' expr {
	//range_chk(7,$4);
	//range_chk(1,$6);
	$$=newEmptyMne();
	$$->type=mneIsInstruction;
	$$->m.inst.lineno=opcode_lineno;
	$$->m.inst.wordnum = 1;
	$$->m.inst.opcode1=$1;
	$$->m.inst.bord=$4;
	$$->m.inst.aexp=$6;
	$$->m.inst.addrMode=addr_fba;
	$$->m.inst.operand = $2;
	
}
	| FBAOP expr ',' expr {
	//range_chk(7,$4);
	$$=newEmptyMne();
	$$->type=mneIsInstruction;
	$$->m.inst.lineno=opcode_lineno;
	$$->m.inst.wordnum = 1;
	$$->m.inst.opcode1=$1;
	$$->m.inst.bord=$4;
	$$->m.inst.aexp=newConstExp(0,opcode_lineno);
	$$->m.inst.addrMode=addr_fba;
	$$->m.inst.operand = $2;
	
}
;

expr: FSR2H '-' NUM {
	$$=newEmptyExp(yylineno);
		$$ -> type = ExpIsStackOffset;
		$$ ->exp.number = $3;
}|
 FSR2H '+' NUM {
	$$=newEmptyExp(yylineno);
		$$ -> type = ExpIsStackOffset;
		$$ ->exp.number = -1*$3;
}

|	NUM { 
		$$ = newEmptyExp(yylineno);
		$$ -> type = ExpIsNumber;
		$$ -> exp.number = $1;
	}

|	QUOSTR {
		$$ = newEmptyExp(yylineno);
		$$ -> type = ExpIsASCII;
		strncpy($$->exp.symbol, $1,MAXSTRSIZE0-1);
	}
|	LABEL {
		$$ = newEmptyExp(label_lineno);
		$$ -> type = ExpIsSymbol;
		strncpy($$ -> exp.symbol,$1,MAXSTRSIZE0-1);
		$$ -> symHash = symbolCaseSensitivity?hashi($1):hash($1);
	}
|	'(' expr ')' {$$ = $2; }
|	'#' expr  {$$ = $2; }
|	'-' expr {
	$$ = newEmptyExp(opcode_lineno);
	$$ ->type = ExpIsTree;
	$$ ->exp.tree.left = newEmptyExp(opcode_lineno);
	$$ ->exp.tree.left -> type = ExpIsNumber;
	$$ ->exp.tree.left -> exp.number = 0;
	$$ ->exp.tree.op = '-';
	$$ ->exp.tree.right = $2;
}
|   expr CEQ expr {
	$$ = newEmptyExp(opcode_lineno);
	$$ -> type = ExpIsTree;
	$$ -> exp.tree.left = $1;
	$$ -> exp.tree.right = $3;
	$$ -> exp.tree.op = '=';

}
|   expr CNE expr {
	$$ = newEmptyExp(opcode_lineno);
	$$ -> type = ExpIsTree;
	$$ -> exp.tree.left = $1;
	$$ -> exp.tree.right = $3;
	$$ -> exp.tree.op = 'N';

}
|   expr CGT expr {
	$$ = newEmptyExp(opcode_lineno);
	$$ -> type = ExpIsTree;
	$$ -> exp.tree.left = $1;
	$$ -> exp.tree.right = $3;
	$$ -> exp.tree.op = '>';

}
|   expr CGE expr {
	$$ = newEmptyExp(opcode_lineno);
	$$ -> type = ExpIsTree;
	$$ -> exp.tree.left = $1;
	$$ -> exp.tree.right = $3;
	$$ -> exp.tree.op = 'G';

}
|   expr CLT expr {
	$$ = newEmptyExp(opcode_lineno);
	$$ -> type = ExpIsTree;
	$$ -> exp.tree.left = $1;
	$$ -> exp.tree.right = $3;
	$$ -> exp.tree.op = '<';
}
|   expr CLE expr {
	$$ = newEmptyExp(opcode_lineno);
	$$ -> type = ExpIsTree;
	$$ -> exp.tree.left = $1;
	$$ -> exp.tree.right = $3;
	$$ -> exp.tree.op = 'B'; // shift left
}

|	expr '+' expr {
	$$ = newEmptyExp(opcode_lineno);
	$$ -> type = ExpIsTree;
	$$ -> exp.tree.left = $1;
	$$ -> exp.tree.right = $3;
	$$ -> exp.tree.op = '+';
}
|	expr '-' expr {
	$$ = newEmptyExp(opcode_lineno);
	$$ -> type = ExpIsTree;
	$$ -> exp.tree.left = $1;
	$$ -> exp.tree.right = $3;
	$$ -> exp.tree.op = '-';

}
|    expr SHRIGHT expr {
	$$ = newEmptyExp(opcode_lineno);
	$$ -> type = ExpIsTree;
	$$ -> exp.tree.left = $1;
	$$ -> exp.tree.right = $3;
	$$ -> exp.tree.op = 'A';


}
|    expr SHLEFT expr {
	$$ = newEmptyExp(opcode_lineno);
	$$ -> type = ExpIsTree;
	$$ -> exp.tree.left = $1;
	$$ -> exp.tree.right = $3;
	$$ -> exp.tree.op = 'B';


}
| expr '&' expr {
	$$ = newEmptyExp(opcode_lineno);
	$$ -> type = ExpIsTree;
	$$ -> exp.tree.left = $1;
	$$ -> exp.tree.right = $3;
	$$ -> exp.tree.op = '&';
	}
| expr '|' expr {
	$$ = newEmptyExp(opcode_lineno);
	$$ -> type = ExpIsTree;
	$$ -> exp.tree.left = $1;
	$$ -> exp.tree.right = $3;
	$$ -> exp.tree.op = '|';
	}	
| 	HIGH expr {
	$$ = newEmptyExp(opcode_lineno);
	$$ ->type = ExpIsTree;
	$$ ->exp.tree.right = newEmptyExp(opcode_lineno);
	$$ ->exp.tree.right -> type = ExpIsNumber;
	$$ ->exp.tree.right -> exp.number = 8;
	$$ ->exp.tree.op = 'A'; // 'A' means right shift
	$$ ->exp.tree.left = $2;
}
| 	HIGHD2 expr {
	$$ = newEmptyExp(opcode_lineno);
	$$ ->type = ExpIsTree;
	$$ ->exp.tree.right = newEmptyExp(opcode_lineno);
	$$ ->exp.tree.right -> type = ExpIsNumber;
	$$ ->exp.tree.right -> exp.number = 9;
	$$ ->exp.tree.op = 'A'; // 'A' means right shift
	$$ ->exp.tree.left = $2;
}
| 	D2 expr {
	expression *exp;

	exp = newEmptyExp(opcode_lineno);
	exp ->type = ExpIsTree;
	exp ->exp.tree.right = newEmptyExp(opcode_lineno);
	exp ->exp.tree.right -> type = ExpIsNumber;
	exp ->exp.tree.right -> exp.number = 1;
	exp ->exp.tree.op = 'A'; // 'A' means right shift
	exp ->exp.tree.left = $2;
	$$ = newEmptyExp(opcode_lineno);
	$$ ->type = ExpIsTree;
	$$ ->exp.tree.right = newEmptyExp(opcode_lineno);
	$$ ->exp.tree.right -> type = ExpIsNumber;
	$$ ->exp.tree.right -> exp.number = 255;
	$$ ->exp.tree.op = '&'; // &255
	$$ ->exp.tree.left = exp;


}
| 	LOW expr {
	$$ = newEmptyExp(opcode_lineno);
	$$ ->type = ExpIsTree;
	$$ ->exp.tree.right = newEmptyExp(opcode_lineno);
	$$ ->exp.tree.right -> type = ExpIsNumber;
	$$ ->exp.tree.right -> exp.number = 255;
	$$ ->exp.tree.op = '&'; // &255
	$$ ->exp.tree.left = $2;
} 

;
	
	

	

FDAOP:
    ADDC { $$=     0x1400        ;}
    |ADDF { $$=     0x1000       ;}
    |ANDF { $$=     0x2800      ;}
    |ARLC { $$=     0x25800     ;}
    |ARRC { $$=     0x25c00      ;}
    |COMF { $$=     0x2400     ;}
    |DCF { $$=     0x3000    ;}
    |DCSUZ { $$=     0x14400    ;}
    |DCSZ { $$=     0x13400    ;}
    |MVF { $$=      0x6400    ;}
    |INF { $$=     0x3800 ;}
    |INSUZ { $$=     0x14000 ;}
    |INSZ { $$=     0x13C00 ;}
    |IORF { $$=     0x2C00    ;}
    |RLF { $$=      0x4800    ;}
    |RLFC { $$=      0x5400    ;}
    |RRF { $$=      0x4C00    ;}
    |RRFC { $$=      0x5000    ;}
    |SUBC { $$=      0x1C00    ;}
    |SUBF { $$=      0x1800    ;}
    |XORF { $$=      0x2000    ;}
;
LITOP:
    SUBL{$$=      0x0800    ;}
    |MVL{$$=      0x0600    ;}
    |RETL{$$=      0x0500    ;}
    |ADDL{$$=	 0x0400     ;}
    |IORL{$$=     0x0200   ;}
    |MULL{$$=      0x20700    ;}
    |ANDL{$$=     0x0100      ;}
    |XORL{$$=      0x0300    ;}
	|PUSHL{$$=	0xc500;		}
;

FAOP:
     SETF{$$=      0x0A00    ;}
    |CLRF{$$=     0x0C00      ;}
    |CPSE{$$=     0x16C00      ;}
    |CPSG{$$=     0x16800      ;}
    |CPSL{$$=     0x16a00      ;}
    |MULF{$$=      0x20e00    ;}
    |TFSZ{$$=      0x16e00    ;}
;
FBAOP:
     BCF{$$=      0x8000 ;}
    |BSF{$$=      0x9000 ;}
    |BTGF{$$=      0xe000 ;}
    |BTSS{$$=      0x1b000 ;}
    |BTSZ{$$=      0x1a000 ;}
;
LBSROP:
	LBSR {$$=0x20010;}
;
KFOP:
	LDPR {$$=0x2000c;}
;
FSFDOP:
	MVFF {$$=0x2d000;}
;
N12OP:
	MVLP {$$=0x20900;}
	|CALL {$$=0x2C000;}
	|JMP {$$=0x2C200;}
;
NOPROP:
     CWDT{$$=      0x0003    ;}
    |IDLE{$$=      0x0001    ;}
	|DAW {$$=      0x0004    ;}
    |SLP{$$=      0x0002    ;}
    |NOP{$$=      0x0000    ;}
    |RET{$$=      0x000a    ;}
    |POP{$$=      0x0021    ;}
    |PUSH{$$=      0x0020    ;}
;
N8OP:
     JC{$$=      0x27000 ;}
    |JN{$$=      0x27200    ;}
    |JNC{$$=      0x27400    ;}
    |JNN{$$=      0x27600    ;}
    |JZ{$$=      0x27100 ;}
    |JO{$$=      0x27300 ;}
    |JNO{$$=      0x27700    ;}
    |JNZ{$$=      0x27500    ;}
;
N11OP:
     RCALL {$$=      0xc800    ;}
    | RJ {$$=      0x7800    ;}
;
ADDBITOP:	
     RETI {$$=      0x0008    ;}
|	 RET {$$=      0x000A    ;}
;

TBLROP:
     TBLR {$$ = 0x20006;}
;


moduleLine:
	MODULE LABEL  { $$ = newEmptyMne(); 
			$$->type=mneIsModuleD;
			$$->m.module.lineno=opcode_lineno;
			strncpy($$->m.module.name,$2,MAXSTRSIZE0-1);
		      };
areaLine:
	areaBase { $$=$1; $$->m.area.offsetAddr=-1;}
| areaBase '@' NUM
{
	$$=$1;
	$1->m.area.offsetAddr = $3;
};

areaBase:
	AREA  '(' CODEDATASTK ')'  { 
		$$ = newEmptyMne();
		$$->type = mneIsAreaD;
		$$->m.area.isABS=0;
		$$->m.area.isOVL=0;
		//$$->m.area.isSTK=0;
		$$->m.area.region=$3;
		$$->m.area.lineno=opcode_lineno;
		strncpy($$->m.area.name, find_area_name($1), MAXSTRSIZE0-1);
		}
| AREA  '(' CODEDATASTK ',' ABSREL ')' {
		$$ = newEmptyMne();
		$$->type = mneIsAreaD;
		$$->m.area.isABS=$5;
		$$->m.area.isOVL=0;
		//$$->m.area.isSTK=0;
		$$->m.area.region=$3;
		$$->m.area.lineno=opcode_lineno;
		strncpy($$->m.area.name, find_area_name($1), MAXSTRSIZE0-1);
		}
| AREA '(' CODEDATASTK ',' ABSREL ',' CONOVL ')'  {
		$$ = newEmptyMne();
		$$->type = mneIsAreaD;
		$$->m.area.isABS=$5;
		$$->m.area.isOVL=$7;
		//$$->m.area.isSTK=0;
		$$->m.area.region=$3; 
		$$->m.area.lineno=opcode_lineno;
		strncpy($$->m.area.name, find_area_name($1), MAXSTRSIZE0-1);
		//fprintf(stderr,"get area \"%s\"\n", find_area_name($1));
		
}

;

CODEDATASTK:
	CODE {$$=regionCODE;}
	| DATA {$$=regionDATA;}
	| XDATA {$$=regionXDATA;}
	| STK {$$=regionSTK;}
;
ABSREL:
	ABS {$$=1;}
	|REL {$$=0;}
;
CONOVL:
	OVL {$$=1;}
	| CON {$$=0;}
;
GLOLOC:
	GLOBL {$$=1;}
	| LOCAL {$$=0;}
;
parachk_line:
	CHKPARAHEAD LABEL NUM {
		$$ = newEmptyMne();
		$$ -> type = mneIsPARACHK;
		$$-> m.paraChk.parachkInfo = strdup($2);
		$$->m.paraChk.paraChkNum = $3;
		$$->m.paraChk.src_lineno=opcode_lineno;
	}
	;
defineLine:
      DEFINE LABEL {
		if(strncmp($1,$2,MAXSTRSIZE0-1)) // only if different
		{

			strncpy( rep_src[rep_str_num],$1,MAXSTRSIZE0-1);
			rep_src_hash[rep_str_num]=symbolCaseSensitivity?hashi($1):hash($1);
			strncpy( rep_dst[rep_str_num],$2,MAXSTRSIZE1-1);
			rep_str_num++;
		}
	}
	| DEFINE {
		strncpy( rep_src[rep_str_num],$1,MAXSTRSIZE0-1);
		rep_src_hash[rep_str_num]=symbolCaseSensitivity?hashi($1):hash($1);
		strncpy( rep_dst[rep_str_num]," ",MAXSTRSIZE1-1);// GIVE SPACE
		rep_str_num++;
	}
	| IFTRUE expr {
		expression * exp = simplifyExp($2, getLocTree());
		if(exp->type!=ExpIsNumber)
		{
			char buf[1024];
			fprintf(stderr,"Warning, expression %s at line %d cannot solved.\n", exprDump($2, buf, 1023), yylineno-1);
			keyword_defined=0;
		}else
		{
			keyword_defined = exp->exp.number;
		}
	}
	| IFFALSE expr {
		expression * exp = simplifyExp($2, getLocTree());
		if(exp->type!=ExpIsNumber)
		{
			char buf[1024];
			fprintf(stderr,"Warning, expression %s at line %d cannot solved.\n", exprDump($2, buf, 1023), yylineno-1);
			keyword_defined=0;
		}else
		{
			keyword_defined = (exp->exp.number)?0:1;
		}
	}
	;



%%

/////////////////////////////////////////////////////////////////////////////
// programs section
extern FILE *yyin;
extern FILE *yyout;
extern int yyparse(void);

void reset_counts(void)
{
    error_count=0;
    code_addr=0;
    lastaddr=0;
    row_count=1;
    endlineno=0;
    equ_count=0;

}
void yyerror(char const *s)
{
    fprintf(stderr,"Error:%s:%s %s\n",now_lex_fname, s, getLocTree());
	error_count++;
}
char lst_filename[PATH_MAX+4]="";
char obj_filename[PATH_MAX+3]="";
char sym_filename[PATH_MAX+4]="";
char adb_filename[PATH_MAX+3]="";
char input_filename[PATH_MAX+2]="";
int binary_assigned=0;
int predef_num=0;
int input_assigned=0;
int cos_req=0;
char  predef[1024][64];

void extract_file_path(char *path, char* file);
int genLstFile=0;
int genSymFile=0;
int genADBFile=0;
int genRELFile=0;
int longMode=0;
int doDumpMNE = 0;
char  filePath[PATH_MAX+1];
char TempDir[PATH_MAX+1];
char lines[10240];
char line1[10240];
char  TempFile[PATH_MAX+3];
char TempFile2[PATH_MAX+3];
char default_inc_path[PATH_MAX+3];
int obj_assigned=0;
char *objfilename=NULL;
int lst_assigned=0;
char *lstfilename=NULL;
extern int phase2err;
char *env;

int main(int argc, char* argv[])
{
    int i,final;
    int result;
    //FILE *listfile;
   // FILE *outputfile;
   // FILE *fileb;
//    int eqnox;



    if(argc<2)
    {
        fprintf(stderr,
                "ussage: %s [options] asmfile\n",argv[0]);
        fprintf(stderr,"%s","\t-l create list\n\
\t-s create symbol file \n\
\t-o [file] create .rel (obj) file [file] is opt.\n\
\t-y generate debug information in .rel file\n\
\t-d dump MNE to stdout\n\
\t-z Disable case sensitivity for symbols\n\
\t-m compatible mode\n\
\t-h08b ..H08B instruction set [modify RJ only]\n\
\t-h08d ..H08D special instruction set [@FSR2-b =0x8b, not 0x0b]\n\
\t-dep depfile :generate dependency to depfile\n\
\t-k path :add path to include search path\n\
\t-rn:   -r1 means ram model medium, no check the range of F in \n\t<f,d,a>/<f,a> and 'a' bit must be set to 1 if f>0xff\n\
\t\t-r2 means linker will insert LBSR, and a bit will left untouched.\n\
\t-V show version\n\
"			);
        return 1;
    }
	parser_init();

    for(i=1; i<argc; i++)
    {
        if(i==(argc-1)) 
		{	
			final=1;
			if(argv[i][0]=='-' && argv[i][1]=='V')// capital!!
			{
				printf("SDASHYA  %s--%s\n",ASLNKVER, __DATE__);
				return 0;
			}
		}
        else
            final=0;
        if(argv[i][0]=='-' && !final)
        {
            switch(argv[i][1])
            {
            case 'l':
		genLstFile=1;
				if(argv[i][2]=='l')
				longMode=1;
		if(i!=(argc-1) && argv[i+1][0]!='-' && strstr(argv[i+1],".lst"))
		{
			lstfilename=argv[++i];
			lst_assigned = 1;
		}
                break;
            case 's':
		genSymFile=1;
                break;
		case 'c':
			fprintf(stderr, "-c skipped in this version.\n");
			break;
	    case 'o':
		genRELFile=1;
			if(i!=(argc-1) &&  argv[i+1][0]!='-' )
			{
				objfilename=argv[++i];
				//fprintf(stderr,"get output filename %s\n",objfilename);

				obj_assigned = 1;
			}
		break;
		case 'k':
			if(i!=(argc-1) && argv[i+1][0]!='-' && incSearchPathNum<128)
			{
				incSearchPath[incSearchPathNum++]=argv[++i];
			}
			break;
		case 'd':
			if(argv[i][2]=='e')
			{
				genDep=1;
				if(i!=(argc-1))
					depFname=argv[++i];
				else
				{
					fprintf(stderr, "-dep need specify dep file.\n");
					return -7;
				}
			}
			else
				doDumpMNE=1;
			break;
	    case 'y':
	      	genADBFile=1;
	        break;
		case 'z':
			symbolCaseSensitivity=1;
			break;
		case 'm':
			compatibleMode=1;
			break;
		case 'r':
			if(isdigit(argv[i][2]))
				ramModel=argv[i][2]-'0';
			break;

		case 'h':
			if(!strcasecmp(argv[i],"-h08b"))
			{
				is08b=1;
			}else if(!strcasecmp(argv[i],"-h08d"))
			{
				is08d=1;
			}
			else {
				fprintf(stderr,
                        "unknown option '%s'\n",argv[i]);
                return (2);
            
			}
			break;

            default:
                fprintf(stderr,
                        "unknown option '%s'\n",argv[i]);
                return (2);
            }
		
        } else
        {
#ifdef _WIN32
		char drive[10];
		char dir[PATH_MAX+1];
		char fname[PATH_MAX+1];
		char dir2[PATH_MAX+1];
		char ext[100];
		char *asmpath2;
#endif
            strncpy(input_filename,argv[i],PATH_MAX+1);
			asmPath=strdup(input_filename);
#ifdef _WIN32
			//PathRemoveFileSpec(asmPath);
			_splitpath(asmPath, drive, dir, fname, ext);
			strncpy(dir2,drive, PATH_MAX);
			strncat(dir2,dir, PATH_MAX);
			asmpath2 = strdup(dir2);
			free(asmPath);
			asmPath=asmpath2;

#else
			asmPath=dirname(asmPath);
#endif
			
			now_lex_fname = input_filename;
            input_assigned=1;
        }
    }
    if(!input_assigned)
    {
        fprintf(stderr,"%s","input file not assigned.\n");
        return 3;
    }
    
    if(genADBFile)
    {
        strncpy(adb_filename,input_filename,PATH_MAX+2);
        ChangeFileExt(adb_filename,".adb");
    }
    if(genRELFile)
    {
		if(obj_assigned)
		{
			strncpy(obj_filename, objfilename, PATH_MAX+2);
			//fprintf(stderr,"get obj filename %s\n", obj_filename);
			//if(!strstr(obj_filename,".rel")&& !strstr(obj_filename, ".REL"))
			//{
				//ChangeFileExt(obj_filename, ".rel");
			//}
			//fprintf(stderr,"obj file name is %s\n", obj_filename);
			if(!strstr(obj_filename,".")) // if there is not extension
				strncat(obj_filename,".rel",PATH_MAX+2); // we add it
			//fprintf(stderr,"obj file name is %s\n", obj_filename);
		}
	   else
		{
        strncpy(obj_filename,input_filename,PATH_MAX+2);
        ChangeFileExt(obj_filename,".rel");
		}
    }
    if(genSymFile)
    {
        strncpy(sym_filename,obj_filename,PATH_MAX+3);  // sym is from obj
        ChangeFileExt(sym_filename,".sym");
    }
	if(lst_assigned)
		strncpy(lst_filename, lstfilename, PATH_MAX+3);
	else
	{
        	strncpy(lst_filename,obj_filename,PATH_MAX+3);
        	ChangeFileExt(lst_filename,".lst");
	}
    

	
	// add include path
		env = getenv("SDCC_HOME");
    default_inc_path[0]='\0';
	if(env!=NULL)
	{
		strncpy(default_inc_path, env,PATH_MAX+2);
#ifdef _WIN32
		strncat(default_inc_path,"\\include\\HY08A",PATH_MAX+2);
#else
		strncat(default_inc_path,"/include/HY08A",PATH_MAX+2);
#endif
		incSearchPath[incSearchPathNum++]=
		default_inc_path;
	}
	else
	{
		fprintf(stderr, "Warning, SDCC_HOME environment variable not found.\n");
	}
    //memset(program_code,0xff,sizeof(program_code));
    //extract_file_path(filePath,input_filename);
    strncpy(TempFile , input_filename, PATH_MAX+2); // TempFile is now the input

    yyin=fopen(TempFile,"r" );
    if(yyin==NULL)
    {
        fprintf(stderr,"input file %s open error.\n", TempFile);
        return 3;
    }
    yyout=stdout;
    before_lex();
    //yyerr=yyout;
    reset_counts();
    for(i=0; i<predef_num; i++)
    {
        strncpy( rep_src[i],predef[i],MAXSTRSIZE0-1);
        strcpy( rep_dst[i],"");
		rep_src_hash[i]=symbolCaseSensitivity?hashi(predef[i]):hash(predef[i]);
        rep_str_num++;
    }
	if(genDep)
	{
		depfp = fopen(depFname,"w")		;
		if(depfp==NULL)
		{
			fprintf(stderr,"dep file %s open write error.\n", depFname);
			return -9;
		}
		fprintf(depfp, "%s: %s ", obj_filename, input_filename);
	}

    do {
        result=yyparse();
    } while(!feof(yyin) && result==0);
	fclose(yyin);
    if(result)
    {
        fprintf(stderr, "..Asm parse failed around line %d\n",yylineno);
        return result;
    }
	if(genDep)
	{
		fprintf(depfp, "\n");
		fclose(depfp);
	}
//	if(error_count)
//		return error_count;
        //printf("ASM Parsed OK \n");
	if(doDumpMNE)
		dumpMne();
	//if((result=phase2())!=0)
	phase2();
		//return result;
		if(errMsgSL)
	{
		showMsgSL(&errMsgSL);
	}
	if(phase2err)
		error_count++;
	//if(genLstFile)
	//{
		genListFileO(TempFile, lst_filename,genLstFile,longMode);
	//}
	phase2err=0;
	if(genSymFile)
		genSymFileO(sym_filename);
		if(phase2err)
		error_count++;
	if(genRELFile) // if error rel file not generated
	{
		if(error_count)
		{
			fprintf(stderr,"Attention!! %s not generated because of error.\n", obj_filename);
			printf ("\a");
		}
		else
			//genOBJFileO(obj_filename,getOnlyBaseName(TempFile,TempFile2,PATH_MAX)); // give source name
		{
			phase2err=0;
			genOBJFileO(obj_filename,TempFile); // give source name
			if(phase2err)
				error_count++;
			}
	}
	
	if(error_count)
		fprintf(stderr,"Assembler operation (%s) failed.\n ", argv[0]);
    fflush(stderr);
    fflush(stdout);

    return error_count;
}


extern int yylineno;
macrot *newMacro(char *name, int check, int declareLineNo, char *declareSrcFName)
{
	macrot *m = (macrot*) calloc(1,sizeof(macrot));
	int i;
	uint64 hashid=symbolCaseSensitivity?hashi(name):hash(name);
	if(check)
	{
	for(i=0;i<defined_macro_num; i++)
		if(hashid==defined_macros[i]->nameHash)
		{
			fprintf(stderr,"Assembler Error!! macro %s doubled at line around %d, give up !!\n", name, yylineno);
			exit(-1005);
		}
	}
	
	memset(m, 0, sizeof(macrot));
	m->macroName=strdup(name);
	m->declareSrcFName = strdup(declareSrcFName);
	m->declareLineNo = declareLineNo;
	m->nameHash = symbolCaseSensitivity?hashi(name):hash(name);
	return m;
}
