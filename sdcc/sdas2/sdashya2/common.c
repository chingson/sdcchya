#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>
#include <stdint.h>

#include "mytype.h"
#include "city.h"
//
//#ifdef _WIN32
//#include <windows.h>
//#endif
//---------------------------------------------------------------------------

srcLine *errMsgSL = NULL;
srcLine *errMsgSLT = NULL;
#ifndef _WIN32
static char *dummy;
#endif

int appendErrMsg(char *msg)
{
	srcLine *n;
	uint64 msghash = hash(msg);
	
	for (n = errMsgSL; n; n = n->next)
		if (n->line && msghash==n->lineHash)
			return 0;
	n= newSrcLine(msg, -1);
	
	appendSrcLine2(&errMsgSL, &errMsgSLT, n);
	return 0;
}
int showMsgSL(srcLine **sl)
{
	srcLine *h = *sl;
	while (h)
	{
		fprintf(stderr, "%s", h->line);
		h = h->next;
	}
	delSrcList(*sl);
	*sl = NULL;
	return 0;
}
// in case there is no ext
char *ChangeFileExt(char *a, char*ext) // ext include the '.'
{
	int i;
	for (i = strlen(a); i >= 0; i--)
	{
		if (a[i] == '.')
			break;
		if (a[i] == '\\' || a[i] == '/')
		{
			strcat(a, ext);
			return a;
		}
	}
	if (i < 0)
	{
		strcat(a, ext);
		return a;
	}

	strcpy(a + i, ext);
	return a;
}



dataItem * newEmptyDataItem(void)
{
	dataItem *itemp = (dataItem*)calloc(1,sizeof(dataItem));
	//memset(itemp, 0, sizeof(dataItem));
	return itemp;
}

dataItem * appendExp(dataItem *head, expression* taile)
{
	dataItem* itemp = head;
	dataItem* itemnp = newEmptyDataItem();
	while (itemp->next != NULL)
		itemp = itemp->next;
	itemnp->exp = taile;
	itemp->next = itemnp;
	return head;
}


expression * newEmptyExp(int lineno)
{
	expression * pexp = (expression*)calloc(1,sizeof(expression));
	//memset(pexp, 0, sizeof(expression));
	pexp->lineno = lineno;
	return pexp;
}
area* appendArea(area *h, area *n)
{
	area *p = h;
	if (h == NULL)
		return n;
	while (p->next)
		p = p->next;
	p->next = n;
	return h; // means source not change

}





uint64 hash(char *s)
{
	return CityHash64(s, strlen(s));
}
uint64 hashi(char *s)
{
	int len = strlen(s);
	int i;
	for (i = 0; i < len; i++)
		s[i] = toupper(s[i]);
	return CityHash64(s, len);
}


symbol *newSymbol(char *name)
{
	symbol *sp = calloc(1,sizeof(symbol));
	//memset(sp, 0, sizeof(symbol));
	strncpy(sp->name, name, MAXSTRSIZE0-1);
	sp->hashid = hash(name);
	return sp;
}

wData *newWDATA1(area *ap)
{
	wData *wdp = calloc(1,sizeof(wData));
	//memset(wdp, 0, sizeof(wData));
	wdp->ap = ap;
	wdp->macro_id = -1;
	wdp->macroLevel = -1;
	wdp->sizeOrORG = 1;
	return wdp;
}

wData *newWDATA(area *ap)
{
	wData *wdp = calloc(1,sizeof(wData));
	//memset(wdp, 0, sizeof(wData));
	wdp->ap = ap;
	wdp->macro_id=-1;
	wdp->macroLevel = -1;
	return wdp;
}
// append WDATA in list dual direction for speed
//wData *dbgWdp=NULL;
wData *appendTailWDATA(area *ap, wData *n, int insthigh)
{
	if (insthigh)
	{
		ap->wDataTailInst = n;
		
	}
	if (ap->wDatas == NULL)
	{
		ap->wDatas = n;
		ap->wDataTail = n;
	}
	else
	{
		ap->wDataTail->next = n;
		n->prev = ap->wDataTail; // 2022 OCT, change to dual direction linklist
		ap->wDataTail = n;
	}
	if (!n->wdIsLabel && ! n->wdIsOrg && ap->offsetAddr >= 0)
	{
		
		ap->offsetAddr += n->sizeOrORG;
	}
	//if (n->wdIsLabel && !strcmp(n->sym->name, "loop_copy1"))
	//{
	//	n->mark = 0x55;
	//	//if(dbgWdp==NULL)
	//		//dbgWdp = n;
	//	//fprintf(stderr, "copy1\n");
	//}
	return n;
}
int str2upper(char *s)
{
	int i = 0;
	while (*s)
	{
		if (islower(*s))
		{
			*s = toupper(*s);
			i++;
		}
#ifdef _WIN32		
		if ((*s) == '\\')
		{
			*s = '/'; // change
			i++;
		}
#endif
		s++;
	}
	return i;
}
int str2lower(char *s)
{
	int i = 0;
	while (*s)
	{
		if (isupper(*s))
		{
			*s = tolower(*s);
			i++;
		}
#ifdef _WIN32
		if ((*s) == '\\')
		{
			*s = '/'; // change
			i++;
		}
#endif
		s++;
	}
	return i;
}

srcLine *newSrcLine(char *line, int lineno)
{
	srcLine *sp = calloc(1,sizeof(srcLine));
	//memset(sp, 0, sizeof(srcLine));
	sp->line = strdup(line);
	sp->lineHash = hash(line);
	sp->lineno = lineno;
	if (!strncmp(line, "$MOD:", 5))
	{
		sp->modLine = 1;
	}
	else if (!strncmp(line, "$EXPORT:", 8))
	{
		sp->exportLine = 1;
		sp->expHash = hash(line+8);
	}
	return sp;
}

char *removeNewLine(char* str)
{
	int i;
	int j = strlen(str);
	// if(strlen(str)>200)
	// 	fprintf(stderr,"long line(removen_before): %s", str);

	for (i = 0; i < j; i++)
	{
		if (str[i] == '\r' || str[i] == '\n')
			str[i] = '\0';
	}
	// if(strlen(str)>200)
	// 	fprintf(stderr,"long line(removen): %s\n", str);
	return str;
}

//char *getOnlyBaseName(char *longName, char *buffer, int lenmax)
//{
//	int i;
//	int j = strlen(longName);
//	for (i = j - 1; i >= 0; i--)
//	{
//		if(longName[i]=='\\' || longName[i] == '/')
//			break;
//		else if (longName[i] == '.')
//		{
//			longName[i] = '\0';
//		}
//	}
//	strncpy(buffer, longName + i + 1, lenmax);
//	return buffer;
//	
//}




gloLocItem *newGloLocItem(char *name, int isGlobl)
{
	gloLocItem *ip = calloc(1,sizeof(gloLocItem));
	//memset(ip, 0, sizeof(gloLocItem));
	ip->hashid = hash(name);
	ip->setGlobl = isGlobl;
	ip->name = strdup(name);
	return ip;
}

char *exprDump(expression *exp, char *buffer, int maxlen)
{
	char left[1024];
	char right[1024];
	if (!exp)
		return NULL;
	switch (exp->type)
	{
	case ExpIsPCNow:
		snprintf(buffer, maxlen, "$$");
		break;
	case ExpIsNumber:
		snprintf(buffer, maxlen, "0x%X", exp->exp.number);
		break;
	case ExpIsASCII:
		snprintf(buffer, maxlen, "\"%s\"", exp->exp.symbol);
		break;
	case ExpIsSymbol:
		snprintf(buffer, maxlen, "%s", exp->exp.symbol);
		break;
	case ExpIsStackOffset:
		snprintf(buffer, maxlen, "(@FSR2-0x%X)", exp->exp.number);
		break;
	case ExpIsTree:
		exprDump(exp->exp.tree.left, left, 1024);
		exprDump(exp->exp.tree.right, right, 1024);
		if (exp->exp.tree.OverflowChk)
		{
			int q0 = exp->exp.tree.OverflowChkUnsign ? '{' : '[';
			int q1 = exp->exp.tree.OverflowChkUnsign ? '}' : ']';
			switch (exp->exp.tree.op)
			{
			case '+': snprintf(buffer, maxlen, "%c %s + %s %c", q0, left, right, q1); break;
			case '-': snprintf(buffer, maxlen, "%c %s - %s %c", q0, left, right, q1); break;
			case 'A': snprintf(buffer, maxlen, "%c %s >> %s %c", q0, left, right, q1); break;
			case 'B': snprintf(buffer, maxlen, "%c %s << %s %c", q0, left, right, q1); break;

			case '&': snprintf(buffer, maxlen, "%c %s & %s %c", q0, left, right, q1); break;
			case '|': snprintf(buffer, maxlen, "%c %s | %s %c", q0, left, right, q1); break;
			case '<': snprintf(buffer, maxlen, "%c %s < %s %c", q0, left, right, q1); break;
			case '>': snprintf(buffer, maxlen, "%c %s > %s %c", q0, left, right, q1); break;
			case 'G': snprintf(buffer, maxlen, "%c %s >= %s %c", q0, left, right, q1); break;
			case 'L': snprintf(buffer, maxlen, "%c %s <= %s %c", q0, left, right, q1); break;
			case '=': snprintf(buffer, maxlen, "%c %s == %s %c", q0, left, right, q1); break;
			case 'N': snprintf(buffer, maxlen, "%c %s != %s %c", q0, left, right, q1); break;
			case 'C': snprintf(buffer, maxlen, "%c %s <> %s %c", q0, left, right, q1); break;
			default:
				snprintf(buffer, maxlen, "unknown op %c", exp->exp.tree.op);
			}

		}
		else
			switch (exp->exp.tree.op)
			{
			case '+': snprintf(buffer, maxlen, "(%s + %s)", left, right); break;
			case '-': snprintf(buffer, maxlen, "(%s - %s)", left, right); break;
			case 'A': snprintf(buffer, maxlen, "(%s >> %s)", left, right); break;
			case 'B': snprintf(buffer, maxlen, "(%s << %s)", left, right); break;
			case '&': snprintf(buffer, maxlen, "(%s & %s)", left, right); break;
			case '|': snprintf(buffer, maxlen, "(%s | %s)", left, right); break;
			case '<': snprintf(buffer, maxlen, "(%s < %s)", left, right); break;
			case '>': snprintf(buffer, maxlen, "(%s > %s)", left, right); break;
			case 'G': snprintf(buffer, maxlen, "(%s >= %s)", left, right); break;
			case 'L': snprintf(buffer, maxlen, "(%s <= %s)", left, right); break;
			case '=': snprintf(buffer, maxlen, "(%s == %s)", left, right); break;
			case 'N': snprintf(buffer, maxlen, "(%s != %s)", left, right); break;
			case 'C': snprintf(buffer, maxlen, "(%s <> %s)", left, right); break;
			default:
				snprintf(buffer, maxlen, "unknown op %c", exp->exp.tree.op);
			}

	}
	return buffer;
}

void delSrcList(srcLine *srcList)
{
	// by dual list
	
	srcLine *sp;
	srcLine *spp;
	srcList->prev = NULL;
	for (sp = srcList; sp->next; sp = sp->next)
			;
	spp = sp->prev;
	

	free(sp);
	while (spp)
	{
		sp = spp->prev;
		free(spp);
		spp = sp;
	}

}
srcLine *appendSrcLine2(srcLine **h, srcLine **t, srcLine *n)
{

	if ((*h) == NULL)
	{
		*h = *t = n;
		return n;
	}
	(*t)->next = n;
	n->prev = (*t);
	(*t) = n;
	return *h;
}

srcLine *appendSrcLine(srcLine *h, srcLine *n)
{
	srcLine *srcp;
	if (!h)
		return n;
	for (srcp = h; srcp->next; srcp = srcp->next)
		;
	n->prev = srcp;
	srcp->next = n;
	return h;
}


FILE *openFileinDirs(char *fname, srcLine *searchDir, char *mode, char *foundFullPath, int maxpath)
{
	char namebuf[PATH_MAX];
	FILE *fp=NULL;
	if(foundFullPath!=NULL)
		foundFullPath[0] = '\0';
	while (searchDir)
	{
		strncpy(namebuf, searchDir->line, PATH_MAX-1);
		appendFinalSlash(namebuf);
		
		strcat(namebuf, fname);
		fp = fopen(namebuf, mode);
		if (fp != NULL)
		{
			if(foundFullPath!=NULL)
				strncpy(foundFullPath, namebuf, maxpath);
			return fp;
		}
		searchDir = searchDir->next;
	}
	return fp;
}
/*
// as the name, we construct the data to srclines
int readFile2SrcLine(char *fname, srcLine **h, srcLine *searchDir, char *fullpath, int maxlen)
{
	int lineno = 1;
	char buffer[1024];
	int count = 0;
	FILE *fsrc;
	srcLine *srcp;
	if(fullpath!=NULL)
		*fullpath = '\0';
	if (!searchDir)
		fsrc = fopen(fname, "r");
	else
		fsrc = openFileinDirs(fname, searchDir, "r", fullpath, maxlen);
	if (fsrc == NULL)
		return -1;
	*h = NULL;
	while (fgets(buffer, 1024, fsrc) != NULL)
	{
		srcp = newSrcLine(removeNewLine(buffer), lineno++);
		*h = appendSrcLine(*h, srcp);
		count++;
	}
	fclose(fsrc);
	return count;
}
*/

// find the src line with the locTree, as its name
srcLine *findSrcLineLocTree(srcLine *h, char *locTree)
{
	char buf[1024];
	uint64 hashid;
	strncpy(buf, locTree, 1023);
	str2lower(buf);
	hashid = hash(buf);
	for (; h; h = h->next)
	{
		if (h->locHash == hashid)
			return h;
	}
	return NULL;
}
char *str_replace(char *orig, char *rep, char *with) {
	char *result; // the return string
	char *ins;    // the next insert point
	char *tmp;    // varies
	int len_rep;  // length of rep (the string to remove)
	int len_with; // length of with (the string to replace rep with)
	int len_front; // distance between rep and end of last rep
	int count;    // number of replacements

				  // sanity checks and initialization
	if (!orig || !rep)
		return NULL;
	len_rep = strlen(rep);
	if (len_rep == 0)
		return NULL; // empty rep causes infinite loop during count
	if (!with)
		with = "";
	len_with = strlen(with);

	// count the number of replacements needed
	ins = orig;
	for (count = 0; (tmp = strstr(ins, rep))!=NULL; ++count) 
	{
		ins = tmp + len_rep;
	}

	tmp = result = malloc(strlen(orig) + (len_with - len_rep) * count + 1);

	if (!result)
		return NULL;

	// first time through the loop, all the variable are set correctly
	// from here on,
	//    tmp points to the end of the result string
	//    ins points to the next occurrence of rep in orig
	//    orig points to the remainder of orig after "end of rep"
	while (count--) {
		ins = strstr(orig, rep);
		len_front = ins - orig;
		tmp = strncpy(tmp, orig, len_front) + len_front;
		tmp = strcpy(tmp, with) + len_with;
		orig += len_front + len_rep; // move to next "end of rep"
	}
	strcpy(tmp, orig);
	return result;
}
int insertMacro2SrcLines(srcLine *inspoint, macrot *m) // insert after inspoint
{
	srcLine *oldn = inspoint->next;
	//srcLine *olds = inspoint; // for dual direction
	srcLine *entry, *cpy;
	char buf[1024]; 
	int offset;
	int i;
	char *rootLoc = inspoint->locTree;
	if (m->isRPT) // this is repeat, no need replace to macro
	{
		
		offset = 0;
		for (i = 0; i < m->paras->exp->exp.number; i++)
		{
			for (entry = m->lines; entry; entry = entry->next)
			{
				cpy = newSrcLine(entry->line, inspoint->lineno); // lineno=-1;
				snprintf(buf,1023, "%s;%s:%d", rootLoc, m->macroName, offset);
				str2lower(buf);
				cpy->locTree = strdup(buf);
				cpy->locHash = hash(buf);
				offset++;
				inspoint->next = cpy;
				cpy->prev = inspoint;
				cpy->next = oldn;
				oldn->prev = cpy;
				inspoint = cpy;

			}
		}
	}
	else
	{
		offset = 1;
		// create replace list , from inspoint, 
		// we replace to 1 level only, but will be very helpful
		char * para_assigned[100];
		int paraAssignedNum = 0;
		char buf[1024];
		char buf1[1024];
		char *p;
		int j = strlen(m->macroName);
		
		strncpy(buf1, inspoint->line, 1023);
		
		for (p = buf1; (*p) && strncmpi(p, m->macroName,j); p++)
			;
		// following is trivial
		if (p)
		{
			while ((*p) && !isspace(*p))
				p++;

			while (*p)
			{
				while ((*p) && ((*p)==','||isspace(*p)) )
				{
					*p = '\0';
					p++;
				}
				if (*p)
					para_assigned[paraAssignedNum++] = p;
				while ((*p) && !((*p) == ',' || isspace(*p)))
					p++;
			}
		}
		for (entry = m->lines; entry; entry = entry->next)
		{
			dataItem *dp=NULL;
			char *temp;
			cpy = newSrcLine(entry->line, inspoint->lineno); // lineno=-1;

			// replace the line with parameter assigned
			if (paraAssignedNum)
				dp = m->paras;
			str2upper(cpy->line); // upper
			for (i = 0; dp!=NULL && i < paraAssignedNum; i++)
			{
				str2upper(dp->exp->exp.symbol); // upper case!!
				temp = str_replace(cpy->line, dp->exp->exp.symbol, para_assigned[i]);
				free(cpy->line);
				cpy->line = temp;
				dp = dp->next;
				if (!dp || dp->exp->exp.symbol[0]=='?') // local
					break; // ';' ...
			}


			snprintf(buf, 1023, "%s;%s:%d", rootLoc, m->macroName, offset);
			
			str2lower(buf);
			cpy->locTree = strdup(buf);
			cpy->locHash = hash(buf);
			offset++;
			inspoint->next = cpy;
			cpy->prev = inspoint;
			cpy->next = oldn;
			oldn->prev = cpy;
			inspoint = cpy;

		}
	}
	return 1;
}

char *getRelPath(char *absPath)
{
	char buffer[PATH_MAX + 1024];
	char currentPath[PATH_MAX + 1];
	int i,j;
	int len = strlen(absPath);
#ifdef _WIN32
	char absBuf[PATH_MAX + 1];
	// for windows, the abs is like c:...
	if (strlen(absPath) < 2 || absPath[1] != ':')
		return absPath; // it should be the relative one
	_fullpath(currentPath, ".", PATH_MAX);
	strncpy(absBuf, absPath, PATH_MAX);
	str2lower(currentPath);
	str2lower(absBuf);
	for (i = 0; i < len; i++)
	{
		if (currentPath[i] != absBuf[i])
			break;
	} // before i is common part
	buffer[0] = '\0';
	if (currentPath[i] == '\0') // compare done
	{
		strncat(buffer, absBuf + i+1, PATH_MAX);
	}
	else
	{
		strncat(buffer, "..\\", PATH_MAX); // at least need 1 more ..
		for (j = i; j < (int)strlen(currentPath); j++)
			if (currentPath[j] == '\\' || currentPath[j] == '/')
				strncat(buffer, "..\\", PATH_MAX);

		strncat(buffer, absBuf + i, PATH_MAX);
	}
	return strdup(buffer);
#else
	if (absPath == NULL || absPath[0] != '/')
		return absPath;
	dummy=realpath(".", currentPath);
	for (i = 0; i < len; i++)
	{
		if (currentPath[i] != absPath[i])
			break;
	} // before i is common part
	buffer[0] = '\0';
	if (currentPath[i] == '\0') // compare done
	{
		strncat(buffer, absPath + i+1, PATH_MAX);
	}
	else
	{
		strncat(buffer, "../", PATH_MAX); // at least need 1 more ..
		for (j = i; j < strlen(currentPath); j++)
			if (currentPath[j] == '\\' || currentPath[j] == '/')
				strncat(buffer, "../", PATH_MAX);

		strncat(buffer, absPath + i, PATH_MAX);
	}
	return strdup(buffer);
	
#endif
}

int readFile2SrcLine2(char *fname, srcLine **h, srcLine **t,srcLine *searchDir, char *fullpath, int maxlen, asmIncInfo *aifph, int *totalLineno, char *rootLoc)
{
	int lineno = 1;
	char buffer[PATH_MAX+10240];
	int count = 0;
	char *shortName;

	FILE *fsrc;
	srcLine *srcp;
	asmIncInfo *aifp = aifph;
	uint64 fnameHash;
	if (fullpath != NULL)
		*fullpath = '\0';
	if (!searchDir)
		fsrc = fopen(fname, "r");
	else
		fsrc = openFileinDirs(fname, searchDir, "r", fullpath, maxlen);
	if (fsrc == NULL)
		return -1;
#ifdef _WIN32
	_fullpath(buffer, fname, PATH_MAX);
	fnameHash = hashi(buffer);
#else
	dummy=realpath(fname, buffer);
	//fprintf(stderr,"rp is %s\n", rp);
	//fprintf(stderr,"fname is %s buffer is %s\n", fname, buffer);
	fnameHash = hash(buffer);
		//fprintf(stderr,"Warning, path error %s\n", fname);
#endif
	*h = NULL;
	*t = NULL;
	shortName = getRelPath(fname);
	while (fgets(buffer, PATH_MAX+10239, fsrc) != NULL)
	{
		char buf1[2560];
		srcp = newSrcLine(removeNewLine(buffer), lineno++);
		srcp->fnameHash = fnameHash;
		snprintf(buf1,2559, "%s,%s:%d", rootLoc, shortName, count + 1);
		str2lower(buf1);
		srcp->locTree = strdup(buf1);
		srcp->locHash = hash(buf1);
		appendSrcLine2(h,t, srcp);
		count++;
	}
	if (totalLineno)
		*totalLineno = count;
	fclose(fsrc);

	while (aifp)
	{
		if (aifp->fname1hash == fnameHash)
		{
			srcLine *hi = NULL;
			srcLine *ti = NULL;
			srcLine *p=NULL,*q=NULL;
			// include uses full name, not in lib
			
			for (p = *h; p; p = p->next)
				if (p->fnameHash == aifp->fname1hash && (p->lineno == aifp->lineno  ||
					p->lineno == aifp->lineno+1))
					break;
			if (!p)
				p = (*t)->prev; // append at last
			q = p->next;

			if (0 > readFile2SrcLine2(aifp->fname2, &hi, &ti, searchDir, NULL, maxlen, aifph, &(aifp->fname2lineno),p->locTree))
			{
				fprintf(stderr, "Error, file %s list generation faild.\n", aifp->fname2);
				return -1;
			}

			p->next = hi;
			hi->prev = p;
			ti->next = q;
			q->prev = ti; // dual link!!

		}
		aifp = aifp->next;
	}
	return count;
}



int writeSrcLine2File(char *fname, srcLine *h)
{
//	char buffer[4096];
	FILE *fpo = fopen(fname, "w");
	int writeCount = 0;
	if (fpo == NULL)
	{
		fprintf(stderr, "Error, %s file open error.\n", fname);
		//snprintf(buffer, 4096, "%s file open error.\n", fname);
		//appendErrMsg(buffer);
		return -2;
	}
	while (h)
	{
		 //int k=strlen(h->line);
		 //if(k>100 && strstr(h->line,":_expf"))
		 	//fprintf(stderr,"long line\n");
		fprintf(fpo, "%s\n",h->line);
		// if(strlen(h->line)>200)
		// 	fprintf(stderr,"long line:%s\n", h->line);
		h = h->next;
		writeCount++;
	}
	fclose(fpo);
	return writeCount;
}

int writeSrcLine2Filep(FILE *fpo, srcLine *h)
{
	//	char buffer[4096];
	//FILE *fpo = fopen(fname, "w");
	int writeCount = 0;
	while (h)
	{
		//int k=strlen(h->line);
		//if(k>100 && strstr(h->line,":_expf"))
		   //fprintf(stderr,"long line\n");
		fprintf(fpo, "%s\n", h->line);
		// if(strlen(h->line)>200)
		// 	fprintf(stderr,"long line:%s\n", h->line);
		h = h->next;
		writeCount++;
	}
	//fclose(fpo);
	fflush(fpo);
	return writeCount;
}

char *getSymNameOfLine(char *buffer, srcLine *s, int maxlen)
{
	char *p = s->line;
	//char *w = buffer;
	maxlen--;

	while (*p && *p != ':') p++;
	p++;
	strncpy(buffer, p, maxlen);
	return buffer;

}


// as the following name
// extract from it
char *getModNameOfLine(char *buffer, srcLine *s, int maxlen)
{
	char *p = s->line;
	char *w = buffer;
	maxlen--;

	while (*p && *p != ':') p++;
	p++;
	while (*p && *p != ':' && maxlen>0)
	{
		*w++ = *p++;
		maxlen--;
	}
	*w = '\0';
	return buffer;
}


int copyPart2NewSrcLine(srcLine *start, srcLine **h,srcLine **tail)
{
	// it can be a module/rel , copy to endmod
	srcLine *srcp;
	int lineno = 1;
	*h = NULL;
	while (strncmp(start->line, "$ENDMOD", 7))
	{
		srcp = newSrcLine(start->line, lineno++);
		appendSrcLine2(h,tail, srcp);
		start = start->next;
	}
	// final line must be copied too
	srcp = newSrcLine(start->line, lineno++);
	appendSrcLine2(h,tail, srcp);
	return lineno - 1;
}



srcLine *concat2list(srcLine **head, srcLine *tail)
{
	srcLine *sp = *head;

	if (*head == NULL)
	{
		*head = tail;
		return tail;
	}
	while (sp->next)
		sp = sp->next;
	sp->next = tail;
	return *head;
}



// just remove the module from the list
// this kind of job is easy in memory
// it is possible first line is the line we want!!
srcLine *removeModInList(srcLine **head, srcLine *modLine)
{
	srcLine *srcp;
	srcLine *srcp2;
	// special case is that first line is module line
	if (*head == modLine)
	{
		for (srcp2 = modLine; srcp2; srcp2 = srcp2->next)
		{
			if (!strncmp(srcp2->line, "$ENDMOD", 7))
				break;
		}
		*head = srcp2->next;
		if (*head != NULL)
			(*head)->prev = NULL;
		srcp2->next = NULL;
		modLine->prev = NULL;
		delSrcList(modLine);
		return *head;

	}
	for (srcp = *head; srcp && srcp->next != modLine; srcp = srcp->next)
		;
	if (srcp == NULL) return NULL;
	for (srcp2 = modLine; srcp2; srcp2 = srcp2->next)
	{
		if (!strncmp(srcp2->line, "$ENDMOD", 7))
			break;
	}
	// now srcp2 ports to ENDMOD
	srcp->next = srcp2->next;
	if(srcp2->next!=NULL)
		srcp2->next->prev = srcp;
	srcp2->next = NULL;
	modLine->prev = NULL;

	delSrcList(modLine);
	return *head;
}



// check if same export symbol, found return 1, no found
// return 0
srcLine * checkExportSymDouble(srcLine *lib, srcLine *rel)
{
	// dual loop, no exception
	srcLine *sa, *sb;
	for (sa = rel; sa; sa = sa->next)
	{
		//if (strncmp(sa->line, "$EXPORT", 7))
		if(!sa->exportLine)
			continue;
		for (sb = lib; sb; sb = sb->next)
		{
			//if (strncmp(sb->line, "$EXPORT", 7))
			if(!sb->exportLine)
				continue;
			if (!strncmp(sa->line, sb->line, MAXSTRSIZE1))
			{
				return sa;
			}
		}
	}
	return NULL;
}

char *appendFinalSlash(char *path)
{
#ifdef _MSC_VER
	if (path[strlen(path) - 1] != '\\')
		strcat(path, "\\");
#else
	if (path[strlen(path) - 1] != '/')
		strcat(path, "/");
#endif
	return path;
}

int printWDATA(char *buffer, int bufmax, wData *wdp, int longmode, int value)
{

	if (longmode)// long more means entire expression
	{
		if (wdp->wdIsErrMark)
		{
			strcpy(buffer, wdp->macroSrc);
			return strlen(buffer);
		}
		if (wdp->wdIsOrg)
		{
			strcpy(buffer, "ORG..");
			return strlen(buffer);
		}
		if (wdp->wdIsLabel)
		{
			strcpy(buffer, "Label..");
			return strlen(buffer);
		}
		if (wdp->wdIsFunction)
		{
			snprintf(buffer, bufmax, "Func:%s", wdp->sym->name);
			return strlen(buffer);
		}
		if (wdp->wdIsFunctionEnd)
		{
			strcpy(buffer, "Endfunc");
			return strlen(buffer);
		}
		if (wdp->wdIsBlank)
		{
			int i;
			for (i = 0; i < wdp->sizeOrORG && i * 2 < bufmax - 2; i++)
			{
				buffer[2 * i] = 'X';
				buffer[2 * i + 1] = 'X';
			}
			buffer[2 * i] = '\0';
			return strlen(buffer);
		}
		exprDump(wdp->exp, buffer, bufmax);
		return strlen(buffer);
	}
	else // short mode
	{
		//if (wdp->wdIsOrg)
		//{
		//	snprintf(buffer, bufmax, "%04X", wdp->sizeOrORG);
		//	return 4;
		//}
		if (wdp->wdIsErrMark)
		{
			strncpy(buffer, "**",bufmax-1);
			strncat(buffer, wdp->macroSrc,bufmax-1);
			return strlen(buffer);
		}
		if (wdp->wdIsLabel || wdp->wdIsOrg)
		{
			buffer[0] = '\0';
			return 0;
		}
		if (wdp->wdIsBlank)
		{
			int i;
			for (i = 0; i < wdp->sizeOrORG && i <4; i++)
			{
				buffer[2 * i] = 'X';
				buffer[2 * i + 1] = 'X';
			}
			buffer[2 * i] = '\0';
			return strlen(buffer);
		}
		if (wdp->wdIsFunction || wdp->wdIsFunctionEnd || wdp->wdIsMacroMark)
		{
			buffer[0] = '\0';
			return 0;
		}
		if (value < 0)
			snprintf(buffer, bufmax, "%02X", getExpMinValue(wdp->exp));
		else
			snprintf(buffer, bufmax, "%02X", value);
		return strlen(buffer);
	}
	return 0;
}

int getExpMinValue(expression *exp)
{
	switch (exp->type)
	{
	case ExpIsASCII:
	case ExpIsPCNow:
	case ExpIsSymbol:
		return 0;
	case ExpIsNumber:
		return exp->exp.number;
	case ExpIsTree:
		switch (exp->exp.tree.op)
		{
		case '+': return getExpMinValue(exp->exp.tree.left) + getExpMinValue(exp->exp.tree.right);
		case '|': return getExpMinValue(exp->exp.tree.left) | getExpMinValue(exp->exp.tree.right);
		default:
			return 0;

		}
	default:
		return 0;

	}
	return 0;
}




expression *newConstExp(int number, int lineno)
{
	expression *exp = newEmptyExp(lineno);
	exp->type = ExpIsNumber;
	exp->exp.number = number;
	return exp;
}
expression *newPCNOWExp(int lineno)
{
	expression *exp = newEmptyExp(lineno);
	exp->type = ExpIsPCNow;
	exp->exp.number = -1;
	return exp;
}

expression *newSymbolExp(char *symName, int lineno)
{
	expression *exp = newEmptyExp(lineno);
	exp->type = ExpIsSymbol; // now use name directly
	strncpy(exp->exp.symbol, symName, sizeof(exp->exp.symbol)-1);
	exp->symHash = hash(exp->exp.symbol);
	return exp;
}

expression *newTreeExp(expression*left, expression *right, char op, int lineno)
{
	expression *exp = newEmptyExp(lineno);
	exp->type = ExpIsTree;
	exp->exp.tree.left = left;
	exp->exp.tree.right = right;
	exp->exp.tree.op = op;
	return exp;
}
int split2lines(char *str, char **splitstr) 
{      
  char *p,*p1;      
  char buf[10240];
  int i=0;      

  p = str;

  while (p != NULL)
  {
	  p1 = strstr(p, "\n");
	  if (p1 == NULL)
	  {
		  strncpy(buf, p, sizeof(buf)-1);
		  p = NULL;
	  }
	  else
	  {
		  int j = 0;
		  while (p != p1 && j < 1022)
			  buf[j++] = *p++;
		  buf[j++] = '\0';
	  }
	  //printf("%s", p);
	  splitstr[i++] = strdup(buf);
	  if (p == NULL)
		  break;
	  p++;
  }
  return i;
}
 asmIncInfo *newAsmIncInfo(char *fname1, int lineno, char *fname2)
{
	 char buffer[PATH_MAX + 1];
	asmIncInfo *aifp = calloc(1, sizeof(asmIncInfo));
	aifp->lineno = lineno;
#ifdef _WIN32
	_fullpath(buffer, fname1, PATH_MAX);
	aifp->fname1 = strdup(buffer);
	aifp->fname1hash = hashi(buffer);
#else
	dummy=realpath(fname1, buffer);
		//fprintf(stderr,"Warning, path error %s\n", fname1);
	aifp->fname1 = strdup(buffer);
	aifp->fname1hash = hash(buffer);
#endif
#ifdef _WIN32
	_fullpath(buffer, fname2, PATH_MAX);
	aifp->fname2 = strdup(buffer);
	aifp->fname2hash = hashi(buffer);
#else
	 dummy=realpath(fname2, buffer);
	aifp->fname2 = strdup(buffer);
	aifp->fname2hash = hash(buffer);
		//fprintf(stderr,"Warning, path error %s\n", fname2);
#endif
	return aifp;
}
 void appendAsmIncInfo(asmIncInfo **h, asmIncInfo**t, asmIncInfo *p)
 {
	 if (*h == NULL)
		 *h = *t = p;
	 else
	 {
		 (*t)->next = p;
		 (*t) = p;
	 }
 }
 // if we give ^aaa.asm:20^bb.s:10
// srcLine *findSrcLineByLocTree(srcLine *head, char *locTree, int level)
// {
//	 char *buffer;
//	 char buffer2[PATH_MAX+1];
//	 srcLine *head1 = head;
//	 int i;
//#ifndef _WIN32
//	char *rp;
//#endif
//	 int nloc=0;
//	 int more = 0;
//	 int lineno;
//	 uint64 fnameHash;
//	 // try to find it by last pos
//	 //static srcLine *lastpos=NULL; // for level0 only
//	 //strncpy(buffer, locTree, 1023);
//	 buffer = strdup(locTree);
//	 for (i = 0; i < 1023; i++)
//	 {
//		 if (buffer[i] == '\0' || (i!=0 && buffer[i] == ','))
//		 {
//			 if (buffer[i] != 0)
//				 more = 1;
//			 buffer[i] = '\0';
//			 break;
//		 }
//		 if (buffer[i] == ':')
//		 {
//			 buffer[i] = '\0';
//			 nloc = i + 1;
//		 }
//	 }
//	 lineno = atoi(buffer + nloc);
//	 
//#ifdef _WIN32
//	 _fullpath(buffer2,buffer+1, PATH_MAX);
//	 fnameHash = hashi(buffer2);
//#else
//	// dummy=realpath(buffer+1, buffer2);
//	 rp=realpath(buffer + 1, buffer2);
//	 fnameHash = hash(buffer2);
//		//fprintf(stderr,"Warning, path error %s\n", buffer+1);
//#endif
//	 // try last result
//	 //if (!more && lastpos && level == 0 && lastpos->fnameHash == fnameHash)
//	 //{
//		// while (lastpos)
//		// {
//		//	 if (lastpos->lineno == lineno)
//		//	 {
//		//		free(buffer);
//		//		 return lastpos;
//		//	}
//		//	 lastpos = lastpos->next;
//		// }
//	 //}
//
//	 while (head)
//	 {
//		 //if (lineno == )
//			// fprintf(stderr, "strangelineno\n");
//		 if (head->fnameHash == fnameHash)
//		 {
//			 if (head->lineno == lineno)
//				 break;
//		 }
//		 head = head->next;
//	 }
//	 if (head == NULL)
//	 {
//		 for (head = head1; head; head = head->next)
//		 {
//			 fprintf(stderr,"%llX,%d,\t%s\n",  head->fnameHash, head->lineno, head->line);
//		 }
//		 fprintf(stderr, "Asm/Linker Internal Err: %s list entry cannot find:%s\n",__FUNCTION__, locTree);
//		 exit(-__LINE__);
//	 }
//	 if (more)
//		 return findSrcLineByLocTree(head, locTree + i, level + 1);
//	 //else
//		// lastpos = head;
//	free(buffer);
//	 return head;
// }
