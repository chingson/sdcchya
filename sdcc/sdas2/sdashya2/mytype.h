#ifndef __MYTYPE_H__
#define __MYTYPE_H__
// we define data structures
#include <stdint.h>
//#define _CRTDBG_MAP_ALLOC
//#include <crtdbg.h>

#include "city.h"
#define MAXSTRSIZE0 127
#define MAXSTRSIZE1 129

// newer version, for push struct!!
#define ASLNKVER "SDASHYA_201"

#ifdef _MSC_VER
extern int yylex(void);
#define strncasecmp _strnicmp
#define strdup _strdup
#define strcasecmp _stricmp
#define strncmpi _strnicmp
#define PATH_MAX _MAX_PATH
#define fileno _fileno
#else
#ifdef __MINGW32__
#ifndef PATH_MAX
#define PATH_MAX _MAX_PATH
#endif
#define strncmpi _strnicmp

#else
#include<linux/limits.h>
#define strncmpi strncasecmp
#endif
#endif



//#define printf(x) fprintf(stderr,"%s\n",x)
//#define printf3(x,y,z) fprintf(stderr,x,y,z)
//#define printf2(x,y) fprintf(stderr,x,y)
// expressions 
    typedef enum {
	ExpIsNumber=1,
	ExpIsSymbol,
	ExpIsASCII, // for .ascii
	ExpIsTree,
	ExpIsPCNow,
	ExpIsStackOffset // for 15P41 expression!!
	} expressionType;
    typedef struct s_expression expression;

	typedef enum {
		regionCODE=1,
		regionDATA,
		regionXDATA,
		regionSTK
	} regionType;

    typedef struct sTree {
	expression *left;
	char op;
	expression *right;
	unsigned int OverflowChk : 1; // default overflow use signed,
	unsigned int OverflowChkUnsign : 1;  // this bit use unsigned
     } expressionTree;

    struct s_expression {
	expressionType type;
	uint64 symHash;
	unsigned int ExpSymIsExtern : 1;
	unsigned int ExpSymIsX : 1;
	unsigned int ExpSymImmed : 1; // for immediate addressing mode
	//unsigned int InstHighByte : 1; // for sync in linker
	int lineno;
	
	union {
		int  number;// -1 means this is PCNOW
		char symbol[MAXSTRSIZE0+1];
		expressionTree tree;
	} exp;
     } ;
	typedef struct sArea area;
	typedef struct swdata wData;
	typedef struct sModuleLD moduleLD;
	typedef struct sFuncRecord funcRecord;
	typedef struct sDataItem dataItem;

	typedef struct localLabels{
		char *string;
		unsigned int isLocal:1;
	} macroParaT;


    typedef struct ssymbol {
        unsigned int symIsLabel:1; // it is an label
		unsigned int symIsEQU : 1; // it is expression
        unsigned int symIsGlobl:1; // if globl sym, we export it
		unsigned int symIsByLinker : 1; // linker generated
		unsigned int symIsExt : 1; // for linker it needs to know it is external
		unsigned int symIsFunction : 1; // function is also a label
		unsigned int symIsFuncLocal : 1; // func local is stack, used in linker
		unsigned int symIsFuncLocalPara : 1; // used in linker, it is local and is para!!
		unsigned int symIsByFSR2 : 1;
		//unsigned int needZP : 1; // when this var is following a skip instruction, in H08D it should be ZP if operand of prev is a different one
		unsigned int  symNoLabel : 1; // the local for local address calculation has not label

		funcRecord *fpHasthis; // which fp has this as local!!
		
		int EQULineNo;
		
        char name[MAXSTRSIZE0]; // symbol name
		uint64 hashid;
		wData *labelPos;
		area *ap; // where it belongs
		expression* exp;
		moduleLD *ldm; // for linker use to get the value
		funcRecord *func; // keep the record
        struct ssymbol * next_in_Module;
		struct ssymbol * next_in_Area;
		struct ssymbol * definedPos; // for linker use, reference to export point
		int symSize;// for linker, need to know the size for local alloc
		int fsr2origOffset; // if no count later push/pop, this is the original offset with respect to FSR2 (before localpush)
		// after local shift, linker will calculate the shift additional FSR2
		int areaOffset; // if symbol have area offset assigned (>=0)
		char *cdbRecord; // for typematch
		int funcParaNum;
		
    } symbol;

	typedef struct sGloLocItem {
		uint64 hashid; // hashcode is enough
		unsigned int setGlobl : 1;
		unsigned int referenced : 1;
		char *name;
		struct sGloLocItem *next;

	} gloLocItem; // global or local

	// this type is for linker, no use in assembler
	struct sModuleLD {
		char name[MAXSTRSIZE0];
		char srcName[MAXSTRSIZE0];
		char asFromDir[PATH_MAX];
		uint64 hashcode;
		symbol *exportSym[32768];
		symbol *refSym[32768];
		symbol *localSym[32768];
		funcRecord *funcs[32768];
		int exportSymNum;
		int refSymNum;
		int funcNum;
		int localSymNum;
		struct sModuleLD *next; 
		unsigned int isFromLib : 1; 
		unsigned int symResolved : 1; // to speed up resolve sym
		char libPath[PATH_MAX];
		// for wdata, it is pointed by them
	};

	struct sFlowList {
		struct swdata *cpuNext;
		struct sFlowList *next;
		unsigned int isBackFromCall : 1; // it means it is back from call
		unsigned int analyzing : 1; // now ana
		unsigned int fromLoop : 1; // branch is from loop
		// if the called function not touch BSR, the analysis may skip the call
		// to speed up, and my save code spaces
		struct sFuncRecord *fp;
	};

     struct swdata {
        //unsigned int uid;
		unsigned int wdIsMacroMark:1; // macromark
		unsigned int wdIsMacroMarkEnd:1; // macromark
		unsigned int wdIsInMacro : 1; // following is assembly macro
		unsigned int wdIsInInclude : 1;
        unsigned int wdIsBlank:1;
        unsigned int wdIsOrg:1; // means this is a org entry
        unsigned int wdIsLabel:1; // just a label point to a symbol
		unsigned int wdIsInstruction : 1; // only instruction can it be optimized.
		unsigned int wdIsCall : 1; // for call xxx, later id can be declared implicit
		unsigned int expCodeLabelDiv2 : 1; // for compatible mode, symbol in code need div2, when asm!!
											// above will add after simplify
		unsigned int wdIsFunction : 1;
		unsigned int wdIsFunctionEnd : 1;
		unsigned int wdBlankIsFuncLocal : 1; // used in linker, allocate when call trace
		unsigned int wdBlankIsByFSR2 : 1;		// it need to be mark if this is by FSR2
		unsigned int wdBlankIsFuncLocalX : 1; // used in linker, allocate when call trace, X area
		unsigned int wdIsErrMark : 1;
		unsigned int instHighByte : 1;
		unsigned int instSkip : 1; // possible skip
		unsigned int labelFixOffset : 1;
		unsigned int hasRomLabel : 1;// if has rom label, it cannot be common-code reuse, because address changing

		unsigned int callfptr : 1; // if fptr is called, BSR will be destroyed
		unsigned int callNormal : 1; // call normal then return at its end.
		unsigned int called : 1; // when construct flow, it need to be set as "called" for stack oper
		unsigned int retHere : 1; // retl/reti/ret
		unsigned int bsrRelated : 1; // if the instruction has "a" bit, it is bsr related!!, mark for further process
		unsigned int pushlChecked : 1; // every code need to be checked for once!! once is enough
		unsigned int pushlWarned : 1;
		unsigned int dummyPushFixed : 1;
		unsigned int noCommonCode : 1; // some times it is not allowed for common code like 7802
		unsigned int callerBackFSR2: 1; // for p58 large stadarg.h call, moving back need many ADDFSR2, instead of single
		unsigned int stkxxAux : 1; // aux flag for stkxx optimization, speed up.
		unsigned int modified2indf0 : 1; // modified to indf0
									// we try not to use indf1, because indf1 reserved for user
		int bankVal; // bank is 4 bit, this bank is for varable (calculateBSR)
		int bankFlow; // this value is for lbsr flow 0 is not check, 99 is not fix,
			// 100 is bank 0, 101 is bank 1

        int sizeOrORG;// can be size or offset
		int areaOffset; // the offset in the area
		int mark;
		
	

		symbol *sym; // if it is label, there is a sym
		struct sFuncRecord *fp;
		int lineno; // corresponding source code lineno
		int macroLevel;
		int includeissue_lineno;
		char *include_fname;
		char *macroSrc; // if it is in macro, we add the text in list files [for err mark, this is error msg]
		int macro_id; // if macro, there is macro id
		int fsr2_stack_offset; // if code, and p41, there will be stack offset
		
		//uint64 macro_line_hash; // a hash with macro macrosrclineno, used_lineno
		expression *exp; // expression of this WDATA
		moduleLD *m; // this is used in linker only, which module it belongs
		unsigned int wValue; // this is defined at linker stage, too
		area *ap; // where is wdata is belong
		dataItem *macroPara; // if there is macroPara
		char *locTree;
        struct swdata *next;
		struct swdata *prev; // prev used in 

		struct sFlowList *flowNext; // construct flows for bsr analysis
		struct sFlowList *flowPrev;
		int inCond; // 2021, build cond for RETI optimization
		int outCond;
    } ;

	 typedef struct sFlowList flowList;

    //typedef struct slstdata {
    //    char fname[MAXSTRSIZE];
    //    int lineno;
    //    int refnum;
    //    int refuid[4]; // max ref is 4 entries, swdata
    //    char outFormat[MAXSTRSIZE];
    //    struct slstdata*next;
    //} lstData;


 struct sArea{
        char name[MAXSTRSIZE0];
		uint64 nameHash;
        unsigned int isABS:1; // ABS/REL
        unsigned int isOVL:1; // OVL/CON
        //unsigned int isSTK:1; // for local & para, !! new
        regionType region; // CODE/DATA/XDATA
        symbol *symbols; // symbols are both in area and module
        wData *wDatas;
		symbol *symTail;
		wData *wDataTail;
		wData *wDataTailInst; // tail instruction
		int startAddr; // in linker, occupied start/
		int occuLength; // and length data for reference
		int endAddr;
		int assignedAddr; // if address is assigned we need to sort
		int offsetAddr; // an area may have many declare and there will be offset for following data

		int accumulatedSize;
        struct sArea *next;
    };

    
    
 

	 typedef struct sLine {
		 int lineno;
		 char *line;
		 uint64 lineHash;
		 uint64 fnameHash;
		 char *locTree;
		 uint64 locHash;
		 //int wDataNum;
		 //wData **warray;
		 wData *wDataStart;
		 wData *wDataEnd;
		 unsigned int onROM : 1;
		 unsigned int modLine : 1; // head with $MOD
		 unsigned int exportLine : 1; // head with $EXPORT
		 uint64 expHash;
		 struct sLine *next;
		 struct sLine *prev;
	 }srcLine;

	 struct sDataItem {
		 expression *exp;
		 unsigned int flag : 1; // local parameters
		 struct sDataItem *next;
	 };


	 typedef enum {
		 addr_fda = 1, // skip 0 default value
		 addr_fba,
		 addr_fa,
		 addr_lit,
		 addr_lbsr,
		 addr_kf,
		 addr_fsfd,
		 addr_n12,
		 addr_nopr,
		 addr_n8,
		 addr_n11,
		 addr_addbit,
		 addr_tblr,
		 addr_addfsr,
		 addr_addulnk,
		 addr_mvsf,
		 addr_mvss
	 } addr_mode;

	 typedef enum {
		 mneIsInstruction = 1,
		 mneIsLabelMark,
		 mneIsAreaD,
		 mneIsModuleD,
		 mneIsGlobalMark,
		 mneIsEQU,
		 mneIsORG,
		 mneIsDBDS,
		 mneIsFuncRec,
		 mneIsFuncEnd,
		 mneIsMacro,
		 mneIsErr,
		 mneIsPARACHK,
		 mneIsCALLFPTR
	 } mneType;

	 typedef struct sInstruction {
		 int lineno;
		 int wordnum;
		 int opcode1;
		 int opcode2;
		 expression * bord;
		 expression * aexp; // for the compatibility of old source!!
		 addr_mode addrMode;
		 expression *operand;
		 expression *operand1;
	 } Instruction;

	 typedef struct sCallFPTRMark {
		 int lineno;
	 } callFPTRMark;

	 typedef struct sLabelMark {
		 int lineno;
		 int uuid;
		 char name[MAXSTRSIZE0];
	 } LabelMark;
	 typedef struct sEquMark {
		 int lineno;
		 char name[MAXSTRSIZE0];
		 uint64 nameHash;

		 expression *valueexp;
	 } equMark;

	 //   typedef struct sDataItem dataItem;
	 typedef struct sGlobalMark {
		 int lineno;
		 int uuid;
		 unsigned int symIsGlobl : 1;
		 dataItem *items;
	 } globalMark;

	 struct sFuncRecord {
		 char name[MAXSTRSIZE0];
		 int lineno;
		 dataItem *callList;
		 dataItem *localList;
		 area *areap; // code or interrupt
					  // for simplicity, following is used in linked phase
		 int calledFuncNum;
		 int highestCallLevel; // for common code reuse
		 uint64 namehash;
		 //wData *firstWD1;// include label
		 //wData *finalWD1;// include following data if any
		 wData *firstWD;
		 wData *finalWD;
		 moduleLD *m; // the module of the function
		 struct sFuncRecord *calledFunc[256]; // max call 256 functions
		 char *inlinefunc_name[256]; // it may inline .... oh 
		 uint64 inlinefunc_name_hash[256];
		 int paramSTKReMap[128];
		 int inlinefunc_num;
		 int declaredParamSize; // added for H08D, because function pointer cannot optimize the para out
		 int returnSize; // 2021 return byte size
		 int BSR_State; // 0: not checked, 1: modified, 2: no modify if no modify, it used old one "pass call gen flow"
		 symbol * localSym[256]; // maximum 256 local symbol
		 symbol * localXSym[4096];
		 wData *retLoc[1024]; // it will not exceed 1024 places to return
		 int retLocCount;
		 wData *callmeWDP[8192]; // where I am called, for inter module optimization
		 int callmeWDPNum;
		 int localSymNum; // add zp and XDATA (normal page)
		 int localSymSize; // for recursive, the size is generally fixed unless paraOnStack!! we need additional operations!!
		 int localSymSizeExcludePara;
		 int realParaSize;
		 // stack seperated make the para and local cancatnated

		 int localXSymNum;
		 int localXSymSize;
		 int baseLocalAddr;
		 int baseLocalXAddr;
		 int callTooDeepWarnedLev; // high levels

		 unsigned int proemi_added : 1; // set 1 if prologue/epilogue added
		 unsigned int calledByMainThread : 1;
		 unsigned int reEntryWarnShown : 1;
		 unsigned int calledByInterruptThread : 1;
		 unsigned int doubleThreadWarned : 1;
		 unsigned int needReAssign : 1; // when pcall, the mark means it needs reassign local var, always
		 unsigned int noAllocWarned : 1;
		 
		 unsigned int p41ReEntry : 1; // re-entry is defined by linker
		 unsigned int h08dReEntry : 1; // h08d re-entry is more efficient
		 unsigned int paraOnStack : 1;
		 unsigned int spreaded : 1; // 1 means calld_func are traced
		 unsigned int spread2 : 1; // second spread
		 unsigned int interruptPushPullAdded : 1;
		 unsigned int calledByFPTR : 1;
		 unsigned int fsr2set : 1;

		 unsigned int doCallFPTR : 1; // it calls fptr functions
		 unsigned int retVchecked : 1; // if return value checked, it will be marked (only once is enough)
		 unsigned int callret2jmpOptimized : 1; // if callret becomes jmp, we don't optimize this function

	 };

	 typedef struct sFuncEnd {
		 int lineno;
		 char name[MAXSTRSIZE0];
	 } funcEnd;

	 typedef struct saread {
		 int lineno;
		 unsigned int isABS : 1;
		 unsigned int isOVL : 1;
		 unsigned int isSTK : 1;
		 int region;
		 int offsetAddr;
		 char name[MAXSTRSIZE0];
	 } aread;

	 typedef struct smoduled {
		 int lineno;
		 char name[MAXSTRSIZE0];
	 } moduled;
	 // for .ascii ...

	 typedef struct sDBDS {
		 int lineno;
		 unsigned int hasContent : 1;
		 unsigned int hasSize : 1;
		 int macroLevel;
		 char *macroLine;
		 //uint64 macroHash; // new version remove macroHash
		 int size;
		 dataItem *items;
	 } DBDS;

	 typedef struct sOrg {
		 int lineno;
		 int offset;
	 } Org;

	 typedef struct sAsmMacro {
		 unsigned int macro_end : 1;
		 int src_lineno;
		 int upper_macro_offset; // if multi-level, we need to know upper level offset
		 int total_lineno;
		 int level; // macro level ... can be many
		 char issueLine[4096];
		 int macro_id;
		 dataItem *paras;
	 } asmMacro;

	 typedef struct sErr {
		 int src_lineno;
		 char *msg;
	 }err_tag;

	 typedef struct sPARACHK {
		 int src_lineno;
		 char *parachkInfo;
		 int paraChkNum;
	 }paraCheckMne;

	 typedef struct sMNE {
		 mneType type;
		 union {
			 Instruction inst;
			 LabelMark label;
			 DBDS	dbds;
			 aread	area;
			 moduled module;
			 equMark equ;
			 Org		org;
			 globalMark globl;
			 funcRecord func;
			 funcEnd func_end;
			 asmMacro asm_macro;
			 err_tag err;
			 paraCheckMne paraChk;
			 callFPTRMark callFPTR;
		 } m;
		 int include_lev;
		 int includeissue_lineno;
		 char *include_fname;
		 char *locTree;
		 int macroLevel; // if it is generated by macro
		 char *macroLine; // the line content of the macro
		 //uint64 macroHash;
		 int macroid;
		 uint64 macrolinehash; // if same line, same hash
		 struct sMNE *next;
	 } MNE;
	 typedef struct sModule {
		 char name[PATH_MAX+5]; // this use larger is fine
		 MNE *mneList;//lstData *lst;
		 area *areas;
		 symbol *symbols;
		 symbol *symTail;
		 gloLocItem *gloHead;
		 gloLocItem *gloTail;
		 srcLine *srcHead;
		 srcLine *srcTail;

	 } module;
	 typedef struct asmIncInfos { // for asm only, C is another story, don't include twice 
		 char *fname1;
		 int lineno;
		 uint64 fname1hash;
		 char *fname2;
		 uint64 fname2hash;
		 int fname2lineno;
		 struct asmIncInfos *next;
	 } asmIncInfo;
	

	char *appendFinalSlash(char *path);
		
	char *removeNewLine(char* str);
	srcLine *newSrcLine(char *line, int lineno);
	area *newArea(char *name, int isABS, int isOVL, regionType region);
	area* appendArea(area *h, area *n);
	uint64 hash(char *str);
	uint64 hashi(char *str); // ignore case
	//uint64 Hash1( char *, int len);
	symbol *newSymbol(char *name);
	wData *newWDATA(area *ap);
	wData *newWDATA1(area *ap);
	expression * newEmptyExp(int lineno);
	wData *appendTailWDATA(area *ap, wData *n,int );
	srcLine *newSrcLine(char *line, int lineno);
	void delSrcList(srcLine *srcList);
	srcLine *appendSrcLine(srcLine *h, srcLine *n);
	char *exprDump(expression *exp, char *buffer, int maxlen);

	//int readFile2SrcLine(char *fname, srcLine **h, srcLine *searchDir, char *fullpath, int maxlen);
	int readFile2SrcLine2(char *fname, srcLine **h, srcLine **t, srcLine *searchDir, char *fullpath, int maxlen,asmIncInfo *aifph, int *totalLineno, char *rootloc);

	srcLine *findSrcLineLocTree(srcLine *h, char *locTree);

	int writeSrcLine2File(char *fname, srcLine *h);
	int writeSrcLine2Filep(FILE *fpo, srcLine *h);
	char *getSymNameOfLine(char *buffer, srcLine *s, int maxlen);
	char *getModNameOfLine(char *buffer, srcLine *s, int maxlen);

	int copyPart2NewSrcLine(srcLine *start, srcLine **h, srcLine **tail);

	srcLine *concat2list(srcLine **head, srcLine *tail);
	srcLine *removeModInList(srcLine **head, srcLine *modLine);
	srcLine *checkExportSymDouble(srcLine *lib, srcLine *rel);
	//char *getOnlyBaseName(char *longName, char *buffer, int lenmax); // for filename to basename
	int printWDATA(char *buffer, int bufmax, wData *wdp, int longmode, int value );
	int getExpMinValue(expression *exp);

	
	dataItem * newEmptyDataItem(void);

	expression *newConstExp(int number, int lineno);
	
	expression *newPCNOWExp(int lineno);
	

	expression *newSymbolExp(char *symName, int lineno);
	 asmIncInfo *newAsmIncInfo(char *fname1, int lineno, char *fname2);
	void appendAsmIncInfo(asmIncInfo **h, asmIncInfo**t, asmIncInfo *p);
	

	expression *newTreeExp(expression*left, expression *right, char op, int lineno);

	extern srcLine *errMsgSL, *errMsgSLT;
	extern int appendErrMsg(char *msg);
	extern srcLine *appendSrcLine2(srcLine **h, srcLine **t, srcLine *n);
	extern int showMsgSL(srcLine **);
	extern int str2upper(char *s);
	extern int str2lower(char *s);

	extern area * parserArea;
int split2lines(char *str, char **splitstr) ;

//srcLine *findSrcLineByLocTree(srcLine *head, char *locTree, int level); // old method
typedef struct macros {
	char *macroName;
	uint64 nameHash;
	dataItem *paras;
	int declareLineNo;
	char *declareSrcFName;
	srcLine *lines;
	srcLine *linestail;
	char *macroRootLoc;
	unsigned int used : 1;
	unsigned int isRPT : 1;
} macrot; // move macro_t here

int insertMacro2SrcLines(srcLine *inspoint, macrot *m); // after inspoint

#endif

