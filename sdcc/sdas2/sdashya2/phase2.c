
#include <limits.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>
#ifndef _WIN32
#include <libgen.h>
#include <unistd.h>
#else
#include <windows.h>
#include <direct.h>
#endif

#include "mytype.h"
#include "asparser.h"

// phase2 construct area and label / module

// module check, get the module specified
// if there are too many module decl, it is an err
int phase2err = 0;
#ifndef _WIN32
static char *dummy;
#endif
int phase2err_num = 0;
char phase2err_str[][256] = {
	"Error, too many module defininition.",
	"area defines are not the same.",
	"error the label is redefined.",
	"error EQU left is redefined.",
	"globl assign error.",
	"dbdw err",
	"mne type err. ",
	"lst file open err. ",
	"org value error.",
	"Error..org can only for ABS area.",
	"error ..Unsolvable expression ... ascii?", //10
	"Expression symbol cannot resolve",//11
	"EQU symbol cannot resolve", //12
	"result out of range", //13
	"OBJ File Open Fail." //14
};
#define PHASE2ERRMAX 15
// we need to add include file information
// new kind of info
// both LST and RST can have it
// in the REL: $ASMINC:"KKK.ASM":lineno:"JJJ.ASM" ==> it will be much easier!!
#define MAX_PARA_CHECK_LINE 32768
static int paraChkTotal = 0;
static char * paraChkName[MAX_PARA_CHECK_LINE];
static int paraChkSize[MAX_PARA_CHECK_LINE];
static uint64 paraChkNameHash[MAX_PARA_CHECK_LINE];
extern char input_filename[PATH_MAX];

int appendErr(MNE *mp, area *current_area);
int appendErrPhase2(MNE *mp, area *current_area,char * description);

int expression_has_label(expression *);

asmIncInfo *asmIncHead = NULL;
asmIncInfo *asmIncTail = NULL;

static MNE* moduleCheck(MNE *mp)
{
	int modulen = 0;
	MNE *foundModuleDecl = NULL;
	while (mp)
	{
		if (mp->type == mneIsModuleD)
		{
			foundModuleDecl = mp;
			++modulen;
		}
		mp = mp->next;
	}
	if (modulen > 1)
	{
		phase2err = 1; // too
	}
	return foundModuleDecl;
}
static void showErrPhase2(void)
{
	if (!phase2err)
		return;
	if (phase2err - 1 <= PHASE2ERRMAX)
		fprintf(stderr, "%s\n", phase2err_str[phase2err - 1]);
	else
		fprintf(stderr, ".. Process Failed.\n");
}

// per new area, we need new s_ l_ e_, means start, length and end



static char *getFileBaseName(char *filename)
{
	// need to remove other paths

#ifdef _WIN32
	char drive[10];
	char dir[PATH_MAX + 1];
	char base[PATH_MAX + 1];
	char ext[PATH_MAX + 1];
	_splitpath(filename, drive, dir, base, ext);
	strcpy(filename, base);
	return filename;
#else


	int i;
	
	filename=basename(filename);
		i = strlen(filename) - 1;

	while (i >= 0)
	{
		if (filename[i] == '.')
		{
			filename[i] = '\0';
			break;
		}
		i--;
	}
		
	return filename;
#endif
}


symbol *getSymByName(char *name)
{
	uint64 hashcode = symbolCaseSensitivity?hashi(name):hash(name);
	symbol *sp = topModule.symbols;
	while (sp)
	{
		if (sp->hashid == hashcode )
			return sp;
		sp = sp->next_in_Module;
	}
	return NULL;
}


area *searchArea(MNE *mp)
{
	area* ap = topModule.areas;
	uint64 hashid = symbolCaseSensitivity?hashi(mp->m.area.name):hash(mp->m.area.name);
	while (ap)
	{
		if (hashid==ap->nameHash)
		{
			if (mp->m.area.isABS != ap->isABS ||
				mp->m.area.isOVL != ap->isOVL ||
//				mp->m.area.isSTK != ap->isSTK ||
				mp->m.area.region != ap->region)
			{
				phase2err = 2;

				return NULL;
			}

			return ap;
		}
		ap = ap->next;
	}
	return ap;
}
// change area by MNE
area *changeArea(MNE* mp)
{
	// at first we search if already declared
	area *ap = searchArea(mp);
	if (ap)
	{
		
		ap->offsetAddr = mp->m.area.offsetAddr;
	

		return ap;
	}
	ap = newArea(mp->m.area.name, mp->m.area.isABS, mp->m.area.isOVL,
		 mp->m.area.region);
	topModule.areas = appendArea(topModule.areas, ap);
	ap->offsetAddr = mp->m.area.offsetAddr;
	return ap;

}



// we append a label and check if it is already defined
// if defined error 3
static int appendLabel(MNE *mp, area *current_area)
{
	symbol *sp;
	wData *wdp;
	if ((sp=getSymByName(mp->m.label.name))!=NULL && !sp->symIsFunction)
	{
		phase2err = 3;
		fprintf(stderr, " label %s error ", mp->m.label.name);
		return 3;
	}
	if (!sp)
	{
		sp = newSymbol(mp->m.label.name);
		sp->ap = current_area;
	}
	
	sp->areaOffset = current_area->offsetAddr;
	sp->symIsLabel = 1;
	// now append a wDATA
	wdp = newWDATA(current_area);
	if (current_area->offsetAddr >= 0)
	{
		wdp->labelFixOffset = 1; // fixed area offset
		wdp->areaOffset = sp->areaOffset;
	}
	wdp->lineno = mp->m.label.lineno;
	wdp->wdIsLabel = 1;
	wdp->sym = sp;
	sp->labelPos = wdp;
	wdp->locTree = mp->locTree;
	appendTailWDATA(current_area, wdp,0);
	if(!sp->symIsFunction)
		appendNewSymbol(current_area, sp); // append in both area and topModule
	if (mp->macroLevel >= 0)
	{
		wdp->wdIsInMacro = 1;
		wdp->macroLevel = mp->macroLevel;
		wdp->macroSrc = mp->macroLine; // for showing macro in list file, it is really tricky!!
		wdp->macro_id = mp->macroid;
		//wdp->macro_line_hash = mp->macroHash;
	}
	if (mp->include_lev)
	{
		wdp->wdIsInInclude = 1;
		wdp->include_fname = mp->include_fname;
		wdp->includeissue_lineno = mp->includeissue_lineno;
	}
	wdp->locTree = mp->locTree;
	return 0;
}
static funcRecord *currentFunc = NULL;
// only mark the func
static int appendCALLFPTR(MNE *mp, area *current_area)
{
	if (currentFunc != NULL)
		currentFunc->doCallFPTR = 1;
	return 0;
}
static int appendFunction(MNE *mp, area *current_area)
{
	symbol *sp;
	wData *wdp;
	if ((sp = getSymByName(mp->m.func.name)) != NULL)
	{
		phase2err = 3;
		fprintf(stderr, " label %s error ", mp->m.label.name);
		return 3;
	}
	sp = newSymbol(mp->m.func.name);
	sp->symIsFunction = 1;
	sp->ap = current_area;
	// now append a wDATA
	wdp = newWDATA(current_area);
	wdp->lineno = mp->m.func.lineno;
	wdp->sym = sp;
	wdp->wdIsFunction = 1;
	sp->func = & (mp->m.func); // it should exist, still
	currentFunc = &(mp->m.func); // for later CALLFPTR
	sp->labelPos = wdp;
	wdp->locTree = mp->locTree;
	appendTailWDATA(current_area, wdp,0);
	appendNewSymbol(current_area, sp); // append in both area and topModule

	return 0;
}
static int appendFunctionEnd(MNE *mp, area *current_area)
{
	symbol *sp;
	wData *wdp;
	if ((sp = getSymByName(mp->m.func_end.name)) == NULL)
	{
		phase2err = 10;
		fprintf(stderr, " funcend 's func-head %s not found\n ", mp->m.func_end.name);
		return 3;
	}
	wdp = newWDATA(current_area);
	wdp->lineno = mp->m.func_end.lineno;
	wdp->sym = sp;
	wdp->wdIsFunctionEnd = 1;
	wdp->locTree = mp->locTree;
	appendTailWDATA(current_area, wdp,0);


	return 0;
}


// this is the opcode a with high byte B9 | A bit
expression *instHighByteWithOperandB9(Instruction *inst)
{
	expression *base=NULL;
	expression *baseA = NULL;
	expression *shiftr8 = NULL;
	expression *and1;

	if (inst->aexp && ramModel == MODELMEDIUM)
	{
		// check aexp
		inst->aexp = newTreeExp(inst->aexp, inst->operand, 'C', inst->lineno);
	}
	switch (inst->addrMode)
	{
		case addr_fa:
			//base = newConstExp((inst->opcode1 >> 8) | inst->a, inst->lineno);
			base = newTreeExp(newTreeExp(newConstExp((inst->opcode1 >> 8), inst->lineno),
				newTreeExp(inst->aexp, newConstExp(1,inst->lineno), '&', inst->lineno)
				, '|', inst->lineno), inst->operand1, '|', inst->lineno);
			break;
		case addr_fba:
		case addr_fda:
			baseA = newTreeExp(newConstExp((inst->opcode1 >> 8), inst->lineno),
				newTreeExp(inst->aexp, newConstExp(1, inst->lineno), '&', inst->lineno)
				, '|', inst->lineno);
			//base = newTreeExp(newConstExp((inst->opcode1 >> 8) | inst->a, inst->lineno),
			base = newTreeExp(baseA, 
				newTreeExp(inst->bord, newConstExp(1,inst->lineno), 'B', inst->lineno), '|', inst->lineno);
			//base = newConstExp((inst->opcode1 >> 8) | inst->a | ((inst->bord) << 1), inst->lineno);
			break;
		default:
			return NULL;
	}
	if (ramModel == MODELMEDIUM)
	{
		return newTreeExp(base, inst->aexp, '|', inst->lineno);
	}
	shiftr8 = newTreeExp(inst->operand, newConstExp(8, inst->lineno),'A', inst->lineno);
	and1 = newTreeExp(shiftr8, newConstExp(1, inst->lineno),'&', inst->lineno);
	and1->exp.tree.OverflowChk = 1; // overflow is error
	and1->exp.tree.OverflowChkUnsign = 1; // b9 is for fda/fba access, it is unsigned!!
	return newTreeExp(base, and1,'|', inst->lineno);
}

expression *instHighByteWithOperandB12(Instruction *inst, int wordind)
{
	expression *base = NULL;
	expression *shiftr8 = NULL;
	expression *andf = NULL;
	if (!wordind) // =0
	{
		switch (inst->addrMode)
		{
		case addr_fsfd:
			base = newConstExp(inst->opcode1 >> 8, inst->lineno);
			shiftr8 = newTreeExp(inst->operand,  newConstExp(8, inst->lineno),'A', inst->lineno);
			andf = newTreeExp(shiftr8, newConstExp(0x0f, inst->lineno),'&', inst->lineno);
			andf->exp.tree.OverflowChk = 1; // MVFF is always unsigned!!!!
			andf->exp.tree.OverflowChkUnsign = 1;
			return newTreeExp(base, andf,'|', inst->lineno);

		default:
			return NULL;
		}
	}
	else
	{
		// second word is 0xf000 | xxx
		switch (inst->addrMode)
		{
		case addr_fsfd:
			base = newConstExp(0xf0, inst->lineno);
			shiftr8 = newTreeExp(inst->operand1,  newConstExp(8, inst->lineno),'A', inst->lineno);
			andf = newTreeExp(shiftr8,  newConstExp(0x0f, inst->lineno),'&', inst->lineno);
			andf->exp.tree.OverflowChk = 1;
			andf->exp.tree.OverflowChkUnsign = 1; // fsfd is not relative, unsigned
			return newTreeExp(base,  andf,'|', inst->lineno);
		default:
			return NULL;
		}
	}
	return NULL;
}



expression *instructionHighByteN11(Instruction *inst)
{
	expression *diff;
	expression *sh9;
	expression *and7;
	expression *base;
	expression *highbyte;
	int haslabel = expression_has_label(inst->operand);

	base = newConstExp(inst->opcode1 >> 8, inst->lineno);
	if (is08b || !haslabel)
		diff = inst->operand;
	else
		diff = newTreeExp(inst->operand, newPCNOWExp(inst->lineno), '-', inst->lineno);
	sh9 = newTreeExp(diff, newConstExp(haslabel?9:8, inst->lineno), 'A', inst->lineno);// diff >>9
	and7 = newTreeExp(sh9, newConstExp(7, inst->lineno), '&', inst->lineno); and7->exp.tree.OverflowChk = 1; // it is signed for n11, unsigned for 08b
	if (is08b)
		and7->exp.tree.OverflowChkUnsign = 1; // unsigned check
	highbyte = newTreeExp(base, and7, '|', inst->lineno);

	return highbyte;

}
expression *instructionLowByteN11(Instruction *inst)
{
	expression *diff;
	expression *sh1;
	expression *andff;
	int haslabel = expression_has_label(inst->operand);

	if (is08b || !haslabel)
		diff = inst->operand;
	else
		diff = newTreeExp(inst->operand, newPCNOWExp(inst->lineno), '-', inst->lineno);
	sh1 = newTreeExp(diff, newConstExp(haslabel?1:0, inst->lineno), 'A', inst->lineno);//>>1
	andff = newTreeExp(sh1, newConstExp(0xff, inst->lineno), '&', inst->lineno);

	return andff;

}
expression *instructionLowByteN8(Instruction *inst)
{
	expression *diff;
	expression *sh1;
	expression *andff;
	int haslabel = expression_has_label(inst->operand);

	if (!haslabel)
		diff = inst->operand;
	else
		diff = newTreeExp(inst->operand, newPCNOWExp(inst->lineno), '-', inst->lineno);
	sh1 = newTreeExp(diff, newConstExp(haslabel?1:0, inst->lineno), 'A', inst->lineno);// diff >>9
	andff = newTreeExp(sh1, newConstExp(0xff, inst->lineno), '&', inst->lineno); andff->exp.tree.OverflowChk = 1; // signed!!

	return andff;
}

expression *instructionHighByte(Instruction *inst, int wordind) // construct byte expression from ...MNE
{
	expression *exp;


	

	switch (inst->addrMode)
	{
		case addr_addbit:
			return newConstExp(inst->opcode1 >> 8, inst->lineno);
		case addr_fa: // FA high bit is a | ((operand&0x100)>>8)
		case addr_fba:
		case addr_fda:
			return instHighByteWithOperandB9(inst);
		case addr_fsfd:
			return instHighByteWithOperandB12(inst, wordind);
		case addr_kf: // LDPR
			if (!wordind) // highword highbyte is 0
				return newConstExp(inst->opcode1>>8, inst->lineno);
			exp = newTreeExp(inst->operand, newConstExp(8, inst->lineno), 'A', inst->lineno); // shift 8
			exp = newTreeExp(exp, newConstExp(0x0f, inst->lineno),  '&', inst->lineno); // and 0f
			exp->exp.tree.OverflowChk = 1;
			exp->exp.tree.OverflowChkUnsign = 1;
			exp = newTreeExp(newConstExp(0xf0, inst->lineno),exp, '|', inst->lineno);
			return exp;
		case addr_lbsr:
		case addr_lit:
		case addr_nopr:
		case addr_n8://jxx, high byte is fixed
		case addr_tblr:
			return newConstExp(inst->opcode1 >> 8, inst->lineno);
		case addr_n11: // 	RJ && RCALL
			return instructionHighByteN11(inst);
		case addr_n12: // N12 is ROM abs, first word is 0xc200 .. more 8 bits is in first low byte
			if(!wordind)
				return newConstExp(inst->opcode1 >> 8, inst->lineno);
			// only for compatible mode literal
			if (compatibleMode && inst->operand->type == ExpIsNumber)
			{
				exp = newTreeExp(inst->operand, newConstExp(8, inst->lineno), 'A', inst->lineno);
			}else
				exp = newTreeExp(inst->operand, newConstExp(9, inst->lineno), 'A', inst->lineno);
			exp = newTreeExp(exp, newConstExp(0x0f, inst->lineno), '&', inst->lineno);
			//exp->exp.tree.OverflowChk = 1; exp->exp.tree.OverflowChkUnsign = 1; // ROM abs is unsigned
			// addressing range is 20 BIT!!!
			exp = newTreeExp(exp, newConstExp(0xf0, inst->lineno) ,'|', inst->lineno);
			return exp;
		case addr_addfsr:
		case addr_addulnk:
			exp = newConstExp(inst->opcode1>>8, inst->lineno); // high byte no change
			return exp;

			// for compiler, stack varable is auto
			// transfer from mvff instruction
			// in linker stage. If assembly need to use, 
			// use number directly
		case addr_mvsf:
			// first number is 7 bit
			// second number is 12 bit
			if (!wordind)
			{
				// first high byte no change
				exp = newConstExp(inst->opcode1 >> 8, inst->lineno); // high byte no change
				return exp;
			}
			// exp = op1 >> 8
			exp = newTreeExp(inst->operand1, newConstExp(8, inst->lineno), 'A', inst->lineno);

			// exp = (exp1 >>8)&0xff, check unsign
			exp = newTreeExp(exp, newConstExp(0x0f, inst->lineno), '&', inst->lineno);
			exp->exp.tree.OverflowChkUnsign = 1;
			exp = newTreeExp(newConstExp(0xf0, inst->lineno), exp, '|', inst->lineno);

			return exp;
		case addr_mvss:
			if (!wordind)
			{
				exp = newConstExp(inst->opcode1 >> 8, inst->lineno); // high byte no change
				return exp;
			}
			exp = newConstExp(0xf0,inst->lineno);
			return exp;


		default:
			fprintf(stderr, "unknown instruction type for high byte expression addrmode:%d\n", inst->addrMode);
	}
	return NULL;

}
expression *instructionLowByte(Instruction *inst, int wordind) // construct byte expression from ...MNE
{
	expression *exp;
	expression *exp1;
	switch (inst->addrMode)
	{
	case addr_addbit:
		return newConstExp(inst->opcode1 &0xff, inst->lineno);
	case addr_fa: // FA high bit is a | ((operand&0x100)>>8)
	case addr_fba:
	case addr_fda:
		return newTreeExp(inst->operand, newConstExp(0xff, inst->lineno), '&', inst->lineno);

	case addr_fsfd:
		if (!wordind)
			return newTreeExp(inst->operand, newConstExp(0xff, inst->lineno), '&', inst->lineno);
		return newTreeExp(inst->operand1, newConstExp(0xff, inst->lineno), '&', inst->lineno);
	case addr_kf: // LDPR
		if (!wordind) // highword highbyte is 0

			return newTreeExp(newConstExp(inst->opcode1 & 0xff, inst->lineno), inst->aexp, '|', inst->lineno);
		return newTreeExp(inst->operand, newConstExp(0xff, inst->lineno), '&', inst->lineno);

	case addr_lbsr: // target shift 8, there is compatible mode
		// not compatible mode, 0x10 | [(operand >>8)&0xf]
		// compatible mode, 0x10 | [operand & 0xf]
		exp = newConstExp(inst->opcode1, inst->lineno);
		if (!compatibleMode)
		{
			// shift 8
			exp1 = newTreeExp(inst->operand, newConstExp(8, inst->lineno), 'A', inst->lineno);
			// & 0xf
			exp1 = newTreeExp(exp1, newConstExp(0x0f, inst->lineno), '&', inst->lineno);
			exp1->exp.tree.OverflowChk = 1; exp1->exp.tree.OverflowChkUnsign = 1;
			// OR 0x10;
			exp = newTreeExp(exp, exp1, '|', inst->lineno);

		}
		else
		{
			exp1 = newTreeExp(inst->operand, newConstExp(0x0f, inst->lineno), '&', inst->lineno);
			exp1->exp.tree.OverflowChk = 1; exp1->exp.tree.OverflowChkUnsign = 1;
			// OR 0x10;
			exp = newTreeExp(exp, exp1, '|', inst->lineno);
		}
		return exp;

	case addr_lit:
		return newTreeExp(inst->operand, newConstExp(0xff, inst->lineno), '&', inst->lineno);
	case addr_nopr:
		return newConstExp(inst->opcode1 & 0xff, inst->lineno);
	case addr_n8://jxx, high byte is fixed, now byte is diff
		return instructionLowByteN8(inst);
	case addr_tblr:
		return newConstExp(inst->opcode1 &0xff, inst->lineno);
	case addr_n11: // 	RJ && RCALL
		return instructionLowByteN11(inst);
	case addr_n12: // N12 is ROM abs, first word is 0xc200 ..
		if (!wordind)
		{
			if(compatibleMode && inst->operand->type==ExpIsNumber)
				exp = newTreeExp(inst->operand, newConstExp(12, inst->lineno), 'A', inst->lineno);
			else
				exp = newTreeExp(inst->operand, newConstExp(13, inst->lineno), 'A', inst->lineno); // >> 13 & 0xff
			exp = newTreeExp(exp, newConstExp(0xff, inst->lineno), '&', inst->lineno);
			exp->exp.tree.OverflowChk = 1; exp->exp.tree.OverflowChkUnsign = 1; // ROM abs is unsigned
			// addressing range is 20 BIT!!!
			return exp;
		}
		if (compatibleMode && inst->operand->type == ExpIsNumber)
			exp = newTreeExp(inst->operand, newConstExp(0, inst->lineno), 'A', inst->lineno);
		else
			exp = newTreeExp(inst->operand, newConstExp(1, inst->lineno), 'A', inst->lineno); // >> 1 & 0xff
		exp = newTreeExp(exp, newConstExp(0xff, inst->lineno), '&', inst->lineno);

		return exp;
	case addr_addfsr:
	case addr_addulnk:
		exp = newTreeExp(inst->operand, newConstExp(0x3f,inst->lineno), '&', inst->lineno);
		exp->exp.tree.OverflowChk = 1;

		exp = newTreeExp(newConstExp(inst->opcode1 & 0xff, inst->lineno), exp, '|', inst->lineno);
		return exp;
	case addr_mvsf:
		if (!wordind)
		{
			// first word low byte is 7 bit, & 0x127
			exp = newTreeExp(inst->operand, newConstExp(127, inst->lineno), '&', inst->lineno);
			exp->exp.tree.OverflowChkUnsign = 1;
			return exp;
		}
		// second low word is operand 1 & 0xff
		exp = newTreeExp(inst->operand1, newConstExp(0xff, inst->lineno), '&', inst->lineno);
		return exp;
	case addr_mvss:
		if (!wordind)
		{
			// first is op0 7 bit
			exp = newTreeExp(inst->operand, newConstExp(127, inst->lineno), '&', inst->lineno);
			exp->exp.tree.OverflowChkUnsign = 1;
			exp = newTreeExp(newConstExp(0x80, inst->lineno), exp, '|', inst->lineno);
			return exp;
		}
		// second is op0 7 bit, but bi7 7 need 1?
		exp = newTreeExp(inst->operand1, newConstExp(128, inst->lineno), '|', inst->lineno);
		exp->exp.tree.OverflowChkUnsign = 1;
		return exp;


	default:
		fprintf(stderr, "unknown instruction type for high byte expression addrmode:%d\n", inst->addrMode);
	}
	return NULL;

}
static wData* appendMacroMark(MNE *mp, area *current_area, dataItem *paras)
{
	wData *wdp;
	wdp = newWDATA(current_area);
	wdp->wdIsMacroMark = 1;
	wdp->macro_id = mp->m.asm_macro.macro_id;
	wdp->lineno = mp->m.asm_macro.src_lineno;
	wdp->macroPara = paras;
	wdp->macroLevel = mp->m.asm_macro.level;
	wdp->macroSrc = strdup(mp->m.asm_macro.issueLine);
	wdp->locTree = mp->locTree;
	appendTailWDATA(current_area, wdp,0);
	return wdp;
}

static int appendInstruction(MNE *mp, area *current_area)
{
	// all instruction have at least 2 bytes
	wData *wdp1;
	wData *wdp2;
	int specialJmp = 0;
	//expression *exp;

	// 08b check 
	if (is08b && (mp->m.inst.opcode1 & 0x20000))
	{
		char *MNE;
		switch (mp->m.inst.opcode1 & 0xFFFF)
		{
		case 0x5800:
			MNE = "ARLC"; break;
		case 0x5c00:
			MNE = "ARRC"; break;
		case 0x0700:
			MNE = "MULL"; break;
		case 0x0e00:
			MNE = "MULF"; break;
		case 0x0010:
			MNE = "LBSR"; break;
		case 0x000c:
			MNE = "LDPR"; break;
		case 0xd000:
			MNE = "MVFF"; break;
		case 0x0900:
			MNE = "MVLP"; break;
		case 0x7000:
			MNE = "JC"; break;
		case 0x7200:
			MNE = "JN"; break;
		case 0x7400:
			MNE = "JNC"; break;
		case 0x7600:
			MNE = "JNN"; break;
		case 0x7100:
			MNE = "JZ"; break;
		case 0x7300:
			MNE = "JO"; break;
		case 0x7700:
			MNE = "JNO"; break;
		case 0x7500:
			MNE = "JNZ"; break;
		case 0x0006:
			MNE = "TBLR"; break;
		default :
			MNE = " ";
			
		}
		phase2err = 21;
		fprintf(stderr, "Instruction %s Not support for H08B at %s\n", MNE, mp->locTree+1);
		return phase2err;
	}

	wdp1 = newWDATA(current_area);
	wdp2 = newWDATA(current_area);
	wdp1->locTree = mp->locTree;
	wdp2->locTree = mp->locTree;

	// for compatible mode, labe in ROM area shall shift 2 more
	wdp1->instSkip = ((mp->m.inst.opcode1) >> 16)&1;
	mp->m.inst.opcode1 &= 0xffff;
	wdp1->exp = instructionHighByte(&mp->m.inst, 0);
	wdp1->instHighByte = 1;
	
	wdp1->sizeOrORG = 1;
	wdp1->lineno = mp->m.inst.lineno;
	if (mp->m.inst.addrMode == addr_lit && compatibleMode)
		wdp2->expCodeLabelDiv2 = 1; // div2 ==> at link time!!
	wdp2->exp = instructionLowByte(&mp->m.inst, 0);
	wdp2->sizeOrORG = 1;
	wdp2->lineno = mp->m.inst.lineno;
	wdp1->wdIsInstruction = 1;
	wdp2->wdIsInstruction = 1;
	if ((mp->m.inst.opcode1 & 0xFFFF) == 0xc000) // mark on first word is ok
	{
		wdp1->wdIsCall = 1;
		wdp2->wdIsCall = 1;
	}
	// special main call main1
	if ((mp->m.inst.opcode1 & 0xFFFF) == 0xc200 && 
			mp->m.inst.operand->type == ExpIsSymbol &&
		mp->m.inst.operand->exp.symbol[0]=='_' &&
			!isdigit(mp->m.inst.operand->exp.symbol[1]) // label is _00xxxDS
		) // main call main1 use jmp
	{
		wdp1->wdIsCall = 1;
		wdp2->wdIsCall = 1;
		specialJmp = 1;
	}

	if (mp->macroLevel >= 0)
	{
		wdp1->wdIsInMacro = 1;
		wdp1->macroLevel = mp->macroLevel;
		wdp2->macroLevel = mp->macroLevel;
		wdp2->wdIsInMacro = 1;
		wdp1->macroSrc = mp->macroLine; // for showing macro in list file, it is really tricky!!
		wdp2->macroSrc = mp->macroLine;
		wdp2->macro_id = wdp1->macro_id = mp->macroid;
		//wdp2->macro_line_hash = wdp1->macro_line_hash = mp->macroHash;

	}
	if (mp->include_lev)
	{
		wdp2->wdIsInInclude = wdp1->wdIsInInclude = 1;
		wdp2->include_fname = wdp1->include_fname = mp->include_fname;
		wdp2->includeissue_lineno =wdp1->includeissue_lineno = mp->includeissue_lineno;
	}

	appendTailWDATA(current_area, wdp1,1);
	appendTailWDATA(current_area, wdp2,0);


	if (mp->m.inst.wordnum > 1)
	{
		wdp1 = newWDATA(current_area);
		wdp2 = newWDATA(current_area);
		wdp1->locTree = mp->locTree;
		wdp2->locTree = mp->locTree;
		wdp1->exp = instructionHighByte(&mp->m.inst, 2);
		wdp1->instHighByte = 1;
		wdp1->sizeOrORG = 1;
		wdp1->lineno = mp->m.inst.lineno;
		wdp2->exp = instructionLowByte(&mp->m.inst, 2);
		wdp2->sizeOrORG = 1;
		wdp2->lineno = mp->m.inst.lineno;
		wdp1->wdIsInstruction = 1; // only instructoin can it be optimized!!
		wdp2->wdIsInstruction = 1;
		if (mp->macroLevel >= 0)
		{
			wdp1->wdIsInMacro = 1;
			wdp2->wdIsInMacro = 1;
			wdp1->macroLevel = mp->macroLevel;
			wdp2->macroLevel = mp->macroLevel;
			wdp1->macroSrc = mp->macroLine; // for showing macro in list file, it is really tricky!!
			wdp2->macroSrc = mp->macroLine;
			wdp2->macro_id = wdp1->macro_id = mp->macroid;
			//wdp2->macro_line_hash = wdp1->macro_line_hash = mp->macroHash;

		}
		if (mp->include_lev)
		{
			wdp2->wdIsInInclude = wdp1->wdIsInInclude = 1;
			wdp2->include_fname = wdp1->include_fname = mp->include_fname;
			wdp2->includeissue_lineno = wdp1->includeissue_lineno = mp->includeissue_lineno;
		}

		appendTailWDATA(current_area, wdp1,1);
		appendTailWDATA(current_area, wdp2,0);
	}


	return phase2err|(specialJmp<<16);
}

static int appendDBDWItem(int itemSize, dataItem *dp, area *current_area, int lineno, int macroLevel, char *macroLine, int macroid,  int include_lev, char *include_fname, int includeissue_lineno, MNE *mp)
{
	//expression *exp=NULL;
	wData *wdp=NULL;
	int i,j;
	
	switch (dp->exp->type)
	{
	case ExpIsNumber:
	case ExpIsTree:
	case ExpIsSymbol:
		// spread to bytes
		for (i = 0; i < itemSize; i++)
		{
			expression *shiftexp = (i == 0) ? dp->exp :
				newTreeExp(dp->exp, newConstExp(i*8, lineno), 'A', lineno);

			wdp = newWDATA(current_area);
			wdp->locTree = mp->locTree;
			wdp->exp = newTreeExp(shiftexp, newConstExp(0xff,lineno), '&', lineno);
			wdp->sizeOrORG = 1; // split to 1 byte!!
			wdp->lineno = lineno;
			if (macroLevel >= 0)
			{
				wdp->wdIsInMacro = 1;
				wdp->macroSrc = macroLine; // for showing macro in list file, it is really tricky!!
				wdp->macro_id = macroid;
				//wdp->macro_line_hash = macrohash;
				wdp->macroLevel = macroLevel;
			}
			if (include_lev)
			{
				wdp->wdIsInInclude = 1;
				wdp->include_fname = include_fname;
				wdp->includeissue_lineno = includeissue_lineno;
			}
			appendTailWDATA(current_area, wdp,0);
		}
		

		return phase2err;

	case ExpIsASCII: // this is special
		j = strlen(dp->exp->exp.symbol);
		for (i = 0; i < j; i++)
		{
			wdp = newWDATA(current_area);
			wdp->locTree = mp->locTree;
			wdp->sizeOrORG = 1;
			wdp->lineno = lineno;
			wdp->exp = newConstExp(dp->exp->exp.symbol[i], wdp->lineno);
			if (macroLevel >= 0)
			{
				wdp->wdIsInMacro = 1;
				wdp->macroSrc = macroLine; // for showing macro in list file, it is really tricky!!
				wdp->macro_id = macroid;
				//wdp->macro_line_hash = macrohash;
				wdp->macroLevel = macroLevel;
			}
			appendTailWDATA(current_area, wdp,0);
		}
		
		return phase2err;
	case ExpIsPCNow:
	default:
		fprintf(stderr, "unknown type for DB/ASCII data append.\n");
		phase2err = 5;


	}
	return phase2err;
}


int appendDBDS(MNE *mp, area *current_area)
{
	if (mp->m.dbds.hasContent)
	{
		// it is ascii or dw/byte(db)
		dataItem *dp = mp->m.dbds.items;
		while (dp)
		{
			appendDBDWItem(mp->m.dbds.size, dp, current_area, mp->m.dbds.lineno, mp->macroLevel, mp->macroLine,
				mp->macroid,  mp->include_lev, mp->include_fname, mp->includeissue_lineno, mp);

			dp = dp->next;
			if (phase2err)
				return phase2err;
		}

	}
	else if (mp->m.dbds.hasSize)
	{
		wData *wdp = newWDATA(current_area);
		wdp->locTree = mp->locTree;
		wdp->sizeOrORG = mp->m.dbds.size;
		wdp->wdIsBlank = 1;
		wdp->lineno = mp->m.dbds.lineno;
		if (mp->macroLevel >= 0)
		{
			wdp->wdIsInMacro = 1;
			wdp->macroSrc = mp->macroLine;
			wdp->macro_id = mp->macroid;
			//wdp->macro_line_hash = mp->macroHash;
			wdp->macroLevel = mp->macroLevel;

		}
		if (mp->include_lev)
		{
			wdp->wdIsInInclude = 1;
			wdp->include_fname = mp->include_fname;
			wdp->includeissue_lineno = includeissue_lineno;
		}

		appendTailWDATA(current_area, wdp,0);
		if (current_area->offsetAddr >= 0)
			current_area->offsetAddr += mp->m.dbds.size;
	}

	return phase2err;
}

int appendErr(MNE *mp, area *current_area)
{
	wData *wdp = newWDATA(current_area);
	wdp->wdIsErrMark = 1;
	wdp->lineno = mp->m.err.src_lineno;
	wdp->macroSrc = mp->m.err.msg;
	wdp->locTree = mp->locTree;
	appendTailWDATA(current_area, wdp,0);
	return 0;
}
extern int error_count;
int appendErrPhase2(MNE *mp, area *current_area, char *description)
{
	wData *wdp = newWDATA(current_area);
	wdp->wdIsErrMark = 1;
	wdp->lineno = mp->m.err.src_lineno;
	wdp->macroSrc = strdup(description);
	wdp->locTree = mp->locTree;
	appendTailWDATA(current_area, wdp,0);
	phase2err_num++;
	error_count++;
	return 0;
}


int appendEQU(MNE *mp, area *current_area)
{
	symbol *sp;
	if ((sp = getSymByName(mp->m.equ.name)) != NULL)
	{
		phase2err = 4;
		fprintf(stderr, " EQU  %s redefined error \n", mp->m.equ.name);
		return 4;
	}
	sp = newSymbol(mp->m.equ.name);
	sp->symIsEQU = 1;
	sp->ap = current_area;
	sp->exp = mp->m.equ.valueexp;
	sp->EQULineNo = mp->m.equ.lineno;
	appendNewSymbol(current_area, sp); // EQU will not append to WDATA
	return 0;
}
int appendGloLoc(MNE *mp)
{
	dataItem *ip;
	char buffer[1024];
	char buf2[1023];
	gloLocItem *glp;
	for (ip = mp->m.globl.items; ip; ip = ip->next)
	{
		if (ip->exp->type != ExpIsSymbol) // this seems not possible to happen
		{
			phase2err = 5;
			snprintf(buffer,1023, "error globl symbol %s ", exprDump(ip->exp, buf2, 1023));
			fprintf(stderr, "%s",buffer);
			return 4;
			
		}
		glp = newGloLocItem(ip->exp->exp.symbol, mp->m.globl.symIsGlobl);
		appendGloLocToModule(glp);

	}
	return 0;
}



int appendORG(MNE *mp, area *ap)
{

	wData *wdp = newWDATA(ap);
	wdp->locTree = mp->locTree;
	if (!ap->isABS)
	{
		phase2err = 10;
		return phase2err;
	}
	wdp->lineno = mp->m.org.lineno;
	wdp->wdIsOrg = 1;
	wdp->sizeOrORG = mp->m.org.offset;
	appendTailWDATA(ap, wdp,0);
	return phase2err;
}

static int simplifyAllExp(void)
{
	wData *wdp;
	area *ap;
	symbol *sp;
	for (sp = topModule.symbols; sp; sp = sp->next_in_Module)
	{
		if (sp->symIsEQU)
			sp->exp = simplifyExp(sp->exp,", "); // we simplify all EQU first, EQU simplify will have no problem
	}
	// then we simplify the wDATA
	for (ap = topModule.areas; ap; ap = ap->next)
	{
		for (wdp = ap->wDatas; wdp; wdp = wdp->next)
		{
			if (wdp->exp)
				wdp->exp = simplifyExp(wdp->exp,wdp->locTree);
		}
	}
	return phase2err;
}

extern int doDumpMNE;
static void appendParaChkLine(char *info, int s)
{
	paraChkName[paraChkTotal] = strdup(info);
	paraChkSize[paraChkTotal] = s;
	paraChkNameHash[paraChkTotal] = hash(info); // case sensitive!!
	paraChkTotal++;
}

int phase2(void)
{
	MNE *mp=topModule.mneList;

	MNE *modulep = moduleCheck(mp);
	area *areap;
	wData *lastMacroMark=NULL;
	wData *lastFuncRec = NULL;
	int chkSpecialJmp = 0;
	
	int lineno=1;

	// first we get the module name
	if (phase2err)
	{
		showErrPhase2();
		return phase2err;
	}
	if (modulep == NULL)
	{

		if(doDumpMNE)
			fprintf(stderr, "Warning, \"module\" line not found, use filename instead.\n");
		strncpy(topModule.name, input_filename, sizeof(topModule.name)-1);
		getFileBaseName(topModule.name);
	}
	else
	{
		strncpy(topModule.name, modulep->m.module.name, MAXSTRSIZE0);
	}

	// second we construct a default area
	// default is "_CODE", somebody will use ABS CODE

	areap = topModule.areas; // current area


	while (mp != NULL)
	{
		switch (mp->type)
		{
		case mneIsPARACHK:
			lineno = mp->m.paraChk.src_lineno;
			appendParaChkLine(mp->m.paraChk.parachkInfo,mp->m.paraChk.paraChkNum);
			break;

		case mneIsMacro:
			lineno = mp->m.asm_macro.src_lineno;
			lastMacroMark=appendMacroMark(mp, areap, mp->m.asm_macro.paras);
			lastMacroMark->locTree = mp->locTree;
			if (mp->m.asm_macro.macro_end)
				lastMacroMark->wdIsMacroMarkEnd = 1;
			break;

		case mneIsModuleD:
			lineno = mp->m.module.lineno;
			mp = mp->next; continue;
			break;
		case mneIsAreaD:
			lineno = mp->m.area.lineno;
			areap=changeArea(mp);
			break;
		case mneIsLabelMark: // it is a symbol, a link a wdatalist
			if (mp->macroLevel>=0 && lastMacroMark)
				mp->m.label.lineno = lastMacroMark->lineno;

			//else
			lineno = mp->m.label.lineno;
			appendLabel(mp,areap);
			break;
		case mneIsEQU:
			lineno = mp->m.equ.lineno;
			// appendEQU(mp, areap); // append at phase 1
			break;
		case mneIsGlobalMark:
			lineno = mp->m.globl.lineno;
			appendGloLoc(mp);
			break;
		case mneIsInstruction:
			if (mp->macroLevel >= 0 && lastMacroMark!=NULL)
				mp->m.inst.lineno = lastMacroMark->lineno;
			lineno = mp->m.inst.lineno;
			chkSpecialJmp =  appendInstruction(mp, areap);
			if (lastFuncRec!=NULL && (chkSpecialJmp >> 16)!=0) // main call main1
			{
				symbol* sp = lastFuncRec->sym;
				funcRecord *fp = sp->func;
				dataItem *callList = fp->callList;// 
				int found=0;
				if (strcmp(mp->m.inst.operand->exp.symbol, fp->name))
				{
					while(callList)
					{
						if (!strcmp(mp->m.inst.operand->exp.symbol, callList->exp->exp.symbol))
						{
							found = 1;
							break;
						}
						callList = callList->next;
					}
					if (!callList)
					{
						if (!fp->callList)
						{
							fp->callList = newEmptyDataItem();
							fp->callList->exp = mp->m.inst.operand;
						}
						else 
						{

								dataItem * p = fp->callList;
							while (p->next) p = p->next;
							p->next = newEmptyDataItem();
							p->next->exp = mp->m.inst.operand;
							
						}
					}
				}
			}
			
			break;
		case mneIsDBDS:
			if (mp->macroLevel >= 0)
				mp->m.dbds.lineno = lastMacroMark->lineno;
			lineno = mp->m.dbds.lineno;
			appendDBDS(mp, areap);
			break;
		case mneIsORG:
			lineno = mp->m.org.lineno;
			appendORG(mp, areap);
			break;
		case mneIsFuncRec:
			lineno = mp->m.func.lineno;
			appendFunction(mp, areap);
			lastFuncRec = areap->wDataTail; // for main call mail1 inline jmp
			break;
		case mneIsFuncEnd:
			lineno = mp->m.func.lineno;
			appendFunctionEnd(mp, areap);
			break;
		case mneIsErr:
			lineno = mp->m.err.src_lineno;
			appendErr(mp, areap);
			break;
		case mneIsCALLFPTR:
			lineno = mp->m.callFPTR.lineno;
			appendCALLFPTR(mp, areap);
			break;

		default:
			fprintf(stderr, "unknown MNE type %d when parse...\n ",
				mp->type
				);
			phase2err = 6;

		}
		if (phase2err)
		{
			fprintf(stderr, "around line %d..", lineno);
			showErrPhase2();
			//return phase2err;
			appendErrPhase2(mp, areap, phase2err_str[phase2err - 1]);
			phase2err = 0;
		}
		mp = mp->next;
	}

	if(checkEQUexpressionValid())
		return phase2err;
	if (checkWDATAListExpValid())
		return phase2err;
	if (simplifyAllExp())
		return phase2err;
	//if (compatibleMode)
		//div2Job();
	constructExportList(); // mark the global symbols
	return 0;
}


// symbols constructed in both areas and top module!!
symbol *appendNewSymbol(area *ap, symbol *n)
{
	if (ap->symbols == NULL)
	{
		ap->symbols = n;
		ap->symTail = n;
	}
	else
	{
		ap->symTail->next_in_Area = n;
		ap->symTail = n;
	}
	if (topModule.symbols == NULL)
	{
		topModule.symbols = n;
		topModule.symTail = n;
	}
	else
	{
		topModule.symTail->next_in_Module = n;
		topModule.symTail = n;
	}
	return n;
}


gloLocItem *appendGloLocToModule(gloLocItem *glp)
{
	if (topModule.gloHead == NULL)
	{
		topModule.gloHead = glp;
		topModule.gloTail = glp;
	}
	else
	{
		topModule.gloTail->next = glp;
		topModule.gloTail = glp;
	}
	return glp;
}


srcLine *appendSrcLine2TopMod(srcLine *srcp)
{
	if (topModule.srcHead == NULL)
	{
		topModule.srcHead = srcp;
		topModule.srcTail = srcp;
	}
	else
	{
		srcp->prev = topModule.srcTail;
		topModule.srcTail->next = srcp;
		topModule.srcTail = srcp;
	}
	return srcp;
}


static int findParaChkNameByHash(uint64 hashv)
{
	int i;
	for (i = 0; i < paraChkTotal; i++)
		if (paraChkNameHash[i] == hashv)
			return i;
	return -1;
}

int genOBJFileO( char *objFileName, char *srcFileName)
{
	symbol *sp;
	gloLocItem *glop;
	area *areap;
	char buffer[1024];
//	
	int ii;
	asmIncInfo *aifp;
	FILE *fp;
	//fprintf(stderr, "objfilename is %s\n", objFileName);
	if (!strchr(objFileName, '.'))
	{
		strncpy(buffer, objFileName,1023);
		objFileName = &buffer[0];
		strcat(buffer, ".rel"); // default .rel


	}
	objFileName = strdup(objFileName);
	/*fprintf(stderr, "objfilename is %s\n", objFileName);
	for (ii = 0; ii < strlen(objFileName); ii++)
		fprintf(stderr,"0x%02X,",objFileName[ii]);
	fprintf(stderr, "\n");*/
	fp = fopen(objFileName, "w");
#ifndef _WIN32
	char buffer2[PATH_MAX + 1];
	char buffercwd[1024];
#endif

	if (fp == NULL)
	{
		phase2err = 15;
		fprintf(stderr, "file %s open error.\n", objFileName);
		return phase2err;
	}
	//fprintf(stderr, "file %s open OK.\n", objFileName);
	// at first, we dump Module Name
	fprintf(fp, "$MOD:%s:\"%s\"\n", topModule.name, objFileName); // relname assigned
	fprintf(fp, "$VER:%s\n", ASLNKVER);
#ifdef _WIN32
	fprintf(fp, "$ASFROM:\"%s\"\n", _getcwd(NULL, 0));
	char full[_MAX_PATH];
	if (_fullpath(full, srcFileName, _MAX_PATH) != NULL)
		fprintf(fp, "$SRC:\"%s\"\n", full);
	else
		fprintf(fp, "$SRC:\"%s\"\n", srcFileName);
#else
	//fprintf(fp, "$ASFROM:\"%s\"\n", get_current_dir_name());
	fprintf(fp, "$ASFROM:\"%s\"\n", getcwd(buffercwd,1023));
	dummy=realpath(srcFileName, buffer2);
	fprintf(fp, "$SRC:\"%s\"\n", buffer2);
#endif
	// add macro informations ??

	for (ii = 0; ii < defined_macro_num; ii++)
	{
		srcLine *srcp;
		if (!(defined_macros[ii]->used))
			continue;
		if (defined_macros[ii]->isRPT)
			fprintf(fp, "$MACRPT%%%s%%%s%%%d%%",
				defined_macros[ii]->macroName,
				defined_macros[ii]->macroRootLoc,
				defined_macros[ii]->paras->exp->exp.number);
		else
		{
			dataItem *dp = defined_macros[ii]->paras;
			fprintf(fp, "$MACDEF%%%s", defined_macros[ii]->macroName);
			while (dp)
			{
				fprintf(fp, ":%s", dp->exp->exp.symbol);
				dp = dp->next;
			}
			fprintf(fp, "%%");
		}
		
		for (srcp = defined_macros[ii]->lines; srcp; srcp = srcp->next)
		{
			fprintf(fp, "%s%%", srcp->line);
		}
		fprintf(fp, "$ENDMACDEF\n");
	}
	// macro insert records
	for (ii = 0; ii < macro_ins_num; ii++)
	{
		fprintf(fp, "$MACINS%%%s%%%s\n", defined_macros[macroInsMacroID[ii]]->macroName,
			macroInsLocTree[ii]);
	}

	// next stage is exported , use P
	for (sp = topModule.symbols; sp; sp = sp->next_in_Module)
	{
		if (sp->symIsGlobl)
		{
			char *cdbr;
			fprintf(fp, "$EXPORT:%s\n", sp->name);
			if ((cdbr = findCDBSym(sp->name)) != NULL)
				fprintf(fp, "$CDB:\"%s\"\n", cdbr); // it include \n
		}
	}

	// next stage is extref, use R
	for (glop = topModule.gloHead; glop; glop = glop->next)
	{
		if (glop->referenced)
		{
			char *cdbr;
			fprintf(fp, "$REF:%s\n", glop->name);
			if ((cdbr = findCDBSym(glop->name)) != NULL)
			{
				// if it is function, we provide cdbf
				int pcId = findParaChkNameByHash(glop->hashid);
				if (pcId >= 0)
					fprintf(fp, "$CDBF:%d:\"%s\"\n", paraChkSize[pcId], cdbr);
				else
					fprintf(fp, "$CDB:\"%s\"\n", cdbr); // it include \n
			}
		}
	}
	// nextstage is area, and all it wdata
	for (areap = topModule.areas; areap; areap = areap->next)
	{
		wData *wdp;
		// A:name:region:abs:ovl:isstk
		fprintf(fp, "$AREA:%s:%d:%d:%d:0\n", areap->name, areap->region, areap->isABS, areap->isOVL);
		// then we write all the WDATA of that region
		for (wdp = areap->wDatas; wdp; wdp = wdp->next)
		{
			// format is
			// O:lineno:XX for org
			// L:lineno:XX for label
			// 0:lineno:size for blank
			// W:lineno:isInstruction:size:expression
			int xoffset=0; // add new line
			if (wdp->wdIsFunction)
			{
				dataItem *theItem;
				if(wdp->sym->func->paraOnStack)
				{
					if (wdp->sym->func->doCallFPTR)
					{
						fprintf(fp, "$FUNC_PSTK_CFPTR:$R:%d:%d:%s:\"%s\"", wdp->sym->func->returnSize,
							wdp->sym->func->declaredParamSize, wdp->sym->name, wdp->locTree); //! means end of tree
						xoffset += 22 + strlen(wdp->sym->name) + strlen(wdp->locTree);

					}
					else
					{
						fprintf(fp, "$FUNC_PSTK:$R:%d:%d:%s:\"%s\"", wdp->sym->func->returnSize,
							wdp->sym->func->declaredParamSize, wdp->sym->name, wdp->locTree); //! means end of tree
						xoffset += 16 + strlen(wdp->sym->name) + strlen(wdp->locTree);
					}
				}
				else
				{
					if (wdp->sym->func->doCallFPTR)
					{
						fprintf(fp, "$FUNC_CFPTR:$R:%d:%d:%s:\"%s\"", wdp->sym->func->returnSize, 
							wdp->sym->func->declaredParamSize, wdp->sym->name, wdp->locTree);
						xoffset += 13 + strlen(wdp->sym->name) + strlen(wdp->locTree);
					}
					else
					{
						fprintf(fp, "$FUNC:$R:%d:%d:%s:\"%s\"", wdp->sym->func->returnSize, 
							wdp->sym->func->declaredParamSize, wdp->sym->name, wdp->locTree);
						xoffset += 13 + strlen(wdp->sym->name) + strlen(wdp->locTree);
					}
				}
				if (wdp->sym->func->callList)
				{
					for (theItem = wdp->sym->func->callList; theItem; theItem = theItem->next)
					{
						char buf[1024];
						if(xoffset>72)
						{
							fprintf(fp,"%s","\\\n");
							xoffset=0;
						}
						fprintf(fp, ":$C:%s", exprDump( theItem->exp,buf, 1023));
						xoffset += 4+strlen(buf);
						
					}
				}
				if (wdp->sym->func->localList)
				{
					for (theItem = wdp->sym->func->localList; theItem; theItem = theItem->next)
					{
						if(xoffset>72)
						{
							fprintf(fp,"%s","\\\n");
							xoffset=0;
						}
						if(theItem->exp->ExpSymIsX)
						{
							fprintf(fp, ":$XL:%s", theItem->exp->exp.symbol);
							xoffset += 5+strlen(theItem->exp->exp.symbol);
						}
						else
						{
							fprintf(fp, ":$L:%s", theItem->exp->exp.symbol);
							xoffset += 4+strlen(theItem->exp->exp.symbol);
						}
					}
					
				}
				//fprintf(fp, ":$S:%d\n", wdp->sym->func->declaredParamSize);
				fprintf(fp, "\n");
			}
			else if (wdp->wdIsFunctionEnd)
			{
				fprintf(fp, "$ENDFUNC\n");
			}else
			if (wdp->wdIsOrg)
			{
				fprintf(fp, "$ORG:%d:%d:\"%s\"\n", wdp->lineno, wdp->sizeOrORG,wdp->locTree);
			}
			else if (wdp->wdIsLabel)
			{
				if(wdp->labelFixOffset)
					fprintf(fp, "$LAB:%d:%s:0x%X:\"%s\"\n", wdp->lineno, wdp->sym->name,wdp->areaOffset,wdp->locTree);
				else
					fprintf(fp, "$LAB:%d:%s:\"%s\"\n", wdp->lineno, wdp->sym->name,wdp->locTree);
			}
			else if (wdp->wdIsBlank)
			{
				fprintf(fp, "$BLANK:%d:%d:\"%s\"\n", wdp->lineno, wdp->sizeOrORG,wdp->locTree);
			}
			else if (wdp->wdIsMacroMark)
			{
				char paraDump[1024]="__NULL";

				int lefts = 1023;
				char *sp = paraDump;
				int len;
				//paraDump[0] = '\0';
				//dataItem *dip = defined_macros[wdp->macro_id]->paras;
				dataItem *dip = wdp->macroPara;
				dataItem *sdip = (wdp->macro_id>=0)?defined_macros[wdp->macro_id]->paras:NULL;
				while (dip && wdp->macro_id>=0)
				{
					snprintf(sp, lefts, "%s=%s", sdip->exp->exp.symbol, dip->exp->exp.symbol);
					len = strlen(sp);
					sp += len;
					lefts -= len;
					dip = dip->next;
					sdip = sdip->next;
					if (dip)
					{
						*sp = ','; sp++;
						lefts--;
					}
				}
				// id, level, lineno, name
				if (wdp->wdIsMacroMarkEnd)
					fprintf(fp, "$MACROENDM:%d:\"%s\"\n", wdp->lineno,wdp->locTree);
				else if(wdp->macro_id<0 ) // it is repeat
					fprintf(fp, "$MACRORPT:%d:%d:\"%s\"\n", wdp->lineno,wdp->macroLevel,wdp->locTree);
				else
					fprintf(fp, "$MACROM:%d:%d:%d:%s:%s:\"%s\"\n", wdp->macro_id, wdp->macroLevel, 
						wdp->lineno, defined_macros[wdp->macro_id]->macroName, paraDump, wdp->locTree) ;
			}
			else

				// otherwise it should be dbds or instruction
				//W:lineno:isInstruction:size:expression
			{
				fprintf(fp, "$%s:%d:%d:%d:%s", 
					wdp->instSkip?"WDATAS":(wdp->instHighByte?"WDATAH":"WDATA"), wdp->lineno, 
					wdp->wdIsInstruction, wdp->sizeOrORG, exprDump(wdp->exp, buffer, 1024));
				if (wdp->expCodeLabelDiv2)
					fputc('^', fp);

				//if (wdp->wdIsInMacro)
				//{
				//	// dump macro things
				//	fprintf(fp, ":%d:%d:\"%s\":$MLINE$%s", wdp->macro_id, wdp->macroLevel, wdp->macroSrc, wdp->locTree);
				//}
				//else if (wdp->wdIsInInclude)
				//{
				//	char buf[100];
				//	snprintf(buf, 100, "%s:%d:%d", wdp->include_fname, wdp->includeissue_lineno,wdp->lineno);
				//	fprintf(fp, ":%d:%d:0x%llX:\"%s\":$MLINE$%s", -2, -1,(long long unsigned int) hash((char *)buf), wdp->locTree, buf);
				//}
				//else
					fprintf(fp, ":\"%s\"", wdp->locTree);

				fprintf(fp, "\n");

			}
		}

	}
	// finally dump the equ terms
	for (sp = topModule.symbols; sp; sp = sp->next_in_Module)
	{
		if (sp->symIsEQU)
		{
			fprintf(fp, "$EQU:%s:%d:%s\n", sp->name, sp->EQULineNo, exprDump(sp->exp, buffer,1024));
		}
	}
	// at last put a finish record
	// ok, we add asm inc info
	for (aifp = asmIncHead; aifp; aifp = aifp->next)
	{
		fprintf(fp, "$ASMINC:\"%s\":%d:\"%s\"\n", aifp->fname1, aifp->lineno, aifp->fname2);
	}
	fprintf(fp, "$ENDMOD:%s:\"%s\"\n", topModule.name,objFileName); // MODULE FIN
	fclose(fp);

	return 0;
}

int genSymFileO(char *symFName)
{
	FILE *fp = fopen(symFName, "w");
	symbol *sp;
	char buffer[1024];
	int offset = 0;
	for (sp = topModule.symbols; sp; sp = sp->next_in_Module)
	{
		fprintf(fp, "Symbol:%s", sp->name);
		offset = strlen(sp->name);
		while (offset < 16)
		{
			fputc(' ', fp);
			++offset;
		}

		if (sp->symIsGlobl)
			fprintf(fp, ", GLOBL, ");
		else
			fprintf(fp, ", LOCAL, ");

		if (sp->symIsLabel)
			fprintf(fp, " LABEL, area %s, offset ~%d\n", sp->ap->name, sp->labelPos->areaOffset);
		else if (sp->symIsEQU)
			fprintf(fp, " EQU, line %d, %s\n", sp->EQULineNo, exprDump(sp->exp, buffer, 1024));
		else
			fprintf(fp ,"???\n");

	}
	fclose(fp);
	return 0;
}

//wData**  newEmptyWDArr(int wsize)
//{
//	wData **wdp = (wData**)calloc(wsize , sizeof(wData *));
//	//memset(wdp, 0, wsize * sizeof(wData*));
//	return wdp;
//}

//

srcLine * genListFromFile(char *srcFileName, int *flineno)
{
	// read all source, and put them
	//	char buffer[1024];
	//FILE *fi = fopen(srcFileName, "r");
	char fullpath[PATH_MAX + 1];
	srcLine *lstHead = NULL;
	srcLine *lstTail = NULL;
	srcLine *entry;
	srcLine **entryLineMap;
	srcLine *macroHead = NULL;
	srcLine *macroTail = NULL;
	//srcLine *macroSrcNow=NULL;
	//srcLine *macroSrcHeadNow = NULL;
	//uint64 nowMacroHash = 0;
	int i;

	int lineno = 1;
	//int lastLineno ;
	area *ap;
	wData *wdp;
	uint64 fNameHash;// = hash(srcFileName);
	static	int areaOffset; // make this can be reset at every level!!
							//int i;
							//int firstWInv = 0;
							//while (fgets(buffer, 1023, fi) != NULL)
							//{
							//	entry = newEmptyLstE();
							//	entry->src = strdup(removeNewLine(buffer));
							//	entry->srcLineno = lineno++;
							//	appendLstE(&lstHead, &lstTail, entry);
							//}

							//fclose(fi);
#ifdef _WIN32
	_fullpath(fullpath, srcFileName, PATH_MAX);
	fNameHash = hashi(fullpath);
#else
	dummy=realpath(srcFileName, fullpath);
	fNameHash = hash(fullpath);
	//fprintf(stderr,"Warning, path error %s\n", fname);
#endif
	
	readFile2SrcLine2(srcFileName, &lstHead, &lstTail, NULL, NULL, PATH_MAX, asmIncHead, &lineno,"");
	if (flineno != NULL)
		*flineno = lineno;
	//lineno = lstTail->lineno;

	entryLineMap = calloc(lineno, sizeof(srcLine*));
	//macroArrayHead = calloc(lineno, sizeof(srcLine*)); // also macroArray!!
	//macroArrayTail = calloc(lineno, sizeof(srcLine*)); // also macroArray!!
	// insert it by search

	for (entry = lstHead; entry; entry = entry->next)
	{
		if (entry->fnameHash == fNameHash)
			entryLineMap[entry->lineno - 1] = entry;
	}

	// insert macros
	for (i = 0; i < macro_ins_num; i++)
	{
		entry = findSrcLineLocTree(lstHead, macroInsLocTree[i]); // binary search? no need yet
		if (!entry)
			continue;
		insertMacro2SrcLines(entry, defined_macros[macroInsMacroID[i]]);
	}

	

	for (ap = topModule.areas; ap; ap = ap->next)
	{

		areaOffset = 0;
		for (wdp = ap->wDatas; wdp; wdp = wdp->next)
		{

			if (wdp->wdIsMacroMark)
			{
				// we display the name of
				char macrohead[6144+10240];
				dataItem *macroItem, *paraItem;
				if (wdp->wdIsMacroMarkEnd)
				{
					//strncpy(macrohead, "--END-MACRO--", 1024);
					continue;
				}
				else
				{
					if (wdp->macro_id >= 0)
					{
						if (wdp->macroLevel > 0)
						{
							// show the issue line
							strncpy(macrohead, wdp->macroSrc, sizeof(macrohead)-1);
							strncat(macrohead, "\t;.. is MACRO..", sizeof(macrohead)-1);
						}
						else
						{
							macrohead[0] = '\0';
							strncat(macrohead, defined_macros[wdp->macro_id]->macroName, sizeof(macrohead)-1);
							strncat(macrohead, " .. is MACRO..", sizeof(macrohead)-1);
						}
					}
					else
					{
						strncpy(macrohead, "--MACRO repeat lines--", sizeof(macrohead)-1);
					}
				}
				// we show the para replace
				paraItem = wdp->macroPara;
				if (wdp->macro_id >= 0)
				{
					macroItem = defined_macros[wdp->macro_id]->paras;
					while (paraItem)
					{
						char buf[10240];
						if (!macroItem)
							break;
						snprintf(buf, sizeof(buf)-1, "%s = %s", macroItem->exp->exp.symbol,
							paraItem->exp->exp.symbol);
						strncat(macrohead, buf, sizeof(macrohead)-1);
						paraItem = paraItem->next;
						macroItem = macroItem->next;
						if (paraItem != NULL)
							strncat(macrohead, ", ", sizeof(macrohead)-1);
					}
				}

				if (macroHead == NULL)
				{
					//macroArray[wdp->lineno - 1] = newSrcLine(macrohead, wdp->lineno);

					entry = newSrcLine(strdup(macrohead), wdp->lineno);
					//appendLstE(&macroArrayHead[wdp->lineno - 1], &macroArrayTail[wdp->lineno - 1], entry);
					appendSrcLine2(&macroHead, &macroTail, entry);

					//macroSrcNow = macroArrayHead[wdp->lineno - 1];
					//macroSrcHeadNow = macroSrcNow;
					//nowMacroHash = 0; // reset the hash

					if (wdp->ap->region == regionCODE)
						entry->onROM = 1;
					entry->wDataStart = entry->wDataEnd = wdp;

				}
				else
				{
					//macroSrcNow = newSrcLine(macrohead, wdp->lineno);
					entry = newSrcLine(strdup(macrohead), wdp->lineno);
					//appendLstE(&macroArrayHead[wdp->lineno - 1], &macroArrayTail[wdp->lineno - 1], entry);
					appendSrcLine2(&macroHead, &macroTail, entry);
					if (wdp->ap->region == regionCODE)
						entry->onROM = 1;


					entry->wDataStart = entry->wDataEnd = wdp;

				}
				//macroSrcNow->warray = newEmptyWDArr(MAX_MACROLINE_WD);
				if (macroHead && wdp->wdIsMacroMarkEnd)
				{
					srcLine *srcp = findSrcLineLocTree(lstHead, macroHead->wDataStart->locTree);
					srcLine *bk = srcp->next;
					srcp->next = macroHead;
					macroTail->next = bk;
					macroHead = NULL;
					macroTail = NULL;
					
				}
			}
			else if (wdp->wdIsInMacro)
			{
				
				entry = findSrcLineLocTree(lstHead, wdp->locTree); // still use loc tree!!
				if (entry == NULL)
					continue;

				if (entry->wDataStart == NULL)
					entry->wDataStart = entry->wDataEnd = wdp;
				else
					entry->wDataEnd = wdp;
				if (wdp->ap->region == regionCODE)
					entry->onROM = 1;

				
				
			}
			else
			{
				// if there is macro, we insert it 
				srcLine *srcp1;
				if (macroHead)
				{
					srcLine *srcp = findSrcLineLocTree(lstHead, macroHead->wDataStart->locTree);
					srcLine *bk = srcp->next;
					srcp->next = macroHead;
					macroTail->next = bk;
					macroHead = NULL;
					macroTail = NULL;
					//uint64 fnamehash = hash(macroHead->wDataStart->)
					//for(srcp=)
				}
				/*if(wdp->lineno==37)
					fprintf(stderr,"line37");*/
				if (wdp->lineno == 0 || wdp->wdIsInInclude || wdp->lineno >= lineno)
					srcp1 = findSrcLineLocTree(lstHead, wdp->locTree);
				else
					srcp1 = entryLineMap[wdp->lineno - 1];

				if (srcp1 == NULL)
				{
					fprintf(stderr, "error when gen list file.\n");
					exit(-1009);
				}
				if (srcp1->wDataStart == NULL)
				{
					srcp1->wDataStart = srcp1->wDataEnd = wdp;
				}
				else
					srcp1->wDataEnd = wdp;
				if (wdp->ap->region == regionCODE)
					srcp1->onROM = 1;
			}
			wdp->areaOffset = areaOffset;
			if (wdp->wdIsOrg)
				areaOffset = wdp->sizeOrORG;
			else areaOffset += wdp->sizeOrORG;
			//lastLineno = wdp->lineno;

		}
	}
	free(entryLineMap);// leak should not be a problem
	return lstHead;

}
int genListFileO(char* srcFileName, char *listFName, int do_out, int longmode)
{
	int finalLineNo = 0;
	srcLine *list = genListFromFile(srcFileName, &finalLineNo);// level0
	srcLine *entry;
	//int i;
	// now dump the file
	char *format1 = (finalLineNo > 10000) ? "%07d " : (finalLineNo > 1000) ? "%05d " : (finalLineNo > 100) ? "%04d " : "%03d ";
	if (do_out)
	{
		FILE *fo = fopen(listFName, "w");
		if (fo == NULL)
		{
			phase2err = 7;
			return phase2err;
		}
		for (entry = list; entry; entry = entry->next)
		{
			// first column is lineno
			fprintf(fo, format1, entry->lineno);
			// second column is areaOffset
			if (entry->wDataStart) // 
			{
				int offsetnow = 0; // there is 17 ch spaces for content
				int writeCount = 0;
				wData *wdp;
				if (entry->onROM)
				{
					fprintf(fo, "%04X ", entry->wDataStart->areaOffset/2);
					for (wdp = entry->wDataStart; wdp&& wdp != entry->wDataEnd; wdp = wdp->next) // need additional loop
					{
						if (wdp->wdIsLabel)
							continue;
						if (wdp->exp == NULL)
							fprintf(fo, (writeCount & 1) ? "   " : "  ");
						else
							fprintf(fo, (writeCount & 1) ? "%02X " : "%02X", getExpMinValue(wdp->exp) & 0xff);
						writeCount++;
						offsetnow += ((writeCount & 1) ? 3 : 2);
						if (offsetnow >= 17 && wdp->next != NULL) // get a new line
						{
							fprintf(fo, "\n");
							fprintf(fo, format1, entry->lineno);
							fprintf(fo, "%04X ", wdp->next->areaOffset >> 1);
							offsetnow = 0;
						}
					}
					if (wdp && !wdp->wdIsLabel)
					{
						if (wdp->exp != NULL)
							fprintf(fo, (writeCount & 1) ? "%02X " : "%02X", getExpMinValue(wdp->exp) & 0xff);
						else
							fprintf(fo, (writeCount & 1) ? "---" : "--");
						writeCount++;
						offsetnow += ((writeCount & 1) ? 3 : 2);
					}
				}
				else
				{
					fprintf(fo, "%04X ", entry->wDataStart->areaOffset);
					for (wdp = entry->wDataStart; wdp != entry->wDataEnd; wdp = wdp->next) // need additional loop
					{
						if (wdp->wdIsLabel)
							continue;
						if (wdp->exp != NULL)
							fprintf(fo, "%02X ", getExpMinValue(wdp->exp) & 0xff);
						else
							fprintf(fo, "-- ");
						writeCount++;
						offsetnow += 3;
						if (offsetnow >= 17 && wdp->next != NULL) // get a new line
						{
							fprintf(fo, "\n");
							fprintf(fo, format1, entry->line);
							fprintf(fo, "%04X ", wdp->next->areaOffset & 0xffff);
							offsetnow = 0;
						}
					}
					if (wdp && !wdp->wdIsLabel)
					{
						if (wdp->exp != NULL)
							fprintf(fo, "%02X ", getExpMinValue(wdp->exp) & 0xff);
						else
							fprintf(fo, "-- ");
						writeCount++;
						offsetnow += 3;
					}
				}
				while (offsetnow < 17) {
					fputc(' ', fo); offsetnow++;
				} // aligned

			}
			else // leave the space, 22 
			{
				fprintf(fo, "                      ");
			}
			// now print the source
			fprintf(fo, "%s\n", entry->line);

		}
		fclose(fo);
	}
	return 0;
}

symbol *hashInSymlist(uint64 hashid, uint64 excludeID) // char *optname)
{
	symbol *sp;
	for (sp = topModule.symbols; sp; sp = sp->next_in_Module)
	{

		if (sp->hashid == hashid && (sp->hashid != excludeID))
			return sp;
		
	}
	return NULL;
}
gloLocItem *hashinGlobal(uint64 hashid, uint64 excludeID, int needSetRef)
{
	gloLocItem *glop;
	for (glop = topModule.gloHead; glop; glop = glop->next)
	{
		if (glop->hashid == hashid && glop->setGlobl && hashid != excludeID)
		{
			if(needSetRef)
				glop->referenced = 1;
			return glop;
		}
	}
	return NULL;
}

// check the symbols in expression in wDATA list , the reference should be in
// symbols list or global list
// otherwise it is not correct.
int failExpression(expression *exp, uint64 excludeId, expression **failedpart)// prevent self ref self
{
	switch (exp->type)
	{
	case ExpIsASCII:
	case ExpIsPCNow:
	case ExpIsNumber:
		return 0; // it is valid
	case ExpIsSymbol:
		//if (!strcmp(exp->exp.symbol, "_f_chk"))
		//{
		//	fprintf(stderr, "check _f_chk in sym list\n");
		//}
		if (hashInSymlist(exp->symHash, excludeId))// exp->exp.symbol))
			return 0;

		if (hashinGlobal(exp->symHash, excludeId,1))
		{
			exp->ExpSymIsExtern = 1;
			return 0;
		}
		*failedpart = exp; //this
		return 1; // 1 means fail
	case ExpIsTree:
		return failExpression(exp->exp.tree.left,excludeId,failedpart) |
			failExpression(exp->exp.tree.right,excludeId, failedpart);
	case ExpIsStackOffset:
		return 0; // stack offset not check ere
	}
	*failedpart = exp;
	return 1; // invalid expression

}

int checkWDATAListExpValid(void)
{
	area *ap;
	wData *wdp;
	wData *prevW;
	wData *prevW1;
	wData *prevW2;

	expression *failedexp=NULL;
	for (ap = topModule.areas; ap; ap = ap->next)
	{
		prevW = NULL;
		prevW1 = NULL;
		prevW2 = NULL;
		for (wdp = ap->wDatas; wdp; wdp = wdp->next)
		{
			if (wdp->wdIsLabel || wdp->wdIsOrg || wdp->wdIsBlank || wdp->wdIsFunction || wdp->wdIsFunctionEnd || wdp->wdIsMacroMark || wdp->wdIsErrMark)
			{
				prevW2 = prevW1;
				prevW1 = prevW;
				prevW = wdp;
				continue;
			}
			if (!wdp->exp)
			{
				fprintf(stderr, "strange wdata??\n");
				prevW2 = prevW1;
				prevW1 = prevW;
				prevW = wdp;
				continue;
			}
			if (failExpression(wdp->exp, 0,&failedexp) ) // if valid, return 0
			{
				char buffer[1024];
				// special case is static function no use parameter ... we skip if the expression have no use
				
				if (failedexp->type == ExpIsSymbol && strstr(failedexp->exp.symbol, "_STK"))
				{
				
					fprintf(stderr, "Assembler warning , symbol %s is no use around asm line %s. We'll drop it\n", failedexp->exp.symbol, wdp->locTree+1);
					if ((getExpMinValue(wdp->exp) & 0xf0) == 0xf0) // MVFF
					{
						prevW2->next = wdp->next;
						wdp = prevW2;
					}
					else
					{
						prevW->next = wdp->next;
						wdp = prevW;
					}
					
				}
				else if (failedexp->type == ExpIsSymbol && wdp->wdIsCall) // implicit call to extern
				{
					gloLocItem * glp = newGloLocItem(failedexp->exp.symbol, 1);
					appendGloLocToModule(glp);
					fprintf(stderr, "Assembler Warning: call function %s at %s not declared, asssume extern.\n", failedexp->exp.symbol, wdp->locTree + 1);
				}
				else
				{
					phase2err = 12;
					exprDump(failedexp, buffer, 1023);
					fprintf(stderr, "Assembler Error: symbol not found around (Assembly) line %s -- %s\n", wdp->locTree+1, buffer);
					return phase2err;
				}
			}
			prevW2 = prevW1;
			prevW1 = prevW;
			prevW = wdp;
		}
	}
	return 0;
}
int checkEQUexpressionValid(void)
{
	symbol *sp;
	expression *failedpart;
	for (sp = topModule.symbols; sp; sp = sp->next_in_Module)
	{
		if (sp->symIsEQU)
		{
			if(failExpression(sp->exp, sp->hashid, &failedpart))// if valid, return 0
			{
			phase2err = 13;
			fprintf(stderr, "%s EQU error (around line %d)\n", sp->name, sp->EQULineNo);
			return phase2err;
			}
		}

	}
	return 0;
}

// expressions can be simplified if both are const

// 2017 July, if model large, no simplify EQU
expression *simplifyExp(expression *exp, char *locTree)
{
	symbol *sp;
	expression *left;
	expression *right;
	char buf[1024];
	switch (exp->type)
	{
	case ExpIsStackOffset: // can not symplify 
	case ExpIsASCII:
	case ExpIsNumber:
	case ExpIsPCNow:
		return exp;
	case ExpIsSymbol:
		sp = hashInSymlist(exp->symHash, 0);// exp->exp.symbol);
		if (sp && sp->symIsEQU && ramModel!=MODELCLARGE) // CLARGE no shrink!!
			return simplifyExp(sp->exp,locTree);
		return exp; // it cannot be simplified, maybe external or label, which will be defined when link
	case ExpIsTree:
		left = simplifyExp(exp->exp.tree.left,locTree);
		right = simplifyExp(exp->exp.tree.right,locTree);
		if (left->type == ExpIsNumber && right->type == ExpIsNumber)// it can be simplified!!
		{
			// if there is range check first
			int num1 = left->exp.number;
			int num2 = right->exp.number;
			int numNew=-1234567890;
			expression *newexp;
			if (exp->exp.tree.OverflowChk && exp->exp.tree.op == '&')// only for
			{
				if (exp->exp.tree.OverflowChkUnsign)
				{
					if (num1 != 0 && num2 == 1) // special case, left to linker 
						return exp;
					if (num1 & (num2 ^ 0xffff))// out of range?
					{
						// after H08D, we skip to bank
						if (num2 == 1)
						{
							// no simplify
							return exp;
						}
						else
						{
							snprintf(buf, 1023, "Assembler Error!! Data out of range at (assembly) line %s\n", locTree + 1);
							appendErrMsg(buf);
							phase2err = 14;
						}
					}
				}
				else
				{
					int max = num2 >> 1;
					int min = -max - 1;
					if (num1 > max || num1 < min)
					{
						snprintf(buf, 1023, "Assembler Error!!Data out of range at (assembly) line %s\n", locTree+1);
						appendErrMsg(buf);
						phase2err = 14;
					}
				}


			}
			if (numNew != 1)
			{
				switch (exp->exp.tree.op)
				{
				case '+': numNew = num1 + num2; break;
				case '-': numNew = num1 - num2; break;
				case '&': numNew = num1 & num2; break;
				case '|': numNew = num1 | num2; break;
				case 'A': numNew = num1 >> num2; break;
				case 'B': numNew = num1 << num2; break;
				case '=': numNew = (num1 == num2); break;
				case '<': numNew = (num1 < num2); break;
				case '>': numNew = (num1 > num2); break;
				case 'L': numNew = (num1 <= num2); break;
				case 'G': numNew = (num1 >= num2); break;
				case 'C': numNew = num1;
					if ( // check only a==0!!
						(num1 == 0 && num2 >= 0x100))
					{
						snprintf(buf, 1023, "Warning, A-bit -range mismatch at (assembly) line %s\n", locTree + 1);
						appendErrMsg(buf);
					}
					break;
					// warning if num1=1 num2<100 or 
				default:
					fprintf(stderr, "Internal error ..unknown op %c \n", exp->exp.tree.op);
					phase2err = 14;
					numNew = 0;
				}
			}
			newexp = newConstExp(numNew, exp->lineno);
			return newexp;

		}
		exp->exp.tree.left = left;
		exp->exp.tree.right = right;
		return exp;

	}
	return NULL;
}
//int div2Job(void)
//{
//	area *ap;
//	wData *wdp;
//	int count=0;
//	for (ap = topModule.areas; ap; ap = ap->next)
//	{
//		if (ap->region != regionCODE)
//			continue;
//		for (wdp = ap->wDatas; wdp; wdp = wdp->next)
//		{
//			if (wdp->exp)
//				count += div2more(&(wdp->exp));
//		}
//	}
//	return count;
//}


int constructExportList(void)
{
	symbol *symp;
	int n = 0;
	for (symp = topModule.symbols; symp; symp = symp->next_in_Module)
	{
		if (hashinGlobal(symp->hashid, 0,0)) // no need to set "referenced here"
		{
			symp->symIsGlobl = 1;
			n++;
		}
	}
	return n;
}


int dumpMne(void)
{
	MNE *mp = topModule.mneList;
	int i = 0;
	int lineno=1;
	char buffer[MAXSTRSIZE1+100];
	while (mp != NULL)
	{
		switch (mp->type)
		{
		
		case mneIsInstruction:
			lineno = mp->m.inst.lineno;
			sprintf(buffer, "inst op:%04X",
				mp->m.inst.opcode1);
			break;
		case mneIsLabelMark:
			lineno = mp->m.label.lineno;
			sprintf(buffer, "label:%s",
				mp->m.label.name);
			break;
		case mneIsAreaD:
			lineno = mp->m.area.lineno;
			sprintf(buffer, "area :%s",
				mp->m.area.name);
			break;
		case mneIsModuleD:
			lineno = mp->m.module.lineno;
			sprintf(buffer, "module:%s",
				mp->m.module.name);
			break;
		case mneIsGlobalMark:
			lineno = mp->m.globl.lineno;
			sprintf(buffer, "globl mark");
			break;
		case mneIsEQU:
			lineno = mp->m.equ.lineno;
			sprintf(buffer, "equ: %s",
				mp->m.equ.name);
			break;
		case mneIsDBDS:
			lineno = mp->m.dbds.lineno;
			sprintf(buffer, "db or ds");
			break;
		case mneIsFuncRec:
			lineno = mp->m.func.lineno;
			sprintf(buffer, "function %s", mp->m.func.name);
			break;
		case mneIsFuncEnd:
			lineno = mp->m.func.lineno;
			sprintf(buffer, "function-end %s", mp->m.func_end.name);
			break;
		default:
			fprintf(stderr, "known type\n");
			break;
		}
		printf("%04d: %s\n", lineno, buffer);
		mp = mp->next;
		i++;
	}
	return i;
}



area *newArea(char *name, int isABS, int isOVL, regionType region)
{
	char buffer[256];
	//gloLocItem *glop;
	area *newa = malloc(sizeof(area));
	memset(newa, 0, sizeof(area));
	strncpy(newa->name, name, MAXSTRSIZE0-1);
	newa->nameHash = symbolCaseSensitivity?hashi(newa->name):hash(newa->name);
	newa->isABS = isABS;
	newa->isOVL = isOVL;
	//newa->isSTK = isSTK;
	newa->region = region;
	snprintf(buffer, 256, "s_%s", name);
	appendGloLocToModule(newGloLocItem(buffer, 1));
	snprintf(buffer, 256, "l_%s", name);
	appendGloLocToModule(newGloLocItem(buffer, 1));
	snprintf(buffer, 256, "e_%s", name);
	appendGloLocToModule(newGloLocItem(buffer, 1));

	return newa;
}
int expression_has_label(expression *exp)
{
	if (exp->type == ExpIsSymbol)
	{
		return 1;
	}
	if (exp->type == ExpIsTree)
	{
		return expression_has_label(exp->exp.tree.left) |
			expression_has_label(exp->exp.tree.right);
	}
	return 0;
}
extern char *yyfile_stack[];
extern int yylineno_stack[];
extern int yylineno;
extern int opcode_lineno;
extern char *opcode_fname;
static void LocSlashChange(char *str)
{
	while (*str++)
		if (*str == '\\')
			*str = '/';
}
extern int macro_used_line[MAX_MACRO_LEVEL];
extern int opcode_macro_used_line[MAX_MACRO_LEVEL];
char *getLocTree(void)
{
	char buf[PATH_MAX*2+100];
	char buf1[PATH_MAX + 100];
	int i;
	buf[0] = '\0';
	if (macro_level >= 0)
	{
		strncpy(buf, macroRootLoc,sizeof(buf)-1);
		for (i = 0; i <= macro_level; i++)
		{
			if(macroid_stack[i]<0)// REPT, no defined macro
				snprintf(buf1, sizeof(buf1)-1, ";%%RPT_%d%%:%d", macroid_stack[i],
					opcode_macro_used_line[i]);
			else
			snprintf(buf1, sizeof(buf1)-1, ";%s:%d", defined_macros[macroid_stack[i]]->macroName,
				opcode_macro_used_line[i]);
			strncat(buf, buf1, sizeof(buf)-1);
		}
		return strdup(buf);
	}
	for (i = 0; i<include_lev; i++)
	{
		// skip first ./
		if(yyfile_stack[i][0]=='.' &&
		(yyfile_stack[i][1]=='/' || yyfile_stack[i][1]=='\\' ))
			snprintf(buf1, PATH_MAX + 99, ",%s:%d", yyfile_stack[i]+2, yylineno_stack[i]);//,not used yet
		else	
			snprintf(buf1, PATH_MAX + 99, ",%s:%d", yyfile_stack[i], yylineno_stack[i]);//,not used yet
		strncat(buf, buf1, sizeof(buf)-1);
	}
	if (strcmp(opcode_fname, now_lex_fname)&& !strstr(buf,now_lex_fname))
	{
		if(now_lex_fname[0]=='.' && ( now_lex_fname[1]=='/' || now_lex_fname[1]=='\\'))
		snprintf(buf1, PATH_MAX + 99, ",%s:%d", now_lex_fname+2, yylineno-1);// final is opcodelineno!!																		 
		else
		snprintf(buf1, PATH_MAX + 99, ",%s:%d", now_lex_fname, yylineno-1);// final is opcodelineno!!																		 
		strncat(buf, buf1, sizeof(buf)-1);
	}
	if(opcode_fname && opcode_fname[0]=='.' && (opcode_fname[1]=='/' || opcode_fname[1]=='\\'))
	snprintf(buf1, PATH_MAX + 99, ",%s:%d", opcode_fname+2, opcode_lineno);// final is opcodelineno!!
	else
	snprintf(buf1, PATH_MAX + 99, ",%s:%d", opcode_fname, opcode_lineno);// final is opcodelineno!!

	strncat(buf, buf1, sizeof(buf)-1);
	LocSlashChange(buf);
	return strdup(buf);
}
MNE * newEmptyMne(void)
{
	MNE *pmne = (MNE*)calloc(1,sizeof(MNE));
	//memset(pmne, 0, sizeof(MNE));
	pmne->locTree = getLocTree();
	return pmne;
}


char *findCDBSym(char *sym)
{
	int i;
	char rec[1024];
	char *dollar;
	/*if (strstr(sym, "main"))
	{
		fprintf(stderr, "main\n");
	}*/

	for (i = 0; i < cdbRecNum; i++)
	{

		if (!strstr(cdbRecs[i], sym + 1))
			continue;
		strncpy(rec, cdbRecs[i], 1023);
		/*if (strstr(rec, "main"))
		{
			fprintf(stderr, "main at %d\n", i);
		}*/
		//S:G$main$0$0({2}DF,SI:S),C,0,0
		if (rec[0] != 'S' || rec[1]!=':' || rec[2]!='G')
			continue;
		dollar = strchr(rec + 4, '$');
		if (!dollar)
			continue;
		*dollar = '\0';

		if (!strcmp(sym + 1, rec + 4))
			return cdbRecs[i];

	}
	return NULL;
}

