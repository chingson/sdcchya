/***********************************|
| HY11P Series HY08A supported:     |
| Model       program     RAM       |
|-----------------------------------|
| HY11P14       8K        512       |
| HY11P13       4K        256       |
| HY11P24       8K        512       |
| HY11P23       4K        256       |
| HY11P36       4K        256       |
| HY11P35       6K        256       |
| HY11P33       4K        256       |
| HY11P42       2K        128       |
| HY11P41       2K        128       |
| HY11P54       8K        256       |
|***********************************/
#ifdef  USE_HY11P14
#include <hy11p14sfr.h>
#endif

#ifdef  USE_HY11P13
#include <hy11p13sfr.h>
#endif

#ifdef  USE_HY11P24
#include <hy11p24sfr.h>
#endif

#ifdef  USE_HY11P23
#include <hy11p23sfr.h>
#endif

//#ifdef  USE_HY11P22
//#include <hy11p22sfr.h>
//#endif

#ifdef  USE_HY11P36
#include <hy11p36sfr.h>
#endif

#ifdef  USE_HY11P35
#include <hy11p35sfr.h>
#endif

#ifdef  USE_HY11P33
#include <hy11p33sfr.h>
#endif

#ifdef  USE_HY11P42
#include <hy11p42sfr.h>
#endif

#ifdef  USE_HY11P41
#include <hy11p41sfr.h>
#endif

//#ifdef  USE_HY11P4104M
//#include <hy11p4104msfr.h>
//#endif

#ifdef  USE_HY11P54
#include <hy11p54sfr.h>
#endif

//#ifdef  USE_HY11P54B
//#include <hy11p54sfr.h>
//#endif

/***********************************|
| HY12P Series HY08A supported:     |
| Model       program     RAM       |
|-----------------------------------|
| HY12P62       4K        256       |
| HY12P65       6K        256       |
| HY12P66       6K        256       |
|***********************************/
#ifdef  USE_HY12P62
#include <hy12p62sfr.h>
#endif

#ifdef  USE_HY12P65
#include <hy12p65sfr.h>
#endif

#ifdef  USE_HY12P66
#include <hy12p66sfr.h>
#endif

/***********************************|
| HY13P Series HY08A supported:     |
| Model       program     RAM       |
|-----------------------------------|
| HY13P53       4K        256       |
| HY13P53B      4K        256       |
| HY13P56       4K        256       |
| HY13P56B      4K        256       |
|***********************************/
#ifdef  USE_HY13P53_2M
#include <hy13p53sfr.h>
#endif

#ifdef  USE_HY13P53_4M
#include <hy13p53sfr.h>
#endif

#ifdef  USE_HY13P53_8M
#include <hy13p53sfr.h>
#endif

#ifdef  USE_HY13P53B_2M
#include <hy13p53bsfr.h>
#endif

#ifdef  USE_HY13P53B_4M
#include <hy13p53bsfr.h>
#endif

#ifdef  USE_HY13P53B_8M
#include <hy13p53bsfr.h>
#endif

#ifdef  USE_HY13P56_2M
#include <hy13p56sfr.h>
#endif

#ifdef  USE_HY13P56_4M
#include <hy13p56sfr.h>
#endif

#ifdef  USE_HY13P56_8M
#include <hy13p56sfr.h>
#endif

#ifdef  USE_HY13P56B_2M
#include <hy13p56bsfr.h>
#endif

#ifdef  USE_HY13P56B_4M
#include <hy13p56bsfr.h>
#endif

#ifdef  USE_HY13P56B_8M
#include <hy13p56bsfr.h>
#endif

/***********************************|
| HY15P Series HY08C supported:     |
| Model       program     RAM       |
|-----------------------------------|
| HY15P41       2K        128       |
| HY15P43       5K        128       |
| HY15P53       5K        128       |
|***********************************/
#ifdef  USE_HY15P41_2M
#include <hy15p41sfr.h>
#endif

#ifdef  USE_HY15P41_4M
#include <hy15p41sfr.h>
#endif

#ifdef  USE_HY15P41_8M
#include <hy15p41sfr.h>
#endif

#ifdef  USE_HY15P43_2M
#include <hy15p43sfr.h>
#endif

#ifdef  USE_HY15P43_4M
#include <hy15p43sfr.h>
#endif

#ifdef  USE_HY15P53_2M
#include <hy15p53sfr.h>
#endif

#ifdef  USE_HY15P53_4M
#include <hy15p53sfr.h>
#endif

/***********************************|
| HY17P Series HY08D supported:     |
| Model       program     RAM       |
|-----------------------------------|
| HY17P48       8K        512       |
| HY17P52       4K        256       |
| HY17P55       4K        256       |
| HY17P56       8K        512       |
| HY17P58       8K        512       |
| HY17P60       8K        512       |
|***********************************/
#ifdef  USE_HY17P48_2M
#include <hy17p48sfr.h>
#endif

#ifdef  USE_HY17P48_4M
#include <hy17p48sfr.h>
#endif

#ifdef  USE_HY17P48_8M
#include <hy17p48sfr.h>
#endif

#ifdef  USE_HY17P52_2M
#include <hy17p52sfr.h>
#endif

#ifdef  USE_HY17P52_4M
#include <hy17p52sfr.h>
#endif

#ifdef  USE_HY17P52_8M
#include <hy17p52sfr.h>
#endif

#ifdef  USE_HY17P55_2M
#include <hy17p55sfr.h>
#endif

#ifdef  USE_HY17P55_4M
#include <hy17p55sfr.h>
#endif

#ifdef  USE_HY17P55_8M
#include <hy17p55sfr.h>
#endif

#ifdef  USE_HY17P56_2M
#include <hy17p56sfr.h>
#endif

#ifdef  USE_HY17P56_4M
#include <hy17p56sfr.h>
#endif

#ifdef  USE_HY17P56_8M
#include <hy17p56sfr.h>
#endif

#ifdef  USE_HY17P58_2M
#include <hy17p58sfr.h>
#endif

#ifdef  USE_HY17P58_4M
#include <hy17p58sfr.h>
#endif

#ifdef  USE_HY17P58_8M
#include <hy17p58sfr.h>
#endif

#ifdef  USE_HY17P60_5M
#include <hy17p60sfr.h>
#endif

#ifdef  USE_HY17P60_10M
#include <hy17p60sfr.h>
#endif

/***********************************|
| HY17M Series HY08D supported:     |
| Model       program     RAM       |
|-----------------------------------|
| HY17M24       4K        256       |
|***********************************/
#ifdef  USE_HY17M24
#include <hy17M24sfr.h>
#endif
#ifdef  USE_HY17M24_2M
#include <hy17M24sfr.h>
#endif
#ifdef  USE_HY17M24_4M
#include <hy17M24sfr.h>
#endif
#ifdef  USE_HY17M24_8M
#include <hy17M24sfr.h>
#endif
#ifdef  USE_HY17M24_16M
#include <hy17M24sfr.h>
#endif
