#include <sfrcommon.h>
#ifndef __HY4231SFR__
#define __HY4231SFR__
__SFRDEF(_SWTGTID,0x1087)
#define _SWTGTID_ 4231
#define USE_HY4231 1
#include<hy4163sfr.h>
#endif
#ifndef __SDCC
#define _SWTGTID (*((volatile unsigned char *)0x1087))
#endif
