#include <sfrcommon.h>
#ifndef __HY4145SFR__
#define __HY4145SFR__
__SFRDEF(_SWTGTID,0x1031)
#define _SWTGTID_ 4145
#define USE_HY4145 1
#include<hy4163sfr.h>
#endif
#ifndef __SDCC
#define _SWTGTID (*((volatile unsigned char *)0x1031))
#endif
