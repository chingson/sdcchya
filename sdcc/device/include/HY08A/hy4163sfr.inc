	.ifndef __HY4163SFR__
	.define __HY4163SFR__
		__SWTGTID=4163; // this means hy4163
		 _INDF0=0;
		 _POINC0=1;
		 _PODEC0=2;
		 _PRINC0=3;
		 _PLUSW0=4;

		 _INDF1=0x5;
		 _POINC1=0x6;
		 _PODEC1=0x7;
		 _PRINC1=0x8;
		 _PLUSW1=0x9;

		 _FSR0H=0xf;
		 _FSR0L=0x10;
		 _FSR1H=0x11;
		 _FSR1L=0x12;

		 _TOSH=0x16;
		 _TOSL=0x17;

		 _STKPTR=0x18;
		 _PCLATU=0x19; //19
		 _PCLATH=0x1a; //1A
		 _PCLATL=0x1b;

		 _TBLPTRH=0x1d; // 1D
		 _TBLPTRL=0x1e;

		 _TBLDH=0x1f; // 1F
		 _TBLDL=0x20; // 20h

		 _PRODH=0x21; // 21
		 _PRODL=0x22;

		 _INTE1=0x23;
		 _INTE2=0x24;
		 _INTE3=0x25;
		 _INTF1=0x26;
		 _INTF2=0x27;
		 _INTF3=0x28;
		 _WREG=0x29; // 29
		 _BSRCN=0x2a; // bank, 2A
		 _STATUS=0x2b; // 2B
		 _PSTATUS=0x2c; // 2C
		 _LVDCON=0x2d; // 2D
		 _LVDCN=0x2d; // 2D
		 _SBMSET1=0x2e; // 2E
		 _PWRCN=0x30; // 30H , skip 2FH

		 _MCKCN1=0x31;
		 _MCKCN2=0x32;
		 _AD1CN1=0x34;
		 _AD1CN2=0x35;
		 _AD1CN3=0x36;
		 _AD2CN1=0x37;
		 _AD2CN2=0x38;
		 _MCKCN3=0x39;

		_ADJCN1=0x3a;
		_ADJCN2=0x3b;
		_ADJCN3=0x3c;
		_CMPOS=0x3d;

		_TSCTL=0x3E;
		_TSCTL2=0x3F;
		_FSTMACN=0x41
		_TMACN=0x41; // same
		

		 _TMAR=0x42; // 42H

		 _ADCO1L=0x43; // 43H
		 _ADCO1M=0x44; // 44H
		 _ADCO1H=0x45; // 45H

		 _ADCO2L=0x46; // 46H
		 _ADCO2M=0x47; // 47H
		 _ADCO2H=0x48; // 48H

		 _DIV0=0x49; // 49H
		 _DIV1=0x4a; // 4aH
		 _DIV2=0x4b; // 4bH
		 _DIV3=0x4c; // 4cH
		 _DIV4=0x4d; // 4dH
		 _DIV5=0x4e; // 4dH
		 _DIV6=0x4f; // 4dH
		 _DIV7=0x50; // 4dH
		 _DIV8=0x51; // 4dH
		 _DIV9=0x52; // 4dH
		 _DIV10=0x53; // 4dH
		 _DIV11=0x54; // 4dH
		 _DIV12=0x55; // 4dH
		 _DIV13=0x56; // 4dH
		 _DIV14=0x57; // 4dH
		 _DIV15=0x58; // 4dH

		 _PT1=0x6d; // 6dH
		 _TRISC1=0x6e; // 6eH
		 _LEDBUF=0x6f; // 6fH
		 _PT1PU=0x70; // 70H
		 _PT1M1=0x71; // 71H
		 _PT1M2=0x72; // 72H
		 _PT1INT=0x73; // 73H

		 _PT2=0x74; // 74H
		 _TRISC2=0x75; // 75H
		 _PT2DA=0x76; // 76H
		 _PT2PU=0x77; // 77H
		 _PT2M1=0x78; // 78H
		 _PT2M2=0x79; // 79H

		 _PT3=0x7a; // 7aH
		 _TRISC3=0x7b; // 7bH
		 _PT3PU=0x7d; // 7dH, skip 7c


		_FLADI0=0x1C1;
		_FLADI1=0x1C2;
		_FLADI2=0x1C3;
		_FLADI3=0x1C4;
		_FLAARL=0x1C5;
		_FLAARH=0x1C6;
		_FLACN1=0x1c7;
		_FLACN2=0x1c8;
		_FLACN3=0x1c9;
		_SSPC1=0x1ca;
		_SSPC2=0x1cb;
		_SSPBUF=0x1CC;
		_SSPPAR=0x1CD;
		_SSPC3=0x1Ce;
		_WREGSDW=0xfd;
		_BSRSDW=0x1FE;
		_STASDW=0x1ff



	.endif
