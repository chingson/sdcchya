#include <sfrcommon.h>
#ifndef __HY4222SFR__
#define __HY4222SFR__
__SFRDEF(_SWTGTID,0x107E)
#define _SWTGTID_ 4222
#define USE_HY4222 1
#include<hy4163sfr.h>
#endif
#ifndef __SDCC
#define _SWTGTID (*((volatile unsigned char *)0x107E))
#endif
