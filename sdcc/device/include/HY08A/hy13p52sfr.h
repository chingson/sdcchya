#include <sfrcommon.h>
#ifndef __HY13P52__SFR_
#define __HY13P52__SFR_
#define USE_HY13P52 1

__SFRDEF(_SWTGTID,0xB6B63502)
__SFRDEF(INDF0,0x0)
__SFRDEF(POINC0,0x1)
__SFRDEF(PODEC0,0x2)
__SFRDEF(PRINC0,0x3)
__SFRDEF(PLUSW0,0x4)
__SFRDEF(FSR0L,0x10)
__SFRDEF(FSR0H,0x11)
__SFRDEF(STKCN,0x18)
__SFRDEF(PCLATH,0x1A)
__SFRDEF(PCLATL,0x1B)
__SFRDEF(INTE0,0x23)
__SFRDEF(INTE1,0x24)
__SFRDEF(INTF0,0x26)
__SFRDEF(INTF1,0x27)
__SFRDEF(WREG,0x29)
__SFRDEF(MSTAT,0x2B)
__SFRDEF(PSTAT,0x2C)
__SFRDEF(BIECN,0x2E)
__SFRDEF(BIEARH,0x2F)
__SFRDEF(BIEARL,0x30)
__SFRDEF(BIEDRH,0x31)
__SFRDEF(BIEDRL,0x32)
__SFRDEF(PWRCN,0x33)
__SFRDEF(OSCCN0,0x34)
__SFRDEF(OSCCN1,0x35)
__SFRDEF(OSCCN2,0x36)
__SFRDEF(WDTCN,0x37)
__SFRDEF(TMACN,0x38)
__SFRDEF(TMAR,0x39)

__SFRDEF(AD1CN0,0x3A)
__SFRDEF(AD1CN1,0x3B)
__SFRDEF(AD1CN2,0x3C)
__SFRDEF(AD1CN3,0x3D)
__SFRDEF(AD1H,0x3E)
__SFRDEF(AD1M,0x3F)
__SFRDEF(AD1L,0x40)

__SFRDEF(CSFCN0,0x41)
__SFRDEF(CSFCN1,0x42)

__SFRDEF(MCCN0,0x4B)
__SFRDEF(MCCN1,0x4C)
__SFRDEF(MCCN2,0x4D)

__SFRDEF(TB1FLAG,0x4E)
__SFRDEF(TB1CN0,0x4F)
__SFRDEF(TB1CN1,0x50)
__SFRDEF(TB1RH,0x51)
__SFRDEF(TB1RL,0x52)
__SFRDEF(TB1C0H,0x53)
__SFRDEF(TB1C0L,0x54)
__SFRDEF(TB1C1H,0x55)
__SFRDEF(TB1C1L,0x56)
__SFRDEF(TB1C2H,0x57)
__SFRDEF(TB1C2L,0x58)

__SFRDEF(PT1,0x70)
__SFRDEF(TRISC1,0x71)
__SFRDEF(PT1DA,0x72)
__SFRDEF(PT1PU,0x73)
__SFRDEF(PT1EG,0x74)
__SFRDEF(PT2,0x75)
__SFRDEF(TRISC2,0x76)
__SFRDEF(PT2DA,0x77)
__SFRDEF(PT2PU,0x78)
__SFRDEF(PT3,0x79)
__SFRDEF(TRISC3,0x7A)
__SFRDEF(PT3DA,0x7B)
__SFRDEF(PT3PU,0x7C)


__SFRDEF(LCDCN1,0x180)
__SFRDEF(LCDCN2,0x181)
__SFRDEF(LCD0,0x182)
__SFRDEF(LCD1,0x183)
__SFRDEF(LCD2,0x184)
__SFRDEF(LCD3,0x185)
__SFRDEF(LCD4,0x186)
__SFRDEF(LCD5,0x187)


extern volatile __xdata unsigned char *FSR0;
extern volatile __xdata unsigned char *FSR1;
extern volatile __xdata unsigned char *FSR2;
// for adc code compatible
__SFRDEF(ADCRH,0x3E)
__SFRDEF(ADCRM,0x3F)
__SFRDEF(ADCRL,0x40)
extern volatile signed long ADCR;
#endif




































#ifndef __SDCC
#define _SWTGTID (*((volatile unsigned char *)0xB6B63502))
#define INDF0 (*((volatile unsigned char *)0x0))
#define POINC0 (*((volatile unsigned char *)0x1))
#define PODEC0 (*((volatile unsigned char *)0x2))
#define PRINC0 (*((volatile unsigned char *)0x3))
#define PLUSW0 (*((volatile unsigned char *)0x4))
#define FSR0L (*((volatile unsigned char *)0x10))
#define FSR0H (*((volatile unsigned char *)0x11))
#define STKCN (*((volatile unsigned char *)0x18))
#define PCLATH (*((volatile unsigned char *)0x1A))
#define PCLATL (*((volatile unsigned char *)0x1B))
#define INTE0 (*((volatile unsigned char *)0x23))
#define INTE1 (*((volatile unsigned char *)0x24))
#define INTF0 (*((volatile unsigned char *)0x26))
#define INTF1 (*((volatile unsigned char *)0x27))
#define WREG (*((volatile unsigned char *)0x29))
#define MSTAT (*((volatile unsigned char *)0x2B))
#define PSTAT (*((volatile unsigned char *)0x2C))
#define BIECN (*((volatile unsigned char *)0x2E))
#define BIEARH (*((volatile unsigned char *)0x2F))
#define BIEARL (*((volatile unsigned char *)0x30))
#define BIEDRH (*((volatile unsigned char *)0x31))
#define BIEDRL (*((volatile unsigned char *)0x32))
#define PWRCN (*((volatile unsigned char *)0x33))
#define OSCCN0 (*((volatile unsigned char *)0x34))
#define OSCCN1 (*((volatile unsigned char *)0x35))
#define OSCCN2 (*((volatile unsigned char *)0x36))
#define WDTCN (*((volatile unsigned char *)0x37))
#define TMACN (*((volatile unsigned char *)0x38))
#define TMAR (*((volatile unsigned char *)0x39))
#define AD1CN0 (*((volatile unsigned char *)0x3A))
#define AD1CN1 (*((volatile unsigned char *)0x3B))
#define AD1CN2 (*((volatile unsigned char *)0x3C))
#define AD1CN3 (*((volatile unsigned char *)0x3D))
#define AD1H (*((volatile unsigned char *)0x3E))
#define AD1M (*((volatile unsigned char *)0x3F))
#define AD1L (*((volatile unsigned char *)0x40))
#define CSFCN0 (*((volatile unsigned char *)0x41))
#define CSFCN1 (*((volatile unsigned char *)0x42))
#define MCCN0 (*((volatile unsigned char *)0x4B))
#define MCCN1 (*((volatile unsigned char *)0x4C))
#define MCCN2 (*((volatile unsigned char *)0x4D))
#define TB1FLAG (*((volatile unsigned char *)0x4E))
#define TB1CN0 (*((volatile unsigned char *)0x4F))
#define TB1CN1 (*((volatile unsigned char *)0x50))
#define TB1RH (*((volatile unsigned char *)0x51))
#define TB1RL (*((volatile unsigned char *)0x52))
#define TB1C0H (*((volatile unsigned char *)0x53))
#define TB1C0L (*((volatile unsigned char *)0x54))
#define TB1C1H (*((volatile unsigned char *)0x55))
#define TB1C1L (*((volatile unsigned char *)0x56))
#define TB1C2H (*((volatile unsigned char *)0x57))
#define TB1C2L (*((volatile unsigned char *)0x58))
#define PT1 (*((volatile unsigned char *)0x70))
#define TRISC1 (*((volatile unsigned char *)0x71))
#define PT1DA (*((volatile unsigned char *)0x72))
#define PT1PU (*((volatile unsigned char *)0x73))
#define PT1EG (*((volatile unsigned char *)0x74))
#define PT2 (*((volatile unsigned char *)0x75))
#define TRISC2 (*((volatile unsigned char *)0x76))
#define PT2DA (*((volatile unsigned char *)0x77))
#define PT2PU (*((volatile unsigned char *)0x78))
#define PT3 (*((volatile unsigned char *)0x79))
#define TRISC3 (*((volatile unsigned char *)0x7A))
#define PT3DA (*((volatile unsigned char *)0x7B))
#define PT3PU (*((volatile unsigned char *)0x7C))
#define LCDCN1 (*((volatile unsigned char *)0x180))
#define LCDCN2 (*((volatile unsigned char *)0x181))
#define LCD0 (*((volatile unsigned char *)0x182))
#define LCD1 (*((volatile unsigned char *)0x183))
#define LCD2 (*((volatile unsigned char *)0x184))
#define LCD3 (*((volatile unsigned char *)0x185))
#define LCD4 (*((volatile unsigned char *)0x186))
#define LCD5 (*((volatile unsigned char *)0x187))
#endif
