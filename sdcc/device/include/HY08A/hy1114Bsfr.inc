
  .ifndef __HY11S14SFR__
  .define __HY11S14SFR__
    __SWTGTID=11114;
     _INDF0=0x00;
     _POINC0=0x01;
     _PODEC0=0x02;
     _PRINC0=0x03;
     _PLUSW0=0x04;
     _INDF1=0x05;
     _POINC1=0x06;
     _PODEC1=0x07;
     _PRINC1=0x08;
     _PLUSW1=0x09;
     _FSR0H=0x0f;
     _FSR0L=0x10;
     _FSR1H=0x11;
     _FSR1L=0x12;
     _TOSH=0x16;
     _TOSL=0x17;
     _STKPTR=0x18;
     _PCLATH=0x1a;
     _PCLATL=0x1b;
     _TBLPTRH=0x1d;
     _TBLPTRL=0x1e;
     _TBLDH=0x1f;
     _TBLDL=0x20;
     _PRODH=0x21;
     _PRODL=0x22;
     _INTE1=0x23;
     _INTE2=0x24;
     _INTE3=0x25;
     _INTF1=0x26;
     _INTF2=0x27;
     _INTF3=0x28;
     _WREG=0x29;
     _BSRCN=0x2a;
     _STATUS=0x2b;
     _PSTATUS=0x2c;
     _LVDCN=0x2d;
     _SBMSET1=0x2e;
     _PWRCN=0x30;
     _MCKCN1=0x31;
     _MCKCN2=0x32;
     _MCKCN3=0x33;
     _CPACN1=0x34;
     _CPACN2=0x35;
     _CPACN3=0x36;
     _OPCN1=0x37;
     _ADCRH=0x39;
     _ADCRM=0x3a;
     _ADCRL=0x3b;
     _ADCCN1=0x3c;
     _ADCCN2=0x3d;
     _ADCCN3=0x3e;
     _AINET1=0x3f;
     _AINET2=0x40;
     _TMACN=0x41;
     _TMAR=0x42;
     _TMBCN=0x43;
     _TMBRH=0x44;
     _TMBRL=0x45;
     _TMCCN=0x46;
     _PRC=0x47;
     _TMCR=0x48;
     _CCPCN=0x49;
     _CCP0RH=0x4a;
     _CCP0RL=0x4b;
     _CCP1RH=0x4c;
     _CCP1RL=0x4d;
     _PASC=0x4e;
     _PWMCN=0x4f;
     _PDBD=0x50;
     _PWMR=0x51;
     _LCDCN1=0x52;
     _LCDCN2=0x53;
     _LCD0=0x54;
     _LCD1=0x55;
     _LCD2=0x56;
     _LCD3=0x57;
     _LCD4=0x58;
     _LCD5=0x59;
     _LCD6=0x5a;
     _LCD7=0x5b;
     _LCD8=0x5c;
     _LCD9=0x5d;
     _SSPCON1=0x5e;
     _SSPSTA=0x60;
     _SSPBUF=0x61;
     _URCON=0x63;
     _URSTA=0x64;
     _BAUDCON=0x65;
     _BRGRH=0x66;
     _BRGRL=0x67;
     _TXREG=0x68;
     _RCREG=0x69;
     _PT4=0x6a;
     _PT4DA=0x6b;
     _PT4PU=0x6c;
     _PT1=0x6d;
     _TRISC1=0x6e;
     _PT1DA=0x6f;
     _PT1PU=0x70;
     _PT1M1=0x71;
     _PT1M2=0x72;
     _PT1INT=0x73;
     _PT2=0x74;
     _TRISC2=0x75;
     _PT2DA=0x76;
     _PT2PU=0x77;
     _PT2M1=0x78;
     _PT2M2=0x79;
     _PT3=0x7a;
     _TRISC3=0x7b;
     _PT3PU=0x7d;
     _LCD10=0x180;
     _LCD11=0x181;
     _LCD12=0x182;
     _LCD13=0x183;
     _LCD14=0x184;
     _LCD15=0x185;
     _LCD16=0x186;
     _LCD17=0x187;
     _LCD18=0x188;
     _LCD19=0x189;
     _PT5=0x192;
     _PT5DA=0x193;
     _PT5PU=0x194;
     _OPCN2=0x19B;
     _WREGSDW=0x1FD;
     _BSRSDW=0x1FE;
     _STASDW=0x1FF;

  .endif
