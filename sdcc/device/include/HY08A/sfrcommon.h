#ifndef __VSHEAD_H_
#define __VSHEAD_H_
#ifndef __SDCC
// this is a header for VS code
#define __interrupt
#define __critical
#define __data
#define __xdata
#define __code
#define __SFRDEF(a,b) volatile unsigned char a;

#else
#define __SFRDEF(a,b) __sfr __at b a;
#endif

#endif
