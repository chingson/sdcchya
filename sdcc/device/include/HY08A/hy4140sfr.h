#include <sfrcommon.h>
#ifndef __HY4140SFR__
#define __HY4140SFR__
#define USE_HY4140 1
__SFRDEF(_SWTGTID,0x7FA1C18E)
__SFRDEF(INDF0    ,  0x00)   
__SFRDEF(POINC0   ,  0x01)   
__SFRDEF(PODEC0   ,  0x02)   
__SFRDEF(PRINC0   ,  0x03)   
__SFRDEF(PLUSW0   ,  0x04)   
__SFRDEF(INDF1    ,  0x05)   
__SFRDEF(POINC1   ,  0x06)   
__SFRDEF(PODEC1   ,  0x07)   
__SFRDEF(PRINC1   ,  0x08)   
__SFRDEF(PLUSW1   ,  0x09)   
__SFRDEF(INDF2    ,  0x0A)   
__SFRDEF(POINC2   ,  0x0B)   
__SFRDEF(PODEC2   ,  0x0C)   
__SFRDEF(PRINC2   ,  0x0D)   
__SFRDEF(PLUSW2   ,  0x0E)   
__SFRDEF(FSR0H    ,  0x0F)   
__SFRDEF(FSR0L    ,  0x10)   
__SFRDEF(FSR1H    ,  0x11)   
__SFRDEF(FSR1L    ,  0x12)   
__SFRDEF(FSR2H    ,  0x13)   
__SFRDEF(FSR2L    ,  0x14)   
__SFRDEF(TOSH     ,  0x16)   
__SFRDEF(TOSL     ,  0x17)   
__SFRDEF(SKCN     ,  0x18)   
__SFRDEF(PCLATH   ,  0x1A)   
__SFRDEF(PCLATL   ,  0x1B)   
//__SFRDEF(TBLPTRH  ,  0x1D)   
//__SFRDEF(TBLPTRL  ,  0x1E)   
__SFRDEF(TBLDH    ,  0x1F)   
__SFRDEF(TBLDL    ,  0x20)   
__SFRDEF(PRODH    ,  0x21)   
__SFRDEF(PRODL    ,  0x22)   
__SFRDEF(INTE0    ,  0x23)   
__SFRDEF(INTE1    ,  0x24)   
//__SFRDEF(INTE2    ,  0x25)   
__SFRDEF(INTF0    ,  0x26)   
__SFRDEF(INTF1    ,  0x27)   
//__SFRDEF(INTF2    ,  0x28)   
__SFRDEF(WREG     ,  0x29)   
__SFRDEF(BSRCN    ,  0x2A)   
__SFRDEF(STATUS   ,  0x2B)   
__SFRDEF(MSTAT    ,  0x2B)   
__SFRDEF(PSTAT    ,  0x2C)   
//__SFRDEF(INTE3    ,  0x2E)   
//__SFRDEF(INTF3    ,  0x2F)   
__SFRDEF(PWRCN    ,  0x30)   
__SFRDEF(OSCCN0   ,  0x31)   
__SFRDEF(OSCCN1   ,  0x32)   
__SFRDEF(OSCCN2   ,  0x33)   
//__SFRDEF(OSCCN3   ,  0x34)   
__SFRDEF(CSFCN0   ,  0x35)   
//__SFRDEF(CSFCN1   ,  0x36)   
__SFRDEF(CSFCN2   ,  0x37)   
__SFRDEF(WDTCN    ,  0x38)   
__SFRDEF(AD1H     ,  0x39)   
__SFRDEF(AD1M     ,  0x3A)   
__SFRDEF(AD1L     ,  0x3B)   
//__SFRDEF(AD1HH    ,  0x3C)   
//__SFRDEF(AD1MM    ,  0x3D)   
//__SFRDEF(AD1LL    ,  0x3E)   
__SFRDEF(AD1CN0   ,  0x3F)   
__SFRDEF(AD1CN1   ,  0x40)   
__SFRDEF(AD1CN2   ,  0x41)   
__SFRDEF(AD1CN3   ,  0x42)   
__SFRDEF(AD1CN4   ,  0x43)   
__SFRDEF(AD1CN5   ,  0x44)   
__SFRDEF(LVDCN    ,  0x45)   
//__SFRDEF(MCCN0    ,  0x46)   
//__SFRDEF(MCCN1    ,  0x47)   
//__SFRDEF(MCCN2    ,  0x48)   
//__SFRDEF(MCCN3    ,  0x49)   
//__SFRDEF(Filter   ,  0x4A)   
__SFRDEF(TMA1CN   ,  0x4B)   
__SFRDEF(TMA1R    ,  0x4C)   
__SFRDEF(TMA1C    ,  0x4D)   
//__SFRDEF(TMA2CN   ,  0x4E)   
//__SFRDEF(TMA2R    ,  0x4F)   
//__SFRDEF(TMA2C    ,  0x50)   
__SFRDEF(TB1FLAG  ,  0x51)   
__SFRDEF(TB1Flag  ,  0x51)   
__SFRDEF(TB1CN0   ,  0x52)   
__SFRDEF(TB1CN1   ,  0x53)   
__SFRDEF(TB1RH    ,  0x54)   
__SFRDEF(TB1RL    ,  0x55)   
__SFRDEF(TB1C0H   ,  0x56)   
__SFRDEF(TB1C0L   ,  0x57)   
__SFRDEF(TB1C1H   ,  0x58)   
__SFRDEF(TB1C1L   ,  0x59)   
__SFRDEF(TB1C2H   ,  0x5A)   
__SFRDEF(TB1C2L   ,  0x5B)   
__SFRDEF(TC1CN0   ,  0x5C)   
//__SFRDEF(TC1CN1   ,  0x5D)   
//__SFRDEF(TC1R0H   ,  0x5E)   
//__SFRDEF(TC1R0L   ,  0x5F)   
//__SFRDEF(TC1R1H   ,  0x60)   
//__SFRDEF(TC1R1L   ,  0x61)   
//__SFRDEF(TB2Flag  ,  0x62)   
//__SFRDEF(TB2CN0   ,  0x63)   
//__SFRDEF(TB2CN1   ,  0x64)   
//__SFRDEF(TB2RH    ,  0x65)   
//__SFRDEF(TB2RL    ,  0x66)   
//__SFRDEF(TB2C0H   ,  0x67)   
//__SFRDEF(TB2C0L   ,  0x68)   
//__SFRDEF(TB2C1H   ,  0x69)   
//__SFRDEF(TB2C1L   ,  0x6A)   
//__SFRDEF(TB2C2H   ,  0x6B)   
//__SFRDEF(TB2C2L   ,  0x6C)   
//__SFRDEF(TC2CN0   ,  0x6D)   
//__SFRDEF(TC2CN1   ,  0x6E)   
//__SFRDEF(TC2R0H   ,  0x6F)   
//__SFRDEF(TC2R0L   ,  0x70)   
//__SFRDEF(TC2R1H   ,  0x71)   
//__SFRDEF(TC2R1L   ,  0x72)   
__SFRDEF(UR0CN    ,  0x73)   
__SFRDEF(UR0STA   ,  0x74)   
__SFRDEF(BA0CN    ,  0x75)   
//__SFRDEF(BG0RH    ,  0x76)   
__SFRDEF(BG0RL    ,  0x77)   
__SFRDEF(TX0R     ,  0x78)   
__SFRDEF(RC0REG   ,  0x79)   
__SFRDEF(BIECN    ,  0x180)  
__SFRDEF(BIEARH   ,  0x181)  
__SFRDEF(BIEARL   ,  0x182)  
__SFRDEF(BIEDRH   ,  0x183)  
__SFRDEF(BIEDRL   ,  0x184)  
__SFRDEF(BIEKEY   ,  0x185)  
//__SFRDEF(BIE2CN   ,  0x186)  
//__SFRDEF(BIE2ARH  ,  0x187)  
//__SFRDEF(BIE2ARL  ,  0x188)  
//__SFRDEF(BIE2DRH  ,  0x189)  
//__SFRDEF(BIE2DRL  ,  0x18A)  
//__SFRDEF(EECR1    ,  0x18B)  
//__SFRDEF(EECR2    ,  0x18C)  
//__SFRDEF(EECOUNTER1 , 0x18D) 
//__SFRDEF(EECounter1 , 0x18D) 
//__SFRDEF(EECOUNTER2 , 0x18E) 
//__SFRDEF(EECounter2 , 0x18E) 
//__SFRDEF(EEPPointer , 0x18F) 
//__SFRDEF(EEPPOINTER , 0x18F) 
__SFRDEF(PT1      ,  0x190)  
__SFRDEF(PT1IN    ,  0x191)  
__SFRDEF(TRISC1   ,  0x192)  
__SFRDEF(PT1DA    ,  0x193)  
__SFRDEF(PT1PU    ,  0x194)  
__SFRDEF(PT1M1    ,  0x195)  
__SFRDEF(PT1INT   ,  0x196)  
__SFRDEF(PT1INTE  ,  0x197)  
__SFRDEF(PT1INTF  ,  0x198)  
__SFRDEF(PT2      ,  0x199)  
__SFRDEF(PT2IN    ,  0x19A)  
__SFRDEF(TRISC2   ,  0x19B)  
__SFRDEF(PT2DA    ,  0x19C)  
__SFRDEF(PT2PU    ,  0x19D)  
__SFRDEF(PT2INT   ,  0x19E)  
__SFRDEF(PT2INTE  ,  0x19F)  
__SFRDEF(PT2INTF  ,  0x1A0)  
__SFRDEF(PT3      ,  0x1A1)  
__SFRDEF(PT3IN    ,  0x1A2)  
__SFRDEF(TRISC3   ,  0x1A3)  
__SFRDEF(PT3DA    ,  0x1A4)  
__SFRDEF(PT3PU    ,  0x1A5)  
__SFRDEF(PT3INT   ,  0x1A6)  
__SFRDEF(PT3INTE  ,  0x1A7)  
__SFRDEF(PT3INTF  ,  0x1A8)  
__SFRDEF(GPort0   ,  0x1A9)  
__SFRDEF(GPort1   ,  0x1AA)  
__SFRDEF(GPort2   ,  0x1AB)  
__SFRDEF(GPort3   ,  0x1AC)  
__SFRDEF(GPort4   ,  0x1AD)  
__SFRDEF(GPort5   ,  0x1AE)  
__SFRDEF(CFG0     ,  0x1AF)  
__SFRDEF(ACT0     ,  0x1B0)  
__SFRDEF(STA0     ,  0x1B1)  
__SFRDEF(CRG0     ,  0x1B2)  
__SFRDEF(TOC0     ,  0x1B3)  
__SFRDEF(RDB0     ,  0x1B4)  
__SFRDEF(TDB0     ,  0x1B5)  
__SFRDEF(SID0     ,  0x1B6)  
//__SFRDEF(CFG2     ,  0x1B7)  
//__SFRDEF(ACT2     ,  0x1B8)  
//__SFRDEF(STA2     ,  0x1B9)  
//__SFRDEF(CRG2     ,  0x1BA)  
//__SFRDEF(TOC2     ,  0x1BB)  
//__SFRDEF(RDB2     ,  0x1BC)  
//__SFRDEF(TDB2     ,  0x1BD)  
//__SFRDEF(SID2     ,  0x1BE)  
//__SFRDEF(SSPCN0   ,  0x1BF)  
//__SFRDEF(SSPSTA0  ,  0x1C0)  
//__SFRDEF(SSPBUF0  ,  0x1C1)  
//__SFRDEF(UR2CN    ,  0x1C2)  
//__SFRDEF(UR2STA   ,  0x1C3)  
//__SFRDEF(BA2CN    ,  0x1C4)  
//__SFRDEF(BG2RH    ,  0x1C5)  
//__SFRDEF(BG2RL    ,  0x1C6)  
//__SFRDEF(TX2R     ,  0x1C7)  
//__SFRDEF(RC2REG   ,  0x1C8)  
__SFRDEF(FWRST    ,  0x1C9)  
//__SFRDEF(TRIMCN0    , 0X1CA) 
//__SFRDEF(TRIMCNTH   , 0X1CB) 
//__SFRDEF(TRIMCNTL   , 0X1CC) 
//__SFRDEF(TRIMCNTVH  , 0X1CD) 
//__SFRDEF(TRIMCNTVL  , 0X1CE) 
//__SFRDEF(TRIMVAL    , 0X1CF) 
//__SFRDEF(TRIMSRC    , 0X1D0) 
__SFRDEF(IOPDR    	, 0x1D1) 
__SFRDEF(IOPUU    	, 0x1D2) 
__SFRDEF(ADCRH,0x39)
__SFRDEF(ADCRM,0x3A)
__SFRDEF(ADCRL,0x3B)
extern volatile signed long ADCR;
#define __xdata __data
#endif
#ifndef __SDCC
#define _SWTGTID (*((volatile unsigned char *)0xB084C8FD))
#define INDF0     (*((volatile unsigned char *)  0x00))     
#define POINC0    (*((volatile unsigned char *)  0x01))     
#define PODEC0    (*((volatile unsigned char *)  0x02))     
#define PRINC0    (*((volatile unsigned char *)  0x03))     
#define PLUSW0    (*((volatile unsigned char *)  0x04))     
#define INDF1     (*((volatile unsigned char *)  0x05))     
#define POINC1    (*((volatile unsigned char *)  0x06))     
#define PODEC1    (*((volatile unsigned char *)  0x07))     
#define PRINC1    (*((volatile unsigned char *)  0x08))     
#define PLUSW1    (*((volatile unsigned char *)  0x09))     
#define INDF2     (*((volatile unsigned char *)  0x0A))     
#define POINC2    (*((volatile unsigned char *)  0x0B))     
#define PODEC2    (*((volatile unsigned char *)  0x0C))     
#define PRINC2    (*((volatile unsigned char *)  0x0D))     
#define PLUSW2    (*((volatile unsigned char *)  0x0E))     
#define FSR0H     (*((volatile unsigned char *)  0x0F))     
#define FSR0L     (*((volatile unsigned char *)  0x10))     
#define FSR1H     (*((volatile unsigned char *)  0x11))     
#define FSR1L     (*((volatile unsigned char *)  0x12))     
#define FSR2H     (*((volatile unsigned char *)  0x13))     
#define FSR2L     (*((volatile unsigned char *)  0x14))     
#define TOSH      (*((volatile unsigned char *)  0x16))     
#define TOSL      (*((volatile unsigned char *)  0x17))     
#define SKCN      (*((volatile unsigned char *)  0x18))     
#define PCLATH    (*((volatile unsigned char *)  0x1A))     
#define PCLATL    (*((volatile unsigned char *)  0x1B))     
//#define TBLPTRH   (*((volatile unsigned char *)  0x1D))     
//#define TBLPTRL   (*((volatile unsigned char *)  0x1E))     
#define TBLDH     (*((volatile unsigned char *)  0x1F))     
#define TBLDL     (*((volatile unsigned char *)  0x20))     
#define PRODH     (*((volatile unsigned char *)  0x21))     
#define PRODL     (*((volatile unsigned char *)  0x22))     
#define INTE0     (*((volatile unsigned char *)  0x23))     
#define INTE1     (*((volatile unsigned char *)  0x24))     
//#define INTE2     (*((volatile unsigned char *)  0x25))     
#define INTF0     (*((volatile unsigned char *)  0x26))     
#define INTF1     (*((volatile unsigned char *)  0x27))     
//#define INTF2     (*((volatile unsigned char *)  0x28))     
#define WREG      (*((volatile unsigned char *)  0x29))     
#define BSRCN     (*((volatile unsigned char *)  0x2A))     
#define STATUS    (*((volatile unsigned char *)  0x2B))     
#define MSTAT     (*((volatile unsigned char *)  0x2B))     
#define PSTAT     (*((volatile unsigned char *)  0x2C))     
//#define INTE3     (*((volatile unsigned char *)  0x2E))     
//#define INTF3     (*((volatile unsigned char *)  0x2F))     
#define PWRCN     (*((volatile unsigned char *)  0x30))     
#define OSCCN0    (*((volatile unsigned char *)  0x31))     
#define OSCCN1    (*((volatile unsigned char *)  0x32))     
#define OSCCN2    (*((volatile unsigned char *)  0x33))     
//#define OSCCN3    (*((volatile unsigned char *)  0x34))     
#define CSFCN0    (*((volatile unsigned char *)  0x35))     
//#define CSFCN1    (*((volatile unsigned char *)  0x36))     
#define CSFCN2    (*((volatile unsigned char *)  0x37))     
#define WDTCN     (*((volatile unsigned char *)  0x38))     
#define AD1H      (*((volatile unsigned char *)  0x39))     
#define AD1M      (*((volatile unsigned char *)  0x3A))     
#define AD1L      (*((volatile unsigned char *)  0x3B))     
//#define AD1HH     (*((volatile unsigned char *)  0x3C))     
//#define AD1MM     (*((volatile unsigned char *)  0x3D))     
//#define AD1LL     (*((volatile unsigned char *)  0x3E))     
#define AD1CN0    (*((volatile unsigned char *)  0x3F))     
#define AD1CN1    (*((volatile unsigned char *)  0x40))     
#define AD1CN2    (*((volatile unsigned char *)  0x41))     
#define AD1CN3    (*((volatile unsigned char *)  0x42))     
#define AD1CN4    (*((volatile unsigned char *)  0x43))     
#define AD1CN5    (*((volatile unsigned char *)  0x44))     
#define LVDCN     (*((volatile unsigned char *)  0x45))     
//#define MCCN0     (*((volatile unsigned char *)  0x46))     
//#define MCCN1     (*((volatile unsigned char *)  0x47))     
//#define MCCN2     (*((volatile unsigned char *)  0x48))     
//#define MCCN3     (*((volatile unsigned char *)  0x49))     
//#define Filter    (*((volatile unsigned char *)  0x4A))     
#define TMA1CN    (*((volatile unsigned char *)  0x4B))     
#define TMA1R     (*((volatile unsigned char *)  0x4C))     
#define TMA1C     (*((volatile unsigned char *)  0x4D))     
//#define TMA2CN    (*((volatile unsigned char *)  0x4E))     
//#define TMA2R     (*((volatile unsigned char *)  0x4F))     
//#define TMA2C     (*((volatile unsigned char *)  0x50))     
#define TB1FLAG   (*((volatile unsigned char *)  0x51))     
#define TB1Flag   (*((volatile unsigned char *)  0x51))     
#define TB1CN0    (*((volatile unsigned char *)  0x52))     
#define TB1CN1    (*((volatile unsigned char *)  0x53))     
#define TB1RH     (*((volatile unsigned char *)  0x54))     
#define TB1RL     (*((volatile unsigned char *)  0x55))     
#define TB1C0H    (*((volatile unsigned char *)  0x56))     
#define TB1C0L    (*((volatile unsigned char *)  0x57))     
#define TB1C1H    (*((volatile unsigned char *)  0x58))     
#define TB1C1L    (*((volatile unsigned char *)  0x59))     
#define TB1C2H    (*((volatile unsigned char *)  0x5A))     
#define TB1C2L    (*((volatile unsigned char *)  0x5B))     
#define TC1CN0    (*((volatile unsigned char *)  0x5C))     
//#define TC1CN1    (*((volatile unsigned char *)  0x5D))     
//#define TC1R0H    (*((volatile unsigned char *)  0x5E))     
//#define TC1R0L    (*((volatile unsigned char *)  0x5F))     
//#define TC1R1H    (*((volatile unsigned char *)  0x60))     
//#define TC1R1L    (*((volatile unsigned char *)  0x61))     
//#define TB2Flag   (*((volatile unsigned char *)  0x62))     
//#define TB2CN0    (*((volatile unsigned char *)  0x63))     
//#define TB2CN1    (*((volatile unsigned char *)  0x64))     
//#define TB2RH     (*((volatile unsigned char *)  0x65))     
//#define TB2RL     (*((volatile unsigned char *)  0x66))     
//#define TB2C0H    (*((volatile unsigned char *)  0x67))     
//#define TB2C0L    (*((volatile unsigned char *)  0x68))     
//#define TB2C1H    (*((volatile unsigned char *)  0x69))     
//#define TB2C1L    (*((volatile unsigned char *)  0x6A))     
//#define TB2C2H    (*((volatile unsigned char *)  0x6B))     
//#define TB2C2L    (*((volatile unsigned char *)  0x6C))     
//#define TC2CN0    (*((volatile unsigned char *)  0x6D))     
//#define TC2CN1    (*((volatile unsigned char *)  0x6E))     
//#define TC2R0H    (*((volatile unsigned char *)  0x6F))     
//#define TC2R0L    (*((volatile unsigned char *)  0x70))     
//#define TC2R1H    (*((volatile unsigned char *)  0x71))     
//#define TC2R1L    (*((volatile unsigned char *)  0x72))     
#define UR0CN     (*((volatile unsigned char *)  0x73))     
#define UR0STA    (*((volatile unsigned char *)  0x74))     
#define BA0CN     (*((volatile unsigned char *)  0x75))     
//#define BG0RH     (*((volatile unsigned char *)  0x76))     
#define BG0RL     (*((volatile unsigned char *)  0x77))     
#define TX0R      (*((volatile unsigned char *)  0x78))     
#define RC0REG    (*((volatile unsigned char *)  0x79))     
#define BIECN     (*((volatile unsigned char *)  0x180))    
#define BIEARH    (*((volatile unsigned char *)  0x181))    
#define BIEARL    (*((volatile unsigned char *)  0x182))    
#define BIEDRH    (*((volatile unsigned char *)  0x183))    
#define BIEDRL    (*((volatile unsigned char *)  0x184))    
#define BIEKEY    (*((volatile unsigned char *)  0x185))    
//#define BIE2CN    (*((volatile unsigned char *)  0x186))    
//#define BIE2ARH   (*((volatile unsigned char *)  0x187))    
//#define BIE2ARL   (*((volatile unsigned char *)  0x188))    
//#define BIE2DRH   (*((volatile unsigned char *)  0x189))    
//#define BIE2DRL   (*((volatile unsigned char *)  0x18A))    
//#define EECR1     (*((volatile unsigned char *)  0x18B))    
//#define EECR2     (*((volatile unsigned char *)  0x18C))    
//#define EECOUNTER1  (*((volatile unsigned char *) 0x18D))   
//#define EECounter1  (*((volatile unsigned char *) 0x18D))   
//#define EECOUNTER2  (*((volatile unsigned char *) 0x18E))   
//#define EECounter2  (*((volatile unsigned char *) 0x18E))   
//#define EEPPointer  (*((volatile unsigned char *) 0x18F))   
//#define EEPPOINTER  (*((volatile unsigned char *) 0x18F))   
//#define PT1       (*((volatile unsigned char *)  0x190))    
//#define PT1IN     (*((volatile unsigned char *)  0x191))    
//#define TRISC1    (*((volatile unsigned char *)  0x192))    
//#define PT1DA     (*((volatile unsigned char *)  0x193))    
//#define PT1PU     (*((volatile unsigned char *)  0x194))    
//#define PT1M1     (*((volatile unsigned char *)  0x195))    
//#define PT1INT    (*((volatile unsigned char *)  0x196))    
//#define PT1INTE   (*((volatile unsigned char *)  0x197))    
//#define PT1INTF   (*((volatile unsigned char *)  0x198))    
#define PT2       (*((volatile unsigned char *)  0x199))    
#define PT2IN     (*((volatile unsigned char *)  0x19A))    
#define TRISC2    (*((volatile unsigned char *)  0x19B))    
#define PT2DA     (*((volatile unsigned char *)  0x19C))    
#define PT2PU     (*((volatile unsigned char *)  0x19D))    
#define PT2INT    (*((volatile unsigned char *)  0x19E))    
#define PT2INTE   (*((volatile unsigned char *)  0x19F))    
#define PT2INTF   (*((volatile unsigned char *)  0x1A0))    
#define PT3       (*((volatile unsigned char *)  0x1A1))    
#define PT3IN     (*((volatile unsigned char *)  0x1A2))    
#define TRISC3    (*((volatile unsigned char *)  0x1A3))    
#define PT3DA     (*((volatile unsigned char *)  0x1A4))    
#define PT3PU     (*((volatile unsigned char *)  0x1A5))    
#define PT3INT    (*((volatile unsigned char *)  0x1A6))    
#define PT3INTE   (*((volatile unsigned char *)  0x1A7))    
#define PT3INTF   (*((volatile unsigned char *)  0x1A8))    
#define GPort0    (*((volatile unsigned char *)  0x1A9))    
#define GPort1    (*((volatile unsigned char *)  0x1AA))    
#define GPort2    (*((volatile unsigned char *)  0x1AB))    
#define GPort3    (*((volatile unsigned char *)  0x1AC))    
#define GPort4    (*((volatile unsigned char *)  0x1AD))    
#define GPort5    (*((volatile unsigned char *)  0x1AE))    
#define CFG0      (*((volatile unsigned char *)  0x1AF))    
#define ACT0      (*((volatile unsigned char *)  0x1B0))    
#define STA0      (*((volatile unsigned char *)  0x1B1))    
#define CRG0      (*((volatile unsigned char *)  0x1B2))    
#define TOC0      (*((volatile unsigned char *)  0x1B3))    
#define RDB0      (*((volatile unsigned char *)  0x1B4))    
#define TDB0      (*((volatile unsigned char *)  0x1B5))    
#define SID0      (*((volatile unsigned char *)  0x1B6))    
//#define CFG2      (*((volatile unsigned char *)  0x1B7))    
//#define ACT2      (*((volatile unsigned char *)  0x1B8))    
//#define STA2      (*((volatile unsigned char *)  0x1B9))    
//#define CRG2      (*((volatile unsigned char *)  0x1BA))    
//#define TOC2      (*((volatile unsigned char *)  0x1BB))    
//#define RDB2      (*((volatile unsigned char *)  0x1BC))    
//#define TDB2      (*((volatile unsigned char *)  0x1BD))    
//#define SID2      (*((volatile unsigned char *)  0x1BE))    
//#define SSPCN0    (*((volatile unsigned char *)  0x1BF))    
//#define SSPSTA0   (*((volatile unsigned char *)  0x1C0))    
//#define SSPBUF0   (*((volatile unsigned char *)  0x1C1))    
//#define UR2CN     (*((volatile unsigned char *)  0x1C2))    
//#define UR2STA    (*((volatile unsigned char *)  0x1C3))    
//#define BA2CN     (*((volatile unsigned char *)  0x1C4))    
//#define BG2RH     (*((volatile unsigned char *)  0x1C5))    
//#define BG2RL     (*((volatile unsigned char *)  0x1C6))    
//#define TX2R      (*((volatile unsigned char *)  0x1C7))    
//#define RC2REG    (*((volatile unsigned char *)  0x1C8))    
#define FWRST     (*((volatile unsigned char *)  0x1C9))    
//#define TRIMCN0     (*((volatile unsigned char *) 0X1CA))   
//#define TRIMCNTH    (*((volatile unsigned char *) 0X1CB))   
//#define TRIMCNTL    (*((volatile unsigned char *) 0X1CC))   
//#define TRIMCNTVH   (*((volatile unsigned char *) 0X1CD))   
//#define TRIMCNTVL   (*((volatile unsigned char *) 0X1CE))   
//#define TRIMVAL     (*((volatile unsigned char *) 0X1CF))   
//#define TRIMSRC     (*((volatile unsigned char *) 0X1D0))   
#define IOPDR     (*((volatile unsigned char *) 0X1D1))   
#define IOPDU     (*((volatile unsigned char *) 0X1D2))   
extern volatile signed long ADCR;
#endif
extern volatile __xdata unsigned char *FSR0;
extern volatile __xdata unsigned char *FSR1;
extern volatile __xdata unsigned char *FSR2;
#define ADCRH (*((volatile unsigned char *)0x39))
#define ADCRM (*((volatile unsigned char *)0x3A))
#define ADCRL (*((volatile unsigned char *)0x3B))
