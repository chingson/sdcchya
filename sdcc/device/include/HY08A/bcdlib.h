#ifndef __BCDLIB_H__
#define __BCDLIB_H__
// this is the header file for BCD operation, include byte2BCD short2BCD bin242BCD, long2BCD, byteBCDinc, shortBCDinc, bin24BCDinc longBCDinc

// we provide INC and sum, there is no
// sub/mul/div/mod
// if a number need to do sub/mul/div/mod, please use intrinsic types

#ifndef __SDCC
#ifndef __xdata
#define __xdata
#endif
#endif

unsigned char _byte2BCD(unsigned char);
unsigned long _short2BCD(unsigned int);
unsigned long _byte3_2BCD(unsigned long);
unsigned long long _long2BCD(unsigned long);// 4,294,967,295. Need 5 bytes
unsigned long long _longlong2BCD(unsigned long long);// maximum 99999999 99999999

// note that following functions available for HY17P56 or later products

void _byteBCDinc(__xdata unsigned char *); // use inc instead of Inc because some fonts confused I,1,l
void _shortBCDinc(__xdata unsigned short *);
void _longBCDinc(__xdata unsigned long *);
void _longlongBCDinc(__xdata unsigned long long*);// maximum 99999999 99999999

unsigned char _byteBCDSum(unsigned char,unsigned char);
unsigned short _shortBCDSum(unsigned short, unsigned short);
unsigned long _longBCDSum(unsigned long, unsigned long);
unsigned long long _longlongBCDSum(unsigned long long, unsigned long long);

unsigned char _byteBCDDiff(unsigned char, unsigned char);
unsigned short _shortBCDDiff(unsigned short, unsigned short);
unsigned long _longBCDDiff(unsigned long, unsigned long);
unsigned long long _longlongBCDDiff(unsigned long long, unsigned long long);

unsigned char _BCD2byte(unsigned char);
unsigned short _BCD2short(unsigned short);
unsigned long _BCD2long(unsigned long);
unsigned long long _BCD2longlong(unsigned long long);

#endif