.IFNDEF __HY17M41SFR__
.DEFINE __HY17M41SFR__
.DEFINE USE_HY17M41 1
__SWTGTID=0xA0EA1056;
_INDF0    =  0x00;   
_POINC0   =  0x01;   
_PODEC0   =  0x02;   
_PRINC0   =  0x03;   
_PLUSW0   =  0x04;   
_INDF1    =  0x05;   
_POINC1   =  0x06;   
_PODEC1   =  0x07;   
_PRINC1   =  0x08;   
_PLUSW1   =  0x09;   
_INDF2    =  0x0A;   
_POINC2   =  0x0B;   
_PODEC2   =  0x0C;   
_PRINC2   =  0x0D;   
_PLUSW2   =  0x0E;   
_FSR0H    =  0x0F;   
_FSR0L    =  0x10;   
_FSR1H    =  0x11;   
_FSR1L    =  0x12;   
_FSR2H    =  0x13;   
_FSR2L    =  0x14;   
_TOSH     =  0x16;   
_TOSL     =  0x17;   
_SKCN     =  0x18;   
_PCLATH   =  0x1A;   
_PCLATL   =  0x1B;   
_TBLPTRH  =  0x1D;   
_TBLPTRL  =  0x1E;   
_TBLDH    =  0x1F;   
_TBLDL    =  0x20;   
_PRODH    =  0x21;   
_PRODL    =  0x22;   
_INTE0    =  0x23;   
_INTE1    =  0x24;   
_INTF0    =  0x26;   
_INTF1    =  0x27;   
_WREG     =  0x29;   
_BSRCN    =  0x2A;   
_MSTAT    =  0x2B;   
_PSTAT    =  0x2C;   
_PWRCN    =  0x30;   
_OSCCN0   =  0x31;   
_OSCCN1   =  0x32;   
_OSCCN2   =  0x33;   
_CSFCN0   =  0x35;   
_CSFCN2   =  0x37;   
_WDTCN    =  0x38;   
_AD1H     =  0x39;   
_AD1M     =  0x3A;   
_AD1L     =  0x3B;   
_AD1CN0   =  0x3F;   
_AD1CN1   =  0x40;   
_AD1CN2   =  0x41;   
_AD1CN3   =  0x42;   
_AD1CN4   =  0x43;   
_AD1CN5   =  0x44;   
_LVDCN    =  0x45;  
_TMA1CN   =  0x4B;   
_TMA1R    =  0x4C;   
_TMA1C    =  0x4D;   
_TB1Flag  =  0x51;   
_TB1CN0   =  0x52;   
_TB1CN1   =  0x53;   
_TB1RH    =  0x54;   
_TB1RL    =  0x55;   
_TB1C0H   =  0x56;   
_TB1C0L   =  0x57;   
_TB1C1H   =  0x58;   
_TB1C1L   =  0x59;   
_TB1C2H   =  0x5A;   
_TB1C2L   =  0x5B;   
_TC1CN0   =  0x5C;   
_UR0CN    =  0x73;   
_UR0STA   =  0x74;   
_BA0CN    =  0x75;   
_BG0RH    =  0x76;   
_BG0RL    =  0x77;   
_TX0R     =  0x78;   
_RC0REG   =  0x79;   
_BIECN    =  0x180;  
_BIEARH   =  0x181;  
_BIEARL   =  0x182;  
_BIEDRH   =  0x183;  
_BIEDRL   =  0x184;  
_BIEKEY   =  0x185;  
_PT2      =  0x199;  
_PT2IN    =  0x19A;  
_TRISC2   =  0x19B;  
_PT2DA    =  0x19C;  
_PT2PU    =  0x19D;  
_PT2INT   =  0x19E;  
_PT2INTE  =  0x19F;  
_PT2INTF  =  0x1A0;  
_PT3      =  0x1A1;  
_PT3IN    =  0x1A2;  
_TRISC3   =  0x1A3;  
_PT3DA    =  0x1A4;  
_PT3PU    =  0x1A5;  
_PT3INT   =  0x1A6;  
_PT3INTE  =  0x1A7;  
_PT3INTF  =  0x1A8;  
_GPort0   =  0x1A9;  
_GPort2   =  0x1AB;  
_GPort4   =  0x1AD;  
_GPort5   =  0x1AE;  
_CFG0     =  0x1AF;  
_ACT0     =  0x1B0;  
_STA0     =  0x1B1;  
_CRG0     =  0x1B2;  
_TOC0     =  0x1B3;  
_RDB0     =  0x1B4;  
_TDB0     =  0x1B5;  
_SID0     =  0x1B6;  
_FWRST    =  0x1C9;  
_IOPDR    = 0X1D1; 
_IOPUU    = 0X1D2; 

_ADRH=0x39;
_ADRM=0x3A;
_ADRL=0x3B;
.ENDIF
