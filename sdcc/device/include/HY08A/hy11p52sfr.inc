
	.ifndef __HY11P52SFR__
	.define __HY11P52SFR__
		__SWTGTID=11052; // this means 11012
		 _INDF0=0;
		 _FSR0L=0x10;
		 _STKPTR=0x18;
		 _PCLATH=0x1a; //1A
		 _PCLATL=0x1b;
     _BIECTRL=0x1C  
     _BIEPTRH=0x1D     
		 _BIEPTRL=0x1E      
		 _BIEDH=0x1f; // 1F 		                    
		 _BIEDL=0x20; // 20h
		 _INTE1=0x23;
		 _INTF1=0x26;
		 _WREG=0x29; // 29
		 _STATUS=0x2b; // 2B
		 _PSTATUS=0x2c; // 2C
		 _LVDCON=0x2d; // 2D
		 _LVDCN=0x2d; // 2D
		 _PWRCN=0x30; // 30H , skip 2FH
		 _MCKCN1=0x31;
		 _MCKCN2=0x32;
		 _MCKCN3=0x33;
		 _ADCRH=0x39; // 39h, skip 38
		 _ADCRM=0x3a;
		 _ADCRL=0x3b;
		 _ADCCN1=0x3c;
		 _ADCCN2=0x3d;
		 _ADCCN3=0x3e;
		 _AINET1=0x3f; // 3FH
		 _AINET2=0x40; // 40H
		 _TMACN=0x41; // 41H
		 _TMAR=0x42; // 42H
		 _LCDCN1=0x52; // 52H
		 _LCDCN2=0x53; // 53H
		 _LCD0=0x54; // 54H
		 _LCD1=0x55; // 55H
		 _LCD2=0x56; // 56H
		 _LCD3=0x57; // 57H
		 _LCD4=0x58; // 58H
		 _LCD5=0x59; // 59H
		 _PT1=0x6d; // 6dH
		 _TRISC1=0x6e; // 6eH
		 _PT1PU=0x70; // 70H
		 _PT1M1=0x71; // 71H
		 _PT1M2=0x72; // 72H
		 _PT2=0x74; // 74H
		 _TRISC2=0x75; // 75H
		 _PT2PU=0x77; // 77H

	.endif
