.IFNDEF __HY17P51SFR__
.DEFINE __HY17P51SFR__
.DEFINE USE_HY17P51 1

__SWTGTID = 0x489E7BC0;
_INDF0      =  0x00 ;
_POINC0     =  0x01 ;
_PODEC0     =  0x02 ;
_PRINC0     =  0x03 ;
_PLUSW0     =  0x04 ;
_INDF1      =  0x05 ;
_POINC1     =  0x06 ;
_PODEC1     =  0x07 ;
_PRINC1     =  0x08 ;
_PLUSW1     =  0x09 ;
_INDF2      =  0x0A ;
_POINC2     =  0x0B ;
_PODEC2     =  0x0C ;
_PRINC2     =  0x0D ;
_PLUSW2     =  0x0E ;
_FSR0H      =  0x0F ;
_FSR0L      =  0x10 ;
_FSR1H      =  0x11 ;
_FSR1L      =  0x12 ;
_FSR2H      =  0x13 ;
_FSR2L      =  0x14 ;
_TOSH       =  0x16 ;
_TOSL       =  0x17 ;
_SKCN       =  0x18 ;
_PCLATH     =  0x1A ;
_PCLATL     =  0x1B ;
_TBLPTRH    =  0x1D ;
_TBLPTRL    =  0x1E ;
_TBLDH      =  0x1F ;
_TBLDL      =  0x20 ;
_PRODH      =  0x21 ;
_PRODL      =  0x22 ;
_INTE0      =  0x23 ;
_INTE1      =  0x24 ;
_INTE2      =  0x25 ;
_INTF0      =  0x26 ;
_INTF1      =  0x27 ;
_INTF2      =  0x28 ;
_WREG       =  0x29 ;
_BSRCN      =  0x2A ;
_STATUS     =  0x2B ;
_MSTAT      =  0x2B ;
_PSTAT      =  0x2C ;
_BIECN      =  0x2E ;
_BIEARH     =  0x2F ;
_BIEARL     =  0x30 ;
_BIEDRH     =  0x31 ;
_BIEDRL     =  0x32 ;
_PWRCN      =  0x33 ;
_OSCCN0     =  0x34 ;
_OSCCN1     =  0x35 ;
_OSCCN2     =  0x36 ;
_CSFCN0     =  0x37 ;
_CSFCN1     =  0x38 ;
_WDTCN      =  0x39 ;
_AD1H       =  0x3A ;
_AD1M       =  0x3B ;
_AD1L       =  0x3C ;
_AD1CN0     =  0x3D ;
_AD1CN1     =  0x3E ;
_AD1CN2     =  0x3F ;
_AD1CN3     =  0x40 ;
_AD1CN4     =  0x41 ;
_AD1CN5     =  0x42 ;
_LVDCN      =  0x43 ;
_TMA1CN     =  0x44 ;
_TMA1R      =  0x45 ;
_TMA1C      =  0x46 ;
_PT1        =  0x47 ;
_TRISC1     =  0x48 ;
_PT1DA      =  0x49 ;
_PT1PU      =  0x4A ;
_PT1M1      =  0x4B ;
_PT8        =  0x4C ;
_TRISC8     =  0x4D ;
_PT8DA      =  0x4E ;
_PT8PU      =  0x4F ;
_UR0CN      =  0x50 ;
_UR0STA     =  0x51 ;
_BA0CN      =  0x52 ;
_BG0RH      =  0x53 ;
_BG0RL      =  0x54 ;
_TX0R       =  0x55 ;
_RC0REG     =  0x56 ;
_LCDCN1     =  0x57 ;
_LCDCN2     =  0x58 ;
_LCDCN3     =  0x59 ;
_LCDCN4     =  0x5A ;
_LCD0       =  0x5B ;
_LCD1       =  0x5C ;
_LCD2       =  0x5D ;
_LCD3       =  0x5E ;
_LCD4       =  0x5F ;
_LCD5       =  0x60 ;
_LCD6       =  0x61 ;
_FILTER     =  0x62 ;
_AD1HH      =  0x63 ;
_AD1MM      =  0x64 ;
_AD1LL      =  0x65 ;

_ADRH=0x3A;
_ADRM=0x3B;
_ADRL=0x3C;
.ENDIF
