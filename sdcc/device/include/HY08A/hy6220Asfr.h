#include <sfrcommon.h>
#ifndef __HY6220ASFR__
#define __HY6220ASFR__
#define USE_HY6220A 1
__SFRDEF(_SWTGTID,0x34C59E8C)
__SFRDEF(INDF0,0x0)
__SFRDEF(POINC0,0x1)
__SFRDEF(PODEC0,0x2)
__SFRDEF(PRINC0,0x3)
__SFRDEF(PLUSW0,0x4)
__SFRDEF(INDF1,0x5)
__SFRDEF(POINC1,0x6)
__SFRDEF(PODEC1,0x7)
__SFRDEF(PRINC1,0x8)
__SFRDEF(PLUSW1,0x9)
__SFRDEF(INDF2,0xA)
__SFRDEF(POINC2,0xB)
__SFRDEF(PODEC2,0xC)
__SFRDEF(PRINC2,0xD)
__SFRDEF(PLUSW2,0xE)
__SFRDEF(FSR0H,0xF)
__SFRDEF(FSR0L,0x10)
__SFRDEF(FSR1H,0x11)
__SFRDEF(FSR1L,0x12)
__SFRDEF(FSR2H,0x13)
__SFRDEF(FSR2L,0x14)
__SFRDEF(TOSH,0x16)
__SFRDEF(TOSL,0x17)
__SFRDEF(SKCN,0x18)
__SFRDEF(PCLATH,0x1A)
__SFRDEF(PCLATL,0x1B)
__SFRDEF(TBLPTRH,0x1D)
__SFRDEF(TBLPTRL,0x1E)
__SFRDEF(TBLDH,0x1F)
__SFRDEF(TBLDL,0x20)
__SFRDEF(PRODH,0x21)
__SFRDEF(PRODL,0x22)
__SFRDEF(INTE0,0x23)
__SFRDEF(INTE1,0x24)
__SFRDEF(INTE2,0x25)
__SFRDEF(INTF0,0x26)
__SFRDEF(INTF1,0x27)
__SFRDEF(INTF2,0x28)
__SFRDEF(WREG,0x29)
__SFRDEF(BSRCN,0x2A)
__SFRDEF(STATUS,0x2B)
__SFRDEF(MSTAT,0x2B)
__SFRDEF(PSTAT,0x2C)

__SFRDEF(PWRCN,0x2E)
__SFRDEF(OSCCN0,0x2F)
__SFRDEF(OSCCN1,0x30)
__SFRDEF(OSCCN2,0x31)
__SFRDEF(CSFCN0,0x32)
__SFRDEF(CSFCN1,0x33)
__SFRDEF(WDTCN,0x34)
__SFRDEF(AD1H,0x35)
__SFRDEF(AD1M,0x36)
__SFRDEF(AD1L,0x37)
__SFRDEF(AD1CN0,0x38)
__SFRDEF(AD1CN1,0x39)
__SFRDEF(AD1CN2,0x3A)
__SFRDEF(AD1CN3,0x3B)
__SFRDEF(AD1CN4,0x3C)
__SFRDEF(AD1CN5,0x3D)
__SFRDEF(LVDCN,0x3E)
__SFRDEF(DACCN0,0x3F)
__SFRDEF(DACCN1,0x40)
__SFRDEF(DACBitH, 0x41)
__SFRDEF(DACBITH, 0x41)
__SFRDEF(DACBitL, 0x42)
__SFRDEF(DACBITL, 0x42)
__SFRDEF(OP1CN0,0x43)
__SFRDEF(OP1CN1,0x44)
__SFRDEF(OP1INP,0x45)
__SFRDEF(OP1INN1,0x46)
__SFRDEF(OP1INN0,0x47)

__SFRDEF(TMA1CN,0x48)
__SFRDEF(TMA1R,0x49)
__SFRDEF(TMA1C,0x4A)
__SFRDEF(TB1FLAG,0x4B)
__SFRDEF(TB1Flag,0x4B)
__SFRDEF(TB1CN0,0x4C)
__SFRDEF(TB1CN1,0x4D)
__SFRDEF(TB1RH,0x4E)
__SFRDEF(TB1RL,0x4F)
__SFRDEF(TB1C0H,0x50)
__SFRDEF(TB1C0L,0x51)
__SFRDEF(TB1C1H,0x52)
__SFRDEF(TB1C1L,0x53)
__SFRDEF(TB1C2H,0x54)
__SFRDEF(TB1C2L,0x55)
__SFRDEF(TC1CN0,0x56)

__SFRDEF(PT1,0x57)
__SFRDEF(PT1IN,0x58)
__SFRDEF(TRISC1,0x59)
__SFRDEF(PT1DA,0x5A)
__SFRDEF(PT1PU,0x5B)
__SFRDEF(PT1M1,0x5C)
__SFRDEF(PT1M2,0x5D)
__SFRDEF(PT1M3,0x5E)
__SFRDEF(PT1INT,0x5F)
__SFRDEF(PT1INTE,0x60)
__SFRDEF(PT1INTF,0x61)   
__SFRDEF(PT2,0x62)
__SFRDEF(PT2IN,0x63)
__SFRDEF(TRISC2,0x64)
__SFRDEF(PT2PU,0x65)
__SFRDEF(PT2M1,0x66)
__SFRDEF(PT2INT,0x67)
__SFRDEF(PT2INTE,0x68)
__SFRDEF(PT2INTF,0x69)
__SFRDEF(PT3,0x6A)
__SFRDEF(PT3IN,0x6B)
__SFRDEF(TRISC3,0x6C)
__SFRDEF(PT3DA,0x6D)
__SFRDEF(PT3PU,0x6E)
__SFRDEF(PT3M1,0x6F)
__SFRDEF(PT3M2,0x70)
__SFRDEF(PT3INT,0x71)
__SFRDEF(PT3INTE,0x72)
__SFRDEF(PT3INTF,0x73)

__SFRDEF(UR0CN,0x74)
__SFRDEF(UR0STA,0x75)
__SFRDEF(BA0CN,0x76)
__SFRDEF(BG0RH,0x77)
__SFRDEF(BG0RL,0x78)
__SFRDEF(TX0R,0x79)
__SFRDEF(RC0REG,0x7A)
__SFRDEF(MCCN0,0x7B)
__SFRDEF(MCCN1,0x7C)
__SFRDEF(MCCN2,0x7D)
__SFRDEF(MCCN3,0x7E)
__SFRDEF(FILTER,0x7F)

__SFRDEF(CFG0,0x180)
__SFRDEF(ACT0,0x181)
__SFRDEF(STA0,0x182)
__SFRDEF(CRG0,0x183)
__SFRDEF(TOC0,0x184)
__SFRDEF(RDB0,0x185)
__SFRDEF(TDB0,0x186)
__SFRDEF(SID0,0x187)
__SFRDEF(BIECN,0x188)
__SFRDEF(BIEARH,0x189)
__SFRDEF(BIEARL,0x18A)
__SFRDEF(BIEDRH,0x18B)
__SFRDEF(BIEDRL,0x18C)
__SFRDEF(BIE2CN,0x18D)
__SFRDEF(BIE2ARH,0x18E)
__SFRDEF(BIE2ARL,0x18F)
__SFRDEF(BIE2DRH,0x190)
__SFRDEF(BIE2DRL,0x191)
__SFRDEF(EECR1,0x192)
__SFRDEF(EECR2,0x193)
__SFRDEF(EECOUNTER1,0x194)
__SFRDEF(EECounter1,0x194)
__SFRDEF(EECOUNTER2,0x195)
__SFRDEF(EECounter2,0x195)
__SFRDEF(EEPPointer,0x196)
__SFRDEF(EEPPOINTER,0x196)
__SFRDEF(EERD0,0x197)
__SFRDEF(EERD1,0x198)
__SFRDEF(EERD2,0x199)
__SFRDEF(EERD3,0x19A)
__SFRDEF(EERD4,0x19B)
__SFRDEF(EERD5,0x19C)
__SFRDEF(EERD6,0x19D)
__SFRDEF(EERD7,0x19E)
__SFRDEF(EERD8,0x19F)
__SFRDEF(EERD9,0x1A0)
__SFRDEF(EERD10,0x1A1)
__SFRDEF(EERD11,0x1A2)
__SFRDEF(EERD12,0x1A3)
__SFRDEF(EERD13,0x1A4)
__SFRDEF(EERD14,0x1A5)
__SFRDEF(EERD15,0x1A6)
__SFRDEF(EERD16,0x1A7)
__SFRDEF(EERD17,0x1A8)
__SFRDEF(EERD18,0x1A9)
__SFRDEF(EERD19,0x1AA)
__SFRDEF(EERD20,0x1AB)
__SFRDEF(EERD21,0x1AC)
__SFRDEF(EERD22,0x1AD)
__SFRDEF(EERD23,0x1AE)
__SFRDEF(EERD24,0x1AF)
__SFRDEF(EERD25,0x1B0)
__SFRDEF(EERD26,0x1B1)
__SFRDEF(EERD27,0x1B2)
__SFRDEF(EERD28,0x1B3)
__SFRDEF(EERD29,0x1B4)
__SFRDEF(EERD30,0x1B5)
__SFRDEF(EERD31,0x1B6)

extern volatile __xdata unsigned char *FSR0;
extern volatile __xdata unsigned char *FSR1;
extern volatile __xdata unsigned char *FSR2;
__SFRDEF(ADCRH,0x35)
__SFRDEF(ADCRM,0x36)
__SFRDEF(ADCRL,0x37)
extern volatile signed long ADCR;
#define __xdata __data
#endif
#ifndef __SDCC
#define _SWTGTID (*((volatile unsigned char *)0xD3109A37))
#define INDF0 (*((volatile unsigned char *)0x0))
#define POINC0 (*((volatile unsigned char *)0x1))
#define PODEC0 (*((volatile unsigned char *)0x2))
#define PRINC0 (*((volatile unsigned char *)0x3))
#define PLUSW0 (*((volatile unsigned char *)0x4))
#define INDF1 (*((volatile unsigned char *)0x5))
#define POINC1 (*((volatile unsigned char *)0x6))
#define PODEC1 (*((volatile unsigned char *)0x7))
#define PRINC1 (*((volatile unsigned char *)0x8))
#define PLUSW1 (*((volatile unsigned char *)0x9))
#define INDF2 (*((volatile unsigned char *)0xA))
#define POINC2 (*((volatile unsigned char *)0xB))
#define PODEC2 (*((volatile unsigned char *)0xC))
#define PRINC2 (*((volatile unsigned char *)0xD))
#define PLUSW2 (*((volatile unsigned char *)0xE))
#define FSR0H (*((volatile unsigned char *)0xF))
#define FSR0L (*((volatile unsigned char *)0x10))
#define FSR1H (*((volatile unsigned char *)0x11))
#define FSR1L (*((volatile unsigned char *)0x12))
#define FSR2H (*((volatile unsigned char *)0x13))
#define FSR2L (*((volatile unsigned char *)0x14))
#define TOSH (*((volatile unsigned char *)0x16))
#define TOSL (*((volatile unsigned char *)0x17))
#define SKCN (*((volatile unsigned char *)0x18))
#define PCLATH (*((volatile unsigned char *)0x1A))
#define PCLATL (*((volatile unsigned char *)0x1B))
#define TBLPTRH (*((volatile unsigned char *)0x1D))
#define TBLPTRL (*((volatile unsigned char *)0x1E))
#define TBLDH (*((volatile unsigned char *)0x1F))
#define TBLDL (*((volatile unsigned char *)0x20))
#define PRODH (*((volatile unsigned char *)0x21))
#define PRODL (*((volatile unsigned char *)0x22))
#define INTE0 (*((volatile unsigned char *)0x23))
#define INTE1 (*((volatile unsigned char *)0x24))
#define INTE2 (*((volatile unsigned char *)0x25))
#define INTF0 (*((volatile unsigned char *)0x26))
#define INTF1 (*((volatile unsigned char *)0x27))
#define INTF2 (*((volatile unsigned char *)0x28))
#define WREG (*((volatile unsigned char *)0x29))
#define BSRCN (*((volatile unsigned char *)0x2A))
#define STATUS (*((volatile unsigned char *)0x2B))
#define MSTAT (*((volatile unsigned char *)0x2B))
#define PSTAT (*((volatile unsigned char *)0x2C))

#define PWRCN (*((volatile unsigned char *)0x2E))
#define OSCCN0 (*((volatile unsigned char *)0x2F))
#define OSCCN1 (*((volatile unsigned char *)0x30))
#define OSCCN2 (*((volatile unsigned char *)0x31))
#define CSFCN0 (*((volatile unsigned char *)0x32))
#define CSFCN1 (*((volatile unsigned char *)0x33))
#define WDTCN (*((volatile unsigned char *)0x34))
#define AD1H (*((volatile unsigned char *)0x35))
#define AD1M (*((volatile unsigned char *)0x36))
#define AD1L (*((volatile unsigned char *)0x37))
#define AD1CN0 (*((volatile unsigned char *)0x38))
#define AD1CN1 (*((volatile unsigned char *)0x39))
#define AD1CN2 (*((volatile unsigned char *)0x3A))
#define AD1CN3 (*((volatile unsigned char *)0x3B))
#define AD1CN4 (*((volatile unsigned char *)0x3C))
#define AD1CN5 (*((volatile unsigned char *)0x3D))
#define LVDCN (*((volatile unsigned char *)0x3E))
#define DACCN0 (*((volatile unsigned char *)0x3F))
#define DACCN1 (*((volatile unsigned char *)0x40))
#define DACBitH (*((volatile unsigned char *) 0x41))
#define DACBITH (*((volatile unsigned char *) 0x41))
#define DACBitL (*((volatile unsigned char *) 0x42))
#define DACBITL (*((volatile unsigned char *) 0x42))
#define OP1CN0 (*((volatile unsigned char *)0x43))
#define OP1CN1 (*((volatile unsigned char *)0x44))
#define OP1INP (*((volatile unsigned char *)0x45))
#define OP1INN1 (*((volatile unsigned char *)0x46))
#define OP1INN0 (*((volatile unsigned char *)0x47))

#define TMA1CN (*((volatile unsigned char *)0x48))
#define TMA1R (*((volatile unsigned char *)0x49))
#define TMA1C (*((volatile unsigned char *)0x4A))
#define TB1FLAG (*((volatile unsigned char *)0x4B))
#define TB1Flag (*((volatile unsigned char *)0x4B))
#define TB1CN0 (*((volatile unsigned char *)0x4C))
#define TB1CN1 (*((volatile unsigned char *)0x4D))
#define TB1RH (*((volatile unsigned char *)0x4E))
#define TB1RL (*((volatile unsigned char *)0x4F))
#define TB1C0H (*((volatile unsigned char *)0x50))
#define TB1C0L (*((volatile unsigned char *)0x51))
#define TB1C1H (*((volatile unsigned char *)0x52))
#define TB1C1L (*((volatile unsigned char *)0x53))
#define TB1C2H (*((volatile unsigned char *)0x54))
#define TB1C2L (*((volatile unsigned char *)0x55))
#define TC1CN0 (*((volatile unsigned char *)0x56))

#define PT1     (*((volatile unsigned char *)0x57))
#define PT1IN   (*((volatile unsigned char *)0x58))
#define TRISC1  (*((volatile unsigned char *)0x59))
#define PT1DA   (*((volatile unsigned char *)0x5A))
#define PT1PU   (*((volatile unsigned char *)0x5B))
#define PT1M1   (*((volatile unsigned char *)0x5C))
#define PT1M2   (*((volatile unsigned char *)0x5D))
#define PT1M3   (*((volatile unsigned char *)0x5E))
#define PT1INT  (*((volatile unsigned char *)0x5F))
#define PT1INTE (*((volatile unsigned char *)0x60))
#define PT1INTF (*((volatile unsigned char *)0x61))
#define PT2     (*((volatile unsigned char *)0x62))
#define PT2IN   (*((volatile unsigned char *)0x63))
#define TRISC2  (*((volatile unsigned char *)0x64))
#define PT2PU   (*((volatile unsigned char *)0x65))
#define PT2M1   (*((volatile unsigned char *)0x66))
#define PT2INT  (*((volatile unsigned char *)0x67))
#define PT2INTE (*((volatile unsigned char *)0x68))
#define PT2INTF (*((volatile unsigned char *)0x69))
#define PT3     (*((volatile unsigned char *)0x6A))
#define PT3IN   (*((volatile unsigned char *)0x6B))
#define TRISC3  (*((volatile unsigned char *)0x6C))
#define PT2DA   (*((volatile unsigned char *)0x6D))
#define PT3PU   (*((volatile unsigned char *)0x6E))
#define PT3M1   (*((volatile unsigned char *)0x6F))
#define PT3M2   (*((volatile unsigned char *)0x70))
#define PT3INT  (*((volatile unsigned char *)0x71))
#define PT3INTE (*((volatile unsigned char *)0x72))
#define PT3INTF (*((volatile unsigned char *)0x73))

#define UR0CN   (*((volatile unsigned char *)0x74))
#define UR0STA  (*((volatile unsigned char *)0x75))
#define BA0CN   (*((volatile unsigned char *)0x76))
#define BG0RH   (*((volatile unsigned char *)0x77))
#define BG0RL   (*((volatile unsigned char *)0x78))
#define TX0R    (*((volatile unsigned char *)0x79))
#define RC0REG  (*((volatile unsigned char *)0x7A))
#define MCCN0   (*((volatile unsigned char *)0x7B))
#define MCCN1   (*((volatile unsigned char *)0x7C))
#define MCCN2   (*((volatile unsigned char *)0x7D))
#define MCCN3   (*((volatile unsigned char *)0x7E))

#define FILTER  (*((volatile unsigned char *)0x7F))

#define CFG0 (*((volatile unsigned char *)0x180))
#define ACT0 (*((volatile unsigned char *)0x181))
#define STA0 (*((volatile unsigned char *)0x182))
#define CRG0 (*((volatile unsigned char *)0x183))
#define TOC0 (*((volatile unsigned char *)0x184))
#define RDB0 (*((volatile unsigned char *)0x185))
#define TDB0 (*((volatile unsigned char *)0x186))
#define SID0 (*((volatile unsigned char *)0x187))
#define BIECN (*((volatile unsigned char *)0x188))
#define BIEARH (*((volatile unsigned char *)0x189))
#define BIEARL (*((volatile unsigned char *)0x18A))
#define BIEDRH (*((volatile unsigned char *)0x18B))
#define BIEDRL (*((volatile unsigned char *)0x18C))
#define BIE2CN (*((volatile unsigned char *)0x18D))
#define BIE2ARH (*((volatile unsigned char *)0x18E))
#define BIE2ARL (*((volatile unsigned char *)0x18F))
#define BIE2DRH (*((volatile unsigned char *)0x190))
#define BIE2DRL (*((volatile unsigned char *)0x191))
#define EECR1 (*((volatile unsigned char *)0x192))
#define EECR2 (*((volatile unsigned char *)0x193))
#define EECOUNTER1 (*((volatile unsigned char *)0x194))
#define EECounter1 (*((volatile unsigned char *)0x194))
#define EECOUNTER2 (*((volatile unsigned char *)0x195))
#define EECounter2 (*((volatile unsigned char *)0x195))
#define EEPPointer (*((volatile unsigned char *)0x196))
#define EEPPOINTER (*((volatile unsigned char *)0x196))
#define EERD0  (*((volatile unsigned char *)0x197))
#define EERD1  (*((volatile unsigned char *)0x198))
#define EERD2  (*((volatile unsigned char *)0x199))
#define EERD3  (*((volatile unsigned char *)0x19A))
#define EERD4  (*((volatile unsigned char *)0x19B))
#define EERD5  (*((volatile unsigned char *)0x19C))
#define EERD6  (*((volatile unsigned char *)0x19D))
#define EERD7  (*((volatile unsigned char *)0x19E))
#define EERD8  (*((volatile unsigned char *)0x19F))
#define EERD9  (*((volatile unsigned char *)0x1A0))
#define EERD10 (*((volatile unsigned char *)0x1A1))
#define EERD11 (*((volatile unsigned char *)0x1A2))
#define EERD12 (*((volatile unsigned char *)0x1A3))
#define EERD13 (*((volatile unsigned char *)0x1A4))
#define EERD14 (*((volatile unsigned char *)0x1A5))
#define EERD15 (*((volatile unsigned char *)0x1A6))
#define EERD16 (*((volatile unsigned char *)0x1A7))
#define EERD17 (*((volatile unsigned char *)0x1A8))
#define EERD18 (*((volatile unsigned char *)0x1A9))
#define EERD19 (*((volatile unsigned char *)0x1AA))
#define EERD20 (*((volatile unsigned char *)0x1AB))
#define EERD21 (*((volatile unsigned char *)0x1AC))
#define EERD22 (*((volatile unsigned char *)0x1AD))
#define EERD23 (*((volatile unsigned char *)0x1AE))
#define EERD24 (*((volatile unsigned char *)0x1AF))
#define EERD25 (*((volatile unsigned char *)0x1B0))
#define EERD26 (*((volatile unsigned char *)0x1B1))
#define EERD27 (*((volatile unsigned char *)0x1B2))
#define EERD28 (*((volatile unsigned char *)0x1B3))
#define EERD29 (*((volatile unsigned char *)0x1B4))
#define EERD30 (*((volatile unsigned char *)0x1B5))
#define EERD31 (*((volatile unsigned char *)0x1B6))

extern volatile __xdata unsigned char *FSR0;
extern volatile __xdata unsigned char *FSR1;
extern volatile __xdata unsigned char *FSR2;
#define ADCRH (*((volatile unsigned char *)0x35))
#define ADCRM (*((volatile unsigned char *)0x36))
#define ADCRL (*((volatile unsigned char *)0x37))
extern volatile signed long ADCR;
#endif
