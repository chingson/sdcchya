;--------------------------------------------------------
;--------------------------------------------------------
	.module _longBCDinc
;--------------------------------------------------------
; overlayable items in internal ram 
; return value is void
;--------------------------------------------------------
;	
.area CCODE(code,REL,CON) ;
	
	.FUNC __longBCDinc:$r:0:2:$L:__longBCDinc_STK00:$L:r0x1000
		; maximum input is 32768, output is 0x32768, long return value W is 0
__longBCDinc:	
	MVF	_FSR0H,1,0
	MVF __longBCDinc_STK00,0,0
	MVF _FSR0L,1,0
	INF _INDF0,0,0
	DAW 
	MVF _POINC0,1,0
	MVL 3
	MVF r0x1000,1,0
LOOP_DAW:	
	MVL 0
	ADDC _INDF0,0,0
	DAW
	MVF _POINC0,1,0
	DCSZ r0x1000,1,0
	JMP LOOP_DAW

	RET

	
	

	.globl	__longBCDinc
	.globl __longBCDinc_STK00

	.ENDFUNC __longBCDinc
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.include "hy11p13sfr.inc"
	.area UDATA(DATA)


	.area STK(STK)
__longBCDinc_STK00: .DS 	1	
r0x1000: 		.DS			1

;--------------------------------------------------------
; global declarations
;--------------------------------------------------------

;--------------------------------------------------------
; global definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
	end
