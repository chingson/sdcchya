;--------------------------------------------------------
;--------------------------------------------------------
	.module _short2BCD
;--------------------------------------------------------
; overlayable items in internal ram 
; return value is (unsigned) long
;--------------------------------------------------------
;	udata_ovr
.area CCODE(code,REL,CON) ;
	
	.FUNC __short2BCD:$r:4:2:$L:__short2BCD_STK00:$L:r0x1000:$L:r0x1001	; input is W, output is also W
		; maximum input is 32768, output is 0x32768, long return value W is 0
__short2BCD:	
	MVF  r0x1000,1,0
	MVL  16
	MVF  r0x1001,1,0
	CLRF STK02	; LSB
	CLRF STK01 
	CLRF STK00
LOOP16:	
	RLFC __short2BCD_STK00,1,0
	RLFC r0x1000,1,0

	MVF  STK02,0,0
	ADDC STK02,0,0
	DAW
	MVF  STK02,1,0

	MVF  STK01,0,0
	ADDC STK01,0,0
	DAW
	MVF  STK01,1,0

	MVF STK00,0,0	; // max is 3
	ADDC STK00,1,0	; // max is 3


	DCSZ r0x1001,1,0
	JMP LOOP16
	CLRF _WREG
	RET

	
	

	.globl	__short2BCD
	.globl __short2BCD_STK00

	.ENDFUNC __short2BCD
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.include "hy11p13sfr.inc"
	.area UDATA(DATA)

	.globl STK01
	.globl STK02
	.globl STK00

	.area STK(STK)
__short2BCD_STK00: .DS 	1	
r0x1000:		.DS 		1
r0x1001:		.DS 		1

;--------------------------------------------------------
; global declarations
;--------------------------------------------------------

;--------------------------------------------------------
; global definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
	end
