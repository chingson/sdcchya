;--------------------------------------------------------
;--------------------------------------------------------
	.module _longlongBCDDiff
;--------------------------------------------------------
; overlayable items in internal ram 
; return value is (unsigned) long
;--------------------------------------------------------
;	udata_ovr
.area CCODE(code,REL,CON) ;
	
	.FUNC __longlongBCDDiff:$r:8:16:$L:__longlongBCDDiff_STK00:$L:__longlongBCDDiff_STK01:$L:__longlongBCDDiff_STK02\
	:$L:__longlongBCDDiff_STK03:$L:__longlongBCDDiff_STK04:$L:__longlongBCDDiff_STK05:$L:__longlongBCDDiff_STK06\
	:$L:__longlongBCDDiff_STK07:$L:__longlongBCDDiff_STK08:$L:__longlongBCDDiff_STK09:$L:__longlongBCDDiff_STK10\
	:$L:__longlongBCDDiff_STK11:$L:__longlongBCDDiff_STK12:$L:__longlongBCDDiff_STK13:$L:__longlongBCDDiff_STK14\
	:$L:r0x1000:$L:r0x1001	; 
		; return value is W - STK00
__longlongBCDDiff:	
	MVF  r0x1000,1,0

	MVF	 __longlongBCDDiff_STK14,0,0
	SUBF __longlongBCDDiff_STK06,0,0
	RLFC r0x1001,1,0	; keep C in temp
	BTSS _STATUS,3		; DC is bit 3
	ADDL 250			; (-6)
	BTSS r0x1001,0
	ADDL 0xA0
	RRFC r0x1001,1,0	; get back c
	MVF  STK06,1,0		; low byte

	MVF	 __longlongBCDDiff_STK13,0,0
	SUBC __longlongBCDDiff_STK05,0,0
	RLFC r0x1001,1,0	; keep C in temp
	BTSS _STATUS,3		; DC is bit 3
	ADDL 250			; (-6)
	BTSS r0x1001,0
	ADDL 0xA0
	RRFC r0x1001,1,0	; get back c
	MVF  STK05,1,0		; 


	MVF	 __longlongBCDDiff_STK12,0,0
	SUBC __longlongBCDDiff_STK04,0,0
	RLFC r0x1001,1,0	; keep C in temp
	BTSS _STATUS,3		; DC is bit 3
	ADDL 250			; (-6)
	BTSS r0x1001,0
	ADDL 0xA0
	RRFC r0x1001,1,0	; get back c
	MVF  STK04,1,0		; 

	MVF	 __longlongBCDDiff_STK11,0,0
	SUBC __longlongBCDDiff_STK03,0,0
	RLFC r0x1001,1,0	; keep C in temp
	BTSS _STATUS,3		; DC is bit 3
	ADDL 250			; (-6)
	BTSS r0x1001,0
	ADDL 0xA0
	RRFC r0x1001,1,0	; get back c
	MVF  STK03,1,0		; 


	MVF	 __longlongBCDDiff_STK10,0,0
	SUBC __longlongBCDDiff_STK02,0,0
	RLFC r0x1001,1,0	; keep C in temp
	BTSS _STATUS,3		; DC is bit 3
	ADDL 250			; (-6)
	BTSS r0x1001,0
	ADDL 0xA0
	RRFC r0x1001,1,0	; get back c
	MVF  STK02,1,0		; 

	MVF	 __longlongBCDDiff_STK09,0,0
	SUBC __longlongBCDDiff_STK01,0,0
	RLFC r0x1001,1,0	; keep C in temp
	BTSS _STATUS,3		; DC is bit 3
	ADDL 250			; (-6)
	BTSS r0x1001,0
	ADDL 0xA0
	RRFC r0x1001,1,0	; get back c
	MVF  STK01,1,0		; 

	MVF	 __longlongBCDDiff_STK08,0,0
	SUBC __longlongBCDDiff_STK00,0,0
	RLFC r0x1001,1,0	; keep C in temp
	BTSS _STATUS,3		; DC is bit 3
	ADDL 250			; (-6)
	BTSS r0x1001,0
	ADDL 0xA0
	RRFC r0x1001,1,0	; get back c
	MVF  STK00,1,0		; 

	MVF	 __longlongBCDDiff_STK07,0,0
	SUBC r0x1000,0,0
	RLFC r0x1001,1,0	; keep C in temp
	BTSS _STATUS,3		; DC is bit 3
	ADDL 250			; (-6)
	BTSS r0x1001,0
	ADDL 0xA0
	RRFC r0x1001,1,0	; get back c


	RET

	
	

	.globl	__longlongBCDDiff
	.globl __longlongBCDDiff_STK00
	.globl __longlongBCDDiff_STK01
	.globl __longlongBCDDiff_STK02
	.globl __longlongBCDDiff_STK03
	.globl __longlongBCDDiff_STK04
	.globl __longlongBCDDiff_STK05
	.globl __longlongBCDDiff_STK06
	.globl __longlongBCDDiff_STK07
	.globl __longlongBCDDiff_STK08
	.globl __longlongBCDDiff_STK09
	.globl __longlongBCDDiff_STK10
	.globl __longlongBCDDiff_STK11
	.globl __longlongBCDDiff_STK12
	.globl __longlongBCDDiff_STK13
	.globl __longlongBCDDiff_STK14

	.globl STK00
	.globl STK01
	.globl STK02
	.globl STK03
	.globl STK04
	.globl STK05
	.globl STK06

	.ENDFUNC __longlongBCDDiff
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.include "hy11p13sfr.inc"
	.area UDATA(DATA)


	.area STK(STK)
__longlongBCDDiff_STK00: .DS 	1	
__longlongBCDDiff_STK01: .DS 	1	
__longlongBCDDiff_STK02: .DS 	1	
__longlongBCDDiff_STK03: .DS 	1	
__longlongBCDDiff_STK04: .DS 	1	
__longlongBCDDiff_STK05: .DS 	1	
__longlongBCDDiff_STK06: .DS 	1	
__longlongBCDDiff_STK07: .DS 	1	
__longlongBCDDiff_STK08: .DS 	1	
__longlongBCDDiff_STK09: .DS 	1	
__longlongBCDDiff_STK10: .DS 	1	
__longlongBCDDiff_STK11: .DS 	1	
__longlongBCDDiff_STK12: .DS 	1	
__longlongBCDDiff_STK13: .DS 	1	
__longlongBCDDiff_STK14: .DS 	1	
r0x1000: .DS 1
r0x1001: .DS 1

;--------------------------------------------------------
; global declarations
;--------------------------------------------------------

;--------------------------------------------------------
; global definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
	end
