;--------------------------------------------------------
;--------------------------------------------------------
	.module _longBCDDiff
;--------------------------------------------------------
; overlayable items in internal ram 
; return value is (unsigned) long
;--------------------------------------------------------
;	udata_ovr
.area CCODE(code,REL,CON) ;
	
	.FUNC __longBCDDiff:$r:4:8:$L:__longBCDDiff_STK00:$L:__longBCDDiff_STK01:$L:__longBCDDiff_STK02\
	:$L:__longBCDDiff_STK03:$L:__longBCDDiff_STK04:$L:__longBCDDiff_STK05:$L:__longBCDDiff_STK06\
	:$L:r0x1000:$L:r0x1001	; 
		; return value is W - STK00
__longBCDDiff:	
	MVF  r0x1000,1,0

	MVF	 __longBCDDiff_STK06,0,0
	SUBF __longBCDDiff_STK02,0,0
	RLFC r0x1001,1,0	; keep C in temp
	BTSS _STATUS,3		; DC is bit 3
	ADDL 250			; (-6)
	BTSS r0x1001,0
	ADDL 0xA0
	RRFC r0x1001,1,0	; get back c
	MVF  STK02,1,0		; low byte

	MVF	 __longBCDDiff_STK05,0,0
	SUBC __longBCDDiff_STK01,0,0
	RLFC r0x1001,1,0	; keep C in temp
	BTSS _STATUS,3		; DC is bit 3
	ADDL 250			; (-6)
	BTSS r0x1001,0
	ADDL 0xA0
	RRFC r0x1001,1,0	; get back c
	MVF  STK01,1,0		; 


	MVF	 __longBCDDiff_STK04,0,0
	SUBC __longBCDDiff_STK00,0,0
	RLFC r0x1001,1,0	; keep C in temp
	BTSS _STATUS,3		; DC is bit 3
	ADDL 250			; (-6)
	BTSS r0x1001,0
	ADDL 0xA0
	RRFC r0x1001,1,0	; get back c
	MVF  STK00,1,0		; 

	MVF	 __longBCDDiff_STK03,0,0
	SUBC r0x1000,0,0
	RLFC r0x1001,1,0	; keep C in temp
	BTSS _STATUS,3		; DC is bit 3
	ADDL 250			; (-6)
	BTSS r0x1001,0
	ADDL 0xA0
	RRFC r0x1001,1,0	; get back c


	RET

	
	

	.globl	__longBCDDiff
	.globl __longBCDDiff_STK00
	.globl __longBCDDiff_STK01
	.globl __longBCDDiff_STK02
	.globl __longBCDDiff_STK03
	.globl __longBCDDiff_STK04
	.globl __longBCDDiff_STK05
	.globl __longBCDDiff_STK06

	.globl STK00
	.globl STK01
	.globl STK02

	.ENDFUNC __longBCDDiff
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.include "hy11p13sfr.inc"
	.area UDATA(DATA)


	.area STK(STK)
__longBCDDiff_STK00: .DS 	1	
__longBCDDiff_STK01: .DS 	1	
__longBCDDiff_STK02: .DS 	1	
__longBCDDiff_STK03: .DS 	1	
__longBCDDiff_STK04: .DS 	1	
__longBCDDiff_STK05: .DS 	1	
__longBCDDiff_STK06: .DS 	1	
r0x1000: .DS 1
r0x1001: .DS 1

;--------------------------------------------------------
; global declarations
;--------------------------------------------------------

;--------------------------------------------------------
; global definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
	end
