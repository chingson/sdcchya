;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.50 #9253 (Feb 22 2016) (MSVC)
; This file was generated Mon Feb 22 17:50:08 2016
;--------------------------------------------------------
; Port for HYCON CPU
;--------------------------------------------------------
	.module _g2ptrget3
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CCODE(code,REL,CON) ; _mulint-code 
;entry:  __mulint:	;Function start
; 2 exit points
;has an exit
;; Starting pCode block
.FUNC __g2ptrget3:$r:3:2:$L:__g2ptrget3_STK00:$L:_temp00


__g2ptrget3:	;Function start
	BTSZ	_WREG,7,0 ; 80 means ROM
	JMP	ROMACC
	
	;MVF	__g2ptrget3_STK00,0,0
	MVF _FSR0H,1,0
	MVF	__g2ptrget3_STK00,0,0
	MVF _FSR0L,1,0
	MVF	_POINC0,0,0
	MVF	STK01,1,0
	MVF	_POINC0,0,0
	MVF	STK00,1,0
	MVF	_POINC0,0,0
	JMP FIN
GETLOWBYTE:
	MVF   _BIEDRL,0,1
	BSF _BIECN,0,1
	RET

	
ROMACC:
	;BCF	_STATUS,4,0
	MVF  _temp00,1,0
	MVL	0x69
	MVF _BIEKEY,1,1
	MVL	0xC0
	MVF	_BIECN,1,1
	RRFC	_temp00,0,0
	ANDL	0x3f
	MVF	_BIEARH,1,1
	RRFC	__g2ptrget3_STK00,0,0
	MVF	_BIEARL,1,1
	BSF _BIECN,0,1
	MVF	_BIEDRH,0,1	; H is first byte!!
	BTSZ	_STATUS,4,0	; hardware is .. big endian
	CALL GETLOWBYTE	; L is second byte
	MVF	STK01,1,0
	;BTSZ	_STATUS,4,0	; hardware is .. big endian
	;BSF _BIECN,0,1
	;BTGF	_STATUS,4,0	; change C
	MVF	_BIEDRH,0,1	; H is first byte!!
	BTSS	_STATUS,4,0	; hardware is .. big endian
	CALL GETLOWBYTE	; H is first byte!!
	MVF	STK00,1,0
	;BTSS	_STATUS,4,0	; hardware is .. big endian
	;BSF _BIECN,0,1
	;BTGF	_STATUS,4,0	; change C
	MVF	_BIEDRH,0,1	; H is first byte!!
	BTSZ	_STATUS,4,0	; hardware is .. big endian
	CALL GETLOWBYTE	; H is first byte!!
FIN:
	CLRF _BIECN,1
	CLRF _BIEKEY,1

	RET
	.ENDFUNC __g2ptrget3

	
	

;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.area UDATA(DATA)

	.include "hy17p41sfr.inc"
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
_temp00: .ds 1	
;--------------------------------------------------------
; global declarations
;--------------------------------------------------------
	.globl	__g2ptrget3

	.area STK(STK)
__g2ptrget3_STK00: .ds 1

	.globl __g2ptrget3_STK00

;--------------------------------------------------------
; global definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
	end
