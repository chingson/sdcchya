;--------------------------------------------------------
;--------------------------------------------------------
	.module _shortBCDDiff
;--------------------------------------------------------
; overlayable items in internal ram 
; return value is (unsigned) long
;--------------------------------------------------------
;	udata_ovr
.area CCODE(code,REL,CON) ;
	
	.FUNC __shortBCDDiff:$r:2:4:$L:__shortBCDDiff_STK00:$L:__shortBCDDiff_STK01:$L:__shortBCDDiff_STK02:$L:r0x1000:$L:r0x1001	; 
		; return value is W - STK00
__shortBCDDiff:	
	MVF  r0x1000,1,0

	MVF	 __shortBCDDiff_STK02,0,0
	SUBF __shortBCDDiff_STK00,0,0
	RLFC r0x1001,1,0	; keep C in temp
	BTSS _STATUS,3		; DC is bit 3
	ADDL 250			; (-6)
	BTSS r0x1001,0
	ADDL 0xA0
	RRFC r0x1001,1,0	; get back c
	MVF  STK00,1,0		; low byte

	MVF	 __shortBCDDiff_STK01,0,0
	SUBC r0x1000,0,0
	RLFC r0x1001,1,0	; keep C in temp
	BTSS _STATUS,3		; DC is bit 3
	ADDL 250			; (-6)
	BTSS r0x1001,0
	ADDL 0xA0
	RRFC r0x1001,1,0	; get back c


	RET

	
	

	.globl	__shortBCDDiff
	.globl __shortBCDDiff_STK00
	.globl __shortBCDDiff_STK01
	.globl __shortBCDDiff_STK02

	.globl STK00

	.ENDFUNC __shortBCDDiff
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.include "hy11p13sfr.inc"
	.area UDATA(DATA)


	.area STK(STK)
__shortBCDDiff_STK00: .DS 	1	
__shortBCDDiff_STK01: .DS 	1	
__shortBCDDiff_STK02: .DS 	1	
r0x1000: .DS 1
r0x1001: .DS 1

;--------------------------------------------------------
; global declarations
;--------------------------------------------------------

;--------------------------------------------------------
; global definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
	end
