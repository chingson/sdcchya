
	.module gsinit4096
	.area SFR1 (DATA,ABS)
	.include "hy11p13sfr.inc"

	.area	IDATAX	(XDATA) ;test-2-data
	.area	XDATAROM	(CODE) 
	.area CODE (code) ; only 1 code area
.globl		__sdcc_gsinit_startup4096
.globl		_main
.globl		s_IDATA
.globl		s_IDATAROM
.globl		l_IDATA
.globl		s_IDATAX
.globl		e_IDATAX
.globl		s_XDATAROM


__sdcc_gsinit_startup4096:
	ldpr	0x80,0
	mvl	0
loop_clear_ram0:
	clrf	_POINC0,0
	INSZ	_WREG,1,0	; d=1 or 0 should be the same?
	JMP	loop_clear_ram0


	ldpr	0x200,0
loop_clear_ram1:
	clrf	_POINC0,0
	MVF		_FSR0H,0,0
	ANDL		0x0f
	JNZ		loop_clear_ram1

;// next is to copy idata

	lbsr	0x100
	
	ldpr	s_IDATA,0
	mvlp	s_IDATAROM
; we move 256 bytes blocks first


	MVL	HIGH(l_IDATA)
	MVF _FSR1L,1,0 // temp
	IORL	0
	JZ		_COPY2
loop_copy0:
	MVL	128
loop_copy1:
	TBLR	*+
	MVFF	_TBLDH,_POINC0 ; it must from even address!!
	MVFF	_TBLDL,_POINC0
	DCSZ	_WREG,1,0
	jmp		loop_copy1
	DCSZ	_FSR1L,1,0
	jmp		loop_copy0

_COPY2:
	; now final ones
	MVL	 (l_IDATA)
	IORL	0
	JZ		FIN
loop_copy2:
	TBLR	*+
	MVFF	_TBLDH,_POINC0 ; it must from even address!!
	DCSUZ	_WREG,1,0
	JMP		FIN
	MVFF	_TBLDL,_POINC0 ; it must from even address!!
	DCSZ	_WREG,1,0
	JMP		loop_copy2
FIN:	
	JMP		_main
	
	
	
	


