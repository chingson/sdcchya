
#define MSB_SET(x) (x&0x8000)

unsigned int
_moduint (unsigned int a, unsigned int b)
{
  unsigned char count = 0;

  while (!MSB_SET(b) && b<=a)
  {
    b <<= 1;
    count++;
  }
  do
  {
    if (a >= b)
      a -= b;
    b >>= 1;
  }
  while (count--);
  return a;
}

