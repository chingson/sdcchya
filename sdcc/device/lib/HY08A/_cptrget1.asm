
	; const table look up
	; even and odd
	; linker will select the correct address
	; 2018 NOV add table look up
	; TBLPTR has already the address, (WORD) 
	; W is the offset (byte)
	; C set 1 means start is odd ==> add C to get real byte offset after ..

	.module _cptrget1

.area CCODE(code,REL,CON) ; 
	
	.FUNC __cptrget1:$r:1:1:$C:__ADDTBL
__cptrget1:	;assume even

	BCF		_STATUS,4
	RRFC 	_WREG,0,0		; 128 word is ok (0x80), now C is oddbyte/even byte
	JC		USE_DL
	CALL	__ADDTBL
	MVF		_TBLDH,0,0
	RET
USE_DL:
	CALL	__ADDTBL
	MVF		_TBLDL,0,0
	RET
	.ENDFUNC __cptrget1

	; ODD version
	.FUNC __cptrgeto:$r:1:1:$C:__ADDTBL
__cptrgeto:	;assume even
	BCF		_STATUS,4
	RRFC 	_WREG,0,0		; 128 word is ok (0x80), now C is oddbyte/even byte
	JC		USE_DLO
	CALL	__ADDTBL
	MVF		_TBLDL,0,0
	RET
USE_DLO:	
	INSUZ	_TBLPTRL,1,0
	INF		_TBLPTRH,1,0
	CALL	__ADDTBL
	MVF		_TBLDH,0,0
	RET
	
	.ENDFUNC __cptrgeto

	
	.FUNC __ADDTBL:$r:1:1
__ADDTBL:
	ADDF	_TBLPTRL,1,0
	BTSZ	_STATUS,4
	INF		_TBLPTRH,1,0
	TBLR    *+
	RET
	.ENDFUNC __ADDTBL
	
	.globl	__cptrget1, __cptrgeto

;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.area UDATA(DATA)
	.include "hy11p13sfr.inc"
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00

	end
