
// divuint
#define MSB_SET(x) (x&0x8000)


unsigned int
_divuint (unsigned int x, unsigned int y)
{
  unsigned int reste = 0;
  unsigned char count = 16;

  do
  {
    // reste: x <- 0;
    reste <<= 1;
	if(x&0x8000)
	{
      		reste |= 1;
	}
    x <<= 1;

    if (reste >= y)
    {
      reste -= y;
      // x <- (result = 1)
      x |= 1;
    }
  }
  while (--count);
  return x;
}

