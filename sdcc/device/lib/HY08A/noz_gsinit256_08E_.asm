
	.module noz_gsinit256_08e
	.area SFR1 (DATA,ABS)
	.include "hy17p41sfr.inc"

	.area CODE (code) ; only 1 code area
.globl		__sdcc_gsinit_startup256_08e
.globl		_main
.globl		s_IDATA
.globl		s_IDATAROM
.globl		l_IDATA


__sdcc_gsinit_startup256_08e:
;	ldpr	0x80,0
;	mvl	0
;loop_clear_ram0:
;	clrf	_POINC0,0
;	INSZ	_WREG,1,0	; d=1 or 0 should be the same?
;	JMP	loop_clear_ram0
;// next is to copy idata

	lbsr	0x100
	
	ldpr	s_IDATA,0
	;mvlp	s_IDATAROM

	mvl 0x69
	mvf _BIEKEY,1,1
	mvl 0xC0;
	mvf _BIECN,1,1

	mvl s_IDATAROM>>1 ; // word base
	mvf _BIEARL,1,1
	mvl s_IDATAROM>>9
	mvf _BIEARH,1,1
	BSF _BIECN,0,1

	MVL	l_IDATA+1
loop_copy1:
	DCSUZ	_WREG,1,0
	JMP	inifin
	
	MVFF	_BIEDRH,_POINC0
	DCSUZ	_WREG,1,0
	JMP	inifin
	MVFF	_BIEDRL,_POINC0
	BSF _BIECN,0,1
	JMP	loop_copy1
	
inifin: ; init finish
	clrf     _BIECN,0;
	clrf	 _BIEKEY,0
	JMP		_main

	


