
	.module noz_gsinit128fsr2
	.area SFR1 (DATA,ABS)
	.include "hy15p41sfr.inc"

	.area CODE (code) ; only 1 code area
.globl		__sdcc_gsinit_startup128fsr2
.globl		_main
.globl		s_IDATA
.globl		s_IDATAROM
.globl		l_IDATA
.globl		a_STKTOP
.globl 	__sdcc_gsinit_startup128fsr2


__sdcc_gsinit_startup128fsr2:
;	ldpr	0x80,0
;	mvl	0x80
;loop_clear_ram0:
;	clrf	_POINC0,0
;	INSZ	_WREG,1,0	; d=1 or 0 should be the same?
;	JMP	loop_clear_ram0
;// next is to copy idata

	lbsr	0x00
	
	ldpr	s_IDATA,0
	mvlp	s_IDATAROM
	MVL	l_IDATA+1
loop_copy1:
	DCSUZ	_WREG,1,0
	JMP	pre1
	TBLR	*+
	MVFF	_TBLDH,_POINC0
	DCSUZ	_WREG,1,0
	JMP	pre1
	MVFF	_TBLDL,_POINC0
	JMP	loop_copy1
	
pre1:
	// set FSR2
	// H08C fix OSCCN1, yet
	;BSF		_OSCCN1,7 ; c compiler enable
	LDPR		a_STKTOP-1,2
	JMP		_main
	
	


