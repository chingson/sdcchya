#define MSB_SET(x) (x&0x80000000UL)
#define UL(x) ((unsigned long)x)
long
_modslong (long a, long b)
{
  unsigned char sign=0;
  unsigned char count = 0;
  //int r;
  if(a<0)
  {
	a=-a;
	sign=0xff;
  }
  if(b<0)
	b=-b;
  while (!MSB_SET(b) && UL(b)<=UL(a))
  {
    b <<= 1;
    count++;
  }
  do
  {
    if (UL(a) >= UL(b))
      a -= b;
    b = UL(b)>>1;
  }
  while (count--);
  if (sign)
    return -a;
  return a;
}

