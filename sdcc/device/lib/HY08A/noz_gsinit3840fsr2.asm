
	.module noz_gsinit3840fsr2
	.area SFR1 (DATA,ABS)
	.include "hy17p56sfr.inc"

	.area CODE (code) ; only 1 code area
.globl		__sdcc_gsinit_startup3840fsr2
.globl		_main
.globl		s_IDATA
.globl		s_IDATAROM
.globl		l_IDATA
.globl		a_STKTOP
.globl 	__sdcc_gsinit_startup3840fsr2
	;_OSCCN1 = 0x35;

; H08D uses flat memory model,
; 80~17f are used by 
__sdcc_gsinit_startup3840fsr2:
;	ldpr	0x80,0
;	mvl	0x00
;loop_clear_ram0:
;	clrf	_POINC0,0
;	INSZ	_WREG,1,0	; d=1 or 0 should be the same?
;	JMP	loop_clear_ram0
;	ldpr	0x200,0
;	mvl	0x3
;loop_clear_ram1:
;	clrf	_POINC0,0
;	CPSL	_FSR0H,0 ; skip if F<W
;	BTSS	_FSR0L,7
;	jmp  loop_clear_ram1
	
;// next is to copy idata

	lbsr	0x00
	
	ldpr	s_IDATA,0 ; // s_IDATA can be start at 200
	mvlp	s_IDATAROM
	MVL l_IDATA>>8
loop_copy0:
	IORL  0
	JZ   copy_left
	CLRF _FSR1L
loop_a:
	TBLR	*+
	MVFF	_TBLDH,_POINC0
	MVFF	_TBLDL,_POINC0
	INF     _FSR1L,1,0
	BTSS	_FSR1L,7
	JMP		loop_a
	DCF		_WREG,0,0
	jmp		loop_copy0
    

copy_left:

	MVL	l_IDATA+1
loop_copy1:
	DCSUZ	_WREG,1,0
	JMP	pre1
	TBLR	*+
	MVFF	_TBLDH,_POINC0
	DCSUZ	_WREG,1,0
	JMP	pre1
	MVFF	_TBLDL,_POINC0
	JMP	loop_copy1
	
pre1:
	// bsr set to 200
	LBSR	0x200
	// set FSR2
	;BSF		_OSCCN1,7 ; c compiler enable
	LDPR		a_STKTOP-1,2
	JMP		_main
	
	


