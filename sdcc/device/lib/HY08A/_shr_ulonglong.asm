;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.3.0 #8604 (Sep 15 2014) (MSVC)
; This file was generated Sat Oct 11 21:41:17 2014
;--------------------------------------------------------
	.module _shr_ulonglong
;--------------------------------------------------------
; Code Head 
;--------------------------------------------------------
	.area CCODE	 (CODE) 
.FUNC __shr_ulonglong:$r:8:1
;--------------------------------------------------------
; code
;--------------------------------------------------------
; sh_ulonglong, assembly is the source!!
__shr_ulonglong:	;Function start
; 2 exit points
	IORL	0
	JZ		FIN
loop_shr:
	BCF		_STATUS,4
	RRFC	STK07,1,0
	RRFC	STK00,1,0
	RRFC	STK01,1,0
	RRFC	STK02,1,0
	RRFC	STK03,1,0
	RRFC	STK04,1,0
	RRFC	STK05,1,0
	RRFC	STK06,1,0
	DCSZ	_WREG,1,0
	JMP		loop_shr
FIN:
	MVF		STK07,0,0
	RET
; 2 exit points
	
	




;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
;--------------------------------------------------------
; global declarations
;--------------------------------------------------------
	.globl	__shr_ulonglong
	.include "hy11p13sfr.inc"
	.globl	STK00
	.globl	STK01
	.globl	STK02
	.globl	STK03
	.globl	STK04
	.globl	STK05
	.globl	STK06
	.globl	STK07


	;end
