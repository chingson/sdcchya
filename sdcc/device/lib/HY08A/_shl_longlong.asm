;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.3.0 #8604 (Sep 15 2014) (MSVC)
; This file was generated Sat Oct 11 21:41:17 2014
;--------------------------------------------------------
	.module _shl_longlong
;--------------------------------------------------------
; Code Head 
;--------------------------------------------------------
	.area CCODE	 (CODE) 
.FUNC __shl_longlong:$r:8:1
;--------------------------------------------------------
; code
;--------------------------------------------------------
; sh_ulonglong, assembly is the source!!
; input: STK0 is high byte
__shl_longlong:	;Function start
; 2 exit points
	IORL		0
	JZ		fin
loop_shr:
	BCF		_STATUS,4 ; c clear
	RLFC	STK06,1,0
	RLFC	STK05,1,0
	RLFC	STK04,1,0
	RLFC	STK03,1,0
	RLFC	STK02,1,0
	RLFC	STK01,1,0
	RLFC	STK00,1,0
	RLFC	STK07,1,0
	DCSZ	_WREG,1,0
	JMP		loop_shr
fin:
	MVF	STK07,0,0 ; MSB at W
	RET
	
	

; exit point of _shl_longlong



;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
;--------------------------------------------------------
; global declarations
;--------------------------------------------------------
	.globl	__shl_longlong
	.include "hy11p13sfr.inc"
	.globl	STK00
	.globl	STK01
	.globl	STK02
	.globl	STK03
	.globl	STK04
	.globl	STK05
	.globl	STK06
	.globl	STK07


	;.area LOCALSTK (STK); local stack var
;__shl_longlong_STK00:	.ds	1
	;.globl __shl_longlong_STK00
;--------------------------------------------------------
; global definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
	;end
