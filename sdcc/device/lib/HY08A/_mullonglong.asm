;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.6.0 #9615 (Linux) [Oct 13 2017]
;--------------------------------------------------------
; Port for HYCON CPU
;--------------------------------------------------------
	.module _mullonglong
	;--cdb--S:G$_mullonglong$0$0({2}DF,SI:SE),C,0,0
	;--cdb--F:_mullonglong:G$_mullonglong$0$0({2}DF,SI:S),C,0,0,0,0,0
	;--cdb--S:G$_mullong$0$0({2}DF,SL:S),C,0,0
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CCODE (code,REL,CON) ; _mullonglong-code 
.globl __mullonglong
.globl __addmul
.define yb0 __mullonglong_STK14
.define yb1 __mullonglong_STK13
.define yb2 __mullonglong_STK12
.define yb3 __mullonglong_STK11
.define yb4 __mullonglong_STK10
.define yb5 __mullonglong_STK09
.define yb6 __mullonglong_STK08
.define yb7 __mullonglong_STK07
.define xb0 __mullonglong_STK06
.define xb1 __mullonglong_STK05
.define xb2 __mullonglong_STK04
.define xb3 __mullonglong_STK03
.define xb4 __mullonglong_STK02
.define xb5 __mullonglong_STK01
.define xb6 __mullonglong_STK00
.define xb7 r0x1223
.define resB7 BUF15+7
_PRODL	equ 	0x22
_PRODH	equ 	0x21
_PRINC0 equ	0x01
_PCLATL equ	0x1B
_PCLATH equ	0x1A
_STATUS equ	0x2B
_FSR0L  equ	0x10
_FSR0H  equ	0x0F
_POINC0 equ	0x01

	
	
	; FSR0 is STK06-k
	; add to STK00
	; callee need add to MSB after return
	; only for 6..1 case..(w=5..0) 7 is simple 2words, skip
	
	
;--------------------------------------------------------
	.FUNC __mullonglong:$r:8:16:$C:__addmul\
:$L:r0x1223:$L:__mullonglong_STK00:$L:__mullonglong_STK01:$L:__mullonglong_STK02:$L:__mullonglong_STK03\
:$L:__mullonglong_STK04:$L:__mullonglong_STK05:$L:__mullonglong_STK06:$L:__mullonglong_STK07:$L:__mullonglong_STK08\
:$L:__mullonglong_STK09:$L:__mullonglong_STK10:$L:__mullonglong_STK11:$L:__mullonglong_STK12:$L:__mullonglong_STK13\
:$L:__mullonglong_STK14:$L:BUF15
;--------------------------------------------------------
	.macro movfw f
	MVF	f,0,0
	.endm
	.macro movwf f
	MVF	f,1,0
	.endm
	.macro addwf f
	ADDF	f,1,0
	.endm
	.macro adcwf f
	ADDC	f,1,0
	.endm
	

	.macro mulab ina,inb
	MVF	ina,0,0
	MULF	inb,0
	.endm
	.macro addbyte7 
	movfw	_PRODL
	addwf	resB7
	.endm

__mullonglong:	;Function start
	MVF	r0x1223,1,0
	CLRF	BUF15
	CLRF	BUF15+1
	CLRF	BUF15+2
	CLRF	BUF15+3
	CLRF	BUF15+4
	CLRF	BUF15+5
	CLRF	BUF15+6
	CLRF	BUF15+7
// now 7
	mulab 	yb0,xb7
	addbyte7
	mulab 	yb1,xb6
	addbyte7
	mulab 	yb2,xb5
	addbyte7
	mulab 	yb3,xb4
	addbyte7
	mulab 	yb4,xb3
	addbyte7
	mulab 	yb5,xb2
	addbyte7
	mulab 	yb6,xb1
	addbyte7
	mulab 	yb7,xb0
	addbyte7

	MVL	(BUF15 + 6)
	MVF	STK00,1,0
	MVL	high(BUF15 + 6)	; add to 6~13
	MVF	STK01,1,0

	mulab 	yb0,xb6
	call	__addmul
	mulab 	yb1,xb5
	call	__addmul
	mulab 	yb2,xb4
	call	__addmul
	mulab 	yb3,xb3
	call	__addmul
	mulab 	yb4,xb2
	call	__addmul
	mulab 	yb5,xb1
	call	__addmul
	mulab 	yb6,xb0
	call	__addmul

	DCF	STK00,1,0
	INSUZ	STK00,0,0
	DCF	STK01,1,0

	mulab 	yb0,xb5
	call	__addmul
	mulab 	yb1,xb4
	call	__addmul
	mulab 	yb2,xb3
	call	__addmul
	mulab 	yb3,xb2
	call	__addmul
	mulab 	yb4,xb1
	call	__addmul
	mulab 	yb5,xb0
	call	__addmul

	DCF	STK00,1,0
	INSUZ	STK00,0,0
	DCF	STK01,1,0

	mulab 	yb0,xb4
	call	__addmul
	mulab 	yb1,xb3
	call	__addmul
	mulab 	yb2,xb2
	call	__addmul
	mulab 	yb3,xb1
	call	__addmul
	mulab 	yb4,xb0
	call	__addmul
	DCF	STK00,1,0
	INSUZ	STK00,0,0
	DCF	STK01,1,0

	mulab 	yb0,xb3
	call	__addmul
	mulab 	yb1,xb2
	call	__addmul
	mulab 	yb2,xb1
	call	__addmul
	mulab 	yb3,xb0
	call	__addmul
	DCF	STK00,1,0
	INSUZ	STK00,0,0
	DCF	STK01,1,0


	mulab 	yb0,xb2
	call	__addmul
	mulab 	yb1,xb1
	call	__addmul
	mulab 	yb2,xb0
	call	__addmul

	DCF	STK00,1,0
	INSUZ	STK00,0,0
	DCF	STK01,1,0

	mulab 	yb0,xb1
	call	__addmul
	mulab 	yb1,xb0
	call	__addmul

	DCF	STK00,1,0
	INSUZ	STK00,0,0
	DCF	STK01,1,0

	mulab 	yb0,xb0
	call	__addmul
	// add from byte7..0
	movfw	BUF15
	movwf	STK06
	movfw	BUF15+1
	movwf	STK05
	movfw	BUF15+2
	movwf	STK04
	movfw	BUF15+3
	movwf	STK03
	movfw	BUF15+4
	movwf	STK02
	movfw	BUF15+5
	movwf	STK01
	movfw	BUF15+6
	movwf	STK00
	movfw	BUF15+7

	
	RET	
; exit point of __mullonglong
	.ENDFUNC	__mullonglong

	.FUNC __addmul:$r:0:0
__addmul:
	movfw	STK00
	movwf	_FSR0L
	movfw	STK01
	movwf	_FSR0H
	movfw	_PRODL
	addf	_POINC0,1,0
	movfw	_PRODH		; some dummy but we accept
	addc	_POINC0,1,0
	MVL	0
	addc	_POINC0,1,0
	addc	_POINC0,1,0
	addc	_POINC0,1,0
	addc	_POINC0,1,0
	addc	_POINC0,1,0
	addc	_POINC0,1,0
	ret
	.ENDFUNC __addmul


;--------------------------------------------------------

;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.globl	__mullong
	.globl	__addmul

	.globl WSAVE
	.globl STK07
	.globl STK07_SAVE
	.globl STK06
	.globl STK06_SAVE
	.globl STK05
	.globl STK05_SAVE
	.globl STK04
	.globl STK04_SAVE
	.globl STK03
	.globl STK03_SAVE
	.globl STK02
	.globl STK02_SAVE
	.globl STK01
	.globl STK01_SAVE
	.globl STK00
	.globl STK00_SAVE
;--------------------------------------------------------
; global -1 declarations
;--------------------------------------------------------
	.globl	__mullonglong

;--------------------------------------------------------
; global -2 definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area IDATA (DATA,REL,CON); pre-def
	.area IDATAROM (CODE,REL,CON); pre-def
	.area UDATA (DATA,REL,CON); pre-def
	.area UDATA (DATA,REL,CON) ;UDL__mullonglong_0	udata
r0x1223:	.ds	1
	.area LOCALSTK (STK); local stack var
__mullonglong_STK00:	.ds	1
	.globl __mullonglong_STK00
__mullonglong_STK01:	.ds	1
	.globl __mullonglong_STK01
__mullonglong_STK02:	.ds	1
	.globl __mullonglong_STK02
__mullonglong_STK03:	.ds	1
	.globl __mullonglong_STK03
__mullonglong_STK04:	.ds	1
	.globl __mullonglong_STK04
__mullonglong_STK05:	.ds	1
	.globl __mullonglong_STK05
__mullonglong_STK06:	.ds	1
	.globl __mullonglong_STK06
__mullonglong_STK07:	.ds	1
	.globl __mullonglong_STK07
__mullonglong_STK08:	.ds	1
	.globl __mullonglong_STK08
__mullonglong_STK09:	.ds	1
	.globl __mullonglong_STK09
__mullonglong_STK10:	.ds	1
	.globl __mullonglong_STK10
__mullonglong_STK11:	.ds	1
	.globl __mullonglong_STK11
__mullonglong_STK12:	.ds	1
	.globl __mullonglong_STK12
__mullonglong_STK13:	.ds	1
	.globl __mullonglong_STK13
__mullonglong_STK14:	.ds	1
BUF15:			.ds	14

	.globl __mullonglong_STK14
	end
