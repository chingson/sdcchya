;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.50 #9253 (Feb 22 2016) (MSVC)
; This file was generated Mon Feb 22 17:50:08 2016
;--------------------------------------------------------
; Port for HYCON CPU
;--------------------------------------------------------
	.module _g2ptrget8_stk
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CCODE(code,REL,CON) ; _mulint-code 
;entry:  __mulint:	;Function start
; 2 exit points
;has an exit
;; Starting pCode block
.FUNC __g2ptrget8:$r:8:2:$L:__g2ptrget8_STK00:$L:TMP0:$L:TMP1:$L:TMP2:$L:TMPA


__g2ptrget8:	;Function start
	BTSZ	_WREG,7,0 ; 80 means ROM
	JMP	ROMACC
	;// here is RAM
	MVF _FSR0H,1,0
	MVF	__g2ptrget8_STK00,0,0
	MVF _FSR0L,1,0
	MVF	_POINC0,0,0
	MVF	STK06,1,0
	MVF	_POINC0,0,0
	MVF	STK05,1,0
	MVF	_POINC0,0,0
	MVF	STK04,1,0
	MVF	_POINC0,0,0
	MVF	STK03,1,0
	MVF	_POINC0,0,0
	MVF	STK02,1,0
	MVF	_POINC0,0,0
	MVF	STK01,1,0
	MVF	_POINC0,0,0
	MVF	STK00,1,0
	MVF	_POINC0,0,0
	JMP FIN

	
ROMACC:
	;BCF	_STATUS,4,0
	RRFC	_WREG,0,0
	ANDL	0x3f
	MVF	_TBLPTRH,1,0
	RRFC	__g2ptrget8_STK00,0,0
	MVF	_TBLPTRL,1,0
	TBLR	*+
	MVF	_TBLDH,0,0	; H is first byte!!
	BTSZ	_STATUS,4,0	; hardware is .. big endian
	MVF	_TBLDL,0,0	; L is second byte
	MVF	STK06,1,0

	BTSZ	_STATUS,4,0	; hardware is .. big endian
	TBLR	*+
	MVF	_TBLDH,0,0	; H is first byte!!
	BTSS	_STATUS,4,0	; hardware is .. big endian
	MVF	_TBLDL,0,0	; H is first byte!!
	MVF	STK05,1,0

	BTSS	_STATUS,4,0	; hardware is .. big endian
	TBLR	*+
	MVF	_TBLDH,0,0	; H is first byte!!
	BTSZ	_STATUS,4,0	; hardware is .. big endian
	MVF	_TBLDL,0,0	; H is first byte!!
	MVF	STK04,1,0

	BTSZ	_STATUS,4,0	; hardware is .. big endian
	TBLR	*+
	MVF	_TBLDH,0,0	; H is first byte!!
	BTSS	_STATUS,4,0	; hardware is .. big endian
	MVF	_TBLDL,0,0	; H is first byte!!
	MVF	STK03,1,0

	BTSS	_STATUS,4,0	; hardware is .. big endian
	TBLR	*+
	MVF	_TBLDH,0,0	; H is first byte!!
	BTSZ	_STATUS,4,0	; hardware is .. big endian
	MVF	_TBLDL,0,0	; H is first byte!!
	;MVF	STK02,1,0
	MVF	TMP0,1,0

	BTSZ	_STATUS,4,0	; hardware is .. big endian
	TBLR	*+
	MVF	_TBLDH,0,0	; H is first byte!!
	BTSS	_STATUS,4,0	; hardware is .. big endian
	MVF	_TBLDL,0,0	; H is first byte!!
	;MVF	STK01,1,0
	MVF	TMP1,1,0


	BTSS	_STATUS,4,0	; hardware is .. big endian
	TBLR	*+
	MVF	_TBLDH,0,0	; H is first byte!!
	BTSZ	_STATUS,4,0	; hardware is .. big endian
	MVF	_TBLDL,0,0	; H is first byte!!
	;MVF	STK00,1,0
	MVF	TMP2,1,0

	BTSZ	_STATUS,4,0	; hardware is .. big endian
	TBLR	*+
	MVF	_TBLDH,0,0	; H is first byte!!
	BTSS	_STATUS,4,0	; hardware is .. big endian
	MVF	_TBLDL,0,0	; H is first byte!!
	MVF	TMPA,1,0
	MVF	TMP2,0,0
	MVF	STK00,1,0
	MVF	TMP1,0,0
	MVF	STK01,1,0
	MVF	TMP0,0,0
	MVF	STK02,1,0
	MVF	TMPA,0,0
FIN:
	RET

	
	.ENDFUNC __g2ptrget8
	
; 2 exit points
; exit point of __mulint

;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.area UDATA(DATA)
	TMP0: .ds 1
	TMP1: .ds 1
	TMP2: .ds 1
	TMPA: .ds 1

	.include "hy15p41sfr.inc"
	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global declarations
;--------------------------------------------------------
	.globl	__g2ptrget8

	.area STK(STK)
__g2ptrget8_STK00: .ds 1
	.globl __g2ptrget8_STK00
;--------------------------------------------------------
; global definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
	end
