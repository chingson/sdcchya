
#define MSB_SET(x) (x&0x80000000L)

unsigned long
_divulong (unsigned long x, unsigned long y)
{
  unsigned long reste = 0L;
  unsigned char count = 32;

  do
  {
    // reste: x <- 0;
    reste <<= 1;
    if(MSB_SET(x))
    {
      reste |= 1L;
    }
    x <<= 1;

    if (reste >= y)
    {
      reste -= y;
      // x <- (result = 1)
      x |= 1L;
    }
  }
  while (--count);
  return x;
}

