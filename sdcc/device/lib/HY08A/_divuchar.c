
unsigned char
_divuchar (unsigned char x, unsigned char y)
{
  unsigned int reste = 0;
  unsigned char count = 8;

  do
  {
    // reste: x <- 0;
    reste <<= 1;
	if(x&0x80)
	{
      		reste |= 1;
	}
    x <<= 1;

    if (reste >= y)
    {
      reste -= y;
      // x <- (result = 1)
      x |= 1;
    }
  }
  while (--count);
  return x;
}

