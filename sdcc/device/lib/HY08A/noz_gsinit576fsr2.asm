
	.module noz_gsinit576fsr2
	.area SFR1 (DATA,ABS)
	.include "hy17p58sfr.inc"

	.area CODE (code) ; only 1 code area
.globl		__sdcc_gsinit_startup576fsr2
.globl		_main
.globl		s_IDATA
.globl		s_IDATAROM
.globl		l_IDATA
.globl		a_STKTOP
.globl 	__sdcc_gsinit_startup576fsr2

; H08D uses flat memory model,
; 80~17f are used by 
__sdcc_gsinit_startup576fsr2:
;	ldpr	0x80,0
;	mvl	0x00
;loop_clear_ram0:
;	clrf	_POINC0,0
;	INSZ	_WREG,1,0	; d=1 or 0 should be the same?
;	JMP	loop_clear_ram0
;// next is to copy idata
;	ldpr	0x200,0 ; 200~33f
;	mvl		0x3
;loop_clear_ram1:
;	clrf	_POINC0,0
;	CPSL	_FSR0H,0
;	BTSS	_FSR0L,6 ; max 33f.. but 
;	JMP		loop_clear_ram1

	
	ldpr	s_IDATA,0 ; // s_IDATA can be start at 200
	mvlp	s_IDATAROM

MVL l_IDATA>>8
loop_copy0:
	IORL  0
	JZ   copy_left
	CLRF _FSR1L,0
loop_a:
	TBLR	*+
	MVFF	_TBLDH,_POINC0
	MVFF	_TBLDL,_POINC0
	INF     _FSR1L,1,0
	BTSS	_FSR1L,7
	JMP		loop_a
	DCF		_WREG,0,0
	jmp		loop_copy0
    

copy_left:




	MVL	l_IDATA+1
loop_copy1:
	DCSUZ	_WREG,1,0
	JMP	pre1
	TBLR	*+
	MVFF	_TBLDH,_POINC0

; we have limits, idata length <256
; and when crossing boundary, it must be 1 byte
	; if 180, we jump to 200!!
	; WREG save to FSR2L
	MVF	_FSR2L,1,0
	MVL		080H
	ADDF	_FSR0L,0,0
	JNZ		CONTINI
	MVL		0FFH
	ADDF	_FSR0H,0,0
	JNZ		CONTINI
	
	ldpr	0x200,0 ; // 180->0x200

CONTINI:	
	MVF	_FSR2L,0,0
	DCSUZ	_WREG,1,0
	JMP	pre1
	MVFF	_TBLDL,_POINC0
	JMP	loop_copy1
	
pre1:
	// bsr set to 200
	LBSR	0x200
	// set FSR2
	;BSF		_OSCCN1,7 ; c compiler enable
	LDPR		a_STKTOP-1,2
	JMP		_main
	
	


