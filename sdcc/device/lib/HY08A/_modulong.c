
#define MSB_SET(x) (x&0x80000000L)

unsigned long
_modulong (unsigned long a, unsigned long b)
{
  unsigned char count = 0;

  while (!MSB_SET(b) && b<=a)
  {
     b <<= 1;
     count++;
  }
  do
  {
    if (a >= b)
      a -= b;
    b >>= 1;
  }
  while (count--);

  return a;
}

