;--------------------------------------------------------
;--------------------------------------------------------
	.module _shortBCDinc
;--------------------------------------------------------
; overlayable items in internal ram 
; return value is void
;--------------------------------------------------------
;	
.area CCODE(code,REL,CON) ;
	
	.FUNC __shortBCDinc:$r:0:2:$L:__shortBCDinc_STK00
		; maximum input is 32768, output is 0x32768, long return value W is 0
__shortBCDinc:	
	MVF	_FSR0H,1,0
	MVF __shortBCDinc_STK00,0,0
	MVF _FSR0L,1,0
	INF _INDF0,0,0
	DAW 
	MVF _POINC0,1,0
	MVL 0
	ADDC _INDF0,0,0
	DAW
	MVF _POINC0,1,0

	RET

	
	

	.globl	__shortBCDinc
	.globl __shortBCDinc_STK00

	.ENDFUNC __shortBCDinc
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.include "hy11p13sfr.inc"
	.area UDATA(DATA)


	.area STK(STK)
__shortBCDinc_STK00: .DS 	1	

;--------------------------------------------------------
; global declarations
;--------------------------------------------------------

;--------------------------------------------------------
; global definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
	end
