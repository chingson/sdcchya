
#define MSB_SET(x) (x&0x80000000L)

long
_divslong (long x, long y)
{
  
  struct aa { unsigned char needinv:1;} xx;
   unsigned long reste = 0L;
  unsigned char count = 32;

  xx.needinv=0;
  if(x<0)
  {
      xx.needinv=1;
      x=-x;
  }
  if(y<0)
  {
      xx.needinv^=1;
      y=-y;
  }
do
  {
    // reste: x <- 0;
    reste <<= 1;
    if(MSB_SET(x))
    {
      reste |= 1L;
    }
    x <<= 1;

    if (reste >= y)
    {
      reste -= y;
      // x <- (result = 1)
      x |= 1L;
    }
  }while (--count);
   if (xx.needinv)
    return -x;
  return x;
}

