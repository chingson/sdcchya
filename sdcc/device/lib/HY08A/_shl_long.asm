;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.3.0 #8604 (Sep 15 2014) (MSVC)
; This file was generated Sat Oct 11 21:41:17 2014
;--------------------------------------------------------
	.module _shl_long
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
;--------------------------------------------------------
; Code Head 
;--------------------------------------------------------
	.area CCODE	 (CODE) 
.FUNC __shl_long:$r:4:1
;--------------------------------------------------------
; code
; keep LSB at STK03 W
;--------------------------------------------------------
__shl_long:	;Function start
; return if 0
	IORL		0
	JZ		fin
loop_shr:
	BCF		_STATUS,4 ; c clear
	RLFC	STK02,1,0
	RLFC	STK01,1,0
	RLFC	STK00,1,0
	RLFC	STK03,1,0
	DCSZ	_WREG,1,0
	JMP		loop_shr
fin:
	MVF	STK03,0,0
	RET
	
	
	

; exit point of _shl_long



;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
;--------------------------------------------------------
; global declarations
;--------------------------------------------------------
	.globl	__shl_long
	.include "hy11p13sfr.inc"
	.globl	STK00
	.globl	STK01
	.globl	STK02
	.globl	STK03


;--------------------------------------------------------
; global definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
	;end
