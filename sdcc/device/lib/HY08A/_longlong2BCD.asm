;--------------------------------------------------------
;--------------------------------------------------------
	.module _longlong2BCD
;--------------------------------------------------------
; overlayable items in internal ram 
; return value is (unsigned) long
;--------------------------------------------------------
;	udata_ovr
.area CCODE(code,REL,CON) ;
	
	.FUNC __longlong2BCD:$r:8:8:$L:__longlong2BCD_STK00:$L:__longlong2BCD_STK01:$L:__longlong2BCD_STK02\
	:$L:__longlong2BCD_STK03:$L:__longlong2BCD_STK04:$L:__longlong2BCD_STK05\
	:$L:__longlong2BCD_STK06\
	:$L:r0x1001:$L:r0x1002
		; maximum input is 99999999 99999999, 0x2386F26FC0FFFF, check 7 bytes
__longlong2BCD:	
	MVL  56
	MVF  r0x1001,1,0

	CLRF STK06	; LSB
	CLRF STK05
	CLRF STK04
	CLRF STK03
	CLRF STK02	
	CLRF STK01 
	CLRF STK00
	CLRF r0x1002 ; MSB
LOOP56:	
	RLFC __longlong2BCD_STK06,1,0 
	RLFC __longlong2BCD_STK05,1,0
	RLFC __longlong2BCD_STK04,1,0
	RLFC __longlong2BCD_STK03,1,0 
	RLFC __longlong2BCD_STK02,1,0
	RLFC __longlong2BCD_STK01,1,0
	RLFC __longlong2BCD_STK00,1,0 

	MVF  STK06,0,0
	ADDC STK06,0,0
	DAW
	MVF  STK06,1,0

	MVF  STK05,0,0
	ADDC STK05,0,0
	DAW
	MVF  STK05,1,0

	MVF  STK04,0,0
	ADDC STK04,0,0
	DAW
	MVF  STK04,1,0

	MVF  STK03,0,0
	ADDC STK03,0,0
	DAW
	MVF  STK03,1,0

	MVF  STK02,0,0
	ADDC STK02,0,0
	DAW
	MVF  STK02,1,0

	MVF  STK01,0,0
	ADDC STK01,0,0
	DAW
	MVF  STK01,1,0

	MVF  STK00,0,0
	ADDC STK00,0,0
	DAW
	MVF  STK00,1,0

	MVF  r0x1002,0,0
	ADDC r0x1002,0,0
	DAW
	MVF  r0x1002,1,0


	DCSZ r0x1001,1,0
	JMP LOOP56
	; MSB should be in W !!
	RET

	
	

	.globl	__longlong2BCD
	.globl __longlong2BCD_STK00
	.globl __longlong2BCD_STK01
	.globl __longlong2BCD_STK02
	.globl __longlong2BCD_STK03
	.globl __longlong2BCD_STK04
	.globl __longlong2BCD_STK05
	.globl __longlong2BCD_STK06

	.ENDFUNC __longlong2BCD
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.include "hy11p13sfr.inc"
	.area UDATA(DATA)


	.globl STK06
	.globl STK05
	.globl STK04
	.globl STK03

	.globl STK02
	.globl STK01
	.globl STK00

	.area STK(STK)
__longlong2BCD_STK00: .DS 	1	
__longlong2BCD_STK01: .DS 	1	
__longlong2BCD_STK02: .DS 	1	
__longlong2BCD_STK03: .DS 	1	
__longlong2BCD_STK04: .DS 	1	
__longlong2BCD_STK05: .DS 	1	
__longlong2BCD_STK06: .DS 	1	

;r0x1000:		.DS 		1
r0x1001:		.DS 		1
r0x1002:		.DS 		1

;--------------------------------------------------------
; global declarations
;--------------------------------------------------------

;--------------------------------------------------------
; global definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
	end
