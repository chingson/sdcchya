;--------------------------------------------------------
;--------------------------------------------------------
	.module _longlongBCDSum
;--------------------------------------------------------
; overlayable items in internal ram 
; return value is (unsigned) long
;--------------------------------------------------------
;	udata_ovr
.area CCODE(code,REL,CON) ;
	
	.FUNC __longlongBCDSum:$r:8:16:$L:__longlongBCDSum_STK00:$L:__longlongBCDSum_STK01:$L:__longlongBCDSum_STK02:$L:__longlongBCDSum_STK03:\
	$L:__longlongBCDSum_STK04:$L:__longlongBCDSum_STK05:$L:__longlongBCDSum_STK06:\
	$L:__longlongBCDSum_STK07:$L:__longlongBCDSum_STK08:$L:__longlongBCDSum_STK09:$L:__longlongBCDSum_STK10:\
	$L:__longlongBCDSum_STK11:$L:__longlongBCDSum_STK12:$L:__longlongBCDSum_STK13:$L:__longlongBCDSum_STK14:\
	$L:r0x1000	; input is W, output is also W
		; maximum input is 32768, output is 0x32768, long return value W is 0
__longlongBCDSum:	
	MVF  	r0x1000,1,0

	MVF 	__longlongBCDSum_STK14,0,0
	ADDF 	__longlongBCDSum_STK06,0,0
	DAW 
	MVF		STK06,1,0

	MVF 	__longlongBCDSum_STK13,0,0
	ADDC 	__longlongBCDSum_STK05,0,0
	DAW 
	MVF		STK05,1,0

	MVF 	__longlongBCDSum_STK12,0,0
	ADDC 	__longlongBCDSum_STK04,0,0
	DAW 
	MVF		STK04,1,0

	MVF 	__longlongBCDSum_STK11,0,0
	ADDC 	__longlongBCDSum_STK03,0,0
	DAW 
	MVF		STK03,1,0

	MVF 	__longlongBCDSum_STK10,0,0
	ADDC 	__longlongBCDSum_STK02,0,0
	DAW 
	MVF		STK02,1,0

	MVF 	__longlongBCDSum_STK09,0,0
	ADDC 	__longlongBCDSum_STK01,0,0
	DAW 
	MVF		STK01,1,0

	MVF 	__longlongBCDSum_STK08,0,0
	ADDC 	__longlongBCDSum_STK00,0,0
	DAW 
	MVF		STK00,1,0

	MVF 	__longlongBCDSum_STK07,0,0
	ADDC 	r0x1000,0,0
	DAW 
	RET

	
	

	.globl	__longlongBCDSum
	.globl __longlongBCDSum_STK00
	.globl __longlongBCDSum_STK01
	.globl __longlongBCDSum_STK02
	.globl __longlongBCDSum_STK03
	.globl __longlongBCDSum_STK04
	.globl __longlongBCDSum_STK05
	.globl __longlongBCDSum_STK06
	.globl __longlongBCDSum_STK07
	.globl __longlongBCDSum_STK08
	.globl __longlongBCDSum_STK09
	.globl __longlongBCDSum_STK10
	.globl __longlongBCDSum_STK11
	.globl __longlongBCDSum_STK12
	.globl __longlongBCDSum_STK13
	.globl __longlongBCDSum_STK14
	.globl STK00
	.globl STK01
	.globl STK02
	.globl STK03
	.globl STK04
	.globl STK05
	.globl STK06
	.ENDFUNC __longlongBCDSum
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.include "hy11p13sfr.inc"
	.area UDATA(DATA)

	.area STK(STK)
__longlongBCDSum_STK00: .DS 	1	
__longlongBCDSum_STK01: .DS 	1	
__longlongBCDSum_STK02: .DS 	1	
__longlongBCDSum_STK03: .DS 	1	
__longlongBCDSum_STK04: .DS 	1	
__longlongBCDSum_STK05: .DS 	1	
__longlongBCDSum_STK06: .DS 	1	
__longlongBCDSum_STK07: .DS 	1	
__longlongBCDSum_STK08: .DS 	1	
__longlongBCDSum_STK09: .DS 	1	
__longlongBCDSum_STK10: .DS 	1	
__longlongBCDSum_STK11: .DS 	1	
__longlongBCDSum_STK12: .DS 	1	
__longlongBCDSum_STK13: .DS 	1	
__longlongBCDSum_STK14: .DS 	1	
r0x1000: .DS 1

;--------------------------------------------------------
; global declarations
;--------------------------------------------------------

;--------------------------------------------------------
; global definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
	end
