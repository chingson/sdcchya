;--------------------------------------------------------
;--------------------------------------------------------
	.module _longBCDSum
;--------------------------------------------------------
; overlayable items in internal ram 
; return value is (unsigned) long
;--------------------------------------------------------
;	udata_ovr
.area CCODE(code,REL,CON) ;
	
	.FUNC __longBCDSum:$r:4:8:$L:__longBCDSum_STK00:$L:__longBCDSum_STK01:$L:__longBCDSum_STK02:$L:__longBCDSum_STK03:$L:__longBCDSum_STK04:$L:__longBCDSum_STK05:$L:__longBCDSum_STK06:$L:r0x1000	; input is W, output is also W
		; maximum input is 32768, output is 0x32768, long return value W is 0
__longBCDSum:	
	MVF  	r0x1000,1,0

	MVF 	__longBCDSum_STK06,0,0
	ADDF 	__longBCDSum_STK02,0,0
	DAW 
	MVF		STK02,1,0

	MVF 	__longBCDSum_STK05,0,0
	ADDC 	__longBCDSum_STK01,0,0
	DAW 
	MVF		STK01,1,0

	MVF 	__longBCDSum_STK04,0,0
	ADDC 	__longBCDSum_STK00,0,0
	DAW 
	MVF		STK00,1,0

	MVF 	__longBCDSum_STK03,0,0
	ADDC 	r0x1000,0,0
	DAW 
	RET

	
	

	.globl	__longBCDSum
	.globl __longBCDSum_STK00
	.globl __longBCDSum_STK01
	.globl __longBCDSum_STK02
	.globl __longBCDSum_STK03
	.globl __longBCDSum_STK04
	.globl __longBCDSum_STK05
	.globl __longBCDSum_STK06
	.globl STK00
	.globl STK01
	.globl STK02
	.ENDFUNC __longBCDSum
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.include "hy11p13sfr.inc"
	.area UDATA(DATA)

	.area STK(STK)
__longBCDSum_STK00: .DS 	1	
__longBCDSum_STK01: .DS 	1	
__longBCDSum_STK02: .DS 	1	
__longBCDSum_STK03: .DS 	1	
__longBCDSum_STK04: .DS 	1	
__longBCDSum_STK05: .DS 	1	
__longBCDSum_STK06: .DS 	1	
r0x1000: .DS 1

;--------------------------------------------------------
; global declarations
;--------------------------------------------------------

;--------------------------------------------------------
; global definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
	end
