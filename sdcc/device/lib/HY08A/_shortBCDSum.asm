;--------------------------------------------------------
;--------------------------------------------------------
	.module _shortBCDSum
;--------------------------------------------------------
; overlayable items in internal ram 
; return value is (unsigned) long
;--------------------------------------------------------
;	udata_ovr
.area CCODE(code,REL,CON) ;
	
	.FUNC __shortBCDSum:$r:2:4:$L:__shortBCDSum_STK00:$L:__shortBCDSum_STK01:$L:__shortBCDSum_STK02:$L:r0x1000	; input is W, output is also W
		; maximum input is 32768, output is 0x32768, long return value W is 0
__shortBCDSum:	
	MVF  	r0x1000,1,0
	MVF 	__shortBCDSum_STK02,0,0
	ADDF 	__shortBCDSum_STK00,0,0
	DAW 
	MVF		STK00,1,0
	MVF		r0x1000,0,0
	ADDC 	__shortBCDSum_STK01,0,0
	DAW		; high byte in W !!
	RET

	
	

	.globl	__shortBCDSum
	.globl __shortBCDSum_STK00
	.globl __shortBCDSum_STK01
	.globl __shortBCDSum_STK02
	.globl STK00
	.ENDFUNC __shortBCDSum
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.include "hy11p13sfr.inc"
	.area UDATA(DATA)

	.area STK(STK)
__shortBCDSum_STK00: .DS 	1	
__shortBCDSum_STK01: .DS 	1	
__shortBCDSum_STK02: .DS 	1	
r0x1000: .DS 1

;--------------------------------------------------------
; global declarations
;--------------------------------------------------------

;--------------------------------------------------------
; global definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
	end
