
	.module gsinit256fsr2ini80
	.area SFR1 (DATA,ABS)
	.include "hy17m24sfr.inc"

	.area CODE (code) ; only 1 code area
.globl		__sdcc_gsinit_startup256fsr2ini80
.globl		_main
.globl		s_IDATA
.globl		s_IDATAROM
.globl		l_IDATA
.globl		a_STKTOP
.globl 	__sdcc_gsinit_startup256fsr2ini80
	;_OSCCN1 = 0x35;

; H08D uses flat memory model,
; 80~17f are used by 
__sdcc_gsinit_startup256fsr2ini80:
	ldpr	0x80,0
	mvl	0x00
loop_clear_ram0:
	clrf	_POINC0,0
	INSZ	_WREG,1,0	; d=1 or 0 should be the same?
	JMP	loop_clear_ram0
;// next is to copy idata .. from 180, no regret
;loop_clear_ram1:
;	clrf	_POINC0,0
;	INSZ	_WREG,1,0	; d=1 or 0 should be the same?
;	JMP	loop_clear_ram1

	lbsr	0x00
	
	ldpr	s_IDATA,0 ; // s_IDATA can be start at 200
	mvlp	s_IDATAROM
	MVL	l_IDATA+1
loop_copy1:
	DCSUZ	_WREG,1,0
	JMP	pre1
	TBLR	*+
	MVFF	_TBLDH,_POINC0
	DCSUZ	_WREG,1,0
	JMP	pre1
	MVFF	_TBLDL,_POINC0
	JMP	loop_copy1
	
pre1:
	// bsr set to 100, we have no 2XX
	LBSR	0x100
	// set FSR2
	;BSF		_OSCCN1,7 ; c compiler enable
	LDPR		a_STKTOP,2
	JMP		_main
	
	


