
#define MSB_SET(x) (x&0x80)
unsigned char
_moduchar (unsigned char a, unsigned char b)
{
 unsigned char count = 0;

  while (!MSB_SET(b) && b<=a)
  {
    b <<= 1;
    count++;
  }
  do
  {
    if (a >= b)
      a -= b;
    b >>= 1;
  }
  while (count--);
  return a;
}

