
	.module noz_gsinit384fsr2
	.area SFR1 (DATA,ABS)
	.include "hy17p56sfr.inc"

	.area CODE (code) ; only 1 code area
.globl		__sdcc_gsinit_startup384fsr2
.globl		_main
.globl		s_IDATA
.globl		s_IDATAROM
.globl		l_IDATA
.globl		a_STKTOP
.globl 	__sdcc_gsinit_startup384fsr2
	;_OSCCN1 = 0x35;

; H08D uses flat memory model,
; 80~17f are used by 
__sdcc_gsinit_startup384fsr2:
;	ldpr	0x80,0
;	mvl	0x00
;loop_clear_ram0:
;	clrf	_POINC0,0
;	INSZ	_WREG,1,0	; d=1 or 0 should be the same?
;	JMP	loop_clear_ram0
;// next is to copy idata
;	ldpr	0x200,0
;	mvl	0x80	; only 128 bytes
;loop_clear_ram1:
;	clrf	_POINC0,0
;	INSZ	_WREG,1,0	; d=1 or 0 should be the same?
;	JMP	loop_clear_ram1

	lbsr	0x00
	
	ldpr	s_IDATA,0 ; // s_IDATA can be start at 200
	mvlp	s_IDATAROM
	MVL	l_IDATA+1
loop_copy1:
	DCSUZ	_WREG,1,0
	JMP	pre1
	TBLR	*+
	MVFF	_TBLDH,_POINC0
	DCSUZ	_WREG,1,0
	JMP	pre1
	MVFF	_TBLDL,_POINC0
	JMP	loop_copy1
	
pre1:
	// bsr set to 200
	LBSR	0x200
	// set FSR2
	;BSF		_OSCCN1,7 ; c compiler enable
	LDPR		a_STKTOP-1,2
	JMP		_main
	
	


