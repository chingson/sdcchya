;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.50 #9253 (Feb 22 2016) (MSVC)
; This file was generated Mon Feb 22 17:50:08 2016
;--------------------------------------------------------
; Port for HYCON CPU
;--------------------------------------------------------
	.module _mullong
	;.list	p=HY11P13
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CCODE (code,REL,CON) ; _mullong-code 
;entry:  __mullong:	;Function start
; 2 exit points
;has an exit
;; Starting pCode block
	.include "hy11p13sfr.inc"
	.FUNC __mullong:$r:4:8:$L:PROD3:$L:PROD2:$L:PROD1:$L:PROD0:$L:__mullong_STK00:$L:__mullong_STK01 :$L:__mullong_STK02 :$L:__mullong_STK03 :$L:__mullong_STK04 :$L:__mullong_STK05 :$L:__mullong_STK06
__mullong:	;Function start
; mul from MSB, no need set to 0
; 2 exit points
	MULF	__mullong_STK06,0
	MVFF	_PRODL,PROD3

	MVF	__mullong_STK00,0,0	; W=A2
	MULF	__mullong_STK06,0		; A2*B0
	MVFF	_PRODL,PROD2
	MVF	_PRODH,0,0	; result is P2,P3
	ADDF	PROD3,1,0

	MVF	__mullong_STK00,0,0	; W=A2
	MULF	__mullong_STK05,0		; A2*B1
	MVF	_PRODL,0,0
	ADDF	PROD3,1,0	; result is P3 only


	MVF	__mullong_STK01,0,0	; W=A1
	MULF	__mullong_STK06,0		; A1*B0, result is P12, but P3 may add1
	MVFF	_PRODL,PROD1
	MVF	_PRODH,0,0
	ADDF	PROD2,1,0
	MVL	0
	ADDC	PROD3,1,0

	MVF	__mullong_STK01,0,0	; W=A1
	MULF	__mullong_STK05,0		; A1*B1, result is P23, 
	MVF	_PRODL,0,0
	ADDF	PROD2,1,0
	MVF	_PRODH,0,0
	ADDC	PROD3,1,0

	MVF	__mullong_STK01,0,0	; W=A1
	MULF	__mullong_STK04,0		; A1*B2, result is P3, 
	MVF	_PRODL,0,0
	ADDF	PROD3,1,0


	MVF	__mullong_STK02,0,0	; W=A0
	MULF	__mullong_STK06,0		; A0*B0
	MVFF	_PRODL,PROD0
	MVF	_PRODH,0,0
	ADDF	PROD1,1,0
	MVL	0
	ADDC	PROD2,1,0
	ADDC	PROD3,1,0


	MVF	__mullong_STK02,0,0	; W=A0
	MULF	__mullong_STK05,0		; A0*B1, result is P12, but P3 may add1
	MVF	_PRODL,0,0
	ADDF	PROD1,1,0
	MVF	_PRODH,0,0
	ADDC	PROD2,1,0
	MVL	0
	ADDC	PROD3,1,0

	MVF	__mullong_STK02,0,0	; W=A0
	MULF	__mullong_STK04,0		; A0*B2, result is P23, 
	MVF	_PRODL,0,0
	ADDF	PROD2,1,0
	MVF	_PRODH,0,0
	ADDC	PROD3,1,0

	MVF	__mullong_STK02,0,0	; W=A0
	MULF	__mullong_STK03,0		; A0*B3, result is P3, 
	MVF	_PRODL,0,0
	ADDF	PROD3,0,0
	MVFF	PROD2,STK00
	MVFF	PROD1,STK01
	MVFF	PROD0,STK02
	RET
; exit point of __mullong
	.ENDFUNC __mullong

;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.area LOCALSTK (STK)
__mullong_STK06:	.ds 1
__mullong_STK05:	.ds 1
__mullong_STK04:	.ds 1
__mullong_STK03:	.ds 1
__mullong_STK02:	.ds 1
__mullong_STK01:	.ds 1
__mullong_STK00:	.ds 1
	.globl __mullong_STK06
	.globl __mullong_STK05
	.globl __mullong_STK04
	.globl __mullong_STK03
	.globl __mullong_STK02
	.globl __mullong_STK01
	.globl __mullong_STK00

	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global declarations
;--------------------------------------------------------
	.globl	__mullong

;--------------------------------------------------------
; global definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area UDATA (DATA,REL,CON) ;UDL__mullong	udata
PROD0:		.ds	1
PROD1:		.ds	1
PROD2:		.ds	1
PROD3:		.ds	1
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
	end
