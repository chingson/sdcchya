/*-------------------------------------------------------------------------
   setjmp.c - source file for ANSI routines setjmp & longjmp

   Copyright (C) 1999, Sandeep Dutta . sandeep.dutta@usa.net

   This library is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 2, or (at your option) any
   later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this library; see the file COPYING. If not, write to the
   Free Software Foundation, 51 Franklin Street, Fifth Floor, Boston,
   MA 02110-1301, USA.

   As a special exception, if you link this library with other files,
   some of which are compiled with SDCC, to produce an executable,
   this library does not by itself cause the resulting executable to
   be covered by the GNU General Public License. This exception does
   not however invalidate any other reasons why the executable file
   might be covered by the GNU General Public License.
-------------------------------------------------------------------------*/

#include <setjmp.h>
#define FSR2H (*((__data unsigned char *)0x13))
#define FSR2L (*((__data unsigned char *)0x14))
//extern volatile unsigned char TOSL;
//extern volatile unsigned char TOSH;
__sfr __at 0x17 TOSL;
__sfr __at 0x16 TOSH;
__sfr __at 0x01 POINC0;
__sfr __at 0x1B PCLATL;
__sfr __at 0x1A PCLATH;
#define STKPTR (*((__data unsigned char *)0x18))
extern volatile __xdata unsigned char * FSR0;
int __setjmp(jmp_buf x)
{
	FSR0=x;
__asm
	MVF _TOSL,0,0
	MVF _POINC0,1,0
	MVF _TOSH,0,0
	MVF _POINC0,1,0
	DCF 0x18,0,0
	MVF _POINC0,1,0
__endasm;
// sub 1 for fsr2
#ifdef MODE08D
	__asm
	ADDFSR 2,-1
	__endasm;
#endif
	__asm
	MVF 0x14,0,0 ; fsr2l
	MVF _POINC0,1,0
	MVF 0x13,0,0 ; fsr2l
	MVF _POINC0,1,0
	__endasm;
#ifdef MODE08D
	__asm
	ADDFSR 2,1
	__endasm;
#endif
	return 0;
}
static unsigned short addrsetjmp;
static unsigned short retval;
void longjmp(jmp_buf j, int r)
{
volatile	unsigned char stk;
	FSR0=j;
	addrsetjmp=POINC0;
	addrsetjmp|=(POINC0<<8);
	stk=POINC0;
	while(STKPTR!=stk)
		__asm POP __endasm;
	retval=r;
	if(!r)
		retval=1;
	FSR2L=POINC0;
	FSR2H=POINC0;
	__asm
		MVFF _retval,STK00
		MVF	 (_retval+1),0,0
		MVFF	(_addrsetjmp+1),_PCLATH
		MVFF	(_addrsetjmp),_PCLATL
	.globl _longjmp
	__endasm;
}

