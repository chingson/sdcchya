
	.module gsinit128
	.area SFR1 (DATA,ABS)
	.include "hy11p13sfr.inc"

	.area CODE (code) ; only 1 code area
.globl		__sdcc_gsinit_startup128
.globl		_main
.globl		s_IDATA
.globl		s_IDATAROM
.globl		l_IDATA


__sdcc_gsinit_startup128:
	ldpr	0x80,0
	mvl	0x80
loop_clear_ram0:
	clrf	_POINC0,0
	INSZ	_WREG,1,0	; d=1 or 0 should be the same?
	JMP	loop_clear_ram0
;// next is to copy idata

	lbsr	0x100
	
	ldpr	s_IDATA,0
	mvlp	s_IDATAROM
	MVL	l_IDATA+1
loop_copy1:
	DCSUZ	_WREG,1,0
	JMP	_main
	TBLR	*+
	MVFF	_TBLDH,_POINC0
	DCSUZ	_WREG,1,0
	JMP	_main
	MVFF	_TBLDL,_POINC0
	JMP	loop_copy1
	
	
	


