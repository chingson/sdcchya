;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.50 #9253 (Feb 22 2016) (MSVC)
; This file was generated Mon Feb 22 17:50:08 2016
;--------------------------------------------------------
; Port for HYCON CPU
;--------------------------------------------------------
	.module _g2ptrget2
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CCODE(code,REL,CON) ; _mulint-code 
;entry:  __mulint:	;Function start
; 2 exit points
;has an exit
;; Starting pCode block
.FUNC __g2ptrget2:$r:2:2:$L:__g2ptrget2_STK00


__g2ptrget2:	;Function start
	BTSZ	_WREG,7,0 ; 80 means ROM
	JMP	ROMACC
	
	MVF _FSR0H,1,0
	MVF	__g2ptrget2_STK00,0,0
	MVF _FSR0L,1,0
	MVF	_POINC0,0,0
	MVF	STK00,1,0
	MVF	_POINC0,0,0
	JMP FIN

	
ROMACC:
	;BCF	_STATUS,4,0
	RRFC	_WREG,0,0
	ANDL	0x3f
	MVF	_TBLPTRH,1,0
	RRFC	__g2ptrget2_STK00,0,0
	MVF	_TBLPTRL,1,0
	TBLR	*+
	MVF	_TBLDH,0,0	; H is first byte!!
	BTSZ	_STATUS,4,0	; hardware is .. big endian
	MVF	_TBLDL,0,0	; L is second byte
	MVF	STK00,1,0
	BTSZ	_STATUS,4,0	; hardware is .. big endian
	TBLR	*+
;	BTGF	_STATUS,4,0	; change C
	MVF	_TBLDH,0,0	; H is first byte!!
	BTSS	_STATUS,4,0	; hardware is .. big endian
	MVF	_TBLDL,0,0	; H is first byte!!
FIN:
	RET
	.ENDFUNC __g2ptrget2

	
	
; 2 exit points
	RET
; exit point of __mulint

;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.area UDATA(DATA)

	.include "hy11p13sfr.inc"
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global declarations
;--------------------------------------------------------
	.globl	__g2ptrget2
	.area STK(STK)
__g2ptrget2_STK00: .ds 1
	.globl __g2ptrget2_STK00

;--------------------------------------------------------
; global definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
	end
