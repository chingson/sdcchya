;--------------------------------------------------------
;--------------------------------------------------------
	.module _byteBCDSum
;--------------------------------------------------------
; overlayable items in internal ram 
; return value is (unsigned) long
;--------------------------------------------------------
;	udata_ovr
.area CCODE(code,REL,CON) ;
	
	.FUNC __byteBCDSum:$r:1:2:$L:__byteBCDSum_STK00	; input is W, output is also W
		; maximum input is 32768, output is 0x32768, long return value W is 0
__byteBCDSum:	
	ADDF __byteBCDSum_STK00,0,0
	DAW 
	RET

	
	

	.globl	__byteBCDSum
	.globl __byteBCDSum_STK00

	.ENDFUNC __byteBCDSum
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.include "hy11p13sfr.inc"
	.area UDATA(DATA)


	.area STK(STK)
__byteBCDSum_STK00: .DS 	1	

;--------------------------------------------------------
; global declarations
;--------------------------------------------------------

;--------------------------------------------------------
; global definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
	end
