
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.3.0 #8604 (Sep 15 2014) (MSVC)
; This file was generated Sat Oct 11 21:41:17 2014
;--------------------------------------------------------
	.module _shr_slong
;--------------------------------------------------------
; Code Head 
;--------------------------------------------------------
	.area CCODE	 (CODE) 

.FUNC __shr_slong:$r:4:1
;--------------------------------------------------------
; code
;--------------------------------------------------------
; sh_slong, assembly is the source!!
__shr_slong:	;Function start
; 2 exit points
	;MVF		__shr_slong_tmp,1,0
	IORL	0
	JZ		FIN
loop_shr:
	;RLFC	STK03,0,0
	ARRC	STK03,1,0
	RRFC	STK00,1,0
	RRFC	STK01,1,0
	RRFC	STK02,1,0
	;DCSZ	__shr_slong_tmp,1,0
	ADDL		#0FFH
	JNZ		loop_shr
FIN:
	MVF		STK03,0,0
	RET
	
	
	

; exit point of _shr_slong


;--------------------------------------------------------
; global declarations
;--------------------------------------------------------
	.globl	__shr_slong
	.include "hy11p13sfr.inc"
	.globl	STK00
	.globl	STK01
	.globl	STK02
	.globl	STK03

	.area UDATA (DATA); local stack var
;__shr_slong_STK00:	.ds	1
;__shr_slong_tmp:	.ds	1
	;.globl __shl_slong_STK00

;--------------------------------------------------------
; global definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
	;end
