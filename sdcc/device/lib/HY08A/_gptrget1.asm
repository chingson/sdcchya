;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.50 #9253 (Feb 22 2016) (MSVC)
; This file was generated Mon Feb 22 17:50:08 2016
;--------------------------------------------------------
; Port for HYCON CPU
;--------------------------------------------------------
	.module _gptrget1
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CCODE(code,REL,CON) ; _mulint-code 
;entry:  __mulint:	;Function start
; 2 exit points
;has an exit
;; Starting pCode block
	
	.FUNC __gptrget1:$r:1:3:$L:__gptrget1_STK00:$L:__gptrget1_STK01

__gptrget1:	;Function start
	BTSZ	_WREG,7,0 ; 80 means ROM
	JMP	ROMACC
	;// here is RAM
	;MVFF	__gptrget1_STK00,_FSR0H
	;MVFF	__gptrget1_STK01,_FSR0L
	MVF	__gptrget1_STK00,0,0
	MVF _FSR0H,1,0
	MVF	__gptrget1_STK01,0,0
	MVF _FSR0L,1,0
	MVF	_POINC0,0,0
	JMP	FIN

	
ROMACC:
	BCF	_STATUS,4,0
	RRFC	__gptrget1_STK00,0,0
	MVF	_TBLPTRH,1,0
	RRFC	__gptrget1_STK01,0,0
	MVF	_TBLPTRL,1,0
	TBLR	*+
	MVF	_TBLDH,0,0	; H is first byte!!
	BTSZ	_STATUS,4,0	; hardware is .. big endian
	MVF	_TBLDL,0,0	; L is second byte
FIN:
	RET

	
	

	.globl	__gptrget1

	.ENDFUNC __gptrget1
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.area UDATA(DATA)

	.include "hy11p13sfr.inc"
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00

	.area STK(STK)
__gptrget1_STK00: .ds 1
__gptrget1_STK01: .ds 1
	.globl __gptrget1_STK00
	.globl __gptrget1_STK01
;--------------------------------------------------------
; global declarations
;--------------------------------------------------------

;--------------------------------------------------------
; global definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
	end
