#include<bcdlib.h>
unsigned long long _BCD2longlong(unsigned long long i)
{
	return _BCD2byte((i>>56)&0xff)*100000000000000ULL +
		_BCD2byte((i>>48)&0xff)   *1000000000000ULL +
		_BCD2byte((i>>40)&0xff)   *10000000000ULL +
		_BCD2byte((i>>32)&0xff)   *100000000ULL +
		_BCD2byte((i>>24)&0xff)   *1000000UL +
		_BCD2byte((i>>16)&0xff)   *10000UL +
		_BCD2byte((i>>8)&0xff)    *100 +
		_BCD2byte(i&0xff);
}
