;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.50 #9253 (Feb 22 2016) (MSVC)
; This file was generated Mon Feb 22 17:50:08 2016
;--------------------------------------------------------
; Port for HYCON CPU
;--------------------------------------------------------
	.module _gptrput4
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CCODE(code,REL,CON) ; _mulint-code 
;entry:  __mulint:	;Function start
; 2 exit points
;has an exit
;; Starting pCode block
	
	.FUNC __gptrput4:$r:0:7:$L:__gptrput4_STK00:$L:__gptrput4_STK01:\
$L:__gptrput4_STK02:$L:__gptrput4_STK03:$L:__gptrput4_STK04:$L:__gptrput4_STK05

__gptrput4:	;Function start
	BTSZ	_WREG,7,0 ; 80 means ROM
	JMP	ROMACC
	;// here is RAM
	;MVFF	__gptrput4_STK00,_FSR0H
	;MVFF	__gptrput4_STK01,_FSR0L
	MVF		__gptrput4_STK00,0,0
	MVF		_FSR0H,1,0
	MVF		__gptrput4_STK01,0,0
	MVF		_FSR0L,1,0
	MVF	__gptrput4_STK02,0,0
	MVF	_POINC0,1,0
	MVF	__gptrput4_STK03,0,0
	MVF	_POINC0,1,0
	MVF	__gptrput4_STK04,0,0
	MVF	_POINC0,1,0
	MVF	__gptrput4_STK05,0,0
	MVF	_POINC0,1,0
	
ROMACC:
	RET

	
	

	.globl	__gptrput4

	.ENDFUNC __gptrput4
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.area UDATA(DATA)

	.include "hy11p13sfr.inc"
	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00

	.area STK(STK)
__gptrput4_STK00: .ds 1
__gptrput4_STK01: .ds 1
__gptrput4_STK02: .ds 1
__gptrput4_STK03: .ds 1
__gptrput4_STK04: .ds 1
__gptrput4_STK05: .ds 1
	.globl __gptrput4_STK00
	.globl __gptrput4_STK01
	.globl __gptrput4_STK02
	.globl __gptrput4_STK03
	.globl __gptrput4_STK04
	.globl __gptrput4_STK05
;--------------------------------------------------------
; global declarations
;--------------------------------------------------------

;--------------------------------------------------------
; global definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
	end
