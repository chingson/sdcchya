;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.3.0 #8604 (Sep 15 2014) (MSVC)
; This file was generated Sat Oct 11 21:41:17 2014
;--------------------------------------------------------
	.module _shr_slonglong
;--------------------------------------------------------
; Code Head 
;--------------------------------------------------------
	.area CCODE	 (CODE) 

.FUNC __shr_slonglong:$r:8:1
;--------------------------------------------------------
; code
;--------------------------------------------------------
; sh_slonglong, assembly is the source!!
__shr_slonglong:	;Function start
; 2 exit points
	IORL	0
	JZ		FIN
	;MVF		__shr_slonglong_tmp,1,0 ; because w will be destroyed later
loop_shr:
	ARRC	STK07,1,0
	RRFC	STK00,1,0
	RRFC	STK01,1,0
	RRFC	STK02,1,0
	RRFC	STK03,1,0
	RRFC	STK04,1,0
	RRFC	STK05,1,0
	RRFC	STK06,1,0
	DCSZ	_WREG,1,0
	JMP		loop_shr
FIN:
	MVF		STK07,0,0
	RET
	
	
	

; exit point of _shr_slonglong


;--------------------------------------------------------
; global declarations
;--------------------------------------------------------
	.globl	__shr_slonglong
	.include "hy11p13sfr.inc"
	.globl	STK00
	.globl	STK01
	.globl	STK02
	.globl	STK03
	.globl	STK04
	.globl	STK05
	.globl	STK06
	.globl	STK07

	.area UDATA (DATA); local stack var
;__shr_slonglong_tmp:	.ds	1
	;.globl __shl_slonglong_STK00

;--------------------------------------------------------
; global definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
	;end

;--------------------------------------------------------
; global definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
	;end
