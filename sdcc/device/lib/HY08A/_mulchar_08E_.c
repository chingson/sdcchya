
#pragma save
#pragma disable_warning 126 /* unreachable code */
#pragma disable_warning 116 /* left shifting more than size of object */
unsigned int
_mulchar (unsigned char a, unsigned char b) __critical
{
  unsigned int  result = 0;
  unsigned int  bb = b;
  unsigned char i;

  /* check all bits in a byte */
  for (i = 0; i < 8u; i++) {
    /* check all bytes in operand (generic code, optimized by the compiler) */
    if (a & 0x01u) result += bb;
    //if (sizeof (a) > 1 && (a & 0x00000100u)) result += (b << 8u);
    //if (sizeof (a) > 2 && (a & 0x00010000ul)) result += (b << 16u);
    //if (sizeof (a) > 3 && (a & 0x01000000ul)) result += (b << 24u);
    a  >>= 1u;
    bb <<= 1u;
  } // for i

  return result;
}
#pragma restore
