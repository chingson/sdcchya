;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.50 #9253 (Feb 22 2016) (MSVC)
; This file was generated Mon Feb 22 17:50:08 2016
;--------------------------------------------------------
; Port for HYCON CPU
;--------------------------------------------------------
;;	.file	"_mulint.c"
	.module _mulint
	;.list	p=HY11P13
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;	udata_ovr
.area CCODE (code,REL,CON) ; _mulint-code 
;entry:  __mulint:	;Function start
; 2 exit points
;has an exit
;; Starting pCode block
	.include "hy11p13sfr.inc"
	.FUNC __mulint:$r:2:4:$L:__mulint_STK00:$L:__mulint_STK01:$L:__mulint_STK02:$L:r_TEMP0
	; W: AMSB
	; STK00: ALSB
	; STK01: BMSB
	; STK02: BLSB
__mulint:	;Function start
; 2 exit points
	MVF	r_TEMP0,1,0		; AMSB is saved
	MVF	__mulint_STK00,0,0		; A LSB to W
	MULF	__mulint_STK02,0		; MUL AB LSB, W is ALSB
	MVFF	_PRODL,STK00		; stk00 not used anymore, this is low byte
	MVFF	_PRODH,__mulint_STK00
	MULF	__mulint_STK01,0			;ALSB *BMSB (STK00xSTK01)
	MVF	_PRODL,0,0
	ADDF	__mulint_STK00,1,0
	MVF	r_TEMP0,0,0		; AMSB 2 w
	MULF	__mulint_STK02,0			; AMSB*BLSB
	MVF	_PRODL,0,0
	ADDF	__mulint_STK00,0,0		; MSB is here, result is [W,STK00 ]
	RET
; exit point of __mulint

	.ENDFUNC __mulint
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------

	.globl STK03
	.globl STK02
	.globl STK01
	.globl STK00
;--------------------------------------------------------
; global declarations
;--------------------------------------------------------
	.globl	__mulint

;--------------------------------------------------------
; global definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
	.area UDATA (DATA,REL,CON) ;UDL__mulint	udata
r_TEMP0:	.ds	1
	.area STK (STK) ;UDL__mulint_0	udata
__mulint_STK00: .ds 1	
__mulint_STK01: .ds 1	
__mulint_STK02: .ds 1	
	.globl __mulint_STK00
	.globl __mulint_STK01
	.globl __mulint_STK02
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
	end
