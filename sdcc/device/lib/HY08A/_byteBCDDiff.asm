;--------------------------------------------------------
;--------------------------------------------------------
	.module _byteBCDDiff
;--------------------------------------------------------
; overlayable items in internal ram 
; return value is (unsigned) long
;--------------------------------------------------------
;	udata_ovr
.area CCODE(code,REL,CON) ;
	
	.FUNC __byteBCDDiff:$r:2:2:$L:__byteBCDDiff_STK00:$L:r0x1000	; 
		; return value is W - STK00
__byteBCDDiff:	
	MVF  r0x1000,1,0
	MVF	 __byteBCDDiff_STK00,0,0
	SUBF r0x1000,0,0
	RLFC r0x1000,1,0	; keep C in temp
	BTSS _STATUS,3		; DC is bit 3
	ADDL 250			; (-6)
	BTSS r0x1000,0
	ADDL 0xA0
	RRFC r0x1000,1,0	; get back c

	RET

	
	

	.globl	__byteBCDDiff
	.globl __byteBCDDiff_STK00

	.ENDFUNC __byteBCDDiff
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.include "hy11p13sfr.inc"
	.area UDATA(DATA)


	.area STK(STK)
__byteBCDDiff_STK00: .DS 	1	
r0x1000: .DS 1

;--------------------------------------------------------
; global declarations
;--------------------------------------------------------

;--------------------------------------------------------
; global definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
	end
