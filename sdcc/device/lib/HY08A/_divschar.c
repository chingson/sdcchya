/*-------------------------------------------------------------------------
   _divschar.c :- routine for signed char (8 bit) division. just calls
                  routine for signed int division after sign extension

   Copyright (C) 2013, Philipp Klaus Krause . pkk@spth.de

   This library is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 2, or (at your option) any
   later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License 
   along with this library; see the file COPYING. If not, write to the
   Free Software Foundation, 51 Franklin Street, Fifth Floor, Boston,
   MA 02110-1301, USA.

   As a special exception, if you link this library with other files,
   some of which are compiled with SDCC, to produce an executable,
   this library does not by itself cause the resulting executable to
   be covered by the GNU General Public License. This exception does
   not however invalidate any other reasons why the executable file
   might be covered by the GNU General Public License.
-------------------------------------------------------------------------*/
#define UC(x) ((unsigned char)x)
signed int
_divschar (signed char x, signed char y)
{
  struct { unsigned char needinv:1;} xx;
  unsigned char reste = 0;
  unsigned char count = 8;

  xx.needinv=0;
  if(x<0)
  {
	x=-x;
    xx.needinv=1;
  }
  if(y<0)
  {
    y=-y;
    xx.needinv^=1;
  }
  do
  {
    // reste: x <- 0;
    reste <<= 1;
	  if(x&0x80)
	  {
    	reste |= 1;
	  }
    x <<= 1;

    if (reste >= UC(y))
    {
      reste -= y;
      // x <- (result = 1)
      x |= 1;
    }
  }
  while (--count);
  
  if(xx.needinv)
    return (signed int)-x;
  return (signed int)x;



}

signed char
_divuschar (unsigned char x, unsigned char y)
{
  struct { unsigned char needinv:1;} xx;
  unsigned char reste = 0;
  unsigned char count = 8;

  xx.needinv=0;
  if(y&0x80)
  {
    y=-y;
    xx.needinv=1;
  }
  do
  {
    // reste: x <- 0;
    reste <<= 1;
	  if(x&0x80)
	  {
    	reste |= 1;
	  }
    x <<= 1;

    if (reste >= y)
    {
      reste -= y;
      // x <- (result = 1)
      x |= 1;
    }
  }
  while (--count);
  
  if(xx.needinv)
    return (signed int)-x;
  return (signed int)x;
}

unsigned char
_divsuchar (signed char x, signed char y)
{
struct { unsigned char needinv:1;} xx;
  unsigned char reste = 0;
  unsigned char count = 8;

  xx.needinv=0;
  if(x<0)
  {
  	x=-x;
    xx.needinv=1;
  }
  do
  {
    // reste: x <- 0;
    reste <<= 1;
	  if(x&0x80)
	  {
    	reste |= 1;
	  }
    x <<= 1;

    if (reste >= UC(y))
    {
      reste -= y;
      // x <- (result = 1)
      x |= 1;
    }
  }
  while (--count);
  
  if(xx.needinv)
    return -x;
  return x;
  
}

