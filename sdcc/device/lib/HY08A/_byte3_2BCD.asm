;--------------------------------------------------------
;--------------------------------------------------------
	.module _byte3_2BCD
;--------------------------------------------------------
; overlayable items in internal ram 
; return value is (unsigned) long
;--------------------------------------------------------
;	udata_ovr
.area CCODE(code,REL,CON) ;
	
	.FUNC __byte3_2BCD:$r:4:4:$L:__byte3_2BCD_STK00:$L:__byte3_2BCD_STK01:$L:__byte3_2BCD_STK02:$L:r0x1001:$L:r0x1002	
		; maximum input is 16777215, output is 0x0x16777215, long return value W is the MSB
__byte3_2BCD:	
	;MVF  r0x1000,1,0 // MSB no use
	MVL  24
	MVF  r0x1001,1,0
	CLRF STK02	; LSB
	CLRF STK01 
	CLRF STK00
	CLRF r0x1002 ; MSB
LOOP24:	
	RLFC __byte3_2BCD_STK02,1,0
	RLFC __byte3_2BCD_STK01,1,0
	RLFC __byte3_2BCD_STK00,1,0

	MVF  STK02,0,0
	ADDC STK02,0,0
	DAW
	MVF  STK02,1,0

	MVF  STK01,0,0
	ADDC STK01,0,0
	DAW
	MVF  STK01,1,0

	MVF  STK00,0,0
	ADDC STK00,0,0
	DAW
	MVF  STK00,1,0

	MVF  r0x1002,0,0
	ADDC r0x1002,0,0
	DAW
	MVF  r0x1002,1,0


	DCSZ r0x1001,1,0
	JMP LOOP24
	; MSB should be in W !!
	RET

	
	

	.globl	__byte3_2BCD
	.globl __byte3_2BCD_STK00
	.globl __byte3_2BCD_STK01
	.globl __byte3_2BCD_STK02

	.ENDFUNC __byte3_2BCD
;--------------------------------------------------------
; external declarations
;--------------------------------------------------------
	.include "hy11p13sfr.inc"
	.area UDATA(DATA)

	.globl STK01
	.globl STK02
	.globl STK00

	.area STK(STK)
__byte3_2BCD_STK00: .DS 	1	
__byte3_2BCD_STK01: .DS 	1	
__byte3_2BCD_STK02: .DS 	1	

r0x1001:		.DS 		1
r0x1002:		.DS 		1

;--------------------------------------------------------
; global declarations
;--------------------------------------------------------

;--------------------------------------------------------
; global definitions
;--------------------------------------------------------
;--------------------------------------------------------
; absolute symbol definitions
;--------------------------------------------------------
;--------------------------------------------------------
; compiler-defined variables
;--------------------------------------------------------
;--------------------------------------------------------
; initialized data
;--------------------------------------------------------
	end
