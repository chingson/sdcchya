
	.module gsinit640
	.area SFR1 (DATA,ABS)
	.include "hy11p13sfr.inc"

	.area	IDATAX	(XDATA) ;test-2-data
	.area	XDATAROM	(CODE) 
	.area CODE (code) ; only 1 code area
.globl		__sdcc_gsinit_startup640
.globl		_main
.globl		s_IDATA
.globl		s_IDATAROM
.globl		l_IDATA
.globl		s_IDATAX
.globl		e_IDATAX
.globl		s_XDATAROM


__sdcc_gsinit_startup640:
	ldpr	0x80,0
	mvl	0
loop_clear_ram0:
	clrf	_POINC0,0
	INSZ	_WREG,1,0	; d=1 or 0 should be the same?
	JMP	loop_clear_ram0


	ldpr	0x200,0
loop_clear_ram1:
	clrf	_POINC0,0
	INSZ	_WREG,1,0	; d=1 or 0 should be the same?
	JMP	loop_clear_ram1
loop_clear_ram2:
	clrf	_POINC0,0
	BTSZ	_FSR0L,7  ; until 0x380
	JMP    loop_clear_ram2

;// next is to copy idata

	lbsr	0x100
	
	ldpr	s_IDATA,0
	mvlp	s_IDATAROM
	MVL	l_IDATA+1
loop_copy1:
	DCSUZ	_WREG,1,0
	JMP	_COPY2
	TBLR	*+
	MVFF	_TBLDH,_POINC0
	DCSUZ	_WREG,1,0
	JMP	_COPY2
	MVFF	_TBLDL,_POINC0
	JMP	loop_copy1
_COPY2:
	ldpr	s_IDATAX,0
	mvlp	s_XDATAROM
	MVL	l_IDATAX+1 	; only 256 bytes 
loop_copy2:
	DCSUZ	_WREG,1,0
	JMP	_main
	TBLR	*+
	MVFF	_TBLDH,_POINC0
	DCSUZ	_WREG,1,0
	JMP	_main
	MVFF	_TBLDL,_POINC0
	JMP	loop_copy2
	
	
	
	
	


