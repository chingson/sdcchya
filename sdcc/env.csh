#!/bin/csh
set rootdir=`dirname $0`       # may be relative path
set rootdir=`cd $rootdir && pwd`    # ensure absolute path
echo $rootdir
setenv SDCC_HOME $rootdir/device
setenv PATH $rootdir/bin:$PATH
